'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const TipoObjecto = use("App/Models/TipoObjecto");
const Database = use("Database");

/**
 * Resourceful controller for interacting with tipoobjectos
 */
class TipoObjectoController {
  /**
   * Show a list of all tipoobjectos.
   * GET tipoobjectos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_objectos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_objectos")
        .where("tipo_objectos.slug", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new tipoobjecto.
   * GET tipoobjectos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new tipoobjecto.
   * POST tipoobjectos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { descricao, slug } = request.all();

    const Verify = await TipoObjecto.query()
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um semelhante",
        Verify
      );

    } else {
      const res = await TipoObjecto.create({
        descricao: descricao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  /**
  * Update tipoobjecto details.
  * PUT or PATCH tipoobjectos/:id
  *
  * @param {object} ctx
  * @param {Request} ctx.request
  * @param {Response} ctx.response
  */


  async update({ params, request }) {
    const data = request.only(["descricao", "slug"]);

    const Verify = await TipoObjecto.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um com mesmo slug",
        Verify
      );
    } else {

      // update with new data entered
      const res = await TipoObjecto.find(params.id);
      res.merge(data);
      await res.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        res
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'descricao', 'slug')
      .from('tipo_objectos')
      .orderBy('descricao', 'ASC');

    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Display a single tipoobjecto.
   * GET tipoobjectos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing tipoobjecto.
   * GET tipoobjectos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }



  /**
   * Delete a tipoobjecto with id.
   * DELETE tipoobjectos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = TipoObjectoController
