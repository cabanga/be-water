'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with flatrateservicos
 */
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
class FlatRateServicoController {
  /**
   * Show a list of all flatrateservicos.
   * GET flatrateservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new flatrateservico.
   * GET flatrateservicos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new flatrateservico.
   * POST flatrateservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single flatrateservico.
   * GET flatrateservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
      const {servicoId}= request.all();
      const flatRate= await Database.select("flat_rate_servicos.id", "flat_rate_servicos.valor","flat_rate_servicos.origem","flat_rate_servicos.capacidade").from('flat_rate_servicos').where('servico_id',servicoId);
      return DataResponse.response("success", 200, "", flatRate);
      
  }

  /**
   * Render a form to update an existing flatrateservico.
   * GET flatrateservicos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update flatrateservico details.
   * PUT or PATCH flatrateservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
     const { id, valor, capacidade, origem}= request.all();

    await Database
    .table('flat_rate_servicos')
    .where('id', id )
    .update({
      'valor':valor,
      'capacidade':capacidade,
      'origem':origem
    })

    return DataResponse.response("success", 200, "Flat Rate  actualizado com sucesso", null);
  }

  /**
   * Delete a flatrateservico with id.
   * DELETE flatrateservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = FlatRateServicoController
