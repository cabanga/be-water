'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const LteCpe = use("App/Models/LteCpe");
const LteNumero = use("App/Models/LteNumero");
const Servico = use("App/Models/Servico");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')
/**
 * Resourceful controller for interacting with ltecpes
 */
class LteCpeController {
  /**
   * Show a list of all ltecpes.
   * GET ltecpes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  async searchNumeroSerie({ request }) {
    const { start, end, search } = request.all();



    const numeroSerie = await Database
      .select('id', 'numero_serie', 'tipo', 'fabricante', 'modelo')
      .from('lte_cpes')
      .where("numero_serie", "like", "%" + search + "%")
      .paginate(start, end);


    return DataResponse.response("success", 200, null, numeroSerie);
  }


  async validateTelefoneLTE({ request, response, auth }) {

    ///console.log(request.all())

    const telefone = request.all().numero_telefone;

    //console.log(telefone)

    const filia = await Database
      .select('fi.id as filialID', 'fi.nome as filialNome')
      .table('lojas as lo')
      .innerJoin('filials as fi', 'fi.id', 'lo.filial_id')
      .where('lo.id', auth.user.loja_id).first()

    //console.log(filia)

    if (filia == undefined) {
      return response.status(302).send(Mensagem.response(response.response.statusCode, 'Utilizador sem loja associada', null))

    }

    const res = await LteNumero.query().where('filial_id', filia.filialID).where('numero', telefone).select('*').first()
    //console.log(res)
    const servico = await Servico.query().where('chaveServico ', telefone).select('*').first()

    //console.log(servico)

    if (res && !servico) {

      console.log(1)

      return response.status(200).send(Mensagem.response(response.response.statusCode, 'Encontrado', null))


    } else if (res && servico) {
      return response.status(302).send(Mensagem.response(response.response.statusCode, 'Usado', null))
    } else {
      //Não encontrado
      return response.status(404).send(Mensagem.response(response.response.statusCode, 'Esse número não existe', null))
    }




  }

  async searchTelefone({ request, auth }) {
    const { start, end, search } = request.all();
    let res = null;
    let subquery = null;
    let chaves = null;

    //console.log(request.all())

    const filia = await Database
      .select('fi.id as filialID', 'fi.nome as filialNome')
      .table('lojas as lo')
      .innerJoin('filials as fi', 'fi.id', 'lo.filial_id')
      .where('lo.id', auth.user.loja_id).first()

    if (filia == undefined) {
      return response.status(302).send(Mensagem.response(response.response.statusCode, 'Utilizador sem loja associada', null))

    } else if (search == null) {
      subquery = Database
        .from('servicos')
        .whereNotNull('ChaveServico')
        .select('ChaveServico')

      chaves = await Database
        .select('id', 'numero', 'filial_id')
        .from('lte_numeros')
        .where('filial_id', filia.filialID)
        .whereNotIn('numero', subquery)
        .paginate(start, end);
    } else {
      subquery = Database
        .from('servicos')
        .whereNotNull('ChaveServico')
        .select('ChaveServico')

      chaves = await Database
        .select('id', 'numero', 'filial_id')
        .from('lte_numeros')
        .where('filial_id', filia.filialID)
        .whereNotIn('numero', subquery)
        .where("numero", "like", "%" + search + "%")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, null, chaves);
  }

  async searchFabricante({ request }) {
    const { start, end, search } = request.all();



    const fabricante = await Database
      .select('fabricante')
      .from('lte_cpes')
      .where("fabricante", "like", "%" + search + "%")
      .paginate(start, end);


    return DataResponse.response("success", 200, null, fabricante);
  }


  async searchModelo({ request }) {
    const { start, end, search } = request.all();



    const modelo = await Database
      .select('modelo')
      .from('lte_cpes')
      .where("modelo", "like", "%" + search + "%")
      .paginate(start, end);


    return DataResponse.response("success", 200, null, modelo);
  }


  async searchTipo({ request }) {
    const { start, end, search } = request.all();

    const tipo = await Database
      .select('tipo')
      .from('lte_cpes')
      .where("tipo", "like", "%" + search + "%")
      .paginate(start, end);


    return DataResponse.response("success", 200, null, tipo);
  }

  /**
   * Render a form to be used for creating a new ltecpe.
   * GET ltecpes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new ltecpe.
   * POST ltecpes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { modelo, fabricante, numero_serie, tipo } = request.all();
    const lte_cpes = await LteCpe.query()
      .where("numero_serie", numero_serie)
      .getCount();
    if (lte_cpes > 0) {
      return DataResponse.response(
        "success",
        500,
        "Esse número de série já existe",
        lte_cpes
      );
    } else {
      await LteCpe.create({
        fabricante: fabricante,
        modelo: modelo,
        numero_serie: numero_serie,
        tipo: tipo,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Dispositivo registado com sucesso",
        null
      );
    }
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "id",
        "modelo",
        "fabricante",
        "numero_serie",
        "tipo",
        "created_at"
      )
        .from("lte_cpes")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "id",
        "modelo",
        "fabricante",
        "numero_serie",
        "tipo",
        "created_at"
      )
        .from("lte_cpes")
        .where("numero_serie", "like", "%" + search + "%")
        .orWhere("modelo", "like", "%" + search + "%")
        .orWhere("fabricante", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Display a single ltecpe.
   * GET ltecpes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing ltecpe.
   * GET ltecpes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update ltecpe details.
   * PUT or PATCH ltecpes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a ltecpe with id.
   * DELETE ltecpes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = LteCpeController
