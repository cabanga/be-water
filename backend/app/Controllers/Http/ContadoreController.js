'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const Contadore = use("App/Models/Contadore");
const Contrato = use("App/Models/Contrato");
const LogsContadore = use("App/Models/LogsContadore");
const LteNumero = use("App/Models/LteNumero");
const Servico = use("App/Models/Servico");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')
var moment = require("moment");


class ContadoreController {

  async index({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "cont.id",
        "cont.contador",
        "cont.marca_id",
        "marcas.descricao as marca",
        "cont.modelo_id",
        "modelos.descricao as modelo",
        "cont.medicao_id",
        "medicaos.descricao as medicao",
        "cont.contador_ID",
        "cont.numero_serie",
        "cont.id_classe_precisao",
        "classe_precisaos.descricao as classe_precisao",
        "cont.calibre_id",
        "calibres.descricao as calibre",
        "cont.caudal_id",
        "cont.tipo_contador_id",
        "cont.estado_contador_id",
        "cont.digitos",
        "cont.ano_fabrico",
        "cont.ano_instalacao",
        "cont.observacao",
        "cont.fabricante_id",
        "fabricantes.descricao as fabricante",
        "cont.selo",
        "cont.created_at",
        "cont.armazem_id",
        "armazens.nome as centro_distribuicao",
        "estado_contadores.slug as estadoContadorSlug",
        "estado_contadores.descricao as estadoContadorDesc",
        "cont.ultima_verificacao",
        "cont.ultima_leitura"
      )
        .from("contadores as cont")
        .leftJoin("marcas", "marcas.id", "cont.marca_id")
        .leftJoin("modelos", "modelos.id", "cont.modelo_id")
        .leftJoin("medicaos", "medicaos.id", "cont.medicao_id")
        .leftJoin("classe_precisaos", "classe_precisaos.id", "cont.id_classe_precisao")
        .leftJoin("armazens", "armazens.id", "cont.armazem_id")
        .leftJoin("fabricantes", "fabricantes.id", "cont.fabricante_id")
        .leftJoin("estado_contadores", "cont.estado_contador_id", "estado_contadores.id")
        .leftJoin("calibres", "cont.calibre_id", "calibres.id")
        .orderBy(orderBy == null ? "cont.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "cont.id",
        "cont.contador",
        "cont.marca_id",
        "marcas.descricao as marca",
        "cont.modelo_id",
        "modelos.descricao as modelo",
        "cont.medicao_id",
        "medicaos.descricao as medicao",
        "cont.contador_ID",
        "cont.numero_serie",
        "cont.id_classe_precisao",
        "classe_precisaos.descricao as classe_precisao",
        "cont.calibre_id",
        "calibres.descricao as calibre",
        "cont.caudal_id",
        "cont.tipo_contador_id",
        "cont.estado_contador_id",
        "cont.digitos",
        "cont.ano_fabrico",
        "cont.ano_instalacao",
        "cont.observacao",
        "cont.fabricante_id",
        "fabricantes.descricao as fabricante",
        "cont.selo",
        "cont.created_at",
        "cont.armazem_id",
        "armazens.nome as centro_distribuicao",
        "estado_contadores.slug as estadoContadorSlug",
        "estado_contadores.descricao as estadoContadorDesc",
        "cont.ultima_verificacao",
        "cont.ultima_leitura"
      )
        .from("contadores as cont")
        .leftJoin("marcas", "marcas.id", "cont.marca_id")
        .leftJoin("modelos", "modelos.id", "cont.modelo_id")
        .leftJoin("medicaos", "medicaos.id", "cont.medicao_id")
        .leftJoin("fabricantes", "fabricantes.id", "cont.fabricante_id")
        .leftJoin("classe_precisaos", "classe_precisaos.id", "cont.id_classe_precisao")
        .leftJoin("armazens", "armazens.id", "cont.armazem_id")
        .leftJoin("estado_contadores", "cont.estado_contador_id", "estado_contadores.id")
        .leftJoin("calibres", "cont.calibre_id", "calibres.id")
        .orWhere("cont.numero_serie", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(cont.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "cont.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async validateSerie({ request, response, auth }) {

    const serie = request.all().numero_serie;

    const res = await Contadore.query().where('numero_serie', serie).select('*').first()

    if (!res) {
      return response.status(404).send(Mensagem.response(response.response.statusCode, 'Esse número não existe', null))
    }

    const validateUse = await Database.select("id")
      .table("local_consumos")
      .where("contador_id", res.id).first();

    if (res && !validateUse) {

      const resultado = await Database
        .select('contadores.id', 'contadores.numero_serie', 'marcas.descricao as marca',
          'modelos.descricao as modelo', 'contadores.ultima_leitura')
        .table('contadores')
        .leftJoin("marcas", "contadores.marca_id", "marcas.id")
        .leftJoin("modelos", "contadores.modelo_id", "modelos.id")
        .where('numero_serie', res.numero_serie).first()

      return response.status(200).send(Mensagem.response(response.response.statusCode, 'Sucesso', resultado))


    } else if (res && validateUse) {
      return response.status(302).send(Mensagem.response(response.response.statusCode, 'Contador em uso', null))
    }




  }

  async viewinfo({ params }) {


    const res = await Database
      .select('contadores.id', 'contadores.numero_serie', 'contadores.calibre', 'medicaos.descricao as medicao', 'classe_precisaos.descricao as precisao', 'marcas.descricao as marca',
        'modelos.descricao as modelo', 'contadores.ultima_leitura')
      .table('contadores')
      .leftJoin("marcas", "contadores.marca_id", "marcas.id")
      .leftJoin("modelos", "contadores.modelo_id", "modelos.id")
      .leftJoin("classe_precisaos", "contadores.id_classe_precisao", "classe_precisaos.id")
      .leftJoin("medicaos", "contadores.medicao_id", "medicaos.id")
      .where('contadores.id', params.id).first()

    return DataResponse.response("success", 200, "", res);




  }


  async store({ request, auth }) {

    const {
      contador,
      contador_ID,
      marca_id,
      modelo_id,
      medicao_id,
      numero_serie,
      id_classe_precisao,
      calibre_id,
      caudal_id,
      digitos,
      ano_fabrico,
      ano_instalacao,
      observacao,
      fabricante_id,
      tipo_contador_id,
      estado_contador_id,
      selo,
      armazem_id,
      ultima_leitura,
      ultima_verificacao,
    } = request.all();

    const estadoContador = await Database.select("id")
      .from("estado_contadores")
      .where("slug", "DISPONIVEL")
      .first();

    const res = await Contadore.query()

      .where("numero_serie", numero_serie)
      .getCount();
    if (res > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um contador com o mesmo Nº de Contador",
        null
      );
    } else {
      const Contador = await Contadore.create({
        contador: contador,
        contador_ID: contador_ID,
        marca_id: marca_id,
        modelo_id: modelo_id,
        medicao_id: medicao_id,
        calibre_id: calibre_id,
        caudal_id: caudal_id,
        numero_serie: numero_serie,
        id_classe_precisao: id_classe_precisao,
        digitos: digitos,
        ano_fabrico: ano_fabrico,
        ano_instalacao: ano_instalacao,
        observacao: observacao,
        fabricante_id: fabricante_id,
        estado_contador_id: estado_contador_id,
        selo: selo,
        armazem_id: armazem_id,
        tipo_contador_id: tipo_contador_id,
        ultima_leitura: ultima_leitura,
        ultima_verificacao: ultima_verificacao,
        user_id: auth.user.id

      });

      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        Contador
      );
    }
  }

  async storeIntroducaoManual({ request, auth, params }) {
    const { tipo_registo_id, ultima_leitura, data } = request.all();
    let operacao = null;
    operacao = "REGISTAR LEITURA";

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const introducao_manual = await Database.table('contadores_introducao_manuals').insert({
      contador_id: params.id,
      tipo_registo_id: tipo_registo_id,
      ultima_leitura: ultima_leitura,
      data: data,
      user_id: auth.user.id,
      'created_at': dataActual,
      'updated_at': dataActual
    });

    const leitura = await Database.table('contadores').update({
      ultima_leitura: ultima_leitura,
      user_id: auth.user.id,
      'updated_at': dataActual
    })
      .where('id', params.id)

    const logContador = new LogsContadore();
    await logContador.registrarLogContador(auth.user.id, operacao, 'contadores_introducao_manuals', params.id, ultima_leitura, tipo_registo_id)

    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      introducao_manual
    );
  }

  async LeiturabyContador({ params }) {
    const res = await Database.select('ultima_leitura')
      .from('contadores')
      .where('id', params.id).first()

    return DataResponse.response(
      "success",
      200,
      null,
      res
    );
  }

  async getContador({ params }) {
    const contador = await Database.select(
      "contadores.id",
      "contadores.numero_serie",
      "contadores.marca_id",
      "marcas.descricao as marca",
      "contadores.modelo_id",
      "modelos.descricao as modelo",
      "estado_contadores.descricao as estadoDescricao",
      "contadores.estado_contador_id"
    )
      .from("contadores")
      .leftJoin("estado_contadores", "estado_contadores.id", "contadores.estado_contador_id")
      .leftJoin("marcas", "marcas.id", "contadores.marca_id")
      .leftJoin("modelos", "modelos.id", "contadores.modelo_id")
      .where("contadores.id", params.id).first()

    return DataResponse.response("success", 200, "", contador);
  }

  async getEstadoFilhosByEstadoPai({ request, params }) {
    const getEstadoContador = await Database.select(
      "contadores.id",
      "contadores.numero_serie",
      "contadores.estado_contador_id"
    )
      .from("contadores")
      .where("id", params.id).first()

    log.estado_contador_id = getEstadoContador.estado_contador_id

    const contador = await Database.select(
      "relacao_estado_contadores.id",
      "relacao_estado_contadores.estado_pai",
      "relacao_estado_contadores.estado_filho",
      "estado_contadores.descricao"
    )
      .from("relacao_estado_contadores")
      .leftJoin("estado_contadores", "estado_contadores.id", "relacao_estado_contadores.estado_filho")
      .where("estado_pai", log.estado_contador_id)

    return DataResponse.response("success", 200, "", contador);
  }

  async updateEstado({ request, auth, params }) {
    const { estado_contador_id, observacao } = request.all();
    const log = request.only(['observacao_old', 'ultima_leitura', 'estado_contador_id_old']);
    let operacao = null;
    operacao = "ATUALIZAR ESTADO";
    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");


    const getDadosContador = await Database.select(
      "id",
      "observacao",
      "ultima_leitura"
    )
      .from("contadores")
      .where("id", params.id).first()

    const getEstadoUsado = await Database.select(
      "contadores.id",
      "contadores.estado_contador_id",
      "estado_contadores.slug"
    )
      .from("contadores")
      .leftJoin("estado_contadores", "contadores.estado_contador_id", "estado_contadores.id")
      .where("contadores.id", params.id).first()

    log.observacao_old = getDadosContador.observacao;
    log.ultima_leitura = getDadosContador.ultima_leitura;
    log.estado_contador_id_old = getEstadoUsado.estado_contador_id;

    if (getEstadoUsado.slug == "USADO") {

      const estado = await Database.table('contadores').update({
        estado_contador_id: estado_contador_id,
        observacao: observacao,
        user_id: auth.user.id,
        'updated_at': dataActual
      })
        .where('id', params.id)

      await Database.table('local_consumos').update({
        contador_id: null,
        user_id: auth.user.id,
        'updated_at': dataActual
      })
        .where('contador_id', params.id)

      const logContador = new LogsContadore();
      await logContador.registrarLogContador(auth.user.id, operacao, 'contadores', params.id, log.ultima_leitura, null, log.estado_contador_id_old, estado_contador_id, log.observacao_old, observacao)

      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        estado
      );

    } else {

      const estado = await Database.table('contadores').update({
        estado_contador_id: estado_contador_id,
        observacao: observacao,
        user_id: auth.user.id,
        'updated_at': dataActual
      })
        .where('id', params.id)

      const logContador = new LogsContadore();
      await logContador.registrarLogContador(auth.user.id, operacao, 'contadores', params.id, log.ultima_leitura, null, log.estado_contador_id_old, estado_contador_id, log.observacao_old, observacao)

      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        estado
      );
    }
  }


  async show({ response }) {
    const res = await Database.select('id', 'contador').from('contadores')
      .orderBy("contadores.contador", "ASC");

    return res;
  }

  async getContadoresByCliente({ params }) {

    const res = await Database.select(
      'id',
      'contador',
      'cliente_id',
      'numero_serie'
    ).from('contadores')
      .where('contadores.cliente_id', params.id)
      .orderBy("contadores.contador", "ASC");

    /* console.log(params.id);
    console.log(res); */
    return res;
  }

  async edit({ params, request, response, view }) {
  }


  async update({ params, request }) {
    const data = request.only([
      "contador",
      "contador_ID",
      "marca_id",
      "modelo_id",
      "medicao_id",
      "numero_serie",
      "id_classe_precisao",
      "calibre_id",
      "caudal_id",
      "digitos",
      "ano_fabrico",
      "ano_instalacao",
      "validade",
      "observacao",
      "fabricante_id",
      "especificacao",
      "selo",
      "estado_contador_id",
      "tipo_contador_id",
      "tipo_fabricacao",
      "armazem_id",
      "ultima_verificacao",
      "ultima_leitura"
    ]);

    const res = await Contadore.query()
      .where("numero_serie", data.numero_serie)
      .whereNot({ id: params.id })
      .getCount();

    const validateUse = await Contadore.query().where('numero_serie', data.numero_serie).whereNotNull('servico_id').select('*').first()

    if (validateUse) {
      return DataResponse.response(
        null,
        201,
        "Esse contador se encontra em uso",
        null
      );
    } else if (res > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um contador com o mesmo Número de série",
        null
      );
    } else {

      // update with new data entered
      const insertInto = await Contadore.find(params.id);
      insertInto.merge(data);
      await insertInto.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        null
      );

    }
  }

  async destroy({ params, request, response }) {
  }
}

module.exports = ContadoreController
