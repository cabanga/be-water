'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const RotaHeader = use("App/Models/RotaHeader");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')

/**
 * Resourceful controller for interacting with rotaheaders
 */
class RotaHeaderController {
  /**
   * Show a list of all rotaheaders.
   * GET rotaheaders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }


  /**
   * Render a form to be used for creating a list rotaheaders.
   * GET rotaheaders/listagem
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "ro.id",
        "ro.descricao",
        "ro.data_inicio",
        "ro.data_fim",
        "ro.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "ro.leitor_id",
        "ro.estado",
        "use.nome as userNome",
        "ro.leitor_id"
      )
        .from("rota_headers as ro")
        .innerJoin("users as use", "use.id", "ro.leitor_id")
        .leftJoin("municipios", "municipios.id", "ro.municipio_id")
        .leftJoin("provincias", "provincias.id", "municipios.provincia_id")

        .orderBy(orderBy == null ? "ro.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "ro.id",
        "ro.descricao",
        "ro.data_inicio",
        "ro.data_fim",
        "ro.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "ro.leitor_id",
        "ro.estado",
        "use.nome as userNome",
        "ro.leitor_id"
      )
        .from("rota_headers as ro")
        .innerJoin("users as use", "use.id", "ro.leitor_id")
        .leftJoin("municipios", "municipios.id", "ro.municipio_id")
        .leftJoin("provincias", "provincias.id", "municipios.provincia_id")

        .where("ro.descricao", "like", "%" + search + "%")
        /* .orWhere("ro.periodo", "like", "%" + search + "%") */
        /* .orWhere(
          Database.raw('DATE_FORMAT(ro.data, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        ) */
        .orderBy(orderBy == null ? "ro.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }


  /**
   * Render a form to be used for creating a new rotaheader.
   * GET rotaheaders/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }


  async getRotasHeaderByLeitor({ params }) {

    let res = null;

    res = await await Database.select(
      "ro.id",
      "ro.descricao as nome",
      "ro.data_inicio",
      "ro.data_fim",
      "ro.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "ro.estado as estado_id",
    )
      .from("rota_headers as ro")
      .leftJoin("municipios", "municipios.id", "ro.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")

      .where("ro.leitor_id", params.id)
      .orderBy("ro.descricao", "ASC");

    return DataResponse.response("success", 200, "", res);
  }

  async getRotasHeaderWithRunsByLeitor({ params }) {

    const estado = await Database.select('id')
      .table('estado_rotas')
      .where('slug', 'PENDENTE')
      .first();

    const res = await Database.select(
      "rota_headers.id",
      "rota_headers.descricao as nome",
      "rota_headers.data_inicio",
      "rota_headers.data_fim",
      "ro.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
    )
      .table('rota_headers')
      .innerJoin('rota_runs', 'rota_runs.rota_header_id', 'rota_headers.id')
      .leftJoin("municipios", "municipios.id", "ro.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")

      .where("rota_headers.leitor_id", params.id)
      .where("rota_runs.estado_rota_id", estado.id)

      .orderBy("rota_headers.created_at", "DESC")
      .groupBy('rota_headers.id')

    return DataResponse.response("success", 200, "", res);
  }

  async getRotasHeaderWithRuns({ params }) {

    const estado = await Database.select('id')
      .table('estado_rotas')
      .where('slug', 'PENDENTE')
      .first();

    const res = await Database.select(
      "rota_headers.id",
      "rota_headers.descricao as nome",
      "rota_headers.data_inicio",
      "rota_headers.data_fim",
      "rota_headers.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
    )
      .table('rota_headers')
      .innerJoin('rota_runs', 'rota_runs.rota_header_id', 'rota_headers.id')
      .leftJoin("municipios", "municipios.id", "rota_headers.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")

      .where("rota_runs.estado_rota_id", estado.id)

      .orderBy("rota_headers.created_at", "DESC")
      .groupBy('rota_headers.id')

    return DataResponse.response("success", 200, "", res);
  }


  /**
   * Create/save a new rotaheader.
   * POST rotaheaders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async store({ request, auth }) {
    const {
      descricao,
      data_inicio,
      data_fim,
      municipio_id,
      leitor_id,
      estado
    } = request.all();

    await RotaHeader.create({
      descricao: descricao,
      data_inicio: data_inicio,
      data_fim: data_fim,
      municipio_id: municipio_id,
      leitor_id: leitor_id,
      estado: estado
    });
    return DataResponse.response(
      "success",
      200,
      "Registado com sucesso",
      null
    );

  }

  async update({ params, request }) {
    const data = request.only([ "descricao", "data_inicio", "data_fim", "municipio_id", "leitor_id", "estado" ])

      const rota_header = await RotaHeader.find(params.id);
      rota_header.merge(data);
      await rota_header.save();
      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
  }

  /**
   * Display a single rotaheader.
   * GET rotaheaders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const rota_header = await RotaHeader.find(params.id)
    await rota_header.load('municipio')
    return DataResponse.response( "success", 200, "", rota_header )
  }

  /**
   * Render a form to update an existing rotaheader.
   * GET rotaheaders/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update rotaheader details.
   * PUT or PATCH rotaheaders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */


  /**
   * Delete a rotaheader with id.
   * DELETE rotaheaders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }






}

module.exports = RotaHeaderController
