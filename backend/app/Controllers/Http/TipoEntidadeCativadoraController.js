'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

 
const TipoEntidadeCativadora = use("App/Models/TipoEntidadeCativadora");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with tipoentidadecativadoras
 */
class TipoEntidadeCativadoraController {
  /**
   * Show a list of all tipoentidadecativadoras.
   * GET tipoentidadecativadoras
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new tipoentidadecativadora.
   * GET tipoentidadecativadoras/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new tipoentidadecativadora.
   * POST tipoentidadecativadoras
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { nome } = request.all();

    const pedido = await TipoEntidadeCativadora.create({
      nome: nome,
      user_id: auth.user.id
    });


    return DataResponse.response("success", 200, "Tipo Entidade registada com sucesso", pedido);

  }

  /**
   * Display a single tipoentidadecativadora.
   * GET tipoentidadecativadoras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing tipoentidadecativadora.
   * GET tipoentidadecativadoras/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update tipoentidadecativadora details.
   * PUT or PATCH tipoentidadecativadoras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a tipoentidadecativadora with id.
   * DELETE tipoentidadecativadoras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async tipoEntidadeCativadoraSelectBox({ request, view, response, auth }){
    const tipos = await TipoEntidadeCativadora.all();
    return DataResponse.response("success", 200, "", tipos);
  }
}

module.exports = TipoEntidadeCativadoraController
