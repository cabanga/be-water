'use strict'

const { verify } = require('crypto');

const DataResponse = use("App/Models/DataResponse");
const ClasseTarifario = use("App/Models/ClasseTarifario");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')
const breaks = use('App/service/breaks')

class ClasseTarifarioController {

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    res = await Database.select(
      "classe_tarifarios.id",
      "classe_tarifarios.tarifario_id",
      "classe_tarifarios.valor",
      "classe_tarifarios.tarifa_variavel",
      "classe_tarifarios.tarifa_fixa_mensal",
      "classe_tarifarios.tarifa_intervalo	",
      "classe_tarifarios.consumo_minimo",
      "classe_tarifarios.consumo_maximo",
      "classe_tarifarios.descricao",
      "tarifarios.descricao  as tarifario",
      "produtos.nome as produto"
    )
      .from("classe_tarifarios")
      .leftJoin("tarifarios", "tarifarios.id", "classe_tarifarios.tarifario_id")
      .leftJoin("produtos", "produtos.id", "classe_tarifarios.produto_id")

      .where(function () {
        if (search != null) {
          this.where("tarifarios.descricao", "like", "%" + search + "%")
        }
      })

      .orderBy(orderBy == null ? "classe_tarifarios.created_at" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);


    return DataResponse.response("success", 200, "", res);
  }

  async selectBox() {
    const res = await Database.select('id', 'descricao')
      .from('tarifarios')
      .orderBy('tarifarios.descricao', 'ASC')

    return DataResponse.response("sucess", 200, null, res)
  }

  async store({ request }) {

    const breakItme = new breaks()

    const { tarifario_id, valor, consumo_minimo, consumo_maximo, produto_id, descricao } = request.all();


    const valideProduto = await Database.select('*').from('classe_tarifarios')
      .where("tarifario_id", tarifario_id)
      .where("produto_id", produto_id)
      .getCount();

    if (valideProduto) {
      return DataResponse.response(
        "error",
        201,
        "Esse produto já se encontra adicionado nesse tarifário",
        null,
      );

    }

    const getTotalConsumo = await Database.select('*').from('classe_tarifarios')
      .where("tarifario_id", tarifario_id)
      .getCount();

    const getConsumo = await Database.select('*').from('classe_tarifarios')
      .where("tarifario_id", tarifario_id)

    let count = 0

    if (getTotalConsumo == 0) {
      const tarifario = await ClasseTarifario.create({
        tarifario_id: tarifario_id,
        valor: valor,
        descricao: descricao,
        produto_id: produto_id,
        consumo_minimo: consumo_minimo,
        consumo_maximo: consumo_maximo
      });

      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        tarifario,
      );

    } else {

      for (const value of getConsumo) {

        count += 1

        if (
          (consumo_minimo > value.consumo_minimo && consumo_minimo < value.consumo_maximo)
          || (consumo_maximo > value.consumo_minimo && consumo_maximo < value.consumo_maximo)
        ) {

          return DataResponse.response(
            "error",
            201,
            "Neste Tarifário já existe Produto com este consumo mínimo ou  máximo  neste intervalo",
            null,
            breakItme.breaks()
          );

        } else {
          if ((value.consumo_minimo > consumo_minimo && value.consumo_minimo < consumo_maximo)
            || (value.consumo_maximo > consumo_minimo && value.consumo_maximo < consumo_maximo)) {
            return DataResponse.response(
              "error",
              201,
              "Neste Tarifário já existe Produto com este consumo mínimo ou  máximo  neste intervalo",
              null,
              breakItme.breaks()
            );

          } else {
            if ((consumo_maximo != value.consumo_minimo && consumo_maximo != value.consumo_maximo)
              && (consumo_maximo > 0 && consumo_maximo > 0)
              && (consumo_maximo != 0 && consumo_maximo != 0)
              && (consumo_maximo != null && consumo_maximo != null)
            ) {
              if (getTotalConsumo == count) {

                const tarifario = await ClasseTarifario.create({
                  tarifario_id: tarifario_id,
                  valor: valor,
                  descricao: descricao,
                  produto_id: produto_id,
                  consumo_minimo: consumo_minimo,
                  consumo_maximo: consumo_maximo
                });
                return DataResponse.response(
                  "success",
                  200,
                  "Registo efectuado com sucesso",
                  tarifario,
                  breakItme.breaks()
                );
              }

            } else {
              return DataResponse.response(
                "error",
                201,
                "Neste Tarifário já existe Produto com este consumo mínimo ou  máximo  neste intervalo",
                null,
                breakItme.breaks()
              );

            }
          }
        }
      }
    }
  }

  async classeTartifarioByTarifario({ params }) {

    const res = await Database.select(
      'id',
      'tarifario_id',
      'produto_id',
      'descricao',
      'valor',
      'ordem',
      'tarifa_variavel',
      'tarifa_fixa_mensal',
      'tarifa_intervalo',
      'consumo_minimo',
      'consumo_maximo',
      'created_at',
    )
      .from('classe_tarifarios')
      .where('tarifario_id', params.id)

    return DataResponse.response(
      "success",
      200,
      null,
      res
    );
  }

  async getProdutoClasseTarifario({ request, auth }) {
    const {
      conta_id,
      classe_tarifario_id,
      preco,
      flag
    } = request.all();

    const servico = await Database.select(
      "produtos.id as servico_id",
      "produtos.nome as servico",
      "produtos.valor as servico_valor",
      "produtos.is_active",
      "produtos.imposto_id",
      "impostos.descricao as imposto",
      "impostos.codigo as imposto_codigo",
      "impostos.valor as imposto_valor",
      "produtos.is_editable",
      "produtos.tipo_produto_id",
      "tipo_produtos.descricao as tipo_produto",
      "produtos.incidencia_id",
      "incidencias.nome as incidencia"
    ).from("classe_tarifarios")
      .innerJoin("produtos", "classe_tarifarios.produto_id", "produtos.id")
      .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
      .leftJoin("tipo_produtos", "produtos.tipo_produto_id", "tipo_produtos.id")
      .leftJoin("incidencias", "produtos.incidencia_id", "incidencias.id")
      .where("classe_tarifarios.id", classe_tarifario_id)
      .where("produtos.is_active", true).first();


    if (servico == null) return DataResponse.response("error", 404, "", null);

    if (flag) servico.servico_valor = preco;


    console.log(servico);

    const linha_factura = await Database.select('*')
      .from('linha_facturas')
      .innerJoin('facturas', 'facturas.conta_id', conta_id)
      .where('linha_facturas.servico_id', servico.servico_id).first();

    console.log(linha_factura);

    return DataResponse.response("success", 200, "", {
      servico_id: servico.servico_id,
      servico: servico.servico,
      servico_valor: servico.servico_valor,
      imposto_id: servico.imposto_id,
      imposto: servico.imposto,
      imposto_codigo: servico.imposto_codigo,
      imposto_valor: parseFloat(parseFloat(servico.imposto_valor / 100).toFixed(2)),
      is_editable: servico.is_editable,
      facturado: (linha_factura != null) ? true : false,
      tipo_produto_id: servico.tipo_produto_id,
      tipo_produto: servico.tipo_produto,
      incidencia_id: servico.incidencia_id,
      incidencia: servico.incidencia
    });

  }
}

module.exports = ClasseTarifarioController
