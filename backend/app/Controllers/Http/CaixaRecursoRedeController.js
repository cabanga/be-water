'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Caixa = use('App/Models/CaixaRecursoRede');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database'); 
/**
 * Resourceful controller for interacting with caixarecursoredes
 */
class CaixaRecursoRedeController {
  /**
   * Show a list of all caixarecursoredes.
   * GET caixarecursoredes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new caixarecursorede.
   * GET caixarecursoredes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new caixarecursorede.
   * POST caixarecursoredes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { descricao, central_id } = request.all(); 
 
      const res = await Caixa.create({
        descricao: descricao,
        central_id: central_id,
        user_id: auth.user.id
      });
      return DataResponse.response("success", 200, "Registo efectuado com sucesso", res);
    
  }

  /**
   * Display a single caixarecursorede.
   * GET caixarecursoredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing caixarecursorede.
   * GET caixarecursoredes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update caixarecursorede details.
   * PUT or PATCH caixarecursoredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a caixarecursorede with id.
   * DELETE caixarecursoredes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = CaixaRecursoRedeController
