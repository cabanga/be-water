'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const User = use("App/Models/User");
const Cambio = use("App/Models/Cambio");
const Moeda = use("App/Models/Moeda");
const DataResponse = use("App/Models/DataResponse");
/**
 * Resourceful controller for interacting with cambios
 */
class CambioController {
  /**
   * Show a list of all cambios.
   * GET cambios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ params }) {  

    const cambios = await Moeda.query()
      .where("id", params.id) 
      .with("cambios.user")
      .with("cambios.moeda") 
      .fetch();   

    return DataResponse.response("success", 2000, null, cambios);
  }
 

  /**
   * Create/save a new cambio.
   * POST cambios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) { 

      const { valor_cambio, data_cambio, moeda_id, is_active } = request.all();
      const moeda = await Moeda.find(moeda_id); 
      
      if(is_active == true){
          await moeda.cambios().where('is_active',true).update({ is_active: false });
      }      

      const cambio = await moeda.cambios().create({
          valor_cambio: valor_cambio,
          data_cambio: data_cambio, 
          is_active: is_active,
          user_id: auth.user.id
      });
     
    return DataResponse.response("success", 200, "Cambio registado com sucesso.", cambio);      
  }
 

  /**
   * Render a form to update an existing cambio.
   * GET cambios/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update cambio details.
   * PUT or PATCH cambios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a cambio with id.
   * DELETE cambios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = CambioController
