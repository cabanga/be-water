'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const {
  validate
} = use('Validator')

const Empresa = use('App/Models/Empresa');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');


/**
 * Resourceful controller for interacting with empresas
 */
class EmpresaController {
  /**
   * Show a list of all empresas.
   * GET empresas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { start, end, search, order } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select('*').from('empresas').orderBy(order, 'asc').paginate(start, end);
    } else {
      res = await Database.select('*').from('empresas')
        .where('companyName', 'like', '%' + search + "%")
        .orWhere('telefone', 'like', '%' + search + "%")
        .orWhere('email', 'like', '%' + search + "%")
        .orWhere('taxRegistrationNumber', 'like', '%' + search + "%")
        .orderBy(order, 'asc').paginate(start, end);
    }

    return DataResponse.response("success", 200, "", res);
  }


  async userEmpresa({ auth }) {
    let res = null;
    res = await Database
    .select(
      'empresas.id',
      'empresas.companyName',
      'empresas.telefone',
      'empresas.addressDetail',
      'empresas.taxRegistrationNumber',
      'empresas.city',
      'empresas.province',
      'empresas.active_tfa',
      'empresas.logotipo',
      'empresas.email',
      'empresas.width',
      'empresas.height',
      'empresas.site'
    )
    .from('empresas')
    .innerJoin("users", "empresas.id", "users.empresa_id")
    .where("users.id", auth.user.id);
    return DataResponse.response("success", 200, "", res);

  }

  /**
   * Render a form to be used for creating a new empresa.
   * GET empresas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new empresa.
   * POST empresas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const data = request.only(['companyName', 'telefone', 'addressDetail', 'taxRegistrationNumber', 'city', 'province', 'active_tfa', 'site']);
    const empresaExiste = await Empresa.query().where('taxRegistrationNumber', data.taxRegistrationNumber).getCount()
    if (empresaExiste > 0) {
      return DataResponse.response("error", 500, "Já existe uma empresa com esse Nº Registro Fiscal.", null);
    } else {
      const empresa = await Empresa.create(data);
      return DataResponse.response("success", 200, "Empresa registado com sucesso.", empresa);
    }

  }

  /**
   * Display a single empresa.
   * GET empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing empresa.
   * GET empresas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update empresa details.
   * PUT or PATCH empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {

    const data = request.only(['companyName', 'telefone', 'addressDetail', 'taxRegistrationNumber', 'city', 'province', 'active_tfa', 'email', 'logotipo', 'site']);
    /*
  const validation = await validate(data, Empresa.rules);
    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages(), data);
    }
  */
    // update with new data entered
    const empresa = await Empresa.find(params.id);
    empresa.merge(data);
    await empresa.save();

    return DataResponse.response("success", 200, "Dados actualizados com sucesso", empresa);
  }
  async upload({ params, request, response }) {

    const data = request.only(['logotipo','width','height']);
    const empresa = await Empresa.find(params.id);
    empresa.merge(data);

    await empresa.save();

    return DataResponse.response("success", 200, "Logotipo actualizados com sucesso", empresa);
  }

  /**
   * Delete a empresa with id.
   * DELETE empresas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  /**
   * selectOptionEmpresas a list of all Roles.
   * GET Roles
   *
   * @param {object} ctx
   */
  async selectOptionEmpresas() {
    const res = await Database.select('*').from('empresas');
    return DataResponse.response("success", 200, "", res);
  }

  async getEmpresa() {
    return await Database.select('*').from('empresas');

  }
}

module.exports = EmpresaController
