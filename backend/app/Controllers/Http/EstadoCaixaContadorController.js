'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const EstadoCaixaContador = use("App/Models/EstadoCaixaContador");
const Database = use("Database");
var moment = require("moment");

/**
 * Resourceful controller for interacting with estadocaixacontadors
 */
class EstadoCaixaContadorController {
  /**
   * Show a list of all estadocaixacontadors.
   * GET estadocaixacontadors
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new estadocaixacontador.
   * GET estadocaixacontadors/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new estadocaixacontador.
   * POST estadocaixacontadors
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single estadocaixacontador.
   * GET estadocaixacontadors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing estadocaixacontador.
   * GET estadocaixacontadors/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update estadocaixacontador details.
   * PUT or PATCH estadocaixacontadors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a estadocaixacontador with id.
   * DELETE estadocaixacontadors/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async selectBox ({ request }) {  
    const res = await Database.select('id', 'nome', 'slug')
    .from('estado_caixa_contadors')
    .orderBy('nome', 'ASC');
     
    return res;
  }
}

module.exports = EstadoCaixaContadorController
