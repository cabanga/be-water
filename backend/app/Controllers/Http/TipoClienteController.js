'use strict'
const TipoClienteRepository = use('App/Repositories/TipoClienteRepository')
const Database = use("Database");

class TipoClienteController {
    #_tipoClienteRepo

    constructor() {
        this.#_tipoClienteRepo = new TipoClienteRepository()
    }

    async index({ response, request }) {
        const page = request.input('page')
        const search = request.input('search')
        const res = await this.#_tipoClienteRepo.getAll(page, search)
        return response.ok(res)
    }

    async store({ request }) {
        const data = request.only(['tipoClienteDesc'])
        const res = await this.#_tipoClienteRepo.create( data )
        return res
    }

    async show({params, response}){
        const gender = await this.#_tipoClienteRepo.findById(params.id)
        return response.ok(gender)
    }

    async update({ params, request }){
        const data = request.only(['tipoClienteDesc'])
        const res = await this.#_tipoClienteRepo.update( params.id, data )
        return res
    }

    async delete({ params }) {
        const res = await this.#_tipoClienteRepo.delete( params.id )
        return res
    }

    async selectBox({ params }) {
        let res = await Database.select(
            "tipo_clientes.id",
            "tipo_clientes.tipoClienteDesc as nome"
        )
        .from("tipo_clientes")
        .orderBy("tipo_clientes.tipoClienteDesc", "ASC")
        return res
    }

}

module.exports = TipoClienteController
