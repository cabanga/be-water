'use strict'
const DataResponse = use("App/Models/DataResponse");
const Fabricante = use("App/Models/Fabricante");
const Database = use("Database");

class FabricanteController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("fabricantes")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("fabricantes")
        .orWhere("fabricantes.slug", "like", "%" + search + "%")
        .orWhere("fabricantes.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    const Verify = await Fabricante.query()
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um semelhante",
        Verify
      );

    } else {
      const fabricante = await Fabricante.create({
        descricao: descricao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const Verify = await Fabricante.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (Verify > 0) {
    return DataResponse.response(
      null,
      201,
      "Já existe um semelhante",
      Verify
    );
  }else{

    // update with new data entered
    const fabricantes = await Fabricante.find(params.id);
    fabricantes.merge(data);
    await fabricantes.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      fabricantes
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('fabricantes')
    .orderBy('descricao', 'ASC');
     
    return res;
  }
}

module.exports = FabricanteController
