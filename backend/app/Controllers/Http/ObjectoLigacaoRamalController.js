'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const ObjectoLigacaoRamal = use("App/Models/ObjectoLigacaoRamal");
const Database = use("Database");
/**
 * Resourceful controller for interacting with objectoligacaoramals
 */
class ObjectoLigacaoRamalController {
  /**
   * Show a list of all objectoligacaoramals.
   * GET objectoligacaoramals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "objecto_ligacao_ramals.id",
        "objecto_ligacao_ramals.descricao",
        "objecto_ligacao_ramals.latitude",
        "objecto_ligacao_ramals.longitude",
        "tipo_objectos.descricao as tipo_objecto_tecnico",
        "tipo_objectos.id as tipo_objecto_id"
      )
        .from("objecto_ligacao_ramals")
        .leftJoin("tipo_objectos", "objecto_ligacao_ramals.tipo_objecto_id", "tipo_objectos.id")
        .orderBy(orderBy == null ? "objecto_ligacao_ramals.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "objecto_ligacao_ramals.id",
        "objecto_ligacao_ramals.descricao",
        "objecto_ligacao_ramals.latitude",
        "objecto_ligacao_ramals.longitude",
        "tipo_objectos.descricao as tipo_objecto_tecnico",
        "tipo_objectos.id as tipo_objecto_id"
      )
        .from("objecto_ligacao_ramals")
        .leftJoin("tipo_objectos", "objecto_ligacao_ramals.tipo_objecto_id", "tipo_objectos.id")
        .where("objecto_ligacao_ramals.descricao", "like", "%" + search + "%")
        .orWhere("objecto_ligacao_ramals.latitude", "like", "%" + search + "%")
        .orWhere("objecto_ligacao_ramals.longitude", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "objecto_ligacao_ramals.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new objectoligacaoramal.
   * GET objectoligacaoramals/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new objectoligacaoramal.
   * POST objectoligacaoramals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { descricao, latitude, longitude, tipo_objecto_id } = request.all();

    const Verify = await ObjectoLigacaoRamal.query()
      .where("latitude", latitude)
      .where("longitude", longitude)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um objecto de ligação na mesma localicação",
        Verify
      );

    } else {
      const res = await ObjectoLigacaoRamal.create({
        descricao: descricao,
        latitude: latitude,
        longitude: longitude,
        tipo_objecto_id: tipo_objecto_id,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  /**
   * Display a single objectoligacaoramal.
   * GET objectoligacaoramals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing objectoligacaoramal.
   * GET objectoligacaoramals/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update objectoligacaoramal details.
   * PUT or PATCH objectoligacaoramals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["descricao", "latitude", "longitude", "tipo_objecto_id"]);

    const Verify = await ObjectoLigacaoRamal.query()
      .where("latitude", data.latitude)
      .where("longitude", data.longitude)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify) {
      return DataResponse.response(
        null,
        201,
        "Já existe um objecto de ligação na mesma localicação",
        Verify
      );
    } else {

      // update with new data entered
      const res = await ObjectoLigacaoRamal.find(params.id);
      res.merge(data);
      await res.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        res
      );
    }
  }

  async selectBoxByTipoLigacao({ params }) {


    let res = await Database
      .select(
        "id",
        "descricao",
        "tipo_objecto_id"
      )
      .from('objecto_ligacao_ramals')
      .where('tipo_objecto_id', params.id)
      .orderBy("descricao", "ASC");

    return res;
  }

  async selectBoxSearchPontoA({ request }) {

    const { start, end, search, ponto_b_id } = request.all();

    let res = null;

    if (search == null) {

      res = await Database
        .select(
          "id",
          "descricao",
          "latitude",
          "longitude"
        )
        .from('objecto_ligacao_ramals')
        .whereNot('id', ponto_b_id)
        .paginate(start, end);
    } else {
      res = await Database
        .select(
          "id",
          "descricao",
          "latitude",
          "longitude"
        )
        .from('objecto_ligacao_ramals')
        .whereNot('id', ponto_b_id)
        .where("descricao", "like", "%" + search + "%")
        .orWhere("latitude", "like", "%" + search + "%")
        .orWhere("longitude", "like", "%" + search + "%")
        .paginate(start, end);
    }

    //console.log(residencias)

    return DataResponse.response("success", 200, null, res);
  }

  async searchInicioLigacao({ request }) {

    const { start, end, search, tipo_objecto_id } = request.all();

    let res = null;

    if (search == null) {

      res = await Database
        .select(
          "id",
          "descricao",
          "latitude",
          "longitude"
        )
        .from('objecto_ligacao_ramals')
        .where('tipo_objecto_id', tipo_objecto_id)
        .paginate(start, end);
    } else {
      res = await Database
        .select(
          "id",
          "descricao",
          "latitude",
          "longitude"
        )
        .from('objecto_ligacao_ramals')
        .where('tipo_objecto_id', tipo_objecto_id)
        .where("descricao", "like", "%" + search + "%")
        .paginate(start, end);
    }

    //console.log(res)

    return DataResponse.response("success", 200, null, res);
  }


  async searchFimLigacao({ request }) {

    const { start, end, search, tipo_objecto_id } = request.all();

    let res = null;

    if (search == null) {

      res = await Database
        .select(
          "id",
          "descricao",
          "latitude",
          "longitude"
        )
        .from('objecto_ligacao_ramals')
        .where('tipo_objecto_id', tipo_objecto_id)
        .whereNotIn('id', Database.select("ponto_b_id").from("ligacao_ramals").whereNotNull("ponto_b_id"))
        .paginate(start, end);
    } else {
      res = await Database
        .select(
          "id",
          "descricao",
          "latitude",
          "longitude"
        )
        .from('objecto_ligacao_ramals')
        .where('tipo_objecto_id', tipo_objecto_id)
        .whereNotIn('id', Database.select("ponto_b_id").from("ligacao_ramals").whereNotNull("ponto_b_id"))
        .where("descricao", "like", "%" + search + "%")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, null, res);
  }

  async selectBoxSearchPontoB({ request }) {

    const { start, end, search, ponto_a_id } = request.all();

    let res = null;

    if (search == null) {

      res = await Database
        .select(
          "id",
          "descricao",
          "latitude",
          "longitude"
        )
        .from('objecto_ligacao_ramals')
        .whereNot('id', ponto_a_id)
        .paginate(start, end);
    } else {
      res = await Database
        .select(
          "id",
          "descricao",
          "latitude",
          "longitude"
        )
        .from('objecto_ligacao_ramals')
        .whereNot('id', ponto_a_id)
        .where("descricao", "like", "%" + search + "%")
        .paginate(start, end);
    }

    //console.log(residencias)

    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Delete a objectoligacaoramal with id.
   * DELETE objectoligacaoramals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = ObjectoLigacaoRamalController
