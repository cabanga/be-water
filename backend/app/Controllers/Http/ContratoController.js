'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Contrato = use("App/Models/Contrato");
const LogEstadoContrato = use("App/Models/LogEstadoContrato");
const TipoContrato = use("App/Models/TipoContrato");
const Leitura = use("App/Models/Leitura");
const MoradaCorrespondencia = use("App/Models/MoradaCorrespondencia");
const Contador = use("App/Models/Contadore");
const RescisaoContrato = use("App/Models/RescisaoContrato");


const LocalConsumo = use("App/Models/LocalConsumo");
const LocalInstalacaoHistorico = use("App/Models/LocalInstalacaoHistorico");

const ContratoRepository = use('App/Repositories/ContratoRepository')
const LocalInstalacaoRepository = use('App/Repositories/LocalInstalacaoRepository')

const Pedido = use("App/Models/Pedido");
const Servico = use("App/Models/Servico");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");
/**
 * Resourceful controller for interacting with contratoes
 */
class ContratoController {


  #_contratoRepository
  #_localInstalacaoRepository

  constructor() {
    this.#_contratoRepository = new ContratoRepository();
    this.#_localInstalacaoRepository = new LocalInstalacaoRepository();
  }


  async index({ request, response }) {

    const contratos = await TipoContrato.query()
      .orderBy('descricao', 'asc')
      .fetch()

    return { data: contratos }
  }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    return await this.#_contratoRepository.getAll(search, orderBy, pagination)
  }

  async getContratoById({ params }) {
    return await this.#_contratoRepository.getContratoById(params.id);
  }

  async show({ params }){
    const contrato =  await this.#_contratoRepository.getContratoById(params.id);
    return DataResponse.response( "success",200,null, contrato);
  }
  async imprimirPDFContrato({ params }) {
    return await this.#_contratoRepository.imprimirContrato(params.id);
  }


  async create({ request, response, view }) {
  }

  async store({ request, auth, response }) {

    const data = request.only([
      "cliente_id",
      "tipo_contracto_id",
      "tipo_medicao_id",
      "tipo_facturacao_id",
      "tipologia_cliente_id",
      "nivel_sensibilidade_id",
      "objecto_contrato_id",
      "motivo_recisao_id",
      "data_inicio",
      "data_fim",
      "morada_correspondencia_flag",
      "numero_habitantes",
      'numero_utilizadores',
      "tipo_mensagem_id",
      "mensagem",
      "morada_contrato",
      "tarifario_id",
      "classe_tarifario_id",
      "local_consumo_id",
      "instalacao_sanitaria_qtd",
      "reservatorio_flag",
      "reservatorio_capacidade",
      "piscina_flag",
      "piscina_capacidade",
      "jardim_flag",
      "campo_jardim_id",
      "poco_alternativo_flag",
      "fossa_flag",
      "fossa_capacidade",
      "acesso_camiao_flag",
      "anexo_flag",
      "anexo_quantidade",
      "caixa_contador_flag",
      "abastecimento_cil_id"
    ]);

    //let servicos_array = [];


    /*
  const getPedidoResidencia = await Database.select("*")
    .table("pedidos")
    .where("local_instalacao_id", data.local_consumo_id).first();

  if (getPedidoResidencia) {
    return DataResponse.response( "success", 201, "Já existe uma Ordem de servico para este Local de Instalação", null
    );
  }
  */

    let conta = await Database.select("*")
      .table("contas")
      .where("cliente_id", data.cliente_id)
      .first()

    const contrato = await Contrato.create({

      cliente_id: data.cliente_id,
      conta_id: conta.id,
      tipo_contracto_id: data.tipo_contracto_id,
      tipo_medicao_id: data.tipo_medicao_id,
      tipo_facturacao_id: data.tipo_facturacao_id,
      tipologia_cliente_id: data.tipologia_cliente_id,
      nivel_sensibilidade_id: data.nivel_sensibilidade_id,
      objecto_contrato_id: data.objecto_contrato_id,
      numero_habitantes: data.numero_habitantes,
      numero_utilizadores: data.numero_utilizadores,
      data_inicio: data.data_inicio,
      data_fim: data.data_fim,
      estado_contrato_id: 1, // estado activo
      morada_correspondencia_flag: data.morada_correspondencia_flag,
      motivo_recisao_id: data.motivo_recisao_id,

      morada_contrato: data.morada_contrato,
      tarifario_id: data.tarifario_id,
      classe_tarifario_id: data.classe_tarifario_id,
      user_id: auth.user.id

    });

    await LogEstadoContrato.create({
      contrato_id: contrato.id,
      id_estado_anterior: 1,
      id_estado_actual: 1,
      data_operacao: data.data_inicio,
      user_id: auth.user.id
    });

/*
    for (var i = 0; i < data.servicos.length; i++) {

      const servico = await Servico.create({
        contrato_id: contrato.id,
        produto_id: data.servicos[i].produto_id,
        valorBase: data.servicos[i].valor,
        imposto: data.servicos[i].imposto_id,
        estado: 1,
        user_id: auth.user.id
      });

      servicos_array.push(data.servicos[i]);

    }
*/

    let local_consumo = null;
    let pedido = null;
    //console.log(contrato);
    if (data.local_consumo_id != null) {

      pedido = await Pedido.pedidoStore(
        data.conta_id,
        contrato.id,
        data.tarifario_id,
        data.local_consumo_id,
        auth.user.id
      );


      const local_instalacao_historico = await this.#_localInstalacaoRepository.getLocalInstalacaoById(data.local_consumo_id);

      local_consumo = await LocalConsumo.create({

        conta_id: data.conta_id,
        contrato_id: contrato.id,
        local_instalacao_id: data.local_consumo_id,
        instalacao_sanitaria_qtd: data.instalacao_sanitaria_qtd,
        reservatorio_flag: data.reservatorio_flag,
        reservatorio_capacidade: data.reservatorio_capacidade,
        piscina_flag: data.piscina_flag,
        piscina_capacidade: data.piscina_capacidade,
        jardim_flag: data.jardim_flag,
        campo_jardim_id: data.campo_jardim_id,
        poco_alternativo_flag: data.poco_alternativo_flag,
        fossa_flag: data.fossa_flag,
        fossa_capacidade: data.fossa_capacidade,
        acesso_camiao_flag: data.acesso_camiao_flag,
        anexo_flag: data.anexo_flag,
        anexo_quantidade: data.anexo_quantidade,
        caixa_contador_flag: data.caixa_contador_flag,
        abastecimento_cil_id: data.abastecimento_cil_id,
        is_active: false,
        user_id: auth.user.id

      });

      if (this.#_localInstalacaoRepository.validateLocalInstalacaoChanges(local_instalacao_historico, data)) {

        const actualizacao = await this.#_localInstalacaoRepository.setLocalInstalacaoChanges(data)

        //console.log("--> update");
        //console.log(actualizacao);

        await LocalInstalacaoHistorico.updateFromContrato({
          local_instalacao_id: data.local_consumo_id,
          historico: local_instalacao_historico,
          actualizacao: actualizacao,
          user_id: auth.user.id
        });

      }
    }

    let dados = {
      contrato: await this.#_contratoRepository.getContratoById(contrato.id),
      pedido: pedido,
      local_consumo: local_consumo,
      //servicos: servicos_array
    };

    return DataResponse.response("success", 200, "Processo finalizado com sucesso", dados);

  }


  async rescindirContrato({ params, request, auth }) {

    var data_actual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    let predio_finded = null;

    let predio_id = null;
    let edificio = null; 

    let leitura_new = null;
    let morada_correspondencia = null;

    const {
      contrato_id,
      morada_correspondencia_flag,
      tipo_medicao_slug,
      contador_id,
      motivo_recisao_id,
      estado_rescisao_id,
      data_rescisao,

      rua_id,
      numero_moradia,
      is_predio,
      predio_nome,
      predio_andar,

      leitura,
      data_leitura,
      leitura_origem,
 
    } = request.all();
  
    const estado_contrato = await Database.select("*")
      .table("estado_contratoes")
      .where("id", estado_rescisao_id).first();

      //console.log(estado_contrato)

    const contrato = await Database.select("*")
      .table("contratoes")
      .where("id", contrato_id)
      .where("estado_contrato_id", estado_contrato.id).first();


    if (contrato) {
      return DataResponse.response(
        "success",
        201,
        "Este contrato já foi rescindido!",
        null
      );
    }
    else {

      await Contrato.query()
        .where('id', contrato_id)
        .update({
          'estado_contrato_id': estado_contrato.id,
          'updated_at': data_actual
        });

      await LogEstadoContrato.create({
        contrato_id: contrato_id,
        id_estado_anterior: (contrato == null) ? estado_contrato.id : contrato.estado_contrato_id,
        id_estado_actual: estado_contrato.id,
        data_operacao: data_rescisao,
        user_id: auth.user.id
      });

    }

    if (contador_id != null && tipo_medicao_slug == "LEITURA") {

      const contador = await Contador.find(contador_id)
      let consumo = Math.abs(Number(leitura) - Number(contador.ultima_leitura))

      if ((leitura != null && data_leitura != null) || leitura_origem != null) {
        leitura_new = await Leitura.create({
          'contador_id': contador_id,
          'leitura': leitura,
          'consumo': consumo,
          'ultima_leitura': contador.ultima_leitura,
          'data_leitura': data_leitura,
          'user_id': user_id
        });

        await Contador.query()
          .where('id', contador_id)
          .update({
            'ultima_leitura': leitura,
            'updated_at': data_actual
          });
      }

    }


    /*if (morada_correspondencia_flag) {

      const morada_correspondencia_count = await MoradaCorrespondencia.query()
        .where("numero_moradia", numero_moradia)
        .where("predio_nome", predio_nome)
        .where("predio_andar", predio_andar)
        .where("rua_id", rua_id)
        .getCount();

      if (morada_correspondencia_count <= 0) {

        if (is_predio) {
          predio_finded = await Database.select("*")
            .from("morada_correspondencias")
            .where("is_predio", true)
            .where("predio_nome", predio_nome.trim())
            .where("rua_id", rua_id)
            .first();

          edificio = predio_nome;

          if (predio_finded) {
            edificio = predio_finded.predio_nome;
            predio_id = predio_finded.predio_id;
          }
          else predio_id = this.uuidv4();

        }

        morada_correspondencia = await MoradaCorrespondencia.create({
          numero_moradia: numero_moradia,
          is_predio: is_predio,
          predio_id: predio_id,
          predio_andar: predio_andar,
          predio_nome: edificio,
          rua_id: rua_id,
          user_id:  auth.user.id
        });
      }
    }*/


    const rescisao_contrato = await RescisaoContrato.create({
      'contrato_id': contrato_id,
      'motivo_recisao_id': motivo_recisao_id,
      'leitura_id': (leitura_new == null) ? null : leitura_new.id,
      'leitura_origem': leitura_origem,
      'morada_correspondencia_id': (morada_correspondencia == null) ? null : morada_correspondencia.id,
      'data_rescisao': data_rescisao,
      'user_id':  auth.user.id
    });
 

    return DataResponse.response("success", 200, "Contrato rescindido com sucesso!", rescisao_contrato);

  }


  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }


  async dashboardEstadoContrato() {
    const countTemporario = await Database.select("contratoes.id")
      .from("contratoes")
      .innerJoin(
        "estado_contratoes",
        "estado_contratoes.id",
        "contratoes.estado_contrato_id"
      )
      .where("estado_contratoes.slug", "TEMPORARIO")
      .getCount();

    const countAtivo = await Database.select("contratoes.id")
      .from("contratoes")
      .innerJoin(
        "estado_contratoes",
        "estado_contratoes.id",
        "contratoes.estado_contrato_id"
      )
      .where("estado_contratoes.slug", "ACTIVO")
      .getCount();

    const countContrato = await Database.select("contratoes.id")
      .from("contratoes")
      .innerJoin(
        "estado_contratoes",
        "estado_contratoes.id",
        "contratoes.estado_contrato_id"
      )
      .where("estado_contratoes.slug", "CONTRATO")
      .getCount();

    const countInativo = await Database.select("contratoes.id")
      .from("contratoes")
      .innerJoin(
        "estado_contratoes",
        "estado_contratoes.id",
        "contratoes.estado_contrato_id"
      )
      .where("estado_contratoes.slug", "INACTIVO")
      .getCount();

    const countCancelado = await Database.select("contratoes.id")
      .from("contratoes")
      .innerJoin(
        "estado_contratoes",
        "estado_contratoes.id",
        "contratoes.estado_contrato_id"
      )
      .where("estado_contratoes.slug", "CANCELADO")
      .getCount();

    return DataResponse.response("success", 200, null, {
      countTemporario: countTemporario,
      countAtivo: countAtivo,
      countContrato: countContrato,
      countInativo: countInativo,
      countCancelado: countCancelado
    });
  }


  async contratoByConta({ params }) {

    return await this.#_contratoRepository.getContratoByConta(params.id);
  }

  async contratoByLocalConsumo({ request, params }) {

    const { search, orderBy, pagination } = request.all();

    let res = null;

    if (search == null) {
      res = await Database.select(
        'contratoes.created_at',
        'contratoes.data_inicio as data_inicio',
        'contratoes.data_fim as data_fim',
        'contratoes.estado_id as estado_id',
        'tarifarios.descricao as tarifario',
        'tipo_contratoes.descricao as tipoContratoDescricao'
      )
        .table('local_consumos')
        .innerJoin('contratoes', 'local_consumos.contrato_id', 'contratoes.id')
        .leftJoin('tarifarios', 'contratoes.tarifario_id', 'tarifarios.id')
        .leftJoin('tipo_contratoes', 'contratoes.tipo_contracto_id', 'tipo_contratoes.id')
        .where("local_consumos.local_instalacao", params.id)
        .whereNotNull("local_consumos.contador_id")
        .orderBy(orderBy == null ? 'local_consumos.created_at' : orderBy, 'DESC')


    } else {

      res = await Database.select(
        'contratoes.created_at',
        'contratoes.data_inicio as data_inicio',
        'contratoes.data_fim as data_fim',
        'contratoes.estado_id as estado_id',
        'tarifarios.descricao as tarifario',
        'tipo_contratoes.descricao as tipoContratoDescricao'
      )
        .table('local_consumos')
        .innerJoin('contratoes', 'local_consumos.contrato_id', 'contratoes.id')
        .leftJoin('tarifarios', 'contratoes.tarifario_id', 'tarifarios.id')
        .leftJoin('tipo_contratoes', 'contratoes.tipo_contracto_id', 'tipo_contratoes.id')
        .where("local_consumos.local_instalacao", params.id)
        .whereNotNull("local_consumos.contador_id")
        .orderBy(orderBy == null ? 'local_consumos.created_at' : orderBy, 'DESC')

    }

    return DataResponse.response("success", 200, "", res);

  }

  async selectBoxContratosConta({ params }) {
    const contratoes = await Database
      .select(
        'contratoes.*',
        'contas.contaDescricao',
        'tarifarios.descricao AS tarifario_descricao'
      )
      .from("contratoes")
      .leftJoin("contas", "contas.id", "contratoes.conta_id")
      .leftJoin("tarifarios", "tarifarios.id", "contratoes.tarifario_id")
      .where("contratoes.conta_id", params.conta_id);

    return DataResponse.response("success", 200, "", contratoes);
  }

  async actualizar_media_consumo({ request, params }) {
    try {
      const data = request.only(['contratos'])
      let contratos = data.contratos

      for (let contrato of contratos) {
        await Contrato.query().where('id', contrato.contrato_id).update({ media_consumo: contrato.media_consumo })
      }
      return DataResponse.response("success", 200, "Media de consumo actualizada com sucesso.", null)
    } catch (error) {
      console.log(error)
      return DataResponse.response("success", 200, "Erro ao actualizar Media de consumo.", error)
    }
  }

}

module.exports = ContratoController
