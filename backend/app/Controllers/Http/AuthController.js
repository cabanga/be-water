"use strict";

const Role = use("App/Models/Role");
const RoleUser = use("App/Models/RoleUser");
const User = use("App/Models/User");
const Database = use("Database");
const DataResponse = use("App/Models/DataResponse");
const Empresa = use("App/Models/Empresa");
const commons = use("App/Models/Commons");
const speakeasy = require("speakeasy");

class AuthController {
  async index({ request ,response }) {

    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "users.id",
        "users.nome",
        "users.telefone",
        "users.email",
        "users.username",
        "users.morada",
        "users.status",
        "users.created_at",
        "users.empresa_id",
        "users.loja_id",
        "role_user.role_id",
        "users.updated_at",
        "roles.name as role",
        "empresas.companyName",
        "lojas.nome as loja",
        "filials.nome as filial"
      )
        .from("users")
        .leftJoin("empresas", "empresas.id", "users.empresa_id")
        .leftJoin("role_user", "role_user.user_id", "users.id")
        .leftJoin("roles", "role_user.role_id", "roles.id")
        .leftJoin("lojas", "lojas.id", "users.loja_id")
        .leftJoin("filials", "filials.id", "lojas.filial_id")
        .orderBy("users.created_at", "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "users.id",
        "users.nome",
        "users.telefone",
        "users.email",
        "users.username",
        "users.morada",
        "users.status",
        "users.created_at",
        "users.empresa_id",
        "users.loja_id",
        "role_user.role_id",
        "users.updated_at",
        "roles.name as role",
        "empresas.companyName",
        "lojas.nome as loja",
        "filials.nome as filial"
      )
        .from("users")
        .leftJoin("empresas", "empresas.id", "users.empresa_id")
        .leftJoin("role_user", "role_user.user_id", "users.id")
        .leftJoin("roles", "role_user.role_id", "roles.id")
        .leftJoin("lojas", "lojas.id", "users.loja_id")
        .leftJoin("filials", "filials.id", "lojas.filial_id")
        .where("users.nome", "like", "%" + search + "%")
        .orWhere("users.morada", "like", "%" + search + "%")
        .orWhere("users.telefone", "like", "%" + search + "%")
        .orWhere("lojas.nome", "like", "%" + search + "%")
        .orWhere("filials.nome", "like", "%" + search + "%")
 
        .orderBy("users.created_at", "DESC")
        .paginate((pagination.page == null ? 1 : pagination.page), pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async register({ request }) {
    const data = request.only([
      "nome",
      "telefone",
      "username",
      "email",
      "password",
      "morada",
      "empresa_id",
      "role_id"
    ]);
    const user = await User.create(data);
    return DataResponse.response(
      "success",
      200,
      "Utilizador registado com sucesso",
      user
    );
  }

  async authenticate({ request, auth, response }) {
    let dado = { user: null, token: null, role: null, permissions: null };
    let permissions = [];

    const { refreshToken, username, password } = request.all();

    if (refreshToken) {
      dado.token = await auth
        .withRefreshToken()
        .query(builder => {
          builder.where("status", 1);
        })
        .attempt(username, password);
      //dado.token = await auth.generateForRefreshToken(refreshToken, true);
      dado.user = await User.findBy("username", username);
    }

    try {
      dado.token = await auth
        .withRefreshToken()
        .query(builder => {
          builder.where("status", 1);
        })
        .attempt(username, password);
        const user = await User.findBy("username", username);
        const empresa = await Empresa.first();
      dado.role = await user.getRoles();
      dado.user = {
        email: user.email,
        funcao_id: user.funcao_id,
        id: user.id,
        is_active: user.status,
        nome: user.nome,
        telefone: user.telefone,
        username: user.username,
        alterPassword: user.is_alterPassword,
        empresa: empresa
      };

      for (let v = 0; v < dado.role.length; v++) {
        let role = await Role.findBy("slug", dado.role[v]);

        let permissionSlugs = await role.getPermissions();

        for (let i = 0; i < permissionSlugs.length; i++) {
          permissions.push(permissionSlugs[i]);
        }
      }
      dado.permissions = permissions;

      return DataResponse.response(
        "error",
        response.response.statusCode,
        "Seja Bem vindo senhor(a) " + dado.user.nome,
        dado
      );
    } catch (e) {
      return response.status(400).send({
        title: "Falha na Autenticação",
        message:
          "Nome de utilizador ou Password Inválido, ou consulta o administrador para verificar se a sua conta está activa"
      });
      // return response.send(Mensagem.response(401, 'Username ou password inválido', dado))
    }
  }

  /*async authenticate({ request, response, auth }) {
   
   const { uname, upass,code, tempSecret, headers} = request.all();  
   let token = commons.userObject.token;
 
	 
    console.log(`DEBUG: Received login request`);
 
    if (commons.userObject.uname && commons.userObject.upass) {
	  const user1 = await Database.select("empresas.active_tfa").from("users").innerJoin("empresas", "empresas.id", "users.empresa_id").where("users.email", commons.userObject.uname);
	  const user = {  active_tfa: user1[0].active_tfa == 1 ? true : false };
		  
      if (!commons.userObject.tfa || !commons.userObject.tfa.secret) {
        if (uname == commons.userObject.uname && upass == commons.userObject.upass) {
          console.log("DEBUG: Login without TFA is successful");
          return DataResponse.response("success", 200, "",  { token, user });
        }
        console.log("ERROR: Login without TFA is not successful");

        return DataResponse.response( "success",403,"Invalid username or password",  { token, user } );
      } else {
        if ( request.body.uname != commons.userObject.uname ||  request.body.upass != commons.userObject.upass ) {
          console.log("ERROR: Login with TFA is not successful");

          return DataResponse.response("success",403,"Invalid username or password", { token, user });
        }
		
         if (code=="") {
          console.log("WARNING: Login was partial without TFA header"); 
		  console.log(commons.userObject.secret); 
          return DataResponse.response( "success",206,"Please enter the Auth Code", { token, user });
        }
		
		commons.userObject.secret = tempSecret;
		
        let isVerified = speakeasy.totp.verify({
          secret: commons.userObject.secret,
          encoding: "base32",
          token: code
        });
		
		console.log("secret: "+commons.userObject.secret);
 
        if (isVerified) { 
		 
          console.log("DEBUG: Login with TFA is verified to be successful"); 
          return DataResponse.response("success", 200, "success",  { token, user });
        } else {
			
			token = await auth.attempt(uname, upass); 
		    const user1 = await Database.select("users.id","users.nome",'users.email',"empresas.active_tfa").from("users").innerJoin("empresas", "empresas.id", "users.empresa_id").where("users.email", uname);
		 
          console.log("ERROR: Invalid AUTH code "+isVerified);

          return DataResponse.response("error", 206, "Invalid Auth Code",  { token, user });
        }
      }
    }else{
		
		let token = await auth.attempt(uname, upass); 
		 const user1 = await Database.select("users.id","users.nome",'users.email',"empresas.active_tfa").from("users").innerJoin("empresas", "empresas.id", "users.empresa_id").where("users.email", uname);
		 
		  const user = { 
			active_tfa: user1[0].active_tfa == 1 ? true : false
		  };
		
		if(user.active_tfa ==true){
			commons.userObject.uname = uname // result.uname;
			commons.userObject.upass = upass; //result.upass;
			commons.userObject.token = token; //result.upass;	
			
			delete commons.userObject.tfa;
		}  
		//console.log(commons.userObject);
		console.log(`DEBUG: create userObject request`);
		
		return DataResponse.response("success", 200, "success",  { token, user });
	} 
  }*/

  /**
   * Update user details.
   * PUT or PATCH Roles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async update({ params, request, response }) {
    const data = request.only([
      "nome",
      "telefone", 
      "email",
      "morada",
      "empresa_id",
      "role_id",
      "status"
    ]);

    const loja = request.only(["loja_id"]);

    //console.log(loja.loja_id)
    /*
    // update with new data entered
    const user = await User.find(params.id);
    user.merge({'loja_id': loja.loja_id, ...data});
    await user.save();

    */

    /*if (loja.loja_id == "null") {
      await Database.table("users")
        .where("id", params.id)
        .update({ loja_id: null, ...data });
    } else {
      await Database.table("users")
        .where("id", params.id)
        .update({ loja_id: loja.loja_id, ...data });
    }*/
	
	await User.query().where("id", params.id).update({ loja_id: loja.loja_id, ...data });

    const role = await Database.select("*")
      .table("role_user")
      //.where('role_id', data.role_id)
      //.where('user_id', user.id)
      .where("user_id", params.id)
      .first();

    if (role == undefined) {
      await Database.table("role_user").insert({
        role_id: data.role_id,
        user_id: params.id
      });
    } else {
      await Database.table("role_user")
        .where("id", role.id)
        .update("role_id", data.role_id);
    }

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      ""
    );
  }

  async show({ params, response }) {
    /*const user = await User.find(params.id);
    const res = {
      nome: user.nome,
      username: user.username,
      email: user.email
    };*/

    const user = await Database.select(
      "users.id",
      "users.nome",
      "users.telefone",
      "users.email",
      "users.username",
      "users.morada",
      "users.status",
      "users.created_at",
      "users.empresa_id",
      "users.role_id",
      "users.updated_at",
      "empresas.companyName",
      "empresas.addressDetail",
      "empresas.email",
      "empresas.telefone",
      "empresas.city",
      "empresas.province",
      "empresas.taxRegistrationNumber",
      "empresas.logotipo",
      "empresas.projecto_isActive",
      "roles.slug",
      "roles.name"
    )
      .from("users")
      .innerJoin("empresas", "empresas.id", "users.empresa_id")
      .innerJoin("roles", "roles.id", "users.role_id")
      .where("users.id", params.id)
      .first();

    const res = {
      id: user.id,
      nome: user.nome,
      username: user.username,
      telefone: user.telefone,
      email: user.email,
      morada: user.morada,
      role: user.role_id,
      role_slug: user.slug,
      role_name: user.name,
      projecto_isActive: user.projecto_isActive
    };

    return response.json(res);
  }
  

  async selectBoxAllUsers({ response }) {
    const res = await Database.select('id', 'nome').from('users')
    .orderBy("users.nome", "ASC");
    return res;
  }

  async resetPassword({ request, response, auth }) {
    const Persona = use("Persona");
    const payload = request.only([
      "old_password",
      "password",
      "password_confirmation",
      "is_alterPassword"
    ]); 
    try {
      const user = await User.find(auth.user.id);
      console.log(user);
      await Database.table("users").where("id", user.id).update({ is_alterPassword: true });
      await Persona.updatePassword(user, payload);
      
      return DataResponse.response(
        "success",
        200,
        "Senha alterada com sucesso.",
        null
      );
    } catch (e) {
      return DataResponse.response(
        "success",
        500,
        "Falha na Aleração da Senha, Verifica as senhas digitadas",
        null
      );
    }
  }

  /**
   * Display a single roleuser.
   * POST changePassword/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async changePassword({ request }) {
    // add this to the top of the file
    const { user_id } = request.all();
    const data = request.only(["password"]);
    // update with new data entered
    const user = await User.find(user_id);
    user.merge(data);
    await Database.table("users")
      .where("id", user.id)
      .update({ is_alterPassword: false });
    const u = await user.save();
    return DataResponse.response(
      "success",
      200,
      "Senha redefinida com sucesso",
      null
    );
  }

  async isAlterPassword({ request, auth }) {
    var r = null;
    r = await Database.select("*").from("users").where("id", auth.user.id).first();  
    return DataResponse.response("success", 200, null, { alterPassword: r.is_alterPassword }); 
  }

  // --------------------------------------------------------
}

module.exports = AuthController;
