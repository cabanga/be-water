'use strict'

const DataResponse = use("App/Models/DataResponse");
const Dadosfacturacao = use("App/Models/Dadosfacturacao");
const Database = use("Database");
var moment = require("moment");

class DadosfacturacaoController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "dadosfacturacaos.id",
        "dadosfacturacaos.municipio_id",
        "municipios.nome as municipio",
        "dadosfacturacaos.nif",
        "dadosfacturacaos.morada",
      )
        .from("dadosfacturacaos")
        .leftJoin("municipios", "dadosfacturacaos.municipio_id", "municipios.id")
        .orderBy(orderBy == null ? "dadosfacturacaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "dadosfacturacaos.id",
        "dadosfacturacaos.municipio_id",
        "municipios.nome as municipio",
        "dadosfacturacaos.nif",
        "dadosfacturacaos.morada",
      )
        .from("dadosfacturacaos")
        .leftJoin("municipios", "dadosfacturacaos.municipio_id", "municipios.id")
        .orWhere("dadosfacturacaos.nif", "like", "%" + search + "%")
        .orWhere("municipios.nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "dadosfacturacaos.created_at" : orderBy, "DESC")
        .orderBy(orderBy == null ? "municipios.nome" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { municipio_id, nif, morada } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('dadosfacturacaos')
      .where("nif", nif)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe registo com este NIF",
        Verify
      );

    } else {
      const Dadosfacturacao = await Database.table('dadosfacturacaos').insert({
        nif: nif,
        municipio_id: municipio_id,
        morada: morada,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        Dadosfacturacao
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["municipio_id", "nif", "morada"]);

    const Verify = await Dadosfacturacao.query()
      .where("nif", data.nif)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe registo com este NIF",
        Verify
      );
    } else {

      // update with new data entered
      const dadosfacturacaos = await Dadosfacturacao.find(params.id);
      dadosfacturacaos.merge(data);
      await dadosfacturacaos.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        dadosfacturacaos
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'morada', 'nif', 'municipio_id')
      .from('dadosfacturacaos')
      .orderBy('descricao', 'ASC');

    return res;
  }

  async getLojasAssociadas({ request, params }) {

    const DadosfacturacaoLojas = await Database.select(
      "dadosfacturacao_lojas.id",
      "dadosfacturacao_lojas.loja_id",
      "lojas.nome as loja",
      "dadosfacturacao_lojas.horario",
    )
      .from("dadosfacturacao_lojas")
      .leftJoin("lojas", "dadosfacturacao_lojas.loja_id", "lojas.id")
      .where("dadosfacturacao_lojas.dadosfacturacao_id", params.id)

    return DataResponse.response("success", 200, "", DadosfacturacaoLojas);
  }

  async getContasAssociadas({ request, params }) {

    const contasbancarias = await Database.select(
      "conta_bancarias.id",
      "conta_bancarias.iban",
      "conta_bancarias.municipio_id",
      "municipios.nome as municipio",
      "conta_bancarias.banco_id",
      "conta_bancarias.is_active",
      "bancos.nome as banco"
    )
      .from("conta_bancarias")
      .leftJoin("municipios", "conta_bancarias.municipio_id", "municipios.id")
      .leftJoin("bancos", "conta_bancarias.banco_id", "bancos.id")
      .where("conta_bancarias.municipio_id", params.id)

    return DataResponse.response("success", 200, "", contasbancarias);
  }

  async associarlojas({ request, auth, params }) {
    const { loja_id, horario } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const DadosfacturacaoLojas = await Database.table('dadosfacturacao_lojas').insert({
      dadosfacturacao_id: params.id,
      loja_id: loja_id,
      horario: horario,
      user_id: auth.user.id,
      'created_at': dataActual,
      'updated_at': dataActual
    });
    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      DadosfacturacaoLojas
    );
  }

}

module.exports = DadosfacturacaoController
