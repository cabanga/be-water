'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Contagen = use("App/Models/Contagen");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

/**
 * Resourceful controller for interacting with contagens
 */
class ContagenController {
  /**
   * Show a list of all contagens.
   * GET contagens
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a list contagen.
   * GET contagens/listagem
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "contagens.id",
        "contagens.rota_run_id",
        "contagens.user_id",
        "contagens.leitura",
        "contagens.ultima_leitura",
        "contagens.latitude",
        "contagens.longitude",
        "contagens.contador_id",
        "contadores.contador as contador",
        "contagens.origem_id",
        "origems.nome as origem",
        "clientes.id as cliente_id",
        "clientes.nome as cliente_nome",
        "users.nome as leitor",
        "contagens.updated_at",
        Database.raw(
          'DATE_FORMAT(contagens.created_at, "%Y-%m-%d") as created_at'
        )
      )
        .from("contagens")
        .leftJoin(
          "rota_runs",
          "rota_runs.id",
          "contagens.rota_run_id"
        )
        .leftJoin(
          "contadores",
          "contadores.id",
          "contagens.contador_id"
        )
        .leftJoin(
          "origems",
          "origems.id",
          "contagens.origem_id"
        )
        .innerJoin(
          "clientes",
          "clientes.id",
          "contadores.cliente_id"
        )
        .leftJoin(
          "users",
          "users.id",
          "contagens.user_id"
        )
        .orderBy(orderBy == null ? "contagens.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "contagens.id",
        "contagens.rota_run_id",
        "contagens.user_id",
        "contagens.leitura",
        "contagens.ultima_leitura",
        "contagens.latitude",
        "contagens.longitude",
        "contagens.contador_id",
        "contadores.contador as contador",
        "contagens.origem_id",
        "origems.nome as origem",
        "clientes.id as cliente_id",
        "clientes.nome as cliente_nome",
        "users.nome as leitor",
        "contagens.updated_at",
        Database.raw(
          'DATE_FORMAT(contagens.created_at, "%Y-%m-%d") as created_at'
        )
      )
        .from("contagens")
        .leftJoin(
          "rota_runs",
          "rota_runs.id",
          "contagens.rota_run_id"
        )
        .leftJoin(
          "contadores",
          "contadores.id",
          "contagens.contador_id"
        )
        .leftJoin(
          "origems",
          "origems.id",
          "contagens.origem_id"
        )
        .innerJoin(
          "clientes",
          "clientes.id",
          "contadores.cliente_id"
        )
        .leftJoin(
          "users",
          "users.id",
          "contagens.user_id"
        )
        .where("contadores.contador", "like", "%" + search + "%")
        .orWhere("users.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(contagens.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "contagens.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async create({ request, response, view }) {
  }

  /**
   * Create/save a new contagen.
   * POST contagens
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request }) {

    let ultima_contagem = null;

    const {
      contador_id,
      origem_id,
      leitura,
      latitude,
      longitude,
      data,
      user_id
    } = request.all();

    ultima_contagem = await Database.select(
      "id",
      "leitura"
    )
      .from("contagens")
      .where("contagens.contador_id", contador_id)
      .orderBy("contagens.created_at", "DESC")
      .first();

    console.log(request.all());
    console.log(ultima_contagem);

    const contagen = await Database.table('contagens')
      .insert({
        origem_id: origem_id,
        contador_id: contador_id,
        leitura: leitura,
        latitude: latitude,
        longitude: longitude,
        user_id: user_id,
        ultima_leitura: (ultima_contagem == null) ? "0" : ultima_contagem.leitura,
        updated_at: data
      })

    console.log(contagen);

    return DataResponse.response(
      "success",
      200,
      "Contagem registado com sucesso",
      contagen
    );

  }

  /**
   * Display a single contagen.
   * GET contagens/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing contagen.
   * GET contagens/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update contagen details.
   * PUT or PATCH contagens/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only([
      "contador_id",
      "origem_id",
      "leitura",
      "latitude",
      "longitude",
      "data",
      "user_id"
    ]);

    const contagen = await Contagen.find(params.id);

    contagen.merge(data);
    await contagen.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      contagen
    );
  }

  /**
   * Delete a contagen with id.
   * DELETE contagens/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  async getLastContagemByContador({ params }) {

    let res = null;

    res = await await Database.select(
      "contagens.id",
      "contagens.rota_run_id",
      "contagens.contador_id",
      "contagens.user_id",
      "contagens.leitura",
      "contagens.ultima_leitura",
      "contagens.created_at"
    )
      .from("contagens")
      .where("contagens.contador_id", params.id)
      .orderBy("contagens.created_at", "DESC")
      .first();

    console.log(params.id);
    console.log(res);
    return res;
  }

  async selectBoxContagemRotaRuns() {
    const contagens = await Database.select("ct.id", "ct.leitura", "rh.nome as rota")
      .table("contagens as ct")
      .innerJoin("rota_runs as rr", "rr.id", "ct.rota_run_id")
      .innerJoin("rota_headers as rh", "rh.id", "rr.id")
    return DataResponse.response("success", 200, "", contagens);
  }


  async selectBoxContagemContadores() {
    const contagens = await Database.select("ct.id", "ct.leitura", "cd.contador as contador")
      .table("contagens as ct")
      .innerJoin("contadores as cd", "cd.id", "ct.contador_id")
    return DataResponse.response("success", 200, "", contagens);
  }

  async selectBoxContagemUsers() {
    const contagens = await Database.select("ct.id", "ct.leitura", "u.nome as user")
      .table("contagens as ct")
      .innerJoin("users as u", "u.id", "ct.user_id")
    return DataResponse.response("success", 200, "", contagens);
  }
}

module.exports = ContagenController
