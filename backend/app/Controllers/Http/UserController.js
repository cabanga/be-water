'use strict'

const User = use('App/Models/User')
const RoleUser = use('App/Models/RoleUser')
const Role = use('App/Models/Role')
const Database = use("Database");
const DataResponse = use("App/Models/DataResponse");

class UserController {
  /**
   * Create/save a new user.
   * POST users
   */
  /*async index({ request, response }) {
    const { perPage, page, search, role_id, organismo_id } = request.all();
    // const users = await Database.select('id', 'nome', 'sobrenome', 'username', 'email', 'telemovel' ).from('users')
    let users = null;
    if (search == null && organismo_id == null && role_id==null) {
      users = await Database.select(
        "users.id",
        "users.nome",
        "users.sobrenome",
        "users.username",
        "users.email",
        "users.telemovel",
        "users.telefone",
        "organismos.designacao as organismo",
        "roles.name as funcao"
      )
        .from("users")
        .innerJoin("role_user", "role_user.user_id", "users.id")
        .innerJoin("roles", "role_user.role_id", "roles.id")
        .paginate(page, perPage);
      return users;
    }
    users = await Database.select(
      "users.id",
      "users.nome",
      "users.sobrenome",
      "users.username",
      "users.email",
      "users.telemovel",
      "users.telefone",
      "organismos.designacao as organismo",
      "roles.name as funcao"
    )
      .from("users")
      .innerJoin("role_user", "role_user.user_id", "users.id")
      .innerJoin("roles", "role_user.role_id", "roles.id")
      .where("roles.id", role_id)
      .orWhere("users.nome", "like", "%" + search + "%")
      .orWhere("organismos.designacao", "like", "%" + search + "%")
      .paginate(page, perPage);

    return users;
  }*/

  /**
   * Display a single roleuser.
   * GET roleusers/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response }) {}
  /**
   * Create/save a new user.
   * POST users

   */
  async store({ request, response, auth }) {
    //await Database.transaction(async (trx) => {
    // const userData = request.all()
    const userData = request.only([
      "nome",
      "telefone",
      "username",
      "email",
      "password",
      "morada",
      "empresa_id",
      "loja_id",
      "status"
    ]);
    const role_id = request.input("role_id");

    try {
      const user = await User.create(userData);
      const role = await Role.find(role_id);
      const user_role = await user.roles().attach([role.id]);


      return DataResponse.response(
        "success",
        200,
        "Utilizador registado com sucesso.",
        user
      );
    } catch (e) {
      return DataResponse.response(
        "info",
        201,
        "Falha não registar utilizador, por favor verifica se o utilizador já existe",
        null
      );
    }
  }
  async resetPassword({ request, response, auth }) {
    const Persona = use("Persona");

    const payload = request.only([
      "old_password",
      "password",
      "password_confirmation",
      "is_alterPassword"
    ]);

    try {
      const user = auth.user;
      await Persona.updatePassword(user, payload);
      return response.send(Mensagem.response( response.response.statusCode, "Senha alterada com sucesso."));
    } catch (e) {
      return response.status(400).send({
        title: "Falha na Aleração da Senha",
        message: "Verifica as senhas digitadas"
      });
      // return response.send(Mensagem.response(401, 'Username ou password inválido', dado))
    }
  }

  /**
   * Display a single roleuser.
   * POST changePassword/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async changePassword({ params, request, response }) {
    // add this to the top of the file
    const Hash = use("Hash");

    const data = request.only(["password"]);
    // update with new data entered
    const user = await User.find(params.id);
    user.merge(data);
    await user.save();
    response.json({ message: "Senha redefinida com sucesso", data: null });
  }

  async selectBox({ params }) {
    const users =  await User.query().where("loja_id", params.id).fetch();
    return DataResponse.response("success", 200, "", users);
  }

  async usersByRole({ params }) {

    let users = null;

    users = await Database.select('users.id', 'users.nome', 'users.email')
      .table('users')
      .innerJoin('roles', 'roles.id', 'users.role_id')
      .whereNotNull('users.email')
      .where('users.role_id', params.id)

    return DataResponse.response("success", 200, "", users);
  }

  async perfil({params}){
    //const user = await User.find(params.id)

    const user = await User.query().where("id",params.id)
    .with('loja')
    .with("empresa")
    .withCount('facturas')
    .withCount('recibos')
    .withCount('caixa')
    .first()

    return DataResponse.response("success", 200, "", user);

  }

  async searchUtilizador({ request }) {
    const { start, end, search } = request.all();

    let res = null;

    if (search == null) {
     res = await Database
      .select('id', 'nome')
      .from('users')
      .orderBy('id','ASC')
      .paginate(start, end);
    } else {

      res = await Database
      .select('id', 'nome')
      .from('users')
      .where("nome", "like", "%" + search + "%")
      .orWhere("telefone", "like", "%" + search + "%")
      .orWhere("email", "like", "%" + search + "%")
      .orderBy('id','ASC')
      .paginate(start, end);
    }

    return DataResponse.response("success", 200, null, res);
  }

  async selectBoxLeitoresWithRotasHeaders({ request }) {
    const { start, end, search } = request.all();

    let res = null;

      res = await Database
      .select(
         'users.id',
        'users.nome',
        'rota_headers.leitor_id'
      )
      .from('rota_headers')
      .innerJoin("users", "users.id", "rota_headers.leitor_id")
      .distinct('users.id')
      .orderBy('users.nome','ASC');

    return res;
  }
  async selectBoxOperadores({ request, view, response, auth }) {
    let operadores = await Database.select("*")
    .from("lojas").leftJoin("users",'lojas.id','users.loja_id').whereNotNull("users.nome")
    return DataResponse.response("success", 200, null, operadores);
  }
}

module.exports = UserController
