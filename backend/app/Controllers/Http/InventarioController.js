'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const StokeMovimento = use('App/Models/StokeMovimento');
const User = use('App/Models/User');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');

/**
 * Resourceful controller for interacting with inventarios
 */
class InventarioController {
   /**
   * Show a list of all facturas.
   * GET facturas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({
    request,
    response,
    view
  }) {
    const {start, end, search, order, searchData} = request.all();
    let res = null;


    if (search == null) {
      res = await Database.select('stoke_movimentos.tipo_doc', 'stoke_movimentos.quantidade','stoke_movimentos.movimento','stoke_movimentos.armazem'
	  ,'stoke_movimentos.created_at').from('stoke_movimentos')
	  .innerJoin('produtos', 'produtos.id', 'stoke_movimentos.artigo')
	  .innerJoin('documentos', 'documentos.id', 'stoke_movimentos.documento_id')
	  .where('stoke_movimentos.tipo_doc','INV')
	  .orderBy('facturas.created_at', 'ASC').paginate(start, end)
    } else {
      res = await Database.select('stoke_movimentos.tipo_doc', 'stoke_movimentos.quantidade','stoke_movimentos.movimento','stoke_movimentos.armazem'
	  ,'stoke_movimentos.created_at','documentos.nome as documento','produtos.nome as produto').from('stoke_movimentos')
	  .innerJoin('produtos', 'produtos.id', 'stoke_movimentos.artigo')
	  .innerJoin('documentos', 'documentos.id', 'stoke_movimentos.documento_id').where('stoke_movimentos.tipo_doc','INV')
		.where('produtos.nome', 'like', '%' + search + "%")
		.orWhere('stoke_movimentos.movimento', 'like', '%' + search + "%")
		.orWhere(Database.raw('DATE_FORMAT(stoke_movimentos.created_at, "%Y-%m-%d")'), 'like', '%' + search + "%")
		.orderBy(order, 'asc').paginate(start, end)
    }

     return DataResponse.response("success", 200, "", res);
  }
  /**
   * Render a form to be used for creating a new inventario.
   * GET inventarios/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request }) {



  }

  /**
   * Create/save a new inventario.
   * POST inventarios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
	  const {serie_id, artigos} = request.all();
	  const serie = await Database.select(
      "series.id as id",
      "series.nome",
      "series.proximo_numero",
      "series.activo",
      "series.movimento",
      "series.tipo_movimento",
      "documentos.nome as documento",
      "documentos.sigla",
      "series.documento_id",
      "documentos.sigla"
    )
      .from("series")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("series.id", serie_id);



	  for (let index = 0; index < artigos.length; index++) {
			await StokeMovimento.create({
        tipo_doc: serie[0].sigla,
        documento_id: serie[0].documento_id,
        artigo: artigos[index].produto_id,
        quantidade: artigos[index].quantidade,
        movimento: serie[0].tipo_movimento,
        armazem: serie[0].armazen_id
      });
     };

	 return DataResponse.response("success", 200, "Dados registados com sucesso", null);
  }

  /**
   * Display a single inventario.
   * GET inventarios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing inventario.
   * GET inventarios/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update inventario details.
   * PUT or PATCH inventarios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a inventario with id.
   * DELETE inventarios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = InventarioController
