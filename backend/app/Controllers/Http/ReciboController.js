"use strict";

const Factura = use("App/Models/Factura");
const Recibo = use("App/Models/Recibo");
const LinhaFactura = use("App/Models/LinhaFactura");
const LinhaRecibo = use("App/Models/LinhaRecibo");
const Cliente = use("App/Models/Cliente");
const User = use("App/Models/User");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var numeral = require("numeral");
const ReferenciaBancaria = use("App/Models/ReferenciaBancaria");
const LinhaPagamento = use("App/Models/LinhaPagamento");

const MovimentoAdiantamento = use("App/Models/MovimentoAdiantamento");

const Adiantamento = use("App/Models/Adiantamento");
const Pagamento = use("App/Models/Pagamento");
const moment = require("moment");

class ReciboController {

  async index({ request, auth }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    //const docs = (auth.user.loja_id == null? ['FT'])
    if (search == null) {
      res = await Database.select(
        "recibos.id as recibo_id",
        "recibos.recibo_sigla",
        "recibos.observacao",
        "recibos.hash",
        "recibos.hash_control",
        "recibos.status",
        "recibos.status_date",
        "recibos.status_reason",
        "recibos.total",
        "recibos.user_id",
        "recibos.id",
        "recibos.numero",
        "recibos.cliente_id",
        "recibos.created_at",
        "recibos.pago",
        "recibos.serie_id",
        "recibos.user_id",
        "recibos.user_id",
        "recibos.user_id",
        "recibos.status",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla",
        "clientes.nome as clienteNome"
      )
      .from("recibos")
      .innerJoin("series", "series.id", "recibos.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .innerJoin("clientes", "clientes.id", "recibos.cliente_id")
      .orderBy(orderBy == null ? "recibos.created_at" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "recibos.id as recibo_id",
        "recibos.recibo_sigla",
        "recibos.observacao",
        "recibos.hash",
        "recibos.hash_control",
        "recibos.status",
        "recibos.status_date",
        "recibos.status_reason",
        "recibos.total",
        "recibos.user_id",
        "recibos.id",
        "recibos.numero",
        "recibos.cliente_id",
        "recibos.created_at",
        "recibos.pago",
        "recibos.serie_id",
        "recibos.user_id",
        "recibos.user_id",
        "recibos.user_id",
        "recibos.status",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla",
        "clientes.nome as clienteNome"
      )
      .from("recibos")
      .innerJoin("series", "series.id", "recibos.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .innerJoin("clientes", "clientes.id", "recibos.cliente_id")
      .where("clientes.nome", "like", "%" + search + "%")
      .orWhere("recibos.numero", "like", "%" + search + "%")
      .orWhere("recibos.pago", "like", "%" + search + "%")
      .orWhere("recibos.status", "like", "%" + search + "%")

      .orWhere("documentos.sigla", "like", "%" + search + "%")
      .orWhere("recibos.recibo_sigla", "like", "%" + search + "%")
      .orWhere(
        Database.raw('DATE_FORMAT(recibos.created_at, "%Y-%m-%d")'),
        "like",
        "%" + search + "%"
      )
      .orderBy(orderBy == null ? "recibos.created_at" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);
    }

    //console.log(res)

    return DataResponse.response("success", 200, "", res);
  }


  async store({ request, auth }) {
    var moment = require("moment")

    //============= VALIDAÇÃO DE ABERTURA DE CAIXA ================
    //=============================================================
    var current_date = moment(new Date()).format("YYYY-MM-DD");

    console.log("========================================")
    console.log( auth.user.id )

    let caixa = await Database
    .select("*")
    .from("caixas")
    .where("is_active", true)
    .where("data_abertura", current_date)
    .where("user_id", auth.user.id)
    .first()

    if(!caixa){
      return DataResponse.response( "success", 201, "Não foi possivel concluir a operação porque não existe caixa aberto", caixa )
    }

    const {
      facturas,
      cliente_id,
      serie_id,
      troco,
      total_saldado,
      total_valor_recebido,
      forma_pagamento_id,
      linha_pagamentos
    } = request.all();

    var estatistica = {
      recibo: null,
      facts: [],
      contTotalFacturaSaldadas: 0,
      contTotalFacturaAbertas: 0,
      contTotalFacturaNaoAvaliadas: 0
    };



    var countRef = 0
    for (let index = 0; index < linha_pagamentos.length; index++) {
      const r = await Database.from('referencia_bancarias').where('referencia', linha_pagamentos[index].referencia_banco).where('banco_id', linha_pagamentos[index].banco_id)

      if (r.length > 0) {
        countRef++;
        estatistica.facts.push(linha_pagamentos[index]);
      }
    }

    if(countRef > 0){
      return DataResponse.response( "success", 500, "Referencias invalidas", estatistica );
    }

    const serie = await Database.select(
      "series.id as id",
      "series.nome",
      "series.proximo_numero",
      "series.activo",
      "documentos.nome as documento",
      "documentos.sigla"
    )
    .from("series")
    .innerJoin("documentos", "documentos.id", "series.documento_id")
    .where("series.id", serie_id);

    let generate_number = serie[0].proximo_numero
    let sigla = serie[0].sigla + " " + serie[0].nome + "/" + serie[0].proximo_numero

    /*
    let exist_recibo = await Recibo.query()
    .select('*')
    .where('numero', generate_number)
    .orWhere('recibo_sigla', sigla)
    .first()

    console.log("========================================")
    console.log( exist_recibo.id )

    if (exist_recibo.id) {
      return DataResponse.response( "success", 409, "Não foi possivel concluir a operação porque existe um recibo com a mesma referência", exist_recibo )
    }
    */


    var moment = require("moment");
    var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    var doc_date = moment(new Date()).format("YYYY-MM-DD");
    var doc_red = moment(new Date()).format("YYYY-MM-DD") + "T" + moment(new Date()).format("HH:mm:ss");
    var l_hash = doc_date + ";" + doc_red + ";" + serie[0].sigla + " " + serie[0].nome + "/" + serie[0].proximo_numero + ";" + numeral(total_saldado).format("0.00") + ";";

    const max_n = await Database
    .from("recibos")
    .where("serie_id", serie_id)
    .max("numero as total");

    var cc = 0;

    if (max_n[0].total == null) {
      cc = 0;
    } else {
      cc = max_n[0].total;
    }

    if (cc > 0) {
      const doc_ant = await Database.select("hash as hash")
        .from("recibos")
        .where("serie_id", serie_id)
        .where("numero", cc);
      l_hash = l_hash + doc_ant[0].hash;
    }

    const pagament = await Pagamento.create({
      valor_recebido: total_valor_recebido,
      troco: troco,
      total_pago: total_saldado,
      forma_pagamento_id: forma_pagamento_id,
      documento_sigla: serie[0].sigla,
      user_id: auth.user.id
    });

    for (let index = 0; index < linha_pagamentos.length; index++) {
      await LinhaPagamento.create({
        valor_recebido: linha_pagamentos[index].valor_entrada,
        referencia: linha_pagamentos[index].referencia_banco,
        data_pagamento: linha_pagamentos[index].data_pagament,
        forma_pagamento_id: linha_pagamentos[index].id,
        pagamento_id: pagament.id,
        user_id: auth.user.id
      });

      if(linha_pagamentos[index].referencia_banco!=null){
        await ReferenciaBancaria.create({
          referencia: linha_pagamentos[index].referencia_banco,
          data_pagamento: moment(linha_pagamentos[index].data_pagament).format("YYYY-MM-DD"),
          banco_id: linha_pagamentos[index].banco_id,
          user_id: auth.user.id
        });
     }
    }

    console.log("======================SERIE ========================")
    console.log("SERIE ", serie[0].id)
    console.log("CAIXA ", caixa.id)
    console.log("PAGAMENT ", pagament.id)

    const recibo = await Recibo.create({
      numero: generate_number,
      recibo_sigla: sigla,
      serie_id: serie[0].id,
      total: numeral(total_saldado).format("0.00"),
      pago: false,
      status: "N",
      status_date: data,
      hash: this.gearAss(l_hash),
      hash_control: "1", //l_hash,
      cliente_id: cliente_id,
      caixa_id: caixa.id,
      user_id: auth.user.id,
      pagamento_id: pagament.id
    });

    estatistica.recibo = recibo


  var ve = total_valor_recebido; // valor de entrada


  for (let index = 0; index < facturas.length; index++) {
      var factura = facturas[index];
      var va = factura.valor_aberto; // valor actual em aberto da factura
      var saldado = 0  // recebe o valor saldado na factura
     if(ve > 0){

        ve = ve - va;
        saldado = (ve < 0 ? ve - va*-1 : va) // calcula o valor saldado
        var van =  va - saldado // calcula o novo valor em aberto para a factura
        await LinhaRecibo.create({
          factura_id: factura.id,
          recibo_id: recibo.id,
          valor_saldado: saldado,
          user_id: auth.user.id,
          novo_valor_aberto: van
        });
        await Database.table("facturas").where("id", factura.id).update({ "pago": (van == 0 ? true : false),"valor_aberto": van });

      /* console.log("---------------------------");
       console.log("Valor Entrada: " + ve);
       console.log("Valor Saldado: " + saldado);
       console.log("Valor Aberto: " + va);
       console.log("Valor Aberto Novo: " + van);
       console.log("---------------------------");*/

       estatistica.facts.push({
         factura_sigla: factura.factura_sigla,// número da factura
         saldado: saldado, // valor saldado(resto)
         va_ant: va, // valor aberto anterior
         va_new: van // valor aberto novo
       });

       if (van == 0) {
         estatistica.contTotalFacturaSaldadas++
       }else{
         estatistica.contTotalFacturaAbertas++
       }

     }else{
        estatistica.contTotalFacturaNaoAvaliadas++;
     }

   }

    /*console.log(estatistica.facts);
    console.log("facturas saldadas: " + estatistica.contTotalFacturaSaldadas);
    console.log("facturas Em aberto: " + estatistica.contTotalFacturaAbertas);
    console.log("facturas Não Avaliadas: " + estatistica.contTotalFacturaNaoAvaliadas);
    console.log("Valor Entrada: " + ve); */



    //}


    await Database.table("series")
      .where("id", serie[0].id)
      .update("proximo_numero", Number(serie[0].proximo_numero) + 1);

    return DataResponse.response(
      "success",
      200,
      "recibo gerado com sucesso",
      estatistica
    );






  }


  async show({ params, request, response, view }) {}


  async edit({ params, request, response, view }) {}


  async update({ params, request, response }) {}


  async destroy({ params, request, response }) {}

  /**
   * Update anular factura details.
   * PUT or PATCH clientes/:id
   * @author caniggiamoreira@hotmail.com ou caniggiamoreira@itgest.pt
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async printerRecibo({ params, request, response }) {
    let cliente = null;
    let user = null;
    let factura = null;
    let recibo = null;
    let conta = null;
    let linha_pagamentos = null;

    recibo = await Database.select(
      "recibos.id as recibo_id",
      "recibos.recibo_sigla",
      "recibos.observacao",
      "recibos.hash",
      "recibos.hash_control",
      "recibos.status",
      "recibos.status_date",
      "recibos.status_reason",
      "recibos.total",
      "recibos.user_id",
      "recibos.id",
      "recibos.numero",
      "recibos.cliente_id",
      "recibos.created_at",
      "recibos.pago",
      "recibos.serie_id",
      "recibos.user_id",
      "recibos.user_id",
      "recibos.user_id",
      "recibos.pagamento_id",

      "movimento_adiantamentos.valor as valorCriadoAdiantamento",
      "adiantamentos.valor as saldoAdiantamento",

      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla",

      "pagamentos.valor_recebido",
      "pagamentos.troco",
      "pagamentos.total_pago",
      "forma_pagamentos.designacao",
      "pagamentos.forma_pagamento_id",

      "movimento_adiantamentos.saldado_recibo"
    )
      .from("recibos")
      .innerJoin("series", "series.id", "recibos.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .innerJoin("pagamentos", "pagamentos.id", "recibos.pagamento_id")
      .leftJoin(
        "movimento_adiantamentos",
        "movimento_adiantamentos.saldado_recibo",
        "recibos.id"
      )
      .leftJoin(
        "adiantamentos",
        "adiantamentos.cliente_id",
        "recibos.cliente_id"
      )
      .leftJoin(
        "forma_pagamentos",
        "forma_pagamentos.id",
        "pagamentos.forma_pagamento_id"
      )
      .where("pagamentos.documento_sigla", "RC")
      .where("recibos.id", params.id)
      .first();

    cliente = await Cliente.find(recibo.cliente_id);

    await cliente.load('municipio')

    user = await Database.select(
      "users.id",
      "users.nome",
      "users.telefone",
      "users.email",
      "users.username",
      "users.morada",
      "users.status",
      "users.created_at",
      "users.empresa_id",
      "users.role_id",
      "users.updated_at",
      "empresas.companyName",
      "empresas.addressDetail",
      "empresas.email",
      "empresas.telefone",
      "empresas.city",
      "empresas.province",
      "empresas.taxRegistrationNumber",
      "empresas.logotipo", "empresas.site",
      "empresas.projecto_isActive",
      "lojas.nome as loja"
    )
      .from("users")
      .innerJoin("empresas", "empresas.id", "users.empresa_id")
      .leftJoin("lojas", "lojas.id", "users.loja_id")
      .where("users.id", recibo.user_id)
      .first();

    factura = await Database.select(
      "facturas.id as factura_id",
      "facturas.factura_sigla",
      "facturas.observacao",
      "facturas.hash",
      "facturas.hash_control",
      "facturas.status",
      "facturas.status_date",
      "facturas.status_reason",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.user_id",
      "facturas.id",
      "facturas.numero",
      "facturas.cliente_id",
      "facturas.created_at",
      "facturas.pago",
      "facturas.serie_id",
      "facturas.user_id",
      "facturas.numero_origem_factura",
      "facturas.data_origem_factura",
      "facturas.is_nota_credito",
      "facturas.moeda_iso",
      "facturas.total_contra_valor",
      "facturas.valor_aberto",
      "facturas.valor_cambio",
      "facturas.totalKwanza",
      "facturas.user_id",
      "facturas.user_id",
      "facturas.conta_id",
      "facturas.servico_id",
      "facturas.data_vencimento",
      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla",
      "linha_recibos.valor_saldado",
      "linha_recibos.novo_valor_aberto"
    )
      .from("linha_recibos")
      .innerJoin("facturas", "linha_recibos.factura_id", "facturas.id")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .leftJoin("contas", "contas.id", "facturas.conta_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("linha_recibos.recibo_id", recibo.recibo_id);

    linha_pagamentos = await Database
      .select('*')
      .from("linha_pagamentos")
      .innerJoin("forma_pagamentos", "forma_pagamentos.id", "linha_pagamentos.forma_pagamento_id")
      .where("linha_pagamentos.pagamento_id", recibo.pagamento_id);

    if (recibo.forma_pagamento_id != null) {
      linha_pagamentos = [{ designacao: recibo.designacao, valor_recebido: recibo.valor_recebido }]
    }

    /*     let contas_bancarias_municipio = await Database
        .select(
          "clientes.id as cliente_id",
          "clientes.nome",
          "clientes.morada",
          "clientes.municipio_id",
          "conta_bancarias.id",
          "conta_bancarias.iban",
          "conta_bancarias.banco_id",
          "conta_bancarias.municipio_id",
          "municipios.nome as municipio",
          "bancos.nome as banco"
        )
        .from("clientes")
        .innerJoin("conta_bancarias", "conta_bancarias.municipio_id", "clientes.municipio_id")
        .innerJoin("bancos", "bancos.id", "conta_bancarias.banco_id")
        .innerJoin("municipios", "municipios.id", "conta_bancarias.municipio_id")
        .where("clientes.id", cliente.id) */

    /*     let lojas = await Database.select(
          "dadosfacturacaos.id",
          "dadosfacturacaos.nif",
          "dadosfacturacaos.morada",
          "dadosfacturacao_lojas.loja_id",
          "lojas.nome as loja",
          "dadosfacturacao_lojas.horario"
        )
        .from("dadosfacturacaos")
        .innerJoin("municipios", "municipios.id", "dadosfacturacaos.municipo_id")
        .innerJoin("dadosfacturacao_lojas", "dadosfacturacaos.id", "dadosfacturacao_lojas.dadosfacturacao_id")
        .innerJoin("lojas", "lojas.id", "dadosfacturacao_lojas.loja_id")
        .where("dadosfacturacaos.municipo_id", cliente.municipio_id) */

    let lojas = await Database.select(
      "*"
    )
      .from("lojas")
      .where("lojas.municipio_id", cliente.municipio_id)

    let contas_bancarias = await Database.select(
      "lojas.id",
      "lojas.nome as loja",
      "lojas.nif",
      "lojas.horario",
      "lojas.endereco",
      "loja_bancos.loja_id",
      "loja_bancos.banco_id",
      "bancos.abreviatura as banco",
      "bancos.iban",
    )
      .from("loja_bancos")
      .innerJoin("lojas", "lojas.id", "loja_bancos.loja_id")
      .innerJoin("bancos", "bancos.id", "loja_bancos.banco_id")
      .where("lojas.municipio_id", cliente.municipio_id)
      .limit(3)


    let tipos_identidades = await Database
      .select(
        'nome',
        'numero_digitos',
        'numero_identidade'
      )
      .from("cliente_identidades")
      .innerJoin("tipo_identidades", "tipo_identidades.id", "cliente_identidades.tipo_identidade_id")
      .where("cliente_identidades.cliente_id", cliente.id)


    let data = {
      recibo: recibo,
      cliente: cliente,
      factura: factura,
      user: user,
      linha_pagamentos: linha_pagamentos,
      tipos_identidades: tipos_identidades,
      /*       contas_bancarias: contas_bancarias_municipio, */
      contas_bancarias: contas_bancarias,
      lojas: lojas,
      conta: conta
    };

    return DataResponse.response("success", 200, "Gerando o recibo... ", data);
  }

  async consultarRecibo({ params }) {
    let recibos = null;

    recibos = await Database.select(
      "recibos.id as recibo_id",
      "recibos.recibo_sigla",
      "recibos.observacao",
      "recibos.hash",
      "recibos.hash_control",
      "recibos.status",
      "recibos.status_date",
      "recibos.status_reason",
      "recibos.total",
      "recibos.user_id",
      "recibos.id",
      "recibos.numero",
      "recibos.cliente_id",
      "recibos.created_at",
      "recibos.pago",
      "recibos.serie_id",
      "recibos.user_id",
      "recibos.user_id",
      "recibos.user_id",
      "recibos.status",
      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla"
    )
      .from("recibos")
      .innerJoin("series", "series.id", "recibos.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("recibos.cliente_id", params.id);

    if (recibos.length == 0) {
      return DataResponse.response( "success", 500,"Nenhum resultado encontrado.", null);
    }

    return DataResponse.response("success", 200, null, recibos);
  }


  async reciboAnular({ request, auth }) {
    var moment = require("moment");
    var statusData = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    const dados = {
      status: "A",
      status_reason: request.input("status_reason"),
      status_date: statusData,
      status_user_id: auth.user.id
    };

    // update with new data entered
    const recibo = await Recibo.find(request.input("recibo_id"));
    if (recibo.status == "A") {
      return DataResponse.response( "error", 500, "O recibo já se encontra anulado.", null );
    }

    const linhaRecibos = await Database.select('*').from("linha_recibos") .where("recibo_id", recibo.id);
    for (let index = 0; index < linhaRecibos.length; index++) {
        const element = linhaRecibos[index];
        var select = (element.valor_saldado==null? "UPDATE facturas SET valor_aberto = total, pago = 0  WHERE id = "+element.factura_id : "UPDATE facturas SET valor_aberto = valor_aberto + "+element.valor_saldado+", pago = 0  WHERE id = "+element.factura_id)
        await Database.raw(select);
    }

    recibo.merge(dados);
    await recibo.save();
    return DataResponse.response( "success", 200,"recibo anulado com sucesso.",recibo);
  }


  gearAss(string) {
    const crypto = require("crypto");
    const fs = require("fs");
    var s = crypto.createSign("RSA-SHA1");
    var key =
      "-----BEGIN RSA PRIVATE KEY-----\n" +
      "MIICXQIBAAKBgQC4faWshk9wvZUouz4A3K4Zzb2NOtbp262HcB1mJYF1QDs3wAnd\n" +
      "kGiqPcBx7TGeIEjuBtg6DFtSy29w1dRCANdqIDqaCqX+/PNE8dz8foCauiy5OEU2\n" +
      "segqAeN3X8PXBevqGThd/x9OPJ4pV2Kgx/oAs7Bwg3/C2AM3qraj0UulhwIDAQAB\n" +
      "AoGAW0RlQk0LXaWb9ZNzn++L/V3niMdz7Crt1JOlJ5QkUAHfibvp5X78GEQGQRXr\n" +
      "NuOX0JD4RPc58mKLldFieOh7p8B/dx8UZyWd11TUOnVwOSJaFd3rwnHzobEUJgH2\n" +
      "24b1bGOWsk+0XEisS1B7xl4d8T74+Dpnpugg4nU/1rAKgjECQQDfpe5Nihi9Fgfz\n" +
      "rcr8s9oGGdKV7nyVXUmBN5Dm5PMfAev49Wo6ZvhO9EW1mb15Kuqfc56Sq5ErDdRg\n" +
      "MPnODP8JAkEA0y2nOcjWn3ZsX0lPvGpKotnFUgO4WlpJfd6fzxTfQrLqHf6ixFPt\n" +
      "wTApqhU0fx9xMWl6m1Kh0WiegMYk8LwUDwJATafsCvh8hotzz2T1KrG4bo3g1Tau\n" +
      "A58Uus10fvfYg1fDe/qbHBRM+/1NhzUO2VfRh/Q5h2wTSAPRTmUzGBzjIQJBAIQw\n" +
      "z70cOz0WpEABZChNYOsP5rSwH3ZvjhF8igzWw+q8lFCyVLEQ2INV4r7VB0eMJw8H\n" +
      "N/iCgUjUdGOnpPgMw4ECQQCboFsTKkzrLOkZJiipgid08xPiBJCfd5Pjl7ggnwoj\n" +
      "CL9JXAsSBvOTvTuo2XnBHOpaWO8oOEUt5xBbUPGLhdaA\n" +
      "-----END RSA PRIVATE KEY-----";
    s.update(string);
    var signature = s.sign(key, "base64");
    return signature;
  }

}

module.exports = ReciboController;
