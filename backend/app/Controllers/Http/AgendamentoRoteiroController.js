'use strict'
const AgendamentoRoteiroRepository = use('App/Repositories/AgendamentoRoteiroRepository')


class AgendamentoRoteiroController {
    #_agendamentoRepo

    constructor() {
      this.#_agendamentoRepo = new AgendamentoRoteiroRepository()
    }

    async index({ response, request }) {
      const page = request.input('page')
      const search = request.input('search')
      const res = await this.#_agendamentoRepo.getAll(page, search)

      return response.ok(res);
    }

    async store({ request }) {
      const data = request.only([
        'data_realizar',
        'rota_run_id'
      ])
      const res = await this.#_agendamentoRepo.create( data )
      return res
    }

    async show({params, response}){
      const res = await this.#_agendamentoRepo.findById(params.id)
      return response.ok(res)
    }

    async update({ params, request }){
      const data = request.only([
        'data_realizar',
        'rota_run_id'
      ])
      const res = await this.#_agendamentoRepo.update( params.id, data )
      return res
    }

    async delete({ params }) {
      const res = await this.#_agendamentoRepo.delete( params.id )
      return res
    }

}

module.exports = AgendamentoRoteiroController
