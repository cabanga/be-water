'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const TipoRamal = use("App/Models/TipoRamal");
const Database = use("Database");
/**
 * Resourceful controller for interacting with tiporamals
 */
class TipoRamalController {
  /**
   * Show a list of all tiporamals.
   * GET tiporamals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_ramals")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_ramals")
        .orWhere("tipo_ramals.descricao", "like", "%" + search + "%")
        .orWhere("tipo_ramals.slug", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    const VerifyTipoRamal = await TipoRamal.query()
      .where("slug", slug)
      .getCount();

    if (VerifyTipoRamal > 0) {
      return DataResponse.response(
        null,
        500,
        "Já existe um semelhante",
        VerifyTipoRamal
      );

    } else {
      const tiporamal = await TipoRamal.create({
        descricao: descricao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const VerifyTipoRamal = await TipoRamal.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (VerifyTipoRamal > 0) {
    return DataResponse.response(
      null,
      500,
      "Já existe um semelhante",
      VerifyTipoRamal
    );
  }else{

    // update with new data entered
    const tiporamal = await TipoRamal.find(params.id);
    tiporamal.merge(data);
    await tiporamal.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tiporamal
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('tipo_ramals')
    .orderBy('tipo_ramals.descricao', 'ASC');
     
    return res;
  }

  /**
   * Render a form to be used for creating a new tiporamal.
   * GET tiporamals/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new tiporamal.
   * POST tiporamals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single tiporamal.
   * GET tiporamals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing tiporamal.
   * GET tiporamals/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update tiporamal details.
   * PUT or PATCH tiporamals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a tiporamal with id.
   * DELETE tiporamals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = TipoRamalController
