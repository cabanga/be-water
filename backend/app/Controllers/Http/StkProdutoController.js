'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Produto = use("App/Models/StkProduto");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with stkprodutos
 */
class StkProdutoController {
  /**
   * Show a list of all stkprodutos.
   * GET stkprodutos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

      res = await Database.select("stk_produtos.id","stk_produtos.descricao", "stk_produtos.status", "stk_produtos.valor", "stk_categoria_produtos.descricao as categoria", "stk_categoria_produtos.id as categoria_id", "stk_produtos.created_at")
        .from("stk_produtos")
        .leftJoin("stk_categoria_produtos", "stk_produtos.categoria_id", "stk_categoria_produtos.id")
        .where(function() {
          if(search != null) {
            this.where("stk_produtos.descricao", "like", "%" + search + "%")          
          }
        })
        .orderBy(orderBy == null ? "stk_produtos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
   

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new stkproduto.
   * GET stkprodutos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new stkproduto.
   * POST stkprodutos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, auth }) {
    const { descricao, categoria_id, status, valor } = request.all();

    await Produto.create({
      descricao: descricao,
      categoria_id: categoria_id,
      valor: valor,
      status: status,
      user_id: auth.user.id
    });
    return DataResponse.response(
      "success",
      200,
      "Registado com sucesso",
      null
    );
  }

  /**
   * Display a single stkproduto.
   * GET stkprodutos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing stkproduto.
   * GET stkprodutos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update stkproduto details.
   * PUT or PATCH stkprodutos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request }) {
    
    const data = request.only(["descricao", "categoria_id", "status", "valor"]);

    const produto = await Produto.find(params.id);
    produto.merge(data);
    await produto.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );
  
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao')
    .from('stk_produtos')
    .where('status', true)
    .orderBy('descricao', 'ASC');
     
    return DataResponse.response("success", 200, "", res);
  }

  async selectBoxMaterialByArmazem ({ request }) {
   
    const { armazem_id } = request.all();

    var res = await Database.select('stk_produtos.id', 'stk_produtos.descricao', 'stk_movimentos.produto_id')
    .from('stk_produtos')
    .innerJoin('stk_movimentos', 'stk_produtos.id', 'stk_movimentos.produto_id')
    .where('stk_produtos.status', true)
    .where('stk_movimentos.armazem_id', armazem_id)
    .groupBy('stk_movimentos.produto_id')
    .orderBy('stk_produtos.descricao', 'ASC');
     
    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Delete a stkproduto with id.
   * DELETE stkprodutos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = StkProdutoController
