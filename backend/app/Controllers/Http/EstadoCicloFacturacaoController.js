'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const EstadoCicloFacturacao = use("App/Models/EstadoCicloFacturacao");
const Database = use("Database");
/**
 * Resourceful controller for interacting with estadociclofacturacaos
 */
class EstadoCicloFacturacaoController {
  /**
   * Show a list of all estadociclofacturacaos.
   * GET estadociclofacturacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("estado_ciclo_facturacaos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("estado_ciclo_facturacaos")
        .where("descricao", "like", "%" + search + "%")
        .orWhere("slug", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new estadociclofacturacao.
   * GET estadociclofacturacaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new estadociclofacturacao.
   * POST estadociclofacturacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    const Verify = await EstadoCicloFacturacao.query()
      .where("slug", slug)
      .getCount();

    if (Verify) {
      return DataResponse.response(
        null,
        500,
        "Esse slug já se encontra registado",
        null
      );

    } else {
      const res = await EstadoCicloFacturacao.create({
        descricao: descricao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }
  /**
   * Display a single estadociclofacturacao.
   * GET estadociclofacturacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing estadociclofacturacao.
   * GET estadociclofacturacaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update estadociclofacturacao details.
   * PUT or PATCH estadociclofacturacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const Verify = await EstadoCicloFacturacao.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (Verify) {
    return DataResponse.response(
      null,
      500,
      "Já existe um semelhante",
      null
    );
  }else{

    // update with new data entered
    const res = await EstadoCicloFacturacao.find(params.id);
    res.merge(data);
    await res.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('estado_ciclo_facturacaos')
    .orderBy('descricao', 'ASC');
        
    return res;
  }

  /**
   * Delete a estadociclofacturacao with id.
   * DELETE estadociclofacturacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = EstadoCicloFacturacaoController
