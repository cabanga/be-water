'use strict'



const DataResponse = use("App/Models/DataResponse");
const TipoMensagem = use("App/Models/TipoMensagem");
const Database = use("Database");
var moment = require("moment");
class TipoMensagemController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("tipo_mensagems")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("tipo_mensagems")
        .orWhere("slug", "like", "%" + search + "%")
        .orWhere("nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { nome, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('tipo_mensagems')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo de Mensagem com este slug",
        Verify
      );

    } else {
      const tipo_mensagem = await Database.table('tipo_mensagems').insert({
        slug: slug,
        nome: nome,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        tipo_mensagem
      );
    }
  }

  async update({ params, request, auth }) {
    const { slug, nome } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('tipo_mensagems')
      .where("slug", slug)
      .whereNot({ id: params.id })
      .getCount(); 

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo de Mensagem com este slug",
        Verify
      );

    } else {
      const tipo_mensagem = await Database.table('tipo_mensagems')
      .where('id', params.id)
      .update({
        slug: slug,
        nome: nome,
        user_id: auth.user.id,
        'user_id': auth.user.id,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        tipo_mensagem
      );
    }
  }

  async selectBox ({ request }) {  
    const res = await Database.select('id', 'nome', 'slug')
    .from('tipo_mensagems')
    .orderBy('nome', 'ASC');
     
    return res;
  }
}

module.exports = TipoMensagemController
