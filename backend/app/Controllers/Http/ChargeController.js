"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const Charge = use("App/Models/Charge");
const LogCharge = use("App/Models/LogCharge");
const Database = use("Database");

/**
 * Resourceful controller for interacting with charges
 */
class ChargeController {
  /**
   * Show a list of all charges.
   * GET charges
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, auth }) {
    const { search, orderBy, pagination,filter} = request.all(); 
    let res = null; 
    var periodo = "" + filter.ano + "" +  filter.mes; 
    res = await Database.select(Database.raw("IF(charges.is_facturado,'Facturado', 'Não Facturado') as facturado"),
        "charges.id", "charges.conta_id", "charges.periodo", "charges.is_facturado",
        "charges.valor", "produtos.nome as invoiceText", "charges.created_at", "clientes.nome as cliente",
        "contas.gestorConta as gestor_conta",  "contas.contaDescricao as conta","charges.facturacao",'rota_headers.descricao as rota')
      .from("charges")
        .leftJoin("contas", "contas.id", "charges.conta_id")
        .leftJoin("clientes", "clientes.id", "contas.cliente_id")
        .leftJoin("local_consumos", "local_consumos.id", "charges.local_consumo_id")
        .leftJoin("servicos", "servicos.id", "charges.servico_id") 
        .leftJoin("produtos", "produtos.id", "charges.produto_id") 
        .leftJoin("rota_headers", "rota_headers.id", "charges.rota_header_id") 
        .whereIn("charges.is_facturado", filter.is_facturado == 'T' || filter.is_facturado == null ? [0,1,2] : [filter.is_facturado])
        //.whereIn("local_consumos.id", filter.chaveServico == null || filter.chaveServico == "" ? Database.select("id").from("local_consumos"): Database.select("id").from("local_consumos").where('chaveServico',filter.chaveServico)) 
        .whereIn("charges.periodo", filter.ano == null && filter.mes == null || filter.ano == "null" && filter.mes == "null" ? Database.select("periodo").from("charges"): Database.select("periodo").from("charges").where("periodo",periodo))
        //.whereIn("clientes.id", search == null && (filter.gestor == 'T' || filter.gestor == null) ? Database.select("id").from("clientes") : search != null && filter.gestor != null ? Database.select("id").from("clientes").where("nome",'like','%'+search+'%').where('gestor_conta', filter.gestor) : filter.gestor != null ? Database.select("id").from("clientes").where('gestor_conta', filter.gestor) : Database.select("id").from("clientes").where("nome",'like','%'+search+'%') )
        .orderBy("charges.is_facturado","charges.periodo", "DESC")
        .paginate(pagination.page==null? 1:pagination.page, pagination.perPage);
     
    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new charge.
   * GET charges/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new charge.
   * POST charges
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   *
  async store({ request, auth }){
    const { mes, ano, flatrate_id, servico_id, conta_id, cliente_id, invoiceText} = request.all();
      
    var periodo = "" + ano + "" +  mes; 
    var moment = require("moment");


    const charges = await Database.select('*').from("charges").where('servico_id', servico_id).where('conta_id', conta_id).where('periodo', periodo).where('invoiceText', invoiceText).where('is_facturado',0)

    if(charges.length > 0){
      return DataResponse.response("error",201,"Já existe essa flatrate", charges);
    } 
   let servico = await Database.select(
    "flat_rate_local_consumos.id", "local_consumos.conta_id",
    "flat_rate_local_consumos.servico_id",
    "flat_rate_local_consumos.valor",  
    Database.raw('DATE_FORMAT(local_consumos.dataEstado, "%Y-%m-%d") as data'),
    Database.raw('DATE_FORMAT(local_consumos.dataEstado, "%Y-%m") as mesAno'),
    Database.raw('DATE_FORMAT(local_consumos.dataEstado, "%d") as dia'),
    "produtos.nome as invoiceText",
    "contas.tipoFacturacao",
    "local_consumos.estado"
    ).from("flat_rate_local_consumos")
    .innerJoin("local_consumos","local_consumos.id", "flat_rate_local_consumos.servico_id")
    .leftJoin("contas", "contas.id", "local_consumos.conta_id") 
    .innerJoin("produtos","produtos.id", "flat_rate_local_consumos.artigo_id") 
    .where('flat_rate_local_consumos.servico_id', servico_id)
    .where('local_consumos.conta_id', conta_id)
    .where('contas.cliente_id', cliente_id)
    .where('flat_rate_local_consumos.id', flatrate_id).first();

    if(servico.tipoFacturacao=='PRE-PAGO'){
      return DataResponse.response("success",500,"Escolhe uma conta POS-PAGO", null);
    }
    if(servico.estado !=1){
      return DataResponse.response("success",500,"Serviço não está activo", null);
    } 
   
    var Current = new Date(ano+"-"+mes + "-01");     
    var ultDia = new Date(Current.getFullYear(), Current.getMonth() + 1, 0); 
    var ultimoDia = moment(ultDia).format("DD");  
   
      const dataFact = "" + ano + "-" +mes;
      var daily = (dataFact == servico.mesAno ? ultimoDia - servico.dia + 1 : ultimoDia) 
      var valor = (servico.valor/ ultimoDia)* daily  

       await Charge.create({  
        invoiceText: servico.invoiceText,
        valor: valor,
        servico_id: servico.servico_id,
        conta_id: servico.conta_id,
        periodo: periodo,
        user_id: auth.user.id,
        daily: daily,
        valorOriginal:  servico.valor
      })  
 
    return DataResponse.response("success",200,"Registo efectuado com sucesso", servico); 

  }*/

  /**
   * Display a single charge.
   * GET charges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing charge.
   * GET charges/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update charge details.
   * PUT or PATCH charges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, auth }) {
    const { charge_id, valor_new, valor_old, observacao } = request.all();

    await Charge.query().where("id", charge_id).update({ valor: valor_new });

    await LogCharge.create({
      valor_new: valor_new,
      valor_old: valor_old,
      charge_id: charge_id,
      observacao: observacao,
      user_id: auth.user.id
    });

    return DataResponse.response("success", 200, "Registo Actualizado com sucesso.",null);

  }

  async anular({ params, request, auth }) {
    const { charge_id, valor_old,observacao } = request.all();

    await Charge.query().where("id", charge_id).update({ is_facturado: 2, observacao: observacao });

    await LogCharge.create({
      valor_new: valor_old,
      valor_old: valor_old,
      charge_id: charge_id,
      observacao: observacao,
      user_id: auth.user.id
    });

    return DataResponse.response("success", 200, "Registo Actualizado com sucesso.",null);

  }

  /**
   * Delete a charge with id.
   * DELETE charges/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = ChargeController;
