'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const LogsCarregamento = use("App/Models/LogsCarregamento");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with logscarregamentos
 */
class LogsCarregamentoController {
  /**
   * Show a list of all logscarregamentos.
   * GET logscarregamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new logscarregamento.
   * GET logscarregamentos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new logscarregamento.
   * POST logscarregamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single logscarregamento.
   * GET logscarregamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing logscarregamento.
   * GET logscarregamentos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update logscarregamento details.
   * PUT or PATCH logscarregamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a logscarregamento with id.
   * DELETE logscarregamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async getCarregamentos({ params }) {

    const carregamentos = await Database.select('created_at','valor','is_carregado', 'factura_id')
      .table('logs_carregamentos')
      .where("ChaveServico", params.id)
      .orderBy('created_at', 'DESC')
      .limit(10)

    return DataResponse.response("success", 200, "", carregamentos);
  }

}

module.exports = LogsCarregamentoController
