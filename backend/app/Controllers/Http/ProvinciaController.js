'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Provincia = use("App/Models/Provincia");
const DataResponse = use("App/Models/DataResponse");
const Database = use('Database');
/**
 * Resourceful controller for interacting with provincias
 */
class ProvinciaController {
  /**
   * Show a list of all provincias.
   * GET provincias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {}


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "provincias.id",
        "provincias.nome",
        "provincias.is_active",
        "provincias.abreviatura",
        "provincias.user_id",
        "users.nome as user",
        "provincias.created_at"
      )
        .from("provincias")
        .innerJoin(
          "users",
          "users.id",
          "provincias.user_id"
        )
        .orderBy(orderBy == null ? "provincias.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "provincias.id",
        "provincias.nome",
        "provincias.is_active",
        "provincias.abreviatura",
        "provincias.user_id",
        "users.nome as user",
        "provincias.created_at"
      )
        .from("provincias")
        .innerJoin(
          "users",
          "users.id",
          "provincias.user_id"
        )
        .orWhere("provincias.nome", "like", "%" + search + "%")
        .orWhere("users.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(provincias.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "provincias.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new provincia.
   * GET provincias/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new provincia.
   * POST provincias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const {
      nome,
      abreviatura,
      user_id
    } = request.all();

    const res = await Provincia.query()
      .where("nome", nome)
      .where("is_active", true)
      .where("abreviatura", abreviatura)
      .getCount();

    if (res > 0) {
      return DataResponse.response(
        null,
        500,
        "Esta rua já existe.",
        null
      );
    } else {
      await Provincia.create({
        nome: nome,
        abreviatura: abreviatura,
        is_active: true,
        user_id: user_id
      });
      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
    }
  }

  /**
   * Display a single provincia.
   * GET provincias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing provincia.
   * GET provincias/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update provincia details.
   * PUT or PATCH provincias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {

    const {
      nome,
      abreviatura,
      is_active,
      user_id
    } = request.all();

    const provincia = await Provincia.find(params.id);

    provincia.merge({
      nome: nome,
      abreviatura: abreviatura,
      is_active: is_active,
      user_id: user_id
    });
    await provincia.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );

  }

  /**
   * Delete a provincia with id.
   * DELETE provincias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}

  async selectBox() {
    let res = await Database.select(
      "provincias.id",
      "provincias.nome",
      "provincias.is_active",
      "provincias.abreviatura"
    )
      .from("provincias")
      .where("provincias.is_active", true)
      .orderBy("provincias.nome", "ASC")
    return DataResponse.response("success", 200, "", res);
  }

  async selectBoxMunicipios() {
    let res = await Database.select(
      "municipios.id",
      "municipios.provincia_id",
      "municipios.nome",
      "municipios.is_active",
    )
      .from("municipios")
      .where("municipios.is_active", 1)
      .orderBy("municipios.nome", "ASC")
    return DataResponse.response("success", 200, "", res);
  }
}

module.exports = ProvinciaController
