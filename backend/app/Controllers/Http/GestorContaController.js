'use strict'
const GestorContaRepository = use('App/Repositories/GestorContaRepository')

class GestorContaController {
    #_gestorRepo

    constructor() {
        this.#_gestorRepo = new GestorContaRepository()
    }

    async index({ response, request }) {
 

        const page = request.input('page')
        const search = request.input('search')
        const gestores = await this.#_gestorRepo.getAll(page, search)

        console.log(gestores);

        return response.ok(gestores);
    }

    async store({ request }) {
        const data = request.only([
            'nome',
            'telefone',
            'email',
            'morada'
        ])
        const res = await this.#_gestorRepo.create( data )
        return res
    }

    async show({params, response}){
        const gender = await this.#_gestorRepo.findById(params.id)
        return response.ok(gender)
    }

    async update({ params, request }){
        const data = request.only([
            'nome',
            'telefone',
            'email',
            'morada'
        ])
        const res = await this.#_gestorRepo.update( params.id, data )
        return res
    }

    async delete({ params }) {
        const res = await this.#_gestorRepo.delete( params.id )
        return res
    }
}

module.exports = GestorContaController
