'use strict'
const DataResponse = use("App/Models/DataResponse");
const ClassePrecisao = use("App/Models/ClassePrecisao");
const Database = use("Database");

class ClassePrecisaoController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("classe_precisaos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("classe_precisaos")
        .orWhere("classe_precisaos.slug", "like", "%" + search + "%")
        .orWhere("classe_precisaos.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    const Verify = await ClassePrecisao.query()
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um semelhante",
        Verify
      );

    } else {
      const classePrecisao = await ClassePrecisao.create({
        descricao: descricao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const Verify = await ClassePrecisao.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (Verify > 0) {
    return DataResponse.response(
      null,
      201,
      "Já existe um semelhante",
      Verify
    );
  }else{

    // update with new data entered
    const classePrecisao = await ClassePrecisao.find(params.id);
    classePrecisao.merge(data);
    await classePrecisao.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      classePrecisao
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('classe_precisaos')
    .orderBy('descricao', 'ASC');
     
    return res;
  }
}

module.exports = ClassePrecisaoController
