'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Provincia = use("App/Models/Provincia");
const Municipio = use("App/Models/Municipio");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage');

/**
 * Resourceful controller for interacting with municipios
 */
class MunicipioController {
  /**
   * Show a list of all municipios.
   * GET municipios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) { }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "municipios.id",
        "municipios.nome",
        "municipios.is_active",
        "municipios.has_distrito",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "municipios.user_id",
        "users.nome as user",
        "municipios.created_at"
      )
        .from("municipios")
        .innerJoin(
          "provincias",
          "provincias.id",
          "municipios.provincia_id"
        )
        .innerJoin(
          "users",
          "users.id",
          "municipios.user_id"
        )
        .orderBy(orderBy == null ? "municipios.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "municipios.id",
        "municipios.nome",
        "municipios.is_active",
        "municipios.has_distrito",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "municipios.user_id",
        "users.nome as user",
        "municipios.created_at"
      )
        .from("municipios")
        .innerJoin(
          "provincias",
          "provincias.id",
          "municipios.provincia_id"
        )
        .innerJoin(
          "users",
          "users.id",
          "municipios.user_id"
        )
        .where("municipios.nome", "like", "%" + search + "%")
        .orWhere("provincias.nome", "like", "%" + search + "%")
        .orWhere("users.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(municipios.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "municipios.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async getMunicipioById({ params, response }) {

    /*
    let res = null;

    res = await Database.select(
      "municipios.id",
      "municipios.nome",
      "municipios.is_active",
      "municipios.has_distrito",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "municipios.user_id",
      "users.nome as user"
    )
      .from("municipios")
      .innerJoin(
        "provincias",
        "provincias.id",
        "municipios.provincia_id"
      )
      .innerJoin(
        "users",
        "users.id",
        "municipios.user_id"
      )
      .where("municipios.id", params.id)


    console.log(params.id)
    console.log(res)
    */
    try {
      const municipio = await Municipio.find(params.id)
      await municipio.load('provincia')

      return response.ok(municipio)
    } catch (error) {
      console.log(error)
    }


  }


  async getMunicipiosByProvincia({ params }) {

    let res = null;

    res = await Database.select(
      "municipios.id",
      "municipios.nome",
      "municipios.is_active",
      "municipios.has_distrito",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "municipios.user_id",
      "users.nome as user",
      "municipios.created_at"
    )
      .from("municipios")
      .innerJoin(
        "provincias",
        "provincias.id",
        "municipios.provincia_id"
      )
      .innerJoin(
        "users",
        "users.id",
        "municipios.user_id"
      )
      .where("municipios.provincia_id", params.id)
      .orderBy("municipios.nome", "ASC");

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new municipio.
   * GET municipios/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) { }

  /**
   * Create/save a new municipio.
   * POST municipios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const {
      nome,
      provincia_id,
      user_id
    } = request.all();

    console.log(request.all());

    const res = await Municipio.query()
      .where("nome", nome)
      .where("provincia_id", provincia_id)
      .getCount();

    if (res > 0) {
      return DataResponse.response(
        null,
        500,
        "Esta município já existe.",
        null
      );
    } else {
      await Municipio.create({
        nome: nome,
        provincia_id: provincia_id,
        is_active: true,
        has_distrito: false,
        user_id: user_id
      });
      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
    }
  }

  /**
   * Display a single municipio.
   * GET municipios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) { }

  /**
   * Render a form to update an existing municipio.
   * GET municipios/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) { }

  /**
   * Update municipio details.
   * PUT or PATCH municipios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {

    const {
      nome,
      provincia_id,
      is_active,
      has_distrito,
      user_id
    } = request.all();

    console.log(request.all());

    const municipio = await Municipio.find(params.id);

    municipio.merge({
      nome: nome,
      provincia_id: provincia_id,
      is_active: is_active,
      has_distrito: has_distrito,
      user_id: user_id
    });
    await municipio.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );
  }

  /**
   * Delete a municipio with id.
   * DELETE municipios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) { }

  async selectBox({ params }) {
    const municipio = await Municipio.query().fetch();
/*     const municipio = await Municipio.query().where("id", params.id).fetch(); */

    return DataResponse.response("success", 200, "", municipio);
  }

  async selectBoxByProvincia({ params }) {

    let res = await Database.select(
      "municipios.id",
      "municipios.nome",
      "municipios.provincia_id",
      "municipios.has_distrito",
      "provincias.nome as provincia"
    )
      .from("municipios")
      .innerJoin(
        "provincias",
        "provincias.id",
        "municipios.provincia_id"
      )
      .where("municipios.provincia_id", params.id)
      .where("municipios.is_active", true)
      .orderBy("municipios.nome", "ASC");

    return res;
  }

}

module.exports = MunicipioController
