'use strict'

const DataResponse = use("App/Models/DataResponse");
const Marca = use("App/Models/Marca");
const Database = use("Database");
var moment = require("moment");

class MarcaController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("marcas")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("marcas")
        .orWhere("marcas.descricao", "like", "%" + search + "%")
        .orWhere("marcas.slug", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('marcas')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Marca com este Slug",
        Verify
      );

    } else {
      const marca = await Database.table('marcas').insert({
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        marca
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao", "slug"]);

    const Verify = await Marca.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um semelhante",
        Verify
      );
    } else {

      // update with new data entered
      const marcas = await Marca.find(params.id);
      marcas.merge(data);
      await marcas.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        marcas
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'descricao', 'slug')
      .from('marcas')
      .orderBy('descricao', 'ASC');

    return res;
  }

  async ModelosbyMarca({ params }) {

    const res = await Database.select('id', 'descricao', 'slug')
      .from('modelos')
      .where('id_marca', params.id)

    return DataResponse.response(
      "success",
      200,
      null,
      res
    );
  }
}


module.exports = MarcaController
