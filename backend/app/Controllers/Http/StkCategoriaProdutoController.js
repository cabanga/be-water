'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Categoria = use("App/Models/StkCategoriaProduto");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with stkcategoriaprodutos
 */
class StkCategoriaProdutoController {
  /**
   * Show a list of all stkcategoriaprodutos.
   * GET stkcategoriaprodutos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request}) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

      res = await Database.select("id","descricao", "created_at")
        .from("stk_categoria_produtos")
        .where(function() {
          if(search != null) {
            this.where("descricao", "like", "%" + search + "%")          
          }
        })
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
   

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new stkcategoriaproduto.
   * GET stkcategoriaprodutos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new stkcategoriaproduto.
   * POST stkcategoriaprodutos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { descricao } = request.only(["descricao"]);
    const categoria = await Categoria.create({
      descricao: descricao,
      user_id: auth.user.id
    });
    return DataResponse.response(
      "success",
      200,
      "Registado com sucesso",
      null
    );
  }

  /**
   * Display a single stkcategoriaproduto.
   * GET stkcategoriaprodutos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing stkcategoriaproduto.
   * GET stkcategoriaprodutos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update stkcategoriaproduto details.
   * PUT or PATCH stkcategoriaprodutos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request }) {
    const data = request.only(["descricao"]);

    const categoria = await Categoria.find(params.id);
    categoria.merge(data);
    await categoria.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );
  }

  async selectOption() {
    const res = await Database.select('id','descricao').from('stk_categoria_produtos');
    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Delete a stkcategoriaproduto with id.
   * DELETE stkcategoriaprodutos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = StkCategoriaProdutoController
