'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const TipoPedido = use("App/Models/TipoPedido");
const Database = use("Database");
/**
 * Resourceful controller for interacting with tipopedidos
 */
class TipoPedidoController {
  /**
   * Show a list of all tipopedidos.
   * GET tipopedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "id",
        "slug",
        "descricao",
        "rota_crm_inicial",
        "created_at"
      )
        .from("tipo_pedidos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "id",
        "slug",
        "descricao",
        "rota_crm_inicial",
        "created_at"
      )
        .from("tipo_pedidos")
        .where("slug", "like", "%" + search + "%")
        .orWhere("descricao", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new tipopedido.
   * GET tipopedidos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  async selectBox() {
    const tipoPedidos = await TipoPedido.all();
    return DataResponse.response("success", 200, "", tipoPedidos);
  }

  /**
   * Create/save a new tipopedido.
   * POST tipopedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { slug, descricao,  rota_crm_inicial} = request.all();
    const tiposPedido = await TipoPedido.query()
      .where("slug", slug)
      .orWhere("descricao", descricao)
      .getCount();
    if (tiposPedido > 0) {
      return DataResponse.response(
        "success",
        500,
        "Slug ou Descrição Já existe..Digite um novo",
        tiposPedido
      );
    } else {
      const tiposPedido = await TipoPedido.create({
        slug: slug,
        descricao: descricao,
        rota_crm_inicial: rota_crm_inicial,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Tipo de registado com sucesso",
        null
      );
    }
  }

  /**
   * Display a single tipopedido.
   * GET tipopedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing tipopedido.
   * GET tipopedidos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update tipopedido details.
   * PUT or PATCH tipopedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["slug","descricao","rota_crm_inicial"]);

    //console.log(data)

    const tiposPedido = await TipoPedido.query()
    .where("slug", data.slug)
    //.orWhere("descricao", data.descricao)
    .whereNot({ id: params.id })
    .getCount();

    //console.log(params.id)
  if (tiposPedido > 0) {
    return DataResponse.response(
      "success",
      500,
      "Já existe Tipo de pedido com mesmo Slug",
      tiposPedido
    );
  }else{

    // update with new data entered
    const tiposPedido = await TipoPedido.find(params.id);
    tiposPedido.merge(data);
    await tiposPedido.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tiposPedido
    );

  }
  }

  /**
   * Delete a tipopedido with id.
   * DELETE tipopedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async rotaTipoPedido({ params }) {

    var slug = params.id.replace("%20", " ");

    const result = await Database.select('*')
      .table('tipo_pedidos')
      .where('slug', slug).first()

     // console.log(result)

    return DataResponse.response("success", 200, "", result);
  }
}

module.exports = TipoPedidoController
