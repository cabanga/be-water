'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Numeracao = use("App/Models/Numeracao");
const LteNumero = use("App/Models/LteNumero");
const CdmaNumero = use("App/Models/CdmaNumero");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with numeracaos
 */
class NumeracaoController {
  /**
   * Show a list of all numeracaos.
   * GET numeracaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "numeracaos.id",
        "numeracaos.numero",
        "numeracaos.tecnologia_id",
        "numeracaos.filial_id",
        "numeracaos.status",
        "filials.nome as filialNome",
        "tecnologias.nome as tecnologiaNome",
        "numeracaos.created_at"
      )
        .from("numeracaos")
        .leftJoin("filials", "filials.id", "numeracaos.filial_id")
        .leftJoin("tecnologias", "tecnologias.id", "numeracaos.tecnologia_id")
        .orderBy(orderBy == null ? "numeracaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "numeracaos.id",
        "numeracaos.numero",
        "numeracaos.tecnologia_id",
        "numeracaos.filial_id",
        "numeracaos.status",
        "filials.nome as filialNome",
        "tecnologias.nome as tecnologiaNome",
        "numeracaos.created_at"
      )
        .from("numeracaos")
        .leftJoin("filials", "filials.id", "numeracaos.filial_id")
        .leftJoin("tecnologias", "tecnologias.id", "numeracaos.tecnologia_id")
        .where("numeracaos.numero", "like", "%" + search + "%")
        .orWhere("tecnologias.nome", "like", "%" + search + "%")
        .orWhere("filials.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(numeracaos.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "numeracaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new numeracao.
   * GET numeracaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new numeracao.
   * POST numeracaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { numero, tecnologia_id, filial_id } = request.all();
    const verifyNumber = await Numeracao.query()
      .where("numero", numero)
      .getCount();
    if (verifyNumber > 0) {
      return DataResponse.response(
        null,
        500,
        "Número já existe",
        null
      );
    } else {
      await Numeracao.create({
        numero: numero,
        tecnologia_id: tecnologia_id,
        filial_id: filial_id,
        user_id: auth.user.id
      });
      return DataResponse.response(
        null,
        200,
        "Registo feito com sucesso",
        null
      );
    }
  }

  /**
   * Display a single numeracao.
   * GET numeracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing numeracao.
   * GET numeracaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update numeracao details.
   * PUT or PATCH numeracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["numero", "tecnologia_id", "filial_id"]);

    const numeracao = await Numeracao.query().where('id', params.id).where('status', true).select('*').first()

    const verifyNumber = await Numeracao.query()
      .where("numero", data.numero)
      .whereNot({ id: params.id })
      .getCount();

    if (numeracao) {
      return DataResponse.response(
        null,
        500,
        "Número já foi disponibilizado",
        null
      );

    } else if (verifyNumber > 0) {
      return DataResponse.response(
        null,
        500,
        "Número já existe",
        null
      );
    } else {

      const numberUpdate = await Numeracao.find(params.id);
      numberUpdate.merge(data);
      await numberUpdate.save();
      return DataResponse.response(
        null,
        200,
        "Dados actualizados com sucesso",
        null
      );

    }
  }

  async disponibilizar({ params, request, auth }) {

    const data = request.only(["numero", "tecnologia"]);

    const numeracao = await Numeracao.query().where('id', params.id).select('*').first()

    if (data.tecnologia == 'CDMA') {

      const cdmaVerify = await CdmaNumero.query().where('chaveServico', numeracao.numero).select('*').first()

      if (!cdmaVerify) {

        await CdmaNumero.create({
          chaveServico: numeracao.numero,
          FilialID: numeracao.filial_id,
          user_id: auth.user.id
        });

        await Database
          .table('numeracaos')
          .where('id', numeracao.id)
          .update('status', true)

        return DataResponse.response("success", 200, "Disponibilizado com sucesso", null);

      } return DataResponse.response(null, 500, "Número já se encontra Registado em CDMA", null);


    } else if (data.tecnologia == 'LTE PRE-PAGO') {

      const lteVerify = await LteNumero.query().where('numero', numeracao.numero).select('*').first()

      if (!lteVerify) {

        await LteNumero.create({
          numero: numeracao.numero,
          filial_id: numeracao.filial_id,
          user_id: auth.user.id
        });

        await Database
          .table('numeracaos')
          .where('id', numeracao.id)
          .update('status', true)

        return DataResponse.response("success", 200, "Disponibilizado com sucesso", null);

      } return DataResponse.response(null, 500, "Número já se encontra Registado em LTE", null);


    }

  }

  /**
   * Delete a numeracao with id.
   * DELETE numeracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = NumeracaoController
