'use strict'
const DataResponse = use("App/Models/DataResponse");
const Medicao = use("App/Models/Medicao");
const Database = use("Database");

class MedicaoController {
 
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("medicaos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("medicaos")
        .orWhere("medicaos.slug", "like", "%" + search + "%")
        .orWhere("medicaos.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    const Verify = await Medicao.query()
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um semelhante",
        Verify
      );

    } else {
      const medicao = await Medicao.create({
        descricao: descricao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const Verify = await Medicao.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (Verify > 0) {
    return DataResponse.response(
      null,
      201,
      "Já existe um semelhante",
      Verify
    );
  }else{

    // update with new data entered
    const medicao = await Medicao.find(params.id);
    medicao.merge(data);
    await medicao.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      medicao
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('medicaos')
    .orderBy('descricao', 'ASC');
     
    return res;
  }
}

module.exports = MedicaoController
