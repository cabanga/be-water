'use strict'
const DataResponse = use("App/Models/DataResponse");
const TiposTipologia = use("App/Models/TiposTipologia");
const Database = use("Database");
var moment = require("moment");

class TiposTipologiaController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "tipos_tipologias.id",
        "tipos_tipologias.descricao",
        "tipos_tipologias.slug",
        "tipos_tipologias.tipologia_id",
        "tipologia_clientes.descricao as tipologia",
      )
        .from("tipos_tipologias")
        .innerJoin("tipologia_clientes", "tipologia_clientes.id", "tipos_tipologias.tipologia_id")
        .orderBy(orderBy == null ? "tipos_tipologias.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "tipos_tipologias.id",
        "tipos_tipologias.descricao",
        "tipos_tipologias.slug",
        "tipos_tipologias.tipologia_id",
        "tipologia_clientes.descricao as tipologia",
      )
        .from("tipos_tipologias")
        .innerJoin("tipologia_clientes", "tipologia_clientes.id", "tipos_tipologias.tipologia_id")
        .orWhere("tipos_tipologias.slug", "like", "%" + search + "%")
        .orWhere("tipos_tipologias.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "tipos_tipologias.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug, tipologia_id} = request.all();

    const Verify = await TiposTipologia.query()
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo de Tipologia com este slug",
        Verify
      );

    } else {
      const tiposTipologias = await TiposTipologia.create({
        descricao: descricao,
        slug: slug,
        tipologia_id: tipologia_id,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug", "tipologia_id"]);

    const Verify = await TiposTipologia.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (Verify > 0) {
    return DataResponse.response(
      null,
      201,
      "Já existe Tipo de Tipologia com este slug",
      Verify
    );
  }else{

    // update with new data entered
    const tiposTipologias = await TiposTipologia.find(params.id);
    tiposTipologias.merge(data);
    await tiposTipologias.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tiposTipologias
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('tipos_tipologias')
    .orderBy('descricao', 'ASC');
     
    return res;
  }
}

module.exports = TiposTipologiaController
