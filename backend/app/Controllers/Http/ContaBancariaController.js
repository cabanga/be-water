'use strict'

const DataResponse = use("App/Models/DataResponse");
const ContaBancaria = use("App/Models/ContaBancaria");
const Database = use("Database");

class ContaBancariaController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "conta_bancarias.id",
        "conta_bancarias.iban",
        "conta_bancarias.municipio_id",
        "municipios.nome as municipio",
        "conta_bancarias.banco_id",
        "conta_bancarias.is_active",
        "bancos.nome as banco"
      )
        .from("conta_bancarias")
        .leftJoin("municipios", "conta_bancarias.municipio_id", "municipios.id")
        .leftJoin("bancos", "conta_bancarias.banco_id", "bancos.id")
        .orderBy(orderBy == null ? "conta_bancarias.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "conta_bancarias.id",
        "conta_bancarias.iban",
        "conta_bancarias.municipio_id",
        "municipios.nome as municipio",
        "conta_bancarias.banco_id",
        "conta_bancarias.is_active",
        "bancos.nome as banco"
      )
        .from("conta_bancarias")
        .leftJoin("municipios", "conta_bancarias.municipio_id", "municipios.id")
        .leftJoin("bancos", "conta_bancarias.banco_id", "bancos.id")
        .orWhere("municipios.nome", "like", "%" + search + "%")
        .orWhere("bancos.nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "conta_bancarias.created_at" : orderBy, "DESC")
        .orderBy(orderBy == null ? "bancos.nome" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { iban, municipio_id, banco_id } = request.all();

/*     const Verify = await ContaBancaria.query()
      .where("nif", nif)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Conta Bancária com este NIF",
        Verify
      );

    } else { */
      const contabancaria = await ContaBancaria.create({
        iban: iban,
        municipio_id: municipio_id,
        banco_id: banco_id,
        is_active: true,
/*         user_id: auth.user.id */
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
  //  }
  }

  async update({ params, request }) {
    const data = request.only([ "iban", "municipio_id", "banco_id"]);

/*     const Verify = await ContaBancaria.query()
      .where("nif", data.nif)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Conta Bancária com este NIF",
        Verify
      );
    } else { */

      // update with new data entered
      const contabancaria = await ContaBancaria.find(params.id);
      contabancaria.merge(data);
      await contabancaria.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        contabancaria
      );
   // }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'iban')
      .from('conta_bancarias')
      .orderBy('iban', 'ASC');

    return res;
  }
}

module.exports = ContaBancariaController
