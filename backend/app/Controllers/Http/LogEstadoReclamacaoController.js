'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const LogEstadoReclamacao = use("App/Models/LogEstadoReclamacao");
const Database = use("Database");
/**
 * Resourceful controller for interacting with logestadoreclamacaos
 */
class LogEstadoReclamacaoController {
  /**
   * Show a list of all logestadoreclamacaos.
   * GET logestadoreclamacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params }) {

    const historicos = await Database.select('log.id','esold.designacao as oldEstado','esnew.designacao as newEstado','log.observacao_old','log.observacao_new', Database.raw('DATE_FORMAT(log.created_at, "%d-%m-%Y %H:%i:%s") as created_at'),'user.nome as operador')
      .table('log_estado_reclamacaos as log')
      .leftJoin('estado_reclamacaos as esold','esold.id','log.id_estado_anterior')
      .leftJoin('estado_reclamacaos as esnew','esnew.id','log.id_estado_novo')
      .innerJoin('users as user','user.id','log.user_id')
      .where('log.reclamacao_id', params.id)
      .orderBy('log.id','ASC')

    return DataResponse.response("success", 200, "", historicos);
  }

  /**
   * Render a form to be used for creating a new logestadoreclamacao.
   * GET logestadoreclamacaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new logestadoreclamacao.
   * POST logestadoreclamacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single logestadoreclamacao.
   * GET logestadoreclamacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing logestadoreclamacao.
   * GET logestadoreclamacaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update logestadoreclamacao details.
   * PUT or PATCH logestadoreclamacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a logestadoreclamacao with id.
   * DELETE logestadoreclamacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = LogEstadoReclamacaoController
