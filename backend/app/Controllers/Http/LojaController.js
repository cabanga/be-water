'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const Loja = use("App/Models/Loja");
const LojaBanco = use("App/Models/LojaBanco");
const Database = use("Database");
/**
 * Resourceful controller for interacting with lojas
 */
class LojaController {
  /**
   * Show a list of all lojas.
   * GET lojas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;
    if (search == null) {
      res = await Database.select(
        "lojas.id",
        "lojas.nome",
        "lojas.numero",
        "lojas.telefone",
        "lojas.email",
        "lojas.endereco",
        "lojas.is_active",
        "lojas.provincia_id",
        "lojas.serie_id",
        "lojas.tipologia_servico_id",
        "lojas.municipio_id",
        "lojas.serie_id_recibo",
        "municipios.nome as municipio",
        "provincias.nome as provincia",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.documento_id",
        "tipologia_servicos.nome as tipologia_servico",
        "lojas.user_id",
        "users.nome as user",
        "lojas.created_at"
      )
        .from("lojas")
        .leftJoin("municipios", "municipios.id", "lojas.municipio_id")
        .leftJoin("provincias", "provincias.id", "lojas.provincia_id")
        .leftJoin("tipologia_servicos", "tipologia_servicos.id", "lojas.tipologia_servico_id")
        .leftJoin("series", "series.id", "lojas.serie_id")
        .leftJoin("documentos", "documentos.id", "series.documento_id")
        .leftJoin("users", "users.id", "lojas.user_id")
        .orderBy("lojas.created_at", "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "lojas.id",
        "lojas.nome",
        "lojas.numero",
        "lojas.telefone",
        "lojas.email",
        "lojas.endereco",
        "lojas.is_active",
        "lojas.provincia_id",
        "lojas.serie_id",
        "lojas.tipologia_servico_id",
        "lojas.municipio_id",
        "lojas.serie_id_recibo",
        "municipios.nome as municipio",
        "provincias.nome as provincia",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla",
        "series.movimento",
        "series.tipo_movimento",
        "series.documento_id",
        "tipologia_servicos.nome as tipologia_servico",
        "lojas.user_id",
        "users.nome as user",
        "lojas.created_at"
      )
        .from("lojas")
        .leftJoin("municipios", "municipios.id", "lojas.municipio_id")
        .leftJoin("provincias", "provincias.id", "lojas.provincia_id")
        .leftJoin("tipologia_servicos", "tipologia_servicos.id", "lojas.tipologia_servico_id")
        .leftJoin("series", "series.id", "lojas.serie_id")
        .leftJoin("documentos", "documentos.id", "series.documento_id")
        .leftJoin("users", "users.id", "lojas.user_id")
        .where("lojas.nome", "like", "%" + search + "%")
        .orWhere("lojas.numero", "like", "%" + search + "%")
        .orWhere("lojas.endereco", "like", "%" + search + "%")
        .orWhere("provincias.nome", "like", "%" + search + "%")
        .orWhere("municipios.nome", "like", "%" + search + "%")

        .orWhere(
          Database.raw('DATE_FORMAT(lojas.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy("lojas.created_at", "DESC")
        .paginate(
          pagination.page == null ? 1 : pagination.page,
          pagination.perPage
        );
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new loja.
   * GET lojas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) { }

  /**
   * Create/save a new loja.
   * POST lojas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response, auth }) {

    let agencia = null;

    const {
      nome,
      numero,
      copiar_agencia,
      tipologia_servico_id,
      agencia_id,
      telefone,
      email,
      endereco,
      municipio_id,
      serie_id,
      serie_id_recibo
    } = request.all();


    if (copiar_agencia) {

      const agencia_copy = await Database.select("*")
        .table("lojas")
        .where("id", agencia_id).first();

      agencia = await Loja.create({
        nome: nome,
        numero: numero,
        telefone: agencia_copy.telefone,
        email: agencia_copy.email,
        endereco: agencia_copy.endereco,
        is_active: true,
        municipio_id: agencia_copy.municipio_id,
        serie_id: agencia_copy.serie_id,
        serie_id_recibo: agencia_copy.serie_id_recibo,
        tipologia_servico_id: tipologia_servico_id,
        user_id: auth.user.id
      });
    }
    else {
      agencia = await Loja.create({
        nome: nome,
        numero: numero,
        telefone: telefone,
        email: email,
        endereco: endereco,
        is_active: true,
        municipio_id: municipio_id,
        serie_id: serie_id,
        serie_id_recibo: serie_id_recibo,
        tipologia_servico_id: tipologia_servico_id,
        user_id: auth.user.id
      });
    }
    return DataResponse.response(
      "success",
      200,
      "Registo Efectuado com sucesso",
      agencia
    );
  }

  /**
   * Display a single loja.
   * GET lojas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) { }

  /**
   * Render a form to update an existing loja.
   * GET lojas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) { }

  /**
   * Update loja details.
   * PUT or PATCH lojas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async update({ params, request, response }) {
    const data = request.only(["nome", "numero", "telefone", "email", "endereco", "is_active", "provincia_id", "municipio_id", "serie_id", "tipologia_servico_id", "serie_id_recibo"]);

    // update with new data entered
    const loja = await Loja.find(params.id);
    loja.merge(data);
    await loja.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      loja
    );
  }

  /**
   * Delete a loja with id.
   * DELETE lojas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) { }

  async selectBox() {
    const lojas = await Loja.all();
    return DataResponse.response("success", 200, "", lojas);
  }
  async selectBoxById({params}) {
    var lojas = await Database.select('*')
    .from('lojas')
    .where('id', params.id)
    .orderBy('nome', 'ASC');

    return DataResponse.response("success", 200, "", lojas);
  }


  async selectBoxAgenciasByTipologiaServico({ params }) {

    var res = await Database.select('id', 'nome', 'numero')
      .from('lojas')
      .where('tipologia_servico_id', params.id)
      .orderBy('nome', 'ASC');

    return res;
  }
  async selectBoxAgenciasById({ params }) {

    var res = await Database.select('id', 'nome', 'numero')
      .from('lojas')
      .where('id', params.id)
      .orderBy('nome', 'ASC');

    return res;
  }


  async addChefeLoja({ request }) {
    const { loja_id, user_chefe_id } = request.all();
    var l = await Loja.query().where("id", loja_id).update({ user_chefe_id: user_chefe_id });
    return DataResponse.response("success", 200, "Registado com sucesso", l);

  }

  async getBancosAssociados({ request, params }) {

    const bancos = await Database.select(
      "loja_bancos.id",
      "bancos.abreviatura as banco",
      "bancos.numero_conta",
      "bancos.iban"
    )
      .from("loja_bancos")
      .leftJoin("bancos", "loja_bancos.banco_id", "bancos.id")
      .where("loja_bancos.loja_id", params.id)

    return DataResponse.response("success", 200, "", bancos);
  }

  async adicionarBancos({ request, auth, params }) {
    const { banco_id } = request.all();

    let loja_id = params.id;

    const Verify = await LojaBanco.query()
      .where("loja_id", loja_id)
      .where("banco_id", banco_id)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Esta Loja não pode ter a mesma conta duas vezes",
        Verify
      );
  } else {
  const loja_bancos = await LojaBanco.create({
    loja_id: loja_id,
    banco_id: banco_id,
    user_id: auth.user.id,
  });
  return DataResponse.response(
    "success",
    200,
    "Registado com sucesso",
    null
  );
}
    }
}

module.exports = LojaController
