'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Saft = use("App/Models/Saft");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database"); 
/**
 * Resourceful controller for interacting with safts
 */
class SaftController {
  /**
   * Show a list of all safts.
   * GET safts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, pagination } = request.all();
    let res = null; 
    if (search == null) {
      res = await Database.select('*').from("log_bill_run_safts") 
      .orderBy("log_bill_run_safts.created_at", "DESC").paginate((pagination.page==null? 1: pagination.page), pagination.perPage);
    } else {
      res = await Database.select('*').from("log_bill_run_safts")  
        .where("log_bill_run_safts.nome_saft", "like", "%" + search + "%") 
        .orWhere(Database.raw('DATE_FORMAT(log_bill_run_safts.created_at, "%Y-%m-%d")'),"like", "%" + search + "%")
        .orderBy("log_bill_run_safts.created_at", "DESC").paginate((pagination.page==null? 1: pagination.page), pagination.perPage);
    } 
    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new saft.
   * GET safts/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new saft.
   * POST safts
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {}

  /**
   * Display a single saft.
   * GET safts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing saft.
   * GET safts/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update saft details.
   * PUT or PATCH safts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a saft with id.
   * DELETE safts/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}

  /**
   * Render a form to update an existing factura.
   * GET facturas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async criarArquivoSaft({ request, auth }) {
    const { ano, de, para } = request.all();
    var r = await Saft.saftXMLDocument(ano, de, para, auth);
    if (r === 500) {
      return DataResponse.response("success",  500,"Nenhum Resultado encotrado.", null);
    } else if (r === 203) {
      return DataResponse.response("success", 500, "Saft em processo.", null);
    }else if (r === 404) {
      var moment = require("moment");
      var CurrentMesDe = moment(ano+"-"+de+"-01").locale('pt-br').format('MMMM');
      var CurrentMesPara = moment(ano+"-"+para+"-30").locale('pt-br').format('MMMM');
      return DataResponse.response("success",  500,"O mes de "+CurrentMesDe+" até "+CurrentMesPara+" de "+ano+" não possuem facturas", null);
    }
    return DataResponse.response("success", 200, "Saft gerado com sucesso!.", null);
  }

  /**
   * Show a list of all focalpoints.
   * GET focalpoints
   *
   * @author caniggia moreira caniggia.moreira@ideiasdinamicas.com
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getStream({ params, response }) {
    const Helpers = use("Helpers");
    const LogBillRunSaft = use("App/Models/LogBillRunSaft");
    const anexo = await LogBillRunSaft.find(params.id);
    //return file as stream
    response.attachment(
      Helpers.tmpPath("./upload/safts/" + anexo.nome_saft + ".xml")
    );
  }
}

module.exports = SaftController
