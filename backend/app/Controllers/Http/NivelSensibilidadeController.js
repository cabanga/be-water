'use strict'
const DataResponse = use("App/Models/DataResponse");
const NivelSensibilidade = use("App/Models/NivelSensibilidade");
const Database = use("Database");
var moment = require("moment");

class NivelSensibilidadeController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("nivel_sensibilidades")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("nivel_sensibilidades")
        .orWhere("nivel_sensibilidades.slug", "like", "%" + search + "%")
        .orWhere("nivel_sensibilidades.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('nivel_sensibilidades')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Nível de Sensibilidade com este slug",
        Verify
      );

    } else {
      const NivelSensibilidade = await Database.table('nivel_sensibilidades').insert({
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        NivelSensibilidade
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao", "slug"]);

    const Verify = await NivelSensibilidade.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Nível de Sensibilidade com este slug",
        Verify
      );
    } else {

      // update with new data entered
      const nivel_sensibilidades = await NivelSensibilidade.find(params.id);
      nivel_sensibilidades.merge(data);
      await nivel_sensibilidades.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        nivel_sensibilidades
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'descricao', 'slug')
      .from('nivel_sensibilidades')
      .orderBy('descricao', 'ASC');

    return res;
  }
}

module.exports = NivelSensibilidadeController
