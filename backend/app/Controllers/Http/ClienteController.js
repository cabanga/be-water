'use strict'

const {
  validate
} = use('Validator')

const Cliente = use("App/Models/Cliente");
const TipoIdentidade = use("App/Models/TipoIdentidade");
const Direccao = use("App/Models/Direccao");
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');
const Mensagem = use('App/Models/ResponseMessage')

const dataClientAgt = null
const ClienteRepository = use('App/Repositories/ClienteRepository')


class ClienteController {
  #_clienteRepo

  constructor() {
    this.#_clienteRepo = new ClienteRepository()
  }

  async index({ request }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    const res = await this.#_clienteRepo.getAll(search, orderBy, pagination)

    /*
    let res = await Database.select(
      'clientes.*',
      "municipios.nome as municipio_nome",
      "municipios.provincia_id",
      "generos.descricao as genero",
      "tipo_clientes.tipoClienteDesc as tipo_cliente",
      "tipo_clientes.slug as tipo_cliente_slug",
    )
    .table("clientes")
    .leftJoin("generos",        "generos.id",       "clientes.genero_id")
    .leftJoin("municipios",     "municipios.id",    "clientes.municipio_id")
    .leftJoin("tipo_clientes",  "tipo_clientes.id", "clientes.tipo_cliente_id")
    .where(function () {
      if (search != null) {
        this.where("clientes.id", "like", "%" + search + "%")
        this.orWhere('clientes.nome', 'like', '%' + search + '%')
        this.orWhere('clientes.telefone', 'like', '%' + search + '%')
        this.orWhere('clientes.email', 'like', '%' + search + '%')
      }
    })
    .orderBy(orderBy == null ? "clientes.nome" : orderBy, "ASC")
    .paginate(pagination.page,pagination.perPage);
    */


    return DataResponse.response("success", 200, "", res);
  }


  async searchCliente({ request }) {
    const { start, end, search } = request.all();


    let count = (search != null) ? 100 : end;

      //console.log(count);

    let res = await Database.select(
      "clientes.id",
      "clientes.nome",
      "clientes.telefone",
      "clientes.email",
      "clientes.genero_id",
      "generos.descricao as genero",
      "clientes.municipio_id",
      "municipios.nome as municipio",
      "clientes.morada",
      "clientes.tipo_identidade_id",
      "tipo_identidades.nome as tipo_identidade",
      "tipo_identidades.numero_digitos",
      "clientes.numero_identificacao",
      "clientes.tipo_cliente_id",
      "tipo_clientes.tipoClienteDesc as tipo_cliente",
      "tipo_clientes.slug as tipo_cliente_slug",
      "clientes.direccao_id",
      "direccaos.designacao as direccao",
      "clientes.gestor_cliente_id",
      "gestor_clientes.nome as gestor_cliente",
      "gestor_clientes.telefone as gestor_cliente_telefone",
      "clientes.is_active",
      "clientes.created_at",
      "clientes.updated_at"
    )
      .from("clientes")
      .leftJoin("generos", "generos.id", "clientes.genero_id")
      .leftJoin("municipios", "municipios.id", "clientes.municipio_id")
      .leftJoin("tipo_identidades", "tipo_identidades.id", "clientes.tipo_identidade_id")
      .leftJoin("tipo_clientes", "tipo_clientes.id", "clientes.tipo_cliente_id")
      .leftJoin("direccaos", "direccaos.id", "clientes.direccao_id")
      .leftJoin("users as gestor_clientes", "gestor_clientes.id", "clientes.gestor_cliente_id")

      .where(function name() {
        if (search != null) {
          this.where("clientes.nome", "like", "%" + search + "%")
          this.orWhere("clientes.numero_identificacao", "like", "%" + search + "%")
        }
      })

      .orderBy("clientes.nome", "ASC")
      .paginate(start, count);


    return DataResponse.response("success", 200, null, res);
  }


  async getClienteByConta({ params, response }) {

    let res = await Database.select(
      "clientes.id",
      "contas.id as conta_id",
      "contas.numero_conta",
      "clientes.nome",
      "clientes.telefone",
      "clientes.email",
      "clientes.genero_id",
      "generos.descricao as genero",
      "clientes.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "clientes.morada",
      "clientes.tipo_identidade_id",
      "tipo_identidades.nome as tipo_identidade",
      "tipo_identidades.numero_digitos",
      "clientes.numero_identificacao",
      "clientes.tipo_cliente_id",
      "tipo_clientes.tipoClienteDesc as tipo_cliente",
      "tipo_clientes.slug as tipo_cliente_slug",
      "clientes.direccao_id",
      "direccaos.designacao as direccao",
      "clientes.gestor_cliente_id",
      "gestor_clientes.nome as gestor_cliente",
      "gestor_clientes.telefone as gestor_cliente_telefone",
      "clientes.is_active",
      "clientes.created_at",
      "clientes.updated_at"
    )
      .from("clientes")
      .innerJoin("contas", "contas.cliente_id", "clientes.id")
      .innerJoin("generos", "generos.id", "clientes.genero_id")
      .leftJoin("municipios", "municipios.id", "clientes.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
      .leftJoin("tipo_identidades", "tipo_identidades.id", "clientes.tipo_identidade_id")
      .leftJoin("tipo_clientes", "tipo_clientes.id", "clientes.tipo_cliente_id")
      .leftJoin("direccaos", "direccaos.id", "clientes.direccao_id")
      .leftJoin("users as gestor_clientes", "gestor_clientes.id", "clientes.gestor_cliente_id")
      .where("contas.id", params.id)
      .first();

    return res;
  }


  async getClienteById({ params, response }) {

    let cliente = null;
    let contas = null;

    cliente = await Database.select(
      "clientes.id",
      "clientes.nome",
      "clientes.telefone",
      "clientes.email",
      "clientes.genero_id",
      "generos.descricao as genero",
      "clientes.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "clientes.morada",
      "clientes.tipo_identidade_id",
      "tipo_identidades.nome as tipo_identidade",
      "tipo_identidades.numero_digitos",
      "clientes.numero_identificacao",
      "clientes.tipo_cliente_id",
      "tipo_clientes.tipoClienteDesc as tipo_cliente",
      "tipo_clientes.slug as tipo_cliente_slug",
      "clientes.direccao_id",
      "direccaos.designacao as direccao",
      "clientes.gestor_cliente_id",
      "gestor_clientes.nome as gestor_cliente",
      "gestor_clientes.telefone as gestor_cliente_telefone",
      "clientes.is_active",
      "clientes.created_at",
      "clientes.updated_at"
    )
      .from("clientes")
      .innerJoin("generos", "generos.id", "clientes.genero_id")
      .leftJoin("municipios", "municipios.id", "clientes.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
      .leftJoin("tipo_identidades", "tipo_identidades.id", "clientes.tipo_identidade_id")
      .leftJoin("tipo_clientes", "tipo_clientes.id", "clientes.tipo_cliente_id")
      .leftJoin("direccaos", "direccaos.id", "clientes.direccao_id")
      .leftJoin("users as gestor_clientes", "gestor_clientes.id", "clientes.gestor_cliente_id")
      .where("clientes.id", params.id)
      .first();

    if (cliente) {
      contas = await Database.select(
        "co.id as conta_id",
        "co.contaDescricao as conta"
      )
        .from("contas as co")
        .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
        .where("co.cliente_id", cliente.id);

      const data = {
        id: cliente.id,
        nome: cliente.nome,
        telefone: cliente.telefone,
        email: cliente.email,
        genero_id: cliente.genero_id,
        genero: cliente.genero,
        municipio_id: cliente.municipio_id,
        municipio: cliente.municipio,
        morada: cliente.morada,
        tipo_identidade_id: cliente.tipo_identidade_id,
        tipo_identidade: cliente.tipo_identidade,
        numero_digitos: cliente.numero_digitos,
        numero_identificacao: cliente.numero_identificacao,
        tipo_cliente_id: cliente.tipo_cliente_id,
        tipo_cliente: cliente.tipo_cliente,
        direccao_id: cliente.direccao_id,
        direccao: cliente.direccao,
        gestor_cliente_id: cliente.gestor_cliente_id,
        gestor_cliente: cliente.gestor_cliente,
        gestor_cliente_telefone: cliente.gestor_cliente_telefone,
        is_active: cliente.is_active,
        created_at: cliente.created_at,
        updated_at: cliente.updated_at
      };

      return data;
    }

    return null;
  }


  async searchClienteTelefone({ request }) {
    const { chaveServico } = request.all();

    let res = null;

    res = await Database.select("clientes.nome", "clientes.id")
      .from("clientes")
      .innerJoin("contas", "contas.cliente_id", "clientes.id")
      .innerJoin("servicos", "servicos.conta_id", "contas.id")
      .where("servicos.chaveServico", chaveServico)
      .first();
    if (res == null) {
      return DataResponse.response("success", 500, "Nenhum resultado encontrado", res);
    }

    return DataResponse.response("success", 200, null, res);
  }


  async store({ request, auth }) {
    const {
      nome,
      telefone,
      email,
      genero_id,
      municipio_id,
      morada,
      tipo_identidade_id,
      numero_identificacao,
      tipo_cliente_id,
      gestor_cliente_id
    } = request.all();


    var identificacao = await Cliente.query().where("numero_identificacao", numero_identificacao)
      .select("*")
      .first();

    const tipo_identidade = await Database
    .select("*")
    .from("tipo_identidades")
    .where("id", tipo_identidade_id)
    .first();

    if(tipo_identidade.nome != 'Desconhecido'){
      if (identificacao != null){
        return DataResponse.response("success", 201, "Aviso: Já existe um cliente com este número de identificação!", null)
      }
    }

     /*
    if (tipo_identidade.numero_digitos != numero_identificacao.length){
      return DataResponse.response("warning", 201, "Aviso: O número de identificação não está correcto! deve possuir " + tipo_identidade.numero_digitos + " caracteres", null)
    }
    */


    //const cliente = await Cliente.create({ user_id: auth.user.id, ...data });

    const cliente = await Cliente.create({
      nome: nome,
      telefone: telefone,
      email: email,
      genero_id: genero_id,
      municipio_id: municipio_id,
      morada: morada,
      tipo_identidade_id: tipo_identidade_id,
      numero_identificacao: numero_identificacao,
      tipo_cliente_id: tipo_cliente_id,
      gestor_cliente_id: gestor_cliente_id,
      is_active: true,
      user_id: auth.user.id
    });

    return DataResponse.response(
      "success", 200, "Cliente registado com sucesso" + (cliente == null ? '.' : ', cliente: ' + cliente.nome), cliente
    );
  }

  async show({ request }) {
    const { numero_identificacao } = request.all();
    const cliente = await Database.select(
      "id",
      "nome",
      "telefone",
      "morada",
      "numero_identificacao"
    )
      .from("clientes")
      .where("numero_identificacao", numero_identificacao);
    return cliente;
  }


  async edit({ params, request, response, view }) { }


  async update({ params, request, response }) {
    const { numero_identificacao } = request.all();
    const data = request.only([
      "nome",
      "telefone",
      "email",
      "genero_id",
      "municipio_id",
      "morada",
      "tipo_identidade_id",
      "numero_identificacao",
      "tipo_cliente_id",
      "gestor_cliente_id"
    ]);


    /*const validation = await validate(data, Cliente.rules);
    if (validation.fails()) {
      return DataResponse.response("error", 500, validation.messages()[0].message + " " + validation.messages()[0].field, data);
    }*/

    var cliente = null;
    var telefone = null;
    var email = null;

    cliente = await Cliente.query()
      .where("numero_identificacao", numero_identificacao)
      .whereNot("id", params.id)
      .whereNot("numero_identificacao", "999999999")
      .select("*")
      .first();

    /*
  telefone = await Cliente.query()
    .where("telefone", data.telefone)
    .whereNot("id", params.id)
    .select("telefone")
    .first();

  email = await Cliente.query()
    .where("email", data.email)
    .whereNot("email", null)
    .whereNot("id", params.id)
    .select("email")
    .first();

    */
    /*
        if (cliente != null && numero_identificacao != "999999999") {
          const tid = await TipoIdentidade.query().where("id", data.tipo_identidade_id).select("*").first();
          return DataResponse.response("success", 201, "Aviso: Já existe um cliente com este número de " + tid.tipoIdentificacao, null);
        } else if (telefone != null) {

          return DataResponse.response("success", 201, "Aviso: Já existe um cliente com este número de telefone", null);
        } else if (email != null) {

          return DataResponse.response("success", 201, "Aviso: Já existe um cliente com este email", null);
        }
        // update with new data entered
        const direccao = await Direccao.query().where("id", data.direccao_id).select("*").first();
        data.direccao = direccao.slug;*/
    const client = await Cliente.find(params.id);
    client.merge(data);
    await client.save();

    return DataResponse.response(
      "success", 200, "Dados actualizados com sucesso" + (cliente == null ? '.' : ', cliente: ' + cliente.nome), cliente
    );

  }

  async destroy({ params, request, response }) { }

  //Busca Cliente para Mudança de Titularidade ou Conta
  async getCliente({ params, response }) {
    let cliente = null;
    let contas = null;
    cliente = await Cliente.query()
      .where("telefone", params.id)
      .orWhere("numero_identificacao", params.id)
      .orWhere("email", params.id)
      .select("*")
      .first();

    if (cliente) {
      contas = await Database.select(
        "co.id as contaID",
        "co.contaDescricao as contaDescricao"
      )
        .from("contas as co")
        .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
        .where("co.cliente_id", cliente.id);

      let data = {
        cliente: cliente,
        contas: contas
      };

      return response
        .status(200)
        .send(Mensagem.response(response.response.statusCode, "success", data));
    }

    return response
      .status(404)
      .send(
        Mensagem.response(response.response.statusCode, "Não encontrado!", null)
      );
  }

  async serviceAGT({ params }) {
    const axios = require("axios");
    const rota =
      "http://www.agt.minfin.gov.ao/servico-comum/api/publico/comum/PortalAGT/consultar-nif/";

    let clientAgt = await axios
      .post(rota, { ivNuContribuinte: params.id })
      .then(data => {
        return data;
      });

    if (clientAgt.data.quantidadeTotalItens >= 1) {
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        clientAgt.data.data[0]
      );
    } else {
      return DataResponse.response(
        "success",
        404,
        "Nenhum dado encontrado",
        null
      );
    }
  }

  async clienteAll({ request, view, response, auth }) {
    let res = null;
    res = await Database.select("*").from("clientes");
    return DataResponse.response("success", 200, "", res);
  }

  async createParceriaCliente({ request, auth }) {
    const {
      codigo,
      cliente_id,
      moeda_id,
      estado,
      tarifario_id,
      chaveServico,
      trunk_in,
      trunk_out,
      origem,
      destino,
      imposto_id
    } = request.all();

    const Conta = use("App/Models/Conta");
    const Servico = use("App/Models/Servico");
    const ParceriaCliente = use("App/Models/ParceriaCliente");
    const FlatRateServico = use("App/Models/FlatRateServico");
    var moment = require("moment");

    var pc = null;

    pc = await ParceriaCliente.query()
      .where("codigo_interconexao", codigo)
      .select("*")
      .first();

    if (pc != null) {
      return DataResponse.response(
        "success",
        500,
        "Código de interconexão já usado.",
        null
      );
    }

    var cliente = null;

    cliente = await ParceriaCliente.query()
      .where("cliente_id", cliente_id)
      .select("*")
      .first();

    if (cliente != null) {
      return DataResponse.response(
        "success",
        500,
        "Já existe uma parceria para este cliente",
        null
      );
    }
    /*
        const cliente = await Cliente.query()
          .where("id", cliente_id)
          .select("*")
          .first();

        const tecnologia = await Database.select("*")
          .from("tecnologias")
          .where("nome", "INTERCONEXAO")
          .first();
        var dataActual = moment(new Date()).format("YYYY-MM-DD");

        const conta = await Conta.create({
          contaDescricao: cliente.nome + " - Parceria Interconexão",
          cliente_id: cliente.id,
          moeda_id: moeda_id,
          agencia_id: auth.user.loja_id == null ? null : auth.user.loja_id,
          estado: 1,
          dataEstado: dataActual,
          tipoFacturacao: "POS-PAGO",
          user_id: auth.user.id
        });

        const servico = await Servico.create({
          chaveServico: chaveServico,
          conta_id: conta.id,
          estado: 1,
          dataEstado: dataActual,
          tarifario_id: tarifario_id,
          tecnologia_id: tecnologia.id,
          user_id: auth.user.id
        });

        const flatRateServico = await FlatRateServico.create({
          servico_id: servico.id,
          valor: 0,
          moeda_id: moeda_id,
          imposto_id: imposto_id,
          origem: origem,
          destino: destino,
          user_id: auth.user.id
        });

        const parceriaCliente = await ParceriaCliente.create({
          cliente_id: cliente.id,
          trunk_in: trunk_in,
          trunk_out: trunk_out,
          moeda_id: moeda_id,
          codigo_interconexao: codigo,
          estado: estado,
          imposto_id: imposto_id,
          servico_id: servico.id,
          user_id: auth.user.id
        }); */

    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso.",
      parceriaCliente
    );
  }

  async direccaosSelectBox({ request, view, response, auth }) {
    let direccaos = await Cliente.query().whereNotNull("direccao").distinct('direccao').fetch()
    return DataResponse.response("success", 200, null, direccaos);
  }


  async gestoresSelectBox({ request, view, response, auth }) {
    let direccaos = await Cliente.query().whereNotNull("gestor_conta").distinct('gestor_conta').fetch()
      console.log(direccaos)
    return DataResponse.response("success", 200, null, direccaos);
  }


  async searchClienteFacturaEmail({ request }) {
    const { start, end, search, filtros } = request.all();
    let res = null;

    if (filtros != null) {

      res = await Database.select(
        "clientes.id",
        "clientes.nome",
        "clientes.telefone",
        "clientes.morada",
        "clientes.email"
      )
        .from("clientes")
        .innerJoin("facturas", "facturas.cliente_id", "clientes.id")/*
        .innerJoin("moedas", "facturas.moeda_id", "moedas.id") */
         .where("facturas.status", "N")/*
      /* .whereIn("clientes.direccao_id", filtros.direccao == 'T' || filtros.direccao == null ? Database.select("designacao").from("direccaos") : [filtros.direccao]) */
        .where("clientes.nome", "like", "%" + search + "%")
        .groupBy('clientes.id', 'clientes.nome', 'clientes.telefone', 'clientes.morada')
        .orderBy('clientes.nome', 'asc')
        .paginate(start, end);
    } else {

      res = await Database.select(
        "clientes.id",
        "nome",
        "telefone",
        "morada",
        "email"
      )
        .from("clientes")
        .innerJoin("facturas", "facturas.cliente_id", "clientes.id")
        .where("nome", "like", "%" + search + "%")
        .orWhere("clientes.id", "like", "%" + search + "%")
        .orWhere("telefone", "like", "%" + search + "%")
        .groupBy('clientes.id', 'nome', 'telefone', 'morada')
        .paginate(start, end);

    }

    return DataResponse.response("success", 200, null, res);
  }
}

module.exports = ClienteController
