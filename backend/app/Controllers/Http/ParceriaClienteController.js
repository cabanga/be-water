'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const ParceriaCliente = use("App/Models/ParceriaCliente"); 
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Mensagem = use("App/Models/ResponseMessage");

/**
 * Resourceful controller for interacting with parceriaclientes
 */
class ParceriaClienteController {
  /**
   * Show a list of all parceriaclientes.
   * GET parceriaclientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;
    const unificado='nao'

    if (search == null) {
      res = await Database.select("parceria_clientes.id","clientes.nome",'parceria_clientes.codigo_interconexao','parceria_clientes.trunk_in','parceria_clientes.trunk_out','moedas.nome as moeda').from("parceria_clientes")
      .innerJoin('clientes','clientes.id','parceria_clientes.cliente_id')
      .leftJoin('moedas','moedas.id','parceria_clientes.moeda_id')
      .orderBy(orderBy == null ? "parceria_clientes.created_at" : orderBy, "DESC").paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select("parceria_clientes.id", "clientes.nome",'parceria_clientes.codigo_interconexao','parceria_clientes.trunk_in','parceria_clientes.trunk_out','moedas.nome as moeda')
        .from("parceria_clientes").innerJoin("clientes", "clientes.id", "parceria_clientes.cliente_id")
        .leftJoin("moedas", "moedas.id", "parceria_clientes.moeda_id")
        .where("clientes.nome", "like", "%" + search + "%")
        .orWhere("clientes.contribuente", "like", "%" + search + "%")
        .orWhere("clientes.id", "like", "%" + search + "%")
        .orWhere("clientes.numeroExterno", "like", "%" + search + "%")
        .orWhere("clientes.morada", "like", "%" + search + "%")
        .orWhere("clientes.telefone", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(parceria_clientes.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(
          orderBy == null ? "parceria_clientes.created_at" : orderBy,
          "DESC"
        )
        .paginate(
          pagination.page == null ? 1 : pagination.page,
          pagination.perPage
        );
    }

    return DataResponse.response("success", 200, null, res);
  }

  /**
   * Render a form to be used for creating a new parceriacliente.
   * GET parceriaclientes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new parceriacliente.
   * POST parceriaclientes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single parceriacliente.
   * GET parceriaclientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing parceriacliente.
   * GET parceriaclientes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update parceriacliente details.
   * PUT or PATCH parceriaclientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a parceriacliente with id.
   * DELETE parceriaclientes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ParceriaClienteController
