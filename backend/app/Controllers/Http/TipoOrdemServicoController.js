'use strict'
const DataResponse = use("App/Models/DataResponse");
const TipoOrdemServico = use("App/Models/TipoOrdemServico");
const Database = use("Database");
var moment = require("moment");


class TipoOrdemServicoController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_ordem_servicos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_ordem_servicos")
        .where("tipo_ordem_servicos.slug", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");
    
    const Verify = await Database.table('tipo_ordem_servicos')
      .where("slug", slug)
      .getCount(); 

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo Ordem Serviço com este slug",
        Verify
      );

    } else {
      const tipo_ordem_servicos = await Database.table('tipo_ordem_servicos').insert({
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        tipo_ordem_servicos
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const Verify = await TipoOrdemServico.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (Verify > 0) {
    return DataResponse.response(
      null,
      201,
      "Já existe Tipo Ordem Serviço com este slug",
      Verify
    );
  }else{

    // update with new data entered
    const tipo_ordem_servicos = await TipoOrdemServico.find(params.id);
    tipo_ordem_servicos.merge(data);
    await tipo_ordem_servicos.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tipo_ordem_servicos
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('tipo_ordem_servicos')
    .orderBy('descricao', 'ASC');
        
/*     return DataResponse.response(
      null,
      200,
      res
      ); */
      return res;
  }
}

module.exports = TipoOrdemServicoController
