'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const EstadoServico = use("App/Models/EstadoServico");
const Database = use("Database");

/**
 * Resourceful controller for interacting with estadoservicos
 */
class EstadoServicoController {
  /**
   * Show a list of all estadoservicos.
   * GET estadoservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async Listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("estado_servicos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("estado_servicos")
        .orWhere("estado_servicos.nome", "like", "%" + search + "%")
        .orWhere("estado_servicos.slug", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async AdicionarServico({ request, auth }) {
    const { nome, slug} = request.all();

    const VerifyEstadoServico = await EstadoServico.query()
      .where("slug", slug)
      .getCount();

    if (VerifyEstadoServico > 0) {
      return DataResponse.response(
        null,
        500,
        "Já existe um semelhante",
        VerifyEstadoServico
      );

    } else {
      const estadoservico = await EstadoServico.create({
        nome: nome,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async EditarServico({ params, request }) {
    const data = request.only(["nome","slug"]);

    const VerifyEstadoServico = await EstadoServico.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (VerifyEstadoServico > 0) {
    return DataResponse.response(
      null,
      500,
      "Já existe um semelhante",
      VerifyEstadoServico
    );
  }else{

    // update with new data entered
    const estadoservico = await EstadoServico.find(params.id);
    estadoservico.merge(data);
    await estadoservico.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      estadoservico
      );
    }
  }

  async index({ request, response, view }) {}

  /**
   * Render a form to be used for creating a new estadoservico.
   * GET estadoservicos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new estadoservico.
   * POST estadoservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {}

  /**
   * Display a single estadoservico.
   * GET estadoservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing estadoservico.
   * GET estadoservicos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update estadoservico details.
   * PUT or PATCH estadoservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a estadoservico with id.
   * DELETE estadoservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}

  async selectBox() {
    const estadoServico = await EstadoServico.all();
    return DataResponse.response("success", 200, "", estadoServico);
  }

}

module.exports = EstadoServicoController
