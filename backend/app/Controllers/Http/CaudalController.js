'use strict'

const DataResponse = use("App/Models/DataResponse");
const Caudal = use("App/Models/Caudal");
const Database = use("Database");
var moment = require("moment");

class CaudalController {
 
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("caudals")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("caudals")
        .orWhere("caudals.descricao", "like", "%" + search + "%")
        .orWhere("caudals.slug", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('caudals')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Caudal com este Slug",
        Verify
      );

    } else {
      const Caudal = await Database.table('caudals').insert({
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        Caudal
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao", "slug"]);

    const Verify = await Caudal.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Caudal com este Slug",
        Verify
      );
    } else {

      // update with new data entered
      const caudals = await Caudal.find(params.id);
      caudals.merge(data);
      await caudals.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        caudals
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'descricao', 'slug')
      .from('caudals')
      .orderBy('descricao', 'ASC');

    return res;
  }
}

module.exports = CaudalController
