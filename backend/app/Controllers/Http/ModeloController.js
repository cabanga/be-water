'use strict'
const O = use("App/Models/O");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");

class ModeloController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "modelos.id",
        "modelos.descricao",
        "modelos.slug",
        "modelos.id_marca",
        "marcas.descricao as marca"
      )
        .from("modelos")
        .leftJoin("marcas", "marcas.id", "modelos.id_marca")
        .orderBy(orderBy == null ? "modelos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "modelos.id",
        "modelos.descricao",
        "modelos.slug",
        "modelos.id_marca",
        "marcas.descricao as marca"
      )
        .from("modelos")
        .leftJoin("marcas", "marcas.id", "modelos.id_marca")
        .orWhere("modelos.slug", "like", "%" + search + "%")
        .orWhere("modelos.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "modelos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug, id_marca } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");
    
    const Verify = await Database.table('modelos')
      .where("slug", slug)
      .getCount(); 

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Marca com este Slug",
        Verify
      );

    } else {
      const modelos = await Database.table('modelos').insert({
        id_marca: id_marca,
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        modelos
      );
    }
  }

  async update({ params, request, auth }) {
    const { id, slug, descricao, id_marca } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('modelos')
      .where("slug", slug)
      .whereNot({ id: params.id })
      .getCount(); 

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Modelo com este slug",
        Verify
      );

    } else {
      const modelo = await Database.table('modelos')
      .where('id', id)
      .update({
        descricao: descricao,
        slug: slug,
        id_marca: id_marca,
        user_id: auth.user.id,
        'user_id': auth.user.id,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        modelo
      );
    }
  }

  async selectBox ({ request, params }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('modelos')
    .orderBy('descricao', 'ASC');
     
    return res;
  }

  async ModelobyId ({ params }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('modelos')
    .where('id', params.id)
   
    return DataResponse.response(
      "success",
      200,
      null,
      res
    );
/*    return res; */
  }
}

module.exports = ModeloController
