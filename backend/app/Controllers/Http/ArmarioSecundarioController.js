'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const ArmarioSecundario = use('App/Models/ArmarioSecundario');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database'); 
/**
 * Resourceful controller for interacting with armariosecundarios
 */
class ArmarioSecundarioController {
  /**
   * Show a list of all armariosecundarios.
   * GET armariosecundarios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new armariosecundario.
   * GET armariosecundarios/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new armariosecundario.
   * POST armariosecundarios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { descricao, central_id } = request.all(); 
 
      const res = await ArmarioSecundario.create({
        descricao: descricao,
        central_id: central_id,
        user_id: auth.user.id
      });
      return DataResponse.response("success", 200, "Registo efectuado com sucesso", res);
    
  }

  /**
   * Display a single armariosecundario.
   * GET armariosecundarios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing armariosecundario.
   * GET armariosecundarios/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update armariosecundario details.
   * PUT or PATCH armariosecundarios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a armariosecundario with id.
   * DELETE armariosecundarios/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ArmarioSecundarioController
