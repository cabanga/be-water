'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const TipoContrato = use("App/Models/TipoContrato");
const Database = use("Database");
/**
 * Resourceful controller for interacting with tipocontratoes
 */
class TipoContratoController {
  /**
   * Show a list of all tipocontratoes.
   * GET tipocontratoes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_contratoes")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_contratoes")
        .orWhere("tipo_contratoes.slug", "like", "%" + search + "%")
        .orWhere("tipo_contratoes.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    const VerifyTipoContrato = await TipoContrato.query()
      .where("slug", slug)
      .getCount();

    if (VerifyTipoContrato > 0) {
      return DataResponse.response(
        null,
        500,
        "Já existe um semelhante",
        VerifyTipoContrato
      );

    } else {
      const tipocontrato = await TipoContrato.create({
        descricao: descricao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const VerifyTipoContrato = await TipoContrato.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (VerifyTipoContrato > 0) {
    return DataResponse.response(
      null,
      500,
      "Já existe um semelhante",
      VerifyTipoContrato
    );
  }else{

    // update with new data entered
    const tipocontrato = await TipoContrato.find(params.id);
    tipocontrato.merge(data);
    await tipocontrato.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tipocontrato
      );
    }
  }



  async infoTipoContrato ({ params }){

    const res = await Database.select('id', 'descricao', 'slug')
    .from('tipo_contratoes')
    .where('id', params.id)
    .first()
     
    return DataResponse.response("success",200,null,res);

  }

  /**
   * Render a form to be used for creating a new tipocontrato.
   * GET tipocontratoes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new tipocontrato.
   * POST tipocontratoes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single tipocontrato.
   * GET tipocontratoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing tipocontrato.
   * GET tipocontratoes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update tipocontrato details.
   * PUT or PATCH tipocontratoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a tipocontrato with id.
   * DELETE tipocontratoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
  
  async selectBox ({ request }) {  
    const res = await Database.select('id', 'descricao', 'slug')
    .from('tipo_contratoes')
    .orderBy('descricao', 'ASC');
     
    return res;
  }
  
}

module.exports = TipoContratoController
