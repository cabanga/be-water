'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const StkMovimento = use("App/Models/StkMovimento");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with stkmovimentos
 */
class StkMovimentoController {
  /**
   * Show a list of all stkmovimentos.
   * GET stkmovimentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, params }) {
    const { search, orderBy, filter, armazem_id, artigo_id, data } = request.all();
    let res = null;

    var artigoID = (params.artigo_id == null || params.artigo_id == 0 ? params.artigo_id : artigo_id);

    res = await Database.select(
      "stk_movimentos.quantidade",
      //"stk_movimentos.total",
      "stk_produtos.valor",
      "stk_movimentos.produto_id",
      "armazens.nome as armazem",
      "stk_movimentos.created_at",
      "stk_tipo_movimentos.movimento",
      "stk_tipo_movimentos.descricao as documento",
      "stk_produtos.descricao as produto"
    ).from("stk_movimentos")
      .innerJoin("stk_produtos", "stk_produtos.id", "stk_movimentos.produto_id")
      .leftJoin("stk_tipo_movimentos", "stk_tipo_movimentos.id", "stk_movimentos.tipo_movimento_id")
      .leftJoin("armazens", "armazens.id", "stk_movimentos.armazem_id")
      .whereIn("stk_produtos.id ", Database.raw("SELECT id FROM stk_produtos  "
        + "  " + (artigoID == null || artigoID == 0 ? " " : " WHERE  stk_produtos.id = " + artigoID)))
      .whereIn("stk_movimentos.armazem_id ", Database.raw("SELECT id FROM armazens  "
        + "  " + (armazem_id == null || armazem_id == 0 ? " " : " WHERE  armazens.id = " + armazem_id)))
      .whereIn("stk_movimentos.id", Database.raw("SELECT id FROM stk_movimentos  "
        + "  " + (data == null ? " " : " WHERE  DATE_FORMAT(created_at, '%Y-%m-%d') = '" + data + "'")))
      .whereIn("stk_produtos.id ", Database.raw("SELECT id FROM stk_produtos  "
        + "  " + (filter.search == null ? " " : " WHERE  stk_produtos.descricao  LIKE '%" + filter.search + "%'")))
      //.orderBy("stk_movimentos.created_at", "DESC")
      .orderBy(orderBy == null ? "stk_movimentos.created_at" : orderBy, "DESC")
      .paginate(filter.pagination.page == null ? 1 : filter.pagination.page, filter.pagination.perPage);





    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new stkmovimento.
   * GET stkmovimentos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new stkmovimento.
   * POST stkmovimentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { artigos, armazem_id } = request.all();

    var tipoMovimentoStock = null;
    tipoMovimentoStock = await Database.select('id').from("stk_tipo_movimentos").where("slug", "ENTRADA-STOCK").first();

    if (tipoMovimentoStock == null) {
      return DataResponse.response("info", 201, "Não foi possível registar, flag (ENTRADA-STOCK) não existe, configure! ", null);
    }


    await StkMovimento.storeData(armazem_id, artigos, tipoMovimentoStock.id, auth.user.id)


    return DataResponse.response("success", 200, "Registado com sucesso");
  }

  async saida({ request, auth }) {
    const { artigos, armazem_id } = request.all();

    var tipoMovimentoStock = null;
    tipoMovimentoStock = await Database.select('id').from("stk_tipo_movimentos").where("slug", "SAIDA-STOCK").first();

    if (tipoMovimentoStock == null) {
      return DataResponse.response("info", 201, "Não foi possível registar, flag (SAIDA-STOCK) não existe, configure! ", null);
    }


    await StkMovimento.storeData(armazem_id, artigos, tipoMovimentoStock.id, auth.user.id)


    return DataResponse.response("success", 200, "Registado com sucesso");
  }

  async transferencia({ request, auth }) {
    const { artigos, armazem_id, armazem_destino_id } = request.all();

    var tipoMovimentoEntrada = null;
    var tipoMovimentoSaida = null;

    tipoMovimentoEntrada = await Database.select('id','slug').from("stk_tipo_movimentos").where("slug", "ENTRADA-STOCK").first();
    
    tipoMovimentoSaida = await Database.select('id','slug').from("stk_tipo_movimentos").where("slug", "SAIDA-STOCK").first();
    
    if (tipoMovimentoEntrada == null) {
      return DataResponse.response("info", 201, "Não foi possível registar, flag (ENTRADA-STOCK) não existe, configure! ", null);
    }

    
    if (tipoMovimentoSaida == null) {
      return DataResponse.response("info", 201, "Não foi possível registar, flag (SAIDA-STOCK) não existe, configure! ", null);
    }

    if(tipoMovimentoSaida.slug == "SAIDA-STOCK"){
      await StkMovimento.storeData(armazem_id, artigos, tipoMovimentoSaida.id, auth.user.id)
    }

    if(tipoMovimentoEntrada.slug == "ENTRADA-STOCK"){
      await StkMovimento.storeData(armazem_destino_id, artigos, tipoMovimentoEntrada.id, auth.user.id)
    }
    
    return DataResponse.response("success", 200, "Registado com sucesso");
  }


  async existenciaStockQtd({ request }) {
    const { armazem_id, artigo_id } = request.all();

    let res = null;

    res = await Database.raw("SELECT armazem_id, produto_id,sum(quantidade) as quantidade FROM (SELECT ms.armazem_id, ms.produto_id, IF(tms.movimento='S',-sum(ms.quantidade),  sum(ms.quantidade)) as quantidade, tms.movimento FROM stk_movimentos ms, stk_tipo_movimentos tms, armazens arm, stk_produtos prod WHERE ms.tipo_movimento_id = tms.id AND ms.armazem_id = arm.id AND ms.produto_id = prod.id "
      + "  " + ("AND ms.armazem_id = '" + armazem_id + "' ")
      + "  " + ("AND  ms.produto_id = '" + artigo_id + "' ")
      + " GROUP BY ms.produto_id, tms.movimento, ms.armazem_id) stocks"
      + " GROUP BY produto_id, armazem_id "
    );

    return DataResponse.response("success", 200, "", res[0]);
  }


  async existenciaStock({ request }) {
    const { armazem_id, artigo_id, search, filter, orderBy } = request.all();
    const PaginationHelper = use("App/Helpers/PaginationHelper")

    let query = null;
    query = await Database.raw("SELECT armazem, material, armazem_id, produto_id,sum(quantidade) as quantidade FROM "
      + " (SELECT arm.nome as armazem, prod.descricao as material, ms.armazem_id, ms.produto_id, IF(tms.movimento='S',-sum(ms.quantidade),  sum(ms.quantidade)) as quantidade, tms.movimento FROM stk_movimentos ms, stk_tipo_movimentos tms, armazens arm, stk_produtos prod WHERE ms.tipo_movimento_id = tms.id AND ms.armazem_id = arm.id AND ms.produto_id = prod.id "
      + "  " + (armazem_id == null || armazem_id == 'null' ? " " : " AND ms.armazem_id = '" + armazem_id + "' ")
      + "  " + (artigo_id == null || artigo_id == 'null' ? " " : "AND  ms.produto_id = '" + artigo_id + "' ")
      + (search == null || search == "" ? " " : " AND arm.nome  LIKE '%" + search + "%'")
      + " GROUP BY ms.produto_id, tms.movimento, ms.armazem_id) stocks"
      + " GROUP BY produto_id, armazem_id "
      //+ " " + (orderBy == null || orderBy == 'null' ? " " : " ORDER BY " + orderBy +" ")
      + " " + (orderBy == null || orderBy == 'null' ? " " : " ORDER BY " + orderBy + " DESC limit " + (filter.pagination.perPage) + " offset " + (((filter.pagination.page > 0 ? filter.pagination.page : 1) - 1) * filter.pagination.perPage))
    );

    let total = await Database.select("produto_id", "armazem_id")
      .from('stk_movimentos')
      .groupBy('produto_id')
      .groupBy('armazem_id')
      .getCount()

    const records = new PaginationHelper(query[0],
      request,
      { custom_build: true },
      filter.pagination.perPage,
      (filter.pagination.page > 0 ? filter.pagination.page : 1),
      total
    ).paginate;


    return DataResponse.response("success", 200, null, records);

  }

  /**
   * Display a single stkmovimento.
   * GET stkmovimentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing stkmovimento.
   * GET stkmovimentos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update stkmovimento details.
   * PUT or PATCH stkmovimentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {

  }

  /**
   * Delete a stkmovimento with id.
   * DELETE stkmovimentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = StkMovimentoController
