'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Database = use("Database");
const DataResponse = use("App/Models/DataResponse");
const Relatorio = use("App/Models/Relatorio");
/**
 * Resourceful controller for interacting with relatorios
 */
class RelatorioController {
  /**
   * Show a list of all relatorios.
   * GET relatorios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async relatorioFacturacaoRealizadaCobrancaGlobal({ request }) {
    let c = await Relatorio.relatorioFacturacaoRealizadaCobrancaGlobal(request.all());
    return DataResponse.response("success", 200, null, c);
  }
  async relatorioFacturacaoRealizadaDetalhada({ request }) {
    let c = await Relatorio.relatorioDetalhada(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioFacturacaopagamentoGlobal({ request }) {
    let c = await Relatorio.relatorioFacturacaopagamentoGlobal(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioFacturacaoporGestorGlobal({ request }) {
    let relatorio = await Relatorio.relatorioFacturacaoporGestorGlobal(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioFacturacaoRealizadaCobrancaGlobalPago({ request }) {
    let relatorio = await Relatorio.relatorioFacturacaoRealizadaCobrancaGlobalPago(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioLoja({ request }) {
    let c = await Relatorio.relatorioLoja(request.all());
    return DataResponse.response("success", 200, null, c);
  }
  /* async relatorioLoja({ request }) {
    let c = await Relatorio.relatorioDetalhada(request.all());
    return DataResponse.response("success", 200, null, c);
  } */
  async relatorioResumoContaCorrente({request}){
    let relatorio= await Relatorio.relatorioCobranca(request.all());
    return DataResponse.response("success", 200, null, relatorio)
  }

  async relatorioIVA({ request }) {
    let c = await Relatorio.relatorioIVA(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioServicoContratados({ request }) {
    let c = await Relatorio.relatorioServicoContratados(request.all());
    return DataResponse.response("success", 200, null, c);
  }

  async relatorioClients({ request }) {
    let c = await Relatorio.relatorioClients(request);
    return DataResponse.response("success", 200, null, c);
  }
  async relatorioMovimentosCaixa({ request }) {
    let c = await Relatorio.relatorioMovimentosCaixa(request.all());
    return DataResponse.response("success", 200, null, c);
  }
  /*   async relatorioExtraccaoClients({ request }) {
      let c = await Relatorio.relatorioExtraccaoClients(request);

      return DataResponse.response("success", 200, null, c);
    }  */

  async relatorioExtraccaoClients({ request }) {
    const { search, orderBy, pagination, filter } = request.all();
    let res = null;

    res = await Database.select(
      "clientes.id as cliente_id",
      "clientes.telefone",
      "clientes.email",
      "clientes.morada",
      "clientes.numero_identificacao",
      "clientes.nome as cliente_nome",
      "clientes.numero_cliente",
      "municipios.nome as municipio",
      "contas.numero_conta",
      "lojas.nome as agencia",
      "tipo_facturacaos.descricao as tipo_facturacao",
      "tipo_clientes.tipoClienteDesc as tipo_cliente",
      "tipo_identidades.nome as tipo_identificacao",
      "generos.descricao as genero",
      "tipo_contratoes.descricao as tipo_contrato",
      "estado_contratoes.nome as estado_contrato",
      "objecto_contratoes.nome as objecto_contrato",
      Database.raw('DATE_FORMAT(contratoes.data_inicio, "%Y-%m-%d") as data_inicio'),
      Database.raw('DATE_FORMAT(contratoes.data_fim, "%Y-%m-%d") as data_fim'),
      "nivel_sensibilidades.descricao as nivel_sensibilidade",
      "classe_tarifarios.descricao as classe_tarifario",
      "tipo_medicaos.nome as tipo_medicao",
    )

      .from("clientes")
      .leftJoin("tipo_identidades", "clientes.tipo_identidade_id", "tipo_identidades.id")
      .leftJoin("municipios", "clientes.municipio_id", "municipios.id")
      .leftJoin("generos", "clientes.genero_id", "generos.id")
      .leftJoin("tipo_clientes", "clientes.tipo_cliente_id", "tipo_clientes.id")
      .innerJoin("contas", "clientes.id", "contas.cliente_id")
      .leftJoin("tipo_facturacaos", "contas.tipo_facturacao_id", "tipo_facturacaos.id")
      .leftJoin("lojas", "contas.agencia_id", "lojas.id")
      .leftJoin("contratoes", "contratoes.conta_id", "contas.id")
      .leftJoin("tipo_medicaos", "contratoes.tipo_medicao_id", "tipo_medicaos.id")
      .leftJoin("tipo_contratoes", "contratoes.tipo_contracto_id", "tipo_contratoes.id")
      .leftJoin("nivel_sensibilidades", "contratoes.nivel_sensibilidade_id", "nivel_sensibilidades.id")
      .leftJoin("objecto_contratoes", "contratoes.objecto_contrato_id", "objecto_contratoes.id")
      .leftJoin("estado_contratoes", "contratoes.estado_contrato_id", "estado_contratoes.id")
      .leftJoin("classe_tarifarios", "contratoes.classe_tarifario_id", "classe_tarifarios.id")

      .where(function () {
        if (filter.genero == null || filter.genero == 'T') {
          this
            .whereNotNull("clientes.genero_id",)
            .orWhere("clientes.genero_id", null)
        } else {
          this
            .where("clientes.genero_id", filter.genero)
        }
      })

      .where(function () {
        if (filter.tipo_contrato == null || filter.tipo_contrato == 'T') {
          this
            .whereNotNull("contratoes.tipo_contracto_id",)
            .orWhere("contratoes.tipo_contracto_id", null)
        } else {
          this
            .where("contratoes.tipo_contracto_id", filter.tipo_contrato)
        }
      })

      .where(function () {
        if (filter.tipo_facturacao == null || filter.tipo_facturacao == 'T') {
          this
            .whereNotNull("contratoes.tipo_facturacao_id",)
            .orWhere("contratoes.tipo_facturacao_id", null)
        } else {
          this
            .where("contratoes.tipo_facturacao_id", filter.tipo_facturacao)
        }
      })

      .where(function () {
        if (filter.tipo_cliente == null || filter.tipo_cliente == 'T') {
          this
            .whereNotNull("clientes.tipo_cliente_id",)
            .orWhere("clientes.tipo_cliente_id", null)
        } else {
          this
            .where("clientes.tipo_cliente_id", filter.tipo_cliente)
        }
      })

      .where(function () {
        if (filter.estado_contrato == null || filter.estado_contrato == 'T') {
          this
            .whereNotNull("contratoes.estado_contrato_id",)
            .orWhere("contratoes.estado_contrato_id", null)
        } else {
          this
            .where("contratoes.estado_contrato_id", filter.estado_contrato)
        }
      })

      .where(function () {
        if (filter.objecto_contrato == null || filter.objecto_contrato == 'T') {
          this
            .whereNotNull("contratoes.objecto_contrato_id",)
            .orWhere("contratoes.objecto_contrato_id", null)
        } else {
          this
            .where("contratoes.objecto_contrato_id", filter.objecto_contrato)
        }
      })

      .where(function () {
        if (filter.tipo_identificacao == null || filter.tipo_identificacao == 'T') {
          this
            .whereNotNull("clientes.tipo_identidade_id",)
            .orWhere("clientes.tipo_identidade_id", null)
        } else {
          this
            .where("clientes.tipo_identidade_id", filter.tipo_identificacao)
        }
      })

      .orderBy(orderBy == null ? "clientes.id" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);

    return DataResponse.response("success", 200, null, res);
  }

}


module.exports = RelatorioController
