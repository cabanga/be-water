'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const CampoJardim = use("App/Models/CampoJardim");
const Database = use("Database");
var moment = require("moment");

/**
 * Resourceful controller for interacting with campojardims
 */
class CampoJardimController {
  /**
   * Show a list of all campojardims.
   * GET campojardims
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new campojardim.
   * GET campojardims/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new campojardim.
   * POST campojardims
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single campojardim.
   * GET campojardims/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing campojardim.
   * GET campojardims/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update campojardim details.
   * PUT or PATCH campojardims/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a campojardim with id.
   * DELETE campojardims/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
  
  async selectBox ({ request }) {  
    const res = await Database.select('id', 'nome', 'slug')
    .from('campo_jardims')
    .orderBy('nome', 'ASC');
     
    return res;
  }
}

module.exports = CampoJardimController
