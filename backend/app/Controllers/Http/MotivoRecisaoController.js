'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const MotivoRecisao = use("App/Models/MotivoRecisao");
const Database = use("Database");
/**
 * Resourceful controller for interacting with motivorecisaos
 */
class MotivoRecisaoController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("motivo_recisaos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("motivo_recisaos")
        .orWhere("slug", "like", "%" + search + "%")
        .orWhere("nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { nome, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('motivo_recisaos')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Motivo de Denúncia com este slug",
        Verify
      );

    } else {
      const motivodenucia = await Database.table('motivo_recisaos').insert({
        slug: slug,
        nome: nome,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        motivodenucia
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["nome", "slug"]);

    const Verify = await motivodenucia.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Motivo de Denúncia com este slug",
        Verify
      );
    } else {

      // update with new data entered
      const motivodenucia = await motivodenucia.find(params.id);
      motivodenucia.merge(data);
      await motivodenucia.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        motivodenucia
      );
    }
  }

  async selectBox({ request }) {
    const res = await Database.select('id', 'nome', 'slug')
      .from('motivo_recisaos')
      .orderBy('nome', 'ASC');

    return res;
  }

  async selectBoxEstado({ request }) {
    const res = await Database.select('id', 'nome', 'slug')
      .from('estado_contratoes')
      .orWhere("nome", "like", "%Resci%") //Provisório
      .orderBy('nome', 'ASC');

    return res;
  }

}

module.exports = MotivoRecisaoController
