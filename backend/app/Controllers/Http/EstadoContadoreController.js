'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const EstadoContadore = use("App/Models/EstadoContadore");
const RelacaoEstadoContadore = use("App/Models/RelacaoEstadoContadore");
const Database = use("Database");
var moment = require("moment");
/**
 * Resourceful controller for interacting with estadocontadores
 */
class EstadoContadoreController {

  async index({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("estado_contadores")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("estado_contadores")
        .orWhere("estado_contadores.slug", "like", "%" + search + "%")
        .orWhere("estado_contadores.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug } = request.all();

    const Verify = await EstadoContadore.query()
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Estado de Contador com este slug",
        Verify
      );

    } else {
      const estadocontadore = await EstadoContadore.create({
        descricao: descricao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao", "slug"]);

    const Verify = await EstadoContadore.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Estado de Contador com este slug",
        Verify
      );
    } else {

      // update with new data entered
      const estadocontadore = await EstadoContadore.find(params.id);
      estadocontadore.merge(data);
      await estadocontadore.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        estadocontadore
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'descricao', 'slug')
      .from('estado_contadores')
      .orderBy('descricao', 'ASC');

    return res;
  }

  async AssociarEstados({ request, auth, params }) {
    const { estado_filho } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    let estado_pai = params.id;

    if (estado_filho == estado_pai) {
      return DataResponse.response(
        null,
        201,
        "Não é possível associar um estado de contador a ele mesmo"
        )

    } else {
      const Verify = await Database.table('relacao_estado_contadores')
        .where("estado_pai", estado_pai)
        .where("estado_filho", estado_filho)
        .getCount();

      if (Verify > 0) {
        return DataResponse.response(
          null,
          201,
          "Essa associação já existe",
          Verify
        );

      } else {
        const relacao_estado = await Database.table('relacao_estado_contadores').insert({
          estado_pai: estado_pai,
          estado_filho: estado_filho,
          user_id: auth.user.id,
          'created_at': dataActual,
          'updated_at': dataActual
        });
        return DataResponse.response(
          "success",
          200,
          "Associado com sucesso",
          null
        );
      }
    }

  }

  async getEstadoFilhosByEstadoPai({ request, params }) {

    const contador = await Database.select(
      "relacao_estado_contadores.id",
      "relacao_estado_contadores.estado_pai",
      "relacao_estado_contadores.estado_filho",
      "estado_contadores.descricao"
    )
      .from("relacao_estado_contadores")
      .leftJoin("estado_contadores", "estado_contadores.id", "relacao_estado_contadores.estado_filho")
      .where("estado_pai", params.id)

    return DataResponse.response("success", 200, "", contador);
  }
}

module.exports = EstadoContadoreController
