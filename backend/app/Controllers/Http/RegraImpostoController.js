'use strict'

const RegraImposto = use('App/Models/RegraImposto');
const Database = use('Database');

class RegraImpostoController {

      /**
   * Show a list of all artigos.
   * GET artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index () {
    const impostos = await Database.table('impostos');
    return impostos;
  }

  /**
   * Create/save a new artigo.
   * POST artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({
    request,
    auth
  }) {
    const {
      regra
    } = request.all();
    const regra = await RegraImposto.create({
      regra: regra,
      user_id: auth.user.id
    });
    return data;
  }
}

module.exports = RegraImpostoController
