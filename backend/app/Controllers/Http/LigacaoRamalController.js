'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const LigacaoRamal = use("App/Models/LigacaoRamal");
const Database = use("Database");
/**
 * Resourceful controller for interacting with ligacaoramals
 */
class LigacaoRamalController {
  /**
   * Show a list of all ligacaoramals.
   * GET ligacaoramals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "ligacao_ramals.id",
        "ligacao_ramals.comprimento",
        "ligacao_ramals.diamentro",
        "ligacao_ramals.profundidade",
        "ponto_a.descricao as ponto_a",
        "ponto_b.descricao as ponto_b",
        "local_instalacaos.moradia_numero",
        "local_instalacaos.predio_nome",
        "local_instalacaos.id"
      )
        .from("ligacao_ramals")
        .leftJoin("objecto_ligacao_ramals as ponto_a", "ligacao_ramals.ponto_a_id", "ponto_a.id")
        .leftJoin("objecto_ligacao_ramals as ponto_b", "ligacao_ramals.ponto_b_id", "ponto_b.id")
        .leftJoin("local_instalacaos", "ligacao_ramals.local_instalacao_id", "local_instalacaos.id")
        .orderBy(orderBy == null ? "ligacao_ramals.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "ligacao_ramals.id",
        "ligacao_ramals.comprimento",
        "ligacao_ramals.diamentro",
        "ligacao_ramals.profundidade",
        "ponto_a.descricao as ponto_a",
        "ponto_b.descricao as ponto_b",
        "local_instalacaos.moradia_numero",
        "local_instalacaos.predio_nome",
        "local_instalacaos.id"
      )
        .from("ligacao_ramals")
        .leftJoin("objecto_ligacao_ramals as ponto_a", "ligacao_ramals.ponto_a_id", "ponto_a.id")
        .leftJoin("objecto_ligacao_ramals as ponto_b", "ligacao_ramals.ponto_b_id", "ponto_b.id")
        .leftJoin("local_instalacaos", "ligacao_ramals.local_instalacao_id", "local_instalacaos.id")
        .where("ponto_a.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "ponto_a.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }


  async getLigacaoRamals({ request }) {
    const { pagination, tipo_objecto_id, objecto_ligacao_id } = request.all();

    let res = null;

      res = await Database.select(
        "ligacao_ramals.id",
        "ligacao_ramals.comprimento",
        "ligacao_ramals.diamentro",
        "ligacao_ramals.profundidade",
        "ponto_a.id as ponto_a_id",
        "ponto_a.descricao as ponto_a",
        "ponto_b.id as ponto_b_id",
        "ponto_b.descricao as ponto_b",
        "ponto_a.tipo_objecto_id as ponto_a_tipo_objecto_id",
        "ponto_a_tipo_objectos.descricao as ponto_a_tipo_objecto",
        "ponto_b.tipo_objecto_id as ponto_b_tipo_objecto_id",
        "ponto_b_tipo_objectos.descricao as ponto_b_tipo_objecto",
        "ligacao_ramals.local_instalacao_id",
        "local_instalacaos.moradia_numero",
        "local_instalacaos.predio_nome",
        "local_instalacaos.predio_andar",
        "local_instalacaos.is_predio"
      )
        .from("ligacao_ramals")
        .leftJoin("objecto_ligacao_ramals as ponto_a", "ligacao_ramals.ponto_a_id", "ponto_a.id")
        .leftJoin("objecto_ligacao_ramals as ponto_b", "ligacao_ramals.ponto_b_id", "ponto_b.id")
        .leftJoin("tipo_objectos as ponto_a_tipo_objectos", "ponto_a.tipo_objecto_id", "ponto_a_tipo_objectos.id")
        .leftJoin("tipo_objectos as ponto_b_tipo_objectos", "ponto_b.tipo_objecto_id", "ponto_b_tipo_objectos.id")
        .leftJoin("local_instalacaos", "ligacao_ramals.local_instalacao_id", "local_instalacaos.id")

        .where(function () {
          /*
          if(pagination.search != null)
          {
            this.where("ligacao_ramals.comprimento", "like", "%" + pagination.search + "%")
            this.orWhere("ligacao_ramals.diamentro", "like", "%" + pagination.search + "%")
            this.orWhere("ligacao_ramals.profundidade", "like", "%" + pagination.search + "%")
            this.orWhere("ponto_a.descricao", "like", "%" + pagination.search + "%")
            this.orWhere("ponto_b.descricao", "like", "%" + pagination.search + "%")
            this.orWhere("ligacao_ramals.local_instalacao_id", "like", "%" + pagination.search + "%")
            this.orWhere("local_instalacaos.moradia_numero", "like", "%" + pagination.search + "%")
            this.orWhere("local_instalacaos.predio_nome", "like", "%" + pagination.search + "%")
    
          }
*/
          if(tipo_objecto_id != null)
          {
            this.where("ponto_a.tipo_objecto_id", tipo_objecto_id)
            this.orWhere("ponto_b.tipo_objecto_id", tipo_objecto_id)
          }

          if(objecto_ligacao_id != null)
          {
            this.where("ponto_a.id", objecto_ligacao_id)
            this.orWhere("ponto_b.id", objecto_ligacao_id)
          }
        })
        
        .orderBy("ligacao_ramals.created_at", "DESC")

        .paginate(pagination.page, pagination.perPage);


    return DataResponse.response("success", 200, "", res);
  }


  async getLigacaoLocalInstalacaosById({ params }) {

    let res = await Database.select(
        "ligacao_ramals.id",
        "ligacao_ramals.comprimento",
        "ligacao_ramals.diamentro",
        "ligacao_ramals.profundidade",
        "ligacao_ramals.ponto_a_id",
        "ponto_a.descricao as ponto_a",
        "ligacao_ramals.ponto_b_id",
        "ponto_b.descricao as ponto_b",
        "ponto_a.tipo_objecto_id as ponto_a_tipo_objecto_id",
        "ponto_a_tipo_objectos.descricao as ponto_a_tipo_objecto",
        "ponto_b.tipo_objecto_id as ponto_b_tipo_objecto_id",
        "ponto_b_tipo_objectos.descricao as ponto_b_tipo_objecto",
        "ligacao_ramals.local_instalacao_id",
        "local_instalacaos.moradia_numero",
        "local_instalacaos.predio_nome",
        "local_instalacaos.predio_andar",
        "local_instalacaos.is_predio"
      )
        .from("ligacao_ramals")
        .leftJoin("objecto_ligacao_ramals as ponto_a", "ligacao_ramals.ponto_a_id", "ponto_a.id")
        .leftJoin("objecto_ligacao_ramals as ponto_b", "ligacao_ramals.ponto_b_id", "ponto_b.id")
        .leftJoin("tipo_objectos as ponto_a_tipo_objectos", "ponto_a.tipo_objecto_id", "ponto_a_tipo_objectos.id")
        .leftJoin("tipo_objectos as ponto_b_tipo_objectos", "ponto_b.tipo_objecto_id", "ponto_b_tipo_objectos.id")
        .leftJoin("local_instalacaos", "ligacao_ramals.local_instalacao_id", "local_instalacaos.id")

        .whereNull("ligacao_ramals.ponto_a_id")
        .where("ligacao_ramals.local_instalacao_id", params.id)
        
        .orderBy("ligacao_ramals.created_at", "DESC")

        .paginate(pagination.page, pagination.perPage);


    return DataResponse.response("success", 200, "", res);
  }

  async getLigacoesFinaisByIdLocalInstacao({ params }) {

    let res = null;

    res = await Database.select(
      "ligacao_ramals.id",
      "ligacao_ramals.comprimento",
      "ligacao_ramals.diamentro",
      "ligacao_ramals.profundidade",
      "ligacao_ramals.ponto_a_id",
      "ponto_b.descricao as ponto_b",

      "local_instalacaos.id",
      "local_instalacaos.predio_nome",
      "local_instalacaos.is_active",
      "local_instalacaos.latitude",
      "local_instalacaos.longitude",
      "local_instalacaos.rua_id",
      /*       "ruas.nome as rua",
            "ruas.bairro_id",
            "bairros.nome as bairro",
            "bairros.distrito_id",
            "distritos.nome as distrito",
            "bairros.municipio_id",
            "municipios.nome as municipio",
            "municipios.provincia_id",
            "provincias.nome as provincia",
            "local_instalacaos.user_id",
            "users.nome as user" */
    )
      .from("ligacao_ramals")
      .leftJoin("objecto_ligacao_ramals as ponto_b", "ligacao_ramals.ponto_b_id", "ponto_b.id")

      .leftJoin("local_instalacaos", "ligacao_ramals.local_instalacao_id", "local_instalacaos.id")
      /* 
    .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
    .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
    .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
    .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
    .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
    .innerJoin("users", "users.id", "local_instalacaos.user_id") */

      .where("ligacao_ramals.local_instalacao_id", params.id)

      .orderBy("ligacao_ramals.created_at", "DESC");


    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new ligacaoramal.
   * GET ligacaoramals/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new ligacaoramal.
   * POST ligacaoramals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { ponto_a_id, ligacao } = request.all();

    const res = await LigacaoRamal.store(ponto_a_id, ligacao, auth);

    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      null
    );

  }

  /**
   * Display a single ligacaoramal.
   * GET ligacaoramals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing ligacaoramal.
   * GET ligacaoramals/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update ligacaoramal details.
   * PUT or PATCH ligacaoramals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   *  const { diamentro, profundidade, comprimento, ponto_a_id, ponto_b_id, local_instalacao_id } = request.all();
   */

  async update({ params, request }) {
    const data = request.only(["diamentro", "profundidade", "comprimento"]);

    // update with new data entered
    const res = await LigacaoRamal.find(params.id);
    res.merge(data);
    await res.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      res
    );


  }
  /* } */

  /**
   * Delete a ligacaoramal with id.
   * DELETE ligacaoramals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = LigacaoRamalController
