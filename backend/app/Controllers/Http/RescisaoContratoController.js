'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const RescisaoContrato = use("App/Models/RescisaoContrato");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

/**
 * Resourceful controller for interacting with rescisaocontratoes
 */
class RescisaoContratoController {
  /**
   * Show a list of all rescisaocontratoes.
   * GET rescisaocontratoes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async gerarRescisaoContrato({ params, request, response }) {
    let data = await RescisaoContrato.gerarRescisaoContrato(params.id);
    
    return data;
  }

  /**
   * Render a form to be used for creating a new rescisaocontrato.
   * GET rescisaocontratoes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new rescisaocontrato.
   * POST rescisaocontratoes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single rescisaocontrato.
   * GET rescisaocontratoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing rescisaocontrato.
   * GET rescisaocontratoes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update rescisaocontrato details.
   * PUT or PATCH rescisaocontratoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a rescisaocontrato with id.
   * DELETE rescisaocontratoes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = RescisaoContratoController
