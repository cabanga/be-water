'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Moeda = use("App/Models/Moeda");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

/**
 * Resourceful controller for interacting with moedas
 */
class MoedaController {
  /**
   * Show a list of all moedas.
   * GET moedas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index() {

    const moedas = await Database.select("*").from("moedas");
    return DataResponse.response( "success",200, null,  moedas );
    console.log(moedas)
  }
  async moedas({request}){

    const{filtros} = request.all();
    const moedas = await Database.select("moedas.nome","moedas.id").from("moedas")
    .innerJoin("facturas","facturas.moeda_id","moedas.id")
    .innerJoin("bill_runs", "facturas.id", "bill_runs.factura_utilitie_id")
    .innerJoin("bill_run_headers", "bill_runs.bill_run_header_id", "bill_run_headers.id")
    .where("bill_run_headers.ano", filtros.ano)
    .whereIn("bill_run_headers.mes", filtros.mes == 'T' || filtros.mes == null ? Database.select("mes").from("bill_run_headers") : [filtros.mes])
    .groupBy("nome","id").orderBy("nome","asc");
     console.log(moedas)
    return DataResponse.response("success", 200, null, moedas);
  }
  async moeda(){

    const moedas = await Database.select("moedas.nome","moedas.id").from("moedas")
     .where("activo",1)
    return DataResponse.response("success", 200, null, moedas);
  }
  /**
   * Render a form to be used for creating a new moeda.
   * GET moedas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new moeda.
   * POST moedas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { nome, codigo_iso , descricao, unidade_fracionaria, activo } = request.all();
    const moeda = await Moeda.query().where("nome", nome).getCount();

    if (moeda > 0) {
      return DataResponse.response("success", 500, "Já existe uma moeda com as mesmas configurações",null);
    } else {
      const serie = await Moeda.create({
        nome: nome,
        codigo_iso: codigo_iso,
        descricao: descricao,
        activo: activo,
        unidade_fracionaria:unidade_fracionaria,
        user_id: auth.user.id
      });
      return DataResponse.response( "success",200,  "moeda registado com sucesso",  moeda );
    }
  }

  /**
   * Display a single moeda.
   * GET moedas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing moeda.
   * GET moedas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update moeda details.
   * PUT or PATCH moedas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(['nome', 'codigo_iso' , 'descricao', 'unidade_fracionaria', 'activo']);
    // update with new data entered
    const moeda = await Moeda.find(params.id);
    moeda.merge(data);
    await moeda.save();

    return DataResponse.response("success", 200, "Dados actualizados com sucesso", moeda);

  }

  /**
   * Delete a moeda with id.
   * DELETE moedas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = MoedaController
