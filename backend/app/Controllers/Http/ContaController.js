'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Conta = use("App/Models/Conta");
const LogEstadoConta = use("App/Models/LogEstadoConta");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage');

/**
 * Resourceful controller for interacting with contas
 */
class ContaController {
  /**
   * Show a list of all contas.
   * GET contas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params }) {

    const contas = await Database.select(
      'contas.id as id',
      'contas.tipo_facturacao_id',
      'tipo_facturacaos.descricao as tipo_facturacao',
      'contas.id',
      'contas.id as numero_conta',
      'contas.estado',
      'estado_contas.descricao as estado_conta',
      'estado_contas.slug as estado_conta_slug',
      'contas.contaDescricao as descricao',
      'contas.moeda_id',
      'moedas.nome as moeda'
    )
      .table('contas')
      .leftJoin("moedas", "contas.moeda_id", "moedas.id")
      .leftJoin("tipo_facturacaos", "contas.tipo_facturacao_id", "tipo_facturacaos.id")
      .leftJoin("estado_contas", "contas.estado_id", "estado_contas.id")
      .where("contas.cliente_id", params.id)

    return DataResponse.response("success", 200, "", contas);
  }


  async create({ request, response, view }) { }


  async store({ request }) {
 
    const { 
      descricao,
      cliente_id,
      tipo_facturacao_id
    } = request.all();

    /*const conta = await Database.select("*")
    .from("contas")
    .where("numero_conta", numero_conta)
    .first();


    if (conta != null){
      return DataResponse.response("info", 208, "Esta conta já existe", null)
    }*/
     
     const result = await Conta.create({ 
        contaDescricao: descricao,
        cliente_id: cliente_id,
        estado_id: 1,
        tipo_facturacao_id: tipo_facturacao_id
      })
    

    return DataResponse.response( "success", 200, "Registo efectuado com sucesso", result
    );
  }


  async show({ params, request, response, view }) { }


  async edit({ params, request, response, view }) { }


  async update({ params, request, response }) {

    const { 
      descricao,
      cliente_id,
      moeda_id,
      agencia_id,
      estado,
      dataEstado,
      gestor_conta_id,
      tipo_facturacao_id
    } = request.all();

    const conta = await Conta.find(params.id);

    conta.merge({ 
      contaDescricao: descricao,
      moeda_id: 1, // Kwanza
      cliente_id: cliente_id,
      estado: estado,
      tipo_facturacao_id: tipo_facturacao_id,
      //user_id: user_id
    });

    await conta.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );
  }


  async destroy({ params, request, response }) { }

  async selectBoxContasCliente({ params, auth }) {

    //if (auth.user.loja_id == null) {
    const contas = await Database.select(
      'contas.id',
      'contas.id as numero_conta',
      'contas.cliente_id',
      'contas.moeda_id', 
      'contas.tipo_facturacao_id',
      "contas.contaDescricao", 
      'tipo_facturacaos.descricao as tipo_facturacao'
    )
      .table('contas')
      .leftJoin("tipo_facturacaos", "tipo_facturacaos.id", "contas.tipo_facturacao_id")
      .where('contas.cliente_id', params.id);


    return DataResponse.response("success", 200, "", contas);
  }

  async getContaEstado({ params }) {
    const conta = await Database.select('id', 'estado')
      .table('contas')
      .where('id', params.id).first()

    return DataResponse.response("success", 200, "", conta);
  }

  async updateEstado({ params, request, auth }) {
    const data = request.only(['estado']);
    const log = request.only(['estado', 'estado_actual']);

    const conta = await Conta.find(params.id);
    conta.merge(data);
    await conta.save();

    await LogEstadoConta.create({ 'conta_id': params.id, 'estado_anterior': log.estado_actual, 'user_id': auth.user.id, 'estado_novo': log.estado })

    return DataResponse.response("success", 200, "Estado actualizado com sucesso", conta);

  }

  async ContaPosPagoCliente({ request }) {

    const contas = await Conta.query()
      .where("tipo_facturacao_id", "POS_PAGO").where('estado', true)
      .paginate(1, 1000000);

    return DataResponse.response("success", 200, "", contas);
  }

  async getClienteContas({ params, request }) {

    const dado = request.only(['conta_antiga']);

    let cliente = null;
    let contas = null;

    cliente = await Database.select(
      "cli.id as clienteID",
      "cli.nome as clienteNome"
    )
      .from("servicos as se")
      .innerJoin("contas as co", "co.id", "se.conta_id")
      .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
      .where("se.id", params.id).first()

    contas = await Database.select(
      "co.id as contaID",
      "co.contaDescricao as descricao"
    )
      .from("contas as co")
      .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
      .where("co.cliente_id", cliente.clienteID)
      .whereNot("co.id", dado.conta_antiga)
    let data = {
      cliente: cliente,
      contas: contas,

    };

    //console.log(data)

    return DataResponse.response("success", 200, "", data);
  }


  async getClienteContasPedidos({ params }) {

    let cliente = null;
    let contas = null;

    cliente = await Database.select(
      "cli.id as clienteID",
      "cli.nome as clienteNome"
    )
      .from("pedidos as pe")
      //.innerJoin("contas as co", "co.id", "pe.conta_id")
      .innerJoin("clientes as cli", "cli.id", "pe.cliente_id")
      .where("pe.id", params.id).first()

    contas = await Database.select(
      "co.id as contaID",
      "co.contaDescricao as descricao"
    )
      .from("contas as co")
      .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
      .where("co.cliente_id", cliente.clienteID)

    let data = {
      cliente: cliente,
      contas: contas,

    };

    //console.log(data)

    return DataResponse.response("success", 200, "", data);
  }


  async getClienteContasSearch({ params, response }) {
    let contas = null;
    contas = await Database.select(
      "co.id as contaID",
      "co.contaDescricao as descricao"
    )
      .from("contas as co")
      .innerJoin("clientes as cli", "cli.id", "co.cliente_id")
      .where("co.cliente_id", params.id);

    return response
      .status(200)
      .send(Mensagem.response(response.response.statusCode, "sucesso", contas));

  }


}

module.exports = ContaController
