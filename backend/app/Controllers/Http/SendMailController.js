'use strict'
const nodemailer = require("nodemailer");
const DataResponse = use('App/Models/DataResponse');
const User = use('App/Models/User');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with sendmails
 */
class SendMailController {
  /**
   * Show a list of all sendmails.
   * GET sendmails
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new sendmail.
   * GET sendmails/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new sendmail.
   * POST sendmails
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
	
   const { email, assunto, mensagem,html } = request.all();	
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: 'caniggiamoreira@gmail.com', // generated ethereal user
      pass: 'caniggia8844' // generated ethereal password
    }
  });

  // send mail with defined transport object
  transporter.sendMail({
    from: ""+email, // sender address
    to: "carlos.quingles@itgest.co.ao",
    subject: ""+assunto, // Subject line
    text: ""+mensagem, // plain text body
    html: ""+html,
	 /*attachments: [{
     filename: 'unig-logo.jpg',
     path: 'assets/img/unig-logo.jpg',
     cid: 'unig-logo' //my mistake was putting "cid:logo@cid" here! 
}]*/
	
 });
 
 
  //console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  //console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info)); 
      return DataResponse.response("success", 200, "Obrigado! Sua mensagem foi enviada, entraremos em contato o mais breve possível.", null);
  
  }

  /**
   * Display a single sendmail.
   * GET sendmails/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing sendmail.
   * GET sendmails/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update sendmail details.
   * PUT or PATCH sendmails/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a sendmail with id.
   * DELETE sendmails/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = SendMailController
