'use strict'

const DataResponse = use("App/Models/DataResponse");
const TipoProduto = use("App/Models/TipoProduto");
const Database = use("Database");
var moment = require("moment");

class TipoProdutoController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_produtos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_produtos")
        .orWhere("tipo_produtos.slug", "like", "%" + search + "%")
        .orWhere("tipo_produtos.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('tipo_produtos')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipologia de Cliente com este slug",
        Verify
      );

    } else {
      const TipoProduto = await Database.table('tipo_produtos').insert({
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        TipoProduto
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao", "slug"]);

    const Verify = await TipoProduto.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipologia de Cliente com este slug",
        Verify
      );
    } else {

      // update with new data entered
      const tipo_produtos = await TipoProduto.find(params.id);
      tipo_produtos.merge(data);
      await tipo_produtos.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        tipo_produtos
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'descricao', 'slug')
      .from('tipo_produtos')
      .orderBy('descricao', 'ASC');

    return res;
  }
}

module.exports = TipoProdutoController
