'use strict'
const DataResponse = use("App/Models/DataResponse");
const EstadoConta = use("App/Models/EstadoConta");
const Database = use("Database");
var moment = require("moment");

class EstadoContaController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("estado_contas")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("estado_contas")
        .orWhere("estado_contas.slug", "like", "%" + search + "%")
        .orWhere("estado_contas.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");
    
    const Verify = await Database.table('estado_contas')
      .where("slug", slug)
      .getCount(); 

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo Facturação com este slug",
        Verify
      );

    } else {
      const estado_contas = await Database.table('estado_contas').insert({
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        estado_contas
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const Verify = await EstadoConta.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (Verify > 0) {
    return DataResponse.response(
      null,
      201,
      "Já existe Tipo Facturação com este slug",
      Verify
    );
  }else{

    // update with new data entered
    const estado_contas = await EstadoConta.find(params.id);
    estado_contas.merge(data);
    await estado_contas.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      estado_contas
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('estado_contas')
    .orderBy('descricao', 'ASC');
        
/*     return DataResponse.response(
      null,
      200,
      res
      ); */
      return res
    }
}

module.exports = EstadoContaController
