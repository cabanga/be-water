'use strict'

const DataResponse = use("App/Models/DataResponse");
const TipoMedicao = use("App/Models/TipoMedicao");
const Database = use("Database");
var moment = require("moment");
class TipoMedicaoController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("tipo_medicaos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("tipo_medicaos")
        .orWhere("slug", "like", "%" + search + "%")
        .orWhere("nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { nome, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('tipo_medicaos')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo de Medição com este slug",
        Verify
      );

    } else {
      const tipo_medicao = await Database.table('tipo_medicaos').insert({
        slug: slug,
        nome: nome,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        tipo_medicao
      );
    }
  }

  async update({ params, request, auth }) {
    const { slug, nome } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('tipo_medicaos')
      .where("slug", slug)
      .whereNot({ id: params.id })
      .getCount(); 

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo de Medição com este slug",
        Verify
      );

    } else {
      const tipo_medicao = await Database.table('tipo_medicaos')
      .where('id', params.id)
      .update({
        slug: slug,
        nome: nome,
        user_id: auth.user.id,
        'user_id': auth.user.id,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        tipo_medicao
      );
    }
  }

    async selectBox ({ request }) {
   
    var res = await Database.select('id', 'nome', 'slug')
    .from('tipo_medicaos')
    .orderBy('nome', 'ASC');
        
      return res;
    }

}

module.exports = TipoMedicaoController
