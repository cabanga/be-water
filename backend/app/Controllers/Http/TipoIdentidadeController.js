'use strict'
const tipoIdentidadeRepository = use('App/Repositories/tipoIdentidadeRepository')
const Database = use("Database");


class TipoIdentidadeController {
    #_tipoIdentidadeRepo

    constructor() {
        this.#_tipoIdentidadeRepo = new tipoIdentidadeRepository()
    }

    async index({ response, request }) {
        const page = request.input('page')
        const search = request.input('search')
        const genders = await this.#_tipoIdentidadeRepo.getAll(page, search)
        return response.ok(genders);
    }

    async store({ request }) {
        const data = request.only(['nome', 'numero_digitos'])
        const res = await this.#_tipoIdentidadeRepo.create( data )
        return res
    }

    async show({params, response}){
        const gender = await this.#_tipoIdentidadeRepo.findById(params.id)
        return response.ok(gender)
    }

    async update({ params, request }){
        const data = request.only(['nome', 'numero_digitos'])

        const res = await this.#_tipoIdentidadeRepo.update( params.id, data )
        return res
    }

    async delete({ params }) {
        const res = await this.#_tipoIdentidadeRepo.delete( params.id )
        return res
    }

    async selectBox({ params }) {

        let res = await Database.select(
          "tipo_identidades.id",
          "tipo_identidades.nome"
        )
        .from("tipo_identidades")
        .where("tipo_identidades.is_delected", false)
        .orderBy("tipo_identidades.nome", "ASC");
    
        return res;
    }
}

module.exports = TipoIdentidadeController
