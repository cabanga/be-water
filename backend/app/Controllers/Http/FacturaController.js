"use strict";
var numeral = require("numeral");

var moment = require("moment");
const Factura = use("App/Models/Factura");
const Recibo = use("App/Models/Recibo");
const LinhaFactura = use("App/Models/LinhaFactura");
const LinhaRecibo = use("App/Models/LinhaRecibo");
const Produto = use("App/Models/Produto");
const Imposto = use("App/Models/Imposto");
const Cliente = use("App/Models/Cliente");
const Pagamento = use("App/Models/Pagamento");
const FacturaBanco = use("App/Models/FacturaBanco");
const Caixa = use("App/Models/Caixa");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");


const LinhaPagamento = use("App/Models/LinhaPagamento");
const MovimentoAdiantamento = use("App/Models/MovimentoAdiantamento");
const { validate } = use("Validator");
const FacturaRepository = use('App/Repositories/FacturaRepository')


class FacturaController {

  #_facturaRepo

  constructor() {
    this.#_facturaRepo = new FacturaRepository()
  }

  async index({ request }) {
    const search = request.input("search");

    const filter = {
      page: request.input("page") || 1,
      perPage: request.input("perPage") || 5,
      orderBy: request.input("orderBy") || "facturas.created_at",
      typeOrderBy: request.input("typeOrderBy") || "DESC",
      documents: request.input("documents")!=undefined ? [""+request.input("documents")+""]: ["FT", "NC", "FR"],
      startDate: moment(request.input("startDate")).format("YYYY-MM-DD"),
      endDate: moment(request.input("endDate")).format("YYYY-MM-DD"),
      keys: { client: ["nome"], invoice: ["factura_sigla", "numero"] },
      typeFilter: request.input("typeFilter").trim().toLowerCase().replace(" ", "_").replace(" ", "_").replace(" ", "_").replace(" ", "_"),
    };

    const invoices = await Factura.findAllInvoices(search, filter);

    return DataResponse.response("success", 200, "", invoices);
  }

  async rotas({ response, request }){
    const page = request.input('page')
    const search = request.input('search')
    const res = await this.#_facturaRepo.rotas(page, search)
    return response.ok(res)
  }

  async charges({ response, request }){
    const page = request.input('page')
    const search = request.input('search')
    const res = await this.#_facturaRepo.charges()
    return response.ok(res)
  }

  async validar_pre_facturacao({ response, request }){
    const data = request.only(['leituras'])
    let contratos = []

    for(let leitura of data.leituras){
      let contrato = await this.#_facturaRepo.este_contador_tem_leituras(leitura.contador_id)
      contratos.push(contrato)
    }

    let filter_contratos = contratos.filter( objct => !objct.ok)
    let open_modal = filter_contratos.length === 0 ? false : true
    return response.ok( {data: filter_contratos, open_modal: open_modal} )
  }

  async processar_pre_facturacao({ response, request }){
    var res = null
    const { consumo, data_ultima_leitura, data_leitura, classe_tarifario_id, leitura_id } = request.all();

    let is_leitura = request.input('nao_leitura')

    const data = {
      consumo: consumo,
      data_ultima_leitura: moment(data_ultima_leitura).format("DD/MM/YYYY"),
      data_leitura: moment(data_leitura).format("DD/MM/YYYY"),
      classe_tarifario_id: classe_tarifario_id,
    }

    if (!is_leitura) {
      res = await this.#_facturaRepo.processar_pre_facturacao_leitura(leitura_id, data)
    } else {
      res = await this.#_facturaRepo.processar_pre_facturacao_nao_leitura()
    }

    return response.ok(res)
  }































  async listaOrcamento({ request, response, view }) {
    const { start, end, search, order, searchData } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "facturas.id as factura_id",
        "facturas.numero",
        "facturas.total",
        "facturas.totalComImposto",
        "facturas.totalSemImposto",
        "facturas.status",
        "facturas.pago",
        "facturas.factura_sigla",
        "facturas.numero_origem_factura",
        "facturas.data_origem_factura",
        "facturas.data_vencimento",
        "facturas.created_at",
        "clientes.id as cliente_id",
        "clientes.nome",
        "clientes.telefone",
        "clientes.contribuente",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla"
      )
        .from("facturas")
        .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
        .innerJoin("series", "series.id", "facturas.serie_id")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .where("documentos.sigla", "OR")
        .orderBy("facturas.created_at", "DESC")
        .paginate(start, end);
    } else {
      res = await Database.select(
        "facturas.id as factura_id",
        "facturas.numero",
        "facturas.total",
        "facturas.totalComImposto",
        "facturas.totalSemImposto",
        "facturas.status",
        "facturas.pago",
        "facturas.factura_sigla",
        "facturas.numero_origem_factura",
        "facturas.data_origem_factura",
        "facturas.data_vencimento",
        "facturas.created_at",
        "clientes.id as cliente_id",
        "clientes.nome",
        "clientes.telefone",
        "clientes.contribuente",
        "series.nome as serie",
        "documentos.nome as documento",
        "documentos.sigla"
      )
        .from("facturas")
        .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
        .innerJoin("series", "series.id", "facturas.serie_id")
        .innerJoin("documentos", "documentos.id", "series.documento_id")
        .where("documentos.sigla", "OR")
        .where("clientes.nome", "like", "%" + search + "%")
        .orWhere("facturas.numero", "like", "%" + search + "%")
        .orWhere("facturas.pago", "like", "%" + search + "%")
        .orWhere("facturas.status", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy("facturas.created_at", "DESC")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, "", res);
  }


  async store({ request, auth, response }) {
    let linhas = [];
    var {
      produtos,
      cliente,
      totalComImposto, // total da soma das linhas de todos os impostos
      totalSemImposto, // subTotal da soma das linhas sem impostos
      total,
      serie_id,
      observacao,
      leitura,
      numero_origem_factura,
      data_origem_factura,
      data_vencimento,
      pagamento,
      contas_cliente,
      moeda,
      is_iplc
    } = request.all();

    var serieID = serie_id

    var facturacao = 'POS-PAGO';


    if (serie_id == null && auth.user.loja_id != null) {
      var s = null;
      s = await Database
      .select('series.id')
      .from("series")
      .innerJoin("lojas", "lojas.serie_id", "series.id")
      .where("activo", true)
      .where("lojas.id", auth.user.loja_id)
      .first()

      if (s == null) {
        return DataResponse.response("info", 201, "Nenhuma serie selecionada.", null);
      }
      serieID = s.id;
    } else if (serie_id == null && auth.user.loja_id == null) {
      return DataResponse.response("info", 201, "Nenhuma serie selecionada.", null);
    }

    var caixa_id = null;
    let caixa = null;


    if (auth.user.loja_id != null) {
      var dataActual = moment(new Date()).format("YYYY-MM-DD");

      var hoje = new Date();
      var dataOntem = new Date(hoje.getTime());
      dataOntem.setDate(hoje.getDate() - 1);
      dataOntem = moment(dataOntem).format("YYYY-MM-DD");

      const c = await Caixa.query()
        .whereBetween("data_abertura", ["2019-12-10", dataOntem])
        .where("is_active", 1)
        .where("user_id", auth.user.id)
        .getCount();


      if (c > 0) {
        return DataResponse.response("info", 201, "Caro operador, não lhe é permitido abrir o caixa n-vezes enquanto o caixa dos dias anterior não forem devidamente fechados.", null);
      }

      caixa = await Database.select("*")
        .from("caixas")
        .where("is_active", true)
        .where("data_abertura", dataActual)
        .where("user_id", auth.user.id)
        .first();

      if (caixa == null) {
        return DataResponse.response( "success", 201, "Não foi possivel concluir a operação porque não existe caixa aberto", caixa )
      } else {
        caixa_id = caixa.id;
      }
    }


    const isConvertLineFactura = true;

    var serie = await Database
    .select(
      "series.id as id",
      "series.nome",
      "series.proximo_numero",
      "series.activo",
      "series.movimento",
      "series.tipo_movimento",
      "documentos.nome as documento",
      "documentos.sigla",
      "series.documento_id"
    )
    .from("series")
    .innerJoin("documentos", "documentos.id", "series.documento_id")
    .where("series.id", serieID);


    var factura_sigla = serie[0].sigla + " " + serie[0].nome + "/" + serie[0].proximo_numero;
    const rules = {
      factura_sigla: "required|unique:facturas,factura_sigla",
    };
    const messages = {
      "factura_sigla.required": "É obrigatório informar o nº da factura",
      "factura_sigla.unique": "Caro operador, Lamentamos informar mas já existe uma factura com número: " + factura_sigla,
      "exists": "É obrigatório infome um registo valido no campo ",
      "integer": "É obrigatório informe um número no campo ",
    }

    const validation = await validate({ factura_sigla: factura_sigla }, rules, messages);

    if (validation.fails()) {
      return response.status(302).send(DataResponse.response("error", 302, validation.messages()[0].message + " " + validation.messages()[0].field.replace("_id", "").replace("_", " "), null));
    }


    var count = null;
    count = await Database
    .from("facturas")
    .where("facturas.factura_sigla", factura_sigla)
    .first(); // returns

    if (count != null) {
      return DataResponse.response( "info", 201, "Caro operador, Lamentamos informar que não é permitido a duplicidade de factura.", null )
    }

    var pagamento_id = null;
    var contra_valor = null;
    var cambio_id = null;
    var valor_cambio = null;
    var moeda_iso = null;

    var cambio = null;
    var valor_cativacao = 0;

    if (serie[0].sigla == "FT") {
      var cat = null;
      cat = await Database.select('tipo_entidade_cativadoras.nome as valor').from('entidade_cativadoras')
        .leftJoin('tipo_entidade_cativadoras', 'tipo_entidade_cativadoras.id', 'entidade_cativadoras.tipo_entidade_cativadora_id')
        .leftJoin('clientes', 'clientes.id', 'entidade_cativadoras.cliente_id').where('clientes.id', cliente).first();

      valor_cativacao = (cat == null ? 0 : Number(cat.valor))

    }
    var m = await Database.select("*")
      .from("moedas")
      .where("moedas.id", moeda)
      .first();

    if (moeda != null) {
      var m = await Database.select("*")
        .from("moedas")
        .where("moedas.id", moeda)
        .first();

      moeda_iso = m.codigo_iso;
      cambio = await Database.select("*")
        .from("cambios")
        .where("cambios.moeda_id", m.id)
        .where("cambios.is_active", true)
        .first();
      if (cambio != null) {
        valor_cambio = cambio.valor_cambio;
        cambio_id = cambio.id;

        if (cambio.codigo_iso != "AOA") {
          contra_valor = total / cambio.valor_cambio;
        }
      }
    } else if (serie[0].sigla == "FR") {
      var m = await Database.select("*").from("moedas").where("moedas.codigo_iso", "AOA").first();
      moeda_iso = m.codigo_iso;
      moeda = m.id;
    }
    if (serie[0].sigla == "NC") {

      var data = await Database.from("facturas").where("facturas.factura_sigla", numero_origem_factura).first(); // returns

      facturacao = data.facturacao
      moeda_iso = data.codigo_iso;
      moeda = data.moeda_id;
      contas_cliente.conta_id = data.conta_id,
      contas_cliente.contrato_id = data.contrato_id
    }

    var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    var InvoiceDate = moment(new Date()).format("YYYY-MM-DD");
    var SystemEntryDate = moment(new Date()).format("YYYY-MM-DD") + "T" + moment(new Date()).format("HH:mm:ss");
    var InvoiceNo = factura_sigla; //serie[0].sigla+" "+serie[0].nome+"/"+serie[0].proximo_numero;
    var GrossTotal = numeral(total).format("0.00");
    //InvoiceDate, SystemEntryDate, InvoiceNo, GrossTotal e Hash

    var l_hash = InvoiceDate + ";" + SystemEntryDate + ";" + InvoiceNo + ";" + GrossTotal + ";";

    const max_n = await Database.from("facturas")
      .where("serie_id", serie_id)
      .max("numero as total");
    var cc = 0;

    if (max_n[0].total == null) {
      cc = 0;
    } else {
      cc = max_n[0].total;
    }

    if (cc != 0) {
      const doc_ant = await Database.select("hash as hash")
        .from("facturas")
        .where("serie_id", serie_id)
        .where("numero", cc)
        .first();
      l_hash = l_hash + "" + doc_ant.hash;
    }


    if (serie[0].sigla == "FR") {
      const pagament = await Pagamento.create({
        valor_recebido: pagamento.total_valor_recebido,
        troco: pagamento.troco,
        total_pago: pagamento.total_pago,
        documento_sigla: serie[0].sigla,
        user_id: auth.user.id
      });

      for (let index = 0; index < pagamento.linha_pagamentos.length; index++) {
        await LinhaPagamento.create({
          valor_recebido: pagamento.linha_pagamentos[index].valor_entrada,
          referencia: pagamento.linha_pagamentos[index].referencia_banco,
          data_pagamento: pagamento.linha_pagamentos[index].data_pagament,
          forma_pagamento_id: pagamento.linha_pagamentos[index].id,
          pagamento_id: pagament.id,
          user_id: auth.user.id
        });
      }

      pagamento_id = pagament.id;
    }

    const factura = await Factura.create({
      numero: serie[0].proximo_numero,
      factura_sigla:
        serie[0].sigla + " " + serie[0].nome + "/" + serie[0].proximo_numero,
      serie_id: serie[0].id,
      totalComImposto:
        moeda_iso != "AOA" &&
          cambio != null &&
          moeda_iso != null &&
          isConvertLineFactura == true
          ? numeral(totalComImposto).format("0.00") / cambio.valor_cambio
          : numeral(totalComImposto).format("0.00"),
      totalSemImposto:
        moeda_iso != "AOA" &&
          cambio != null &&
          moeda_iso &&
          isConvertLineFactura == true
          ? numeral(totalSemImposto).format("0.00") / cambio.valor_cambio
          : numeral(totalSemImposto).format("0.00"),
      total:
        moeda_iso != "AOA" &&
          cambio != null &&
          moeda_iso &&
          isConvertLineFactura == true
          ? numeral(total).format("0.00") / cambio.valor_cambio
          : numeral(total).format("0.00"),
      totalKwanza: total,
      valor_aberto: (serie[0].sigla == "FR" || serie[0].sigla == "NC" ? 0 : moeda_iso != "AOA" && cambio != null && moeda_iso && isConvertLineFactura == true ? numeral(total - (totalComImposto * (valor_cativacao / 100))).format("0.00") / cambio.valor_cambio : numeral(total - (totalComImposto * (valor_cativacao / 100))).format("0.00")),
      valor_cativo: valor_cativacao,
      imposto_cativo: (totalComImposto * (valor_cativacao / 100)),
      pago: serie[0].sigla == "FR" || serie[0].sigla == "NC" ? true : false,
      status: "N",
      status_date: data,
      systemEntryDate: data,
      observacao: observacao,
      leitura: leitura,
      numero_origem_factura: numero_origem_factura,
      data_origem_factura: "" + (data_origem_factura == null ? null : data_origem_factura),
      data_vencimento: data_vencimento,
      hash: this.gearAss(l_hash),
      hash_control: 1, //l_hash,
      is_nota_credito: serie[0].sigla == "NC" ? true : false,
      cliente_id: cliente,
      user_id: auth.user.id,
      pagamento_id: pagamento_id,
      caixa_id: caixa_id,
      conta_id: contas_cliente.conta_id,
      contrato_id: contas_cliente.contrato_id,
      moeda_id: moeda,
      is_iplc: serie[0].sigla == "NC" ? is_iplc : is_iplc,
      total_contra_valor: contra_valor,
      cambio_id: cambio_id,
      valor_cambio: valor_cambio,
      moeda_iso: moeda_iso,
      facturacao: facturacao
    });
    await Database.table("series").where("id", serie[0].id).update("proximo_numero", Number(serie[0].proximo_numero) + 1);

    if (serie[0].sigla == "NC") {

      var fact = await Factura.query().with("serie").with("serie.documento").with("cliente").where("status", "N").where("factura_sigla", numero_origem_factura).first();
      fact = fact.toJSON();
      var vaberto = (fact.serie.documento.sigla == 'FR' ? 0 : numeral(fact.valor_aberto).format("0.00") - numeral(total).format("0.00"));

      await Database.table("facturas").where("factura_sigla", numero_origem_factura)
        .update({ valor_aberto: numeral(vaberto).format("0.00"), pago: (vaberto == 0 ? true : false), is_nota_credito: 2 });
    }

    if (serie[0].sigla == "FT" || serie[0].sigla == "FR") {

      for (var x = 0; x < pagamento.bancos.length; x++) {
        //associando projectos com as tecnologias aplicadas neles
        await FacturaBanco.create({
          factura_id: factura.id,
          banco_id: pagamento.bancos[x].id
        });
      }
    }

    for (let index = 0; index < produtos.length; index++) {
      linhas[0] = await LinhaFactura.create({
        artigo_id: produtos[index].produto_id,
        servico_id: serie[0].sigla == "NC" ? produtos[index].servico_id : contas_cliente.servico_id,
        factura_id: factura.id,
        user_id: auth.user.id,
        valorProdutoKwanza: produtos[index].valor,
        valor:
          moeda_iso != "AOA" &&
            cambio != null &&
            moeda_iso &&
            isConvertLineFactura == true
            ? numeral(produtos[index].valor).format("0.00") /
            cambio.valor_cambio
            : numeral(produtos[index].valor).format("0.00"),
        quantidade: produtos[index].quantidade,
        total:
          moeda_iso != "AOA" &&
            cambio != null &&
            moeda_iso &&
            isConvertLineFactura == true
            ? numeral(produtos[index].total).format("0.00") /
            cambio.valor_cambio
            : numeral(produtos[index].total).format("0.00"),
        linhaTotalSemImposto:
          moeda_iso != "AOA" &&
            cambio != null &&
            moeda_iso &&
            isConvertLineFactura == true
            ? numeral(produtos[index].linhaTotalSemImposto).format("0.00") /
            cambio.valor_cambio
            : numeral(produtos[index].linhaTotalSemImposto).format("0.00"),
        valor_imposto:
          moeda_iso != "AOA" &&
            cambio != null &&
            moeda_iso &&
            isConvertLineFactura == true
            ? numeral(produtos[index].valorImposto).format("0.00") /
            cambio.valor_cambio
            : numeral(produtos[index].valorImposto).format("0.00"),
        valor_desconto:
          moeda_iso != "AOA" &&
            cambio != null &&
            moeda_iso &&
            isConvertLineFactura == true
            ? numeral(produtos[index].desconto).format("0.00") /
            cambio.valor_cambio
            : numeral(produtos[index].desconto).format("0.00"),
        imposto_id: produtos[index].imposto_id,
        observacao: produtos[index].observacao,
        cambio_id: cambio_id,
        valor_cambio: valor_cambio,
        moeda_iso: isConvertLineFactura == true ? moeda_iso : "AOA",
      });


    }




    return DataResponse.response(
      "success",
      200,
      (serie[0].sigla == "FT"
        ? "Factura"
        : serie[0].sigla == "FR"
          ? "Factura/Recibo"
          : serie[0].sigla == "OR"
            ? "Orçamento"
            : "Nota de Crédito") +
      " gerada com sucesso",
      factura
    );
  }


  async facturarServicosContrato({ request, auth, response }) {

    var dataActual = moment(new Date()).format("YYYY-MM-DD");

    const {

      cliente_id,
      gestor_cliente_id,
      conta_id,
      servicos,
      total_sem_imposto,
      total_com_imposto,
      valor_total_imposto,
      valor_aberto,
      valor_facturar,

    } = request.all();


    const configuracoes = await Database.select("*")
      .table("configuracaos")
      .where("slug", 'moeda_id_default').first();


    const moeda = await Database.select("*")
    .table("moedas")
    .where("id", Number(configuracoes.valor)).first();
    //console.log(conta_cliente);

    const gestor_cliente = await Database.select("*")
      .table("users")
      .where("id", gestor_cliente_id).first();
      //console.log(gestor_cliente);


    const agencia = await Database.select("*")
      .table("lojas")
      .where("id", auth.user.loja_id).first();

    const serie = await Database.select("*")
      .table("series")
      .where("id", 1).first();

    const documento = await Database.select("*")
      .table("documentos")
      .where("id", serie.documento_id).first();


   var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
   var InvoiceDate = moment(new Date()).format("YYYY-MM-DD");
   var SystemEntryDate = moment(new Date()).format("YYYY-MM-DD") + "T" + moment(new Date()).format("HH:mm:ss");
   var InvoiceNo = documento.sigla + " " + serie.nome + "/" + serie.proximo_numero;
   var GrossTotal = numeral(total_com_imposto).format("0.00");
   //InvoiceDate, SystemEntryDate, InvoiceNo, GrossTotal e Hash

   var l_hash = InvoiceDate + ";" + SystemEntryDate + ";" + InvoiceNo + ";" + GrossTotal + ";";

    const factura = await Factura.create({
      numero: serie.proximo_numero,
      factura_sigla: documento.sigla + " " + serie.nome + "/" + serie.proximo_numero,
      hash: this.gearAss(l_hash),
      hash_control: 1,
      totalComImposto: 0,
      serie_id: serie.id,
      status_date: data,
      SystemEntryDate: SystemEntryDate,
      totalSemImposto: total_sem_imposto,
      totalComImposto: total_com_imposto,
      total: total_com_imposto,
      totalKwanza: total_com_imposto,
      valor_aberto: valor_aberto,
      pago: (total_com_imposto == valor_facturar) ? true : false,
      moeda_id: moeda.id,
      moeda_iso: moeda.codigo_iso,
      status: "N",
      hash: this.gearAss(l_hash),
      hash_control: 1,
      is_nota_credito: documento.sigla == "NC" ? true : false,
      cliente_id: cliente_id,
      conta_id: conta_id,
      facturacao: "PRE-PAGO",

      //pagamento_id: pagamento_id,

      //contrato_id: contas_cliente.contrato_id,
      //moeda_id: moeda,
      //total_contra_valor: contra_valor,
      user_id: auth.user.id
    });


    let linhas_facturacao = [];
    let servicos_result = [];

    for (let index = 0; index < servicos[index].length; index++) {
      let item = servicos[index];

      if (item.facturar) {

        let valor_imposto = ((item.imposto_valor) ? item.imposto_valor : 0) * item.servico_valor;
        let servico_valor = item.servico_valor + valor_imposto;

        item.facturar = false;
        item.facturado = true;

        servicos_result = item;

        linhas_facturacao = await LinhaFactura.create({
          factura_id: factura.id,
          artigo_id: item.produto_id,
          servico_id: item.produto_id,
          valor: servico_valor,
          valor_imposto: valor_imposto,
          valor_desconto: 0,
          valor_cambio: 0,
          imposto_id: item.imposto_id,
          valorProdutoKwanza: servico_valor,
          moeda_id: moeda.id,
          moeda_iso: moeda.codigo_iso,
          user_id: auth.user.id,
          quantidade: 1,
          total: servico_valor,
        });

      }


    }




    return DataResponse.response(
      "success",
      200,
      (documento.sigla == "FT"
        ? "Factura"
        : documento.sigla == "FR"
          ? "Factura/Recibo"
          : documento.sigla == "OR"
            ? "Orçamento"
            : "Nota de Crédito") +
      " gerada com sucesso", {
      factura: factura,
      servicos: servicos,
      linhas_facturacao: linhas_facturacao
    }
    );


  }


  async show({ params }) {
    var data = await Factura.findById(params.id);
    if (data == null) {
      return DataResponse.response("success", 500, "Nenhum resultado encontrado", null);
    }
    return DataResponse.response("success", 200, "", data);
  }

  async getFacturasNaoPagas({ request }) {
    const { cliente_id } = request.all();

    const facturas = await Database.select(
      "facturas.id",
      "facturas.numero",
      "facturas.total",
      "facturas.valor_aberto",
      "facturas.factura_sigla",
      "facturas.created_at",
      "facturas.total",
      "facturas.pago",
      "users.nome",
      "facturas.cliente_id"
    )
      .from("facturas")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .innerJoin("users", "users.id", "facturas.user_id")
      .where("facturas.cliente_id", cliente_id)
      .where("facturas.pago", false)
      .where("facturas.status", "N")
      .where("documentos.sigla", "FT");

    console.log(cliente_id);

    if (facturas.length == 0) {
      return DataResponse.response(
        "error",
        500,
        "Nenhum resultado encontrado",
        null
      );
    }

    return DataResponse.response("success", 200, "", facturas);
  }

  async update({ params, request, response }) { }

  async destroy({ params, request, response }) { }


  async getFacturaNotaCrediro({ request, response }) {
    const { cliente_id } = request.all();

    const facturas = await Database.select(
      "facturas.id",
      "facturas.numero",
      "created_at as data",
      "facturas.total"
    )
      .from("facturas")
      .where({
        "facturas.cliente_id": cliente_id,
        status: "N",
        pago: false
      });

    return response
      .status(200)
      .send(DataResponse.response("success", 200, "", facturas));
  }


  async getLinhasFacturaNotaCrediro({ request, response }) {
    const { factura_id } = request.all();

    const linhas = await Database.select(
      "linha_facturas.id as linha_id",
      "linha_facturas.artigo_id",
      "linha_facturas.total",
      "linha_facturas.quantidade",
      "linha_facturas.valor_imposto",
      "linha_facturas.valor_desconto",
      "linha_facturas.factura_id",
      "linha_facturas.artigo_id",
      "produtos.nome"
    )
      .from("linha_facturas")
      .innerJoin("produtos", "produtos.id", "linha_facturas.artigo_id")
      .where({
        factura_id: factura_id
      });

    return response
      .status(200)
      .send(DataResponse.response("success", 200, "", linhas));
  }

  gearAss(string) {
    const crypto = require("crypto");
    var s = crypto.createSign("RSA-SHA1");
    var key =
      "-----BEGIN RSA PRIVATE KEY-----\n" +
      "MIICXQIBAAKBgQC4faWshk9wvZUouz4A3K4Zzb2NOtbp262HcB1mJYF1QDs3wAnd" +
      "kGiqPcBx7TGeIEjuBtg6DFtSy29w1dRCANdqIDqaCqX+/PNE8dz8foCauiy5OEU2" +
      "segqAeN3X8PXBevqGThd/x9OPJ4pV2Kgx/oAs7Bwg3/C2AM3qraj0UulhwIDAQAB" +
      "AoGAW0RlQk0LXaWb9ZNzn++L/V3niMdz7Crt1JOlJ5QkUAHfibvp5X78GEQGQRXr" +
      "NuOX0JD4RPc58mKLldFieOh7p8B/dx8UZyWd11TUOnVwOSJaFd3rwnHzobEUJgH2" +
      "24b1bGOWsk+0XEisS1B7xl4d8T74+Dpnpugg4nU/1rAKgjECQQDfpe5Nihi9Fgfz" +
      "rcr8s9oGGdKV7nyVXUmBN5Dm5PMfAev49Wo6ZvhO9EW1mb15Kuqfc56Sq5ErDdRg" +
      "MPnODP8JAkEA0y2nOcjWn3ZsX0lPvGpKotnFUgO4WlpJfd6fzxTfQrLqHf6ixFPt" +
      "wTApqhU0fx9xMWl6m1Kh0WiegMYk8LwUDwJATafsCvh8hotzz2T1KrG4bo3g1Tau" +
      "A58Uus10fvfYg1fDe/qbHBRM+/1NhzUO2VfRh/Q5h2wTSAPRTmUzGBzjIQJBAIQw" +
      "z70cOz0WpEABZChNYOsP5rSwH3ZvjhF8igzWw+q8lFCyVLEQ2INV4r7VB0eMJw8H" +
      "N/iCgUjUdGOnpPgMw4ECQQCboFsTKkzrLOkZJiipgid08xPiBJCfd5Pjl7ggnwoj" +
      "CL9JXAsSBvOTvTuo2XnBHOpaWO8oOEUt5xBbUPGLhdaA" +
      "\n-----END RSA PRIVATE KEY-----";
    s.update(string);
    var signature = s.sign(key, "base64");
    return signature;
  }

  async privateKey({ request }) {
    const { string } = request.all();
    var r = this.gearAss(string);
    //var data = moment(new Date()).format("YYYY-MM-DDTHH:mm:ss");
    /*var CurrentDate = new Date();

    var CurrentDate = new Date("2019-02-01");
    var ultDia = new Date(CurrentDate.getFullYear(), CurrentDate.getMonth() + 1, 0);
     var ultimoDia = moment(ultDia).format("DD"); */
    return DataResponse.response("success", 200, "", { r, string });
  }


  async anular({ params, request, auth }) {
    var moment = require("moment");
    const { id } = auth.user
    var statusData = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    const dados = {
      status: "A",
      status_reason: request.input("status_reason"),
      status_date: statusData,
      anulado_user_id: id
    };

    // update with new data entered
    let factura = await Factura.findById(params.id);

    if (factura == null) { return DataResponse.response("error", 500, "Nenhum resultado encontrado", null); }
    else if (factura.status == "A") {
      return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " já se encontra anulada.", null);
    }

    if (factura.serie.documento.sigla != "NC" && factura.__meta__.total_recibos > 0 && factura.__meta__.totalReciboAnulados != 0) {
      return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " não pode ser anulada porque já possui recebimentos", factura);
    } else {
      if (factura.serie.documento.sigla != "NC") {
        var count_nc = 0;
        if (factura.notas_credito.length > 0) {
          for (let index = 0; index < factura.notas_credito.length; index++) {
            const element = factura.notas_credito[index];
            count_nc += element.status == "N" ? 1 : 0;
          }
          if (count_nc > 0) {
            return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " poosui " + count_nc + " nota de crédito que não está anulada", null);
          }
        }
      }

      if (factura.serie.documento.sigla == "FR") {
        if (factura.adiantamento != null) {
          if (factura.adiantamento.saldado == 1) {
            return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " não pode ser anulada porque o seu adiantamento já foi utilizado", null);
          }
        }
      }
      await Factura.query().where("id", factura.id).update(dados);
      if (factura.serie.documento.sigla == "FR") {
        const annulment = { status: "A", annulment_reason: request.input("status_reason"), annulment_date: statusData };
        await Pagamento.query().where("id", factura.pagamento_id).update(annulment);
        if (factura.pagamento !== null) {
          const maNew = await Database.select('*').from("movimento_adiantamentos").where('saldado', 0).where("saldado_factura", factura.id);

          if (maNew.length != 0) {
            await Database.raw("UPDATE adiantamentos SET valor = valor - (SELECT SUM(valor) FROM movimento_adiantamentos WHERE saldado = 0 AND saldado_factura =" + factura.id + ") WHERE cliente_id = " + factura.cliente_id);
            //await Database.raw("UPDATE movimento_adiantamentos SET saldado = 1, status = 'N' WHERE saldado_factura = " + factura.id);
            const ad = { saldado: 1, status: "A", annulment_reason: request.input("status_reason"), annulment_date: statusData };
            await MovimentoAdiantamento.query().where("saldado_factura", factura.id).update(ad);
          }
          const maOld = await Database.select('*').from("movimento_adiantamentos").where('saldado', 1).where("factura_id", factura.id);

          if (maOld.length != 0) {
            await Database.raw("UPDATE adiantamentos SET valor = (SELECT SUM(valor) FROM movimento_adiantamentos WHERE saldado = 1 AND factura_id =" + factura.id + ") WHERE cliente_id = " + factura.cliente_id);
            await Database.raw("UPDATE movimento_adiantamentos SET saldado = 0 WHERE factura_id = " + factura.id);
          }

        }
      }

      if (factura.serie.documento.sigla == 'NC') {
        var fact = await Factura.query().with("serie").with("serie.documento").where("factura_sigla", factura.numero_origem_factura).first();
        fact = fact.toJSON();
        var vaberto = (fact.serie.documento.sigla == 'FR' ? 0 : fact.valor_aberto + factura.total);

        await Database.table("facturas").where("factura_sigla", factura.numero_origem_factura).update({ valor_aberto: numeral(vaberto).format("0.00"), pago: fact.serie.documento.sigla == 'FR' ? 1 : 0 });
      }

      if (factura.serie.documento.sigla == 'FT') {
      /*   var fact_periodo = await Database.select(
          "bill_run_headers.ano",
          "bill_run_headers.mes",
          "bill_runs.conta_id as conta"
        )
          .from("bill_runs").innerJoin("bill_run_headers", "bill_runs.bill_run_header_id", "bill_run_headers.id")
          .where("bill_runs.factura_utilitie_id", factura.id);


        console.log(fact_periodo[0].ano)
        var periodo = "" + fact_periodo[0].ano + "" + (fact_periodo[0].mes < 10 ? "0" + fact_periodo[0].mes : fact_periodo[0].mes);
        console.log(periodo)
        await Database.raw("UPDATE charges SET is_facturado = 0 WHERE conta_id = " + fact_periodo[0].conta + " and periodo =" + periodo);
       */
      await Database.raw("UPDATE facturas SET status = 'A' WHERE id = " + factura.id );
      }
    }

    return DataResponse.response("success", 200, "Factura anulada com sucesso.", factura);
  }


  async gerarFactura({ params, request, response }) {
    let data = await Factura.gerarFactura(params.id);
    return DataResponse.response("success", 200, "Sucesso", data);
  }


  async findFactFromNc({ params }) {
    let data = await Factura.findFactFromNc(params.id);
    return DataResponse.response("success", data.code, data.message, data.data);
  }


  async saftXMLDocument({ request }) {
    const { ano, de, para } = request.all();

    let impostos = null;
    let clientes = null;
    let produtos = null;
    const facturas = [];

    //clientes =  await Database.select('').from('clientes').limit(5);
    clientes = await Cliente.all();
    impostos = await Imposto.all();
    produtos = await Produto.all();
    const allFacturas = await Database.select(
      "facturas.cliente_id",
      "facturas.factura_sigla",
      "facturas.observacao",
      "facturas.created_at",
      "facturas.hash",
      "facturas.hash_control",
      "facturas.status",
      "facturas.status_date",
      "facturas.status_reason",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.user_id",
      "facturas.id",
      "facturas.numero",
      "facturas.numero_origem_factura",
      "facturas.data_origem_factura",
      "users.nome as user",
      "clientes.nome as cliente",

      "series.nome as serie",
      "documentos.sigla"
    )
      .from("facturas")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("users", "users.id", "facturas.user_id")
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .whereIn("documentos.sigla", ["NC", "FT", "FR", "ND"])
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%m")'), [
        de,
        para
      ])
      .where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y")'), ano);

    if (allFacturas[0] == undefined) {
      return DataResponse.response(
        "success",
        500,
        "Nenhum Resultado encotrado. ",
        null
      );
    }
    for (let oneFactura of allFacturas) {
      const linhaFacturas = await Database.select(
        "produtos.nome",
        "produtos.barcode",
        "produtos.valor as valorProduto",
        "produtos.tipo as tipoProduto",
        "produtos.barcode",
        "produtos.tipo as tipoProduto",

        "linha_facturas.valor",
        "linha_facturas.quantidade",
        "linha_facturas.total",
        "linha_facturas.valor_desconto",
        "linha_facturas.quantidade",
        "linha_facturas.artigo_id",
        "linha_facturas.imposto_id",
        "linha_facturas.linhaTotalSemImposto",
        "impostos.descricao as taxType",
        "impostos.codigo as codigoImposto",
        "impostos.valor as taxCode",
        "linha_facturas.valor_imposto as taxAmount"
      )
        .from("linha_facturas")
        .innerJoin("produtos", "produtos.id", "linha_facturas.artigo_id")
        .innerJoin("impostos", "impostos.id", "produtos.imposto_id")
        .where("linha_facturas.factura_id", oneFactura.id);

      facturas.push({ oneFactura, linhaFacturas });
    }

    let data = {
      clientes: clientes,
      impostos: impostos,
      produtos: produtos,
      facturas: facturas
    };

    return DataResponse.response(
      "success",
      200,
      "Factura anulada com sucesso ",
      data
    );
  }

  async dashboardFacturacao() {
    const countRecibos = await Recibo.getCount();

    const countFacturas = await Database.select("facturas.id")
      .from("facturas")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("status", "N")
      .whereIn("documentos.sigla", ["FT", "FR"])
      .getCount();

    const countFacturasVencidas = await Database.select("facturas.id")
      .from("facturas")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("pago", false)
      .where(
        Database.raw('DATE_FORMAT(facturas.data_vencimento, "%d-%m-%Y")'),
        "<",
        Database.raw('DATE_FORMAT(facturas.created_at, "%d-%m-%Y")')
      )
      .where("documentos.sigla", "FT")
      .getCount();

    const countFacturasContaCorrente = await Database.select("facturas.id")
      .from("facturas")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .where("pago", false)
      .where("documentos.sigla", "FT")
      .where("status", "N")
      .getCount();

    return DataResponse.response("success", 200, null, {
      countRecibos: countRecibos,
      countFacturas: countFacturas,
      countFacturasVencidas: countFacturasVencidas,
      countFacturasContaCorrente: countFacturasContaCorrente
    });
  }


}

module.exports = FacturaController;
