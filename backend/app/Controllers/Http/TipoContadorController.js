'use strict'
const DataResponse = use("App/Models/DataResponse");
const TipoContador = use("App/Models/TipoContador");
const Database = use("Database");

class TipoContadorController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_contadors")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_contadors")
        .orWhere("tipo_contadors.slug", "like", "%" + search + "%")
        .orWhere("tipo_contadors.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    const Verify = await TipoContador.query()
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe um semelhante",
        Verify
      );

    } else {
      const tipoContador = await TipoContador.create({
        descricao: descricao,
        slug: slug,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const Verify = await TipoContador.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (Verify > 0) {
    return DataResponse.response(
      null,
      201,
      "Já existe um semelhante",
      Verify
    );
  }else{

    // update with new data entered
    const tipocontadors = await TipoContador.find(params.id);
    tipocontadors.merge(data);
    await tipocontadors.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tipocontadors
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('tipo_contadors')
    .orderBy('descricao', 'ASC');
     
    return res;
  }
}

module.exports = TipoContadorController
