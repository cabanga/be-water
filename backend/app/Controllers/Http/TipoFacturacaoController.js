'use strict'
const DataResponse = use("App/Models/DataResponse");
const TipoFacturacao = use("App/Models/TipoFacturacao");
const Database = use("Database");
var moment = require("moment");

class TipoFacturacaoController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_facturacaos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("tipo_facturacaos")
        .orWhere("tipo_facturacaos.slug", "like", "%" + search + "%")
        .orWhere("tipo_facturacaos.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug} = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");
    
    const Verify = await Database.table('tipo_facturacaos')
      .where("slug", slug)
      .getCount(); 

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo Facturação com este slug",
        Verify
      );

    } else {
      const tipo_facturacaos = await Database.table('tipo_facturacaos').insert({
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        tipo_facturacaos
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao","slug"]);

    const Verify = await TipoFacturacao.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (Verify > 0) {
    return DataResponse.response(
      null,
      201,
      "Já existe Tipo Facturação com este slug",
      Verify
    );
  }else{

    // update with new data entered
    const tipo_facturacaos = await TipoFacturacao.find(params.id);
    tipo_facturacaos.merge(data);
    await tipo_facturacaos.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tipo_facturacaos
      );
    }
  }

  async selectBox ({ request }) {
   
    var res = await Database.select('id', 'descricao', 'slug')
    .from('tipo_facturacaos')
    .orderBy('descricao', 'ASC');
        
      return res;
    }
  
}

module.exports = TipoFacturacaoController
