'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Pedido = use("App/Models/Pedido");
const LogEstadoPedido = use("App/Models/LogEstadoPedido");
const LogsContadore = use("App/Models/LogsContadore");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");
const Mensagem = use('App/Models/ResponseMessage');

/**
 * Resourceful controller for interacting with pedidos
 */
class PedidoController {
  /**
   * Show a list of all pedidos.
   * GET pedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params }) {
    //const pedidos = await Pedido.query().where("cliente_id", params.id).fetch(); 
    const pedidos = await Database.select(
      'pe.id as id',
      'pe.cliente_id as cliente_id',
      'pe.tipoPedido as tipoPedido',
      'pe.telefone as telefone',
      'pe.dataPedido as dataPedido',
      'pe.observacao as observacao',
      'ta.descricao as tarifario')
      .table('pedidos as pe')
      .leftJoin('tarifarios as ta', 'ta.id', 'pe.tarifario_id')
      .where("pe.cliente_id", params.id)

    return DataResponse.response("success", 200, "", pedidos);
  }

  /**
   * Render a form to be used for creating a new pedido.
   * GET pedidos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new pedido.
   * POST pedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth, response }) {
    const { conta_id, tarifario_id, residencia_id } = request.all();


    const estadoPedido = await Database.select("id")
      .table("estado_pedidos")
      .where("sigla", "ABERTA").first();

    const getPedidoResidencia = await Database.select("*")
      .table("pedidos")
      .where("residencia_id", residencia_id).first();


    if (getPedidoResidencia) {
      return DataResponse.response(
        "success",
        201,
        "Já existe um pedido feito a essa Residência",
        null
      );
    }


    /*  const pedido = await Pedido.create({
       conta_id: conta_id,
       tecnologia_id: tecnologia_id,
       tarifario_id: tarifario_id,
       residencia_id: residencia_id,
       estado: estadoPedido.id,
       user_id: auth.user.id
     }); */

    let pedido = await Pedido.pedidoStore(conta_id, tarifario_id, residencia_id, auth.user.id);

    return DataResponse.response("success", 200, "Pedido registado com sucesso", pedido);

  }

  /**
   * Display a single pedido.
   * GET pedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing pedido.
   * GET pedidos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  async gerarPdfInstalacao({ params }) {

    let cliente = null;
    let pedido = null;

    pedido = await Database.select(Database.raw('DATE_FORMAT(pe.dataPedido, "%d-%m-%Y") as pedidoData'), 'pe.id as id', 'pe.id as codigo', 'pe.cliente_id as cliente_id', 'pe.tipoPedido as tipoPedido', 'pe.telefone as telefone', 'pe.dataPedido as dataPedido', 'pe.observacao as observacao', 'ta.descricao as tarifario')
      .table('pedidos as pe')
      .leftJoin('tarifarios as ta', 'ta.id', 'pe.tarifario_id')
      .where("pe.id", params.id).first()

    //console.log(pedido)
    cliente = await Database.select('cli.genero as genero', 'pro.nome as clienteProvincia', 'cli.observacao as obsCliente', 'tc.tipoClienteDesc as tipoCliente', 'cli.nome as clienteNome', 'cli.telefone as clienteTelefone', 'cli.morada as clienteMorada', 'cli.contribuente  as clieteContribuente', 'td.tipoIdentificacao as tipoIdentificacao')
      .table('clientes as cli')
      .innerJoin("pedidos as pe", "pe.cliente_id", "cli.id")
      .leftJoin("tipo_identidades as td", "td.id", "cli.tipo_identidade_id")
      .leftJoin("provincias as pro", "pro.id", "cli.province")
      .leftJoin("tipo_clientes as tc", "tc.id", "cli.tipo_cliente_id")
      .where("pe.cliente_id", pedido.cliente_id).first()

    let data = {
      cliente: cliente,
      pedido: pedido,

    };

    //console.log(data)


    return DataResponse.response("success", 200, "", data);
  }

  /**
   * Update pedido details.
   * PUT or PATCH pedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ request, auth }) {
    const {

      contrato_id,
      pedido_id,
      conta_id,
      local_instalacao_id,
      contador_id,
      estado_ordem_servico_id,
      estado_ordem_servico_slug,
      observacao,

    } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD HH:MM:ss");

    let tabela = "local_consumos";

    const contador = await Database.select("id", "observacao", "ultima_leitura")
      .from("contadores")
      .where("id", contador_id).first();

    /* const estado_ordem_servico = await Database.select("id", "slug")
      .table("estado_ordem_servicos")
      .where("id", estado_ordem_servico_id).first(); */

    const estado_contador = await Database.select("id")
      .table("estado_contadores")
      .where("slug", "INSTALADO").first();

    const pedido_old = await Database.select("*")
      .table("pedidos")
      .where("id", pedido_id).first();

    const local_consumo = await Database.table('local_consumos')
      .where('local_instalacao_id', local_instalacao_id)
      .where('conta_id', conta_id)
      .where('contrato_id', contrato_id)
      .update({
        'is_active': true,
        'contador_id': contador_id,
        'updated_at': dataActual
      });

    await Database
      .table('pedidos')
      .where('id', pedido_id)
      .update({
        'estado_ordem_servico_id': estado_ordem_servico_id,
        'observacao': observacao,
        'updated_at': dataActual,
        'local_consumo_id': local_consumo.id

      })

    await Database
      .table('contratoes')
      .where('id', contrato_id)
      .update({
        'estado_contrato_id': 1,
        'activation_date': dataActual
      })

    // ACTUALIZA O CONTADOR PARA EM USO
    await Database
      .table('contadores')
      .where('id', contador_id)
      .update({
        'estado_contador_id': estado_contador.id,
        'updated_at': dataActual

      })

    await LogEstadoPedido.create({ 'pedido_id': pedido_id, 'id_estado_anterior': null, 'id_estado_novo': null, 'user_id': auth.user.id, 'observacao_old': pedido_old.observacao, 'observacao_new': observacao })

    const user = await LogsContadore.create({
      'contador_id': contador_id,
      'estado_contador_id': estado_contador.id,
      'ultima_leitura': contador.ultima_leitura,
      'user_id': auth.user.id,
      'local_instalacao_id': local_instalacao_id,
      'local_consumo_id': local_consumo.id,
      'operacao': "CONTRATO LEITURA",
      'tabela': tabela,
      'conta_id': pedido_old.conta_id,
      'observacao_new': observacao
    })

    console.log(user);

    return DataResponse.response("success", 200, "Ordem submetida com sucesso", local_consumo);
  }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select('pedidos.created_at as created_at',
        'pedidos.id',
        'pedidos.tipo_ordem_servico_id',
        'tipo_ordem_servicos.descricao as tipo_ordem_servico',
        'pedidos.contrato_id',
        'contratoes.tipo_contracto_id',
        'contratoes.tarifario_id',
        'tarifarios.descricao as tarifario',
        'tipo_contratoes.descricao as tipo_contrato',
        'contratoes.tipo_medicao_id',
        'tipo_medicaos.nome as tipo_medicao',
        'tipo_medicaos.slug as tipo_medicao_slug',
        'contratoes.conta_id',
        'contas.numero_conta',
        'contas.cliente_id',
        'clientes.nome as cliente',
        'clientes.morada as cliente_morada',
        'clientes.telefone as cliente_telefone',
        'pedidos.estado_ordem_servico_id',
        'estado_ordem_servicos.nome as estado_ordem_servico',
        'estado_ordem_servicos.slug as estado_ordem_servico_slug',
        'pedidos.local_instalacao_id',
        'pedidos.observacao'
      )
        .table('pedidos')
        .innerJoin('contratoes', 'pedidos.contrato_id', 'contratoes.id')
        .innerJoin('contas', 'contratoes.conta_id', 'contas.id')
        .innerJoin('clientes', 'contas.cliente_id', 'clientes.id')
        .leftJoin('tipo_ordem_servicos', 'pedidos.tipo_ordem_servico_id', 'tipo_ordem_servicos.id')
        .leftJoin('estado_ordem_servicos', 'pedidos.estado_ordem_servico_id', 'estado_ordem_servicos.id')
        .leftJoin('tarifarios', 'pedidos.tarifario_id', 'tarifarios.id')
        .leftJoin('tipo_medicaos', 'contratoes.tipo_medicao_id', 'tipo_medicaos.id')
        .leftJoin('tipo_contratoes', 'contratoes.tipo_contracto_id', 'tipo_contratoes.id')

        .orderBy(orderBy == null ? 'pedidos.created_at' : orderBy, 'DESC')
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select('pedidos.created_at as created_at',
        'pedidos.id',
        'pedidos.tipo_ordem_servico_id',
        'tipo_ordem_servicos.descricao as tipo_ordem_servico',
        'pedidos.contrato_id',
        'contratoes.tipo_contracto_id',
        'contratoes.tarifario_id',
        'tarifarios.descricao as tarifario',
        'tipo_contratoes.descricao as tipo_contrato',
        'contratoes.tipo_medicao_id',
        'tipo_medicaos.nome as tipo_medicao',
        'tipo_medicaos.slug as tipo_medicao_slug',
        'contratoes.conta_id',
        'contas.numero_conta',
        'contas.cliente_id',
        'clientes.nome as cliente',
        'clientes.morada as cliente_morada',
        'clientes.telefone as cliente_telefone',
        'pedidos.estado_ordem_servico_id',
        'estado_ordem_servicos.nome as estado_ordem_servico',
        'estado_ordem_servicos.slug as estado_ordem_servico_slug',
        'pedidos.local_instalacao_id',
        'pedidos.observacao'
      )
        .table('pedidos')
        .innerJoin('contratoes', 'pedidos.contrato_id', 'contratoes.id')
        .innerJoin('contas', 'contratoes.conta_id', 'contas.id')
        .innerJoin('clientes', 'contas.cliente_id', 'clientes.id')
        .leftJoin('tipo_ordem_servicos', 'pedidos.tipo_ordem_servico_id', 'tipo_ordem_servicos.id')
        .leftJoin('estado_ordem_servicos', 'pedidos.estado_ordem_servico_id', 'estado_ordem_servicos.id')
        .leftJoin('tarifarios', 'pedidos.tarifario_id', 'tarifarios.id')
        .leftJoin('tipo_medicaos', 'contratoes.tipo_medicao_id', 'tipo_medicaos.id')
        .leftJoin('tipo_contratoes', 'contratoes.tipo_contracto_id', 'tipo_contratoes.id')


        .where("clientes.nome", "like", "%" + search + "%")
        .orWhere("clientes.telefone", "like", "%" + search + "%")
        .orWhere(Database.raw('DATE_FORMAT(pedidos.created_at, "%Y-%m-%d")'), "like", "%" + search + "%")
        .orderBy(orderBy == null ? "pedidos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }


  async pedidoByConta({ params, request }) {

    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select('pedidos.created_at as created_at',
        'pedidos.id as idPedido',
        'pedidos.conta_id as conta_id',
        'pedidos.local_instalacao_id as local_instalacao_id',
        'pedidos.observacao as observacao',
        'tarifarios.descricao as tarifario',
        'clientes.nome as clienteNome',
        'clientes.morada as clienteMorada',
        'clientes.telefone as clienteTelefone',
        'contadores.numero_serie as contadorNumeroSerie',
        'estado_pedidos.sigla as siglaEstadoPedido',
        'estado_pedidos.designacao as estadoDesignacao'
      )
        .table('pedidos')
        .innerJoin('contas', 'pedidos.conta_id', 'contas.id')
        .innerJoin('clientes', 'contas.cliente_id', 'clientes.id')
        .leftJoin('tarifarios', 'pedidos.tarifario_id', 'tarifarios.id')
        .leftJoin('estado_pedidos', 'estado_pedidos.id', 'pedidos.estado')
        .leftJoin('local_consumos', 'local_consumos.local_instalacao', 'pedidos.local_instalacao_id')
        .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
        .where("pedidos.conta_id", params.id)
        .orderBy(orderBy == null ? 'pedidos.created_at' : orderBy, 'DESC')
        .paginate(pagination.page, pagination.perPage);

    } else {

      res = await Database.select('pedidos.created_at as created_at',
        'pedidos.id as idPedido',
        'pedidos.conta_id as conta_id',
        'pedidos.local_instalacao_id as local_instalacao_id',
        'pedidos.observacao as observacao',
        'tarifarios.descricao as tarifario',
        'clientes.nome as clienteNome',
        'clientes.morada as clienteMorada',
        'clientes.telefone as clienteTelefone',
        'contadores.numero_serie as contadorNumeroSerie',
        'estado_pedidos.sigla as siglaEstadoPedido',
        'estado_pedidos.designacao as estadoDesignacao'
      )
        .table('pedidos')
        .innerJoin('contas', 'pedidos.conta_id', 'contas.id')
        .innerJoin('clientes', 'contas.cliente_id', 'clientes.id')
        .leftJoin('tarifarios', 'pedidos.tarifario_id', 'tarifarios.id')
        .leftJoin('estado_pedidos', 'estado_pedidos.id', 'pedidos.estado')
        .leftJoin('local_consumos', 'local_consumos.local_instalacao', 'pedidos.local_instalacao_id')
        .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
        .where("pedidos.conta_id", params.id)
        .where("clientes.telefone", "like", "%" + search + "%")
        .orWhere("pedidos.tipoPedido", "like", "%" + search + "%")
        .orWhere("estado_pedidos.designacao", "like", "%" + search + "%")
        .orWhere(Database.raw('DATE_FORMAT(pedidos.created_at, "%Y-%m-%d")'), "like", "%" + search + "%")
        .orderBy(orderBy == null ? "pedidos.created_at" : orderBy, "DESC")

    }

    //console.log(res)

    return DataResponse.response("success", 200, "", res);
  }


  async estadosPedidos() {
    const estados = await Database.select('id', 'designacao').table('estado_pedidos')
      .orderBy('designacao', 'ASC');
    return DataResponse.response("success", 200, "", estados);
  }

  async selectBox({ params }) {

    const pedidoEstado = await Database.select('pedidos.id',
      'pedidos.capacidade as capacidade',
      'pedidos.origem as origem',
      'pedidos.destino as destino',
      'tarifarios.tecnologia',
      'pedidos.tipoPedido as tipoPedido',
      'pedidos.estado',
      'pedidos.observacao as observacao',
      'tarifarios.descricao as tarifarioDescricao',
      'tarifarios.id as idTarifario',
      'pedidos.central as central',
      'pedidos.armario as armario',
      'pedidos.caixa as caixa',
      'pedidos.cabo as cabo_id',
      'pedidos.par_cabo as par_cabo',
      'pedidos.armario_primario as armario_primario',
      'pedidos.armario_secundario as armario_secundario',
      'pedidos.par_caixa as par_caixa',
      'pedidos.par_adsl as par_adsl'
    )
      .table('pedidos')
      .innerJoin('tarifarios', 'tarifarios.id', 'pedidos.tarifario_id')
      .where('pedidos.id', params.id).first()

    return DataResponse.response("success", 200, "", pedidoEstado);
  }

  async detalheLocalInstalacao({ params }) {


    const servico = await Database
      .select('local_instalacaos.moradia_numero as residenciaNumero',
        'local_instalacaos.predio_nome as residenciaNome',
        'ruas.nome as rua',
        'bairros.nome as bairro',
        'distritos.nome as distritoComuna',
        'municipios.nome as municipio'
      )
      .from('local_instalacaos')
      .leftJoin('ruas', 'local_instalacaos.rua_id', 'ruas.id')
      .leftJoin('bairros', 'ruas.bairro_id', 'bairros.id')
      .leftJoin('distritos', 'bairros.distrito_id', 'distritos.id')
      .leftJoin('municipios', 'bairros.municipio_id', 'municipios.id')
      .where('local_instalacaos.id', params.id).first()


    return DataResponse.response("success", 200, "", servico);
  }

  async totalDashBoard({ request }) {

    let totalFinalizado = null;
    let totalRejeitado = null;
    let totalOutro = null;

    totalFinalizado = await Database.select(
      Database.raw("count(pedidos.id) as totalFinalizado")
    )
      .from("pedidos")
      .innerJoin("estado_pedidos", "estado_pedidos.id", "pedidos.estado")
      .where("estado_pedidos.sigla", "FN")
      .first();


    totalRejeitado = await Database.select(
      Database.raw("count(pedidos.id) as totalRejeitado")
    )
      .from("pedidos")
      .innerJoin("estado_pedidos", "estado_pedidos.id", "pedidos.estado")
      .where("estado_pedidos.sigla", "RJ")
      .first();

    totalOutro = await Database.select(
      Database.raw("count(pedidos.id) as totalOutro")
    )
      .from("pedidos")
      .innerJoin("estado_pedidos", "estado_pedidos.id", "pedidos.estado")
      .whereNot("estado_pedidos.sigla", "FN")
      .whereNot("estado_pedidos.sigla", "RJ")
      .first();

    let data = {
      totalFinalizado: totalFinalizado,
      totalRejeitado: totalRejeitado,
      totalOutro: totalOutro
    };


    // console.log(data)


    return DataResponse.response("success", 200, "", data);
  }



  async updatEstado({ params, request, auth }) {
    const data = request.only(['estado', 'observacao']);
    const log = request.only(['estado', 'estado_actual', 'observacao_old']);

    const pedido = await Pedido.find(params.id);
    pedido.merge(data);
    await pedido.save();

    await LogEstadoPedido.create({ 'pedido_id': params.id, 'id_estado_anterior': log.estado_actual, 'user_id': auth.user.id, 'id_estado_novo': log.estado, 'observacao_old': log.observacao_old, 'observacao_new': data.observacao })

    return DataResponse.response("success", 200, "Estado actualizado com sucesso", pedido);

  }

  async anular({ request, auth }) {
    const { estado_ordem_servico_id, observacao, pedido_id } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD HH:MM:ss");

    const estado_ordem_servico = await Database.select("id")
      .table("estado_ordem_servicos")
      .where("slug", estado_ordem_servico_id).first();

    const getPedido = await Database.select("*")
      .table("pedidos")
      .where("id", pedido_id).first();

    await Database
      .table('pedidos')
      .where('id', pedido_id)
      .update({
        'estado': estado_ordem_servico.id,
        'observacao': observacao,
        'updated_at': dataActual

      })

    await LogEstadoPedido.create({ 'pedido_id': pedido_id, 'id_estado_anterior': getPedido.estado, 'id_estado_novo': estado_ordem_servico.id, 'user_id': auth.user.id, 'observacao_old': getPedido.observacao, 'observacao_new': observacao })


    return DataResponse.response("success", 200, "Pedido anulado com sucesso", null);


  }

  async estadosPedidosFNRJ() {
    const estados = await Database.select('id', 'designacao', 'sigla').table('estado_pedidos')
      .where('sigla', 'FN')
      .orWhere('sigla', 'RJ')
      .orderBy('designacao', 'ASC')

    return DataResponse.response("success", 200, "", estados);
  }


  async updatePedidoCliente({ request }) {
    const { pedidoId, tipoPedido, tarifario, telefone } = request.all()


    console.log(tarifario)

    await Database
      .table('pedidos')
      .where('id', pedidoId)
      .update({
        'tipoPedido': tipoPedido,
        'tarifario_id': tarifario,
        'telefone': telefone
      })

    return DataResponse.response("success", 200, "Pedido actualizado com sucesso", null);


  }
  /**
   * Delete a pedido with id.
   * DELETE pedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = PedidoController
