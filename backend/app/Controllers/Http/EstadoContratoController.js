'use strict'
const DataResponse = use("App/Models/DataResponse");
const EstadoContrato = use("App/Models/EstadoContrato");
const Database = use("Database");
var moment = require("moment");

class EstadoContratoController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "nivel",
        "nome",
        "slug"
      )
        .from("estado_contratoes")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "nivel",
        "nome",
        "slug"
      )
        .from("estado_contratoes")
        .orWhere("slug", "like", "%" + search + "%")
        .orWhere("nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { nome, nivel, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('estado_contratoes')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Estado de Contrato com este slug",
        Verify
      );

    } else {
      const estadocontrato = await Database.table('estado_contratoes').insert({
        slug: slug,
        nivel: nivel,
        nome: nome,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        estadocontrato
      );
    }
  }

  async update({ params, request, auth }) {
    const { slug, nome, nivel } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('estado_contratoes')
      .where("slug", slug)
      .whereNot({ id: params.id })
      .getCount(); 

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Estado de Contrato com este slug",
        Verify
      );

    } else {
      const estadocontrato = await Database.table('estado_contratoes')
      .where('id', params.id)
      .update({
        slug: slug,
        nome: nome,
        nivel: nivel,
        user_id: auth.user.id,
        'user_id': auth.user.id,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        estadocontrato
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'nivel', 'nome', 'slug')
      .from('estado_contratoes')
      .orderBy('nome', 'ASC');

    return res;
  }
}

module.exports = EstadoContratoController
