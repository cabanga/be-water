'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const Banco = use("App/Models/Banco");
const DataResponse = use("App/Models/DataResponse"); 
const Database = use("Database");

class BancoController {
  
  async index() {
    const bancos = await Database.select("*").from("bancos");
    return DataResponse.response("success",2000,null,bancos);
  }

  async store({ request, auth }) {
    const { nome, abreviatura, iban, numero_conta } = request.all();
    const banco = await Banco.query().where("nome", nome).where("iban", iban).where("numero_conta", iban).getCount();

    if (banco > 0) {
      return DataResponse.response("success",  500, "Já existe uma Banco com as mesmas configurações",null);
    } else {
      const banco = await Banco.create({
        nome: nome,
        abreviatura: abreviatura,
        iban: iban,
        numero_conta:numero_conta,
        user_id: auth.user.id
      });
      return DataResponse.response("success", 200, "Banco registado com sucesso", banco);
    }
  }

  async update({ params, request }) {
    const data = request.only(["nome", "abreviatura", "iban", "numero_conta"]);

/*     const Verify = await Banco.query()
      .where("nif", data.nif)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Conta Bancária com este NIF",
        Verify
      );
    } else  */
    {

      // update with new data entered
      const banco = await Banco.find(params.id);
      banco.merge(data);
      await banco.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        banco
      );
    }
  }
}

module.exports = BancoController
