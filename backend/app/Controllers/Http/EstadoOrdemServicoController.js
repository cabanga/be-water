'use strict'
const DataResponse = use("App/Models/DataResponse");
const EstadoOrdemServico = use("App/Models/EstadoOrdemServico");
const Database = use("Database");
var moment = require("moment");


class EstadoOrdemServicoController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "estado_ordem_servicos.id",
        "estado_ordem_servicos.nome",
        "estado_ordem_servicos.slug"
      )
        .from("estado_ordem_servicos")
        .orderBy(orderBy == null ? "estado_ordem_servicos.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "estado_ordem_servicos.id",
        "estado_ordem_servicos.nome",
        "estado_ordem_servicos.slug",
      )
        .from("estado_ordem_servicos")
        .orWhere("estado_ordem_servicos.nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "estado_ordem_servicos.nome" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { nome, slug, juro_mora, sujeito_corte, nivel_sensibilidade_id, caucao } = request.all();
    /* 
        console.log(request.all()); */

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    /*     const Verify = await Database.table('estado_ordem_servicos')
          .where("slug", slug)
          .getCount();
    
        if (Verify > 0) {
          return DataResponse.response(
            null,
            201,
            "Já existe Tipologia de Cliente com este slug",
            Verify
          );
    
        } else { */
    const estadoOrdemServico = await Database.table('estado_ordem_servicos').insert({
      slug: slug,
      nome: nome,
      user_id: auth.user.id,
      'created_at': dataActual,
      'updated_at': dataActual
    });
    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      estadoOrdemServico
    );
    //}
  }

  async update({ params, request }) {
    const data = request.only(["nome", "slug", "juro_mora", "sujeito_corte", "nivel_sensibilidade_id", "caucao"]);


    // update with new data entered
    const estado_ordem_servicos = await EstadoOrdemServico.find(params.id);
    estado_ordem_servicos.merge(data);
    await estado_ordem_servicos.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      estado_ordem_servicos
    );
    //}
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'nome', 'slug')
      .from('estado_ordem_servicos')
      .orderBy('nome', 'ASC');

    return res;
  }

  async selectGroupBox({ request }) {

    var res = await Database.select(
      'estado_ordem_servicos.id', 
      'estado_ordem_servicos.nome', 
      'estado_ordem_servicos.slug')
      .from('estado_ordem_servicos')
      .where('parent_id', null)
      .orderBy('nome', 'ASC');

    for (let index = 0; index < res.length; index++) {

      var child = await Database.select(
        'estado_ordem_servicos.id', 
        'estado_ordem_servicos.nome', 
        'estado_ordem_servicos.slug')
        .from('estado_ordem_servicos')
        .where('parent_id', res[index].id)
        .orderBy('nome', 'ASC');

        res[index].childs = child;

    }

    return res;
  }

  async getEstadoOrdemServicoById({ params }) {

    var res = await Database.select(
      'estado_ordem_servicos.id', 
      'estado_ordem_servicos.nome', 
      'estado_ordem_servicos.slug')
      .from('estado_ordem_servicos')
      .where('id', params.id)
      .first();

    return res;
  }

  

}

module.exports = EstadoOrdemServicoController
