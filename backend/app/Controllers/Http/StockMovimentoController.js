'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
 
const StokeMovimento = use('App/Models/StokeMovimento');
const User = use('App/Models/User');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');
/**
 * Resourceful controller for interacting with stockmovimentos
 */
class StockMovimentoController {
   /**
   * Show a list of all facturas.
   * GET facturas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({
    request,
    response,
    view
  }) {
    const {start, end, search, order, searchData} = request.all();
    let res = null;
	 

    if (search == null) {
      res = await Database.select('stoke_movimentos.tipo_doc', 'stoke_movimentos.quantidade','stoke_movimentos.movimento','armazens.nome as armazem'
	  ,'stoke_movimentos.created_at','documentos.nome as documento','produtos.nome as produto').from('stoke_movimentos')
	  .innerJoin('produtos', 'produtos.id', 'stoke_movimentos.artigo') 
    .innerJoin('documentos', 'documentos.id', 'stoke_movimentos.documento_id') 
    .innerJoin('armazens', 'armazens.id', 'stoke_movimentos.armazem').paginate(start, end) 
    } else {
      res = await Database.select('stoke_movimentos.tipo_doc', 'stoke_movimentos.quantidade','stoke_movimentos.movimento','armazens.nome as armazem'
	  ,'stoke_movimentos.created_at','documentos.nome as documento','produtos.nome as produto').from('stoke_movimentos')
	  .innerJoin('produtos', 'produtos.id', 'stoke_movimentos.artigo') 
    .innerJoin('documentos', 'documentos.id', 'stoke_movimentos.documento_id') 
    .innerJoin('armazens', 'armazens.id', 'stoke_movimentos.armazem') 
		.where('produtos.nome', 'like', '%' + search + "%")
		.orWhere('stoke_movimentos.movimento', 'like', '%' + search + "%") 
		.orWhere(Database.raw('DATE_FORMAT(stoke_movimentos.created_at, "%Y-%m-%d")'), 'like', '%' + search + "%") 
		.orderBy(order, 'asc').paginate(start, end)
    }

     return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new stockmovimento.
   * GET stockmovimentos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new stockmovimento.
   * POST stockmovimentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single stockmovimento.
   * GET stockmovimentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing stockmovimento.
   * GET stockmovimentos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update stockmovimento details.
   * PUT or PATCH stockmovimentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a stockmovimento with id.
   * DELETE stockmovimentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
  
  async stock(){ 
	 /*await Database.select('sum(stoke_movimentos.quantidade)').from('stoke_movimentos')
	  .innerJoin('produtos', 'produtos.id', 'stoke_movimentos.artigo')  
	  .groupBy('stoke_movimentos.artigo') */
	  
	let res = await Database.raw("SELECT movimento_entrada.id,movimento_entrada.nome,(movimento_entrada.somaE - movimento_saida.somaS) stock, movimento_entrada.movimento,movimento_saida.movimento,movimento_entrada.somaE,movimento_saida.somaS FROM  (SELECT p.nome,p.id, COUNT(s.artigo), SUM(s.quantidade) somaE, s.movimento FROM produtos p, stoke_movimentos s WHERE p.id = s.artigo AND s.movimento='E' GROUP BY s.artigo) movimento_entrada, (SELECT p.nome, p.id, COUNT(s.artigo), SUM(s.quantidade) somaS, s.movimento FROM produtos p, stoke_movimentos s WHERE p.id = s.artigo AND s.movimento='S'GROUP BY s.artigo) movimento_saida WHERE movimento_saida.id =  movimento_entrada.id")
	 
	
	return DataResponse.response("success", 200, "", res);
  }
}

module.exports = StockMovimentoController
