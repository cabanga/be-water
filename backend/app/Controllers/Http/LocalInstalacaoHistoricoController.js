'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const LocalInstalacaoHistorico = use("App/Models/LocalInstalacaoHistorico");
const Database = use("Database");
var moment = require("moment");
/**
 * Resourceful controller for interacting with localinstalacaohistoricos
 */
class LocalInstalacaoHistoricoController {
  /**
   * Show a list of all localinstalacaohistoricos.
   * GET localinstalacaohistoricos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  async getHistoricoById({ params }) {

    let res = await Database.select(
      "local_instalacao_historicos.id",
      "local_instalacao_historicos.local_instalacao_id",
      "local_instalacao_historicos.operacao",
      "local_instalacao_historicos.historico",
      "local_instalacao_historicos.actualizacao",
      "local_instalacao_historicos.user_id",
      "users.nome as user",
      "local_instalacao_historicos.created_at"
    )
      .from("local_instalacao_historicos")
      .leftJoin("users", "users.id", "local_instalacao_historicos.user_id")

      .where("local_instalacao_id", params.id)

      .orderBy("local_instalacao_historicos.created_at", "DESC");


    return res;
  }

  /**
   * Render a form to be used for creating a new localinstalacaohistorico.
   * GET localinstalacaohistoricos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new localinstalacaohistorico.
   * POST localinstalacaohistoricos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single localinstalacaohistorico.
   * GET localinstalacaohistoricos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing localinstalacaohistorico.
   * GET localinstalacaohistoricos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update localinstalacaohistorico details.
   * PUT or PATCH localinstalacaohistoricos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a localinstalacaohistorico with id.
   * DELETE localinstalacaohistoricos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = LocalInstalacaoHistoricoController
