'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const TipologiaServico = use("App/Models/TipologiaServico");
const Database = use("Database");
var moment = require("moment");

/**
 * Resourceful controller for interacting with tipologiaservicos
 */
class TipologiaServicoController {
  /**
   * Show a list of all tipologiaservicos.
   * GET tipologiaservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new tipologiaservico.
   * GET tipologiaservicos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new tipologiaservico.
   * POST tipologiaservicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single tipologiaservico.
   * GET tipologiaservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing tipologiaservico.
   * GET tipologiaservicos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update tipologiaservico details.
   * PUT or PATCH tipologiaservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a tipologiaservico with id.
   * DELETE tipologiaservicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  
  async selectBox({ request }) {

    var res = await Database.select('id', 'nome', 'slug', 'nova_ligacao_flag', 'novo_contrato_flag', 'rescisao_contrato_flag')
      .from('tipologia_servicos')
      .orderBy('nome', 'ASC');

    return res;
  }

}

module.exports = TipologiaServicoController
