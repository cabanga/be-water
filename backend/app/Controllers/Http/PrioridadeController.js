'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const Prioridade = use("App/Models/Prioridade");
const Database = use("Database");
/**
 * Resourceful controller for interacting with prioridades
 */
class PrioridadeController {
  /**
   * Show a list of all prioridades.
   * GET prioridades
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async selectBox() {
    /*
    const prioridades = await Prioridade.all();
    return DataResponse.response("success", 200, "", prioridades);
    */
   const prioridades = await Database.select('*')
   .table('prioridades')
   .orderBy('designacao', 'ASC');
   return DataResponse.response("success", 200, "", prioridades);

  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "prioridades.id",
        "prioridades.designacao",
        "prioridades.created_at"
      )
        .from("prioridades")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "prioridades.id",
        "prioridades.designacao",
        "prioridades.created_at"
      )
        .from("prioridades")
        .where("prioridades.designacao", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(prioridades.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new prioridade.
   * GET prioridades/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new prioridade.
   * POST prioridades
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const {
      designacao
    } = request.all();
    const prioridade = await Prioridade.query()
      .where("designacao", designacao)
      .getCount();
    if (prioridade > 0) {
      return DataResponse.response(
        "success",
        500,
        "Já existe uma prioridade com as mesma designação",
        serie
      );
    } else {
      const prioridade = await Prioridade.create({
        designacao: designacao,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Prioridade registado com sucesso",
        prioridade
      );
    }
  }

  /**
   * Display a single prioridade.
   * GET prioridades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing prioridade.
   * GET prioridades/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update prioridade details.
   * PUT or PATCH prioridades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only([
      "designacao"
    ]);

    //console.log(data)

    const prioridade = await Prioridade.query()
    .where("designacao", data.designacao)
    .whereNot({ id: params.id })
    .getCount();
  if (prioridade > 0) {
    return DataResponse.response(
      "success",
      500,
      "Já existe uma prioridade com as mesma designação",
      prioridade
    );
  }else{

    // update with new data entered
    const prioridade = await Prioridade.find(params.id);
    prioridade.merge(data);
    await prioridade.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      prioridade
    );

  }
  }
  /**
   * Delete a prioridade with id.
   * DELETE prioridades/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = PrioridadeController
