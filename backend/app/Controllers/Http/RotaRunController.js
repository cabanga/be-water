'use strict'

const RotaRun = use("App/Models/RotaRun");
const RotaHeader = use("App/Models/RotaHeader");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const AgendamentoRoteiro = use('App/Models/AgendamentoRoteiro')
const PeriodicidadeRoteiro = use('App/Models/PeriodicidadeRoteiro')


class RotaRunController {

  async index({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        'rota_runs.id',
        "rota_runs.rota_header_id",
        "rota_headers.descricao as rota_header",
        'rota_runs.local_consumo_id',
        'local_consumos.local_instalacao as local_instalacao_id',
        'local_consumos.conta_id',
        'local_instalacaos.moradia_numero',
        'local_instalacaos.is_predio',
        'local_instalacaos.predio_id',
        'local_instalacaos.predio_nome',
        'local_consumos.contador_id',
        'contadores.numero_serie',
        'rota_runs.estado_rota_id',
        'estado_rotas.designacao as estado_rota'
      )
      .table('rota_runs')
      .innerJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
      .leftJoin('local_consumos', 'rota_runs.local_consumo_id', 'local_consumos.id')
      .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
      .leftJoin('estado_rotas', 'rota_runs.estado_rota_id', 'estado_rotas.id')
      .leftJoin('local_instalacaos', 'local_consumos.local_instalacao', 'local_instalacaos.id')
      .orderBy(orderBy == null ? "rota_runs.created_at" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        'rota_runs.id',
        "rota_runs.rota_header_id",
        "rota_headers.descricao as rota_header",
        'rota_runs.local_consumo_id',
        'local_consumos.local_instalacao as local_instalacao_id',
        'local_consumos.conta_id',
        'local_instalacaos.moradia_numero',
        'local_instalacaos.is_predio',
        'local_instalacaos.predio_id',
        'local_instalacaos.predio_nome',
        'local_consumos.contador_id',
        'contadores.numero_serie',
        'rota_runs.estado_rota_id',
        'estado_rotas.designacao as estado_rota'
      )
      .table('rota_runs')
      .innerJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
      .leftJoin('local_consumos', 'rota_runs.local_consumo_id', 'local_consumos.id')
      .innerJoin('local_instalacaos', 'local_consumos.local_instalacao', 'local_instalacaos.id')
      .innerJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
      .leftJoin('estado_rotas', 'rota_runs.estado_rota_id', 'estado_rotas.id')
      .where("rota_headers.descricao", "like", "%" + search + "%")
      .orWhere("local_instalacaos.moradia_numero", "like", "%" + search + "%")
      .orWhere("local_instalacaos.predio_nome", "like", "%" + search + "%")
      .orWhere("contadores.numero_serie", "like", "%" + search + "%")
      .orWhere("local_consumos.descricao", "like", "%" + search + "%")
      .orWhere("estado_rotas.designacao", "like", "%" + search + "%")

      .orderBy(orderBy == null ? "rota_runs.created_at" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async getRotas({ request }) {
    let res = null;

    res = await Database.select(
      'rota_runs.id',
      "rota_runs.rota_header_id",
      "rota_headers.descricao as rota_header",
      'rota_runs.local_consumo_id',
      'local_consumos.local_instalacao as local_instalacao_id',
      'local_consumos.conta_id',
      'local_instalacaos.moradia_numero',
      'local_instalacaos.is_predio',
      'local_instalacaos.predio_id',
      'local_instalacaos.predio_nome',
      'local_consumos.contador_id',
      'contadores.numero_serie',
      'rota_runs.estado_rota_id',
      'estado_rotas.designacao as estado_rota'
    )
    .table('rota_runs')
    .innerJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
    .leftJoin('local_consumos', 'rota_runs.local_consumo_id', 'local_consumos.id')
    .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
    .leftJoin('estado_rotas', 'rota_runs.estado_rota_id', 'estado_rotas.id')
    .leftJoin('local_instalacaos', 'local_consumos.local_instalacao', 'local_instalacaos.id')
    .orderBy("rota_runs.created_at", "DESC");

    return res;
  }

  async getRotasRunPendentesByRotaHeader({ params }) {

    const estado = await Database.select('id')
      .table('estado_rotas')
      .where('slug', 'PENDENTE')
      .first();

    const res = await Database.select(
      'rota_runs.id',
      "rota_runs.rota_header_id",
      "rota_headers.descricao as rota_header",
      "rota_headers.leitor_id as leitor_id",
      'rota_runs.local_consumo_id',
      'local_consumos.contador_id',
      'contadores.numero_serie',
      'local_consumos.local_instalacao as local_instalacao_id',
      'local_consumos.conta_id',
      'local_instalacaos.moradia_numero',
      'local_instalacaos.is_predio',
      'local_instalacaos.predio_id',
      'local_instalacaos.predio_nome',
      'contas.cliente_id',
      'clientes.nome as cliente',
      'clientes.morada as cliente_morada',
      'clientes.telefone as cliente_telefone',
      'leituras.leitura',
      'contadores.ultima_leitura',
      //'leituras.ultima_leitura',
      'leituras.periodo',
      'leituras.data_leitura',
      'rota_runs.estado_rota_id',
      'estado_rotas.designacao as estado_rota',
      'estado_rotas.slug as estado_rota_slug'
    )
      .table('rota_runs')
      .innerJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
      .leftJoin('local_consumos', 'rota_runs.local_consumo_id', 'local_consumos.id')
      .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
      .leftJoin('estado_rotas', 'rota_runs.estado_rota_id', 'estado_rotas.id')
      .leftJoin('local_instalacaos', 'local_consumos.local_instalacao', 'local_instalacaos.id')
      .leftJoin('contas', 'local_consumos.conta_id', 'contas.id')
      .leftJoin('clientes', 'contas.cliente_id', 'clientes.id')
      .leftJoin('leituras', 'leituras.rota_run_id', 'rota_runs.id')

      .orderBy("rota_runs.created_at", "DESC")
      .where("rota_runs.rota_header_id", params.id)
      .where("estado_rotas.id", estado.id)


    return DataResponse.response("success", 200, "", res);
  }

  async selectBoxRotasRunPendentesByRotaReader({ params }) {
    const estado = await Database.select('id')
    .table('estado_rotas')
    .where('slug', 'PENDENTE')
    .first();

    const res = await Database.select(
      'rota_runs.id',
      "rota_runs.rota_header_id",
      'rota_runs.local_consumo_id',
      'local_consumos.contador_id',
      'contadores.numero_serie',
      'local_consumos.local_instalacao as local_instalacao_id',
      'local_consumos.conta_id',
      'contas.cliente_id',
      'clientes.nome as cliente',
      'rota_runs.estado_rota_id',
    )
    .table('rota_runs')
    .innerJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
    .leftJoin('local_consumos', 'rota_runs.local_consumo_id', 'local_consumos.id')
    .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
    .leftJoin('estado_rotas', 'rota_runs.estado_rota_id', 'estado_rotas.id')
    .leftJoin('local_instalacaos', 'local_consumos.local_instalacao', 'local_instalacaos.id')
    .leftJoin('contas', 'local_consumos.conta_id', 'contas.id')
    .leftJoin('clientes', 'contas.cliente_id', 'clientes.id')

    .orderBy("rota_runs.created_at", "DESC")
    .where("rota_runs.rota_header_id", params.id)
    .where("estado_rotas.id", estado.id)

    return res;
  }

  async create({ request }) {

  }

  async store({ request }) {

    const {
      rota_header_id,
      local_consumo_id,
      dia_mes,
      ciclo,
      dia_semana,
      periodicidade_roteiro_id,
      user_id
    } = request.all()

    let data1 = await this.generate_data_realizar(dia_mes, ciclo)
    let data2 = await this.generate_data_realizar(dia_mes, Number(ciclo) * 2)


    const estado_rota = await Database.select("*")
      .table("estado_rotas")
      .where("slug", "PENDENTE")
      .first()

    await RotaRun.create({
      data_realizar: data1,
      proxima_data_realizar: data2,
      rota_header_id: rota_header_id,
      dia_mes: dia_mes,
      dia_semana: dia_semana,
      estado_rota_id: estado_rota.id,
      local_consumo_id: local_consumo_id,
      periodicidade_roteiro_id: periodicidade_roteiro_id,
    })

    return DataResponse.response( "success", 200, "Registado com sucesso", null )

  }

  async generate_data_realizar(dia, ciclo){
    var today = new Date()
    var set_day = new Date(today.setDate(dia))
    var set_month = new Date( set_day.setMonth(today.getMonth() + ciclo) )

    return set_month
  }

  /*
  async create_agendamento(roteiro){
    var periodicidade = await PeriodicidadeRoteiro.find(roteiro.periodicidade_roteiro_id)

    if( periodicidade.slug === 'mensais'){
      await this.agendamento_mensal(roteiro, 1)
    }

    if( periodicidade.slug === 'bimestrais'){
      await this.agendamento_mensal(roteiro, 2)
    }

    if( periodicidade.slug === 'trimestrais'){
      await this.agendamento_mensal(roteiro, 3)
    }

    if( periodicidade.slug === 'semestrais'){
      await this.agendamento_mensal(roteiro, 6)
    }

  }

  async agendamento_mensal(roteiro, cicle){
    var current_date = new Date()
    var last_date = null

    do {
      var next_month = current_date.setMonth( current_date.getMonth() + 1 )
      var new_date = new Date(next_month)
      var set_day = new Date( new_date.setDate(roteiro.dia_mes) )

      let data = {
        data_realizar: set_day,
        rota_run_id: roteiro.id
      }

      await AgendamentoRoteiro.create(data)

      cicle = cicle - 1
      last_date = set_day
    } while (cicle > 0)
  }

  */


  /**
   * Display a single rotarun.
   * GET rotaruns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ response }) {
    const res = await Database.select('rr.id', 'rh.nome').from('rota_runs as rr')
      .innerJoin("rota_headers as rh", "rh.id", "rr.id_rota_header")
      .orderBy("rh.nome", "ASC");

    return res;
  }

  /**
   * Render a form to update an existing rotarun.
   * GET rotaruns/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  async getRotasRunByRotaHeader({ params }) {

    const res = await Database.select(
      'rota_runs.id',
      "rota_runs.rota_header_id",
      "rota_runs.nao_leitura",
      "rota_runs.motivo",
      "rota_headers.descricao as rota_header",
      "rota_headers.leitor_id as leitor_id",
      'rota_runs.local_consumo_id',
      'local_consumos.contador_id',
      'contadores.numero_serie',
      'contadores.ultima_leitura',
      'local_consumos.local_instalacao as local_instalacao_id',
      'local_consumos.conta_id',
      'local_instalacaos.moradia_numero',
      'local_instalacaos.is_predio',
      'local_instalacaos.predio_id',
      'local_instalacaos.predio_nome',
      'local_instalacaos.latitude',
      'local_instalacaos.longitude',
      'local_instalacaos.cil',
      'contas.cliente_id',
      'clientes.nome as cliente',
      'clientes.morada as cliente_morada',
      'clientes.telefone as cliente_telefone',
      'leituras.leitura',
      //'leituras.ultima_leitura',
      'leituras.periodo',
      'leituras.data_leitura',
      'rota_runs.estado_rota_id',
      'estado_rotas.designacao as estado_rota',
      'estado_rotas.slug as estado_rota_slug'
    )
      .table('rota_runs')
      .innerJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
      .leftJoin('local_consumos', 'rota_runs.local_consumo_id', 'local_consumos.id')
      .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
      .leftJoin('estado_rotas', 'rota_runs.estado_rota_id', 'estado_rotas.id')
      .leftJoin('local_instalacaos', 'local_consumos.local_instalacao_id', 'local_instalacaos.id')
      .leftJoin('contas', 'local_consumos.conta_id', 'contas.id')
      .leftJoin('clientes', 'contas.cliente_id', 'clientes.id')
      .leftJoin('leituras', 'leituras.rota_run_id', 'rota_runs.id')

      .orderBy("rota_runs.created_at", "DESC")
      .where("rota_runs.rota_header_id", params.id)


    //const rota = await RotaRun.find( params.id )
    //await rota.load('local_consumo')

    return DataResponse.response("success", 200, "", res)
  }

  /**
   * Update rotarun details.
   * PUT or PATCH rotaruns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {

    const {
      rota_header_id,
      local_consumo_id,
      estado_rota_id,
      user_id
    } = request.all();

    const rota_run = await RotaRun.find(params.id);

    local_instalacao.merge({
      rota_header_id: rota_header_id,
      local_consumo_id: local_consumo_id,
      estado_rota_id: estado_rota_id
    });

    await RotaRun.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );

  }

  /**
   * Delete a rotarun with id.
   * DELETE rotaruns/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  async selectBoxContagemServicos() {
    const contagens = await Database.select("ct.id", "ct.leitura", "sv.descricao_operacao as servico")
      .table("contagens as ct")
      .innerJoin("servicos as sv", "sv.id", "ct.rota_run_id")
      .where("r.nome", 'INTERCONEXAO');
    return DataResponse.response("success", 200, "", contagens);

  }

  async getRoteiros({ request }){
    const rotas = await RotaHeader.query().select("*").fetch();
    return DataResponse.response("success", 200, "", rotas);
  }
}

module.exports = RotaRunController
