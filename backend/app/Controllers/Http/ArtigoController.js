"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const { validate } = use("Validator");
const Produto = use("App/Models/Produto");
const DataResponse = use("App/Models/DataResponse");
const ProdutoImposto = use("App/Models/Filial");

const ClasseTarifarioRepository = use('App/Repositories/ClasseTarifarioRepository')

const Filial = use("App/Models/ProdutoImposto");

const Database = use("Database");
/**
 * Resourceful controller for interacting with artigos
 */
class ArtigoController {


  #_classeTarifarioRepository

  constructor() {
    this.#_classeTarifarioRepository = new ClasseTarifarioRepository();
  }

  async get_all({ request }) {
    let res = await Database.select(
      "produtos.id",
      "produtos.nome",
      "produtos.valor",
      "produtos.tipo_produto_id",
      "tipo_produtos.descricao as tipo",
      "produtos.imposto_id",
      "produtos.moeda_id",
      "produtos.created_at",
      "moedas.codigo_iso",
      "produtos.is_trigger",
      "produtos.is_active",
      "moedas.nome as moeda",
      "produtos.observacao",
      "impostos.valor as imposto_valor"
    )
    .from("produtos")
    .leftJoin("moedas", "moedas.id", "produtos.moeda_id")
    .leftJoin("tipo_produtos", "tipo_produtos.id", "produtos.tipo_produto_id")
    .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
    .orderBy("produtos.created_at", "DESC")


    return DataResponse.response("success", 200, "", res);
  }



  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "produtos.id",
        "produtos.nome",
        "produtos.valor",
/*         "produtos.tipo", */
        "produtos.tipo_produto_id",
        "tipo_produtos.descricao as tipo",
        "produtos.imposto_id",
        "produtos.moeda_id",
        "produtos.is_trigger",
        "produtos.is_active",
        "produtos.created_at",
        "produtos.observacao",
        "moedas.codigo_iso",
        "moedas.nome as moeda",
        "impostos.valor as imposto_valor"
      )
      .from("produtos")
      .leftJoin("moedas", "moedas.id", "produtos.moeda_id")
      .leftJoin("tipo_produtos", "tipo_produtos.id", "produtos.tipo_produto_id")
      .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
      .orderBy("produtos.created_at", "DESC")
      .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "produtos.id",
        "produtos.nome",
        "produtos.valor",
/*         "produtos.tipo", */
        "produtos.tipo_produto_id",
        "tipo_produtos.descricao as tipo",
        "produtos.imposto_id",
        "produtos.moeda_id",
        "produtos.created_at",
        "moedas.codigo_iso",
        "produtos.is_trigger",
        "produtos.is_active",
        "moedas.nome as moeda",
        "produtos.observacao",
        "impostos.valor as imposto_valor"
      )
      .from("produtos")
      .leftJoin("moedas", "moedas.id", "produtos.moeda_id")
      .leftJoin("tipo_produtos", "tipo_produtos.id", "produtos.tipo_produto_id")
      .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
      .where("produtos.nome", "like", "%" + search + "%")
      .orWhere(
        Database.raw('DATE_FORMAT(produtos.created_at, "%Y-%m-%d")'),
        "like",
        "%" + search + "%"
      )
      .orderBy("produtos.created_at", "DESC")
      .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Show a list of all artigos.
   * GET artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async searchArtigo({ request }) {

    const { pagination, search } = request.all();

    return await this.#_classeTarifarioRepository.getServicosFacturacao(pagination, search);
  }

  /**
   * Show a list of all artigos.
   * GET artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async selectProdutos() {
    let res = await Produto.all();
    return DataResponse.response("success", 200, "", res);
  }

  async ValorByProduto({ params }) {

    const res = await Database.select('valor')
      .from('produtos')
      .where('id', params.id).first()

    return DataResponse.response(
      "success",
      200,
      null,
      res
    );
  }

  /**
   * Create/save a new artigo.
   * POST artigos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const data = request.all();
    const {
      nome,
      valor,
      tipo_produto_id,
      classificacao_produto_id,
      imposto_id,
      moeda_id,
      observacao
    } = request.all();

    const tipo = 'A';

    const produto = await Produto.create({
      nome: nome,
      valor: valor,
      tipo_produto_id: tipo_produto_id,
      classificacao_produto_id: classificacao_produto_id,
      tipo: tipo,
      user_id: auth.user.id,
      imposto_id: imposto_id,
      moeda_id: moeda_id,
      observacao: observacao
    });


    return DataResponse.response(
      "success",
      200,
      "Produto criado com sucesso",
      produto
    );
  }

  /**
   * Display a single artigo.
   * GET artigos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ request, auth }) {
    const {
      produto_id,
      quantidade,
      desconto,
      cliente_id,
      observacao,
      preco
    } = request.all();



    let retorno = {
      produto_id: 0,
      nome: null,
      valor: 0.0,
      quantidade: quantidade,
      desconto: 0.0,
      total: 0.0,
      valorImposto: 0.0,
      imposto_id: null,
      linhaTotalSemImposto: 0.0,
      cliente_id: cliente_id,
      observacao: observacao,
      is_trigger: 0,
      valor_original: null
    };

    const produto = await Database.select(
      "produtos.id",
      "produtos.tipo",
      "produtos.nome",
      "produtos.valor",
      "produtos.imposto_id",
      "produtos.moeda_id",
      "produtos.is_trigger",
      "impostos.valor as valor_imposto"
    ).from("produtos")
      .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
      .where("produtos.id", produto_id);

    retorno.valor_original = produto[0].valor;


    if (auth.user.loja_id != null) {

      const filial = await Database.select("impostos.valor as valor_imposto", "filials.imposto_id")
        .from("lojas")
        .innerJoin("filials", "filials.id", "lojas.filial_id")
        .innerJoin("impostos", "impostos.id", "filials.imposto_id")
        .whereNotNull('filials.imposto_id')
        .where("lojas.id", auth.user.loja_id);

      if (filial.length != 0 && produto[0].tipo == "A") {
        produto[0].valor_imposto = filial[0].valor_imposto;
        produto[0].imposto_id = filial[0].imposto_id;
      }


    }

    if (produto[0].valor == null) {
      produto[0].valor = preco
    }

    var cambio = null;
    cambio = await Database
      .select('moedas.codigo_iso', 'cambios.valor_cambio', 'cambios.id')
      .from("cambios")
      .innerJoin("moedas", "moedas.id", "cambios.moeda_id")
      .where("moedas.id", produto[0].moeda_id)
      .where("cambios.is_active", true)
      .first()

    if (cambio != null) {
      if (cambio.codigo_iso != "AOA") {
        produto[0].valor = produto[0].valor * cambio.valor_cambio;
      }
    }


    retorno.desconto = parseFloat(parseFloat(produto[0].valor * (desconto / 100)).toFixed(2));

    var desc = produto[0].valor - retorno.desconto;

    retorno.valorImposto = parseFloat(parseFloat(quantidade * (desc * (produto[0].valor_imposto / 100))).toFixed(2));


    retorno.linhaTotalSemImposto = parseFloat(parseFloat(desc * quantidade).toFixed(2));

    retorno.total = retorno.linhaTotalSemImposto + retorno.valorImposto;
    retorno.is_trigger = produto[0].is_trigger;

    //parseFloat(parseFloat(quantidade * (desc + retorno.valorImposto)).toFixed(2));



    retorno.produto_id = produto[0].id;
    retorno.nome = produto[0].nome;
    retorno.valor = produto[0].valor;
    retorno.imposto_id = produto[0].imposto_id;
    return DataResponse.response("success", 200, "", retorno);
  }

  /**
   * Update artigo details.
   * PUT or PATCH artigos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const data = request.only([
      "nome",
      "valor",
      "tipo",
      "tipo_produto_id",
      "classificacao_produto_id",
      "is_trigger",
      "imposto_id",
      "moeda_id",
      "is_active",
      "observacao"
    ]);

    const count = await Database.from("linha_facturas").where("artigo_id", params.id).getCount() // returns array
    //const total = count[0]['count("cliente_id")'];
    if (count != 0 || count == undefined) {
      //return DataResponse.response("error", 500,"Os dados não podem ser editados porque o produto já possui facturas associada", data);
    }

    const produto = await Produto.find(params.id);
    produto.merge(data);
    await produto.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      produto
    );
  }

  /**
   * Delete a artigo with id.
   * DELETE artigos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) { }
}

module.exports = ArtigoController;
