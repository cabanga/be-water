'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Cliente = use("App/Models/Cliente");
const Factura = use("App/Models/Factura");
const BillRunHeader = use("App/Models/BillRunHeader");
const BillRunEmail = use("App/Models/BillRunEmail");
const BillRunEmailHeader = use("App/Models/BillRunEmailHeader");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");
const Env = use('Env')
const nodemailer = require("nodemailer");
const fs = require("fs");

const UNIG_PDF = use("App/Models/UnigJsPdf");
const fileStreamBase64 = use("App/Helpers/fileStreamBase64");

/**
 * Resourceful controller for interacting with billrunheaders
 */
class BillRunHeaderController {

  async stream({ request, view, response, auth }){
    var path ="uploads/facturas/"
    var fileName ="factura"
    var format ="pdf"
      var file =  await fileStreamBase64(path, fileName, format, response);
    return DataResponse.response("success", 200, "", file);
  }
  /**
   * Show a list of all billrunheaders.
   * GET billrunheaders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {

    const { search, pagination } = request.all();
    let res = null;
    if (search == null) {
      res = await Database.select(
        "bill_run_headers.id",
        "bill_run_headers.mes",
        "bill_run_headers.ano",
        "bill_run_headers.estado",
        "bill_run_headers.created_at",
        "bill_run_headers.updated_at",
        "series.nome as serie",
        "tecnologias.nome as tecnologia"
      )
        .from("bill_run_headers")
        .innerJoin("series", "series.id", "bill_run_headers.serie_id")
        .innerJoin("tecnologias", "tecnologias.id", "bill_run_headers.tecnologia_id")
        .orderBy("bill_run_headers.created_at", "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "users.nome as user_chefe"
      )
        .from("lojas")
        .leftJoin("municipios", "municipios.id", "lojas.municipio_id")
        .leftJoin("provincias", "provincias.id", "lojas.provincia_id")
        .leftJoin("filials", "filials.id", "lojas.filial_id")
        .leftJoin("series", "series.id", "lojas.serie_id")
        .leftJoin("documentos", "documentos.id", "series.documento_id")
        .leftJoin("users", "users.id", "lojas.user_chefe_id")
        .where("lojas.nome", "like", "%" + search + "%")
        .orWhere("lojas.numero", "like", "%" + search + "%")
        .orWhere("lojas.endereco", "like", "%" + search + "%")
        .orWhere("provincias.nome", "like", "%" + search + "%")
        .orWhere("municipios.nome", "like", "%" + search + "%")

        .orWhere(
          Database.raw('DATE_FORMAT(lojas.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy("lojas.created_at", "DESC")
        .paginate(
          pagination.page == null ? 1 : pagination.page,
          pagination.perPage
        );
    }

    return DataResponse.response("success", 200, "", res);
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "bill_run_headers.id",
        "bill_run_headers.mes",
        "bill_run_headers.ano",
        "bill_run_headers.estado",
        "bill_run_headers.created_at",
        "bill_run_headers.updated_at",
        "series.nome as serie",
        "tecnologias.nome as tecnologia"
      )
        .from("bill_run_headers")
        .innerJoin("series", "series.id", "bill_run_headers.serie_id")
        .innerJoin("tecnologias", "tecnologias.id", "bill_run_headers.tecnologia_id")
        .where("bill_run_headers.estado", 1)
        .where("tecnologias.nome", "VOZ")
        .orderBy(orderBy == null ? 'bill_run_headers.created_at' : orderBy, 'DESC')
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "bill_run_headers.id",
        "bill_run_headers.mes",
        "bill_run_headers.ano",
        "bill_run_headers.estado",
        "bill_run_headers.created_at",
        "bill_run_headers.updated_at",
        "series.nome as serie",
        "tecnologias.nome as tecnologia"
      )
        .from("bill_run_headers")
        .innerJoin("series", "series.id", "bill_run_headers.serie_id")
        .innerJoin("tecnologias", "tecnologias.id", "bill_run_headers.tecnologia_id")
        .where("bill_run_headers.estado", 1)
        .where("tecnologias.nome", "VOZ")
        .where("bill_run_headers.ano", "like", "%" + search + "%")
        /*
                .orWhere("bill_run_headers.mes", "like", "%" + search + "%")
                .orWhere("series.nome", "like", "%" + search + "%")
                .orWhere("tecnologias.nome", "like", "%" + search + "%")
                .orWhere(
                  Database.raw('DATE_FORMAT(bill_run_headers.created_at, "%Y-%m-%d")'),
                  "like",
                  "%" + search + "%"
                )
                */
        .orderBy(orderBy == null ? "bill_run_headers.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new billrunheader.
   * GET billrunheaders/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  async consultarFacturasPospagoVoz({ params }) {
    let facturas = null;

    facturas = await Database.select(
      "facturas.id as factura_id",
      "facturas.factura_sigla",
      "facturas.observacao",
      "facturas.hash",
      "facturas.hash_control",
      "facturas.status",
      "facturas.status_date",
      "facturas.status_reason",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.user_id",
      "facturas.id",
      "facturas.numero",
      "facturas.cliente_id",
      "facturas.created_at",
      "facturas.pago",
      "facturas.serie_id",
      "facturas.user_id",
      "facturas.numero_origem_factura",
      "facturas.data_origem_factura",
      "facturas.is_nota_credito",
      "facturas.moeda_iso",
      "facturas.total_contra_valor",
      "facturas.valor_cambio",
      "facturas.totalKwanza",
      "facturas.user_id",
      "facturas.user_id",
      "facturas.conta_id",
      "facturas.servico_id",
      "facturas.facturacao",
      "facturas.data_vencimento",
      "facturas.minutos_interconexao",
      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla",
      "facturas.pagamento_id",
      "pagamentos.valor_recebido",
      "pagamentos.troco",
      "bancos.nome as banco",
      "forma_pagamentos.designacao",
      "pagamentos.forma_pagamento_id",
      "contas.numeroExterno as numeroExterno",
      "contas.contaDescricao as contaDescricao",
      "lojas.nome as lojaNome",
      "filials.nome as filialNome",
      "movimento_adiantamentos.saldado_factura"
    )
      .from("facturas")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .leftJoin("pagamentos", "pagamentos.id", "facturas.pagamento_id")
      .leftJoin("forma_pagamentos", "forma_pagamentos.id", "pagamentos.forma_pagamento_id")
      .leftJoin("bancos", "bancos.id", "pagamentos.banco_id")
      .leftJoin("contas", "contas.id", "facturas.conta_id")
      .leftJoin("lojas", "contas.agencia_id", "lojas.id")
      .leftJoin("filials", "filials.id", "lojas.filial_id")
      .leftJoin("movimento_adiantamentos", "movimento_adiantamentos.saldado_factura", "facturas.id")
      .innerJoin("bill_runs", "bill_runs.factura_utilitie_id", "facturas.id")
      .innerJoin("bill_run_headers", "bill_run_headers.id", "bill_runs.bill_run_header_id")
      .innerJoin("tecnologias", "tecnologias.id", "bill_run_headers.tecnologia_id")

      //.innerJoin("series", "series.id", "bill_run_headers.serie_id")
      //.innerJoin("tecnologias", "tecnologias.id", "bill_run_headers.tecnologia_id")
      .where("bill_run_headers.estado", 1)
      .where("tecnologias.nome", "VOZ")

      .where("facturas.cliente_id", params.id);


    if (facturas.length == 0) {
      return DataResponse.response("success", 500, "Nenhuma Factura encontrado.", null);
    }

    return DataResponse.response("success", 200, null, facturas);
  }

  async findFacturasToSend({ params, auth }) {
    let factura = null;
    if (await this.findFacturasQtd(params)) {
      factura = await Database.select('facturas.id', 'facturas.factura_sigla', 'clientes.email', 'clientes.id as clienteId')
        .table('bill_run_headers')
        .leftJoin('bill_runs', 'bill_runs.bill_run_header_id', 'bill_run_headers.id')
        .leftJoin('facturas', 'facturas.id', 'bill_runs.factura_utilitie_id')
        .leftJoin('clientes', 'clientes.id', 'facturas.cliente_id')
        .whereNotNull('clientes.email')
        .whereNotIn("facturas.id", Database.select('factura_id').from("bill_run_emails").groupBy("factura_id"))
        .where('bill_run_headers.id', params.id).first();
      const dados = await Factura.gerarFactura(factura.id);
      await this.sendFacturaToEmail(dados, params.id, auth.user.id);
      await this.findFacturasToSend({ params, auth });
    }
    return DataResponse.response("success", 200, "", null);
  }


  async findFacturasQtd(params) {
    let facturas = [];
    facturas = await Database.select('facturas.id', 'facturas.factura_sigla', 'clientes.email', 'clientes.id as clienteId')
      .table('bill_run_headers')
      .leftJoin('bill_runs', 'bill_runs.bill_run_header_id', 'bill_run_headers.id')
      .leftJoin('facturas', 'facturas.id', 'bill_runs.factura_utilitie_id')
      .leftJoin('clientes', 'clientes.id', 'facturas.cliente_id')
      .whereNotNull('clientes.email')
      .whereNotIn("facturas.id", Database.select('factura_id').from("bill_run_emails"))
      .where('bill_run_headers.id', params.id);

    return (facturas.length > 0 ? true : false);
  }


  async findOneFacturasToSend({ params, auth }) {
    let factura = null;
      factura = await Database.select('facturas.id', 'facturas.factura_sigla', 'clientes.email', 'clientes.id as clienteId', 'bill_run_headers.id as header_id')
        .table('bill_run_headers')
        .leftJoin('bill_runs', 'bill_runs.bill_run_header_id', 'bill_run_headers.id')
        .leftJoin('facturas', 'facturas.id', 'bill_runs.factura_utilitie_id')
        .leftJoin('clientes', 'clientes.id', 'facturas.cliente_id')
        .whereNotNull('clientes.email')
        .where('facturas.id', params.id).first();
      const dados = await Factura.gerarFactura(factura.id);
      await this.sendFacturaToEmail(dados, factura.header_id, auth.user.id);
    return DataResponse.response("success", 200, "Factura enviada com sucesso", null);
  }


  async sendFacturaToEmail(dados, header_id, userLogado) {

    const res = await Database.select('*').from('bill_run_headers').where('id', header_id).first();

    const file_reference = await UNIG_PDF.PDFFactura(dados.factura, dados.produtos, dados.cliente, dados.user, dados.linha_pagamentos, dados.servico_conta, dados.facturas_clientes, dados.tecnologia, dados.por_pagar);

    //let testAccount = await nodemailer.createTestAccount();
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: Env.get('NODEMAILER_HOST'),
      port: Env.get('NODEMAILER_PORT'),
      secure: false, // true for 465, false for other ports
      auth: {
        user: Env.get('NODEMAILER_USER'), // generated ethereal user
        pass: Env.get('NODEMAILER_PASS') // generated ethereal password
      }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: '"Angola Telecom" <' + Env.get("NODEMAILER_USER") + '>', // sender address
      to: dados.cliente.email,
      subject: 'Factura do mês de ' + res.mes + ' de ' + res.ano + ' em anexo', // Subject line
      //text: "Ideias Dinâmicas! Obrigado pela preferência", // plain text body
      html: 'Estimado/a  ' + (dados.cliente.nome == null ? '' : dados.cliente.nome) + ' Junto enviamos a sua factura do mês de ' + res.mes + ' de ' + res.ano + ' em anexo. Para visualizar a sua factura precisa do Acrobat Reader versão 6 ou superior. <br/><br/> Para mais informações visite-nos em <a href="http://www.angolatelecom.com/">www.angolatelecom.com</a>, dirija-se a uma das nossas lojas ou contacte-nos através do seu gestor de conta e da linha de apoio ao cliente: <b>222 700 000 / 800 220 220</b>. <br/><br/> <b>Nota</b>: Esta é uma comunicação automática. Por favor não responda a este email.',
      attachments: [{
        filename: file_reference + '.pdf',
        path: 'public/uploads/facturas/' + file_reference + '.pdf',
        cid: file_reference //
      }]
    });

    //console.log("Message sent: %s", info.messageId);
    //console.log(info);

    const path = 'public/uploads/facturas/' + file_reference + '.pdf'

    fs.unlink(path, (err) => {
      if (err) {
        //console.error(err)
      }
    })


    const billRun = await BillRunEmailHeader.create({
      bill_run_header_id: header_id,
      user_id: userLogado
    });

    await BillRunEmail.create({
      bill_run_email_header_id: billRun.id,
      factura_id: dados.factura.id,
      cliente_email: dados.cliente.email,
      server_response: JSON.stringify(info)
    });

    return DataResponse.response("success", 200, "Sucesso.", null);

  }

  /**
   * Create/save a new billrunheader.
   * POST billrunheaders
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single billrunheader.
   * GET billrunheaders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing billrunheader.
   * GET billrunheaders/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update billrunheader details.
   * PUT or PATCH billrunheaders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a billrunheader with id.
   * DELETE billrunheaders/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  async  saveReportDiarioPDF({ request, auth }) {

    const userLogado = auth.user.id;

    const express = require("express");
    const bodyParser = require("body-parser");
    const app = express();

    app.use(bodyParser.json({ limit: '10mb', extended: true }))
    app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))

    const CryptoJS = require("crypto-js");
    const Base64 = require("js-base64");

    const { data, role_id, report_name } = request.all();

    //console.log(role_id)

    let new_cipher = '';

    for (let x = 0; x < data.length; x++) {
      if (data[x].toString() === "_")
        new_cipher += "/";
      else
        new_cipher += data[x];
    }

    const bytes = CryptoJS.AES.decrypt(new_cipher, "ask.me");

    let decrypted = bytes.toString(CryptoJS.enc.Utf8);
    decrypted = decrypted.replace('"', "") //CLEAN THE FIRST
    decrypted = decrypted.replace('"', "") //CLEAN THE LAST
    decrypted = decrypted.toString().slice(51);//THE CHARACTERS WE ARE INTERSTED ON BEGIN AT POSITION 51

    var buffer = Buffer.from(decrypted, 'base64');

    let dir = 'assets/uploads/reportdiario'
    if (!(fs.existsSync(dir))) {
      fs.mkdirSync(dir);
      fs.writeFile('assets/uploads/reportdiario/' + report_name + '.pdf', buffer, function (err) {
        if (err) throw err;
        return 200;
      });
    }
    else {

      fs.writeFile('assets/uploads/reportdiario/' + report_name + '.pdf', buffer, function (err) {
        if (err) throw err;
        return 200;
      });

    }

    await this.sendReportDiarioToEmail(role_id, report_name, userLogado)
  }

  async sendReportDiarioToEmail(role_id, report_name, userLogado) {

    let res = null;

    res = await Database.select('users.id', 'users.nome', 'users.email')
      .table('users')
      .innerJoin('roles', 'roles.id', 'users.role_id')
      .whereNotNull('users.email')
      .where('users.role_id', role_id)


    //let testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: Env.get('NODEMAILER_HOST'),
      port: Env.get('NODEMAILER_PORT'),
      secure: false, // true for 465, false for other ports
      auth: {
        user: Env.get('NODEMAILER_USER'), // generated ethereal user
        pass: Env.get('NODEMAILER_PASS') // generated ethereal password
      }
    });

    /*
    for (var i = 0; i < res.length; i++) {
      console.log(res[i].email)
    }
  */

    // send mail with defined transport object
    for (var i = 0; i < res.length; i++) {
      let info = transporter.sendMail({
        from: '"Angola Telecom" <' + Env.get("NODEMAILER_USER") + '>', // sender address
        to: res[i].email,
        subject: 'Report Diário', // Subject line
        //text: "Ideias Dinâmicas! Obrigado pela preferência", // plain text body
        html: 'Report Diário',
        attachments: [{
          filename: report_name + '.pdf',
          path: 'assets/uploads/reportdiario/' + report_name + '.pdf',
          cid: report_name //
        }]

      });
    }


    return DataResponse.response("success", 200, "Sucesso.", null);

  }




  async  downloadPDF({ request, auth }) {

    const userLogado = auth.user.id;
    /*

    let factura_id = factura;

    let randomNumbers = '';
    for (let i = 0; i < 10; i++) {
      randomNumbers += Math.floor(Math.random() * 9)
    }

    let pdfURL = 'http://localhost:4200/getPdfEmail/179080';
    //let pdfURL = 'http://localhost:4200/getPdfEmail/'+ factura_id;
    let pdfBuffer = await request.get({ uri: pdfURL, encoding: 'binary', headers:{'Content-type':'application/pdf'} });
    //let pdfBuffer = await request.get({uri: 'https://www.ieee.org/content/dam/ieee-org/ieee/web/org/pubs/ecf_faq.pdf', encoding: null});
    var filename = 'Factura_'+randomNumbers;
    fs.writeFileSync('assets/uploads/facturas/'+filename+'.pdf', pdfBuffer);
    *

    const express = require("express");
    const bodyParser = require("body-parser");
    const app = express();

    app.use(bodyParser.json({ limit: '10mb', extended: true }))
    app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))

    const CryptoJS = require("crypto-js");
    const Base64 = require("js-base64");

    const { data, factura_id, report_name, factura_numero } = request.all();

    let new_cipher = '';

    for (let x = 0; x < data.length; x++) {
      if (data[x].toString() === "_")
        new_cipher += "/";
      else
        new_cipher += data[x];
    }

    const bytes = CryptoJS.AES.decrypt(new_cipher, "ask.me");

    let decrypted = bytes.toString(CryptoJS.enc.Utf8);
    decrypted = decrypted.replace('"', "") //CLEAN THE FIRST
    decrypted = decrypted.replace('"', "") //CLEAN THE LAST
    decrypted = decrypted.toString().slice(51);//THE CHARACTERS WE ARE INTERSTED ON BEGIN AT POSITION 51

    var buffer = Buffer.from(decrypted, 'base64');


    fs.writeFile('assets/uploads/facturas/'+report_name+'.pdf', buffer, function(err) {
      if (err) throw err;
      return 200;
    });

    */

    const new_report_name = (factura_numero + '_' + report_name);

    //console.log(new_report_name)

    let dir = 'assets/uploads/facturas'
    if (!(fs.existsSync(dir))) {
      fs.mkdirSync(dir);
      fs.writeFile('assets/uploads/facturas/' + new_report_name + '.pdf', buffer, function (err) {
        if (err) throw err;
        return 200;
      });
    }
    else {

      fs.writeFile('assets/uploads/facturas/' + new_report_name + '.pdf', buffer, function (err) {
        if (err) throw err;
        return 200;
      });

    }

    await this.sendFacturaToEmail(factura_id, new_report_name, userLogado);
    return DataResponse.response("success", 200, "");
  }

  async meses({request}){

    const{ano} = request.all();
    const meses = await Database.select("mes").from("bill_run_headers").where("ano",ano).groupBy("mes").orderBy("mes","asc");

    return DataResponse.response("success", 200, "Sucesso.", meses);
  }

}

module.exports = BillRunHeaderController
