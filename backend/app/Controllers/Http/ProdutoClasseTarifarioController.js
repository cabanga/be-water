'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const ProdutoClasseTarifario = use("App/Models/ProdutoClasseTarifario");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')

const ClasseTarifarioRepository = use('App/Repositories/ClasseTarifarioRepository')
/**
 * Resourceful controller for interacting with produtoclassetarifarios
 */
class ProdutoClasseTarifarioController {

  #_classeTarifarioRepository

  constructor() {
    this.#_classeTarifarioRepository = new ClasseTarifarioRepository();
  }
  /**
   * Show a list of all produtoclassetarifarios.
   * GET produtoclassetarifarios
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "produto_classe_tarifarios.id",
        "produto_classe_tarifarios.produto_id",
        "produtos.nome as produto",
        "produto_classe_tarifarios.valor_fixo",
        "produtos.nome as produto",
        "produto_classe_tarifarios.classe_tarifario_id",
        "classe_tarifarios.valor",
        "classe_tarifarios.descricao",
        "classe_tarifarios.consumo_maximo",
        "classe_tarifarios.consumo_minimo",
        "tarifarios.descricao as tarifario",
      )
        .from("produto_classe_tarifarios")
        /*         .leftJoin("tarifarios", "tarifarios.id", "produto_classe_tarifarios.tarifario_id") */
        .leftJoin("produtos", "produtos.id", "produto_classe_tarifarios.produto_id")
        .leftJoin("classe_tarifarios", "classe_tarifarios.id", "produto_classe_tarifarios.classe_tarifario_id")
        .leftJoin("tarifarios", "tarifarios.id", "classe_tarifarios.tarifario_id")
        .orderBy(orderBy == null ? "produto_classe_tarifarios.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "produto_classe_tarifarios.id",
        "produto_classe_tarifarios.produto_id",
        "produtos.nome as produto",
        "produto_classe_tarifarios.classe_tarifario_id",
        "classe_tarifarios.valor",
        "classe_tarifarios.descricao",
        "classe_tarifarios.consumo_maximo",
        "classe_tarifarios.consumo_minimo",
        "tarifarios.descricao as tarifario",
        "produto_classe_tarifarios.valor_fixo",
      )
        .from("produto_classe_tarifarios")
        /*         .leftJoin("tarifarios", "tarifarios.id", "produto_classe_tarifarios.tarifario_id") */
        .leftJoin("produtos", "produtos.id", "produto_classe_tarifarios.produto_id")
        .leftJoin("classe_tarifarios", "classe_tarifarios.id", "produto_classe_tarifarios.classe_tarifario_id")
        .leftJoin("tarifarios", "tarifarios.id", "classe_tarifarios.tarifario_id")
        .where("tarifarios.descricao", "like", "%" + search + "%")
        .where("produtos.nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "produto_classe_tarifarios.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async selectBoxTarifario() {
    const res = await Database.select('id', 'descricao')
      .from('tarifarios')
      .orderBy('tarifarios.descricao', 'ASC')

    return DataResponse.response("sucess", 200, null, res)
  }

  async selectBoxProduto() {
    const res = await Database.select('id', 'nome')
      .from('produtos')
      .orderBy('nome', 'ASC')

    return res;
  }

  async selectBoxClasseTarifario() {
    const res = await Database.select('id', 'valor')
      .from('classe_tarifarios')
      .orderBy('classe_tarifarios.valor', 'ASC')

    return DataResponse.response("sucess", 200, null, res)
  }

  async store({ request, auth }) {
    const { classe_tarifario_id, produto_id, valor_fixo } = request.all();

    const tarifario = await ProdutoClasseTarifario.create({
      classe_tarifario_id: classe_tarifario_id,
      produto_id: produto_id,
      valor_fixo: valor_fixo
    });

    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      null
    );
  }

  /* async AdicionarProdutoClasseTarifario({ request, auth }) {
    const { classe_tarifario_id, produtos } = request.all();

    for (let index = 0; index < produtos.length; index++) {

      const verify = await ProdutoClasseTarifario.query()
      .where("classe_tarifario_id", classe_tarifario_id)
      .where("produto_id", produtos[index].id)
      .getCount();

      if (verify <= 0) {
        
      const tarifario = await ProdutoClasseTarifario.create({
        classe_tarifario_id: classe_tarifario_id,
        produto_id: produtos[index].id
        });
      }
    }
    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      null
    );
  } */

  async update({ params, request }) {
    const data = request.only(["classe_tarifario_id", "produto_id"]);

    const produtoclassetarifarios = await ProdutoClasseTarifario.find(params.id);
    produtoclassetarifarios.merge(data);
    await produtoclassetarifarios.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      produtoclassetarifarios
    );
  }

  async getClassePrecisaobyTarifario({ params, request }) {

    const data = request.only(["tarifario_id"]);

    classe_tarifario = await Database.select(
      "id",
      "valor",
      "consumo_minimo",
      "consumo_maximo",
    )
      .from("classe_tarifarios")
      .where("id", data.tarifario_id)

    return DataResponse.response("success", 200, "", classe_tarifario);
  }

  async getClassePrecisaoByTarifario({ params }) {

    let res = null;

    res = await Database.select(
      "classe_tarifarios.id",
      "classe_tarifarios.valor",
      "classe_tarifarios.consumo_minimo",
      "classe_tarifarios.consumo_maximo",
      "tarifarios.descricao as tarifario",
    )
      .from("classe_tarifarios")
      .leftJoin("tarifarios", "tarifarios.id", "classe_tarifarios.tarifario_id")
      .where("classe_tarifarios.tarifario_id", params.id)
    /* .orderBy("municipios.nome","ASC"); */

    return DataResponse.response("success", 200, "", res);
  }

  async getTarifario({ params }) {

    const cartaosupervisor = await Database.select(
      "cartao_supervisors.id",
      "cartao_supervisors.numero_cartao",
      "cartao_supervisors.pin",
      "cartao_supervisors.observacao",
      "estado_cartao_supervisors.nome as estadoCartaoSupervisor",
      "cartao_supervisors.estado_cartaosupervisors_id"
    )
      .from("cartao_supervisors")
      .leftJoin("estado_cartao_supervisors", "estado_cartao_supervisors.id", "cartao_supervisors.estado_cartaosupervisors_id")
      .where("cartao_supervisors.id", params.id).first()

    return DataResponse.response("success", 200, "", cartaosupervisor);
  }


  async getServicosByClasseTarifario({ params, request }) {

    const { conta_id } = request.all();

    let result = await this.#_classeTarifarioRepository.getServicosByClasseTarifario(params.id, conta_id);

    return DataResponse.response("success", 200, null, result);
  }

  async getServicoById({ params, request }) {

    const { conta_id } = request.all();

    let result = await this.#_classeTarifarioRepository.getServicoById(params.id, conta_id)

    return result;

  }


  /**
   * Render a form to be used for creating a new produtoclassetarifario.
   * GET produtoclassetarifarios/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

}

module.exports = ProdutoClasseTarifarioController
