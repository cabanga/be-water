'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const TipoMovimento = use("App/Models/StkTipoMovimento");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with stktipomovimentos
 */
class StkTipoMovimentoController {
  /**
   * Show a list of all stktipomovimentos.
   * GET stktipomovimentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

      res = await Database.select("id","descricao", "slug", "created_at")
        .from("stk_tipo_movimentos")
        .where(function() {
          if(search != null) {
            this.where("descricao", "like", "%" + search + "%")          
          }
        })
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
   

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new stktipomovimento.
   * GET stktipomovimentos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new stktipomovimento.
   * POST stktipomovimentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, auth }) {
    const { descricao, slug } = request.only(["descricao", "slug"]);

    const verifySlug = await TipoMovimento.query().where("slug", slug)
    .select("slug")
    .first();

    if (verifySlug != null){
      return DataResponse.response("success", 201, "Aviso: Esse slug já existe!", null);
    }

    await TipoMovimento.create({
      descricao: descricao,
      slug: slug,
      user_id: auth.user.id
    });
    return DataResponse.response(
      "success",
      200,
      "Registado com sucesso",
      null
    );
  }

  /**
   * Display a single stktipomovimento.
   * GET stktipomovimentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing stktipomovimento.
   * GET stktipomovimentos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update stktipomovimento details.
   * PUT or PATCH stktipomovimentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request }) {
    const data = request.only(["descricao", "slug"]);

    const verifySlug = await TipoMovimento.query()
    .where("slug", data.slug)
    .whereNot("id", params.id)
    .select("slug")
    .first();

    if (verifySlug != null){
      return DataResponse.response("success", 201, "Aviso: Esse slug já existe!", null);
    }

    const tipo_movimento = await TipoMovimento.find(params.id);
    tipo_movimento.merge(data);
    await tipo_movimento.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );
  }

  /**
   * Delete a stktipomovimento with id.
   * DELETE stktipomovimentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = StkTipoMovimentoController
