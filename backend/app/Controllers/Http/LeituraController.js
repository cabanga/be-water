'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Leitura = use("App/Models/Leitura");
const Contrato = use('App/Models/Contrato')
const TipoNaoLeitura = use('App/Models/TipoNaoLeitura')

const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const { response } = require('express');
var moment = require("moment");

class LeituraController {

  async index({ request, response, view }) {
    const { search, orderBy, pagination } = request.all();

    if (pagination) {
      const res = await Leitura
      .query()
      .where(function (params)  { if(search){ this.orWhere('estado', 'like', "%"+ search + "%" ) } })
      .with('roteiro')
      .orderBy('id', 'asc')
      .paginate(pagination.page, pagination.perPage)
      return res
    }

    const res = await Leitura
    .query()
    .where(function () { if(search){ this.orWhere('estado', 'like', "%"+ search + "%" ) } })
    .with('roteiro')
    .orderBy('id', 'asc')
    .paginate(pagination.page, pagination.perPage)

    .fetch()

    return DataResponse.response("success", 200, "", res);
  }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        'leituras.id',
        'leituras.estado',
        'leituras.rota_run_id',
        'leituras.leitura',
        'leituras.consumo',
        'leituras.ultima_leitura',
        'leituras.data_leitura',

        'leituras.motivo',
        'leituras.nao_leitura',

        'leituras.periodo',
        'rota_runs.rota_header_id',
        'rota_headers.descricao as rota_header',
        'leituras.contador_id',
        'contadores.numero_serie',
        'contadores.cliente_id',
        'clientes.nome as cliente',
        'clientes.telefone',
        'leituras.user_id as leitor_id',
        'users.nome as leitor',
        'leituras.created_at',
        'cil',

      )
        .table('leituras')
        .leftJoin('rota_runs', 'leituras.rota_run_id', 'rota_runs.id')

        .innerJoin(
          'local_consumos as local_consumo',
          'local_consumo.id', 'rota_runs.local_consumo_id'
        )

        .innerJoin(
          'local_instalacaos as local_instalacao',
          'local_instalacao.id', 'local_consumo.local_instalacao_id',
        )

        .leftJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
        .leftJoin('contadores', 'leituras.contador_id', 'contadores.id')
        .leftJoin('clientes', 'contadores.cliente_id', 'clientes.id')
        .leftJoin('users', 'leituras.user_id', 'users.id')

        .orderBy(orderBy == null ? 'leituras.created_at' : orderBy, 'DESC')
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        'leituras.id',
        'leituras.rota_run_id',
        'leituras.leitura',
        'leituras.ultima_leitura',
        'leituras.data_leitura',

        'leituras.motivo',
        'leituras.nao_leitura',

        'leituras.periodo',
        'rota_runs.rota_header_id',
        'rota_headers.descricao as rota_header',
        'leituras.contador_id',
        'contadores.numero_serie',
        'contadores.cliente_id',
        'clientes.nome as cliente',
        'clientes.telefone',
        'leituras.user_id as leitor_id',
        'users.nome as leitor',
        'leituras.created_at',
        'cil',
      )
        .table('leituras')
        .leftJoin('rota_runs', 'leituras.rota_run_id', 'rota_runs.id')

        .innerJoin(
          'local_consumos as local_consumo',
          'local_consumo.id', 'rota_runs.local_consumo_id'
        )

        .innerJoin(
          'local_instalacaos as local_instalacao',
          'local_instalacao.id', 'local_consumo.local_instalacao_id',
        )

        .leftJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
        .leftJoin('contadores', 'leituras.contador_id', 'contadores.id')
        .leftJoin('clientes', 'contadores.cliente_id', 'clientes.id')
        .leftJoin('users', 'leituras.user_id', 'users.id')

        .orWhere("leituras.leitura", "like", "%" + search + "%")
        .orWhere("leituras.ultima_leitura", "like", "%" + search + "%")
        .orWhere("contadores.numero_serie", "like", "%" + search + "%")
        .orWhere("rota_runs.periodo", "like", "%" + search + "%")
        .orWhere("clientes.nome", "like", "%" + search + "%")
        .orWhere("users.nome", "like", "%" + search + "%")

        .orderBy(orderBy == null ? "leituras.data_leitura" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }
    return DataResponse.response("success", 200, "", res);
  }


  async nao_leituras({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        'leituras.id',
        'leituras.estado',
        'leituras.rota_run_id',
        'leituras.leitura',
        'leituras.consumo',
        'leituras.ultima_leitura',
        'leituras.data_leitura',
        'leituras.motivo',
        'leituras.nao_leitura',
        'leituras.periodo',
        'rota_runs.rota_header_id',
        'rota_headers.descricao as rota_header',
        'leituras.contador_id',
        'contadores.numero_serie',
        'contadores.cliente_id',
        'clientes.nome as cliente',
        'clientes.telefone',
        'leituras.user_id as leitor_id',
        'users.nome as leitor',
        'leituras.created_at',
        'cil',

      )
      .table('leituras')
      .leftJoin('rota_runs', 'leituras.rota_run_id', 'rota_runs.id')
      .innerJoin(
        'local_consumos as local_consumo',
        'local_consumo.id', 'rota_runs.local_consumo_id'
      )
      .innerJoin(
        'local_instalacaos as local_instalacao',
        'local_instalacao.id', 'local_consumo.local_instalacao_id',
      )

      .leftJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
      .leftJoin('contadores', 'leituras.contador_id', 'contadores.id')
      .leftJoin('clientes', 'contadores.cliente_id', 'clientes.id')
      .leftJoin('users', 'leituras.user_id', 'users.id')
      .where("leituras.nao_leitura", true)

      .orderBy(orderBy == null ? 'leituras.created_at' : orderBy, 'DESC')
      .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        'leituras.id',
        'leituras.rota_run_id',
        'leituras.leitura',
        'leituras.ultima_leitura',
        'leituras.motivo',
        'leituras.nao_leitura',
        'leituras.data_leitura',
        'leituras.periodo',
        'rota_runs.rota_header_id',
        'rota_headers.descricao as rota_header',
        'leituras.contador_id',
        'contadores.numero_serie',
        'contadores.cliente_id',
        'clientes.nome as cliente',
        'clientes.telefone',
        'leituras.user_id as leitor_id',
        'users.nome as leitor',
        'leituras.created_at',
        'cil',
      )
      .table('leituras')
      .leftJoin('rota_runs', 'leituras.rota_run_id', 'rota_runs.id')
      .innerJoin(
        'local_consumos as local_consumo',
        'local_consumo.id', 'rota_runs.local_consumo_id'
      )
      .innerJoin(
        'local_instalacaos as local_instalacao',
        'local_instalacao.id', 'local_consumo.local_instalacao_id',
      )
      .leftJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
      .leftJoin('contadores', 'leituras.contador_id', 'contadores.id')
      .leftJoin('clientes', 'contadores.cliente_id', 'clientes.id')
      .leftJoin('users', 'leituras.user_id', 'users.id')
      .where("leituras.nao_leitura", true)
      .orWhere("leituras.leitura", "like", "%" + search + "%")
      .orWhere("leituras.ultima_leitura", "like", "%" + search + "%")
      .orWhere("contadores.numero_serie", "like", "%" + search + "%")
      .orWhere("rota_runs.periodo", "like", "%" + search + "%")
      .orWhere("clientes.nome", "like", "%" + search + "%")
      .orWhere("users.nome", "like", "%" + search + "%")
      .orderBy(orderBy == null ? "leituras.data_leitura" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);
    }
    return DataResponse.response("success", 200, "", res);
  }

  async create({ request, response, view }) {
  }

  async motivos_nao_leitura(){
    const res = await TipoNaoLeitura.all()
    return DataResponse.response("success", 200, "", res);
  }

  async contador_e_associados(id){
    let sql = await Database.select(
      'contador.id AS contador_id',
      'contador.ultima_leitura',
      'contador.numero_serie',
      'cliente.nome',
      'cliente.telefone',
      'cliente.morada',
      'tarifario.descricao AS tarifario_descricao',
      'classe_tarifario.id AS classe_tarifario_id',
      'classe_tarifario.descricao',
      'classe_tarifario.tarifa_variavel',
      'classe_tarifario.tarifa_fixa_mensal',
      'objecto_contrato.slug'
      )
      .table('contadores AS contador')
      .leftJoin('clientes as cliente','cliente.id', 'contador.cliente_id')
      .leftJoin('contratoes as contrato','cliente.id', 'contrato.cliente_id')
      .leftJoin('estado_contratoes as estado_contrato','estado_contrato.id', 'contrato.estado_contrato_id')
      .leftJoin('objecto_contratoes as objecto_contrato','contrato.objecto_contrato_id', 'objecto_contrato.id')
      .leftJoin('tarifarios as tarifario','tarifario.id', 'contrato.tarifario_id')
      .leftJoin('classe_tarifarios as classe_tarifario','classe_tarifario.tarifario_id', 'tarifario.id')
      .where('contador.id', id)
      .where('estado_contrato.slug', "ACTIVO")
      .first()

    return sql
  }

  async contador({ params }){
    await this.actualizar_media_de_consumo( params.id )
  }

  async calcule_media_de_consumo(contador_id){
    let quant_months = await Database
    .select('*')
    .table('configuracaos')
    .where('slug', 'meses_facturacao_media_consumo')
    .first()

    let month = quant_months ? Number(quant_months.valor) : 4

    const total_consumos = await Database
    .from('leituras')
    .where('contador_id', contador_id)
    .where('nao_leitura', false)
    .orderBy('created_at', 'DESC')
    .limit( month )
    .getAvg('consumo')

    return total_consumos
  }


  async actualizar_media_de_consumo(contador_id){
    let media_consumos = await this.calcule_media_de_consumo(contador_id)
    let media = media_consumos ? media_consumos : 0

    let contrato = await Database
    .select(
      'contador.id AS contador_id',
      'contrato.id AS contrato_id',
      'contador.ultima_leitura',
      'contador.numero_serie',
      'cliente.nome',
      'cliente.telefone',
      'cliente.morada'
    )
    .table('contadores AS contador')
    .leftJoin('clientes as cliente','cliente.id', 'contador.cliente_id')
    .leftJoin('contratoes as contrato','cliente.id', 'contrato.cliente_id')
    .leftJoin('estado_contratoes as estado_contrato','estado_contrato.id', 'contrato.estado_contrato_id')
    .where('contador.id', contador_id)
    .where('estado_contrato.slug', "ACTIVO")
    .first()

    await Contrato.query().where('id', contrato.contrato_id).update({ media_consumo: media })
  }

  async store({ request }) {

    var dataActual = moment(new Date()).format("YYYY-MM-DD");

    const {
      rota_run_id,
      contador_id,
      leitura,
      data_leitura,
      ultima_leitura,
      nao_leitura,
      motivo,
      user_id
    } = request.all();

    let contador = await this.contador_e_associados(contador_id)
    //let consumo = nao_leitura ? (await this.calcule_media_de_consumo(contador_id)) : Math.abs(Number(leitura) - Number(contador.ultima_leitura))

    await this.calcule_media_de_consumo(contador_id)

    let consumo = nao_leitura ? 0 : Math.abs(Number(leitura) - Number(ultima_leitura))

    const leitura_last = await Database.select("*")
    .table("leituras")
    .where("contador_id", contador_id)
    .orderBy("created_at", "DESC")
    .first()

    let last = Boolean(leitura_last) ? leitura_last : data_leitura

    await Leitura.create({
      classe_tarifario_id: contador.classe_tarifario_id,
      tarifario_descricao: contador.tarifario_descricao,

      data_ultima_leitura: last.data_leitura,
      nao_leitura: nao_leitura,
      motivo: motivo,
      rota_run_id: rota_run_id,
      contador_id: contador_id,
      leitura: leitura,
      consumo: consumo,
      ultima_leitura: ultima_leitura, // last.ultima_leitura,
      data_leitura: data_leitura,
      user_id: user_id
    });

    const estado_rota = await Database
      .select("*")
      .table("estado_rotas")
      .where("slug", "REALIZADA")
      .first();

    if (estado_rota != null) {
      await Database.select("*")
        .table("rota_runs")
        .where("id", rota_run_id)
        .update({
          estado_rota_id: estado_rota.id,
          nao_leitura: nao_leitura,
          motivo: motivo,
        });

      await Database.select("*")
        .table("contadores")
        .where("id", contador_id)
        .update({
          ultima_leitura: leitura,
          'updated_at': dataActual
        });
    }

    return DataResponse.response("success", 200, "Registado com sucesso", null)
  }


  async getLeiturasByLeitorAPI({ params }) {
    let res = null;

    res = await Database.select(
      'leituras.id',
      'leituras.rota_run_id',
      'leituras.leitura',
      'leituras.ultima_leitura',
      'leituras.data_leitura',
      'leituras.periodo',
      'rota_runs.rota_header_id',
      'rota_headers.descricao as rota_header',
      'leituras.contador_id',
      'contadores.numero_serie as contador',
      'contadores.cliente_id',
      'clientes.nome',
      'clientes.telefone',
      'leituras.user_id as leitor_id',
      'users.nome as leitor',
      'leituras.created_at'
    )
      .table('leituras')
      .leftJoin('rota_runs', 'leituras.rota_run_id', 'rota_runs.id')
      .leftJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
      .leftJoin('contadores', 'leituras.contador_id', 'contadores.id')
      .leftJoin('clientes', 'contadores.cliente_id', 'clientes.id')
      .leftJoin('users', 'leituras.user_id', 'users.id')

      .where("leituras.user_id", params.id)

      .orderBy('leituras.created_at', 'DESC')


    //console.log(res);

    return DataResponse.response("success", 200, "", res);
  }


  async storeAPI({ request }) {

    var dataActual = moment(new Date()).format("YYYY-MM-DD");

    const {
      rota_run_id,
      contador_id,
      leitura,
      ultima_leitura,
      data_leitura,
      user_id,
      latitude,
      longitude
    } = request.all();

    //console.log(request.all());

    const ult_leitura = await Database.select("*")
      .table("leituras")
      .where("rota_run_id", rota_run_id)
      .where("contador_id", contador_id)
      .where("user_id", user_id)
      .orderBy("created_at", "DESC")
      .first();

    await Leitura.create({
      rota_run_id: rota_run_id,
      contador_id: contador_id,
      leitura: leitura,
      ultima_leitura: ult_leitura,
      data_leitura: data_leitura,
      latitude: latitude,
      longitude: longitude,
      user_id: user_id
    });

    const estado_rota = await Database.select("*")
      .table("estado_rotas")
      .where("slug", "REALIZADA").first();

    if (estado_rota != null) {
      await Database.select("*")
        .table("rota_runs")
        .where("id", rota_run_id)
        .update({
          estado_rota_id: estado_rota.id,
          'updated_at': dataActual
        });

      await Database.select("*")
        .table("contadores")
        .where("id", contador_id)
        .update({
          ultima_leitura: leitura,
          'updated_at': dataActual
        });

    }

    //console.log(rota_run);

    return DataResponse.response(
      "success",
      200,
      "Registado com sucesso",
      null
    );

  }

  async storeArrayAPI({ request }) {

    var dataActual = moment(new Date()).format("YYYY-MM-DD");

    const array_leituras = request.all();

    //console.log(array_leituras);

    for (let index = 0; index < array_leituras.length; index++) {
      const element = array_leituras[index];

      const ult_leitura = await Database.select("*")
        .table("leituras")
        .where("rota_run_id", array_leituras[index].rota_run_id)
        .where("contador_id", array_leituras[index].contador_id)
        .where("user_id", array_leituras[index].user_id)
        .orderBy("created_at", "DESC")
        .first();

      await Leitura.create({
        rota_run_id: array_leituras[index].rota_run_id,
        contador_id: array_leituras[index].contador_id,
        leitura: array_leituras[index].leitura,
        ultima_leitura: ult_leitura,
        data_leitura: array_leituras[index].data_leitura,
        latitude: array_leituras[index].latitude,
        longitude: array_leituras[index].longitude,
        user_id: array_leituras[index].user_id
      });

      const estado_rota = await Database.select("*")
        .table("estado_rotas")
        .where("slug", "REALIZADA").first();

      if (estado_rota != null) {
        await Database.select("*")
          .table("rota_runs")
          .where("id", array_leituras[index].rota_run_id)
          .update({
            'estado_rota_id': estado_rota.id,
            'updated_at': dataActual
          });

        await Database.select("*")
          .table("contadores")
          .where("id", array_leituras[index].contador_id)
          .update({
            'ultima_leitura': array_leituras[index].leitura,
            'updated_at': dataActual
          });
      }

    }

    /*

    */
    //console.log(rota_run);

    return DataResponse.response(
      "success",
      200,
      "Leituras Registadas com sucesso",
      null
    );

  }

  /**
   * Display a single leitura.
   * GET leituras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing leitura.
   * GET leituras/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update leitura details.
   * PUT or PATCH leituras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only([
      'contador_id',
      'nao_leitura',
      'motivo',
      'leitura',
      'perido',
      'leitura',
      'ultima_leitura',
      'data_leitura',
      'consumo',
      'estado'
    ])

    try {
      let res = await Leitura.find(params.id)

      data.consumo = Math.abs(Number(data.leitura) - Number(data.ultima_leitura))
      await Leitura.query().where('id', params.id).update(data)
      res = await Leitura.find(params.id)
      return DataResponse.response("success", 200, "Actualização feita com sucesso.", res.data)
    } catch (error) {
      console.log( error )
      return DataResponse.response("info", 201, "Falha não actualizar, por favor verifica o erro", error)
    }
  }


  /**
   * Delete a leitura with id.
   * DELETE leituras/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = LeituraController
