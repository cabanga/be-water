'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const AbastecimentoCil = use("App/Models/AbastecimentoCil");
const Database = use("Database");
var moment = require("moment");

/**
 * Resourceful controller for interacting with abastecimentocils
 */
class AbastecimentoCilController {
  /**
   * Show a list of all abastecimentocils.
   * GET abastecimentocils
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new abastecimentocil.
   * GET abastecimentocils/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new abastecimentocil.
   * POST abastecimentocils
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single abastecimentocil.
   * GET abastecimentocils/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing abastecimentocil.
   * GET abastecimentocils/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update abastecimentocil details.
   * PUT or PATCH abastecimentocils/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a abastecimentocil with id.
   * DELETE abastecimentocils/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
    
  async selectBox ({ request }) {  
    const res = await Database.select('id', 'nome', 'slug')
    .from('abastecimento_cils')
    .orderBy('nome', 'ASC');
     
    return res;
  }

}

module.exports = AbastecimentoCilController
