'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");  
const Adiantamento = use("App/Models/Adiantamento");
const Database = use("Database");
const MovimentoAdiantamento = use("App/Models/MovimentoAdiantamento");
/**
 * Resourceful controller for interacting with adiantamentos
 */
class AdiantamentoController {
  /**
   * Show a list of all adiantamentos.
   * GET adiantamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new adiantamento.
   * GET adiantamentos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new adiantamento.
   * POST adiantamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
   async store({request, auth }) {
    const { valor, cliente_id, referencia, banco_id, data_pagamento} = request.all();  
      let a = null
      a = await Database.table("adiantamentos").where("cliente_id", cliente_id).first();
      if (a == null) {
        a = await Adiantamento.create({
          valor: valor,          
          cliente_id: cliente_id,
          descricao: "criação por interface",
          user_id: auth.user.id
        });
      }

    const mov = await MovimentoAdiantamento.create({
      data_pagamento: data_pagamento,
      referencia: referencia,
      banco_id: banco_id,
      valor: valor,
      descritivo: "adiantamento gerado manualmente",
      saldado: false,
      adiantamento_id: a.id,
      user_id: auth.user.id
    }); 
    const list = await Database.raw("UPDATE adiantamentos SET valor = (SELECT SUM(valor) FROM movimento_adiantamentos WHERE saldado = 0 AND adiantamento_id = " + a.id + ") WHERE cliente_id = " + a.cliente_id);       
    
    return DataResponse.response("success", 200, "Registo efectuado com sucesso.", list);
  }


  /**
   * Display a single adiantamento.
   * GET adiantamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing adiantamento.
   * GET adiantamentos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update adiantamento details.
   * PUT or PATCH adiantamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a adiantamento with id.
   * DELETE adiantamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async adiantamentoFactura({ request }){
     const {cliente_id}=request.all();
     const list = await Adiantamento.AdiantamentoFactura(cliente_id);

     return DataResponse.response("success",200,null, list );
  }
}

module.exports = AdiantamentoController
