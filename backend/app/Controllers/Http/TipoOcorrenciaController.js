'use strict'
const TipoOcorrencia = use("App/Models/TipoOcorrencia");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");


class TipoOcorrenciaController {

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        'tipo_ocorrencias.id',
        'tipo_ocorrencias.nome',
        'tipo_ocorrencias.is_active',
        'tipo_ocorrencias.user_id as user_id',
        'users.nome as user',
        'tipo_ocorrencias.created_at',
        'tipo_ocorrencias.updated_at'
      )
        .table('tipo_ocorrencias')
        .leftJoin('users', 'tipo_ocorrencias.user_id', 'users.id')

        .orderBy(orderBy == null ? 'tipo_ocorrencias.created_at' : orderBy, 'DESC')
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        'tipo_ocorrencias.id',
        'tipo_ocorrencias.nome',
        'tipo_ocorrencias.is_active',
        'tipo_ocorrencias.user_id as user_id',
        'users.nome as user',
        'tipo_ocorrencias.created_at',
        'tipo_ocorrencias.updated_at'
      )
        .table('tipo_ocorrencias')
        .leftJoin('users', 'tipo_ocorrencias.user_id', 'users.id')

        .orWhere("tipo_ocorrencias.nome", "like", "%" + search + "%")

        .orderBy(orderBy == null ? "tipo_ocorrencias.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }
    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    
    const {
      nome,
      user_id
    } = request.all();

    const Verify = await Database.table('tipo_ocorrencias')
      .where("nome", nome)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Esse Tipo de Ocorrência já existe",
        Verify
      );
    } else {
      const ocorrencia = await TipoOcorrencia.create({
        nome: nome,
        is_active: true,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        ocorrencia
      );
    }
  }

  async update({ params, request, auth }) {

    const {
      nome,
      is_active,
      user_id
    } = request.all();

    const rota_run = await TipoOcorrencia.find(params.id);
    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('tipo_ocorrencias')
      .where("nome", nome)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Esse Tipo de Ocorrência já existe",
        Verify
      );
    } else {
      const ocorrencia = await Database.table('tipo_ocorrencias')
        .where('id', params.id)
        .update({
          nome: nome,
          is_active: is_active,
          user_id: auth.user.id,
          'updated_at': dataActual
        });
        return DataResponse.response(
          "success",
          200,
          "Dados actualizados com sucesso",
          ocorrencia
        );
    }
  }

  async selectBox({ request }) {
    let res = null;

    res = await Database.select(
      'tipo_ocorrencias.id',
      'tipo_ocorrencias.nome'
    )
      .table('tipo_ocorrencias')

      .where('tipo_ocorrencias.is_active', true)
      .orderBy('tipo_ocorrencias.nome', 'ASC');

    return res;
  }
}

module.exports = TipoOcorrenciaController
