'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const Rua = use("App/Models/Rua");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage');
/**
 * Resourceful controller for interacting with ruas
 */
class RuaController {
  /**
   * Show a list of all ruas.
   * GET ruas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "ruas.id",
        "ruas.nome",
        "ruas.is_active",
        "ruas.quarteirao_id",
        "quarteiraos.nome as quarteirao",
        "ruas.bairro_id",
        "bairros.nome as bairro",
        "bairros.distrito_id",
        "distritos.nome as distrito",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "ruas.user_id",
        "users.nome as user"
      )
        .from("ruas")
        .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
        .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
        .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
        .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
        .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
        .innerJoin("users", "users.id", "ruas.user_id")
        .orderBy(orderBy == null ? "ruas.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "ruas.id",
        "ruas.nome",
        "ruas.is_active",
        "ruas.quarteirao_id",
        "quarteiraos.nome as quarteirao",
        "ruas.bairro_id",
        "bairros.nome as bairro",
        "bairros.distrito_id",
        "distritos.nome as distrito",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "ruas.user_id",
        "users.nome as user"
      )
        .from("ruas")
        .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
        .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
        .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
        .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
        .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
        .innerJoin("users", "users.id", "ruas.user_id")

        .where("ruas.nome", "like", "%" + search + "%")
        .orWhere("provincias.nome", "like", "%" + search + "%")
        .orWhere("municipios.nome", "like", "%" + search + "%")
        .orWhere("distritos.nome", "like", "%" + search + "%")
        .orWhere("bairros.nome", "like", "%" + search + "%")
        .orWhere("users.nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "ruas.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async getRuaById({ params }) {
    let res = null;

    res = await Database.select(
      "ruas.id",
      "ruas.nome",
      "ruas.is_active",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "ruas.user_id",
      "users.nome as user"
    )
      .from("ruas")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
      .innerJoin("users", "users.id", "ruas.user_id")

      .where("ruas.id", params.id)
      .first();

    return DataResponse.response("success", 200, "", res);
  }

  async getRuasByQuarteirao({ params }) {
    let res = null;

    res = await Database.select(
    "ruas.id",
    "ruas.nome",
    "ruas.is_active",
    "ruas.quarteirao_id",
    "quarteiraos.nome as quarteirao",
    "ruas.bairro_id",
    "bairros.nome as bairro",
    "bairros.distrito_id",
    "distritos.nome as distrito",
    "bairros.municipio_id",
    "municipios.nome as municipio",
    "municipios.provincia_id",
    "provincias.nome as provincia",
    "ruas.user_id",
    "users.nome as user"
    )
    .from("ruas")
    .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
    .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
    .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
    .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
    .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
    .innerJoin("users", "users.id", "ruas.user_id")

    .where("ruas.quarteirao_id", params.id)
    .orderBy("ruas.nome", "ASC");

    return DataResponse.response("success", 200, "", res);
  }

  async getRuasByBairro({ params }) {
    let res = null;

    res = await Database.select(
      "ruas.id",
      "ruas.nome",
      "ruas.is_active",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "ruas.user_id",
      "users.nome as user"
    )
      .from("ruas")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
      .innerJoin("users", "users.id", "ruas.user_id")

      .where("ruas.bairro_id", params.id)
      .orderBy("ruas.nome", "ASC");

    return DataResponse.response("success", 200, "", res);
  }



  /**
   * Render a form to be used for creating a new rua.
   * GET ruas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new rua.
   * POST ruas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {

    const {
      nome,
      bairro_id,
      quarteirao_id,
      user_id
    } = request.all();

    //console.log("request");
    //console.log(request.all());

    const res = await Rua.query()
      .where("nome", nome)
      .where("bairro_id", bairro_id)
      .getCount();

    if (res > 0) {
      return DataResponse.response(
        null,
        500,
        "Esta rua já existe.",
        null
      );
    } else {
      await Rua.create({
        nome: nome,
        bairro_id: bairro_id,
        quarteirao_id: quarteirao_id,
        is_active: true,
        user_id: user_id
      });
      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
    }

  }

  /**
   * Display a single rua.
   * GET ruas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing rua.
   * GET ruas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update rua details.
   * PUT or PATCH ruas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {

    const {
      nome,
      bairro_id,
      quarteirao_id,
      is_active,
      user_id
    } = request.all();

    const rua = await Rua.find(params.id);

    rua.merge({
      nome: nome,
      bairro_id: bairro_id,
      quarteirao_id: quarteirao_id,
      is_active: is_active,
      user_id: user_id
    });
    await rua.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );

  }

  /**
   * Delete a rua with id.
   * DELETE ruas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }


  async selectBoxByBairro({ params }) {

    let res = await Database.select(
      "ruas.id",
      "ruas.nome",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
    )
      .from("ruas")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .innerJoin("provincias", "provincias.id", "municipios.provincia_id")

      .where("ruas.bairro_id", params.id)
      .where("ruas.is_active", true)
      .orderBy("ruas.nome", "ASC");

    //console.log(res);

    return res;

  }

}

module.exports = RuaController
