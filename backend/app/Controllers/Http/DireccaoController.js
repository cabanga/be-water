'use strict'
const Direccao = use("App/Models/Direccao");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

const DireccaoRepository = use('App/Repositories/DireccaoRepository')


class DireccaoController {
    #_direccaoRepo

    constructor() {
        this.#_direccaoRepo = new DireccaoRepository()
    }

    async listagem({ request }) {
        const { search, orderBy, pagination } = request.all();
        let res = null;

        if (search == null) {
            res = await await Database.select("id", "designacao", "slug", "created_at")
                .from("direccaos")
                .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
                .paginate(pagination.page, pagination.perPage);

        } else {
            res = await await Database.select("id", "designacao", "slug", "created_at")
                .from("direccaos")
                .where("designacao", "like", "%" + search + "%")
                .orWhere("slug", "like", "%" + search + "%")
                .orWhere(Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'), "like", "%" + search + "%")
                .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
                .paginate(pagination.page, pagination.perPage);
        }

        return DataResponse.response("success", 200, "", res);
    }

    async selectBox() {
        const res = await Database.select(
            'id',
            'designacao'
        )
            .from('direccaos')
            .orderBy('designacao', 'ASC')
        return res;
    }

    async index({ response, request }) {
        const page = request.input('page')
        const search = request.input('search')
        const direccoes = await this.#_direccaoRepo.getAll(page, search)
        return response.ok(direccoes);
    }

    async store({ request }) {
        const data = request.only([
            'designacao',
            'user_id'
        ])
        const res = await this.#_direccaoRepo.create(data)
        return res
    }

    async show({ params, response }) {
        const gender = await this.#_direccaoRepo.findById(params.id)
        return response.ok(gender)
    }

    async update({ params, request }) {
        const data = request.only([
            'designacao',
            'user_id'
        ])
        const res = await this.#_direccaoRepo.update(params.id, data)
        return res
    }

    async delete({ params }) {
        const res = await this.#_direccaoRepo.delete(params.id)
        return res
    }
}

module.exports = DireccaoController
