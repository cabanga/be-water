'use strict'
const Caucao = use("App/Models/Caucao");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");


class CaucaoController {

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        'caucaos.id',
        'caucaos.nome',
        'caucaos.slug',
        'caucaos.is_active',
        'caucaos.user_id as user_id',
        'users.nome as user',
        'caucaos.created_at',
        'caucaos.updated_at'
      )
        .table('caucaos')
        .leftJoin('users', 'caucaos.user_id', 'users.id')

        .orderBy(orderBy == null ? 'caucaos.created_at' : orderBy, 'DESC')
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        'caucaos.id',
        'caucaos.nome',
        'caucaos.slug',
        'caucaos.is_active',
        'caucaos.user_id as user_id',
        'users.nome as user',
        'caucaos.created_at',
        'caucaos.updated_at'
      )
        .table('caucaos')
        .leftJoin('users', 'caucaos.user_id', 'users.id')

        .orWhere("caucaos.nome", "like", "%" + search + "%")

        .orderBy(orderBy == null ? "caucaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }
    return DataResponse.response("success", 200, "", res);
  }

  async getCaucaoById({ params }) {

    let res = await Database.select(
      'caucaos.id',
      'caucaos.nome',
      'caucaos.slug'
    )
      .table('caucaos')
      .where('caucaos.id', params.id)
      .first();

    return res;
  }

  async store({ request, auth }) {

    const {
      nome,
      slug,
      user_id
    } = request.all();

    const Verify = await Database.table('caucaos')
      .where("nome", nome)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Esse Tipo de Ocorrência já existe",
        Verify
      );
    } else {
      const caucao = await Caucao.create({
        nome: nome,
        slug: slug,
        is_active: true,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        caucao
      );
    }
  }

  async update({ params, request, auth }) {

    const {
      nome,
      is_active,
      user_id
    } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('caucaos')
      .where("nome", nome)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Esse Tipo de Ocorrência já existe",
        Verify
      );
    } else {
      const caucao = await Database.table('caucaos')
        .where('id', params.id)
        .update({
          nome: nome,
          is_active: is_active,
          user_id: auth.user.id,
          'updated_at': dataActual
        });
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        caucao
      );
    }
  }

  async selectBox({ request }) {
    let res = null;

    res = await Database.select(
      'caucaos.id',
      'caucaos.nome',
      'caucaos.slug'
    )
      .table('caucaos')

      .where('caucaos.is_active', true)
      .orderBy('caucaos.nome', 'ASC');

    return res;
  }
}

module.exports = CaucaoController
