'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const FormaPagamento = use('App/Models/FormaPagamento'); 
const Cliente = use('App/Models/Cliente'); 
const Factura = use('App/Models/Factura'); 
const Produto = use('App/Models/Produto'); 
const Documento = use('App/Models/Documento'); 

const User = use('App/Models/User');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database');

/**
 * Resourceful controller for interacting with formapagamentos
 */
class DashboardController {
  /**
   * Show a list of all facturas.
   * GET facturas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({auth}) { 
     var moment = require('moment');

    const clienteCount = await Cliente.getCount(); 
	const produtoCount = await Produto.getCount(); 
	const facturaCount = await Factura.query().where('user_id',auth.user.id).getCount(); 
	const documentoCount = await Documento.getCount();  
	
	const facturaSumTotalSemImpostoGeral = await Factura.query().where('user_id',auth.user.id).getSum('totalSemImposto')
	 
	const facturaSumTotalSemImpostoHoje = await Factura.query().where('user_id',auth.user.id).where(Database.raw('DATE_FORMAT(created_at, "%d")'), moment(new Date()).format("DD")).getSum('totalSemImposto')
	const facturaSumTotalSemImpostoOntem = await Factura.query().where('user_id',auth.user.id).where(Database.raw('DATE_FORMAT(created_at, "%d")'), (moment(new Date()).format("DD")-1)).getSum('totalSemImposto')
	
	const facturaSumTotalSemImpostoMes = await Factura.query().where('user_id',auth.user.id).where(Database.raw('DATE_FORMAT(created_at, "%m")'), moment(new Date()).format("MM")).getSum('totalSemImposto')
	const facturaSumTotalSemImpostoMesAnterior = await Factura.query().where('user_id',auth.user.id).where(Database.raw('DATE_FORMAT(created_at, "%m")'), (moment(new Date()).format("MM"))-1).getSum('totalSemImposto')
	
	//-----------------------------------------   -----------------------------------------------
	const facturaSumTotalComImpostoGeral = await Factura.query().where('user_id',auth.user.id).getSum('totalSemImposto')
	
	const facturaSumTotalComImpostoHoje = await Factura.query().where('user_id',auth.user.id).where(Database.raw('DATE_FORMAT(created_at, "%d")'), moment(new Date()).format("DD")).getSum('totalComImposto')
	const facturaSumTotalComImpostoOntem = await Factura.query().where('user_id',auth.user.id).where(Database.raw('DATE_FORMAT(created_at, "%d")'), (moment(new Date()).format("DD")-1)).getSum('totalComImposto')
	
	const facturaSumTotalComImpostoMes = await Factura.query().where('user_id',auth.user.id).where(Database.raw('DATE_FORMAT(created_at, "%m")'), moment(new Date()).format("MM")).getSum('totalComImposto')
	const facturaSumTotalComImpostoMesAnterior = await Factura.query().where('user_id',auth.user.id).where(Database.raw('DATE_FORMAT(created_at, "%m")'), (moment(new Date()).format("MM"))-1).getSum('totalComImposto')
	
 
     
    return DataResponse.response("success", 200, "",{
		clienteCount: clienteCount,
		facturaCount: facturaCount,
		produtoCount: produtoCount,
		documentoCount: documentoCount,
		
		facturaSumTotalSemImpostoHoje: facturaSumTotalSemImpostoHoje,
		facturaSumTotalSemImpostoMes: facturaSumTotalSemImpostoMes,
		facturaSumTotalSemImpostoOntem:facturaSumTotalSemImpostoOntem,
		facturaSumTotalSemImpostoMesAnterior:facturaSumTotalSemImpostoMesAnterior,
		facturaSumTotalSemImpostoGeral:facturaSumTotalSemImpostoGeral,
		
		facturaSumTotalComImpostoHoje: facturaSumTotalComImpostoHoje,
		facturaSumTotalComImpostoMes: facturaSumTotalComImpostoMes,
		facturaSumTotalComImpostoOntem:facturaSumTotalComImpostoOntem,
		facturaSumTotalComImpostoMesAnterior:facturaSumTotalComImpostoMesAnterior,
		facturaSumTotalComImpostoGeral:facturaSumTotalComImpostoGeral
	});
  } 
}

module.exports = DashboardController
