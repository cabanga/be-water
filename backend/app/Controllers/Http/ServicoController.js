'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Wimax = use("App/Models/Wimax");
const Servico = use("App/Models/Servico");
const Conta = use("App/Models/Conta");
const PedidoServico = use("App/Models/PedidoServico");
const Prevision = use("App/Models/Prevision");
const LogEstadoServico = use("App/Models/LogEstadoServico");
const LogMudancaContaServico = use("App/Models/LogMudancaContaServico");
const LogEstadoPedido = use("App/Models/LogEstadoPedido");
const CdmaServico = use("App/Models/CdmaServico");
const FlatRateServico = use("App/Models/FlatRateServico");
const DataResponse = use("App/Models/DataResponse");


const Database = use("Database");
/**
 * Resourceful controller for interacting with servicos
 */
class ServicoController {
  /**
   * Show a list of all servicos.
   * GET servicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new servico.
   * GET servicos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new servico.
   * POST servicos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { conta_id, tarifario_id, numero_serie, } = request.all();


      const servico = await Servico.create({
        //chaveServico: chaveServico,
        conta_id: conta_id,
        //tecnologia: tecnologia,
        tarifario_id: tarifario_id,
        estado: 1,
        user_id: auth.user.id
      });

      await Database
        .table('contadores')
        .where('id', numero_serie)
        .update({ 'servico_id': servico.id })


      return DataResponse.response("success", 200, "Registo efectuado com sucesso", servico);

    

  }

  /**
   * Display a single servico.
   * GET servicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing servico.
   * GET servicos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update servico details.
   * PUT or PATCH servicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a servico with id.
   * DELETE servicos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  async selectBoxServicosConta({ params }) {

    const servicos = await Database.select(
      "servicos.id",
      "produtos.nome as servico",
      "contratoes.conta_id",   
    )
      .from("servicos")
      .innerJoin("produtos", "produtos.id", "servicos.produto_id") 
      .innerJoin("contratoes", "contratoes.conta_id", "servicos.contrato_id") 
      .where("servicos.estado", 1)
      .where("contratoes.conta_id", params.id);

    return DataResponse.response("success", 200, "", servicos);
  }

  async servicosClientes({ params, request }) {

    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select('co.numero_serie as contadorSerie','co.contador as contador','se.created_at', 'se.conta_id', 'se.id as servicoID', 'se.ChaveServico as chaveServico', 'se.tarifario_id', 'ta.descricao as tarifario', 'te.nome as tecnologia')
        .table('servicos as se')
        .innerJoin("tarifarios as ta", "ta.id", "se.tarifario_id")
        .innerJoin("tecnologias as te", "te.id", "ta.tecnologia_id")
        .innerJoin("contadores as co", "co.servico_id", "se.id")
        .where("conta_id", params.id)
        .orderBy(orderBy == null ? "se.id" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);

    } else {

      res = await Database.select('co.numero_serie as contadorSerie','co.contador as contador', 'se.created_at','se.conta_id', 'se.id as servicoID', 'se.ChaveServico as chaveServico', 'se.tarifario_id', 'ta.descricao as tarifario','te.nome as tecnologia')
        .table('servicos as se')
        .innerJoin("tarifarios as ta", "ta.id", "se.tarifario_id")
        .innerJoin("tecnologias as te", "te.id", "ta.tecnologia_id")
        .innerJoin("contadores as co", "co.servico_id", "se.id")
        .where("conta_id", params.id)
        .where("se.ChaveServico", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "se.id" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);

    }

    //console.log(res)

    return DataResponse.response("success", 200, "", res);
  }

  async getServico({ params }) {

    const servico = await Database.select('es.nome as estadoDescricao', 'se.conta_id', 'se.estado as estadoActualID', 'se.conta_id as conta_id', 'se.id as servicoID', 'se.ChaveServico as chaveServico', 'ta.descricao as tarifario', 'ta.tecnologia as tecnologia')
      .table('servicos as se')
      .innerJoin("tarifarios as ta", "ta.id", "se.tarifario_id")
      .innerJoin("estado_servicos as es", "es.id", "se.estado")
      .where("se.id", params.id).first()

    return DataResponse.response("success", 200, "", servico);
  }

  async updatEstado({ params, request, auth }) {
    const data = request.only(['estado']);
    const log = request.only(['estado', 'estado_actual_id']);

    const servico = await Servico.find(params.id);
    servico.merge(data);
    await servico.save();

    await LogEstadoServico.create({ 'servico_id': params.id, 'id_estado_anterior': log.estado_actual_id, 'user_id': auth.user.id, 'id_estado_novo': log.estado })

    return DataResponse.response("success", 200, "Estado actualizado com sucesso", servico);

  }

  async mudancaContaServico({ request, auth }) {
    const data = request.only(['conta_id', 'servico_id', 'conta_antiga']);

    //console.log(data)

    const servico = await Database
      .table('servicos')
      .where('id', data.servico_id)
      .update('conta_id', data.conta_id)

    await LogMudancaContaServico.create({ 'servico_id': data.servico_id, 'conta_antiga_id': data.conta_antiga, 'user_id': auth.user.id, 'conta_nova_id': data.conta_id })

    return DataResponse.response("success", 200, "Actualizado com sucesso", servico);

  }

  async gerarContracto({ params }) {

    let cliente = null;
    let servicosCdma = null;
    let servicosWimax = null;
    let servicosCobre = null;
    let servicosCobreADSL = null;
    let servicosLTE = null;

    const servicos = await Database.select(Database.raw('DATE_FORMAT(se.updated_at, "%d/%m/%Y") as dataEmissao'), 'se.id as idServico', 'se.conta_id as conta_id', 'se.ChaveServico as chaveServico', 'ta.descricao as tarifario', 'ta.tecnologia as tecnologia')
      .table('servicos as se')
      .innerJoin("tarifarios as ta", "ta.id", "se.tarifario_id")
      //.where("se.chaveServico", params.id).first()
      .where("se.id", params.id).first()

    servicosCdma = await Database.select(Database.raw('DATE_FORMAT(se.updated_at, "%d/%m/%Y") as dataEmissao'), 'fi.id as cdmaFiliaiD', 'cs.ChaveServico as cdmaChaveServico', 'fi.nome as filialNome', 'di.cargo as directorCargo', 'di.nome as directorNome', 'cf.FabricanteDesc as cdmaFabricanteDesc', 'cm.DescricaoModelo as cdmaModelo', 'ce.Num_Serie as cdmaEquipamentoSerie', 'se.conta_id as conta_id', 'se.ChaveServico as chaveServico', 'ta.descricao as tarifario', 'ta.tecnologia as tecnologia')
      .table('servicos as se')
      .innerJoin("tarifarios as ta", "ta.id", "se.tarifario_id")
      .leftJoin("cdma_servicos as cs", "se.id", "cs.servico_id")
      .leftJoin("cdma_equipamentos as ce", "ce.id", "cs.cdma_equipamento_id")
      .leftJoin("cdma_modelos as cm", "ce.IDModelo", "cm.IDModelo")
      .leftJoin("cdma_fabricantes as cf", "ce.Fabricante", "cf.IDFabricante")
      .leftJoin("directores as di", "di.filial_id", "ce.AgenciaFilialID")
      .leftJoin("filials as fi", "di.filial_id", "fi.id")
      .where("se.id", servicos.idServico).first()
    //.where("se.chaveServico", servicos.chaveServico).first()

    servicosCobre = await Database.select(Database.raw('DATE_FORMAT(se.updated_at, "%d/%m/%Y") as dataEmissao'), 'fi.id as cdmaFiliaiD', 'se.ChaveServico as ChaveServico', 'co.tipoFacturacao as tipoFacturacao', 'fi.nome as filialNome', 'di.cargo as directorCargo', 'di.nome as directorNome', 'se.conta_id as conta_id', 'ta.descricao as tarifario', 'ta.tecnologia as tecnologia')
      .table('servicos as se')
      .leftJoin("tarifarios as ta", "ta.id", "se.tarifario_id")
      .leftJoin("users as use", "use.id", "se.user_id")
      .leftJoin("contas as co", "co.id", "se.conta_id")
      .leftJoin("lojas as lo", "lo.id", "use.loja_id")
      .leftJoin("filials as fi", "fi.id", "lo.filial_id")
      .leftJoin("directores as di", "di.filial_id", "fi.id")
      .where("se.id", servicos.idServico).first()

    //console.log(servicosCdma)

    cliente = await Database.select('pro.nome as clienteProvincia', 'cli.nome as clienteNome', 'cli.telefone as clienteTelefone', 'cli.morada as clienteMorada', 'cli.contribuente  as clieteContribuente', 'td.tipoIdentificacao as tipoIdentificacao')
      .table('clientes as cli')
      .innerJoin("contas as co", "co.cliente_id", "cli.id")
      .leftJoin("tipo_identidades as td", "td.id", "cli.tipo_identidade_id")
      .leftJoin("provincias as pro", "pro.id", "cli.province")
      .where("co.id", servicos.conta_id).first()

    servicosWimax = await Database.select(Database.raw('DATE_FORMAT(se.updated_at, "%d/%m/%Y") as dataEmissao'), 'fi.nome as filialNome', 'di.cargo as directorCargo', 'di.nome as directorNome', 'wi.EqNbrSerie as wimaxSerie', 'wi.ServicoID as wimaxServicoID', 'wi.PasswordDados as PasswordDados', 'wi.MainKey as wimaxPassword', 'wi.MainKey as wimaxTelefone', 'se.adsl_username as adsl_username', 'se.adsl_password as adsl_password', 'se.conta_id as conta_id', 'se.ChaveServico as chaveServico', 'ta.descricao as tarifario', 'ta.tecnologia as tecnologia')
      .table('servicos as se')
      .innerJoin("tarifarios as ta", "ta.id", "se.tarifario_id")
      .innerJoin("wimaxes as wi", "wi.ServicoID", "se.id")
      .leftJoin("directores as di", "di.filial_id", "wi.AgenciaFilialID ")
      .leftJoin("filials as fi", "di.filial_id", "fi.id")
      .where("se.id", params.id).first()

    servicosLTE = await Database.select(Database.raw('DATE_FORMAT(servicos.created_at, "%d/%m/%Y") as dataEmissao'), Database.raw('DATE_FORMAT(servicos.created_at, "%d-%m-%Y") as created_at'), 'tarifarios.condicoes as debito', 'tecnologias.tipoFacturacao as tipoServico', 'filials.nome as filialNome', 'servicos.chaveServico as numero')
      .table('servicos')
      .leftJoin("sim_cards", "sim_cards.id", "servicos.sim_card_id")
      .leftJoin("lte_numeros", "lte_numeros.numero", "servicos.chaveServico")
      .leftJoin("filials", "lte_numeros.filial_id", "filials.id")
      .leftJoin("tarifarios", "tarifarios.id", "servicos.tarifario_id")
      .leftJoin("tecnologias", "tecnologias.id", "tarifarios.tecnologia_id")
      .where("servicos.id", params.id).first()

    servicosCobreADSL = await Database.select(Database.raw('DATE_FORMAT(se.updated_at, "%d/%m/%Y") as dataEmissao'), 'fi.nome as filialNome', 'di.cargo as directorCargo', 'di.nome as directorNome', 'se.adsl_username as adsl_username', 'se.adsl_password as adsl_password', 'se.conta_id as conta_id', 'se.ChaveServico as chaveServico', 'ta.descricao as tarifario', 'ta.tecnologia as tecnologia')
      .table('servicos as se')
      .leftJoin("tarifarios as ta", "ta.id", "se.tarifario_id")
      .leftJoin("users as use", "use.id", "se.user_id")
      .leftJoin("contas as co", "co.id", "se.conta_id")
      .leftJoin("lojas as lo", "lo.id", "use.loja_id")
      .leftJoin("filials as fi", "fi.id", "lo.filial_id")
      .leftJoin("directores as di", "di.filial_id", "fi.id")
      .where("se.id", servicos.idServico).first()
    //.where("se.chaveServico", params.id).first()

    let data = {
      servicos: servicos,
      servicosCdma: servicosCdma,
      servicosWimax: servicosWimax,
      cliente: cliente,
      servicosCobre: servicosCobre,
      servicosCobreADSL: servicosCobreADSL,
      servicosLTE: servicosLTE

    };

    //console.log(data)


    return DataResponse.response("success", 200, "", data);
  }

  async servicoPedidoCreate({ request, auth }) {
    const { estado_pedido_id, tipoFacturacao, conta_id, tarifario_id, adsl_username, chaveServico, adsl_password, agencia_id, par_cabo, par_caixa, par_adsl, armario_primario, armario_secundario, estado_pedido, observacao_pedido, cabo_id, caixa, central, armario, tipo_pedido } = request.all()
    const data = request.only(['estado_pedido', 'estado_pedido_id', 'observacao_pedido', 'pedido_id']);
    const log = request.only(['estado_pedido', 'estado_pedido_id', 'estado_actual_pedido', 'pedido_id']);
    const pedidoService = request.only(['servico', 'servico_class', 'endereco_ponto_b',
      'servico_omg', 'nivel_servico', 'descricao_servico', 'endereco_ponto_a',
      'debito_binario', 'interface_dte', 'redundancia', 'cpe_sede_a',
      'cpe_sede_b', 'latitude_ponto_a', 'longitude_ponto_a', 'latitude_ponto_b', 'longitude_ponto_b']);

    //console.log(pedidoService)

    if (estado_pedido == 'RJ') {

      const estadoPedido = await Database
        .select('id')
        .table('estado_pedidos')
        //.where('sigla', estado_pedido)
        .where('id', estado_pedido_id).first()

      const pedido = await Database
        .table('pedidos')
        .where('id', data.pedido_id)
        .update({ 'estado': estadoPedido.id, 'observacao': observacao_pedido })

      await LogEstadoPedido.create({ 'pedido_id': data.pedido_id, 'id_estado_anterior': log.estado_actual_pedido, 'user_id': auth.user.id, 'id_estado_novo': estadoPedido.id })

      return DataResponse.response("success", 200, "Estado actualizados com sucesso", pedido);

    } else if (conta_id == null && estado_pedido == 'FN') {

      const {
        contaDescricao,
        cliente_id
      } = request.all();


      //console.log(data.pedido_id)

      const loja = await Database
        .select('lojas.id as idLoja')
        .table('pedidos')
        .leftJoin('users', 'pedidos.user_id', 'users.id')
        .leftJoin('lojas', 'users.loja_id', 'lojas.id')
        .where('pedidos.id', data.pedido_id).first()

      const conta = await Conta.create({
        contaDescricao: contaDescricao,
        cliente_id: cliente_id,
        tipoFacturacao: tipoFacturacao,
        agencia_id: loja.idLoja,
        estado: 1,
        tipoServico: 10
      });

      const servico = await Servico.create({
        chaveServico: chaveServico,
        conta_id: conta.id,
        tarifario_id: tarifario_id,
        estado: 1,
        adsl_username: adsl_username,
        adsl_password: adsl_password,
        user_id: auth.user.id
      });

      const estadoPedido = await Database
        .select('id')
        .table('estado_pedidos')
        .where('id', data.estado_pedido_id).first()

      await Database
        .table('pedidos')
        .where('id', data.pedido_id)
        .update({
          'estado': estadoPedido.id,
          'caixa': caixa,
          'armario': armario,
          'cabo': cabo_id,
          'central': central,
          'par_cabo': par_cabo,
          'par_caixa': par_caixa,
          'par_adsl': par_adsl,
          'armario_primario': armario_primario,
          'armario_secundario': armario_secundario,
          'observacao': observacao_pedido

        })



      if (tipo_pedido == "Circuito Empresarial") {

        await PedidoServico.create({
          pedido_id: data.pedido_id,
          servico_id: servico.id,
          servico: pedidoService.servico,
          servico_class: pedidoService.servico_class,
          servico_omg: pedidoService.servico_omg,
          nivel_servico: pedidoService.nivel_servico,
          debito_binario: pedidoService.debito_binario,
          interface_dte: pedidoService.interface_dte,
          redundancia: pedidoService.redundancia,
          cpe_sede_a: pedidoService.cpe_sede_a,
          cpe_sede_b: pedidoService.cpe_sede_b,
          endereco_ponto_a: pedidoService.endereco_ponto_a,
          endereco_ponto_b: pedidoService.endereco_ponto_b,
          latitude_ponto_a: pedidoService.latitude_ponto_a,
          longitude_ponto_a: pedidoService.longitude_ponto_a,
          latitude_ponto_b: pedidoService.latitude_ponto_b,
          longitude_ponto_b: pedidoService.longitude_ponto_b,
          descricao_servico: pedidoService.descricao_servico
        });

      }

      if (estado_pedido == "FN" && tipo_pedido == "Cobre+ADSL") {

        await PedidoServico.create({
          pedido_id: data.pedido_id,
          servico_id: servico.id
        });

        const provisionHuaweiAaas = await Database.select("huaweiTarifario")
          .from("provision_huawei_aaas")
          .where("tarifario_id", tarifario_id)
          .first();

        const provisionIns = await Database.select(
          "profile_HuaweiIN",
          "credmensalindefault"
        )
          .from("provision_ins")
          .where("id", tarifario_id)
          .first();

        const ID = servico.id;
        const ChaveServico = chaveServico;
        const ADSL_USERNAME = adsl_username;
        const ADSL_PASSWORD = adsl_password;
        const HuaweiAAAProfileID = provisionHuaweiAaas.huaweiTarifario;
        const PROFILE_HuaweiIN = provisionIns.profile_HuaweiIN;
        const CREDMENSALINDEFAULT = provisionIns.credmensalindefault;

        var str = ID + ";" + ChaveServico + ";" + ADSL_USERNAME + ";" + ADSL_PASSWORD + ";" + HuaweiAAAProfileID + ";" + PROFILE_HuaweiIN + ";" + CREDMENSALINDEFAULT;
        //console.log(str)
        Prevision.create_account_adsl(str);
      }

      await LogEstadoPedido.create({ 'pedido_id': data.pedido_id, 'id_estado_anterior': log.estado_actual_pedido, 'user_id': auth.user.id, 'id_estado_novo': estadoPedido.id })


      return DataResponse.response("success", 200, "Registo efectuado com sucesso", servico);

    } else if (conta_id != null && estado_pedido == 'FN') {

      const servico = await Servico.create({
        chaveServico: chaveServico,
        conta_id: conta_id,
        tarifario_id: tarifario_id,
        estado: 1,
        adsl_username: adsl_username,
        adsl_password: adsl_password,
        user_id: auth.user.id
      });

      const estadoPedido = await Database
        .select('id')
        .table('estado_pedidos')
        .where('id', data.estado_pedido_id).first()

      await Database
        .table('pedidos')
        .where('id', data.pedido_id)
        .update({
          'estado': estadoPedido.id,
          'caixa': caixa,
          'armario': armario,
          'cabo': cabo_id,
          'central': central,
          'par_cabo': par_cabo,
          'par_caixa': par_caixa,
          'par_adsl': par_adsl,
          'armario_primario': armario_primario,
          'armario_secundario': armario_secundario,
          'observacao': observacao_pedido

        })



      if (tipo_pedido == "Circuito Empresarial") {

        await PedidoServico.create({
          pedido_id: data.pedido_id,
          servico_id: servico.id,
          servico: pedidoService.servico,
          servico_class: pedidoService.servico_class,
          servico_omg: pedidoService.servico_omg,
          nivel_servico: pedidoService.nivel_servico,
          debito_binario: pedidoService.debito_binario,
          interface_dte: pedidoService.interface_dte,
          redundancia: pedidoService.redundancia,
          cpe_sede_a: pedidoService.cpe_sede_a,
          cpe_sede_b: pedidoService.cpe_sede_b,
          endereco_ponto_a: pedidoService.endereco_ponto_a,
          endereco_ponto_b: pedidoService.endereco_ponto_b,
          latitude_ponto_a: pedidoService.latitude_ponto_a,
          longitude_ponto_a: pedidoService.longitude_ponto_a,
          latitude_ponto_b: pedidoService.latitude_ponto_b,
          longitude_ponto_b: pedidoService.longitude_ponto_b,
          descricao_servico: pedidoService.descricao_servico
        });

      }


      if (estado_pedido == "FN" && tipo_pedido == "Cobre+ADSL") {

        await PedidoServico.create({
          pedido_id: data.pedido_id,
          servico_id: servico.id
        });

        const provisionHuaweiAaas = await Database.select("huaweiTarifario")
          .from("provision_huawei_aaas")
          .where("tarifario_id", tarifario_id)
          .first();

        const provisionIns = await Database.select(
          "profile_HuaweiIN",
          "credmensalindefault"
        )
          .from("provision_ins")
          .where("id", tarifario_id)
          .first();

        const ID = servico.id;
        const ChaveServico = chaveServico;
        const ADSL_USERNAME = adsl_username;
        const ADSL_PASSWORD = adsl_password;
        const HuaweiAAAProfileID = provisionHuaweiAaas.huaweiTarifario;
        const PROFILE_HuaweiIN = provisionIns.profile_HuaweiIN;
        const CREDMENSALINDEFAULT = provisionIns.credmensalindefault;

        var str =
          ID +
          ";" +
          ChaveServico +
          ";" +
          ADSL_USERNAME +
          ";" +
          ADSL_PASSWORD +
          ";" +
          HuaweiAAAProfileID +
          ";" +
          PROFILE_HuaweiIN +
          ";" +
          CREDMENSALINDEFAULT;
        //console.log(str)
        Prevision.create_account_adsl(str);
      }

      await LogEstadoPedido.create({ 'pedido_id': data.pedido_id, 'id_estado_anterior': log.estado_actual_pedido, 'user_id': auth.user.id, 'id_estado_novo': estadoPedido.id })


      return DataResponse.response("success", 200, "Registo efectuado com sucesso", servico);
    }

  }

  async storeServicoPospago({ request, auth }) {
    const { valor, moeda_id, imposto_id, capacidade, destino, origem, nome, descricao_operacao, observacao, dataContrato, conta_id, estado, ligacao_cacti, tarifario_id, tecnologia } = request.all();

    const servico = await Servico.create({
      chaveServico: nome,
      descricao_operacao: descricao_operacao,
      conta_id: conta_id,
      estado: estado,
      dataEstado: dataContrato,
      ligacao_cacti: ligacao_cacti,
      observacao: observacao,
      tarifario_id: tarifario_id,
      tecnologia_id: tecnologia,
      user_id: auth.user.id
    });

    const flatRateServico = await FlatRateServico.create({
      servico_id: servico.id,
      valor: valor,
      moeda_id: moeda_id,
      imposto_id: imposto_id,
      origem: origem,
      destino: destino,
      capacidade: capacidade,
      user_id: auth.user.id
    });


    return DataResponse.response("success", 200, "Registo efectuado com sucesso.", servico);
  }

  async mudarTarifarioServico({ request, auth }) {
    const { tarifario_id_novo, servico_id } = request.all();
    const tarifario = await Database.select('*').table('tarifarios as t').where("t.id", tarifario_id_novo).first();
    console.log(tarifario);
    const servico = await Servico.query().where("id", servico_id).update({
      tarifario_id: tarifario.id
    });
    return DataResponse.response("success", 200, "tarifario actualizado com sucesso", servico);
  }


}

module.exports = ServicoController
