'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const ReferenciaBancaria = use("App/Models/ReferenciaBancaria");
const Database = use("Database");
const DataResponse = use("App/Models/DataResponse");

/**
 * Resourceful controller for interacting with referenciabancarias
 */
class ReferenciaBancariaController {
  /**
   * Show a list of all referenciabancarias.
   * GET referenciabancarias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {}

  /**
   * Render a form to be used for creating a new referenciabancaria.
   * GET referenciabancarias/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new referenciabancaria.
   * POST referenciabancarias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {}

  /**
   * Display a single referenciabancaria.
   * GET referenciabancarias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing referenciabancaria.
   * GET referenciabancarias/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update referenciabancaria details.
   * PUT or PATCH referenciabancarias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a referenciabancaria with id.
   * DELETE referenciabancarias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}


  /**
   * @author caniggiamoreira@gmail.com
   * @description 'validação das referencias bancarias'
   * 
   * validation a referenciabancaria.
   * POST referenciabancarias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async validarReferenciaBancaria({ request }) {
      const {referencia, banco_id} = request.all()       
      const r = await Database.from('referencia_bancarias').where('referencia', referencia).where('banco_id', banco_id) 
      if(r.length > 0){
        return DataResponse.response("success", 500,null, null);
      }
      return DataResponse.response("success", 200,null,null);
  }
}

module.exports = ReferenciaBancariaController
