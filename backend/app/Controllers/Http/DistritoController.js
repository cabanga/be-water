'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Municipio = use("App/Models/Municipio");
const Distrito = use("App/Models/Distrito");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage');

/**
 * Resourceful controller for interacting with distritos
 */
class DistritoController {
  /**
   * Show a list of all distritos.
   * GET distritos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) { }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "distritos.id",
        "distritos.nome",
        "distritos.is_active",
        "distritos.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "distritos.user_id",
        "users.nome as user",
        "distritos.created_at"
      )
        .from("distritos")
        .innerJoin(
          "municipios",
          "municipios.id",
          "distritos.municipio_id"
        )
        .innerJoin(
          "provincias",
          "provincias.id",
          "municipios.provincia_id"
        )
        .innerJoin(
          "users",
          "users.id",
          "distritos.user_id"
        )
        .orderBy(orderBy == null ? "distritos.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "distritos.id",
        "distritos.nome",
        "distritos.is_active",
        "distritos.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "distritos.user_id",
        "users.nome as user",
        "distritos.created_at"
      )
        .from("distritos")
        .innerJoin(
          "municipios",
          "municipios.id",
          "distritos.municipio_id"
        )
        .innerJoin(
          "provincias",
          "provincias.id",
          "municipios.provincia_id"
        )
        .innerJoin(
          "users",
          "users.id",
          "distritos.user_id"
        )
        .where("distritos.nome", "like", "%" + search + "%")
        .orWhere("provincias.nome", "like", "%" + search + "%")
        .orWhere("municipios.nome", "like", "%" + search + "%")
        .orWhere("users.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(distritos.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "distritos.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async getDistritoById({ params, request }) {

    const {
      has_distrito,
      municipio_id
    } = request.all();

    //console.log(request.all());

    let res = null;

    if (has_distrito) {

      res = await Database.select(
        "distritos.id",
        "distritos.nome",
        "distritos.is_active",
        "distritos.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "distritos.user_id",
        "users.nome as user",
        "distritos.created_at"
      )
        .from("distritos")
        .innerJoin("municipios", "municipios.id", "distritos.municipio_id")
        .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
        .innerJoin("users", "users.id", "distritos.user_id")
        .where("distritos.id", params.id)
        .first();
    }
    else {

      res = await Database.select(
        "municipios.id as municipio_id",
        "municipios.nome as municipio",
        "municipios.is_active",
        "municipios.has_distrito",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "municipios.user_id",
        "users.nome as user"
      )
        .from("municipios")
        .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
        .innerJoin("users", "users.id", "municipios.user_id")
        .where("municipios.id", municipio_id)
        .first();

    }

    //console.log(res);

    return res;
  }

  async getDistritosByMunicipio({ params }) {

    let res = null;

    res = await Database.select(
      "distritos.id",
      "distritos.nome",
      "distritos.is_active",
      "distritos.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "distritos.user_id",
      "users.nome as user",
      "distritos.created_at"
    )
      .from("distritos")
      .innerJoin("municipios", "municipios.id", "distritos.municipio_id")
      .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
      .innerJoin("users", "users.id", "distritos.user_id")
      .where("distritos.municipio_id", params.id)
      .orderBy("distritos.nome", "ASC");

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new distrito.
   * GET distritos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) { }

  /**
   * Create/save a new distrito.
   * POST distritos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
    const {
      nome,
      municipio_id,
      user_id
    } = request.all();

    const res = await Distrito.query()
      .where("nome", nome)
      .where("municipio_id", municipio_id)
      .getCount();

    if (res > 0) {
      return DataResponse.response(
        null,
        500,
        "Distrito " + nome + " já existe",
        null
      );
    } else {
      await Distrito.create({
        nome: nome,
        municipio_id: municipio_id,
        is_active: true,
        user_id: user_id
      });

      const municipio = await Municipio.find(municipio_id);

      municipio.merge({
        has_distrito: true
      });
      await municipio.save();

      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
    }

  }

  /**
   * Display a single distrito.
   * GET distritos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const distrito = await Distrito.find(params.id);
    return DataResponse.response( "success", 200, "", distrito)
  }

  /**
   * Render a form to update an existing distrito.
   * GET distritos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) { }

  /**
   * Update distrito details.
   * PUT or PATCH distritos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {

    const {
      nome,
      municipio_id,
      has_distrito,
      is_active,
      user_id
    } = request.all();

    console.log(request.all());

    const distrito = await Distrito.find(params.id);

    distrito.merge({
      nome: nome,
      municipio_id: municipio_id,
      has_distrito: has_distrito,
      is_active: is_active,
      user_id: user_id
    });
    await distrito.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );

  }

  /**
   * Delete a distrito with id.
   * DELETE distritos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) { }

  async selectBox({ params }) {
    const distrito = await Distrito.query().where("id", params.id).fetch();

    return DataResponse.response("success", 200, "", distrito);
  }

  async selectBoxByMunicipio({ params }) {

    let res = await Database.select(
      "distritos.id",
      "distritos.nome",
      "distritos.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia"
    )
      .from("distritos")
      .innerJoin(
        "municipios",
        "municipios.id",
        "distritos.municipio_id"
      )
      .innerJoin(
        "provincias",
        "provincias.id",
        "municipios.provincia_id"
      )
      .where("distritos.municipio_id", params.id)
      .where("distritos.is_active", true)
      .orderBy("distritos.nome", "ASC");

    return res;
  }

}

module.exports = DistritoController
