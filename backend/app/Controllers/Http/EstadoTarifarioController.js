'use strict'

const DataResponse = use("App/Models/DataResponse");
const EstadoTarifario = use("App/Models/EstadoTarifario");
const Database = use("Database");
var moment = require("moment");

class EstadoTarifarioController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("estado_tarifarios")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("estado_tarifarios")
        .orWhere("estado_tarifarios.descricao", "like", "%" + search + "%")
        .orWhere("estado_tarifarios.slug", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('estado_tarifarios')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Estado Tarifário com este Slug",
        Verify
      );

    } else {
      const EstadoTarifario = await Database.table('estado_tarifarios').insert({
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        EstadoTarifario
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao", "slug"]);

    const Verify = await EstadoTarifario.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Estado Tarifário com este Slug",
        Verify
      );
    } else {

      // update with new data entered
      const estado_tarifarios = await EstadoTarifario.find(params.id);
      estado_tarifarios.merge(data);
      await estado_tarifarios.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        estado_tarifarios
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'descricao', 'slug')
      .from('estado_tarifarios')
      .orderBy('descricao', 'ASC');

    return res;
  }
}

module.exports = EstadoTarifarioController
