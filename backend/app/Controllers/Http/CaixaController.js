"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Caixa = use("App/Models/Caixa");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with caixas
 */
class CaixaController {
  /**
   * Show a list of all caixas.
   * GET caixas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, auth }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;
    if ((search == null) && (auth.user.loja_id == null)) {
      res = await Database.select(
        "caixas.user_id",
        Database.raw('DATE_FORMAT(caixas.data_abertura, "%Y-%m-%d") as abertura'),
        "caixas.valor_abertura",
        "caixas.user_id",
        "caixas.data_abertura",
        "caixas.hora_abertura",
        "caixas.valor_fecho",
        "caixas.valor_fecho",
        "caixas.data_fecho",
        "caixas.valor_vendas",
        "caixas.valor_deposito",
        "caixas.data_deposito",
        "caixas.referencia_banco",
        "caixas.is_active",
        "caixas.id",
        "bancos.nome as banco",
        "bancos.numero_conta",
        "bancos.abreviatura",
        "bancos.iban",
        "users.nome as operador",
        "lojas.nome as loja"
      )
        .from("caixas")
        .leftJoin("bancos", "bancos.id", "caixas.banco_id")
        .leftJoin("lojas", "lojas.id", "caixas.loja_id")
        .leftJoin("users", "users.id", "caixas.user_id")
        .orderBy(orderBy == null ? "caixas.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else if((search != null) && (auth.user.loja_id == null)){
      res = await Database.select(
        "caixas.user_id",
        Database.raw('DATE_FORMAT(caixas.data_abertura, "%Y-%m-%d") as abertura'),
        "caixas.valor_abertura",
        "caixas.data_abertura",
        "caixas.hora_abertura",
        "caixas.valor_fecho",
        "caixas.valor_fecho",
        "caixas.data_fecho",
        "caixas.valor_vendas",
        "caixas.valor_deposito",
        "caixas.data_deposito",
        "caixas.referencia_banco",
        "caixas.is_active",
        "caixas.id",
        "bancos.numero_conta",
        "bancos.abreviatura",
        "bancos.iban",
        "users.nome as operador",
        "lojas.nome as loja"
      )
        .from("caixas")
        .leftJoin("bancos", "bancos.id", "caixas.banco_id")
        .leftJoin("lojas", "lojas.id", "caixas.loja_id")
        .leftJoin("users", "users.id", "caixas.user_id")
        .orWhere("users.nome", "like", "%" + search + "%")
        .orWhere("lojas.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(caixas.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "caixas.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }else  if ((search == null) && (auth.user.loja_id != null)) {
      res = await Database.select(
        "caixas.user_id",
        Database.raw('DATE_FORMAT(caixas.data_abertura, "%Y-%m-%d") as abertura'),
        "caixas.valor_abertura",
        "caixas.data_abertura",
        "caixas.hora_abertura",
        "caixas.valor_fecho",
        "caixas.valor_fecho",
        "caixas.data_fecho",
        "caixas.valor_vendas",
        "caixas.valor_deposito",
        "caixas.data_deposito",
        "caixas.referencia_banco",
        "caixas.is_active",
        "caixas.id",
        "bancos.nome as banco",
        "bancos.numero_conta",
        "bancos.abreviatura",
        "bancos.iban",
        "users.nome as operador",
        "lojas.nome as loja"
      )
        .from("caixas")
        .leftJoin("bancos", "bancos.id", "caixas.banco_id")
        .leftJoin("lojas", "lojas.id", "caixas.loja_id")
        .leftJoin("users", "users.id", "caixas.user_id")
        .where("caixas.loja_id", auth.user.loja_id)
        .orderBy(orderBy == null ? "caixas.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else if((search != null) && (auth.user.loja_id != null)){
      res = await Database.select(
        "caixas.user_id",
        Database.raw('DATE_FORMAT(caixas.data_abertura, "%Y-%m-%d") as abertura'),
        "caixas.valor_abertura",
        "caixas.data_abertura",
        "caixas.hora_abertura",
        "caixas.valor_fecho",
        "caixas.valor_fecho",
        "caixas.data_fecho",
        "caixas.valor_vendas",
        "caixas.valor_deposito",
        "caixas.data_deposito",
        "caixas.referencia_banco",
        "caixas.is_active",
        "caixas.id",
        "bancos.numero_conta",
        "bancos.abreviatura",
        "bancos.iban",
        "users.nome as operador",
        "lojas.nome as loja"
      )
        .from("caixas")
        .leftJoin("bancos", "bancos.id", "caixas.banco_id")
        .leftJoin("lojas", "lojas.id", "caixas.loja_id")
        .leftJoin("users", "users.id", "caixas.user_id")
        .where("caixas.loja_id", auth.user.loja_id)
        .orWhere("users.nome", "like", "%" + search + "%")
        .orWhere("lojas.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(caixas.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "caixas.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 2000, "", res);
  }

  /**
   * Render a form to be used for creating a new caixa.
   * GET caixas/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new caixa.
   * POST caixas
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { valor_abertura } = request.all();

    var moment = require("moment");
    var dataActual = moment(new Date()).format("YYYY-MM-DD");

    if (auth.user.loja_id == null) {
        return DataResponse.response( "info", 201,"Caro operador, lamentamos informar que essa operação é exclusivamente para operador Loja.", null );
    }


    var hoje = new Date();
    var dataOntem = new Date(hoje.getTime());
    dataOntem.setDate(hoje.getDate() - 1);   
    dataOntem = moment(dataOntem).format("YYYY-MM-DD"); 
 
 
    const c = await Caixa.query().whereBetween('data_abertura', ['2019-12-10', dataOntem]).where('is_active', 1).where("user_id", auth.user.id).getCount();

    if (c > 0) {
      return DataResponse.response( "info", 201, "Caro operador, não lhe é permitido abrir o caixa n-vezes enquanto o caixa dos dias anterior não forem devidamente fechados.", null );
    }

    const caixa = await Caixa.query().where("data_abertura", dataActual).where("user_id", auth.user.id).getCount();

    if (caixa > 0) {
      return DataResponse.response( "info", 201, "Caro operador, não lhe é permitido abrir o caixa duas vezes no mesmo dia, por favor entra em contacto com administrador.", caixa );
    } else {
      var moment = require("moment");
      var data_abertura = moment(new Date()).format("YYYY-MM-DD");
      var hora_abertura = moment(new Date()).format("HH:mm:ss");

      const c = await Caixa.create({
        valor_abertura: valor_abertura,
        data_abertura: data_abertura,
        hora_abertura: hora_abertura,
        is_active: true,
        loja_id: auth.user.loja_id,
        user_id: auth.user.id
      });

      return DataResponse.response("success", 200, "Registado com sucesso", c);
    }
  }

  /**
   * Display a single caixa.
   * GET caixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing caixa.
   * GET caixas/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update caixa details.
   * PUT or PATCH caixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ request, auth }) {
    const { 
      valor_venda,
      valor_fecho,
      caixa_id,
      observacao
    } = request.all();

    if(caixa_id == null || caixa_id == 'null'){
      return DataResponse.response( "info", 201,"Caro operador, por favor informe o caixa", null );
    }

    
    if (auth.user.loja_id == null) {
        return DataResponse.response( "info", 201,"Caro operador, lamentamos informar que essa operação é exclusivamente para operador Loja.", null );
    }
    
    const caixa = await Database.select("*")
      .from("caixas")
      .where("id", caixa_id)
      .where("is_active", true)
      .where("user_id", auth.user.id)
      .first();

    if (caixa != null) {
      var moment = require("moment");

      var data_fecho = moment(new Date()).format("YYYY-MM-DD");
      var hora_fecho = moment(new Date()).format("HH:mm:ss");

      var c = await Caixa.query()
        .where("id", caixa_id)
        .where("user_id", auth.user.id)
        .where("is_active", true)
        .update({
          valor_fecho: valor_fecho,
          data_fecho: data_fecho,
          hora_fecho: hora_fecho,
          valor_vendas: valor_venda,
          observacao:observacao, 
          is_active: false, 
          loja_id: auth.user.loja_id,
          user_id: auth.user.id
        }); 

        const cc = await Database.select("caixas.id","caixas.user_id",Database.raw('DATE_FORMAT(caixas.data_abertura, "%Y-%m-%d") as data_abertura'))
        .from("caixas").where("caixas.id", caixa_id).first();

      return DataResponse.response("success", 200, "Registado com sucesso", cc);
    } else {
      return DataResponse.response(
        "info",
        500,
        "Não tem caixa aberto, por favor faça abertura do caixa",
        null
      );
    }
  }

   /**
   * Update caixa details.
   * PUT or PATCH caixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async updateDepositoCaixa({ request, auth }) {
    
  }

  async totalVendas({ request, auth }) { 
    const { caixa_id } = request.all(); 

    if(caixa_id == null || caixa_id == 'null'){return DataResponse.response("info", 404,"", null)}

    const caixa  = await Database.select('id as caixa_id','user_id',Database.raw('DATE_FORMAT(data_abertura, "%Y-%m-%d") as data_abertura'))
    .from('caixas').where('id', caixa_id).where('loja_id',auth.user.loja_id).where('user_id',auth.user.id).first()

    const factura = await Database.table("facturas")
    .innerJoin("pagamentos", "pagamentos.id", "facturas.pagamento_id")
    .where("facturas.status", "N")
    .where("facturas.pago", 1).where('facturas.user_id', caixa.user_id)
    .where("facturas.caixa_id", caixa.caixa_id)
    .select(Database.raw("SUM(facturas.total) as total"),Database.raw("SUM(pagamentos.troco) as troco"))
   
     
    const recibo = await Database.table("recibos")
    .leftJoin("pagamentos", "pagamentos.id", "recibos.pagamento_id")
    .where("recibos.status", "N")
    .where('recibos.user_id', caixa.user_id)
    .where(Database.raw('DATE_FORMAT(pagamentos.created_at, "%Y-%m-%d")'), caixa.data_abertura)    
    .select(Database.raw("SUM(recibos.total) as total"),Database.raw("SUM(pagamentos.troco) as troco"))
 
    const recebimentos ={ 
      factura:{total:(factura[0].total==null? 0: factura[0].total), troco:(factura[0].troco==null? 0: factura[0].troco)},
      recibo:{total:(recibo[0].total==null? 0: recibo[0].total), troco:(recibo[0].troco==null? 0: recibo[0].troco)} ,
      total: (factura[0].total==null? 0: factura[0].total) + (recibo[0].total==null? 0: recibo[0].total)     
    }

    return DataResponse.response("info", 200, "", recebimentos);
  }

  async getCaixaAberto({ auth }) {
    const caixa = await Database.from("caixas")
      .where("user_id", auth.user.id)
      .where("is_active", true);
    return DataResponse.response("info", 5000, null, caixa);
  }

  async selectBox({ auth }) {

    if (auth.user.loja_id !=null) {
    const caixa = await Database.select(
    "caixas.id",
    Database.raw('DATE_FORMAT(caixas.data_abertura, "%Y-%m-%d") as data_abertura')).from("caixas")
    .where("caixas.is_active", true)
    .where("caixas.user_id", auth.user.id); 
    return DataResponse.response("info", 2000, null, caixa); 
    }else{ 
        return DataResponse.response( "info", 201,"Caro operador, lamentamos informar que essa operação é exclusivamente para operador Loja.", [] );
    }


  }

  async selectBoxCaixasFechados({ params, auth }) {

    if (auth.user.loja_id != null) {
      const caixa = await Database
      .select("caixas.id","caixas.data_abertura","caixas.data_fecho","caixas.valor_fecho","caixas.valor_vendas", "caixas.valor_abertura","users.nome").from("caixas") 
        .leftJoin("users", "users.id", "caixas.user_id")
        .where("caixas.is_active", false) 
        .whereNull('caixas.deposito_id')  
        .where('data_abertura', params.data)
        .where("caixas.loja_id", auth.user.loja_id).orderBy('users.nome')

      return DataResponse.response("info", 2000, null, caixa);
    }
  }
 
  async getDiarioVendas({ request }) {

    const { user, data1, data2, caixa_id } =  request.all()
   
    let data = await Caixa.getDiarioVendas(user, data1, data2, caixa_id);
    
    return DataResponse.response("success", 200, "", data);
  }

  async reportResumoRecebimentoPorCaixa({ request, auth }){
      const { data ,user_id,  byUser} =  request.all() ;
      const user = user_id||auth.user.id;
     // reportResumoRecebimentoPorCaixa(loja, data, user_id ,byUser = 0)
      let dados = await Caixa.reportResumoRecebimentoPorCaixa(auth.user.loja_id, data, user, byUser);
      return DataResponse.response("success", 200, "", dados);
  }

  async fechoCaixaResumoRecebimentoVendas({ request, auth }){
      const { caixa_id } = request.all(); 
      if(caixa_id == null || caixa_id == 'null'){ return DataResponse.response("success", 500, "", null) }
 
      const d =  await Database.select('caixas.id','caixas.data_abertura').from('users').innerJoin('caixas', 'caixas.user_id', 'users.id').where('caixas.id', caixa_id).where('caixas.loja_id',auth.user.loja_id).where('users.id',auth.user.id).first()
      let dados =  await Caixa.fechoCaixaResumoRecebimentoVendas(auth.user.loja_id, d.data_abertura, auth.user.id);

      return DataResponse.response("success", 200, "", dados);
  }

  /**
   * Delete a caixa with id.
   * DELETE caixas/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = CaixaController;
