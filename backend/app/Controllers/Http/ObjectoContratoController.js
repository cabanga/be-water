'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const ObjectoContrato = use("App/Models/ObjectoContrato");
const Database = use("Database");
var moment = require("moment");

/**
 * Resourceful controller for interacting with objectocontratoes
 */
class ObjectoContratoController {
 
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("objecto_contratoes")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("objecto_contratoes")
        .orWhere("slug", "like", "%" + search + "%")
        .orWhere("nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { nome, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('objecto_contratoes')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Objecto de Contrato com este slug",
        Verify
      );

    } else {
      const objectocontrato = await Database.table('objecto_contratoes').insert({
        slug: slug,
        nome: nome,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        objectocontrato
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["nome", "slug"]);

    const Verify = await ObjectoContrato.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Objecto de Contrato com este slug",
        Verify
      );
    } else {

      // update with new data entered
      const objectocontrato = await ObjectoContrato.find(params.id);
      objectocontrato.merge(data);
      await objectocontrato.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        objectocontrato
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'nome', 'slug')
      .from('objecto_contratoes')
      .orderBy('nome', 'ASC');

    return res;
  }
  
  async selectBox ({ request }) {  
    const res = await Database.select('id', 'nome', 'slug')
    .from('objecto_contratoes')
    .orderBy('nome', 'ASC');
     
    return res;
  }

}

module.exports = ObjectoContratoController
