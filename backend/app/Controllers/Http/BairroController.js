'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const Bairro = use("App/Models/Bairro");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage');
/**
 * Resourceful controller for interacting with bairros
 */
class BairroController {
  /**
   * Show a list of all bairros.
   * GET bairros
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "bairros.id",
        "bairros.nome",
        "bairros.has_quarteirao",
        "bairros.is_active",
        "bairros.distrito_id",
        "distritos.nome as distrito",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "bairros.user_id",
        "users.nome as user",
        "bairros.created_at"
      )
        .from("bairros")
        .leftJoin(
          "distritos",
          "distritos.id",
          "bairros.distrito_id"
        )
        .leftJoin(
          "municipios",
          "municipios.id",
          "bairros.municipio_id"
        )
        .leftJoin(
          "provincias",
          "provincias.id",
          "municipios.provincia_id"
        )
        .innerJoin(
          "users",
          "users.id",
          "bairros.user_id"
        )
        .orderBy(orderBy == null ? "bairros.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "bairros.id",
        "bairros.nome",
        "bairros.has_quarteirao",
        "bairros.is_active",
        "bairros.distrito_id",
        "distritos.nome as distrito",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "bairros.user_id",
        "users.nome as user",
        "bairros.created_at"
      )
        .from("bairros")
        .leftJoin(
          "distritos",
          "distritos.id",
          "bairros.distrito_id"
        )
        .leftJoin(
          "municipios",
          "municipios.id",
          "bairros.municipio_id"
        )
        .leftJoin(
          "provincias",
          "provincias.id",
          "municipios.provincia_id"
        )
        .innerJoin(
          "users",
          "users.id",
          "bairros.user_id"
        )
        .where("bairros.nome", "like", "%" + search + "%")
        .orWhere("provincias.nome", "like", "%" + search + "%")
        .orWhere("municipios.nome", "like", "%" + search + "%")
        .orWhere("distritos.nome", "like", "%" + search + "%")
        .orWhere("users.nome", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(bairros.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "bairros.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }


  async getBairroById({ params }) {
    let res = null;

    res = await Database.select(
      "bairros.id",
      "bairros.nome",
      "bairros.has_quarteirao",
      "bairros.is_active",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "municipios.has_distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "bairros.user_id",
      "users.nome as user",
      "bairros.created_at"
    )
      .from("bairros")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
      .innerJoin("users", "users.id", "bairros.user_id")
      .where("bairros.id", params.id)
      .first();

    return DataResponse.response("success", 200, "", res);
  }


  async getBairrosByMunicipio({ params }) {
    let res = null;

    res = await Database.select(
      "bairros.id",
      "bairros.nome",
      "bairros.has_quarteirao",
      "bairros.is_active",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "municipios.has_distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "bairros.user_id",
      "users.nome as user",
      "bairros.created_at"
    )
      .from("bairros")
      .leftJoin(
        "distritos",
        "distritos.id",
        "bairros.distrito_id"
      )
      .leftJoin(
        "municipios",
        "municipios.id",
        "bairros.municipio_id"
      )
      .leftJoin(
        "provincias",
        "provincias.id",
        "municipios.provincia_id"
      )
      .innerJoin(
        "users",
        "users.id",
        "bairros.user_id"
      )
      .where("bairros.municipio_id", params.id)

    return DataResponse.response("success", 200, "", res);
  }



  /**
   * Render a form to be used for creating a new bairro.
   * GET bairros/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new bairro.
   * POST bairros
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {

    const {
      nome,
      distrito_id,
      municipio_id,
      user_id
    } = request.all();

    //console.log("request");
    //console.log(request.all());


    const res = await Bairro.query()
      .where("nome", nome)
      .where("municipio_id", municipio_id)
      .getCount();



    console.log(res);
    if (res > 0) {
      return DataResponse.response(
        null,
        500,
        "Este bairro já existe.",
        null
      );
    } else {
      await Bairro.create({
        nome: nome,
        distrito_id: distrito_id,
        municipio_id: municipio_id,
        is_active: true,
        user_id: user_id
      });
      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
    }

  }

  /**
   * Display a single bairro.
   * GET bairros/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing bairro.
   * GET bairros/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update bairro details.
   * PUT or PATCH bairros/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {

    const {
      nome,
      distrito_id,
      is_active,
      user_id
    } = request.all();

    const bairro = await Bairro.find(params.id);

    if (bairro != null) {

      bairro.merge({
        nome: nome,
        distrito_id: distrito_id,
        is_active: is_active,
        user_id: user_id
      });
      await bairro.save();

    }
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );

  }

  /**
   * Delete a bairro with id.
   * DELETE bairros/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }


  async selectBoxByMunicipio({ params }) {

    let res = await Database.select(
      "bairros.id",
      "bairros.nome",
      "bairros.has_quarteirao",
      "bairros.is_active",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "bairros.distrito_id",
      "distritos.nome as distrito"
    )
      .from("bairros")
      .leftJoin(
        "distritos",
        "distritos.id",
        "bairros.distrito_id"
      )
      .leftJoin(
        "municipios",
        "municipios.id",
        "bairros.municipio_id"
      )
      .leftJoin(
        "provincias",
        "provincias.id",
        "municipios.provincia_id"
      )
      .where("bairros.municipio_id", params.id)
      .where("bairros.is_active", true)
      .orderBy("bairros.nome", "ASC");

    //console.log(params.id);
    //console.log(res);

    if (res.length == 0)
      return DataResponse.response(
        "success",
        404,
        "Não foram encontrados Bairros",
        null
      );

    return res;
  }


}

module.exports = BairroController
