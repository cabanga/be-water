'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const EstadoPedido = use("App/Models/EstadoPedido");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
/**
 * Resourceful controller for interacting with estadopedidos
 */
class EstadoPedidoController {
  /**
   * Show a list of all estadopedidos.
   * GET estadopedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "estado_pedidos.id",
        "estado_pedidos.designacao",
        "estado_pedidos.sigla",
        "estado_pedidos.rota_form",
        "estado_pedidos.tipo_pedido_id",
        "tipo_pedidos.descricao as tipoPedidoDesc",
        "estado_pedidos.created_at"
      )
        .from("estado_pedidos")
        .leftJoin("tipo_pedidos", "tipo_pedidos.id", "estado_pedidos.tipo_pedido_id")
        .orderBy(orderBy == null ? "estado_pedidos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "estado_pedidos.id",
        "estado_pedidos.designacao",
        "estado_pedidos.sigla",
        "estado_pedidos.rota_form",
        "estado_pedidos.tipo_pedido_id",
        "tipo_pedidos.descricao as tipoPedidoDesc",
        "estado_pedidos.created_at"
      )
        .from("estado_pedidos")
        .leftJoin("tipo_pedidos", "tipo_pedidos.id", "estado_pedidos.tipo_pedido_id")
        .where("estado_pedidos.designacao", "like", "%" + search + "%")
        .orWhere("estado_pedidos.sigla", "like", "%" + search + "%")
        .orWhere("tipo_pedidos.descricao", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(estado_pedidos.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "estado_pedidos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new estadopedido.
   * GET estadopedidos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new estadopedido.
   * POST estadopedidos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
 async store({ request, auth }) {
    const { designacao, sigla, tipo_pedido_id, rota_form} = request.all();
    const estado = await EstadoPedido.query()
      .where("designacao", designacao)
/*       .where("tipo_pedido_id", tipo_pedido_id) */
      .getCount();
    if (estado > 0) {
      return DataResponse.response(
        "success",
        500,
        "Já existe um estado com a mesma designação",
        estado
      );
    } else {
      await EstadoPedido.create({
        designacao: designacao,
        sigla: sigla,
/*         tipo_pedido_id: tipo_pedido_id,*/
        rota_form: rota_form, 
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Estado registado com sucesso",
        null
      );
    }
  }

  /**
   * Display a single estadopedido.
   * GET estadopedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing estadopedido.
   * GET estadopedidos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }


  async estadoNotAberto({ params }) {

    const estado = await Database.select(
      'id', 
      'designacao',
      'sigla', 
      'rota_form',
      'sigla'
    )
      .table('estado_pedidos')
      .whereNot('sigla', 'ABERTO')

    return DataResponse.response("success", 200, "", estado);
  }

  /**
   * Update estadopedido details.
   * PUT or PATCH estadopedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["designacao","sigla", "rota_form"]);

    //console.log(data)
/*
  const estado = await EstadoPedido.query()
    .where("designacao", data.designacao)
    //.whereNot({ id: params.id })
    .getCount();

    //console.log(params.id)
  if (estado > 0) {
    return DataResponse.response(
      "success",
      500,
      "Já existe um estado com a mesma designação",
      estado
    );
  }else{

    */

    // update with new data entered
    const estado = await EstadoPedido.find(params.id);
    estado.merge(data);
    await estado.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      estado
    );

 // }
  }

  async estadoPedidoValue({ params }) {

    const result = await Database.select('estado_pedidos.id','estado_pedidos.designacao','estado_pedidos.sigla','estado_pedidos.rota_form')
      .table('estado_pedidos')
      .where('estado_pedidos.id', params.id).first()

     // console.log(result)

    return DataResponse.response("success", 200, "", result);
  }

  /**
   * Delete a estadopedido with id.
   * DELETE estadopedidos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = EstadoPedidoController
