'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
/** @type {import('@adonisjs/framework/src/Env')} */

const Env = use('Env')
const ContextoConfiguracao = use("App/Models/ContextoConfiguracao");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");

/**
 * Resourceful controller for interacting with contexto_configuracaos
 */
class ContextoConfiguracaoController {
  /**
   * Show a list of all contexto_configuracaos.
   * GET contexto_configuracaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        'contexto_configuracaos.id',
        'contexto_configuracaos.nome',
        'contexto_configuracaos.campo',
        'contexto_configuracaos.is_delected',
        'contexto_configuracaos.created_at'
      )
        .table('contexto_configuracaos')
        .where("contexto_configuracaos.is_delected", false)

        .orderBy(orderBy == null ? 'contexto_configuracaos.nome' : orderBy, 'ASC')
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        'contexto_configuracaos.id',
        'contexto_configuracaos.nome',
        'contexto_configuracaos.campo',
        'contexto_configuracaos.is_delected',
        'contexto_configuracaos.created_at'
      )
        .table('contexto_configuracaos')

        .where("contexto_configuracaos.is_delected", false)
        .orWhere("contexto_configuracaos.nome", "like", "%" + search + "%")
        .orWhere("contexto_configuracaos.campo", "like", "%" + search + "%")
        .orWhere("users.nome", "like", "%" + search + "%")

        .orderBy(orderBy == null ? "contexto_configuracaos.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    }

    //console.log(res);

    return DataResponse.response("success", 200, "", res);
  }


  async getInformationSchema({ request }) {

    let res = null;

    res = await Database.raw("SELECT TABLE_NAME from information_schema.tables WHERE information_schema.tables.TABLE_SCHEMA = '" + Env.get('DB_DATABASE') + "'");
    //console.log(res);

    return res[0];
  }


  async getTableDescription({ params }) {

    let res = null;

    res = await Database.raw("describe " + Env.get('DB_DATABASE') + "." + params.table);

    //console.log(res);

    return res[0];
  }

  async getContextoConfiguracaobySlug({ params }) {

    //console.log(params);

    let res = await Database.select(
      'contexto_configuracaos.id',
      'contexto_configuracaos.nome',
      'contexto_configuracaos.campo',
      'contexto_configuracaos.is_delected',
      'contexto_configuracaos.created_at'
    )
      .table('contexto_configuracaos')
      .where('contexto_configuracaos.is_delected', false)
      .where('contexto_configuracaos.nome', params.nome)
      .first();

    //console.log(res);

    return DataResponse.response("success", (res != null) ? 200 : 404, "", res);

  }


  async selectBoxContextoConfiguracaos({ params }) {

    let res = await Database.select(
      'contexto_configuracaos.id',
      'contexto_configuracaos.nome',
      'contexto_configuracaos.campo',
      'contexto_configuracaos.is_delected'
    )
      .table('contexto_configuracaos')
      .where('contexto_configuracaos.is_delected', false)      
      .orderBy("contexto_configuracaos.nome", "ASC");

    //console.log(res);

    return res;

  }


  async getDataContexto({ request }) {

    const {
      table,
      column
    } = request.all();

    //console.log(request.all());

    let res = await Database.raw("SELECT id, " + column + " as contexto_valor FROM " + Env.get('DB_DATABASE') + "." + table + " ORDER BY " + column + " ASC");
    //console.log(res);

    return res[0];

  }

  async getDataContextoById({ params }) {

    const contexto = await Database.select(
      'contexto_configuracaos.id',
      'contexto_configuracaos.nome',
      'contexto_configuracaos.campo'
    )
      .table('contexto_configuracaos')
      .where("contexto_configuracaos.id", params.id)
      .first();

    //console.log(contexto);

    let res = await Database.raw("SELECT id, " + contexto.campo + " as contexto_valor FROM " + Env.get('DB_DATABASE') + "." + contexto.nome + " ORDER BY " + contexto.campo + " ASC");
    //console.log(res);

    return res[0];

  }

  async getSelectBoxModulos({ request }) {

    let res = await Database.select(
      'contexto_configuracaos.id',
      'contexto_configuracaos.campo',
      'contexto_configuracaos.campo_id',
    )
      .table('contexto_configuracaos')
      .orderBy('contexto_configuracaos.campo', 'ASC')

    return res;
  }


  /**
   * Render a form to be used for creating a new ContextoConfiguracao.
   * GET contexto_configuracaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new rotarun.
   * POST rotaruns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request }) {

    const {
      nome,
      campo,
      //user_id
    } = request.all();

    //console.log(request.all());

    const res = await ContextoConfiguracao.query()
      .where("nome", nome)
      .where("campo", campo)
      .first();

      /* console.log("query id");
      console.log(res); */

    if (res != null) {

      if(res.is_delected)
      {
        res.merge({
          nome: nome,
          campo: campo,
          is_delected: false,
          //user_id: user_id
        });
    
        await contexto_configuracao.save();
  
      }
      else {
        return DataResponse.response(null, 500, "Esta configuração já existe.", null);
      }

    } else {

      await ContextoConfiguracao.create({
        nome: nome,
        campo: campo,
        is_delected: false,
        //user_id: user_id
      });

      let listagem = await Database.select(
        'contexto_configuracaos.id',
        'contexto_configuracaos.nome',
        'contexto_configuracaos.campo',
        'contexto_configuracaos.is_delected',
        'contexto_configuracaos.created_at'
      )
        .table('contexto_configuracaos')
        .where("contexto_configuracaos.is_delected", false)
        .orderBy("contexto_configuracaos.nome", "ASC");

        return DataResponse.response(null, 200, "Registado com sucesso", listagem);      
    }

  }


  /**
   * Display a single ContextoConfiguracao.
   * GET contexto_configuracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing ContextoConfiguracao.
   * GET contexto_configuracaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update ContextoConfiguracao details.
   * PUT or PATCH contexto_configuracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {

    var dataActual = moment(new Date()).format("YYYY-MM-DD");

    const {
      nome,
      campo,
      is_delected
    } = request.all();

    //console.log(request.all());

    const contexto_configuracao = await ContextoConfiguracao.find(params.id);

    //console.log(campo_id);

    contexto_configuracao.merge({
      nome: nome,
      is_delected: is_delected,
      campo: campo,
      //user_id: user_id
    });

    await contexto_configuracao.save();

    let listagem = await Database.select(
      'contexto_configuracaos.id',
      'contexto_configuracaos.nome',
      'contexto_configuracaos.campo',
      'contexto_configuracaos.is_delected',
      'contexto_configuracaos.created_at'

    )
      .table('contexto_configuracaos')
      .orderBy("contexto_configuracaos.nome", "ASC");


    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      listagem
    );

  }


  /**
   * Delete a ContextoConfiguracao with id.
   * DELETE contexto_configuracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = ContextoConfiguracaoController
