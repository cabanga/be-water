'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const Servico = use("App/Models/Servico");
const CdmaEquipamento = use("App/Models/CdmaEquipamento");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')
var moment = require("moment");
/**
 * Resourceful controller for interacting with cdmaequipamentos
 */
class CdmaEquipamentoController {
  /**
   * Show a list of all cdmaequipamentos.
   * GET cdmaequipamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new cdmaequipamento.
   * GET cdmaequipamentos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new cdmaequipamento.
   * POST cdmaequipamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { Num_Serie, A_Key, MINCode, AgenciaFilialID } = request.all();

    var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

    const numeroSerie = await CdmaEquipamento.query()
      .where("Num_Serie", Num_Serie)
      .getCount();

    const Akey = await CdmaEquipamento.query()
      .where("A_Key", A_Key)
      .getCount();

    if (numeroSerie > 0) {
      return DataResponse.response(null, 500 ,"Esse número de série já existe", null);
      //return response.status(500).send(Mensagem.response(response.response.statusCode, 'Esse número de série já existe', numeroSerie))
    } else if (Akey > 0) {
      return DataResponse.response(
        null,
        500,
        "Esse A_Key já existe",
        null
      );
    } else {
      await CdmaEquipamento.create({
        Num_Serie: Num_Serie,
        A_Key: A_Key,
        MINCode: MINCode,
        AgenciaFilialID: AgenciaFilialID,
        Data_Status: data,
        DataEntrada: data,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Dispositivo registado com sucesso",
        null
      );
    }
  }

  /**
   * Display a single cdmaequipamento.
   * GET cdmaequipamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing cdmaequipamento.
   * GET cdmaequipamentos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update cdmaequipamento details.
   * PUT or PATCH cdmaequipamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["Num_Serie", "A_Key", "MINCode", "AgenciaFilialID"]);

    //console.log(data)

      const numeroSerie = await CdmaEquipamento.query()
      .where("Num_Serie", data.Num_Serie)
      .whereNot({ id: params.id })
      .getCount();

    const Akey = await CdmaEquipamento.query()
      .where("A_Key", data.A_Key)
      .whereNot({ id: params.id })
      .getCount();

    if (numeroSerie > 0) {
      return DataResponse.response(null, 500 ,"Esse número de série já existe", null);
      //return response.status(500).send(Mensagem.response(response.response.statusCode, 'Esse número de série já existe', numeroSerie))
    } else if (Akey > 0) {
      return DataResponse.response(
        null,
        500,
        "Esse A_Key já existe",
        null
      );
    } else {

      // update with new data entered
      const cdma = await CdmaEquipamento.find(params.id);
      cdma.merge(data);
      await cdma.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        null
      );

    }
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "cdma_equipamentos.id",
        "cdma_equipamentos.Num_Serie",
        "cdma_equipamentos.A_Key",
        "cdma_equipamentos.MINCode",
        "cdma_equipamentos.AgenciaFilialID",
        "filials.id as filialID",
        "filials.nome as filialNome",
        //"lojas.nome as lojaNome",
        "cdma_equipamentos.created_at"
      )
        .from("cdma_equipamentos")
        .leftJoin("filials", "filials.id", "cdma_equipamentos.AgenciaFilialID")
        //.leftJoin("lojas", "lojas.filial_id", "filials.id")
        .orderBy(orderBy == null ? "cdma_equipamentos.id" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "cdma_equipamentos.id",
        "cdma_equipamentos.Num_Serie",
        "cdma_equipamentos.A_Key",
        "cdma_equipamentos.MINCode",
        "cdma_equipamentos.AgenciaFilialID",
        "filials.id as filialID",
        "filials.nome as filialNome",
        //"lojas.nome as lojaNome",
        "cdma_equipamentos.created_at"
      )
        .from("cdma_equipamentos")
        .leftJoin("filials", "filials.id", "cdma_equipamentos.AgenciaFilialID")
        //.leftJoin("lojas", "lojas.filial_id", "filials.id")
        .where("cdma_equipamentos.Num_Serie", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "cdma_equipamentos.id" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    //console.log(res)

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Delete a cdmaequipamento with id.
   * DELETE cdmaequipamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

  async validateSerie({ request, response, auth }) {

    const Serie = request.all().numero_serie;
    const cdma_numero = request.all().cdma_numero;
    //const cdmaEquipamento = request.all().numero_serie;

    const filia = await Database
    .select('fi.id as filialID', 'fi.nome as filialNome')
    .table('lojas as lo')
    .innerJoin('filials as fi', 'fi.id', 'lo.filial_id')
    .where('lo.id', auth.user.loja_id).first()
    //console.log(filia)
    const res = await CdmaEquipamento.query().where('AgenciaFilialID', filia.filialID).where('Num_Serie', Serie).select('*').first()

    const servico = await Servico.query().where('chaveServico ', cdma_numero).select('*').first()
    //console.log(servico)

    if(res && !servico){
          
      const resultado = await Database
      .select('ce.id as idCdmaEquip','cf.FabricanteDesc as fabricante', 'cm.DescricaoModelo as modelo')
      .table('cdma_equipamentos as ce')
      .innerJoin('cdma_fabricantes as cf', 'ce.Fabricante', 'cf.IDFabricante')
      .innerJoin('cdma_modelos as cm', 'ce.IDModelo', 'cm.IDModelo')
      .where('ce.Fabricante', res.Fabricante)
      .where('ce.IDModelo', res.IDModelo)
      .where('ce.Num_Serie', res.Num_Serie).first()

      //console.log(resultado)
          //encontrado e não usado
          return response.status(200).send(Mensagem.response(response.response.statusCode, 'Encontrado', resultado))
       
          
    }else if(res && servico){
      return response.status(302).send(Mensagem.response(response.response.statusCode, 'Ocupado', null))
    }else{
      //Não encontrado
      return response.status(404).send(Mensagem.response(response.response.statusCode, 'Não encontrado', null))
    }
   // return DataResponse.response.status(429).send("success", 200, "", res);
    

  }
}

module.exports = CdmaEquipamentoController
