'use strict'
const DataResponse = use("App/Models/DataResponse");
const TipologiaCliente = use("App/Models/TipologiaCliente");
const Database = use("Database");
var moment = require("moment");


class TipologiaClienteController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "tipologia_clientes.id",
        "tipologia_clientes.descricao",
        "tipologia_clientes.slug",
        "tipologia_clientes.juro_mora",
        "tipologia_clientes.sujeito_corte",
        "tipologia_clientes.nivel_sensibilidade_id",
        "nivel_sensibilidades.descricao as nivelsensibilidade",
        "tipologia_clientes.caucao"
      )
        .from("tipologia_clientes")
        .innerJoin("nivel_sensibilidades", "nivel_sensibilidades.id", "tipologia_clientes.nivel_sensibilidade_id")
        .orderBy(orderBy == null ? "tipologia_clientes.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "tipologia_clientes.id",
        "tipologia_clientes.descricao",
        "tipologia_clientes.slug",
        "tipologia_clientes.juro_mora",
        "tipologia_clientes.sujeito_corte",
        "tipologia_clientes.nivel_sensibilidade_id",
        "nivel_sensibilidades.descricao as nivelsensibilidade",
        "tipologia_clientes.caucao"
      )
        .from("tipologia_clientes")
        .innerJoin("nivel_sensibilidades", "nivel_sensibilidades.id", "tipologia_clientes.nivel_sensibilidade_id")
        /*         .orWhere("tipologia_clientes.slug", "like", "%" + search + "%") */
        .orWhere("tipologia_clientes.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "tipologia_clientes.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug, juro_mora, sujeito_corte, nivel_sensibilidade_id, caucao } = request.all();
    /* 
        console.log(request.all()); */

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    /*     const Verify = await Database.table('tipologia_clientes')
          .where("slug", slug)
          .getCount();
    
        if (Verify > 0) {
          return DataResponse.response(
            null,
            201,
            "Já existe Tipologia de Cliente com este slug",
            Verify
          );
    
        } else { */
    const tipologiaCliente = await Database.table('tipologia_clientes').insert({
      slug: slug,
      descricao: descricao,
      user_id: auth.user.id,
      juro_mora: juro_mora,
      caucao: caucao,
      sujeito_corte: sujeito_corte,
      nivel_sensibilidade_id: nivel_sensibilidade_id,
      'created_at': dataActual,
      'updated_at': dataActual
    });
    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      tipologiaCliente
    );
    //}
  }

  async update({ params, request }) {
    const data = request.only(["descricao", "slug", "juro_mora", "sujeito_corte", "nivel_sensibilidade_id", "caucao"]);

    /*     const Verify = await TipologiaCliente.query()
          .where("slug", data.slug)
          .whereNot({ id: params.id })
          .getCount();
    
        if (Verify > 0) {
          return DataResponse.response(
            null,
            201,
            "Já existe Tipologia de Cliente com este slug",
            Verify
          );
        } else { */

    // update with new data entered
    const tipologia_clientes = await TipologiaCliente.find(params.id);
    tipologia_clientes.merge(data);
    await tipologia_clientes.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tipologia_clientes
    );
    //}
  }

  async AssociarTiposTipologia({ request, auth, params }) {
    const { tipologia_id, descricao, slug, juro_mora, sujeito_corte, nivel_sensibilidade_id, caucao } = request.all();
    const data = request.only(["juro_mora", "sujeito_corte", "nivel_sensibilidade_id", "caucao"]);

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const dadosPai = await Database.select('id', 'descricao', 'juro_mora', 'sujeito_corte', 'nivel_sensibilidade_id', 'caucao')
      .from('tipologia_clientes')
      .where('id', params.id).first()

    data.juro_mora = dadosPai.juro_mora;
    data.sujeito_corte = dadosPai.sujeito_corte;
    data.nivel_sensibilidade_id = dadosPai.nivel_sensibilidade_id;
    data.caucao = dadosPai.caucao;

    if (nivel_sensibilidade_id !== null || juro_mora !== null || sujeito_corte !== null || caucao !== null) {

      /*       console.log('All Null'); */

      const tipos_tipologias = await Database.table('tipos_tipologias').insert({
        tipologia_id: params.id,
        descricao: descricao,
        juro_mora: data.juro_mora,
        sujeito_corte: dadosPai.sujeito_corte,
        nivel_sensibilidade_id: dadosPai.nivel_sensibilidade_id,
        caucao: dadosPai.caucao,
        parent_id: params.id,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }

    if (nivel_sensibilidade_id != null || juro_mora === null || sujeito_corte != null || caucao != null) {

      /*       console.log('Juro Mora is null'); */

      const tipos_tipologias = await Database.table('tipos_tipologias').insert({
        tipologia_id: params.id,
        descricao: descricao,
        juro_mora: data.juro_mora,
        sujeito_corte: sujeito_corte,
        nivel_sensibilidade_id: nivel_sensibilidade_id,
        caucao: caucao,
        parent_id: params.id,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }

    if (nivel_sensibilidade_id != null || juro_mora != null || sujeito_corte === null || caucao != null) {

      /*       console.log('Sujeito Corte is null'); */

      const tipos_tipologias = await Database.table('tipos_tipologias').insert({
        tipologia_id: params.id,
        descricao: descricao,
        juro_mora: juro_mora,
        sujeito_corte: data.sujeito_corte,
        nivel_sensibilidade_id: nivel_sensibilidade_id,
        caucao: caucao,
        parent_id: params.id,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }

    if (nivel_sensibilidade_id != null || juro_mora != null || sujeito_corte != null || caucao === null) {

      /*       console.log('Caução is null'); */

      const tipos_tipologias = await Database.table('tipos_tipologias').insert({
        tipologia_id: params.id,
        descricao: descricao,
        juro_mora: juro_mora,
        sujeito_corte: data.sujeito_corte,
        nivel_sensibilidade_id: nivel_sensibilidade_id,
        caucao: data.caucao,
        parent_id: params.id,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }

    if (nivel_sensibilidade_id === null || juro_mora != null || sujeito_corte != null || caucao != null) {

      /*       console.log('Nivel Sensibilidade is null'); */

      const tipos_tipologias = await Database.table('tipos_tipologias').insert({
        tipologia_id: params.id,
        descricao: descricao,
        juro_mora: juro_mora,
        sujeito_corte: sujeito_corte,
        nivel_sensibilidade_id: data.nivel_sensibilidade_id,
        caucao: data.caucao,
        parent_id: params.id,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    } else {

      /*       console.log('Normal'); */

      const tipos_tipologias = await Database.table('tipos_tipologias').insert({
        tipologia_id: params.id,
        descricao: descricao,
        juro_mora: juro_mora,
        sujeito_corte: sujeito_corte,
        nivel_sensibilidade_id: nivel_sensibilidade_id,
        caucao: caucao,
        parent_id: params.id,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    }
  }

  async getTiposbyTipologia({ request, params }) {

    const { orderBy } = request.all();

    const TiposTipologia = await Database.select(
      "tipos_tipologias.id",
      "tipos_tipologias.descricao",
      "tipologia_clientes.slug",
      "tipos_tipologias.juro_mora",
      "tipos_tipologias.sujeito_corte",
      "tipos_tipologias.tipologia_id",
      "tipologia_clientes.descricao as tipologia",
      "tipos_tipologias.tipologia_id",
      "tipos_tipologias.nivel_sensibilidade_id",
      "nivel_sensibilidades.descricao as nivelsensibilidade",
      "tipos_tipologias.caucao"
    )
      .from("tipos_tipologias")
      .leftJoin("nivel_sensibilidades", "nivel_sensibilidades.id", "tipos_tipologias.nivel_sensibilidade_id")
      .leftJoin("tipologia_clientes", "tipologia_clientes.id", "tipos_tipologias.tipologia_id")
      .where("tipos_tipologias.tipologia_id", params.id)
      .orderBy(orderBy == null ? "tipos_tipologias.created_at" : orderBy, "DESC")

    return DataResponse.response("success", 200, "", TiposTipologia);

  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'descricao', 'slug', 'juro_mora', 'sujeito_corte', 'caucao')
      .from('tipologia_clientes')
      .orderBy('descricao', 'ASC');

    return res;
  }

  async selectGroupBox({ request }) {

    var res = await Database.select(
      'tipologia_clientes.id', 
      'tipologia_clientes.descricao', 
      'tipologia_clientes.slug', 
      'tipologia_clientes.juro_mora', 
      'tipologia_clientes.sujeito_corte', 
      'tipologia_clientes.caucao')
      .from('tipologia_clientes')
      .where('parent_id', null)
      .orderBy('descricao', 'ASC');

    for (let index = 0; index < res.length; index++) {

      var child = await Database.select(
        'tipologia_clientes.id', 
        'tipologia_clientes.descricao', 
        'tipologia_clientes.slug', 
        'tipologia_clientes.juro_mora', 
        'tipologia_clientes.sujeito_corte', 
        'tipologia_clientes.caucao')
        .from('tipologia_clientes')
        .where('parent_id', res[index].id)
        .orderBy('descricao', 'ASC');

        res[index].childs = child;

    }


    return res;
  }


}

module.exports = TipologiaClienteController
