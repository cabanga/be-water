'use strict'
const ClassificacaoProdutoRepository = use('App/Repositories/ClassificacaoProdutoRepository')

class ClassificacaoProdutoController {

    #_classificacaoRepo

    constructor() {
        this.#_classificacaoRepo = new ClassificacaoProdutoRepository()
    }

    async index({ response }) {
        const res = await this.#_classificacaoRepo.getAll()
        return response.ok(res);
    }

    async store({ request }) {
        const data = request.only(['descricao'])
        const res = await this.#_classificacaoRepo.create( data )
        return res
    }

    async show({params, response}){
        const gender = await this.#_classificacaoRepo.findById(params.id)
        return response.ok(gender)
    }

    async update({ params, request }){
        const data = request.only(['descricao'])
        const res = await this.#_classificacaoRepo.update( params.id, data )
        return res
    }

    async delete({ params }) {
        const res = await this.#_classificacaoRepo.delete( params.id )
        return res
    }
    
}

module.exports = ClassificacaoProdutoController
