'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const LogsContadore = use("App/Models/LogsContadore");
const Database = use("Database");

/**
 * Resourceful controller for interacting with logscontadores
 */
class LogsContadoreController {
  /**
   * Show a list of all logscontadores.
   * GET logscontadores
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */

  async index({ params }) {

    const historicos = await Database.select(
      'log.id',
      'log.contador_id',
      'log.estado_contador_id',
      'estado_contadores.descricao as estado',
      'log.observacao_new',
      'log.ultima_leitura',
      'log.tipo_registo_id',
      'tipo_registos.descricao as tipo_registo',
      'user.nome as operador',
      "log.conta_id",
      "contas.contaDescricao as conta",
      "clientes.nome as cliente",
      "log.local_instalacao_id",
      "local_instalacaos.moradia_numero",
      "local_instalacaos.predio_id",
      "local_instalacaos.predio_nome",
      "local_instalacaos.rua_id",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      Database.raw('DATE_FORMAT(log.created_at, "%d-%m-%Y %H:%i:%s") as created_at'))
      .table('logs_contadores as log')
      .leftJoin('estado_contadores','estado_contadores.id','log.estado_contador_id')
      .leftJoin('tipo_registos','tipo_registos.id','log.tipo_registo_id')
      .leftJoin('users as user','user.id','log.user_id')
      .leftJoin("contas", "log.conta_id", "contas.id")
      .leftJoin("clientes", "contas.cliente_id", "clientes.id")
      .leftJoin("local_instalacaos", "log.local_instalacao_id", "local_instalacaos.id")
      .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
      .where('log.contador_id', params.id)
      .orderBy('log.id','DESC')

    return DataResponse.response("success", 200, "", historicos);
  }

  /**
   * Render a form to be used for creating a new logscontadore.
   * GET logscontadores/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new logscontadore.
   * POST logscontadores
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single logscontadore.
   * GET logscontadores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing logscontadore.
   * GET logscontadores/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update logscontadore details.
   * PUT or PATCH logscontadores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a logscontadore with id.
   * DELETE logscontadores/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = LogsContadoreController
