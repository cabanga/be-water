
'use strict'
const PeriodicidadeRoteiroRepository = use('App/Repositories/PeriodicidadeRoteiroRepository')


class PeriodicidadeRoteiroController {
    #_periodicidadeRepo

    constructor() {
      this.#_periodicidadeRepo = new PeriodicidadeRoteiroRepository()
    }

    async index({ response, request }) {
      const page = request.input('page')
      const search = request.input('search')
      const res = await this.#_periodicidadeRepo.getAll(page, search)
      return response.ok(res);
    }

    async store({ request }) {
      const data = request.only([
        'descricao',
        'ciclo'
      ])
      const res = await this.#_periodicidadeRepo.create( data )
      return res
    }

    async show({params, response}){
      const res = await this.#_periodicidadeRepo.findById(params.id)
      return response.ok(res)
    }

    async update({ params, request }){
      const data = request.only([
        'descricao',
        'ciclo'
      ])
      const res = await this.#_periodicidadeRepo.update( params.id, data )
      return res
    }

    async getBySlug({params, response}){
      const res = await this.#_periodicidadeRepo.getBySlug(params.slug)
      return response.ok(res)
    }

    async delete({ params }) {
      const res = await this.#_periodicidadeRepo.delete( params.id )
      return res
    }

}

module.exports = PeriodicidadeRoteiroController
