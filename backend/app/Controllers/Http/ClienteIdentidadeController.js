'use strict'

const ClienteIdentidadeRepository = use('App/Repositories/ClienteIdentidadeRepository')


class ClienteIdentidadeController {
  #_clienteIdentityRepo

  constructor() {
    this.#_clienteIdentityRepo = new ClienteIdentidadeRepository()
  }

  async index({ response, request }) {
    const page = request.input('page')
    const search = request.input('search')
    const res = await this.#_clienteIdentityRepo.getAll(page, search)

    return response.ok(res)
  }

  async store({ request }) {
    const data = request.only([
      'cliente_id',
      'tipo_identidade_id',
      'numero_identidade'
    ])

    const res = await this.#_clienteIdentityRepo.create( data )
    return res
  }

  async show({params, response}){
    const gender = await this.#_clienteIdentityRepo.findById(params.id)
    return response.ok(gender)
  }

  async identidadesClinete({params, response}){
    const gender = await this.#_clienteIdentityRepo.identidadesClinete(params.id)
    return response.ok(gender)
  }

  async update({ params, request }){
    const data = request.only([
      'cliente_id',
      'tipo_identidade_id',
      'numero_identidade'
    ])

    const res = await this.#_clienteIdentityRepo.update( params.id, data )
    return res
  }

  async delete({ params }) {
    const res = await this.#_clienteIdentityRepo.delete( params.id )
    return res
  }


}

module.exports = ClienteIdentidadeController
