'use strict'

const Factura = use("App/Models/Factura");
const DataResponse = use("App/Models/DataResponse");
const Produto = use("App/Models/Produto");
const Charge = use("App/Models/Charge");
const Moeda = use("App/Models/Moeda");
const Cambio = use("App/Models/Cambio");
const Leitura = use("App/Models/Leitura");
const ClasseTarifario = use("App/Models/ClasseTarifario");
const LogEstadoContrato = use("App/Models/LogEstadoContrato");
const CicloFacturacao = use("App/Models/CicloFacturacao");
const ContaCorrenteEstimativa = use("App/Models/ContaCorrenteEstimativa");


const Database = use("Database");
var numeral = require("numeral");
var moment = require("moment");

class FacturaUtilitieController {

  async index({ request, response, view }) { }

  async store({ request, response, auth }) {

    const { rota_header_id } = request.all();

    //const ciclo = await CicloFacturacao.query().where('ano', ano).where('mes',mes).first();


    const contas =  await Database
    .select("contas.*","contratoes.id as contrato_id").from("charges")
    .innerJoin("contas", "contas.id", "charges.conta_id")
    .innerJoin("contratoes", "contratoes.conta_id", "contas.id")
    .where('charges.is_facturado', 0)
    .where('contas.estado', 1)
    //.where('charges.ciclo_facturacao_id', ciclo.id)
    .where('charges.rota_header_id', rota_header_id)
    .groupBy('charges.conta_id','contratoes.id')


    for (let i = 0; i < contas.length; i++) {
      const conta = contas[i];
      const moeda = await this.findMoedaAndCambio(conta.moeda_id==null? 1: conta.moeda_id);

      var facturacao = {
        user_id: auth.user.id,
        conta_id: conta.id,
        contador_id:null,
        contrato_id: conta.contrato_id,
        cliente_id: conta.cliente_id,
        serie: 1, //ciclo.serie_id,
        moeda: moeda.moeda,
        produtos: [],
        totalImposto: 0,
        subtotal: 0,
        total: 0,
        valor_aberto: 0,
      };

      const charges = await Database
      .select(
        "charges.*",
        'impostos.valor as taxa',
        'produtos.imposto_id',
        'produtos.moeda_id',
        "charges.produto_id"
      )
      .from("charges")
      .innerJoin(
        "produtos", "produtos.id",
        "charges.produto_id"
      )
      .innerJoin(
        "impostos", "impostos.id",
        "produtos.imposto_id"
      )
      //.where('charges.ciclo_facturacao_id', ciclo.id)
      .where("charges.conta_id", conta.id)
      .where('charges.rota_header_id', rota_header_id)
      .where('charges.valor', '>', 0 );

      if (charges.length > 0) {

        for (let j = 0; j < charges.length; j++) {
          const charge = charges[j];
          charge.valor = (moeda.cambio!=null? cambio.codigo_iso != "AOA"? element.valor * cambio.valor_cambio: charge.valor: charge.valor)
          facturacao.contador_id = charge.contador_id;


          let produto = {
            charge_id: charge.id,
            artigo_id: charge.produto_id,
            valor: 0.0,
            quantidade: 1,
            desconto: 0.0,
            total: 0,
            valorImposto: 0.0,
            imposto_id: charge.imposto_id,
            moeda_id: conta.moeda_id,
            subtotal: 0.0,
          };

          produto.valor = charge.valor;
          produto.valorImposto = parseFloat(parseFloat(produto.quantidade * (produto.valor * (charge.taxa / 100))).toFixed(2));
          produto.subtotal = parseFloat(parseFloat(produto.valor * produto.quantidade).toFixed(2));
          produto.total = produto.subtotal + produto.valorImposto;

          facturacao.total += produto.total;
          facturacao.subtotal += produto.subtotal;
          facturacao.totalImposto += produto.valorImposto;

          facturacao.produtos.push(produto);
        }

        if (facturacao.total > 0) {
          const f = await Factura.FacturaCreate(facturacao);
        }
      }
    }

    return DataResponse.response( "success", contas.length > 0 ? 200 : 201, contas.length > 0 ? "(" + contas.length + ") contas facturadas com sucesso..." : "Nenhum registo encontrado", null );

  } 


  async preFacturacao({ request, auth }) {

    const { ano, mes, roteiro_id } = request.all();
    const ciclo = await Database.select('id','dia_facturacao as dia', 'ano','mes').from('ciclo_facturacaos').where('ano', ano).where('mes',mes).first();

    var length = 0+await this.preFactuacaoFixo(["ESTIMATIVA","LEITURA"], ciclo,roteiro_id, auth.user.id);
    length += await this.preFactuacaoPorLeitura(["LEITURA"],ciclo,roteiro_id, auth.user.id);  
    return DataResponse.response("success", (length > 0 ? 200 : 201), (length > 0 ? "Pré-facturação: (" + length + ") serviço facturados com sucesso..." : "Nenhum registo encontrado, verifica se a pré factura do perido selecione já foi processado..."), null);
  }

  async preFactuacaoFixo(type, ciclo,roteiro_id, user) {

    var periodo = ciclo.ano + "" + ciclo.mes;

    const servicos = await this.findServicoNotCharge(type,roteiro_id, periodo);
    console.log("------- preFactuacaoFixo ---------");
    for (let index = 0; index < servicos.length; index++) {
        const servico = servicos[index];
        const amount = await this.amountDaysConsumed(ciclo.ano, ciclo.mes,ciclo.dia, servico, servico.valor);
        console.log(amount);
        if(amount.value > 0){
            const ch = await Charge.create({
              conta_id: servico.conta_id,
              servico_id: servico.servico_id,
              produto_id: servico.produto_id,
              valor: amount.value,
              periodo: periodo,
              user_id: user,
              daily: amount.daily,
              local_consumo_id: servico.localConsumoId,
              valorOriginal: servico.valor,
              facturacao: "ESTIMATIVA",
              ciclo_facturacao_id: ciclo.id,
              rota_header_id: roteiro_id
            });
          }
    }
    return servicos.length > 0 ? servicos.length : 0;
  }

  async preFactuacaoPorLeitura(type,ciclo,roteiro_id, user) {
    var periodo = ciclo.ano + "" + ciclo.mes;
    const qtdMonth = 1;
    var length = 0;

    const servicos =await this.findConsumoNotCharge(type, roteiro_id, periodo);
    console.log("------- preFactuacaoPorLeitura ---------");

    for (let index = 0; index < servicos.length; index++) {

        const servico = servicos[index];
        const leitura  = await this.monthReading(servico.contador_id, periodo, qtdMonth);
      if (leitura.encontrado != -1) {

        const value_tarifa_consumo = await this.classeTarifarioConumoMinMaxValue(servico.tarifario_id, leitura.leitura.valor);
        const produto_id = value_tarifa_consumo.produto_id;
        var valor = value_tarifa_consumo.valor * (leitura.leitura.valor/(leitura.encontrado == 0 ? qtdMonth : 1)); 
        var valorOriginal = valor;  
 
          if (leitura.encontrado == 1) { 
            const cce = await Database.select('*').from('conta_corrente_estimativas').where("contador_id", servico.contador_id).where('estado', false).orderBy('valor', 'ASC')

            if (cce.length > 0) {
                var control = 0;
                for (let index = 0; index < cce.length; index++) {
                  const element = cce[index];
                    valor -= element.valor;
                    if (control == 0) {
                        control = 1;
                        await ContaCorrenteEstimativa.query().where("id", element.id).update({periodo_usado:periodo, estado: true })
                    }
                    control = (valor < 0 ? 1 : 0);
                }

              if (valor < 0) {
                const ch = await ContaCorrenteEstimativa.create({
                  valor: valor < 0 ? valor * -1 : valor,
                  periodo: periodo,
                  contador_id: servico.contador_id,
                  meses: 0,
                  descricao: "CREDITO",
                  user_id: user,
                });
              }
               valor = valor > 0 ? valor : 0;
            }
          }else if (leitura.encontrado == 0) {
            console.log("ESTIMATIVA",leitura);
            const ch = await ContaCorrenteEstimativa.create({
                valor: valor,
                periodo: periodo,
                contador_id: servico.contador_id,
                meses:qtdMonth,
                descricao:'MEDIA DE CONSUMO',
                user_id: user
            });
          } 

        if (valor > 0) {
          const data={
            conta_id: servico.conta_id,
            produto_id: produto_id,
            valor: valor,
            contador_id: servico.contador_id,
            periodo: periodo,
            user_id: user, 
            local_consumo_id: servico.localConsumoId,
            valorOriginal: valorOriginal,
            facturacao: leitura.encontrado == 1 ? "LEITURA" : "MEDIA CONSUMO",
            ciclo_facturacao_id: ciclo.id,
            rota_header_id: roteiro_id
          } 
          const ch = await Charge.create(data);
          length++;
        }

      }

    }
    return length;
  }

  /**
   * @description permite retornar: 1 - mes de leitura se existir, senão 2 - n-meses(2, 3 ou 4 dependendo da valor passo em qtdMonth) de leituras,
   * senão 3 - Anomalida(alguma coisa não está)
   */
  async monthReading(contador, periodo, qtdMonth) {

    var leitura = await Leitura.query().select('contador_id','consumo as valor').where("contador_id", contador).where(Database.raw('DATE_FORMAT(leituras.data_leitura, "%Y%m")'), periodo).first();

    if (!leitura) {
        var leitura = await Database.raw("select l.contador_id, SUM(l.consumo) as valor, COUNT(l.contador_id) count from leituras l, "
        +"(select DATE_FORMAT(data_leitura, '%Y%m') as periodo, consumo, contador_id from leituras where contador_id="+contador+" order by DATE_FORMAT(data_leitura, '%Y%m') desc LIMIT "+qtdMonth+") "
        +"leituras2 WHERE DATE_FORMAT(l.data_leitura, '%Y%m') = leituras2.periodo AND l.contador_id = "+contador+" GROUP BY l.contador_id ")
      leitura = leitura[0];

        return { encontrado: leitura.length > 0 ? (leitura[0].count == qtdMonth ? 0:-1) :-1, leitura: leitura[0]};
    }
    return { encontrado: 1, leitura: leitura };
  }

  async amountDaysConsumed(ano, mes,dia, contrato, valor) {

    console.log('dia ',dia);
        var Current = new Date(ano+"-"+mes + "-01");
        var ultDia = new Date(Current.getFullYear(), Current.getMonth() + 1, 0);
        var ultimoDia = moment(ultDia).format("DD");

        const periodo = "" + ano + "-" +(mes < 10 ? "0" + mes : mes);

        let log_estado = await LogEstadoContrato.query().select('log_estado_contratoes.*',Database.raw('DATE_FORMAT(log_estado_contratoes.data_operacao, "%Y-%m") as mesAno'),
        "log_estado_contratoes.data_operacao","log_estado_contratoes.id_estado_actual", Database.raw('DATE_FORMAT(log_estado_contratoes.data_operacao, "%d") as dia'))
        .innerJoin('estado_contratoes','estado_contratoes.id','log_estado_contratoes.id_estado_actual')
        .where("log_estado_contratoes.contrato_id", contrato.contrato_id)
        .where(Database.raw('DATE_FORMAT(log_estado_contratoes.data_operacao, "%Y-%m")'), periodo)
        .orderBy("id", "desc").first();

        var dailyestadoContrato = 0;
        var value = 0;
        var daily = 0;

        if(log_estado != null){
            if(periodo == log_estado.mesAno){ dailyestadoContrato = log_estado.id_estado_actual ==1 ? (ultimoDia - log_estado.dia+1):log_estado.dia; }
              var daily = (periodo == log_estado.mesAno ? (ultimoDia - log_estado.dia+1) : ultimoDia);
              daily = (dailyestadoContrato > 0? dailyestadoContrato-dia:daily-dia);
              value = (valor*daily)/(ultimoDia);
            return { value, daily };
        }else{
            daily = ultimoDia-dia;
            value = (valor*daily)/(ultimoDia);
          return { value, daily };
        }
  }

  async findServicoNotCharge(type,roteiro_id, periodo) {

    const servicos = await Database.select(
        "tipo_medicaos.slug as type", "local_consumos.contador_id","local_consumos.contrato_id",
        "servicos.id as servico_id", "servicos.produto_id", "servicos.valorBase as valor", "local_consumos.id as localConsumoId", "contratoes.conta_id as conta_id","contratoes.id",
        Database.raw('DATE_FORMAT(contratoes.activation_date, "%Y-%m-%d") as contratodata'),
        Database.raw('DATE_FORMAT(contratoes.activation_date, "%Y-%m") as contratoMesAno'),
        Database.raw('DATE_FORMAT(contratoes.activation_date, "%m") as contratoMes'),
        Database.raw('DATE_FORMAT(contratoes.activation_date, "%d") as contratoDia')).from("servicos")
      .innerJoin("contratoes", "servicos.contrato_id", "contratoes.id")
      .innerJoin("local_consumos", "contratoes.id", "local_consumos.contrato_id")
      .innerJoin("tipo_medicaos", "contratoes.tipo_contracto_id", "tipo_medicaos.id")
      .whereNotNull('contratoes.activation_date').whereIn("tipo_medicaos.slug", type)
      .where("servicos.estado", 1)
      .whereIn("local_consumos.id", Database.select('local_consumo_id').from("rota_runs").where('rota_header_id',roteiro_id))
      .whereNotIn("servicos.id", Database.select('servico_id').from("charges").where('rota_header_id', roteiro_id).where('periodo', periodo));

    return servicos;
  }

  async findConsumoNotCharge(type, roteiro_id,periodo) {
    const servicos = await Database.select("tarifarios.id as tarifario_id",
          "tipo_medicaos.slug as type", "local_consumos.contador_id","local_consumos.contrato_id",
          "local_consumos.id as localConsumoId", "contratoes.conta_id as conta_id", "contratoes.id",
          Database.raw('DATE_FORMAT(contratoes.activation_date, "%Y-%m-%d") as contratodata'),
          Database.raw('DATE_FORMAT(contratoes.activation_date, "%Y-%m") as contratoMesAno'),
          Database.raw('DATE_FORMAT(contratoes.activation_date, "%m") as contratoMes'),
          Database.raw('DATE_FORMAT(contratoes.activation_date, "%d") as contratoDia'))
      .from("contratoes")
      .innerJoin("local_consumos", "contratoes.id", "local_consumos.contrato_id")
      .innerJoin("tipo_medicaos", "contratoes.tipo_contracto_id", "tipo_medicaos.id")
      .innerJoin("tarifarios", "tarifarios.id", "contratoes.tarifario_id")
      .whereIn("tipo_medicaos.slug", type)
      .whereIn("local_consumos.id", Database.select('local_consumo_id').from("rota_runs").where('rota_header_id',roteiro_id))
      .whereNotIn("local_consumos.contador_id", Database.select('contador_id').from("charges").whereNotNull('contador_id').where('rota_header_id', roteiro_id).where('periodo', periodo));

    return servicos;
  }

  async classeTarifarioConumoMinMaxValue(tarifario_id, consumo) {

    var tarifa = await ClasseTarifario.query().select("classe_tarifarios.*",'tarifa_variavel as valor')
      .where("tarifario_id", tarifario_id).whereNotNull("consumo_maximo").whereNotNull("consumo_minimo")
      .whereRaw("consumo_minimo < "+consumo+" AND consumo_maximo > "+consumo).first();
    if (!tarifa) {
       tarifa = await ClasseTarifario.query()
         .select("classe_tarifarios.*", "tarifa_fixa_mensal as valor")
         .where("tarifario_id", tarifario_id).whereNull("consumo_maximo").whereNull("consumo_minimo")
         .first();
    }

    return tarifa;
  }


  async findMoedaAndCambio(moeda_id) {
      const moeda = await Moeda.find(moeda_id);
      console.log('moeda_id: ',moeda_id);
      const cambio = await Cambio.query().where('moeda_id',moeda_id).where("cambios.is_active", true).first();

    return { moeda, cambio};
  }




}

module.exports = FacturaUtilitieController
