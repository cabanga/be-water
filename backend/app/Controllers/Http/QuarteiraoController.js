'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const Quarteirao = use("App/Models/Quarteirao");
const Bairro = use("App/Models/Bairro");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage');
/**
 * Resourceful controller for interacting with quarteiraos
 */
class QuarteiraoController {
  /**
   * Show a list of all quarteiraos.
   * GET quarteiraos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "quarteiraos.id",
        "quarteiraos.nome",
        "quarteiraos.is_active",
        "quarteiraos.bairro_id",
        "bairros.nome as bairro",
        "bairros.distrito_id",
        "distritos.nome as distrito",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "quarteiraos.user_id",
        "users.nome as user"
      )
        .from("quarteiraos")
        .leftJoin("bairros", "bairros.id", "quarteiraos.bairro_id")
        .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
        .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
        .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
        .innerJoin("users", "users.id", "quarteiraos.user_id")

        .orderBy(orderBy == null ? "quarteiraos.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "quarteiraos.id",
        "quarteiraos.nome",
        "quarteiraos.is_active",
        "quarteiraos.bairro_id",
        "bairros.has_quarteirao",
        "bairros.nome as bairro",
        "bairros.distrito_id",
        "distritos.nome as distrito",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "quarteiraos.user_id",
        "users.nome as user"
      )
        .from("quarteiraos")
        .leftJoin("bairros", "bairros.id", "quarteiraos.bairro_id")
        .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
        .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
        .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
        .innerJoin("users", "users.id", "quarteiraos.user_id")

        .where("quarteiraos.nome", "like", "%" + search + "%")
        .orWhere("provincias.nome", "like", "%" + search + "%")
        .orWhere("municipios.nome", "like", "%" + search + "%")
        .orWhere("distritos.nome", "like", "%" + search + "%")
        .orWhere("bairros.nome", "like", "%" + search + "%")
        .orWhere("users.nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "quarteiraos.nome" : orderBy, "ASC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }


  async getQuarteiraoById({ params, request }) {
    let res = null;

    const {
      has_quarteirao,
      bairro_id
    } = request.all();

    //console.log(request.all());

    if (has_quarteirao) {

      res = await Database.select(
        "quarteiraos.id",
        "quarteiraos.nome",
        "quarteiraos.is_active",
        "quarteiraos.bairro_id",
        "bairros.has_quarteirao",
        "bairros.nome as bairro",
        "bairros.distrito_id",
        "distritos.nome as distrito",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "quarteiraos.user_id",
        "users.nome as user"
      )
        .from("quarteiraos")
        .leftJoin("bairros", "bairros.id", "quarteiraos.bairro_id")
        .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
        .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
        .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
        .innerJoin("users", "users.id", "quarteiraos.user_id")

        .where("quarteiraos.id", params.id)
        .first();
    }
    else {

      res = await Database.select(
        "bairros.id",
        "bairros.nome",
        "bairros.has_quarteirao",
        "bairros.is_active",
        "bairros.distrito_id",
        "distritos.nome as distrito",
        "municipios.has_distrito",
        "bairros.municipio_id",
        "municipios.nome as municipio",
        "municipios.provincia_id",
        "provincias.nome as provincia",
        "bairros.user_id",
        "users.nome as user",
        "bairros.created_at"
      )
        .from("bairros")
        .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
        .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
        .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
        .innerJoin("users", "users.id", "bairros.user_id")
        .where("bairros.id", bairro_id)
        .first();
    }

    //console.log(res);

    return DataResponse.response("success", 200, "", res);
  }



  async getQuarteiraosByBairro({ params }) {
    let res = null;

    res = await Database.select(
      "quarteiraos.id",
      "quarteiraos.nome",
      "quarteiraos.is_active",
      "quarteiraos.bairro_id",
      "bairros.has_quarteirao",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "quarteiraos.user_id",
      "users.nome as user"
    )
      .from("quarteiraos")
      .leftJoin("bairros", "bairros.id", "quarteiraos.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
      .innerJoin("users", "users.id", "quarteiraos.user_id")

      .where("quarteiraos.bairro_id", params.id)
      .orderBy("quarteiraos.nome", "ASC");

    return DataResponse.response("success", 200, "", res);
  }



  /**
   * Render a form to be used for creating a new quarteirao.
   * GET quarteiraos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new quarteirao.
   * POST quarteiraos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {

    const {
      nome,
      bairro_id,
      user_id
    } = request.all();

    //console.log("request");
    //console.log(request.all());

    const res = await Quarteirao.query()
      .where("nome", nome)
      .where("bairro_id", bairro_id)
      .getCount();

    if (res > 0) {
      return DataResponse.response(
        null,
        500,
        "Esta quarteirao já existe.",
        null
      );
    } else {
      await Quarteirao.create({
        nome: nome,
        bairro_id: bairro_id,
        is_active: true,
        user_id: user_id
      });

      
      const bairro = await Bairro.find(bairro_id);

      bairro.merge({
        has_quarteirao: true
      });
      await bairro.save();


      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
    }

  }

  /**
   * Display a single quarteirao.
   * GET quarteiraos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing quarteirao.
   * GET quarteiraos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update quarteirao details.
   * PUT or PATCH quarteiraos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {

    const {
      nome,
      bairro_id,
      is_active,
      user_id
    } = request.all();

    const quarteirao = await Quarteirao.find(params.id);

    quarteirao.merge({
      nome: nome,
      bairro_id: bairro_id,
      is_active: is_active,
      user_id: user_id
    });
    await quarteirao.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );

  }

  /**
   * Delete a quarteirao with id.
   * DELETE quarteiraos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }


  async selectBoxByBairro({ params }) {

    let res = await Database.select(
      "quarteiraos.id",
      "quarteiraos.nome",
      "quarteiraos.bairro_id",
      "bairros.has_quarteirao",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
    )
      .from("quarteiraos")
      .leftJoin("bairros", "bairros.id", "quarteiraos.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .innerJoin("provincias", "provincias.id", "municipios.provincia_id")

      .where("quarteiraos.bairro_id", params.id)
      .where("quarteiraos.is_active", true)
      .orderBy("quarteiraos.nome", "ASC");

    //console.log(res);

    return res;

  }

}

module.exports = QuarteiraoController
