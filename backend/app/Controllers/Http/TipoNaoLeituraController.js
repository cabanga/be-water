'use strict'

const TipoNaoLeitura = use("App/Models/TipoNaoLeitura");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");

class TipoNaoLeituraController {

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        'tipo_nao_leituras.id',
        'tipo_nao_leituras.nome',
        'tipo_nao_leituras.is_active',
        'tipo_nao_leituras.user_id as user_id',
        'users.nome as user',
        'tipo_nao_leituras.created_at',
        'tipo_nao_leituras.updated_at'
      )
        .table('tipo_nao_leituras')
        .leftJoin('users', 'tipo_nao_leituras.user_id', 'users.id')

        .orderBy(orderBy == null ? 'tipo_nao_leituras.created_at' : orderBy, 'DESC')
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        'tipo_nao_leituras.id',
        'tipo_nao_leituras.nome',
        'tipo_nao_leituras.is_active',
        'tipo_nao_leituras.user_id as user_id',
        'users.nome as user',
        'tipo_nao_leituras.created_at',
        'tipo_nao_leituras.updated_at'
      )
        .table('tipo_nao_leituras')
        .leftJoin('users', 'tipo_nao_leituras.user_id', 'users.id')

        .orWhere("tipo_nao_leituras.nome", "like", "%" + search + "%")

        .orderBy(orderBy == null ? "tipo_nao_leituras.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }
    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {

    const {
      nome,
      user_id
    } = request.all();

    const Verify = await Database.table('tipo_nao_leituras')
      .where("nome", nome)
      .getCount();

    if (Verify > 0) {

      return DataResponse.response(
        null,
        201,
        "Esse Tipo de Não Leitura já existe",
        Verify
      );

    } else {

      const leitura = await TipoNaoLeitura.create({
        nome: nome,
        is_active: true,
        user_id: auth.user.id
      });


      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
    }
  }

  async update({ params, request, auth }) {

    const {
      nome,
      is_active,
      user_id
    } = request.all();

    const rota_run = await TipoNaoLeitura.find(params.id);
    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('tipo_nao_leituras')
      .where("nome", nome)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {

      return DataResponse.response(
        null,
        201,
        "Esse Tipo de Não Leitura já existe",
        Verify
      );

    } else {

      const leitura = await Database.table('tipo_nao_leituras')
        .where('id', params.id)
        .update({
          nome: nome,
          is_active: is_active,
          user_id: auth.user.id,
          'updated_at': dataActual
        });
/* 
        
      const listagem = await Database.select(
        'tipo_nao_leituras.id',
        'tipo_nao_leituras.nome',
        'tipo_nao_leituras.is_active',
        'tipo_nao_leituras.user_id as user_id',
        'users.nome as user',
        'tipo_nao_leituras.created_at',
        'tipo_nao_leituras.updated_at'
      )
        .table('tipo_nao_leituras')
        .leftJoin('users', 'tipo_nao_leituras.user_id', 'users.id')
        .orderBy('tipo_nao_leituras.created_at', 'DESC');

 */
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        null
      );

    }
  }

  async selectBox({ request }) {
    let res = null;

    res = await Database.select(
      'tipo_nao_leituras.id',
      'tipo_nao_leituras.nome'
    )
      .table('tipo_nao_leituras')

      .where('tipo_nao_leituras.is_active', true)
      .orderBy('tipo_nao_leituras.nome', 'ASC');

    return res;
  }


}

module.exports = TipoNaoLeituraController
