'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const CicloFacturacao = use("App/Models/CicloFacturacao");
const Database = use("Database");
/**
 * Resourceful controller for interacting with ciclofacturacaos
 */
class CicloFacturacaoController {
  /**
   * Show a list of all ciclofacturacaos.
   * GET ciclofacturacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "ciclo_facturacaos.id",
        "ciclo_facturacaos.serie_id",
        "ciclo_facturacaos.dia_facturacao",
        "ciclo_facturacaos.ano",
        "ciclo_facturacaos.mes",
        "ciclo_facturacaos.dia_ultima_leitura",
        "ciclo_facturacaos.estado",
        "series.nome as serieDescricao",
        "documentos.nome as documento",
        "documentos.sigla",
        "estado_ciclo_facturacaos.descricao as estadoDescricao"
      )
        .from("ciclo_facturacaos")
        .leftJoin("series", "ciclo_facturacaos.serie_id", "series.id")
        .leftJoin("documentos", "documentos.id", "series.documento_id")
        .leftJoin("estado_ciclo_facturacaos", "ciclo_facturacaos.estado_id", "estado_ciclo_facturacaos.id")
        .orderBy(orderBy == null ? "ciclo_facturacaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "ciclo_facturacaos.id",
        "ciclo_facturacaos.serie_id",
        "ciclo_facturacaos.dia_facturacao",
        "ciclo_facturacaos.ano",
        "ciclo_facturacaos.mes",
        "ciclo_facturacaos.dia_ultima_leitura",
        "ciclo_facturacaos.estado",
        "series.nome as serieDescricao",
        "documentos.nome as documento",
        "documentos.sigla",
        "estado_ciclo_facturacaos.descricao as estadoDescricao"
      )
        .from("ciclo_facturacaos")
        .leftJoin("series", "ciclo_facturacaos.serie_id", "series.id")
        .leftJoin("documentos", "documentos.id", "series.documento_id")
        .leftJoin("estado_ciclo_facturacaos", "ciclo_facturacaos.estado_id", "estado_ciclo_facturacaos.id")
        .where("ciclo_facturacaos.ano", "like", "%" + search + "%")
        .orWhere("ciclo_facturacaos.mes", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "ciclo_facturacaos.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new ciclofacturacao.
   * GET ciclofacturacaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new ciclofacturacao.
   * POST ciclofacturacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { 
      dia_facturacao, 
      ano, 
      mes, 
      serie_id, 
      dia_ultima_leitura 
    } = request.all();

    const Verify = await CicloFacturacao.query()
      .where("ano", ano)
      .where("mes", mes)
      //.where("dia_facturacao", dia_facturacao)
      .getCount();

    if (Verify) {
      return DataResponse.response(
        null,
        500,
        "Já existe um ciclo de facturação com o mesmo ano e mês",
        null
      );

    } else {

      const res = await CicloFacturacao.create({
        dia_facturacao: dia_facturacao,
        ano: ano,
        mes: mes,
        serie_id: serie_id,
        dia_ultima_leitura: dia_ultima_leitura,
        estado: true,
        user_id: auth.user.id
      });

    }

    return DataResponse.response(
      "success",
      200,
      "Registo efectuado com sucesso",
      null
    );
  }

  /**
   * Display a single ciclofacturacao.
   * GET ciclofacturacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing ciclofacturacao.
   * GET ciclofacturacaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update ciclofacturacao details.
   * PUT or PATCH ciclofacturacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, auth }) {

    const { 
      dia_facturacao, 
      ano, 
      mes, 
      serie_id, 
      estado,
      dia_ultima_leitura 
    } = request.all();

    const ciclo_facturacao = await CicloFacturacao.find(params.id);

    ciclo_facturacao.merge({
      dia_facturacao: dia_facturacao,
      ano: ano,
      mes: mes,
      serie_id: serie_id,
      dia_ultima_leitura: dia_ultima_leitura,
      estado: estado,
      user_id: auth.user.id
    });

    await ciclo_facturacao.save();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );
  }


  async selectBoxAno({ request }) {

    var res = await Database.select('ano')
      .from('ciclo_facturacaos')
      .groupBy('ano');

    return res;
  }


  async selectMesByAno({ request }) {


    const { ano } = request.all();

    var res = await Database.select('id', 'mes', 'mes as number')
      .from('ciclo_facturacaos')
      .where('estado', false)
      .where('ano', ano)
      .orderBy('mes', "ASC")

    return res;
  }

  /**
   * Delete a ciclofacturacao with id.
   * DELETE ciclofacturacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }

  async selectBox({ request }) {
    const res = await Database.select('id', 'dia_facturacao', 'ano', 'mes', 'dia_ultima_leitura')
      .from('ciclo_facturacaos')
      .orderBy('ano', 'ASC');

    return res;
  }

}

module.exports = CicloFacturacaoController
