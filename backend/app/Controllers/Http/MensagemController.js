'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const TipoMensagem = use("App/Models/TipoMensagem");
const Database = use("Database");
/**
 * Resourceful controller for interacting with mensagems
 */
class MensagemController {
  /**
   * Show a list of all mensagems.
   * GET mensagems
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  async getMensagemByTipoMensagem({ params, request }) {

    let res = null;

    const {
      tipo_cliente_id
    } = request.all();

    if (tipo_cliente_id > 0) {
      res = await Database.select('id', 'texto', 'tipo_mensagem_id', 'tipo_cliente_id')
        .from('mensagems')
        .where('id', params.id)
        .where('tipo_cliente_id', tipo_cliente_id)
        .first();

    } else {
      res = await Database.select('id', 'texto', 'tipo_mensagem_id')
        .from('mensagems')
        .where('id', params.id)
        .first();

    }

    return res;
  }


  /**
   * Render a form to be used for creating a new mensagem.
   * GET mensagems/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new mensagem.
   * POST mensagems
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single mensagem.
   * GET mensagems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing mensagem.
   * GET mensagems/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update mensagem details.
   * PUT or PATCH mensagems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a mensagem with id.
   * DELETE mensagems/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = MensagemController
