'use strict'
const DataResponse = use("App/Models/DataResponse");
const Calibre = use("App/Models/Calibre");
const Database = use("Database");
var moment = require("moment");

class CalibreController {
  
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("calibres")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "descricao",
        "slug"
      )
        .from("calibres")
        .orWhere("calibres.descricao", "like", "%" + search + "%")
        .orWhere("calibres.slug", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { descricao, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('calibres')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Calibre com este Slug",
        Verify
      );

    } else {
      const Calibre = await Database.table('calibres').insert({
        slug: slug,
        descricao: descricao,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        Calibre
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["descricao", "slug"]);

    const Verify = await Calibre.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Calibre com este Slug",
        Verify
      );
    } else {

      // update with new data entered
      const calibres = await Calibre.find(params.id);
      calibres.merge(data);
      await calibres.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        calibres
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'descricao', 'slug')
      .from('calibres')
      .orderBy('descricao', 'ASC');

    return res;
  }
}

module.exports = CalibreController
