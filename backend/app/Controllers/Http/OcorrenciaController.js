'use strict'

const Ocorrencia = use("App/Models/Ocorrencia");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");

class OcorrenciaController {

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        'ocorrencias.id',
        'ocorrencias.motivo',
        'ocorrencias.rota_run_id',

        'rota_runs.rota_header_id',
        'rota_headers.descricao as rota_header',
        'rota_runs.local_consumo_id',
        'local_consumos.contador_id',
        'contadores.numero_serie',
        'local_consumos.local_instalacao as local_instalacao_id',
        'local_consumos.conta_id',
        'local_instalacaos.moradia_numero',
        'contas.cliente_id',
        'clientes.nome as cliente',
        'clientes.morada as cliente_morada',
        'clientes.telefone as cliente_telefone',
        'rota_runs.estado_rota_id',
        'estado_rotas.designacao as estado_rota',
        'estado_rotas.slug as estado_rota_slug',

        'ocorrencias.tipo_ocorrencia_id',
        'tipo_ocorrencias.nome as tipo_ocorrencia',
        'ocorrencias.is_delected',
        'ocorrencias.user_id as user_id',
        'users.nome as user',
        'ocorrencias.created_at',
        'ocorrencias.updated_at'
      )
        .table('ocorrencias')
        .innerJoin('rota_runs', 'ocorrencias.rota_run_id', 'rota_runs.id')
        .innerJoin('rota_headers', 'rota_runs.rota_header_id', 'rota_headers.id')
        .leftJoin('local_consumos', 'rota_runs.local_consumo_id', 'local_consumos.id')
        .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
        .leftJoin('estado_rotas', 'rota_runs.estado_rota_id', 'estado_rotas.id')
        .leftJoin('local_instalacaos', 'local_consumos.local_instalacao', 'local_instalacaos.id')
        .leftJoin('contas', 'local_consumos.conta_id', 'contas.id')
        .leftJoin('clientes', 'contas.cliente_id', 'clientes.id')
        .leftJoin('tipo_ocorrencias', 'ocorrencias.tipo_ocorrencia_id', 'tipo_ocorrencias.id')
        .leftJoin('users', 'ocorrencias.user_id', 'users.id')

        .orderBy(orderBy == null ? 'ocorrencias.created_at' : orderBy, 'DESC')
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        'ocorrencias.id',
        'ocorrencias.motivo',
        'ocorrencias.is_delected',
        'ocorrencias.user_id as user_id',
        'users.nome as user',
        'ocorrencias.created_at',
        'ocorrencias.updated_at'
      )
        .table('ocorrencias')
        .leftJoin('users', 'ocorrencias.user_id', 'users.id')

        .orWhere("ocorrencias.motivo", "like", "%" + search + "%")

        .orderBy(orderBy == null ? "ocorrencias.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }
    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {

    const {
      motivo,
      rota_run_id,
      tipo_ocorrencia_id,
      latitude,
      longitude,
      user_id
    } = request.all();

    const Verify = await Database.table('ocorrencias')
      .where("motivo", motivo)
      .getCount();

    if (Verify > 0) {

      return DataResponse.response(
        null,
        201,
        "Esse  de Ocorrência já existe",
        Verify
      );

    } else {

      const leitura = await Ocorrencia.create({
        motivo: motivo,        
        rota_run_id,
        tipo_ocorrencia_id,
        latitude,
        longitude,
        is_delected: false,
        user_id: auth.user.id
      });


      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
    }
  }

  async update({ params, request, auth }) {

    const {
      motivo,
      is_delected,
      user_id
    } = request.all();

    const rota_run = await Ocorrencia.find(params.id);
    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('ocorrencias')
      .where("motivo", motivo)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {

      return DataResponse.response(
        null,
        201,
        "Esse  de Ocorrência já existe",
        Verify
      );

    } else {

      const leitura = await Database.table('ocorrencias')
        .where('id', params.id)
        .update({
          motivo: motivo,
          is_delected: is_delected,
          user_id: auth.user.id,
          'updated_at': dataActual
        });
      /* 
              
            const listagem = await Database.select(
              'ocorrencias.id',
              'ocorrencias.motivo',
              'ocorrencias.is_delected',
              'ocorrencias.user_id as user_id',
              'users.nome as user',
              'ocorrencias.created_at',
              'ocorrencias.updated_at'
            )
              .table('ocorrencias')
              .leftJoin('users', 'ocorrencias.user_id', 'users.id')
              .orderBy('ocorrencias.created_at', 'DESC');
      
       */
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        null
      );

    }
  }

  async selectBox({ request }) {
    let res = null;

    res = await Database.select(
      'ocorrencias.id',
      'ocorrencias.motivo'
    )
      .table('ocorrencias')

      .where('ocorrencias.is_delected', true)
      .orderBy('ocorrencias.motivo', 'ASC');

    return res;
  }


}

module.exports = OcorrenciaController
