'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const LocalConsumo = use("App/Models/LocalConsumo");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const LogEstadoPedido = use("App/Models/LogEstadoPedido");
const LogsContadore = use("App/Models/LogsContadore");
var moment = require("moment");

const LocalConsumoRepository = use('App/Repositories/LocalConsumoRepository')

/**
 * Resourceful controller for interacting with localconsumos
 */
class LocalConsumoController {

  #_localRepo

  constructor() {
    this.#_localRepo = new LocalConsumoRepository()
  }

  async getByRua({ params, response }) {
    const res = await this.#_localRepo.getByRua(params.rua_id)
    return response.ok(res)
  }

  async getByQuarterao({ params, response }) {
    const res = await this.#_localRepo.getByQuarterao(params.quarteirao_id)
    return response.ok(res)
  }

  async getByDistrito({ params, response }) {
    const res = await this.#_localRepo.getByDistrito(params.distrito_id)
    return response.ok(res)
  }

  async getByBairro({ params, response }) {
    const res = await this.#_localRepo.getByBairro(params.bairro_id)
    return response.ok(res)
  }

  async getByMunicipio({ params, response }) {
    const res = await this.#_localRepo.getByMunicipio(params.municipio_id)
    return response.ok(res)
  }

  async getByProvincia({ params, response }) {
    const res = await this.#_localRepo.getByProvincia(params.provincia_id)
    return response.ok(res)
  }
















  /**
   * Show a list of all localconsumos.
   * GET localconsumos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();

    let res = await Database.select(
      "local_consumos.id",
      "local_consumos.conta_id",
      "local_consumos.contador_id",
      "local_consumos.instalacao_sanitaria_qtd",
      "local_consumos.reservatorio_flag",
      "local_consumos.reservatorio_capacidade",
      "local_consumos.piscina_flag",
      "local_consumos.piscina_capacidade",
      "local_consumos.jardim_flag",
      "local_consumos.campo_jardim_id",
      "campo_jardims.nome as campo_jardim",
      "local_consumos.poco_alternativo_flag",
      "local_consumos.fossa_flag",
      "local_consumos.fossa_capacidade",
      "local_consumos.acesso_camiao_flag",
      "local_consumos.anexo_flag",
      "local_consumos.anexo_quantidade",
      "local_consumos.caixa_contador_flag",
      "local_consumos.abastecimento_cil_id",
      "abastecimento_cils.nome as abastecimento_cil",

      "local_consumos.local_instalacao_id",
      "local_instalacaos.moradia_numero",
      "local_instalacaos.is_predio",
      "local_instalacaos.predio_nome",
      "local_instalacaos.latitude",
      "local_instalacaos.longitude",
      "local_instalacaos.rua_id",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.has_distrito",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "contadores.numero_serie",

      "clientes.nome as cliente",
      "clientes.morada as cliente_morada",
      "clientes.telefone as cliente_telefone"

    )
      .from("local_consumos")
      .leftJoin("campo_jardims", "campo_jardims.id", "local_consumos.campo_jardim_id")
      .leftJoin("abastecimento_cils", "abastecimento_cils.id", "local_consumos.abastecimento_cil_id")
      .leftJoin("local_instalacaos", "local_instalacaos.id", "local_consumos.local_instalacao_id")
      .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
      .innerJoin("contadores", "local_consumos.contador_id", "contadores.id")
      .innerJoin("contas", "local_consumos.conta_id", "contas.id")
      .innerJoin("clientes", "contas.cliente_id", "clientes.id")

      .where(function () {
        if (search != null) {
          this.where("campo_jardims.nome", "like", "%" + search + "%")
          this.orWhere("abastecimento_cils.nome", "like", "%" + search + "%")
          this.orWhere("local_instalacaos.predio_nome", "like", "%" + search + "%")
          this.orWhere("contadores.numero_serie", "like", "%" + search + "%")
          this.orWhere("contas.id", "like", "%" + search + "%")
          this.orWhere("clientes.nome", "like", "%" + search + "%")
          this.orWhere("clientes.telefone", "like", "%" + search + "%")
          this.orWhere("municipios.nome", "like", "%" + search + "%")
          this.orWhere("distritos.nome", "like", "%" + search + "%")
          this.orWhere("ruas.nome", "like", "%" + search + "%")
          this.orWhere("users.nome", "like", "%" + search + "%")
        }
      })

      .orderBy(orderBy == null ? "local_instalacaos.created_at" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);


    return DataResponse.response("success", 200, "", res);
  }


  /**
   * Render a form to be used for creating a new localconsumo.
   * GET localconsumos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new localconsumo.
   * POST localconsumos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async store({ request, auth }) {
    const {
      contrato_id,
      residencia_id,
      contador_id,
      estado_pedido_sigla,
      observacao, pedido_id
    } = request.all();
    const log = request.only(['ultima_leitura']);

    var dataActual = moment(new Date()).format("YYYY-MM-DD HH:MM:ss");

    let operacao = null;
    operacao = "CONTRATO LEITURA";
    let tabela = null;
    tabela = "local_consumos";

    const getDadosContador = await Database.select(
      "id",
      "observacao",
      "ultima_leitura"
    )
      .from("contadores")
      .where("id", contador_id).first()

    log.ultima_leitura = getDadosContador.ultima_leitura;

    const estadoPedido = await Database.select("id")
      .table("estado_pedidos")
      .where("sigla", estado_pedido_sigla).first();

    const estadoContador = await Database.select("id")
      .table("estado_contadores")
      .where("slug", "USADO").first();

    const getPedido = await Database.select("*")
      .table("pedidos")
      .where("id", pedido_id).first();

    const localConsumo = await LocalConsumo.create({
      contrato_id: contrato_id,
      contador_id: contador_id,
      local_instalacao: residencia_id,
      user_id: auth.user.id
    });

    await Database
      .table('pedidos')
      .where('id', pedido_id)
      .update({
        'estado': estadoPedido.id,
        'observacao': observacao,
        'updated_at': dataActual,
        'local_consumo_id': localConsumo.id

      })

    await Database
      .table('contratoes')
      .where('id', contrato_id)
      .update({
        'estado_id': 1,
        'activation_date': dataActual
      })

    // ACTUALIZA O CONTADOR PARA EM USO
    await Database
      .table('contadores')
      .where('id', contador_id)
      .update({
        'estado_contador_id': estadoContador.id,
        'updated_at': dataActual

      })

    /*     await Database.table('logs_contadores').insert({
          contador_id: contador_id,
          estado_contador_id: estadoContador.id,
          ultima_leitura: log.ultima_leitura,
          user_id: auth.user.id,
          local_instalacao_id: residencia_id,
          local_consumo_id: localConsumo.id,
          operacao: operacao,
          tabela: tabela,
          conta_id: getPedido.conta_id,
          observacao_new: observacao,
          'created_at': dataActual,
          'updated_at': dataActual
        }) */

    await LogEstadoPedido.create({ 'pedido_id': pedido_id, 'id_estado_anterior': getPedido.estado, 'id_estado_novo': estadoPedido.id, 'user_id': auth.user.id, 'observacao_old': getPedido.observacao, 'observacao_new': observacao })

    const user = await LogsContadore.create({
      'contador_id': contador_id,
      'estado_contador_id': estadoContador.id,
      'ultima_leitura': log.ultima_leitura,
      'user_id': auth.user.id,
      'local_instalacao_id': residencia_id,
      'local_consumo_id': localConsumo.id,
      'operacao': operacao,
      'tabela': tabela,
      'conta_id': getPedido.conta_id,
      'observacao_new': observacao
    })

    //console.log(user);

    /*    const logContador = new LogsContadore();
       await logContador.registrarLogContador(auth.user.id, operacao, 'local_consumos', contador_id, log.ultima_leitura, null, null, estadoContador.id, null, observacao, residencia_id, localConsumo.id, getPedido.conta_id)

    */

    return DataResponse.response("success", 200, "Pedido finalizado com sucesso", localConsumo);


  }

  async localByConta({ params, request }) {

    let res = await Database.select(
      "local_consumos.id",
      "local_consumos.conta_id",
      "local_consumos.contador_id",

      "local_consumos.instalacao_sanitaria_qtd",
      "local_consumos.reservatorio_flag",
      "local_consumos.reservatorio_capacidade",
      "local_consumos.piscina_flag",
      "local_consumos.piscina_capacidade",
      "local_consumos.jardim_flag",
      "local_consumos.campo_jardim_id",
      "campo_jardims.nome as campo_jardim",
      "local_consumos.poco_alternativo_flag",
      "local_consumos.fossa_flag",
      "local_consumos.fossa_capacidade",
      "local_consumos.acesso_camiao_flag",
      "local_consumos.anexo_flag",
      "local_consumos.anexo_quantidade",
      "local_consumos.caixa_contador_flag",
      "local_consumos.abastecimento_cil_id",
      "abastecimento_cils.nome as abastecimento_cil",
      "local_consumos.calibre_id",

      "local_consumos.local_instalacao_id",
      "local_instalacaos.moradia_numero",
      "local_instalacaos.is_predio",
      "local_instalacaos.predio_nome",
      "local_instalacaos.latitude",
      "local_instalacaos.longitude",
      "local_instalacaos.rua_id",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.has_distrito",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "contadores.numero_serie",

      "clientes.nome as cliente",
      "clientes.morada as cliente_morada",
      "clientes.telefone as cliente_telefone"

    )
      .from("local_consumos")
      .leftJoin("campo_jardims", "campo_jardims.id", "local_consumos.campo_jardim_id")
      .leftJoin("abastecimento_cils", "abastecimento_cils.id", "local_consumos.abastecimento_cil_id")
      .leftJoin("local_instalacaos", "local_instalacaos.id", "local_consumos.local_instalacao_id")
      .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
      .innerJoin("contadores", "local_consumos.contador_id", "contadores.id")
      .innerJoin("contas", "local_consumos.conta_id", "contas.id")
      .innerJoin("clientes", "contas.cliente_id", "clientes.id")

      .where("local_consumos.conta_id", params.id)
      .orderBy('local_consumos.created_at', 'DESC')

    return res;
  }

  async getLocaisConsumoByContrato({ params }) {

    let res = await Database.select(
      "local_consumos.id",
      "local_consumos.conta_id",
      "local_consumos.contador_id",
      "local_consumos.contrato_id",

      /*
      "local_consumos.instalacao_sanitaria_qtd",
      "local_consumos.reservatorio_flag",
      "local_consumos.reservatorio_capacidade",
      "local_consumos.piscina_flag",
      "local_consumos.piscina_capacidade",
      "local_consumos.jardim_flag",
      "local_consumos.campo_jardim_id",
      "campo_jardims.nome as campo_jardim",
      "local_consumos.poco_alternativo_flag",
      "local_consumos.fossa_flag",
      "local_consumos.fossa_capacidade",
      "local_consumos.acesso_camiao_flag",
      "local_consumos.anexo_flag",
      "local_consumos.anexo_quantidade",
      "local_consumos.caixa_contador_flag",
      "local_consumos.abastecimento_cil_id",
      "abastecimento_cils.nome as abastecimento_cil",
*/

      "local_consumos.local_instalacao_id",
      "local_instalacaos.moradia_numero",
      "local_instalacaos.is_predio",
      "local_instalacaos.predio_nome",
      "local_instalacaos.latitude",
      "local_instalacaos.longitude",
      "local_instalacaos.rua_id",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.has_distrito",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "contadores.numero_serie",

      "clientes.nome as cliente",
      "clientes.morada as cliente_morada",
      "clientes.telefone as cliente_telefone"

    )
      .from("local_consumos")
      //.leftJoin("campo_jardims", "campo_jardims.id", "local_consumos.campo_jardim_id")
      //.leftJoin("abastecimento_cils", "abastecimento_cils.id", "local_consumos.abastecimento_cil_id")
      .leftJoin("local_instalacaos", "local_instalacaos.id", "local_consumos.local_instalacao_id")
      .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
      .leftJoin("contadores", "local_consumos.contador_id", "contadores.id")
      .innerJoin("contas", "local_consumos.conta_id", "contas.id")
      .innerJoin("clientes", "contas.cliente_id", "clientes.id")

      .where("local_consumos.contrato_id", params.id)
      .orderBy('local_consumos.created_at', 'DESC')

    //console.log(res)

    return res;
  }


  async getLocalConsumoWithContrato({ request }) {

    const { id, conta_id } = request.all();

    let res = await Database.select("id", "conta_id", "contador_id", "contrato_id")
      .from("local_consumos")
      .where("id", id)
      .where("conta_id", conta_id);

    return res;
  }


  /**
   * Display a single localconsumo.
   * GET localconsumos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing localconsumo.
   * GET localconsumos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update localconsumo details.
   * PUT or PATCH localconsumos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a localconsumo with id.
   * DELETE localconsumos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = LocalConsumoController
