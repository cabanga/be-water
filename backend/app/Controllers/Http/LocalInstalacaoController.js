'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */


const DataResponse = use("App/Models/DataResponse");
const LocalInstalacao = use("App/Models/LocalInstalacao");
const LocalInstalacaoHistorico = use("App/Models/LocalInstalacaoHistorico");
const LigacaoRamal = use("App/Models/LigacaoRamal");
var moment = require("moment");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage');

const LocalInstalacaoRepository = use('App/Repositories/LocalInstalacaoRepository')


class LocalInstalacaoController {

  #_localInstalacaoRepository

  constructor() {
    this.#_localInstalacaoRepository = new LocalInstalacaoRepository();
  }


  /**
   * Show a list of all local_instalacaos.
   * GET local_instalacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();

    let res = await Database.select(
      "local_instalacaos.id",
      "local_instalacaos.moradia_numero",
      "local_instalacaos.is_predio",
      "local_instalacaos.predio_id",
      "local_instalacaos.predio_nome",
      "local_instalacaos.predio_andar",
      "local_instalacaos.cil",
      "local_instalacaos.is_active",
      "local_instalacaos.latitude",
      "local_instalacaos.longitude",
      "local_instalacaos.saneamento_flag",
      "local_instalacaos.instalacao_sanitaria_qtd",
      "local_instalacaos.reservatorio_flag",
      "local_instalacaos.reservatorio_capacidade",
      "local_instalacaos.piscina_flag",
      "local_instalacaos.piscina_capacidade",
      "local_instalacaos.jardim_flag",
      "local_instalacaos.campo_jardim_id",
      "local_instalacaos.poco_alternativo_flag",
      "local_instalacaos.fossa_flag",
      "local_instalacaos.fossa_capacidade",
      "local_instalacaos.acesso_camiao_flag",
      "local_instalacaos.anexo_flag",
      "local_instalacaos.anexo_quantidade",
      "local_instalacaos.caixa_contador_flag",
      "local_instalacaos.estado_caixa_contador_id",
      "local_instalacaos.abastecimento_cil_id",
      "local_instalacaos.calibre_id",
      "ligacao_ramals.diamentro as ligacao_diametro",
      "ligacao_ramals.comprimento as ligacao_comprimento",
      "ligacao_ramals.profundidade as ligacao_profundidade",
      "ligacao_ramals.ponto_a_id as objecto_ligacao_id",
      "objecto_ligacao_ramals.descricao as objecto_ligacao",
      "objecto_ligacao_ramals.latitude as objecto_ligacao_latitude",
      "objecto_ligacao_ramals.longitude as objecto_ligacao_longitude",
      "tipo_objectos.descricao as tipo_objecto_ligacao",
      "local_instalacaos.rua_id",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "local_instalacaos.user_id",
      "users.nome as user",
      "local_instalacaos.created_at"
    )
      .from("local_instalacaos")
      .leftJoin("ligacao_ramals", "ligacao_ramals.local_instalacao_id", "local_instalacaos.id")
      .leftJoin("objecto_ligacao_ramals", "objecto_ligacao_ramals.id", "ligacao_ramals.ponto_a_id")
      .leftJoin("tipo_objectos", "tipo_objectos.id", "objecto_ligacao_ramals.tipo_objecto_id")
      .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
      .leftJoin("users", "users.id", "local_instalacaos.user_id")

      .where(function () {
        if (search != null) {
          this.where("local_instalacaos.predio_nome", "like", "%" + search + "%")
          this.orWhere("provincias.nome", "like", "%" + search + "%")
          this.orWhere("municipios.nome", "like", "%" + search + "%")
          this.orWhere("distritos.nome", "like", "%" + search + "%")
          this.orWhere("ruas.nome", "like", "%" + search + "%")
          this.orWhere("users.nome", "like", "%" + search + "%")
        }
      })

      .orderBy(orderBy == null ? "local_instalacaos.created_at" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);
    return DataResponse.response("success", 200, "", res);
  }



  async getLocaisMapa({ request }) {

    let res = await Database.select(
      "local_instalacaos.id",
      "local_instalacaos.moradia_numero",
      "local_instalacaos.is_predio",
      "local_instalacaos.predio_nome",
      "local_instalacaos.latitude",
      "local_instalacaos.longitude",
      "local_instalacaos.rua_id",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.has_distrito",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "local_consumos.conta_id",
      "local_consumos.contador_id",
      "contadores.numero_serie",

      'clientes.nome as cliente',
      'clientes.morada as cliente_morada',
      'clientes.telefone as cliente_telefone',

      "rota_runs.id",
      "rota_runs.rota_header_id",
      "rota_headers.descricao as rota_header",
      "rota_runs.local_consumo_id",
      "rota_runs.estado_rota_id",
      "estado_rotas.designacao as estado_rota"

    )
      .from("local_instalacaos")
      .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
      .leftJoin("local_consumos", "local_consumos.local_instalacao", "local_instalacaos.id")
      .leftJoin("contadores", "local_consumos.contador_id", "contadores.id")
      .leftJoin('contas', 'local_consumos.conta_id', 'contas.id')
      .leftJoin('clientes', 'contas.cliente_id', 'clientes.id')
      .leftJoin("rota_runs", "rota_runs.local_consumo_id", "local_consumos.id")
      .leftJoin("rota_headers", "rota_runs.rota_header_id", "rota_headers.id")
      .leftJoin("estado_rotas", "rota_runs.estado_rota_id", "estado_rotas.id")


      .orderBy("local_instalacaos.created_at", "DESC");

    return res;
  }

  async getLigacoesFinaisByIdLocalInstacao({ params }) {

    return DataResponse.response("success", 200, "", await this.#_localInstalacaoRepository.getLigacoesFinaisByIdLocalInstacao(params.id));
  }


  async getLocalInstalacaoByRuaAndMoradia({ request }) {
    const {
      rua_id,
      moradia_numero
    } = request.all();

    //console.log(request.all())
    let res = null;

    res = await Database.select(
      "local_instalacaos.id",
      "local_instalacaos.moradia_numero",
      "local_instalacaos.is_predio",
      "local_instalacaos.predio_id",
      "local_instalacaos.predio_nome",
      "local_instalacaos.predio_andar",
      "local_instalacaos.cil",
      "local_instalacaos.latitude",
      "local_instalacaos.longitude",
      "local_instalacaos.rua_id",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia"
    )
      .from("local_instalacaos")
      .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .innerJoin("provincias", "provincias.id", "municipios.provincia_id")

      .where("local_instalacaos.rua_id", rua_id)
      .where("local_instalacaos.moradia_numero", moradia_numero)
      .first();

    return res;
  }

  async getLocalInstalacaoById({ params }) {

    return await this.#_localInstalacaoRepository.getLocalInstalacaoById(params.id);
  }



  async getLocalInstalacaosByRua({ params }) {
    let res = null;

    res = await Database.select(
      "local_instalacaos.id",
      "local_instalacaos.predio_nome",
      "local_instalacaos.is_active",
      "local_instalacaos.latitude",
      "local_instalacaos.longitude",
      "local_instalacaos.rua_id",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      "local_instalacaos.user_id",
      "users.nome as user"
    )
      .from("local_instalacaos")
      .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
      .innerJoin("users", "users.id", "local_instalacaos.user_id")

      .where("local_instalacaos.rua_id", params.id)
      .orderBy("local_instalacaos.created_at", "DESC");

    return DataResponse.response("success", 200, "", res);
  }




  async getSelectLocalInstalacaosByRua({ request }) {

    const { start, end, search, rua_id } = request.all();

    let residencias = await Database
      .select('id', 'moradia_numero', 'is_predio', 'predio_nome', 'predio_andar', 'rua_id', 'cil')
      .from('local_instalacaos')

      .where(function () {
        if (search != null) {
          this.where("moradia_numero", "like", "%" + search + "%")
        }
      })
      .where('rua_id', rua_id)
      .where("is_active", true)
      .paginate(start, end);

    //console.log(residencias)

    return DataResponse.response("success", 200, null, residencias);
  }


  async getLocalInstalacaosByCILAndMoradia({ request }) {

    const { start, end, search } = request.all();

    const subquery = Database
  .from('local_consumos')
  .whereNotNull('local_instalacao_id')
  .select('local_instalacao_id');

  //console.log(subquery);

    let residencias = await Database
      .select(
        'local_instalacaos.id',
        'local_instalacaos.moradia_numero',
        'local_instalacaos.is_predio',
        'local_instalacaos.predio_nome',
        'local_instalacaos.predio_andar',
        'local_instalacaos.rua_id',
        'local_instalacaos.cil',
        'local_instalacaos.instalacao_sanitaria_qtd',
        'local_instalacaos.reservatorio_flag',
        'local_instalacaos.reservatorio_capacidade',
        'local_instalacaos.piscina_flag',
        'local_instalacaos.piscina_capacidade',
        'local_instalacaos.jardim_flag',
        'local_instalacaos.campo_jardim_id',
        'local_instalacaos.poco_alternativo_flag',
        'local_instalacaos.fossa_flag',
        'local_instalacaos.fossa_capacidade',
        'local_instalacaos.acesso_camiao_flag',
        'local_instalacaos.anexo_flag',
        'local_instalacaos.anexo_quantidade',
        'local_instalacaos.caixa_contador_flag',
        'local_instalacaos.estado_caixa_contador_id',
        'local_instalacaos.abastecimento_cil_id')
      .from('local_instalacaos')


      .where("local_instalacaos.is_active", true)
      .whereNotIn("local_instalacaos.id", subquery)
      .where(function () {
        if (search != null) {
          this.where("cil", "like", "%" + search + "%")
          this.orWhere("predio_nome", "like", "%" + search + "%")
          this.orWhere("predio_andar", "like", "%" + search + "%")
          this.orWhere("moradia_numero", "like", "%" + search + "%")
        }
      })
      .paginate(start, end);

    //console.log(residencias)

    return DataResponse.response("success", 200, null, residencias);
  }

  // NEW LIGAÇÃO RAMAL
  async ligacaoRamLocalInstalacaosByRua({ request }) {

    const { start, end, search, rua_id } = request.all();

    let residencias = null;

    residencias = await LocalInstalacao.ligacaoRamLocalInstalacaosByRua(start, end, search, rua_id);

    return DataResponse.response("success", 200, null, residencias);
  }

  // END NEW LIGAÇÃO RAMAL


  async getSelectResidenciaInLocalConsumoByRua({ request }) {

    const { start, end, search, rua_id } = request.all();


    let residencias = null;

    if (search == null) {

      residencias = await Database
        .select(
          'local_instalacaos.id',
          'local_instalacaos.moradia_numero',
          'local_consumos.id as local_consumo_id',
          'local_instalacaos.is_predio',
          'local_instalacaos.predio_nome')
        .from('local_instalacaos')

        .rightJoin(
          "local_consumos",
          "local_consumos.local_instalacao_id",
          "local_instalacaos.id"
        )

        .where('rua_id', rua_id)
        .where("local_instalacaos.is_active", true)
        .where("local_consumos.is_active", true)
        .paginate(start, end);
    } else {
      residencias = await Database
        .select('id', 'moradia_numero', 'is_predio', 'predio_nome')
        .from('local_instalacaos')
        .where('rua_id', rua_id)
        .where("is_active", true)
        .where("moradia_numero", "like", "%" + search + "%")
        .paginate(start, end);
    }

    //console.log(residencias)

    return DataResponse.response("success", 200, null, residencias);
  }

  async getCurrentGeolocation({ params, request }) {

    let result;
    let local = null;
    const axios = require('request');
    const places_apiKey = "AIzaSyDJ8uA-rm_2wI2r-oj_UEZIboOuIM0yzIY";

    local = await Database.select(
      "ruas.id",
      "ruas.nome",
      "ruas.is_active",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
    )
      .from("ruas")
      .leftJoin(
        "bairros",
        "bairros.id",
        "ruas.bairro_id"
      )
      .leftJoin(
        "distritos",
        "distritos.id",
        "bairros.distrito_id"
      )
      .leftJoin(
        "municipios",
        "municipios.id",
        "bairros.municipio_id"
      )
      .innerJoin(
        "provincias",
        "provincias.id",
        "municipios.provincia_id"
      )
      .where("ruas.id", params.id)
      .first();

    const query = await local.provincia
      + "+" + local.municipio
      + ((local.distrito_id > 0) ? "+" + local.distrito : "");

    const uri = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + query + "&key=" + places_apiKey; console.log(params.id)
    /* console.log(local)
    console.log(request.all())
    console.log(uri) */


    axios.post(uri, {
      json: { todo: '' }
    }, (error, res, body) => {
      if (error) {
        console.error(error);
        return null;
      }
      //console.log(`statusCode: ${res.statusCode}`)
      console.log(body);

      result = body;
      return body;
    })

  }



  /**
   * Render a form to be used for creating a new local_instalacao.
   * GET local_instalacaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new local_instalacao.
   * POST local_instalacaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {

    let predio_finded = null;

    let predio_id = null;
    let edificio = null;

    const {
      moradia_numero,
      is_predio,
      predio_andar,
      predio_nome,
      cil,
      rua_id,
      latitude,
      longitude,
      user_id,

      objecto_ligacao_id,
      diamentro,
      comprimento,
      profundidade,

      instalacao_sanitaria_qtd,
      reservatorio_flag,
      reservatorio_capacidade,
      piscina_flag,
      piscina_capacidade,
      jardim_flag,
      campo_jardim_id,
      poco_alternativo_flag,
      fossa_flag,
      fossa_capacidade,
      acesso_camiao_flag,
      anexo_flag,
      anexo_quantidade,
      caixa_contador_flag,
      estado_caixa_contador_id,
      abastecimento_cil_id

    } = request.all();


    //console.log("request");
    //console.log(request.all());

    const res = await LocalInstalacao.query()
      .where("moradia_numero", moradia_numero)
      .where("cil", cil)
      .where("rua_id", rua_id)
      .getCount();

    if (res > 0) {
      return DataResponse.response(
        null,
        500,
        "Este local instalação já existe.",
        null
      );
    } else {

      if (is_predio) {
        predio_finded = await Database.select(
          "local_instalacaos.id",
          "local_instalacaos.moradia_numero",
          "local_instalacaos.is_predio",
          "local_instalacaos.predio_id",
          "local_instalacaos.predio_nome",
          "local_instalacaos.predio_andar",
          "local_instalacaos.cil",
          "local_instalacaos.is_active",
          "local_instalacaos.latitude",
          "local_instalacaos.longitude",
          "local_instalacaos.rua_id"
        )
          .from("local_instalacaos")
          .where("is_predio", true)
          .where("predio_nome", predio_nome.trim())
          .where("rua_id", rua_id)
          .first();

        edificio = predio_nome;

        if (predio_finded) {
          edificio = predio_finded.predio_nome;
          predio_id = predio_finded.predio_id;
        }
        else predio_id = this.uuidv4();

        //console.log(predio_id);

      }

      const local_instalacao = await LocalInstalacao.create({
        moradia_numero: moradia_numero,
        is_predio: is_predio,
        predio_id: predio_id,
        predio_andar: predio_andar,
        predio_nome: edificio,
        cil: cil,
        rua_id: rua_id,
        latitude: latitude,
        longitude: longitude,

        instalacao_sanitaria_qtd: instalacao_sanitaria_qtd,
        reservatorio_flag: reservatorio_flag,
        reservatorio_capacidade: reservatorio_capacidade,
        piscina_flag: piscina_flag,
        piscina_capacidade: piscina_capacidade,
        jardim_flag: jardim_flag,
        campo_jardim_id: campo_jardim_id,
        poco_alternativo_flag: poco_alternativo_flag,
        fossa_flag: fossa_flag,
        fossa_capacidade: fossa_capacidade,
        acesso_camiao_flag: acesso_camiao_flag,
        anexo_flag: anexo_flag,
        anexo_quantidade: anexo_quantidade,
        caixa_contador_flag: caixa_contador_flag,
        estado_caixa_contador_id: estado_caixa_contador_id,
        abastecimento_cil_id: abastecimento_cil_id,

        is_active: true,
        user_id: user_id
      });


      /*
      const local_instalacao = await Database.select("id")
        .from("local_instalacaos")
        .where("moradia_numero", moradia_numero)
        .where("predio_nome", predio_nome)
        .where("rua_id", rua_id);
*/

      const ligacao_ramal = await LigacaoRamal.create({
        diamentro: diamentro,
        profundidade: profundidade,
        comprimento: comprimento,
        ponto_a_id: objecto_ligacao_id,
        local_instalacao_id: local_instalacao.id,
        user_id: user_id
      });

      const local_instalacao_historico = await LocalInstalacaoHistorico.save({
        local_instalacao: local_instalacao,
        user_id: user_id
      });

      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        null
      );
    }

  }

  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  /**
   * Display a single local_instalacao.
   * GET local_instalacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing local_instalacao.
   * GET local_instalacaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update local_instalacao details.
   * PUT or PATCH local_instalacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {

    let predio_finded = null;

    let predio_id = null;
    let edificio = null;

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const {
      moradia_numero,
      is_predio,
      is_active,
      predio_nome,
      rua_id,
      latitude,
      longitude,
      user_id,

      objecto_ligacao_id,
      diamentro,
      comprimento,
      profundidade,

      instalacao_sanitaria_qtd,
      reservatorio_flag,
      reservatorio_capacidade,
      piscina_flag,
      piscina_capacidade,
      jardim_flag,
      campo_jardim_id,
      poco_alternativo_flag,
      fossa_flag,
      fossa_capacidade,
      acesso_camiao_flag,
      anexo_flag,
      anexo_quantidade,
      caixa_contador_flag,
      estado_caixa_contador_id,
      abastecimento_cil_id

    } = request.all();

    const local_instalacao_historico = await this.#_localInstalacaoRepository.getLocalInstalacaoById(params.id);


    if (is_predio) {
      predio_finded = await Database.select(
        "local_instalacaos.id",
        "local_instalacaos.moradia_numero",
        "local_instalacaos.is_predio",
        "local_instalacaos.predio_id",
        "local_instalacaos.predio_nome",
        "local_instalacaos.predio_andar",
        "local_instalacaos.cil",
        "local_instalacaos.is_active",
        "local_instalacaos.latitude",
        "local_instalacaos.longitude",
        "local_instalacaos.rua_id"
      )
        .from("local_instalacaos")
        .where("is_predio", true)
        .where("predio_nome", predio_nome.trim())
        .where("rua_id", rua_id)
        .first();

      edificio = predio_nome;

      if (predio_finded) {
        edificio = predio_finded.predio_nome;
        predio_id = predio_finded.predio_id;
      }
      else predio_id = this.uuidv4();

    }

    await LocalInstalacao.query()
      .where('id', params.id)
      .update({
        moradia_numero: moradia_numero,
        is_predio: is_predio,
        predio_id: predio_id,
        predio_nome: edificio,
        rua_id: rua_id,
        latitude: latitude,
        longitude: longitude,

        instalacao_sanitaria_qtd: instalacao_sanitaria_qtd,
        reservatorio_flag: reservatorio_flag,
        reservatorio_capacidade: reservatorio_capacidade,
        piscina_flag: piscina_flag,
        piscina_capacidade: piscina_capacidade,
        jardim_flag: jardim_flag,
        campo_jardim_id: campo_jardim_id,
        poco_alternativo_flag: poco_alternativo_flag,
        fossa_flag: fossa_flag,
        fossa_capacidade: fossa_capacidade,
        acesso_camiao_flag: acesso_camiao_flag,
        anexo_flag: anexo_flag,
        anexo_quantidade: anexo_quantidade,
        caixa_contador_flag: caixa_contador_flag,
        estado_caixa_contador_id: estado_caixa_contador_id,
        abastecimento_cil_id: abastecimento_cil_id,

        is_active: is_active,
        updated_at: dataActual
      });

    /*
        await LigacaoRamal.query()
          .where('local_instalacao_id', params.id)
          .update({
            diamentro: diamentro,
            profundidade: profundidade,
            comprimento: comprimento,
            ponto_a_id: objecto_ligacao_id,
            updated_at: dataActual
          });
    */

    await LocalInstalacaoHistorico.update({
      local_instalacao_id: params.id,
      historico: local_instalacao_historico,
      actualizacao: await this.#_localInstalacaoRepository.getLocalInstalacaoById(params.id),
      user_id: user_id
    });

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      null
    );



  }

  /**
   * Delete a local_instalacao with id.
   * DELETE local_instalacaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }


  async selectBoxByRua({ params }) {

    let res = await Database.select(
      "local_instalacaos.id",
      "local_instalacaos.predio_nome",
      "local_instalacaos.rua_id",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
    )
      .from("local_instalacaos")
      .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
      .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
      .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
      .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
      .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
      .innerJoin("provincias", "provincias.id", "municipios.provincia_id")
      .innerJoin("users", "users.id", "local_instalacaos.user_id")

      .where("ruas.rua_id", params.id)
      .where("local_instalacaos.is_active", true)
      .orderBy("local_instalacaos.created_at", "DESC");
    return res;
  }

}

module.exports = LocalInstalacaoController
