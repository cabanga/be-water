'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const DataResponse = use("App/Models/DataResponse");
const CdmaNumero = use("App/Models/CdmaNumero");
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')
/**
 * Resourceful controller for interacting with cdmanumeros
 */
class CdmaNumeroController {
  /**
   * Show a list of all cdmanumeros.
   * GET cdmanumeros
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new cdmanumero.
   * GET cdmanumeros/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new cdmanumero.
   * POST cdmanumeros
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, response }) {
  }

  /**
   * Display a single cdmanumero.
   * GET cdmanumeros/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing cdmanumero.
   * GET cdmanumeros/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update cdmanumero details.
   * PUT or PATCH cdmanumeros/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
  }

  /**
   * Delete a cdmanumero with id.
   * DELETE cdmanumeros/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, auth }) {
  }

  async searchCdmaNumero({ request, auth }) {
    const { start, end, search } = request.all();
    let res = null;
    let subquery = null;
    let chaves = null;

    //console.log(request.all())

    const filia = await Database
    .select('fi.id as filialID', 'fi.nome as filialNome')
    .table('lojas as lo')
    .innerJoin('filials as fi', 'fi.id', 'lo.filial_id')
    .where('lo.id', auth.user.loja_id).first()

    if (search == null) {
       subquery = Database
      .from('cdma_servicos')
      .select('ChaveServico')

     chaves = await Database
      .select('id', 'ChaveServico','FilialID')
      .from('cdma_numeros')
      .where('FilialID', filia.filialID)
      .whereNotIn('ChaveServico', subquery)
        .paginate(start, end);
    } else {
      subquery = Database
      .from('cdma_servicos')
      .select('ChaveServico')

     chaves = await Database
      .select('id', 'ChaveServico','FilialID')
      .from('cdma_numeros')
      .where('FilialID', filia.filialID)
      .whereNotIn('ChaveServico', subquery)
        .where("ChaveServico", "like", "%" + search + "%")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, null, chaves);
  }

  async selectBox({ auth, response }) {

    //console.log(auth.user.loja_id)

    const filia = await Database
      .select('fi.id as filialID', 'fi.nome as filialNome')
      .table('lojas as lo')
      .innerJoin('filials as fi', 'fi.id', 'lo.filial_id')
      .where('lo.id', auth.user.loja_id).first()

    //console.log(filia)
    if(filia == undefined){
      return response.status(302).send(Mensagem.response(response.response.statusCode, 'Utilizador sem loja associada', null))
      //return DataResponse.response("error", 302, "Utilizador sem loja associada", null);
    }
/*
    const chaves = await Database
      .select('id', 'ChaveServico')
      .table('cdma_numeros')
      .where('FilialID', filia.filialID)
*/
    const subquery = Database
      .from('cdma_servicos')
      .select('ChaveServico')

    const chaves = await Database
      .select('id', 'ChaveServico','FilialID')
      .from('cdma_numeros')
      .where('FilialID', filia.filialID)
      .whereNotIn('ChaveServico', subquery)
      .limit(10)


    return DataResponse.response("success", 200, "", chaves);
  }
}

module.exports = CdmaNumeroController
