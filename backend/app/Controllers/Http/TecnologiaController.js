'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Tecnologia = use("App/Models/Tecnologia");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");

/**
 * Resourceful controller for interacting with tecnologias
 */
class TecnologiaController {
  /**
   * Show a list of all tecnologias.
   * GET tecnologias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "id",
        "nome",
        "opcao",
        "tipoFacturacao",
        "rota_list_dispositivos",
        "created_at"
      )
        .from("tecnologias")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "id",
        "nome",
        "opcao",
        "tipoFacturacao",
        "rota_list_dispositivos",
        "created_at"
      )
        .from("tecnologias")
        .where("nome", "like", "%" + search + "%")
        .orWhere("opcao", "like", "%" + search + "%")
        .orWhere("tipoFacturacao", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }


  async TecnologiasPrepago({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "id",
        "nome",
        "opcao",
        "tipoFacturacao",
        "rota_insert_dispositivo",
        "rota_list_dispositivos",
        "created_at"
      )
        .from("tecnologias")
        .where("tipoFacturacao", "PRE-PAGO")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await await Database.select(
        "id",
        "nome",
        "opcao",
        "tipoFacturacao",
        "rota_insert_dispositivo",
        "rota_list_dispositivos",
        "created_at"
      )
        .from("tecnologias")
        .where("tipoFacturacao", "PRE-PAGO")
        /*
        .where("nome", "like", "%" + search + "%")
        .orWhere("opcao", "like", "%" + search + "%")
        .orWhere("tipoFacturacao", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        */
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  

  /**
   * Render a form to be used for creating a new tecnologia.
   * GET tecnologias/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new tecnologia.
   * POST tecnologias
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { nome, opcao, tipoFacturacao} = request.all();
    const tecnologia = await Tecnologia.query()
      .where("nome", nome)
      .getCount();
    if (tecnologia > 0) {
      return DataResponse.response(
        null,
        500,
        "Já existe uma tecnologia com o mesmo nome",
        tecnologia
      );
    } else {
      const tecnologia = await Tecnologia.create({
        nome: nome,
        opcao: opcao,
        tipoFacturacao: tipoFacturacao,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Tecnologia registado com sucesso",
        null
      );
    }
  }

  /**
   * Display a single tecnologia.
   * GET tecnologias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing tecnologia.
   * GET tecnologias/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update tecnologia details.
   * PUT or PATCH tecnologias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only(["nome","opcao","tipoFacturacao", "rota_list_dispositivos"]);

    //console.log(data)

    const tecnologia = await Tecnologia.query()
    .where("nome", data.nome)
    .whereNot({ id: params.id })
    .getCount();

    //console.log(params.id)
  if (tecnologia > 0) {
    return DataResponse.response(
      null,
      500,
      "Já existe uma tecnologia com o mesmo nome",
      tecnologia
    );
  }else{

    // update with new data entered
    const tecnologia = await Tecnologia.find(params.id);
    tecnologia.merge(data);
    await tecnologia.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      tecnologia
    );

  }
  }

  /**
   * Delete a tecnologia with id.
   * DELETE tecnologias/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }

 async selectBox({request}) {
   const { tipoFacturacao } = request.all();
    const tecnologias = await Database.from('tecnologias').whereIn('tipoFacturacao',tipoFacturacao);
    return DataResponse.response("success", 200, "", tecnologias);
  } 


  async selectTecnologiaNome({ params }) {
    
    const tecnologia = await Database.select("*").table("tecnologias").where('id',params.id).first();

    return DataResponse.response("success", 200, "", tecnologia);
  }
  
  async selectPrePago( ) {
    const tipoFacturacao = "PRE-PAGO"
     const tecnologias = await Database.select("id","nome")
     .from('tecnologias')
     .where('tipoFacturacao',tipoFacturacao);
     return DataResponse.response(null, 200, "", tecnologias);
   } 

}

module.exports = TecnologiaController
