'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const DataResponse = use("App/Models/DataResponse");
const Ramal = use("App/Models/Ramal");
const Database = use("Database");
/**
 * Resourceful controller for interacting with ramals
 */
class RamalController {
  /**
   * Show a list of all ramals.
   * GET ramals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "ramals.id",
        "ramals.Material",
        "ramals.Diametro_nominal",
        "ramals.Comprimento",
        "ramals.Profundidade",
        "ramals.tipo_ramal_id",
        "tipo_ramals.descricao as Tipo_ramal",
        "ramals.Bairro",
        "ramals.Rua"
      )
        .from("ramals")
        .innerJoin("tipo_ramals", "tipo_ramals.id", "ramals.tipo_ramal_id")
        .orderBy(orderBy == null ? "ramals.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "ramals.id",
        "ramals.Material",
        "ramals.Diametro_nominal",
        "ramals.Comprimento",
        "ramals.Profundidade",
        "ramals.tipo_ramal_id",
        "tipo_ramals.descricao as Tipo_ramal",
        "ramals.Bairro",
        "ramals.Rua"
      )
        .from("ramals")
        .innerJoin("tipo_ramals", "tipo_ramals.id", "ramals.tipo_ramal_id")
        .where("ramals.Material", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "ramals.created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { Material, Diametro_nominal, Comprimento, Profundidade, tipo_ramal_id, Bairro, Rua } = request.all();

/*     const VerifyTipoRamal = await Ramal.query()
      .where("slug", slug)
      .getCount(); */

/*     if (VerifyTipoRamal > 0) {
      return DataResponse.response(
        null,
        500,
        "Já existe um semelhante",
        VerifyTipoRamal
      );
    } 
    else { */

      const ramal = await Ramal.create({
        Material: Material,
        Diametro_nominal: Diametro_nominal,
        Comprimento: Comprimento,
        Profundidade: Profundidade,
        Bairro: Bairro,
        Rua: Rua,
        tipo_ramal_id: tipo_ramal_id,
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        null
      );
    // } 
  }

  async update({ params, request }) {
    const data = request.only(["Material", "Diametro_nominal", "Comprimento", "Profundidade", "tipo_ramal_id", "Bairro", "Rua"]);

/*     const VerifyTipoRamal = await TipoRamal.query()
    .where("slug", data.slug)
    .whereNot({ id: params.id })
    .getCount();

  if (VerifyTipoRamal > 0) {
    return DataResponse.response(
      null,
      500,
      "Já existe um semelhante",
      VerifyTipoRamal
    );
  }
  else{ */

    // update with new data entered
    const ramal = await Ramal.find(params.id);
    ramal.merge(data);
    await ramal.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      ramal
      );
    // }
  }

  /**
   * Render a form to be used for creating a new ramal.
   * GET ramals/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new ramal.
   * POST ramals
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single ramal.
   * GET ramals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing ramal.
   * GET ramals/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update ramal details.
   * PUT or PATCH ramals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a ramal with id.
   * DELETE ramals/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = RamalController
