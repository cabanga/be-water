'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
/** @type {import('@adonisjs/framework/src/Env')} */

const Env = use('Env')
const Configuracao = use("App/Models/Configuracao");
const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
var moment = require("moment");

/**
 * Resourceful controller for interacting with configuracaos
 */
class ConfiguracaoController {
  /**
   * Show a list of all configuracaos.
   * GET configuracaos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request, response, view }) {
  }


  async listagem({ request }) {
    const { search, orderBy, pagination } = request.all();
    
    let res = await Database.select(
        'configuracaos.id',
        'configuracaos.slug',
        'configuracaos.default',
        'configuracaos.modulo',
        'configuracaos.modulo_id',
        'configuracaos.valor',
        'configuracaos.required',
        'configuracaos.is_active',
        'configuracaos.is_boolean',
        'configuracaos.contexto_configuracao_id',
        'contexto_configuracaos.nome as contexto',
        'contexto_configuracaos.campo as campo',
        'configuracaos.user_id',
        'users.nome as user',
        'configuracaos.created_at'

      )
        .table('configuracaos')
        .leftJoin('contexto_configuracaos', 'configuracaos.contexto_configuracao_id', 'contexto_configuracaos.id')
        .leftJoin('users', 'configuracaos.user_id', 'users.id')

        .where(function() {
          if (search != null) {
            this.orWhere("configuracaos.slug", "like", "%" + search + "%")
            this.orWhere("configuracaos.descricao", "like", "%" + search + "%")
            this.orWhere("configuracaos.valor", "like", "%" + search + "%")
            this.orWhere("configuracaos.modulo", "like", "%" + search + "%")
            this.orWhere("users.nome", "like", "%" + search + "%")  
          }
        })

        .orderBy(orderBy == null ? 'configuracaos.modulo' : orderBy, 'ASC')
        .paginate(pagination.page, pagination.perPage);
   

    //console.log(res.data);
    
    for (let index = 0; index < res.data.length; index++) {

      let contexto_configuracao = null;
      
      if(res.data[index].contexto != null && res.data[index].campo != null)
      {
        contexto_configuracao = await Database.raw("SELECT id, " + res.data[index].campo + " as contexto_valor FROM " + Env.get('DB_DATABASE') + "." + res.data[index].contexto + " WHERE id = " + res.data[index].valor + " LIMIT 1");
        //console.log(contexto_configuracao[0][0].contexto_valor);
      }
      //const user = await Database.raw("SELECT t.id, t.nome FROM tipos_negocios t, estabelecimentos e where t.id = e.tipo_negocio_id && e.contrato_id = " + contrato_id + " limit 1");

      const data = {
        id: res.data[index].id,
        slug: res.data[index].slug,
        default: res.data[index].default,
        modulo: res.data[index].modulo,
        modulo_id: res.data[index].modulo_id,
        valor: res.data[index].valor,
        required: res.data[index].required,
        is_active: res.data[index].is_active,
        is_boolean: res.data[index].is_boolean,
        contexto_configuracao_id: res.data[index].contexto_configuracao_id,
        contexto: res.data[index].contexto,
        campo: res.data[index].campo,
        contexto_valor: (res.data[index].contexto != null) ? contexto_configuracao[0][0].contexto_valor : null,
        user_id: res.data[index].user_id,
        user: res.data[index].user,
        created_at: res.data[index].created_at
      }

      res.data[index] = data;
    }

    //console.log(res);

    return DataResponse.response("success", 200, "", res);
  }

  async getConfiguracaobySlug({ params }) {

    //console.log(params);

    let res = await Database.select(
      'configuracaos.id',
      'configuracaos.slug',
      'configuracaos.default',
      'configuracaos.modulo',
      'configuracaos.modulo_id',
      'configuracaos.valor',
      'configuracaos.required',
      'configuracaos.is_boolean'
    )
      .table('configuracaos')
      .where('configuracaos.is_active', true)
      .where('configuracaos.slug', params.slug)
      .first();

      if(res.is_boolean) res.valor = (res.valor == 'false') ? null : res.valor;

    //console.log(res);

    return DataResponse.response("success", (res == null || res == "undefined") ? 404 : 200, "", res);

  }

  async getSelectBoxModulos({ request }) {

    let res = await Database.select(
      'configuracaos.id',
      'configuracaos.modulo',
      'configuracaos.modulo_id',
    )
      .table('configuracaos')
      .orderBy('configuracaos.modulo', 'ASC')

    return res;

  }


  /**
   * Render a form to be used for creating a new Configuracao.
   * GET configuracaos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {
  }

  /**
   * Create/save a new rotarun.
   * POST rotaruns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request }) {

    let modulo_finded = null;
    let modulo_id = null;
    let modulo_root = null;

    const {
      slug,
      descricao,
      modulo,
      contexto_configuracao_id,
      valor,
      required,
      is_boolean,
      user_id,
      default_conf
    } = request.all();

    //console.log(request.all());


    const res = await Configuracao.query()
      .where("slug", slug)
      .where("modulo", modulo)
      .getCount();

    if (res > 0) {
      return DataResponse.response(
        null,
        500,
        "Esta configuração já existe.",
        null
      );
    } else {

      modulo_finded = await Database.select(
        'configuracaos.id',
        'configuracaos.slug',
        'configuracaos.modulo',
        'configuracaos.modulo_id'
      )
        .table('configuracaos')
        .where("modulo", modulo.trim())
        .first();

      modulo_root = modulo;

      if (modulo_finded) {
        modulo_root = modulo_finded.modulo;
        modulo_id = modulo_finded.modulo_id;
      }
      else modulo_id = this.uuidv4();

      //console.log(modulo_id);

      await Configuracao.create({
        slug: slug,
        is_active: true,
        is_boolean: is_boolean,
        modulo: modulo_root,
        modulo_id: modulo_id,
        contexto_configuracao_id: contexto_configuracao_id,
        descricao: descricao,
        valor: valor,
        required: required,
        default: default_conf,
        user_id: user_id
      });

      let listagem = await Database.select(
        'configuracaos.id',
        'configuracaos.slug',
        'configuracaos.default',
        'configuracaos.modulo',
        'configuracaos.modulo_id',
        'configuracaos.valor',
        'configuracaos.required',
        'configuracaos.is_active',
        'configuracaos.is_boolean',
        'configuracaos.contexto_configuracao_id',
        'contexto_configuracaos.nome as contexto',
        'contexto_configuracaos.campo as campo',
        'configuracaos.user_id',
        'users.nome as user',
        'configuracaos.created_at'

      )
        .table('configuracaos')
        .leftJoin('contexto_configuracaos', 'configuracaos.contexto_configuracao_id', 'contexto_configuracaos.id')
        .leftJoin('users', 'configuracaos.user_id', 'users.id')

        .orderBy("configuracaos.modulo", "ASC");

      return DataResponse.response(
        "success",
        200,
        "Registado com sucesso",
        listagem
      );
    }

  }


  /**
   * Create/save a new rotarun.
   * POST rotaruns
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async autoStore({ request }) {

    let modulo_finded = null;
    let modulo_id = null;
    let modulo_root = null;

    const {
      slug,
      descricao,
      modulo,
      valor,
      required,
      user_id,
      default_conf
    } = request.all();

    const res = await Configuracao.query()
      .where("slug", slug)
      .where("modulo", modulo)
      .getCount();

    if (res <= 0) {

      modulo_finded = await Database.select(
        'configuracaos.id',
        'configuracaos.slug',
        'configuracaos.modulo',
        'configuracaos.modulo_id'
      )
        .table('configuracaos')
        .where("modulo", modulo.trim())
        .first();

      modulo_root = modulo;

      if (modulo_finded) {
        modulo_root = modulo_finded.modulo;
        modulo_id = modulo_finded.modulo_id;
      }
      else modulo_id = this.uuidv4();

      //console.log(modulo_id);

      await Configuracao.create({
        slug: slug,
        is_active: true,
        is_boolean: false,
        modulo: modulo_root,
        modulo_id: modulo_id,
        descricao: descricao,
        valor: valor,
        required: required,
        default: default_conf,
        user_id: user_id
      });

      return true;

    }

    return false;

  }

  uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  /**
   * Display a single Configuracao.
   * GET configuracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing Configuracao.
   * GET configuracaos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {
  }

  /**
   * Update Configuracao details.
   * PUT or PATCH configuracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {

    let modulo_finded = null;
    let modulo_id = null;
    let modulo_root = null;

    var dataActual = moment(new Date()).format("YYYY-MM-DD");

    const {
      slug,
      descricao,
      modulo,
      contexto_configuracao_id,
      valor,
      required,
      default_conf,
      is_active,
      is_boolean,
      user_id
    } = request.all();

    //console.log(request.all());


    const configuracao = await Configuracao.find(params.id);

    modulo_finded = await Database.select(
      'configuracaos.id',
      'configuracaos.slug',
      'configuracaos.modulo',
      'configuracaos.modulo_id'
    )
      .table('configuracaos')
      .where("modulo", modulo.trim())
      .first();

    modulo_root = modulo;

    if (modulo_finded) {
      modulo_root = modulo_finded.modulo_nome;
      modulo_id = modulo_finded.modulo_id;
    }
    else modulo_id = this.uuidv4();

    //console.log(modulo_id);

    configuracao.merge({
      slug: slug,
      modulo: modulo_root,
      modulo_id: modulo_id,      
      contexto_configuracao_id: contexto_configuracao_id,
      descricao: descricao,
      valor: valor,
      required: required,
      default: default_conf,
      is_active: is_active,
      is_boolean: is_boolean,
      user_id: user_id
    });

    await configuracao.save();

    let res = await Database.select(
      'configuracaos.id',
      'configuracaos.slug',
      'configuracaos.default',
      'configuracaos.modulo',
      'configuracaos.modulo_id',
      'configuracaos.valor',
      'configuracaos.required',
      'configuracaos.is_active',
      'configuracaos.is_boolean',
      'configuracaos.contexto_configuracao_id',
      'contexto_configuracaos.nome as contexto',
      'contexto_configuracaos.campo as campo',
      'configuracaos.user_id',
      'users.nome as user',
      'configuracaos.created_at'
    )
      .table('configuracaos')
      .leftJoin('contexto_configuracaos', 'configuracaos.contexto_configuracao_id', 'contexto_configuracaos.id')
      .leftJoin('users', 'configuracaos.user_id', 'users.id')

      .orderBy("configuracaos.modulo", "ASC");

      //console.log(res);
      
    for (let index = 0; index < res.length; index++) {

      let contexto_configuracao = null;
      
      if(res[index].contexto != null && res[index].campo != null)
      {
        contexto_configuracao = await Database.raw("SELECT id, " + res[index].campo + " as contexto_valor FROM " + Env.get('DB_DATABASE') + "." + res[index].contexto + " WHERE id = " + res[index].valor + " LIMIT 1");
      }

      const data = {
        id: res[index].id,
        slug: res[index].slug,
        default: res[index].default,
        modulo: res[index].modulo,
        modulo_id: res[index].modulo_id,
        valor: res[index].valor,
        required: res[index].required,
        is_active: res[index].is_active,
        is_boolean: res[index].is_boolean,        
        contexto_configuracao_id: res[index].contexto_configuracao_id,
        contexto: res[index].contexto,
        campo: res[index].campo,
        contexto_valor: (res[index].contexto != null) ? contexto_configuracao[0][0].contexto_valor : null,
        user_id: res[index].user_id,
        user: res[index].user,
        created_at: res[index].created_at
      }

      res[index] = data;
    }

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      res
    );


  }


  /**
   * Delete a Configuracao with id.
   * DELETE configuracaos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
  }
}

module.exports = ConfiguracaoController
