'use strict'
const DataResponse = use("App/Models/DataResponse");
const MesesMediaConsumo = use("App/Models/MediaConsumo");
const Database = use("Database");
var moment = require("moment");

class MesesMediaConsumoController {
  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("media_consumos")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "nome",
        "slug"
      )
        .from("media_consumos")
        .orWhere("slug", "like", "%" + search + "%")
        .orWhere("nome", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { nome, slug } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('media_consumos')
      .where("slug", slug)
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Meses de Média de Consumo com este slug",
        Verify
      );

    } else {
      const meses_mediaconsumos = await Database.table('media_consumos').insert({
        slug: slug,
        nome: nome,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        meses_mediaconsumos
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["nome", "slug"]);

    const Verify = await MesesMediaConsumo.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

    if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Meses de Média de Consumo com este slug",
        Verify
      );
    } else {

      // update with new data entered
      const meses_mediaconsumos = await MesesMediaConsumo.find(params.id);
      meses_mediaconsumos.merge(data);
      await meses_mediaconsumos.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        meses_mediaconsumos
      );
    }
  }

  async selectBox({ request }) {

    var res = await Database.select('id', 'nome', 'slug')
      .from('media_consumos')
      .orderBy('nome', 'ASC');

    return res;
  }
}

module.exports = MesesMediaConsumoController
