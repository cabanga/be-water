'use strict'
const GenderRepository = use('App/Repositories/GenderRepository')
const Database = use('Database');


class GeneroController {
    #_genderRepo

    constructor() {
        this.#_genderRepo = new GenderRepository()
    }

    async index({ response, request }) {
        const page = request.input('page')
        const search = request.input('search')
        const genders = await this.#_genderRepo.getAll(page, search)

        return response.ok(genders);
    }

    async store({ request }) {
        const data = request.only([
            'abreviacao',
            'descricao'
        ])
        const res = await this.#_genderRepo.create( data )
        return res
    }

    async show({params, response}){
        const gender = await this.#_genderRepo.findById(params.id)
        return response.ok(gender)
    }

    async update({ params, request }){
        const data = request.only([
            'abreviacao',
            'descricao'
        ])
        const res = await this.#_genderRepo.update( params.id, data )
        return res
    }

    async delete({ params }) {
        const res = await this.#_genderRepo.delete( params.id )
        return res
    }


    async selectBox() {
        const res = await Database.select(
            'id',
            'abreviacao',
            'descricao as nome'
        )
            .from('generos')
            .orderBy('descricao', 'ASC')
        return res;
    }

}

module.exports = GeneroController
