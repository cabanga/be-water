'use strict'

const DataResponse = use("App/Models/DataResponse");
const TipoJuro = use("App/Models/TipoJuro");
const Database = use("Database");
var moment = require("moment");

class TipoJuroController {

  async index({ request }) {
    const { search, orderBy, pagination } = request.all();
    let res = null;

    if (search == null) {
      res = await Database.select(
        "id",
        "dias",
        "valor_dias",
        "percentagem",
        "slug"
      )
        .from("tipo_juros")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    } else {
      res = await Database.select(
        "id",
        "dias",
        "valor_dias",
        "percentagem",
        "slug"
      )
        .from("tipo_juros")
        .orWhere("tipo_juros.slug", "like", "%" + search + "%")
        .orWhere("tipo_juros.descricao", "like", "%" + search + "%")
        .orderBy(orderBy == null ? "created_at" : orderBy, "DESC")
        .paginate(pagination.page, pagination.perPage);
    }

    return DataResponse.response("success", 200, "", res);
  }

  async store({ request, auth }) {
    const { slug, dias, valor_dias, percentagem } = request.all();

    var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

    const Verify = await Database.table('tipo_juros')
      .where("slug", slug)
      .getCount();

/*     if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo de Juro com este slug",
        Verify
      );

    } else */
     {
      const TipoJuro = await Database.table('tipo_juros').insert({
        slug: slug,
        dias: dias,
        valor_dias: valor_dias,
        percentagem: percentagem,
        user_id: auth.user.id,
        'created_at': dataActual,
        'updated_at': dataActual
      });
      return DataResponse.response(
        "success",
        200,
        "Registo efectuado com sucesso",
        TipoJuro
      );
    }
  }

  async update({ params, request }) {
    const data = request.only(["slug", "dias", "valor_dias", "percentagem"]);

    const Verify = await TipoJuro.query()
      .where("slug", data.slug)
      .whereNot({ id: params.id })
      .getCount();

/*     if (Verify > 0) {
      return DataResponse.response(
        null,
        201,
        "Já existe Tipo de Juro com este slug",
        Verify
      );
    } else */
     {

      // update with new data entered
      const tipo_juros = await TipoJuro.find(params.id);
      tipo_juros.merge(data);
      await tipo_juros.save();
      return DataResponse.response(
        "success",
        200,
        "Dados actualizados com sucesso",
        tipo_juros
      );
    }
  }
  
  async selectBox ({ request }) {  
    const res = await Database.select('id', 'dias', 'valor_dias', 'percentagem', 'slug')
    .from('tipo_juros')
    .orderBy('dias', 'ASC');
     
    return res;
  }
}

module.exports = TipoJuroController
