'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

const Projecto = use("App/Models/Projecto");
const ProjectoSerie = use("App/Models/ProjectoSerie");
const Database = use("Database");
const DataResponse = use("App/Models/DataResponse");

/**
 * Resourceful controller for interacting with projectos
 */
class ProjectoController {
  /**
   * Show a list of all projectos.
   * GET projectos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ request }) {
    const { start, end, search, order } = request.all();
    let res = null;

    if (search == null) {
      res = await await Database.select(
        "projectos.id",
        "descricao",
        "morada_projecto",
        "municipio",
        "provincia",
        "projectos.telefone",
        "data_inicio",
        "data_fim",
        "custo_total"
      )
        .from("projectos")
        .orderBy(order, "asc")
        .paginate(start, end);
    } else {
      res = await await Database.select(
        "projectos.id",
        "descricao",
        "morada_projecto",
        "municipio",
        "provincia",
        "projectos.telefone",
        "data_inicio",
        "data_fim",
        "custo_total"
      )
        .from("projectos")
        .where("projectos.descricao", "like", "%" + search + "%")
        .orWhere(
          Database.raw('DATE_FORMAT(projectos.created_at, "%Y-%m-%d")'),
          "like",
          "%" + search + "%"
        )
        .orderBy(order, "asc")
        .paginate(start, end);
    }

    return DataResponse.response("success", 200, "", res);
  }

  /**
   * Render a form to be used for creating a new projecto.
   * GET projectos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new projecto.
   * POST projectos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  async store({ request, auth }) {
    const {
      descricao,
      morada_projecto,
      municipio,
      provincia,
      telefone,
      data_inicio,
      data_fim,
      custo_total
    } = request.all();
    const projecto = await Projecto.query()
      .where("descricao", descricao)
      .getCount();

    if (projecto > 0) {
      return DataResponse.response(
        "success",
        500,
        "Projecto já existente",
        projecto
      );
    } else {
      const serie = await Projecto.create({
        descricao: descricao,
        morada_projecto: morada_projecto,
        municipio: municipio,
        provincia: provincia,
        telefone: telefone,
        data_inicio: data_inicio,
        data_fim: data_fim,
        custo_total: custo_total,
        user_id: auth.user.id
      });
      return DataResponse.response(
        "success",
        200,
        "Projecto registado com sucesso",
        projecto
      );
    }
  }

  /**
   * Display a single projecto.
   * GET projectos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing projecto.
   * GET projectos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update projecto details.
   * PUT or PATCH projectos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request }) {
    const data = request.only([
      "descricao",
      "morada_projecto",
      "municipio",
      "provincia",
      "telefone",
      "data_inicio",
      "data_fim",
      "custo_total"
    ]);

    // update with new data entered
    const projecto = await Projecto.find(params.id);
    projecto.merge(data);
    await projecto.save();
    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      projecto
    );
  }

  /**
   * Delete a projecto with id.
   * DELETE projectos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request }) {
    const projecto = await Projecto.find(params.id);
    await projecto.delete();

    return DataResponse.response(
      "success",
      200,
      "Dados actualizados com sucesso",
      projecto
    );
  }

  async adicionarSerieProjecto({ request, auth }) {

    const { serie_id, projecto_id} = request.all();
    const projecto = await ProjectoSerie.query().where("serie_id", serie_id).where("projecto_id", projecto_id).getCount();

    if (projecto > 0) {
      return DataResponse.response("success",500,"serie já existente",projecto);
    } else {
      const serie = await ProjectoSerie.create({
        serie_id: serie_id,
        projecto_id: projecto_id,
        user_id: auth.user.id
      });
      return DataResponse.response( "success", 200, "Projecto registado com sucesso", projecto );
    }
  }
   async serieProjecto({ request }){
      const { projecto_id } = request.all();
      const res = await Database.select('series.id', 'series.nome', 'series.proximo_numero', 'series.activo','documentos.nome as documento','documentos.sigla','series.movimento','series.tipo_movimento','series.documento_id','series.created_at')
        .from('series').innerJoin('documentos', 'documentos.id', 'series.documento_id')
        .innerJoin('projecto_series', 'series.id', 'projecto_series.serie_id')
        .where('projecto_series.projecto_id', projecto_id);

      return DataResponse.response("success", 200, "", res);
   }
}

module.exports = ProjectoController
