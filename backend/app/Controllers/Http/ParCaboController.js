'use strict'

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
const ParCabo = use('App/Models/ParCabo');
const DataResponse = use('App/Models/DataResponse');
const Database = use('Database'); 
/**
 * Resourceful controller for interacting with parcabos
 */
class ParCaboController {
  /**
   * Show a list of all parcabos.
   * GET parcabos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new parcabo.
   * GET parcabos/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new parcabo.
   * POST parcabos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ request, auth }) {
    const { descricao, central_id } = request.all(); 
 
      const res = await ParCabo.create({
        descricao: descricao,
        central_id: central_id,
        user_id: auth.user.id
      });
      return DataResponse.response("success", 200, "Registo efectuado com sucesso", res);
    
  }

  /**
   * Display a single parcabo.
   * GET parcabos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing parcabo.
   * GET parcabos/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update parcabo details.
   * PUT or PATCH parcabos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a parcabo with id.
   * DELETE parcabos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ParCaboController
