
'use strict'
const Database = use('Database');
const UnificarClienteDados = use('App/Models/UnificarCliente');

class UnificarClienteFirstStep{

    async store({cliente_manter, cliente_unificar, cliente_nome_manter, cliente_nome_unificar}, user){

        const unificados= await UnificarClienteDados.query().where("id_manter_cliente", cliente_manter).orWhere("id_unificar_cliente", cliente_unificar).first();
        if(unificados){
            
            return DataResponse.response("Alert", 500, "Os Dados Ja foram Unificados", unificados);
        }else{
            const clientesUnificar = await UnificarClienteDados.create(
                {
                    id_manter_cliente: cliente_manter,
                    nome_cliente_manter	: cliente_nome_manter,
                    id_unificar_cliente: cliente_unificar,
                    nome_cliente_unificar:cliente_nome_unificar,
                    user_id: user.user.id
    
                }
            );
        
        }

    }
}