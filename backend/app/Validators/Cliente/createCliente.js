'use strict'

class createCliente {
   get rules() {
    return {
      contribuente: `required|max:20`,
      nome: `required|min:3|max:80`,
      telefone: `required|min:9|max:80`,
      email: `required|email|string|max:80`, //"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"  regex:/(.+)@(.+)\.(.+)/i
      tipo_cliente_id: `required|integer|min:1|exists:tipo_clientes,id`,
      tipo_identidade_id: `required|integer|min:1|exists:tipo_identidades,id`,
    };
  }
  get messages() {
    return {
      "required": "É obrigatório informar o", 
      "nome.min": "É obrigatório informar no minimo 3 caráter no campo ",
      "nome.max": "É obrigatório informar no maxímo 80 caráter no campo ",
      
      "telefone.min": "É obrigatório informar no minimo 9 caráter no campo ",
      "telefone.max": "É obrigatório informar no maxímo 80 caráter no campo ",

      //"regex": "Não é valido o ",
       
      "exists": "É obrigatório infome um registo valido no campo ",
      "unique": "É obrigatório infome um registo de caracter único no campo ",
      "integer": "É obrigatório informe um número no campo ",
    };
  }

  async fails(errorMessages) { 
   return this.ctx.response.status(400).send(errorMessages[0].message+" "+errorMessages[0].field);
  }
}

module.exports = createCliente
