'use strict'

class facturaListFactura {
  get rules() { 

    return {
      startDate: "required",
      endDate: "required",  
    };
  }
  get messages() {
    return {  
      "startDate.required": "É obrigatório informar a data inicio ",
      "endDate.required": "É obrigatório informar a data fim", 
    };
  }

  async fails(errorMessages) {
   return this.ctx.response.status(400).send(errorMessages[0].message);
  }
}

module.exports = facturaListFactura
