'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class NaoLeitura extends Model {
}

module.exports = NaoLeitura
