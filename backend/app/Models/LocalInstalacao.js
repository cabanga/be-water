'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database");

class LocalInstalacao extends Model {

  rua() {
    return this.belongsTo('App/Models/Rua')
  }

  locais_consumo() {
    return this.hasMany("App/Models/LocalConsumo")
  }

  static async ligacaoRamLocalInstalacaosByRua(start, end, search, rua_id) {

    let residencias = null;

    if (search == null) {

      residencias = await Database
        .select('id', 'moradia_numero', 'is_predio', 'predio_nome')
        .from('local_instalacaos')
        .where('rua_id', rua_id)
        .where("is_active", true)
        .whereNotIn('id', Database.select("local_instalacao_id").from("ligacao_ramals").whereNotNull("local_instalacao_id"))
        .paginate(start, end);
    } else {
      residencias = await Database
        .select('id', 'moradia_numero', 'is_predio', 'predio_nome')
        .from('local_instalacaos')
        .where('rua_id', rua_id)
        .where("is_active", true)
        .whereNotIn('id', Database.select("local_instalacao_id").from("ligacao_ramals").whereNotNull("local_instalacao_id"))
        .where("moradia_numero", "like", "%" + search + "%")
        .paginate(start, end);
    }

    return residencias;
  }
}

module.exports = LocalInstalacao
