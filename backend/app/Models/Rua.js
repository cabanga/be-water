'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Rua extends Model {
  bairro() {
    return this.belongsTo('App/Models/Bairro')
  }

  quarteirao() {
    return this.belongsTo('App/Models/Quarteirao')
  }

  locais_instalacao() {
    return this.hasMany("App/Models/LocalInstalacao")
  }

  locais_de_consumo() {
    return this.hasMany("App/Models/LocalInstalacao")
      .select(
        'cil',
        'is_predio',
        'predio_nome',
        'predio_andar',
        'moradia_numero',
        'contaDescricao',
        'numero_conta',
        'local_consumo.id as local_consumo_id',
        'local_instalacaos.id as local_instalacao_id',
        'moradia_numero',
        'latitude',
        'longitude',
      )
      .innerJoin(
        'local_consumos as local_consumo',
        'local_consumo.local_instalacao_id', 'local_instalacaos.id',
      )
      .whereNotNull('contador_id')
      .innerJoin(
        'contas as conta',
        'local_consumo.conta_id', 'conta.id'
      )
      .distinct(
        'contaDescricao',
        'numero_conta',
        'latitude',
        'longitude'
      )
  }
}

module.exports = Rua
