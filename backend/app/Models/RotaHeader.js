'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class RotaHeader extends Model {
  municipio() {
    return this.belongsTo('App/Models/Municipio').with('distritos')
  }

  roteiros(){
    return this.hasMany('App/Models/RotaRun')
    .select(
      'rota_runs.id',
      'rota_header_id',
      'local_consumo_id',
      'dia_mes',

      'cil',
      'numero_serie',
      'cliente.nome as cliente',
      'cliente.telefone',

      "leitura.id AS leitura_id",
      "leitura.nao_leitura",
      "leitura.motivo",

      "leitura.leitura",
      "leitura.contador_id",
      "leitura.ultima_leitura",
      "leitura.data_leitura",
      "leitura.consumo",
      "leitura.estado",
      "leitura.foi_facturado",
      "leitura.data_ultima_leitura",

      "leitura.classe_tarifario_id",
      "leitura.tarifario_descricao"
    )
    .innerJoin(
      'estado_rotas as estado',
      'estado.id', 'rota_runs.estado_rota_id'
    )
    .innerJoin(
      'local_consumos as local_consumo',
      'local_consumo.id', 'rota_runs.local_consumo_id',
    )
    .innerJoin(
      'local_instalacaos as local_instalacao',
      'local_instalacao.id', 'local_consumo.local_instalacao_id'
    )

    .innerJoin(
      'leituras as leitura',
      'leitura.rota_run_id', 'rota_runs.id'
    )
    .innerJoin(
      'contadores as contador',
      'contador.id', 'leitura.contador_id'
    )
    .innerJoin(
      'clientes as cliente',
      'cliente.id', 'contador.cliente_id'
    )
    .where('leitura.foi_facturado', false)
    .where('estado.slug', 'REALIZADA')
  }

  charges(){
    return this.hasMany('App/Models/RotaRun')
    .select(
      'rota_runs.id',
      'rota_header_id',
      'local_consumo_id',
      'dia_mes',

      'cil',
      'numero_serie',
      'cliente.nome as cliente',
      'cliente.telefone',

      "leitura.id AS leitura_id",
      "leitura.nao_leitura",
      "leitura.motivo",

      "leitura.leitura",
      "leitura.contador_id",
      "leitura.ultima_leitura",
      "leitura.data_leitura",
      "leitura.consumo",
      "leitura.estado",
      "leitura.foi_facturado",
      "leitura.data_ultima_leitura",

      "leitura.classe_tarifario_id",
      "leitura.tarifario_descricao"
    )
    .innerJoin(
      'estado_rotas as estado',
      'estado.id', 'rota_runs.estado_rota_id'
    )
    .innerJoin(
      'local_consumos as local_consumo',
      'local_consumo.id', 'rota_runs.local_consumo_id',
    )
    .innerJoin(
      'local_instalacaos as local_instalacao',
      'local_instalacao.id', 'local_consumo.local_instalacao_id'
    )

    .innerJoin(
      'leituras as leitura',
      'leitura.rota_run_id', 'rota_runs.id'
    )
    .innerJoin(
      'contadores as contador',
      'contador.id', 'leitura.contador_id'
    )
    .innerJoin(
      'clientes as cliente',
      'cliente.id', 'contador.cliente_id'
    )
    .where('leitura.foi_facturado', true)
    .where('estado.slug', 'REALIZADA')

  }
}

module.exports = RotaHeader
