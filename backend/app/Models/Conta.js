'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Conta extends Model {
  moeda() {
    return this.belongsTo("App/Models/Moeda");
  }

  contrato() {
    return this.hasMany("App/Models/Contrato");
  }

}

module.exports = Conta
