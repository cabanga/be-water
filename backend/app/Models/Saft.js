'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Cliente = use("App/Models/Cliente");
const Imposto = use("App/Models/Imposto");
const Produto = use("App/Models/Produto");
const Empresa = use("App/Models/Empresa");
const LogBillRunSaft = use("App/Models/LogBillRunSaft");
const Factura = use("App/Models/Factura");
const Database = use("Database"); 
  
class Saft extends Model {

  static async saftXMLDocument(ano, de, para,auth) { 

    let impostos = null;
    let clientes = null;
    let produtos = null;
    const facturas = [];
    var empresa = null

    var fs = require("fs");
    
    var moment = require("moment");
    var data = moment(new Date()).format("YYYY-MM-DD");
    var hora = moment(new Date()).format("H");
    var min = moment(new Date()).format("m");
    var sec = moment(new Date()).format("s");

    let lbrs = null;

    lbrs = await Database.select("*").from("log_bill_run_safts").where("mes_de", de).where("mes_para", para).where("ano", ano).first();

    if (lbrs == null) {
        const count = await Database.from("facturas").where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y")'), ano).whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%m")'), [ de, para ]).getCount() // returns array 
        if (count == 0 || count == undefined) {
          return 404;
        } 

      lbrs = await LogBillRunSaft.create({
        mes_de: de,
        mes_para:para,
        ano: ano,   
        user_id: auth.user.id
      }); 
    }else{
      if (lbrs.estado == 0) {
        return 203;
      } 
    }

   

    
    const allFacturas = await Database.select(
      "facturas.cliente_id",
      "facturas.factura_sigla",
      "facturas.observacao",
      Database.raw(
        'DATE_FORMAT(facturas.created_at, "%Y-%m-%d") as created_at'
      ),Database.raw(
        'Concat(DATE_FORMAT(facturas.systemEntryDate, "%Y-%m-%d"),"T",DATE_FORMAT(facturas.systemEntryDate, "%T")) as SystemEntryDate'
      ),Database.raw(
        'Concat(DATE_FORMAT(facturas.created_at, "%Y-%m-%d"),"T",DATE_FORMAT(facturas.created_at, "%T")) as SystemEntryDateNotNull'
      ),
      "facturas.hash",
      "facturas.hash_control",
      "facturas.status",
      "facturas.status_date",
      "facturas.status_reason",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.user_id",
      "facturas.id",
      "facturas.numero",
      "facturas.numero_origem_factura",
      Database.raw(
        'DATE_FORMAT(facturas.data_origem_factura, "%Y-%m-%d") as data_origem_factura'
      ),
      "users.username as user",
      "clientes.nome as cliente",
      "series.nome as serie",
      "documentos.sigla"
    )
      .from("facturas")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("users", "users.id", "facturas.user_id")
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .whereIn("documentos.sigla", ["NC", "FT", "FR", "ND", "FS"])
      .where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y")'), ano)
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%m")'), [
        de,
        para
      ])


      
      

    if (allFacturas[0] == undefined) {
       await LogBillRunSaft.query().where("id",lbrs.id).update({ estado: -1, nome_saft: 'Nenhum Resultado encotrado.' });
      return 500;
    }
      clientes = await Database.select("*").from("clientes").whereIn("id",Database.distinct('facturas.cliente_id')
      .from("facturas").innerJoin("series", "series.id", "facturas.serie_id") 
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id").innerJoin("documentos", "documentos.id", "series.documento_id")
      .whereIn("documentos.sigla", ["NC", "FT", "FR", "ND","FS"]).where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y")'), ano)
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%m")'), [ de, para ]))
      //clientes = await Cliente.all();
      impostos = await Database.select("*").from("impostos");
      produtos = await Database.select("*").from("produtos").whereIn('id', Database.select("artigo_id").from("linha_facturas"));
      empresa = await Database.select("*").from("empresas").first();
     
    for (let oneFactura of allFacturas) {
      const linhaFacturas = await Database.select(
        "produtos.nome",
        "produtos.barcode",
        "produtos.valor as valorProduto",
        "produtos.tipo as tipoProduto",
        "produtos.barcode",
        "produtos.tipo as tipoProduto",
        "linha_facturas.valor",
        "linha_facturas.quantidade",
        "linha_facturas.total",
        "linha_facturas.valor_desconto",
        "linha_facturas.quantidade",
        "linha_facturas.artigo_id",
        "linha_facturas.imposto_id",
        "linha_facturas.linhaTotalSemImposto",
        "impostos.descricao as taxType",
        "impostos.codigo as codigoImposto",
        "impostos.valor as taxCode",
        "linha_facturas.valor_imposto as taxAmount"
      )
        .from("linha_facturas")
        .innerJoin("produtos", "produtos.id", "linha_facturas.artigo_id")
        .innerJoin("impostos", "impostos.id", "linha_facturas.imposto_id")
        .where("linha_facturas.factura_id", oneFactura.id);
      facturas.push({ oneFactura, linhaFacturas }); 
    }

    /*let data = {
      clientes: clientes,
      impostos: impostos,
      produtos: produtos,
      facturas: facturas
    };*/
    
 
    
    var xml = await Saft.gerarSaftXML(clientes, impostos, produtos, facturas, empresa,{mes_de: de, mes_para: para, ano:ano });
    var file = "saft-unig-"+lbrs.id+" "+data+" "+ano + "-" + de + " " + hora + "" + "" + min + "" + sec;
    
    var  dir = 'tmp' 
    if (!fs.existsSync(dir)){fs.mkdirSync(dir);fs.mkdirSync(dir+"/upload");fs.mkdirSync(dir+"/upload/safts")}
    
    fs.writeFile("tmp/upload/safts/"+file+".xml",xml, function(err) {
      if (err) throw err;                  
      return 200;
    });

    await LogBillRunSaft.query().where("id",lbrs.id).update({ estado: 1, nome_saft:file}); 
     
    return 200;
  }


 static async gerarSaftXML(clientes, impostos, produtos, facturas, empresa,saftDate) { 
 

    var builder = require('xmlbuilder');
    var numeral = require("numeral");
    var moment = require("moment");

    var produtosXML = [];
    var clientesXML = [];
    var impostosXML = [];
    var documentXML = [];
    var totalDebit = 0
    var totalCredit = 0
    let totalFacturasCounts = 0;

    var CurrentDate = new Date();

    var dataActual = moment(CurrentDate).format('YYYY') + "-" + moment(CurrentDate).format('MM')+"-"+moment(CurrentDate).format('DD'); 

    var Current = new Date(saftDate.ano+"-"+saftDate.mes_para + "-01");     
    var ultDia = new Date(Current.getFullYear(), Current.getMonth() + 1, 0); 
    var ultimoDia = moment(ultDia).format("DD"); 

    /*FOR: Produtos */
    produtos.forEach(element => {  
      produtosXML.push({
        ProductType: element.tipo == "Artigo" ? "P" : "S",
        ProductCode: element.id,
        ProductGroup: null,
        ProductDescription: element.nome,
        ProductNumberCode: element.id
      });
    });



    /*FOR: Clientes */
    var LineCustomerNumber = 0;
    clientes.forEach(element => {
      
      clientesXML.push(
        {
          'CustomerID': element.id,
          'AccountID': 'Desconhecido',
          'CustomerTaxID': (element.contribuente == '999999999' || element.contribuente == 0 || element.contribuente == null ? '999999999' : element.contribuente),
          'CompanyName': element.nome.substr(1,100),
          'Contact': element.contactPerson,

          'BillingAddress': {
            'BuildingNumber': element.buildingNumber,
            'StreetName': element.streetName,
            'AddressDetail': element.morada.slice(0, 198),
            'City': "" + (element.city == null ? 'Desconhecido': element.city),
            'PostalCode': 'Desconhecido',
            'Country': 'AO'
          },
          'Telephone': Saft.remove_str_space((element.telefone == null ? "" : element.telefone.toString().substr(1,9))),
          'Fax': null,
          'Email': element.email,
          'Website': null,
          'SelfBillingIndicator': 0
        }
      );
      if (clientesXML[LineCustomerNumber].Telephone == null || clientesXML[LineCustomerNumber].Telephone.length !== 9 ) {
        delete clientesXML[LineCustomerNumber].Telephone;
      }
      LineCustomerNumber++;
    });

    /*FOR: Impostos */
    impostos.forEach(element => { 
      impostosXML.push({
        TaxType: element.valor == 0 ? "IS" : "IVA",
        TaxCountryRegion: "AO",
        TaxCode: element.valor == 0 ? "ISE" : "NOR",
        Description: element.descricao,
        TaxPercentage: element.valor,
        TaxAmount: null
      });
    });
    

    /* FOR: DOCUMENTOS */
    for (let index = 0; index < facturas.length; index++) {
      const element = facturas[index];
        
      totalFacturasCounts++
      var LineNumber = 0;
      var lineFacturaXML = [];
      var OrderReferences = (element.oneFactura.sigla == 'NC' || element.oneFactura.sigla == 'ND' ? { 'OriginatingON': element.oneFactura.numero_origem_factura, 'OrderDate': element.oneFactura.data_origem_factura } : null )

      totalDebit += (element.oneFactura.sigla == 'NC' && element.oneFactura.status =='N' ? element.oneFactura.total : 0.00)
      totalCredit += ((element.oneFactura.sigla == 'FT' || element.oneFactura.sigla == 'FR' || element.oneFactura.sigla == 'ND' || element.oneFactura.sigla === 'FS') && element.oneFactura.status =='N' ? element.oneFactura.total : 0.00)
      //var taxPayableLine = 0;
      //var netTotal = 0;
      
      element.linhaFacturas.forEach(line => {
        //taxPayableLine += line.taxAmount;
        //netTotal = line.linhaTotalSemImposto;
        lineFacturaXML.push({
          'LineNumber': (LineNumber + 1), // 1, 2, 3, 4 ...
          'OrderReferences': OrderReferences,
          'ProductCode': line.artigo_id,
          'ProductDescription': line.nome,
          'Quantity': line.quantidade,
          'UnitOfMeasure': 'Unidade',
          'UnitPrice': numeral(line.valor).format("0.00"),
          'TaxPointDate': element.oneFactura.created_at,
          'Description': line.nome,
          'DebitAmount': (element.oneFactura.sigla === 'FT' || element.oneFactura.sigla === 'FR' || element.oneFactura.sigla === 'ND' || element.oneFactura.sigla === 'FS' ? '0' : numeral(line.total).format("0.00")), // Factura - 0
          'CreditAmount': (element.oneFactura.sigla === 'NC'  ? '0' : numeral(line.total).format("0.00")),// Nota de credito - 0 
          'Tax': {
            'TaxType': 'IVA',//line.taxType.toUpperCase(),
            'TaxCountryRegion': 'AO',
            'TaxCode': (line.taxCode == 0 ? 'ISE' : 'NOR'),
            'TaxPercentage': line.taxCode,
            'TaxAmount': null
          },
          'TaxExemptionReason': line.taxType,
          'TaxExemptionCode': line.codigoImposto,
          'SettlementAmount': numeral(line.valor_desconto).format("0.00"), //total do desconto
        })


        if (lineFacturaXML[LineNumber].DebitAmount == '0') {
          delete lineFacturaXML[LineNumber].DebitAmount;
        }
        if (lineFacturaXML[LineNumber].CreditAmount == '0') {
          delete lineFacturaXML[LineNumber].CreditAmount;
        }
        if (lineFacturaXML[LineNumber].Tax.TaxPercentage != 0) {
          delete lineFacturaXML[LineNumber].TaxExemptionReason;
          delete lineFacturaXML[LineNumber].TaxExemptionCode;
        }



        LineNumber++;
      }); 
      console.log(element.oneFactura.SystemEntryDate, element.oneFactura.factura_sigla,);
      documentXML.push({
        InvoiceNo: element.oneFactura.factura_sigla, // FT ou NC - documento A - serie (FT)
        ATCUD: 0,
        DocumentStatus: {
          InvoiceStatus: element.oneFactura.status,
          InvoiceStatusDate: element.oneFactura.status_date.replace('.000Z', '').replace(' ', 'T'),
          Reason: element.oneFactura.status_reason,
          SourceID: element.oneFactura.user, // id do user que fez alteração do documento
          SourceBilling: "P"
        },
        Hash: element.oneFactura.hash,
        HashControl: element.oneFactura.hash_control,
        InvoiceDate: element.oneFactura.created_at,
        InvoiceType: element.oneFactura.sigla, // FT ou NC (Factura ou note de Credito)
        SpecialRegimes: {
          SelfBillingIndicator: "0",
          CashVATSchemeIndicator: "0",
          ThirdPartiesBillingIndicator: "0"
        },
        SourceID: element.oneFactura.user, // id user que fez a factura
        SystemEntryDate: (element.oneFactura.SystemEntryDate==null? element.oneFactura.SystemEntryDateNotNull : element.oneFactura.SystemEntryDate.toString()).toString(), // moment(element.oneFactura.status_date).format("YYYY-MM-DDTHH:mm:ss"),// element.oneFactura.status_date_data+"T"+element.oneFactura.status_date_hora,
        CustomerID: element.oneFactura.cliente_id,
        /* FOR: DOCUMENTOS LINHAS*/
        Line: lineFacturaXML,
        DocumentTotals: {
          TaxPayable: numeral(element.oneFactura.totalComImposto).format("0.00"),//numeral(taxPayableLine).format("0.00"),  // , // total da soma das linhas de todos os impostos
          NetTotal: numeral(element.oneFactura.totalSemImposto).format("0.00"),//numeral(netTotal).format("0.00"),//, // subTotal da soma das linhas sem impostos
          GrossTotal: numeral(element.oneFactura.total).format("0.00")
        }
      });
    }

    var feedObj = {
      'AuditFile': {
        /*START HEADER */
        'Header': { 

          'AuditFileVersion': '1.01_01',
          'CompanyID': ""+empresa.id,
          'TaxRegistrationNumber': ""+empresa.taxRegistrationNumber,
          'TaxAccountingBasis': 'F',
          'CompanyName': ''+empresa.companyName,
          'CompanyAddress': {
            'StreetName': ""+empresa.city,
            'AddressDetail': ""+empresa.addressDetail,
            'City': 'Luanda',
            'PostalCode': '0',
            'Province': 'Luanda',
            'Country': 'AO'
          },
          'FiscalYear': ''+saftDate.ano,
          'StartDate': saftDate.ano+'-'+saftDate.mes_de+'-01',
          'EndDate': saftDate.ano+'-'+saftDate.mes_para+'-'+ultimoDia,
          'CurrencyCode': 'AOA',
          'DateCreated': dataActual,
          'TaxEntity': 'Global',
          'ProductCompanyTaxID': '5417139211',
          'SoftwareValidationNumber': '4/AGT/2019',
          'ProductID': 'UNIG/ITGEST',
          'ProductVersion': '01.01.010',
          'Telephone': '944581845'
        }, /*END HEADER */
        'MasterFiles': {
          /*FOR: Clientes*/
          'Customer': clientesXML,

          'Product': produtosXML,
          /*FOR: IMPOSTOS*/
          'TaxTable': { 'TaxTableEntry': impostosXML }
        },
        /* FOR: DOCUMENTOS */
        'SourceDocuments': {
          
          'SalesInvoices': {
            'NumberOfEntries': totalFacturasCounts,
            'TotalDebit': numeral(totalDebit).format("0.00"),
            'TotalCredit': numeral(totalCredit).format("0.00"),
            'Invoice': documentXML
          }
        }

      }
    }

    

    var feed = builder.create(feedObj, { encoding: 'utf-8' })
    this.saft = feed.end({ pretty: true }); 

    return this.saft;
  }


  /*static async _saftXMLDocument(ano, de, para,auth){ 
    const empresa = await Empresa.query().first();
    const produtos = await Produto.query().fetch();
    const impostos = await Imposto.query().fetch(); 
    /*const clientes = await Database.from('clientes').whereExists(function () {
      this.distinct('cliente_id').from('facturas').where('cliente.id', 'facturas.cliente_id')
      .where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y")'), ano)
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%m")'), [de,para]) 
    })*/

    /*const clientes = Cliente.query(builder => {
      builder.innerJoin('facturas',"facturas.cliente_id","clientes.id")
      .where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y")'), ano)
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%m")'), [de,para]) 
     }).fetch()*
    //.hasFactura().where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y")'), ano)
    //.whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%m")'), [de,para]).fetch()
 
     const facturas = Factura.query()
      .with('serie').with('serie.documento').with('cliente').with('user').with('lines')
      .where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y")'), ano)
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%m")'), [de,para])
      .fetch(); 
    return facturas 
  }*/

  /**
   * Show a list of all focalpoints.
   * GET focalpoints
   *
   * @author paulino esperança paulino.paulo@gitgest.pt
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async getStreamSaft(id) {
    
    const anexo = await LogBillRunSaft.find(id)
    //return file as stream
    response.attachment(Helpers.tmpPath('./upload/' + anexo.nome_saft+".xml")) 
  }

 static remove_str_space(str) {  
    var tamanho    = str.length;
    var novaString = "";
    for( var i = 0; i < tamanho; i++ ) { 
      if( str.charAt(i) != " " ) {                    
        novaString += str.charAt(i);                            
      }
    }
    return novaString
  }
}

module.exports = Saft
