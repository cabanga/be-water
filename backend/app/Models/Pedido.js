'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database");  

class Pedido extends Model {

    static async pedidoStore(conta_id, contrato_id, tarifario_id, local_instalacao_id, user) {
/* 
        const estado_ordem_servico = await Database.select("id")
            .table("estado_ordem_servicos")
            .where("sigla", "ABERTA").first(); */

        const pedido = await Pedido.create({
            conta_id: conta_id,
            contrato_id: contrato_id,
            //tecnologia_id: tecnologia_id,
            tarifario_id: tarifario_id,
            //tipo_ordem_servico_id: tipo_ordem_servico_id,
            local_instalacao_id: local_instalacao_id,
            estado_ordem_servico_id: 1,
            user_id: user
        });

        return pedido;
    }
}

module.exports = Pedido
