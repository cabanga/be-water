'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database")
class LogsContadore extends Model {

    async registrarLogContador(user_id, operacao, tabela_origem, contador_id, ultima_leitura, tipo_registo_id, estado_contador_id_old, estado_contador_id, observacao_old, observacao_new, local_instalacao_id, local_consumo_id, conta_id) {
        const origem_id = await Database.select(
            "id"
        ).from(tabela_origem)
            .where("id", contador_id).first()
        await LogsContadore.create({
            'contador_id': contador_id,
            'user_id': user_id,
            'id_origem': origem_id.id,
            'tabela': tabela_origem,
            'operacao': operacao,
            'ultima_leitura': ultima_leitura,
            'tipo_registo_id': tipo_registo_id,
            'estado_contador_id_old': estado_contador_id_old,
            'estado_contador_id': estado_contador_id,
            'observacao_old': observacao_old,
            'observacao_new': observacao_new,
            'local_instalacao_id': local_instalacao_id,
            'local_consumo_id': local_consumo_id,
            'conta_id': conta_id
        })
    }
}

module.exports = LogsContadore
