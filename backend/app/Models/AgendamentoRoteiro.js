'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class AgendamentoRoteiro extends Model {
  roteiro() {
    return this.belongsTo("App/Models/RotaRun")
    .select(
      'rota_runs.id',
      'rota_header_id',
      'local_consumo_id',
      'dia_mes',

      'cil',
      'is_predio',
      'predio_nome',
      'predio_andar',
      'moradia_numero',

      'rota_header.descricao as nome_rota',
      'leitor.nome as nome_leitor'
    )
    .innerJoin(
      'local_consumos as local_consumo',
      'local_consumo.id', 'rota_runs.local_consumo_id'
    )
    .innerJoin(
      'local_instalacaos as local_instalacao',
      'local_instalacao.id', 'local_consumo.local_instalacao_id',
    )


    .innerJoin(
      'rota_headers as rota_header',
      'rota_header.id', 'rota_runs.rota_header_id'
    )
    .innerJoin(
      'users as leitor',
      'leitor.id', 'rota_header.leitor_id'
    )

  }
}

module.exports = AgendamentoRoteiro


