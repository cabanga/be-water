'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const LinhaFacturaUtilitie = use("App/Models/LinhaFacturaUtilitie");
const Factura = use("App/Models/Factura");
const LinhaFactura = use("App/Models/LinhaFactura");
const BillRun = use("App/Models/BillRun");
const Database = use("Database");
var moment = require("moment");
var numeral = require("numeral");
const Charge = use("App/Models/Charge");
const ChargeInterconexao = use("App/Models/ChargeInterconexao");

class FacturaUtilitie extends Model {

static gearAss(string) {
    const crypto = require("crypto");
    const fs = require("fs");
    var s = crypto.createSign("RSA-SHA1");
    var key =
      "-----BEGIN RSA PRIVATE KEY-----\n" +
      "MIICXQIBAAKBgQC4faWshk9wvZUouz4A3K4Zzb2NOtbp262HcB1mJYF1QDs3wAnd\n" +
      "kGiqPcBx7TGeIEjuBtg6DFtSy29w1dRCANdqIDqaCqX+/PNE8dz8foCauiy5OEU2\n" +
      "segqAeN3X8PXBevqGThd/x9OPJ4pV2Kgx/oAs7Bwg3/C2AM3qraj0UulhwIDAQAB\n" +
      "AoGAW0RlQk0LXaWb9ZNzn++L/V3niMdz7Crt1JOlJ5QkUAHfibvp5X78GEQGQRXr\n" +
      "NuOX0JD4RPc58mKLldFieOh7p8B/dx8UZyWd11TUOnVwOSJaFd3rwnHzobEUJgH2\n" +
      "24b1bGOWsk+0XEisS1B7xl4d8T74+Dpnpugg4nU/1rAKgjECQQDfpe5Nihi9Fgfz\n" +
      "rcr8s9oGGdKV7nyVXUmBN5Dm5PMfAev49Wo6ZvhO9EW1mb15Kuqfc56Sq5ErDdRg\n" +
      "MPnODP8JAkEA0y2nOcjWn3ZsX0lPvGpKotnFUgO4WlpJfd6fzxTfQrLqHf6ixFPt\n" +
      "wTApqhU0fx9xMWl6m1Kh0WiegMYk8LwUDwJATafsCvh8hotzz2T1KrG4bo3g1Tau\n" +
      "A58Uus10fvfYg1fDe/qbHBRM+/1NhzUO2VfRh/Q5h2wTSAPRTmUzGBzjIQJBAIQw\n" +
      "z70cOz0WpEABZChNYOsP5rSwH3ZvjhF8igzWw+q8lFCyVLEQ2INV4r7VB0eMJw8H\n" +
      "N/iCgUjUdGOnpPgMw4ECQQCboFsTKkzrLOkZJiipgid08xPiBJCfd5Pjl7ggnwoj\n" +
      "CL9JXAsSBvOTvTuo2XnBHOpaWO8oOEUt5xBbUPGLhdaA\n" +
      "-----END RSA PRIVATE KEY-----";
    s.update(string);
    var signature = s.sign(key, "base64");
    return signature;
  }
  

static async facturar(factura,facturaMesAuto_id,brh, auth) {
    let linhas = []; 

    var servicos = factura.servicos;
    var conta_id = factura.conta.id;
    var cliente = factura.conta.cliente_id;
    var totalComImposto = factura.totalComImposto; // total da soma das linhas de todos os impostos
    var totalSemImposto = factura.totalSemImposto; // subTotal da soma das linhas sem impostos
    var total = factura.total;
    var serie_id = factura.serie; 
    var moeda = factura.moeda; 
    
     
  

    var empresa = await Database.select("*").from("empresas").where("empresas.id", auth.user.empresa_id).first();

    const isConvertLineFactura =true// empresa.isConvertLineFactura;

    const serie = await Database.select(
      "series.id as id",
      "series.nome",
      "series.proximo_numero",
      "series.activo",
      "series.movimento",
      "series.tipo_movimento",
      "documentos.nome as documento",
      "documentos.sigla",
      "series.documento_id"
    ).from("series").innerJoin("documentos", "documentos.id", "series.documento_id").where("series.id", serie_id);

    var valor_cativacao = 0;

    if (serie[0].sigla == "FT") {

    var cat = await Database.select('tipo_entidade_cativadoras.nome as valor').from('entidade_cativadoras')
        .leftJoin('tipo_entidade_cativadoras','tipo_entidade_cativadoras.id','entidade_cativadoras.tipo_entidade_cativadora_id')
        .leftJoin('clientes','clientes.id','entidade_cativadoras.cliente_id').where('clientes.id', cliente).first();
      
        valor_cativacao = (cat == null ? 0: Number(cat.valor))
        
    }

  

    var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    var doc_date = moment(new Date()).format("YYYY-MM-DD");
    var doc_red =moment(new Date()).format("YYYY-MM-DD") +"T" +moment(new Date()).format("HH:mm:ss");

    var l_hash =
      doc_date +
      ";" +
      doc_red +
      ";" +
      serie[0].sigla +
      " " +
      serie[0].nome +
      "/" +
      serie[0].proximo_numero +
      ";" +numeral(total).format("0.00") +";";

    const max_n = await Database.from("facturas")
      .where("serie_id", serie_id)
      .max("numero as total");
    var cc = 0;

    if (max_n[0].total == null) {
      cc = 0;
    } else {
      cc = max_n[0].total;
    }

    if (cc > 0) {
      const doc_ant = await Database.select("hash as hash")
        .from("facturas")
        .where("serie_id", serie_id)
        .where("numero", cc);
      l_hash = l_hash + doc_ant[0].hash;
    }
 

    const fact = await Factura.create({ 
      numero: serie[0].proximo_numero,
      factura_sigla: serie[0].sigla + " " + serie[0].nome + "/" + serie[0].proximo_numero,
      serie_id: serie[0].id,
      totalComImposto: moeda_iso != "AOA" && cambio != null && moeda_iso != null ? numeral(totalComImposto).format("0.00") / cambio.valor_cambio  : numeral(totalComImposto).format("0.00"),
      totalSemImposto: moeda_iso != "AOA" && cambio != null && moeda_iso ? numeral(totalSemImposto).format("0.00") / cambio.valor_cambio : numeral(totalSemImposto).format("0.00"),
      total:  moeda_iso != "AOA" && cambio != null && moeda_iso ? numeral(total).format("0.00") / cambio.valor_cambio : numeral(total).format("0.00"),
      valor_aberto:  (serie[0].sigla == "FR"  ? 0 : moeda_iso != "AOA" && cambio != null &&  moeda_iso && isConvertLineFactura == true ? numeral(total - (totalComImposto * (valor_cativacao/100))).format("0.00") / cambio.valor_cambio : numeral(total - (totalComImposto * (valor_cativacao/100))).format("0.00")),
      valor_cativo: valor_cativacao,
      imposto_cativo: (totalComImposto * (valor_cativacao/100)),
      pago: 0,
      status: "N",
      status_date: data,  
      hash: FacturaUtilitie.gearAss(l_hash),
      hash_control: "1", //l_hash,
      cliente_id: cliente,
      conta_id:conta_id,
      user_id: auth.user.id,
      moeda_id: moeda, 
      cambio_id: cambio_id,
      valor_cambio: valor_cambio,
      moeda_iso: moeda_iso,
      is_iplc:true,
      facturacao:'POS-PAGO'
    });
 
    for (let index = 0; index < servicos.length; index++) {
        linhas[0] = await LinhaFactura.create({
        servico_id: servicos[index].servico_id,
        artigo_id : servicos[index].artigo_id,
        factura_id: fact.id,
        user_id: auth.user.id,
        valor: moeda_iso != "AOA" && cambio != null && moeda_iso ? numeral(servicos[index].valor).format("0.00") / cambio.valor_cambio : numeral(servicos[index].valor).format("0.00"),
        quantidade: servicos[index].quantidade,
        total:  moeda_iso != "AOA" && cambio != null && moeda_iso ? numeral(servicos[index].total).format("0.00") / cambio.valor_cambio : numeral(servicos[index].total).format("0.00"),
        linhaTotalSemImposto:  moeda_iso != "AOA" && cambio != null && moeda_iso ? numeral(servicos[index].linhaTotalSemImposto).format("0.00") / cambio.valor_cambio : numeral(servicos[index].linhaTotalSemImposto).format("0.00"),
        valor_imposto:  moeda_iso != "AOA" && cambio != null &&  moeda_iso  ? numeral(servicos[index].valorImposto).format("0.00") / cambio.valor_cambio : numeral(servicos[index].valorImposto).format("0.00"),
        valor_desconto:  moeda_iso != "AOA" &&  cambio != null &&  moeda_iso ? numeral(servicos[index].desconto).format("0.00") / cambio.valor_cambio: numeral(servicos[index].desconto).format("0.00"),
        imposto_id: servicos[index].imposto_id,
        cambio_id: cambio_id,
        valor_cambio: valor_cambio,
        moeda_id:servicos[index].moeda_id,
        moeda_iso: isConvertLineFactura == true ? moeda_iso : "AOA"
      });

      if (servicos[index].charge_id != null) {
        await Charge.query().where("id",servicos[index].charge_id).update({ is_facturado: 1});
      }
 
    }
    await Database.table("series").where("id", serie[0].id).update("proximo_numero", Number(serie[0].proximo_numero) + 1);
    await BillRun.create({
      facturaMesAuto_id: facturaMesAuto_id,
      bill_run_header_id: brh,
      conta_id: conta_id,
      factura_utilitie_id: fact.id
    });

     
    return true;
  }


static async facturarICT(factura,facturaMesAuto_id,brh, auth) {
    let linhas = []; 

    var  servicos = factura.servicos;
    var conta_id = factura.conta.id;
    var cliente = factura.conta.cliente_id;
    var minutos_interconexao = factura.minutos_interconexao;
    var totalComImposto = factura.totalComImposto; // total da soma das linhas de todos os impostos
    var totalSemImposto = factura.totalSemImposto; // subTotal da soma das linhas sem impostos
    var total = factura.total;
    var serie_id = factura.serie; 
    var moeda = factura.moeda;  
 

    const serie = await Database.select(
      "series.id as id",
      "series.nome",
      "series.proximo_numero",
      "series.activo",
      "series.movimento",
      "series.tipo_movimento",
      "documentos.nome as documento",
      "documentos.sigla",
      "series.documento_id"
    ).from("series").innerJoin("documentos", "documentos.id", "series.documento_id").where("series.id", serie_id);
  
    var valor_cativacao = 0;

    if (serie[0].sigla == "FT") {
    var cat = await Database.select('tipo_entidade_cativadoras.nome as valor').from('entidade_cativadoras')
        .leftJoin('tipo_entidade_cativadoras','tipo_entidade_cativadoras.id','entidade_cativadoras.tipo_entidade_cativadora_id')
        .leftJoin('clientes','clientes.id','entidade_cativadoras.cliente_id').where('clientes.id', cliente).first();      
        valor_cativacao = (cat == null ? 0: Number(cat.valor))        
    }

    var cambio_id = null;
    var valor_cambio = null;
    var moeda_iso = null;

    if (moeda != null) {
      var m = await Database.select("*").from("moedas").where("moedas.id", moeda).first();
      moeda_iso = m.codigo_iso;      
    }

    var factura_sigla = serie[0].sigla + " " + serie[0].nome + "/" + serie[0].proximo_numero;
      
    var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss"); 
    var InvoiceDate = moment(new Date()).format("YYYY-MM-DD");
    var SystemEntryDate =  moment(new Date()).format("YYYY-MM-DD") +"T"+moment(new Date()).format("HH:mm:ss");
    var InvoiceNo = factura_sigla; //serie[0].sigla+" "+serie[0].nome+"/"+serie[0].proximo_numero;
    var GrossTotal = numeral(total).format("0.00");
    //InvoiceDate, SystemEntryDate, InvoiceNo, GrossTotal e Hash

    var l_hash = InvoiceDate+";"+SystemEntryDate+";"+InvoiceNo+";"+GrossTotal+";";

    const max_n = await Database.from("facturas").where("serie_id", serie_id).max("numero as total");
    var cc = 0;

    if (max_n[0].total == null) {
      cc = 0;
    } else {
      cc = max_n[0].total;
    }

    if (cc != 0) {
      const doc_ant = await Database.select("hash as hash").from("facturas").where("serie_id", serie_id).where("numero", cc).first();
      l_hash = l_hash + "" + doc_ant.hash;
    }
 

    const fact = await Factura.create({
      numero: serie[0].proximo_numero,
      factura_sigla: serie[0].sigla + " " + serie[0].nome + "/" + serie[0].proximo_numero,
      serie_id: serie[0].id,
      totalComImposto: numeral(totalComImposto).format("0.00"),
      totalSemImposto: numeral(totalSemImposto).format("0.00"),
      total: numeral(total).format("0.00"),
      valor_aberto:  numeral(total - (totalComImposto * (valor_cativacao/100))).format("0.00"),
      valor_cativo: valor_cativacao,
      imposto_cativo: (totalComImposto * (valor_cativacao/100)),
      pago: 0,//serie[0].sigla == "FR" ? true : false,
      status: "N",
      status_date: data,
      hash: FacturaUtilitie.gearAss(l_hash),
      hash_control: "1", //l_hash,
      cliente_id: cliente,
      conta_id: conta_id,
      user_id: auth,
      moeda_id: moeda,
      cambio_id: cambio_id,
      valor_cambio: valor_cambio,
      moeda_iso: moeda_iso,
      is_iplc: true,
      facturacao: "POS-PAGO",
      minutos_interconexao: minutos_interconexao,
      systemEntryDate: data,
    });
 
    for (let index = 0; index < servicos.length; index++) {
        linhas[0] = await LinhaFactura.create({
        servico_id: servicos[index].servico_id,
        artigo_id : servicos[index].artigo_id,
        factura_id: fact.id,
        user_id: auth,
        valor: numeral(servicos[index].valor).format("0.00"),
        quantidade: servicos[index].quantidade,
        total:   numeral(servicos[index].total).format("0.00"), 
        linhaTotalSemImposto:   numeral(servicos[index].linhaTotalSemImposto).format("0.00"),
        valor_imposto: numeral(servicos[index].valorImposto).format("0.00"),
        valor_desconto: numeral(servicos[index].desconto).format("0.00"),
        imposto_id: servicos[index].imposto_id, 
        cambio_id: cambio_id,
        valor_cambio: valor_cambio,
        moeda_id:servicos[index].moeda_id,
        moeda_iso: moeda_iso 
      });

      if (servicos[index].charge_id != null) {
        await ChargeInterconexao.query().where("id",servicos[index].charge_id).update({ is_facturado: 1});
      } 
       
    }
    await Database.table("series").where("id", serie[0].id).update("proximo_numero", Number(serie[0].proximo_numero) + 1);
    await BillRun.create({ 
      bill_run_header_id: brh,
      conta_id: conta_id,
      factura_utilitie_id: fact.id
    });

     
    return fact.id;
  }

 



}

module.exports = FacturaUtilitie
