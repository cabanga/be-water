'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const Model = use('Model');
const Database = use("Database");
const LocalInstalacao = use("App/Models/LocalInstalacao");
var moment = require("moment");

class LocalInstalacaoHistorico extends Model {

    static operacao = {
        CREATED: 'Criação',
        UPDATE: 'Actualização',
        UPDATE_FROM_CONTRATO: 'Actualização em Contrato'
    }

    static async save(item) {

        const result = await LocalInstalacaoHistorico.create({
            operacao: this.operacao.CREATED,
            local_instalacao_id: item.local_instalacao.id,
            historico: null,
            actualizacao: JSON.stringify(item.local_instalacao),
            user_id: item.user_id
        });

        return result;
    }

    static async update(item) {

        //console.log('hist' + item.historico);

        const result = await LocalInstalacaoHistorico.create({
            operacao: this.operacao.UPDATE,
            local_instalacao_id: item.local_instalacao_id,
            historico: JSON.stringify(item.historico),
            actualizacao: JSON.stringify(item.actualizacao),
            user_id: item.user_id
        });

        return result;

    }

    static async updateFromContrato(item) {

        var dataActual = moment(new Date()).format("YYYY-MM-DD h:mm:ss");

        await LocalInstalacao.query()
            .where('id', item.local_instalacao_id)
            .update({
                instalacao_sanitaria_qtd: item.actualizacao.instalacao_sanitaria_qtd,
                reservatorio_flag: item.actualizacao.reservatorio_flag,
                reservatorio_capacidade: item.actualizacao.reservatorio_capacidade,
                piscina_flag: item.actualizacao.piscina_flag,
                piscina_capacidade: item.actualizacao.piscina_capacidade,
                jardim_flag: item.actualizacao.jardim_flag,
                campo_jardim_id: item.actualizacao.campo_jardim_id,
                poco_alternativo_flag: item.actualizacao.poco_alternativo_flag,
                fossa_flag: item.actualizacao.fossa_flag,
                fossa_capacidade: item.actualizacao.fossa_capacidade,
                acesso_camiao_flag: item.actualizacao.acesso_camiao_flag,
                anexo_flag: item.actualizacao.anexo_flag,
                anexo_quantidade: item.actualizacao.anexo_quantidade,
                caixa_contador_flag: item.actualizacao.caixa_contador_flag,
                estado_caixa_contador_id: item.actualizacao.estado_caixa_contador_id,
                abastecimento_cil_id: item.actualizacao.abastecimento_cil_id,

                updated_at: dataActual
            });

        const result = await LocalInstalacaoHistorico.create({
            operacao: this.operacao.UPDATE_FROM_CONTRATO,
            local_instalacao_id: item.local_instalacao_id,
            historico: JSON.stringify(item.historico),
            actualizacao: JSON.stringify(item.actualizacao),
            user_id: item.user_id
        });

        return result;
    }
}

module.exports = LocalInstalacaoHistorico
