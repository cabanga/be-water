'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class RotaRun extends Model {

  local_consumo() {
    return this.belongsTo("App/Models/LocalConsumo")
    .select(
      'local_consumos.contador_id',
      'local_consumos.local_instalacao',
      'local_consumos.conta_id'
    )
  }

  periodicidade() {
    return this.belongsTo("App/Models/PeriodicidadeRoteiro")
  }

  agendamento(){
    return this.belongsTo("App/Models/RotaHeader")
  }

}

module.exports = RotaRun
