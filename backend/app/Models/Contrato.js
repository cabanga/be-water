'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Contrato extends Model {

    conta() {
        return this.belongsTo("App/Models/Conta");
    }


    tarifa(){
        return this.belongsTo("App/Models/Tarifario");
    }
    local_consumo(){
        return this.belongsTo("App/Models/LocalConsumo",'contrato_idj')
    }
    contador(){
        return this.belongsTo("App/Models/LocalConsumo")
        .innerJoin('contadores','contadores.id','local_consumos.contador_id');
    }

    

}

module.exports = Contrato
