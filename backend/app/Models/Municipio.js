'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Municipio extends Model {
  provincia() {
    return this.belongsTo("App/Models/Provincia");
  }

  distritos() {
    return this.belongsTo("App/Models/Distrito");
  }

  locais_de_consumo() {
    return this.hasMany("App/Models/Bairro")
      .select(
        'cil',
        'is_predio',
        'predio_nome',
        'predio_andar',
        'moradia_numero',

        'contaDescricao',
        'numero_conta',
        'local_consumo.id as local_consumo_id',
        'local_instalacao.id as local_instalacao_id',
        'moradia_numero',
        'latitude',
        'longitude',
      )
      .innerJoin(
        'ruas as rua',
        'rua.bairro_id', 'bairros.id'
      )
      .innerJoin(
        'local_instalacaos as local_instalacao',
        'local_instalacao.rua_id', 'rua.id'
      )
      .innerJoin(
        'local_consumos as local_consumo',
        'local_consumo.local_instalacao_id', 'local_instalacao.id',
      )
      .whereNotNull('contador_id')
      .innerJoin(
        'contas as conta',
        'local_consumo.conta_id', 'conta.id'
      )
      .distinct(
        'contaDescricao',
        'numero_conta',
        'latitude',
        'longitude'
      )
  }

}

module.exports = Municipio
