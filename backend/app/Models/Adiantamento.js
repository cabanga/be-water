"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");
const Database = use("Database");

class Adiantamento extends Model {
    
  /**
   * Show a list of all adiantamentos.
   * GET adiantamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  static async index({ cliente }) {
      const adList =  Adiantamento.query().where('cliente_id', cliente)
      return adList;
  } 

  static async AdiantamentoFactura(cliente){    
     const adList = await Database.select(
       "movimento_adiantamentos.id",
       "movimento_adiantamentos.saldado",
       "movimento_adiantamentos.valor",
       "movimento_adiantamentos.adiantamento_id",
       "adiantamentos.valor as saldo_contabilistico"
     )
       .from("movimento_adiantamentos")
       .innerJoin(
         "adiantamentos",
         "adiantamentos.id",
         "movimento_adiantamentos.adiantamento_id"
       )
       .where("cliente_id", cliente)
       .where("saldado", 0);
    return adList;
  }

  /**
   * Create/save a new adiantamento.
   * POST adiantamentos
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  static async store({ request, auth }) {
    const { valor, descricao, cliente_id } = request.all();
    const ad = await Adiantamento.create({
      valor: valor,
      descricao: descricao,
      cliente_id: cliente_id,
      user_id: auth.user.id
    });

    return ad;
  }

  /**
   * Display a single adiantamento.
   * GET adiantamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  static async show({ params, request }) {}

  /**
   * Update adiantamento details.
   * PUT or PATCH adiantamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  static async update({ params, valor }) {

    // update with new data entered
    const ad = await Adiantamento.where("id", params).update({ valor: valor });
    return ad;

  }

  /**
   * Delete a adiantamento with id.
   * DELETE adiantamentos/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  static async destroy({ params, request, response }) {}

  movimentosAdiantamentos() {
    return this.hasMany("App/Models/movimentoAdiantamento");
  }

}

module.exports = Adiantamento;
