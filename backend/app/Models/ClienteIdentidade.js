'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ClienteIdentidade extends Model {
  tipo_identidade() {
    return this.belongsTo("App/Models/TipoIdentidade");
  }
}

module.exports = ClienteIdentidade
