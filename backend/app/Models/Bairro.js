'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Bairro extends Model {
  municipio() {
    return this.belongsTo('App/Models/Municipio')
  }

  quarteiroes() {
    return this.hasMany("App/Models/Quarteirao");
  }

  locais_de_consumo() {
    return this.hasMany("App/Models/Rua")
      .select(
        'cil',
        'is_predio',
        'predio_nome',
        'predio_andar',
        'moradia_numero',

        'contaDescricao',
        'numero_conta',
        'local_consumo.id as local_consumo_id',
        'local_instalacao.id as local_instalacao_id',
        'moradia_numero',
        'latitude',
        'longitude'
      )
      .innerJoin(
        'local_instalacaos as local_instalacao',
        'local_instalacao.rua_id', 'ruas.id'
      )
      .innerJoin(
        'local_consumos as local_consumo',
        'local_consumo.local_instalacao_id', 'local_instalacao.id',
      )
      .whereNotNull('contador_id')
      .innerJoin(
        'contas as conta',
        'local_consumo.conta_id', 'conta.id'
      )
      .distinct(
        'contaDescricao',
        'numero_conta',
        'latitude',
        'longitude'
      )
  }

}

module.exports = Bairro




/*
'use strict'

//@type {typeof import('@adonisjs/lucid/src/Lucid/Model')}
const Model = use('Model')
const Rua = use('App/Models/Rua')

class Bairro extends Model {
  municipio() {
    return this.belongsTo('App/Models/Municipio')
  }

  quarteiroes() {
    return this.hasMany("App/Models/Quarteirao");
  }

  static async locais_de_consumo(locais_ids) {
    return Rua.query()
      .select(
        'contaDescricao',
        'numero_conta',
        'local_consumo.id as local_consumo_id',
        'local_instalacao.id as local_instalacao_id',
        'moradia_numero',
        'latitude',
        'longitude'
      )
      .innerJoin(
        'local_instalacaos as local_instalacao',
        'local_instalacao.rua_id', 'ruas.id'
      )
      .innerJoin(
        'local_consumos as local_consumo',
        'local_consumo.local_instalacao_id', 'local_instalacao.id',
      )
      .whereNotIn('local_consumo.id', locais_ids)
      .whereNotNull('contador_id')
      .innerJoin(
        'contas as conta',
        'local_consumo.conta_id', 'conta.id'
      )
      .distinct(
        'contaDescricao',
        'numero_conta',
        'latitude',
        'longitude'
      )
      .fetch()
  }

}

module.exports = Bairro
*/
