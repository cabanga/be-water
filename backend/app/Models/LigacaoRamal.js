'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database");

class LigacaoRamal extends Model {

    static async store(ponto_a_id, ligacao, auth ) {

        let res = null;

        for (let index = 0; index < ligacao.linhas.length; index++) {

            
              const diamentro = ligacao.linhas[index].slug == "OBJECTO-DO-RAMAL" ? ligacao.linhas[index].diamentro : ligacao.linhas[index].slug == "LOCAL-INSTALACAO" ? ligacao.linhas[index].diamentro : ligacao.linhas[index].diamentro;
              const profundidade = ligacao.linhas[index].slug == "OBJECTO-DO-RAMAL" ? ligacao.linhas[index].profundidade : ligacao.linhas[index].slug == "LOCAL-INSTALACAO" ? ligacao.linhas[index].profundidade : ligacao.linhas[index].profundidade;
              const comprimento = ligacao.linhas[index].slug == "OBJECTO-DO-RAMAL" ? ligacao.linhas[index].comprimento : ligacao.linhas[index].slug == "LOCAL-INSTALACAO" ? ligacao.linhas[index].comprimento : ligacao.linhas[index].comprimento;
      
              res = await LigacaoRamal.create({
                diamentro: diamentro,
                profundidade: profundidade,
                comprimento: comprimento,
                ponto_a_id: ponto_a_id,
                ponto_b_id: ligacao.linhas[index].slug == "OBJECTO-DO-RAMAL" ? ligacao.linhas[index].codigo : null,
                local_instalacao_id: ligacao.linhas[index].slug == "LOCAL-INSTALACAO" ? ligacao.linhas[index].codigo : null,
                user_id: auth.user.id
              });
      }

      return res;
    }
}

module.exports = LigacaoRamal
