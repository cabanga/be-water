"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");
const Database = use("Database");
class Relatorio extends Model {


  static async relatorioFacturacaoRealizadaCobrancaGlobal(filter) {

    var direccao = "";
    var produto = "";
    var loja = "";
    if (filter.direccao != "T") {
      direccao += " AND d.designacao = '" + filter.direccao + "'";
    }
    if (filter.produto != "T") {
      produto +=
        " AND id IN (SELECT factura_id FROM linha_facturas WHERE artigo_id = " +
        filter.produto +
        ")";
    }
    if (filter.loja != "T") {
      loja +=
        " AND caixa_id IN (SELECT caixas.id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    let raw
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      raw = await Database.raw(
        "SELECT facturacao,d.designacao as direccao, SUM(total) total, mes FROM (SELECT facturacao, DATE_FORMAT(created_at, '%m') as mes, SUM(total) as total, cliente_id FROM facturas WHERE DATE_FORMAT(created_at, '%Y') = " +
        filter.ano +
        " AND  status = 'N' " +
        loja +
        " " +
        produto +
        "  GROUP BY DATE_FORMAT(created_at, '%m'), facturacao, cliente_id ORDER BY DATE_FORMAT(created_at, '%m')) ft, clientes c,direccaos d WHERE c.direccao_id = d.id AND ft.cliente_id = c.id AND c.direccao_id IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.municipe == 'T' ? " " : " AND c.municipio_id = " + filter.municipe) + " " +
        " GROUP BY c.direccao_id, mes, facturacao ORDER BY c.direccao_id, mes"
      );
    } else {
      raw = await Database.raw(
        "SELECT facturacao, d.designacao as direccao, SUM(total) total, mes FROM (SELECT f.facturacao, DATE_FORMAT(f.created_at, '%m') as mes, SUM(f.total) as total, f.cliente_id FROM facturas f WHERE DATE_FORMAT(f.created_at, '%Y') = '" +
        filter.ano + "' AND  f.status = 'N' " + loja + " " + produto +
        "  GROUP BY DATE_FORMAT(f.created_at, '%m'), f.facturacao, f.cliente_id ORDER BY DATE_FORMAT(f.created_at, '%m')) ft, clientes c, direccaos d WHERE c.direccao_id = d.id AND  ft.cliente_id = c.id AND c.direccao_id IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.municipe == 'T' ? " " : " AND c.municipio_id = " + filter.municipe) + " " +
        " GROUP BY c.direccao_id, mes, facturacao ORDER BY c.direccao_id, mes"
      );



    }

    var facturacaos = [];
    var meses = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 }
    ];

    var meses2 = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 },
      { nome_mes: "d", numero_mes: "13", valor: 0 }
    ];

    let d = raw[0];

    for (let i = 0; i < d.length; i++) {
      const element = d[i];
      if (facturacaos.length == 0) {
        facturacaos.push({ tipoFacturacao: element.facturacao, direccaos: [] });
      } else {
        var v = 0;
        for (let j = 0; j < facturacaos.length; j++) {
          const e = facturacaos[j];
          if (e.tipoFacturacao === element.facturacao) {
            v = 1;
          }
        }
        if (v == 0) {
          facturacaos.push({
            tipoFacturacao: element.facturacao,
            direccaos: []
          });
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < d.length; i++) {
        const element = d[i];
        if (elef.tipoFacturacao === element.facturacao) {
          if (facturacaos[f].direccaos.length == 0) {
            facturacaos[f].direccaos.push({
              direccao: element.direccao,
              saldos: [],
              total: 0
            });
          } else {
            var v = 0;
            for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
              const e = facturacaos[f].direccaos[j];
              if (e.direccao == element.direccao) {
                v = 1;
              }
            }
            if (v == 0) {
              facturacaos[f].direccaos.push({
                direccao: element.direccao,
                saldos: [],
                total: 0
              });
            }
          }
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < elef.direccaos.length; i++) {
        const element = elef.direccaos[i];
        for (let j = 0; j < d.length; j++) {
          const el = d[j];
          if (
            element.direccao === el.direccao &&
            elef.tipoFacturacao === el.facturacao
          ) {
            var mes = "";
            for (let index = 0; index < meses.length; index++) {
              const ele = meses[index];
              if (ele.numero_mes === el.mes) {
                mes = ele.nome_mes;
              }
            }
            facturacaos[f].direccaos[i].saldos.push({
              nome_mes: mes,
              numero_mes: el.mes,//"" + (el.mes < 10 && filter.tipoFacturacao == 'POS-PAGO' ? "0" + el.mes : el.mes),
              valor: el.total
            });
          }
        }
      }
    }

    for (let i = 0; i < meses.length; i++) {
      const mes = meses[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          var v = 0;
          const element = facturacaos[f].direccaos[j].saldos;
          for (let k = 0; k < element.length; k++) {
            const el = element[k];

            if (el.numero_mes === mes.numero_mes) {
              v = 1;
              facturacaos[f].direccaos[j].total += el.valor;
            }
          }
          if (v == 0) {
            facturacaos[f].direccaos[j].saldos.push(meses[i]);
          }
        }
      }
    }
    //return facturacaos;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let index = 0; index < facturacaos[f].direccaos.length; index++) {
        facturacaos[f].direccaos[index].saldos.sort(function (a, b) {
          return a.numero_mes - b.numero_mes;
        });
      }
    }

    for (let i = 0; i < meses2.length; i++) {
      var total = 0;
      const element = meses2[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          for (let s = 0; s < facturacaos[f].direccaos[j].saldos.length; s++) {
            if (element.numero_mes == facturacaos[f].direccaos[j].saldos[s].numero_mes) {
              total += facturacaos[f].direccaos[j].saldos[s].valor;
            }
          }
        }
      }
      meses2[i].valor = total;
    }

    total = 0;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
        total += facturacaos[f].direccaos[j].total;
      }
    }

    meses2[12].valor = total;

    var mesesCalculoHorizontal = meses2;

    return { facturacaos, mesesCalculoHorizontal };
  }

  static async relatorioFacturacaoRealizadaCobrancaGlobalPago(filter) {
    var direccao = "";
    var produto = "";
    var loja = "";
    if (filter.direccao != "T") {
      direccao += " AND c.direccao = '" + filter.direccao + "'";
    }
    if (filter.produto != "T") {
      produto +=
        " AND id IN (SELECT factura_id FROM linha_facturas WHERE artigo_id = " +
        filter.produto +
        ")";
    }
    if (filter.loja != "T") {
      loja +=
        " AND caixa_id IN (SELECT id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    let raw
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      raw = await Database.raw(
        "SELECT facturacao, direccao, SUM(total) total, mes FROM (SELECT facturacao, DATE_FORMAT(created_at, '%m') as mes, SUM(total) as total, cliente_id FROM facturas WHERE  DATE_FORMAT(created_at, '%Y') = " +
        filter.ano +
        " AND  status = 'N' " +
        loja +

        " " +
        produto +

        " GROUP BY DATE_FORMAT(created_at, '%m'), facturacao, cliente_id ORDER BY DATE_FORMAT(created_at, '%m')) ft, clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );
    } else {
      raw = await Database.raw(
        "SELECT facturacao, direccao, SUM(total) total, mes FROM (SELECT f.facturacao, brh.mes as mes, SUM(f.total) as total, f.cliente_id FROM facturas f, bill_runs br, bill_run_headers brh WHERE f.id = br.factura_utilitie_id AND br.bill_run_header_id = brh.id AND brh.ano = " +
        filter.ano + " AND  status = 'N' " + loja + " " + produto +
        "  GROUP BY   brh.mes, f.facturacao, f.cliente_id ORDER BY brh.mes) ft, clientes c WHERE ft.cliente_id = c.id IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );



    }

    var facturacaos = [];
    var meses = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 }
    ];

    var meses2 = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 },
      { nome_mes: "d", numero_mes: "13", valor: 0 }
    ];

    let d = raw[0];

    for (let i = 0; i < d.length; i++) {
      const element = d[i];
      if (facturacaos.length == 0) {
        facturacaos.push({ tipoFacturacao: element.facturacao, direccaos: [] });
      } else {
        var v = 0;
        for (let j = 0; j < facturacaos.length; j++) {
          const e = facturacaos[j];
          if (e.tipoFacturacao === element.facturacao) {
            v = 1;
          }
        }
        if (v == 0) {
          facturacaos.push({
            tipoFacturacao: element.facturacao,
            direccaos: []
          });
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < d.length; i++) {
        const element = d[i];
        if (elef.tipoFacturacao === element.facturacao) {
          if (facturacaos[f].direccaos.length == 0) {
            facturacaos[f].direccaos.push({
              direccao: element.direccao,
              saldos: [],
              total: 0
            });
          } else {
            var v = 0;
            for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
              const e = facturacaos[f].direccaos[j];
              if (e.direccao == element.direccao) {
                v = 1;
              }
            }
            if (v == 0) {
              facturacaos[f].direccaos.push({
                direccao: element.direccao,
                saldos: [],
                total: 0
              });
            }
          }
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < elef.direccaos.length; i++) {
        const element = elef.direccaos[i];
        for (let j = 0; j < d.length; j++) {
          const el = d[j];
          if (
            element.direccao === el.direccao &&
            elef.tipoFacturacao === el.facturacao
          ) {
            var mes = "";
            for (let index = 0; index < meses.length; index++) {
              const ele = meses[index];
              if (ele.numero_mes === el.mes) {
                mes = ele.nome_mes;
              }
            }
            facturacaos[f].direccaos[i].saldos.push({
              nome_mes: mes,
              numero_mes: "" + (el.mes < 10 && filter.tipoFacturacao == 'PRE-PAGO' ? "0" + el.mes : el.mes),
              valor: el.total
            });
          }
        }
      }
    }

    for (let i = 0; i < meses.length; i++) {
      const mes = meses[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          var v = 0;
          const element = facturacaos[f].direccaos[j].saldos;
          for (let k = 0; k < element.length; k++) {
            const el = element[k];

            if (el.numero_mes === mes.numero_mes) {
              v = 1;
              facturacaos[f].direccaos[j].total += el.valor;
            }
          }
          if (v == 0) {
            facturacaos[f].direccaos[j].saldos.push(meses[i]);
          }
        }
      }
    }
    //return facturacaos;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let index = 0; index < facturacaos[f].direccaos.length; index++) {
        facturacaos[f].direccaos[index].saldos.sort(function (a, b) {
          return a.numero_mes - b.numero_mes;
        });
      }
    }

    for (let i = 0; i < meses2.length; i++) {
      var total = 0;
      const element = meses2[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          for (let s = 0; s < facturacaos[f].direccaos[j].saldos.length; s++) {
            if (element.numero_mes == facturacaos[f].direccaos[j].saldos[s].numero_mes) {
              total += facturacaos[f].direccaos[j].saldos[s].valor;
            }
          }
        }
      }
      meses2[i].valor = total;
    }

    total = 0;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
        total += facturacaos[f].direccaos[j].total;
      }
    }

    meses2[12].valor = total;

    var mesesCalculoHorizontal = meses2;

    return { facturacaos, mesesCalculoHorizontal };
  }
  static async relatorioLoja(filter) {
    let datas = null;
    console.log(filter)
    datas = await Database.select(
      "facturas.id as factura_id",
      "facturas.numero",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.status",
      "facturas.pago",
      "lojas.nome as loja",
      "facturas.valor_aberto",
      "facturas.is_iplc",
      "facturas.factura_sigla",
      "facturas.numero_origem_factura",
      "facturas.data_origem_factura",
      "facturas.data_vencimento",
      "facturas.status_date",
      "facturas.created_at",
      Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d") as data'),
      "clientes.id as cliente_id",
      "clientes.nome",
      "clientes.telefone",
      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla"
    )
      .from("facturas")
      .leftJoin("clientes", "clientes.id", "facturas.cliente_id")
      .leftJoin("series", "series.id", "facturas.serie_id")
      .leftJoin("documentos", "documentos.id", "series.documento_id")
      .leftJoin("caixas", "caixas.id", "facturas.caixa_id")
      .leftJoin("lojas", "lojas.id", "caixas.loja_id")
      .leftJoin("adiantamentos", "adiantamentos.cliente_id", "clientes.id")
      .leftJoin("municipios", "clientes.municipio_id", "municipios.id")
      .whereIn("facturas.pago", filter.estado == "T" ? [1, 0] : [filter.estado])
      .where(function () {
        if (filter.filial!="T") {
          this.where("municipios.id", filter.filial)
          if(filter.loja != "T" ){
            this.where("lojas.id", filter.loja)
          }
        }
      })
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .orderBy("clientes.nome");

    return datas;
  }

  /* static async relatorioDetalhada(filter) {
    var data =
      filter.ano + "-" + filter.mes + "-" + (filter.dia < 10 ? "0" + filter.dia : filter.dia);
    let datas = null;
    datas = await Database.select(
      "facturas.id as factura_id",
      "facturas.numero",
      "facturas.total",
      "facturas.valor_aberto",
      "facturas.is_iplc",
      "facturas.factura_sigla",
      Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d") as data'),
      "clientes.id as cliente_id",
      "clientes.nome",
    )
      .from("facturas")
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
      .whereIn("facturas.pago", filter.estado == "T" ? [1, 0] : [filter.estado])
      .whereIn("facturas.user_id", filter.loja == "T" ? Database.select("id").from("users") : Database.select("id").from("users").where("loja_id", filter.loja))
      .where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), data)
      .orderBy("clientes.nome");

    return datas;
  } */

  static async relatorioIVA(filter) {
    var data = filter.ano + "-" + filter.mes + "-" + (filter.dia < 10 ? "0" + filter.dia : filter.dia);
    let datas = null;

    datas = await Database.select(
      "facturas.id as factura_id",
      "facturas.factura_sigla",
      "facturas.numero",
      "facturas.total",
      "facturas.totalComImposto",
      "facturas.totalSemImposto",
      "facturas.status",
      "facturas.pago as pago",
      Database.raw("IF(facturas.pago,'Pago','Não Pago')as pagos"),
      "facturas.valor_aberto",
      "facturas.factura_sigla",
      "facturas.data_vencimento",
      "facturas.status_date",
      "facturas.created_at",
      Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d") as data'),
      "clientes.id as cliente_id",
      "clientes.nome",
      "clientes.telefone",
      "clientes.contribuente",
      "series.nome as serie",
      "documentos.nome as documento",
      "documentos.sigla",
      "clientes.direccao"
    )
      .from("facturas")
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .whereIn("facturas.pago", filter.estado == "T" ? [1, 0] : [filter.estado])
      .whereIn("clientes.direccao", filter.direccao == "T" ? [1, 0] : [filter.direccao])
      .whereIn("documentos.id", filter.documento == "T" ? Database.raw('SELECT id FROM documentos') : [filter.documento])
      .where(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), data)

    return datas;
  }

  static async relatorioMovimentosCaixa(filter) {

    var loja = ""
    var operador = ""
    if (filter.loja != "T") {
      loja += " AND l.id = '" + filter.loja + "'";
    }
    if (filter.operador != "T") {
      operador += " AND u.id = '" + filter.operador + "'";
    }

    let datas = await Database.raw("SELECT u.nome as operador,l.nome as loja ,c.valor_abertura as valor_abertura,c.is_active,DATE_FORMAT(c.data_abertura,'%d-%m-%Y') as data_abertura,c.hora_abertura,c.hora_fecho,c.valor_fecho,DATE_FORMAT(c.data_fecho,'%d-%m-%Y') as data_fecho,c.valor_vendas,c.valor_deposito,c.data_deposito,b.numero_conta,c.referencia_banco " +
      " from  caixas c LEFT join bancos b on c.banco_id=b.id LEFT join lojas l on c.loja_id=l.id  LEFT JOIN users u on c.user_id=u.id" +
      " where date_format(c.created_at, '%Y-%m-%d') BETWEEN '" + filter.data1 + "' AND '" + filter.data2 + "'" + loja + "' AND '" + operador + "ORDER BY l.nome"

    );

    return datas[0];
  }
  static async relatorioDetalhada(filter) {
    let datas = null;
    datas = await Database.select(
      "facturas.id as factura_id",
      "facturas.numero",
      "facturas.total",
      "facturas.status",
      Database.raw("IF(facturas.status,'ativo','inativo') as statu"),
      "facturas.valor_aberto",
      "facturas.pago",
      Database.raw("IF(facturas.pago,'sim','não') as pagos"),
      "facturas.factura_sigla",
      "facturas.numero_origem_factura",
      "facturas.data_origem_factura",
      "facturas.data_vencimento",
      "facturas.status_date",
      "facturas.created_at",
      Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d") as data'),
      Database.raw('DATE_FORMAT(facturas.created_at, "%m") as mes'),
      "clientes.id as cliente_id",
      "clientes.nome as cliente",
      "clientes.telefone",
      "clientes.morada",
      "lojas.nome as loja",
      "users.nome as user",
      "classe_tarifarios.descricao"
    )
      .from("facturas")
      .leftJoin("clientes", "clientes.id", "facturas.cliente_id")
      .leftJoin("contratoes", "contratoes.id", "facturas.contrato_id")
      .leftJoin("contas", "contratoes.conta_id", "contas.id")
      .leftJoin("classe_tarifarios", "contratoes.classe_tarifario_id", "classe_tarifarios.id")
      .leftJoin("caixas", "caixas.id", "facturas.caixa_id")
      .leftJoin("lojas", "lojas.id", "caixas.loja_id")
      .leftJoin("users", "users.id", "caixas.user_id")
      .where(Database.raw("date_format(facturas.created_at, '%Y-%m-%d') BETWEEN '" + filter.data1 + "' AND '" + filter.data2 + "' AND (facturas.factura_sigla LIKE '%"+'FT'+"%'OR facturas.factura_sigla LIKE '%"+'FR'+"%'OR facturas.factura_sigla LIKE '%"+ 'NC'+"%')"))

       return datas;

  }

  static async relatorioCobranca(filter) {
    let datas = null;
    datas = await Database.select(
      "clientes.id",
      "clientes.nome",
      "clientes.morada",
      "clientes.telefone",
      "clientes.email",
      "lojas.nome as loja",
      "users.nome as user",
      "classe_tarifarios.descricao",
      "recibos.recibo_sigla",
      Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d") as data' ),
      "recibos.total",
      "recibos.status",
      Database.raw("IF(facturas.status,'ativo','inativo') as statu"),
    )
      .sum("facturas.total as totals")
      .from("recibos")
      .leftJoin("linha_recibos", "linha_recibos.recibo_id", "recibos.id")
      .leftJoin("facturas", "facturas.id", "linha_recibos.factura_id")
      .leftJoin("clientes", "facturas.cliente_id", "clientes.id")
      .leftJoin("contratoes", "contratoes.id", "facturas.contrato_id")
      .leftJoin("contas", "contratoes.conta_id", "contas.id")
      .leftJoin("classe_tarifarios", "contratoes.classe_tarifario_id", "classe_tarifarios.id")
      .leftJoin("users", "users.id", "recibos.user_id")
      .leftJoin("lojas", "lojas.id", "users.loja_id")
      .whereBetween(Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d")'), [filter.data1, filter.data2])
      .groupByRaw("recibos.id,clientes.id,facturas.id")

    return datas;
  }


  static async relatorioServicoContratados(filter) {


    let datas = await Database.raw("SELECT f.conta_id as conta, p.nome as produto, s.chaveServico as nome_servico, f.servico_id as numero_servico, s.dataEstado as data_servico, e.nome as nome_estado_servicos, SUM(lf.total) as mensalidade " +
      "FROM servicos s, produtos p, facturas f, linha_facturas lf, estado_servicos e WHERE f.servico_id = s.id AND s.estado = e.id AND  lf.factura_id =  f.id AND lf.artigo_id = p.id AND f.cliente_id = " + filter.cliente_id + " " +

      " " + (filter.estado == "T" ? "" : " AND e.id = " + filter.estado) + " " +
      " " + (filter.conta == "T" ? '' : " AND f.conta_id =" + filter.conta) + "" +
      " " + (filter.servico == "T" ? '' : " AND f.servico_id =" + filter.servico) + "" +
      " " + (filter.produto == "T" ? '' : " AND lf.artigo_id =" + filter.produto) + "" +
      " GROUP BY lf.artigo_id, f.servico_id, f.conta_id");

    return datas[0];
  }


  static async relatorioClients(request) {
    const { search, orderBy, pagination, filter } = request.all();
    let res = null;
    const unificado = 'nao';


    res = await Database.select(
      "clientes.id",
      "clientes.nome",
      "clientes.telefone",
      "clientes.email",
      "clientes.genero_id",
      "generos.descricao as genero",
      "clientes.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id as provincia_id",
      "clientes.morada",
      "clientes.tipo_identidade_id",
      "tipo_identidades.nome as tipo_identidade",
      /*       "tipo_identidades.numero_digitos", */
      "clientes.numero_identificacao",
      /*       "clientes.numero_entidade", */
      "clientes.numero_cliente",
      "clientes.tipo_cliente_id",
      "tipo_clientes.tipoClienteDesc as tipo_cliente",
      /*       "tipo_clientes.slug as tipo_cliente_slug", */
      "clientes.direccao_id",
      "direccaos.designacao as direccao",
      "clientes.gestor_conta_id",
      "gestor_contas.nome as gestor_conta",
      /*       "gestor_clientes.telefone as gestor_cliente_telefone", */
      "clientes.is_active",
      "clientes.created_at",
      "clientes.updated_at"
    )
      .from("clientes")
      .leftJoin("generos", "generos.id", "clientes.genero_id")
      .leftJoin("municipios", "municipios.id", "clientes.municipio_id")
      .leftJoin("tipo_identidades", "tipo_identidades.id", "clientes.tipo_identidade_id")
      .leftJoin("tipo_clientes", "tipo_clientes.id", "clientes.tipo_cliente_id")
      .leftJoin("direccaos", "direccaos.id", "clientes.direccao_id")
      .leftJoin("gestor_contas", "gestor_contas.id", "clientes.gestor_conta_id")

      .whereIn("clientes.id ", Database.raw("SELECT id FROM clientes WHERE "
        + " " + (filter.email == "S" ? " email IS NULL " : " email IS NOT NULL")
        + " " + (filter.gestor_conta == "S" ? " AND  gestor_conta_id IS NULL " : (filter.gestor_conta == "T" || filter.gestor_conta == null ? '' : " AND gestor_conta_id = '" + filter.gestor_conta + "'"))
        + " " + (filter.genero == "S" ? " AND  genero_id IS NULL " : (filter.genero == "T" || filter.genero == null ? '' : " AND genero_id = '" + filter.genero + "'"))
        + " " + (filter.tipo_identidade == "S" ? " AND  tipo_identidade_id IS NULL " : (filter.tipo_identidade == "T" || filter.tipo_identidade == null ? '' : " AND tipo_identidade_id = '" + filter.tipo_identidade + "'"))
        + " " + (filter.tipo_cliente == "S" ? " AND  tipo_cliente_id IS NULL " : (filter.tipo_cliente == "T" || filter.tipo_cliente == null ? '' : " AND tipo_cliente_id = '" + filter.tipo_cliente + "'"))
        + " " + (filter.direccao == "S" ? " direccao_id IS NULL" : (filter.direccao == "T" || filter.direccao == null ? '' : "AND direccao_id = '" + filter.direccao + "'"))
        + "  " + (search == null ? " " : "  clientes.nome  LIKE '%" + search + "%'"
          /*           + " OR clientes.contribuente  LIKE '%" + search + "%' OR clientes.id  LIKE '%" + search + "%' OR clientes.numeroExterno  LIKE '%" + search + "%'"
                    + " OR clientes.morada  LIKE '%" + search + "%' OR clientes.telefone  LIKE '%" + search + "%' OR clientes.email  LIKE '%" + search + "%'" */
          + " OR DATE_FORMAT(clientes.created_at, '%Y-%m-%d')  LIKE '%" + search + "%'"
        )))
      /*       .where("clientes.unificado", unificado) */
      /*       .orderBy(orderBy == null ? "clientes.created_at" : orderBy, orderBy == "created_at" ? "DESC" : "ASC") */
      .orderBy(orderBy == null ? "clientes.nome" : orderBy, "ASC")
    /* .paginate( pagination.page == null ? 1 : pagination.page, pagination.perPage); */
    /* console.log(filter); */
    return res;
  }





  static async relatorioFacturacaopagamentoGlobal(filter) {

    var direccao = "";
    var produto = "";
    var loja = "";
    if (filter.direccao != "T") {
      direccao += " AND c.direccao = '" + filter.direccao + "'";
    }
    if (filter.produto != "T") {
      produto +=
        " AND id IN (SELECT factura_id FROM linha_facturas WHERE artigo_id = " +
        filter.produto +
        ")";
    }
    if (filter.loja != "T") {
      loja +=
        " AND caixa_id IN (SELECT id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    let raw
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      raw = await Database.raw(
        "SELECT facturacao, c.direccao as direccao, SUM(total) total, mes FROM (SELECT cliente.direccao AS direccao,facturas.facturacao,ANY_VALUE(facturas.cliente_id) as cliente_id,date_format(facturas.created_at, '%m') AS mes, sum(facturas.total) AS total FROM facturas LEFT JOIN series serie_id ON facturas.serie_id = serie_id.id LEFT JOIN clientes cliente ON facturas.cliente_id = cliente.id WHERE (facturas.status = 'N' AND facturas.pago = TRUE AND facturas.facturacao = 'PRE-PAGO' AND date_format(facturas.created_at, '%Y')='" + filter.ano + "')  AND facturas.facturacao ='" + filter.tipoFacturacao + "' " + loja + " " + produto +
        "  GROUP BY DATE_FORMAT(facturas.created_at, '%m'),direccao) ft ,municipios m, clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.municipe == 'T' ? " " : " AND c.province = " + filter.municipe) + " " +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );
    } else {
      raw = await Database.raw(
        "SELECT facturacao, c.direccao as direccao, SUM(total) total, mes FROM (SELECT cliente.direccao AS direccao,facturas.facturacao, bill_run_header_id.mes AS mes,cliente.id cliente_id, sum(facturas.total) AS total FROM facturas LEFT JOIN bill_runs bill_runs ON facturas.id = bill_runs.factura_utilitie_id LEFT JOIN bill_run_headers bill_run_header_id ON bill_runs.bill_run_header_id = bill_run_header_id.id LEFT JOIN clientes cliente ON facturas.cliente_id = cliente.id LEFT JOIN series serie_id ON facturas.serie_id = serie_id.id WHERE (facturas.facturacao = 'POS-PAGO' AND facturas.status = 'N' AND bill_run_header_id.ano ='" + filter.ano + "' AND facturas.serie_id <> 58)  AND facturas.facturacao ='" + filter.tipoFacturacao + "' " + loja + " " + produto + "  GROUP BY cliente.direccao, bill_run_header_id.mes, cliente_id) ft , clientes c WHERE ft.cliente_id = c.id AND c.direccao IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + direccao + " " + (filter.municipe == 'T' ? " " : " AND c.province = " + filter.municipe) + " " +
        " GROUP BY c.direccao, mes, facturacao ORDER BY c.direccao, mes"
      );

    }

    var facturacaos = [];
    var meses = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 }
    ];

    var meses2 = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 },
      { nome_mes: "d", numero_mes: "13", valor: 0 }
    ];

    let d = raw[0];

    for (let i = 0; i < d.length; i++) {
      const element = d[i];
      if (facturacaos.length == 0) {
        facturacaos.push({ tipoFacturacao: element.facturacao, direccaos: [] });
      } else {
        var v = 0;
        for (let j = 0; j < facturacaos.length; j++) {
          const e = facturacaos[j];
          if (e.tipoFacturacao === element.facturacao) {
            v = 1;
          }
        }
        if (v == 0) {
          facturacaos.push({
            tipoFacturacao: element.facturacao,
            direccaos: []
          });
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < d.length; i++) {
        const element = d[i];
        if (elef.tipoFacturacao === element.facturacao) {
          if (facturacaos[f].direccaos.length == 0) {
            facturacaos[f].direccaos.push({
              direccao: element.direccao,
              saldos: [],
              total: 0
            });
          } else {
            var v = 0;
            for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
              const e = facturacaos[f].direccaos[j];
              if (e.direccao == element.direccao) {
                v = 1;
              }
            }
            if (v == 0) {
              facturacaos[f].direccaos.push({
                direccao: element.direccao,
                saldos: [],
                total: 0
              });
            }
          }
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < elef.direccaos.length; i++) {
        const element = elef.direccaos[i];
        for (let j = 0; j < d.length; j++) {
          const el = d[j];
          if (
            element.direccao === el.direccao &&
            elef.tipoFacturacao === el.facturacao
          ) {
            var mes = "";
            for (let index = 0; index < meses.length; index++) {
              const ele = meses[index];
              if (ele.numero_mes === el.mes) {
                mes = ele.nome_mes;
              }
            }
            facturacaos[f].direccaos[i].saldos.push({
              nome_mes: mes,
              numero_mes: "" + (el.mes < 10 && filter.tipoFacturacao == 'POS-PAGO' ? "0" + el.mes : el.mes),
              valor: el.total
            });
          }
        }
      }
    }

    for (let i = 0; i < meses.length; i++) {
      const mes = meses[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          var v = 0;
          const element = facturacaos[f].direccaos[j].saldos;
          for (let k = 0; k < element.length; k++) {
            const el = element[k];

            if (el.numero_mes === mes.numero_mes) {
              v = 1;
              facturacaos[f].direccaos[j].total += el.valor;
            }
          }
          if (v == 0) {
            facturacaos[f].direccaos[j].saldos.push(meses[i]);
          }
        }
      }
    }
    //return facturacaos;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let index = 0; index < facturacaos[f].direccaos.length; index++) {
        facturacaos[f].direccaos[index].saldos.sort(function (a, b) {
          return a.numero_mes - b.numero_mes;
        });
      }
    }

    for (let i = 0; i < meses2.length; i++) {
      var total = 0;
      const element = meses2[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          for (let s = 0; s < facturacaos[f].direccaos[j].saldos.length; s++) {
            if (element.numero_mes == facturacaos[f].direccaos[j].saldos[s].numero_mes) {
              total += facturacaos[f].direccaos[j].saldos[s].valor;
            }
          }
        }
      }
      meses2[i].valor = total;
    }

    total = 0;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
        total += facturacaos[f].direccaos[j].total;
      }
    }

    meses2[12].valor = total;

    var mesesCalculoHorizontal = meses2;

    return { facturacaos, mesesCalculoHorizontal };
  }

  static async relatorioFacturacaoporGestorGlobal(filter) {

    var direccao = "";
    var produto = "";
    var loja = "";
    var gestor = "";
    if (filter.gestor != "T") {
      gestor += " AND c.gestor_conta = '" + filter.gestor + "'";
    }
    if (filter.direccao != "T") {
      direccao += " AND c.direccao = '" + filter.direccao + "'";
    }
    if (filter.produto != "T") {
      produto +=
        " AND id IN (SELECT factura_id FROM linha_facturas WHERE artigo_id = " +
        filter.produto +
        ")";
    }
    if (filter.loja != "T") {
      loja +=
        " AND caixa_id IN (SELECT id FROM caixas WHERE loja_id = " +
        filter.loja +
        ")";
    }

    let raw
    if (filter.tipoFacturacao == 'PRE-PAGO') {
      raw = await Database.raw(
        "SELECT facturacao, c.gestor_conta as direccao, SUM(total) total, mes FROM (SELECT facturacao, DATE_FORMAT(created_at, '%m') as mes, SUM(total) as total, cliente_id FROM facturas WHERE DATE_FORMAT(created_at, '%Y') = " +
        filter.ano +
        " AND  status = 'N' " +
        loja +
        " " +
        produto +
        "  GROUP BY DATE_FORMAT(created_at, '%m'), facturacao, cliente_id ORDER BY DATE_FORMAT(created_at, '%m')) ft, clientes c WHERE ft.cliente_id = c.id AND c.gestor_conta IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + gestor + " " + (filter.municipe == 'T' ? " " : " AND c.province = " + filter.municipe) + " " +
        " GROUP BY c.gestor_conta, mes, facturacao ORDER BY c.gestor_conta, mes"
      );
    } else {
      raw = await Database.raw(
        "SELECT facturacao,c.gestor_conta as direccao, SUM(total) total, mes FROM (SELECT f.facturacao, brh.mes as mes, SUM(f.total) as total, f.cliente_id FROM facturas f, bill_runs br, bill_run_headers brh WHERE f.id = br.factura_utilitie_id AND br.bill_run_header_id = brh.id AND brh.ano = " +
        filter.ano + " AND  status = 'N' " + loja +
        "  GROUP BY   brh.mes, f.facturacao, f.cliente_id ORDER BY brh.mes) ft, clientes c WHERE  ft.cliente_id = c.id AND c.gestor_conta IS NOT NULL" +
        " AND facturacao = '" + filter.tipoFacturacao + "' " + gestor + " " + (filter.municipe == 'T' ? " " : " AND c.province = " + filter.municipe) + " " +
        " GROUP BY c.gestor_conta, mes, facturacao ORDER BY c.gestor_conta, mes"
      );



    }

    var facturacaos = [];
    var meses = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 }
    ];

    var meses2 = [
      { nome_mes: "Janeiro", numero_mes: "01", valor: 0 },
      { nome_mes: "Fevereiro", numero_mes: "02", valor: 0 },
      { nome_mes: "Março", numero_mes: "03", valor: 0 },
      { nome_mes: "Abril", numero_mes: "04", valor: 0 },
      { nome_mes: "Maio", numero_mes: "05", valor: 0 },
      { nome_mes: "Junho", numero_mes: "06", valor: 0 },
      { nome_mes: "Julho", numero_mes: "07", valor: 0 },
      { nome_mes: "Agosto", numero_mes: "08", valor: 0 },
      { nome_mes: "Setembro", numero_mes: "09", valor: 0 },
      { nome_mes: "Outobro", numero_mes: "10", valor: 0 },
      { nome_mes: "Novembro", numero_mes: "11", valor: 0 },
      { nome_mes: "Dezembro", numero_mes: "12", valor: 0 },
      { nome_mes: "d", numero_mes: "13", valor: 0 }
    ];

    let d = raw[0];

    for (let i = 0; i < d.length; i++) {
      const element = d[i];
      if (facturacaos.length == 0) {
        facturacaos.push({ tipoFacturacao: element.facturacao, direccaos: [] });
      } else {
        var v = 0;
        for (let j = 0; j < facturacaos.length; j++) {
          const e = facturacaos[j];
          if (e.tipoFacturacao === element.facturacao) {
            v = 1;
          }
        }
        if (v == 0) {
          facturacaos.push({
            tipoFacturacao: element.facturacao,
            direccaos: []
          });
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < d.length; i++) {
        const element = d[i];
        if (elef.tipoFacturacao === element.facturacao) {
          if (facturacaos[f].direccaos.length == 0) {
            facturacaos[f].direccaos.push({
              direccao: element.direccao,
              saldos: [],
              total: 0
            });
          } else {
            var v = 0;
            for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
              const e = facturacaos[f].direccaos[j];
              if (e.direccao == element.direccao) {
                v = 1;
              }
            }
            if (v == 0) {
              facturacaos[f].direccaos.push({
                direccao: element.direccao,
                saldos: [],
                total: 0
              });
            }
          }
        }
      }
    }

    for (let f = 0; f < facturacaos.length; f++) {
      const elef = facturacaos[f];
      for (let i = 0; i < elef.direccaos.length; i++) {
        const element = elef.direccaos[i];
        for (let j = 0; j < d.length; j++) {
          const el = d[j];
          if (
            element.direccao === el.direccao &&
            elef.tipoFacturacao === el.facturacao
          ) {
            var mes = "";
            for (let index = 0; index < meses.length; index++) {
              const ele = meses[index];
              if (ele.numero_mes === el.mes) {
                mes = ele.nome_mes;
              }
            }
            facturacaos[f].direccaos[i].saldos.push({
              nome_mes: mes,
              numero_mes: "" + (el.mes < 10 && filter.tipoFacturacao == 'POS-PAGO' ? "0" + el.mes : el.mes),
              valor: el.total
            });
          }
        }
      }
    }

    for (let i = 0; i < meses.length; i++) {
      const mes = meses[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          var v = 0;
          const element = facturacaos[f].direccaos[j].saldos;
          for (let k = 0; k < element.length; k++) {
            const el = element[k];

            if (el.numero_mes === mes.numero_mes) {
              v = 1;
              facturacaos[f].direccaos[j].total += el.valor;
            }
          }
          if (v == 0) {
            facturacaos[f].direccaos[j].saldos.push(meses[i]);
          }
        }
      }
    }
    //return facturacaos;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let index = 0; index < facturacaos[f].direccaos.length; index++) {
        facturacaos[f].direccaos[index].saldos.sort(function (a, b) {
          return a.numero_mes - b.numero_mes;
        });
      }
    }

    for (let i = 0; i < meses2.length; i++) {
      var total = 0;
      const element = meses2[i];
      for (let f = 0; f < facturacaos.length; f++) {
        for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
          for (let s = 0; s < facturacaos[f].direccaos[j].saldos.length; s++) {
            if (element.numero_mes == facturacaos[f].direccaos[j].saldos[s].numero_mes) {
              total += facturacaos[f].direccaos[j].saldos[s].valor;
            }
          }
        }
      }
      meses2[i].valor = total;
    }

    total = 0;

    for (let f = 0; f < facturacaos.length; f++) {
      for (let j = 0; j < facturacaos[f].direccaos.length; j++) {
        total += facturacaos[f].direccaos[j].total;
      }
    }

    meses2[12].valor = total;

    var mesesCalculoHorizontal = meses2;

    return { facturacaos, mesesCalculoHorizontal };
  }


}

module.exports = Relatorio;
