'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class StkMovimento extends Model {

    static async storeData(armazem_id, artigos, tipo_movimento_id, user_id){

        for (let index = 0; index < artigos.length; index++) {

            await StkMovimento.create({
              produto_id: artigos[index].artigo_id,
              armazem_id: armazem_id,
              tipo_movimento_id: tipo_movimento_id,
              quantidade: artigos[index].quantidade,
              user_id: user_id
            });
      
          } 

          return true;
    }
}

module.exports = StkMovimento
