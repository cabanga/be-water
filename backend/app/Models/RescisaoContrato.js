'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Database = use("Database");
const LocalConsumo = use("App/Models/LocalConsumo");
const Factura = use("App/Models/Factura");
const Leitura = use("App/Models/Leitura");

class RescisaoContrato extends Model {

  contrato() {
    return this.belongsTo("App/Models/Contrato");
  }
  motivo_rescisao() {
    return this.belongsTo("App/Models/MotivoRecisao");
  }
  morada_correspondencia() {
    return this.belongsTo("App/Models/MoradaCorrespondencia");
  }
  leitura() {
    return this.belongsTo("App/Models/Leitura");
  }
  conta() {
    return this.belongsTo("App/Models/Conta");
  }
  user() {
    return this.belongsTo("App/Models/User");
  }
  cliente() {
    return this.belongsTo("App/Models/Cliente");
  }

  static async findById(id) {
    var rescisao = await RescisaoContrato.query().select('rescisao_contratoes.*', Database.raw('DATE_FORMAT(rescisao_contratoes.created_at, "%Y-%m-%d") as created_at'))
      .with('motivo_rescisao').with('leitura')
      .with('morada_correspondencia').with("contrato").with('contrato.tarifa')
      .with("contrato.conta").with("conta.cliente").with("cliente")
      .with("cliente.tipoCliente").with("cliente.municipio").with("cliente.municipio.provincia")
      .with("cliente.tipoIdentidade")
      .with('user').with('user.agencia').with('user.empresa')
      .where("rescisao_contratoes.contrato_id", id).first();
    if (rescisao == null) {
      return null;
    }

    rescisao = rescisao.toJSON();

    return rescisao;
  }

  static async gerarRescisaoContrato(id) {
    var rescisao = await this.findById(id);
    let contrato = null;
    let morada_correspondencia = null;

    contrato = await Database
    .select(
      'contratoes.id as contrato_id',
      'contratoes.conta_id',
      'contas.numero_conta as conta',
      'contas.cliente_id',
      'clientes.id as cliente_id',
      'clientes.nome as cliente_nome',
      'clientes.numero_identificacao',
      'clientes.morada as cliente_morada',
      'clientes.municipio_id',
      'clientes.telefone as cliente_telefone',
      'clientes.email as cliente_email',
      'contratoes.objecto_contrato_id',
      'objecto_contratoes.nome as objecto_contrato',
      'contratoes.data_fim',
      'contratoes.tipo_contracto_id',
      'tipo_contratoes.descricao as tipo_contrato',
      Database.raw('DATE_FORMAT(contratoes.data_inicio, "%Y-%m-%d") as data_inicio'),
      'contratoes.tarifario_id',
      'tarifarios.descricao as tarifa',
      'local_consumos.local_instalacao_id',
      'local_consumos.contador_id',
      'contadores.numero_serie as contador',

      'local_instalacaos.is_predio',
      'local_instalacaos.predio_nome',
      'local_instalacaos.predio_andar',
      "local_instalacaos.id",
      "local_instalacaos.moradia_numero",
      "local_instalacaos.rua_id",
      "local_instalacaos.cil",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.distrito_id",
      "distritos.nome as distrito",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
      Database.raw('DATE_FORMAT(rescisao_contratoes.data_rescisao, "%Y-%m-%d") as data_rescisao'),
      'rescisao_contratoes.leitura_id',
      'leituras.ultima_leitura',
      'rescisao_contratoes.motivo_recisao_id',
      'motivo_recisaos.nome as motivo_rescisao'
    )
    .from('contratoes')
    .leftJoin('contas', 'contas.id', 'contratoes.conta_id')
    .leftJoin('clientes', 'contas.cliente_id', 'clientes.id')
    .leftJoin('objecto_contratoes', 'objecto_contratoes.id', 'contratoes.objecto_contrato_id')
    .leftJoin('tarifarios', 'tarifarios.id', 'contratoes.tarifario_id')
    .leftJoin('local_consumos', 'local_consumos.contrato_id', 'contratoes.id')
    .leftJoin('local_instalacaos', 'local_instalacaos.id', 'local_consumos.local_instalacao_id')
    .leftJoin('ruas', 'ruas.id', 'local_instalacaos.rua_id')
    .leftJoin('bairros', 'bairros.id', 'ruas.bairro_id')
    .leftJoin('quarteiraos', 'quarteiraos.id', 'ruas.quarteirao_id')
    .leftJoin('municipios', 'municipios.id', 'bairros.municipio_id')
    .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
    .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
    .leftJoin('contadores', 'contadores.id', 'local_consumos.contador_id')
    .leftJoin('rescisao_contratoes', 'rescisao_contratoes.contrato_id', 'contratoes.id')
    .leftJoin('motivo_recisaos', 'rescisao_contratoes.motivo_recisao_id', 'motivo_recisaos.id')
    .leftJoin('leituras', 'rescisao_contratoes.leitura_id', 'leituras.id')
    .leftJoin('tipo_contratoes', 'tipo_contratoes.id', 'contratoes.tipo_contracto_id')
    .where('contratoes.id', id).first();

    morada_correspondencia = await Database
    .select(
      'rescisao_contratoes.id',
      'rescisao_contratoes.contrato_id',
      'rescisao_contratoes.morada_correspondencia_id',
      "morada_correspondencias.rua_id",
      'morada_correspondencias.is_predio',
      'morada_correspondencias.predio_nome',
      'morada_correspondencias.predio_andar',
      "morada_correspondencias.predio_id",
      "morada_correspondencias.numero_moradia",
      "ruas.nome as rua",
      "ruas.quarteirao_id",
      "quarteiraos.nome as quarteirao",
      "ruas.bairro_id",
      "bairros.nome as bairro",
      "bairros.municipio_id",
      "municipios.nome as municipio",
      "municipios.provincia_id",
      "provincias.nome as provincia",
    )
    .from('rescisao_contratoes')
    .leftJoin('morada_correspondencias', 'morada_correspondencias.id', 'rescisao_contratoes.morada_correspondencia_id')
    .leftJoin('ruas', 'ruas.id', 'morada_correspondencias.rua_id')
    .leftJoin('bairros', 'bairros.id', 'ruas.bairro_id')
    .leftJoin('quarteiraos', 'quarteiraos.id', 'ruas.quarteirao_id')
    .leftJoin('municipios', 'municipios.id', 'bairros.municipio_id')
    .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
    .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
    .where('rescisao_contratoes.contrato_id', id).first();

    let tipos_identidades = await Database
    .select(
      'nome',
      'numero_digitos',
      'numero_identidade'
    )
    .from("cliente_identidades")
    .innerJoin("tipo_identidades", "tipo_identidades.id", "cliente_identidades.tipo_identidade_id")
    .where("cliente_identidades.cliente_id", contrato.cliente_id)

    let data = {
      contrato: contrato,
      tipos_identidades: tipos_identidades,
      user: rescisao.user,
      morada_correspondencia: morada_correspondencia,

    };
    return data;
  }
}

module.exports = RescisaoContrato
