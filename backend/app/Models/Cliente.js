"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Cliente extends Model {


  lista_identidades(){
    return this.hasMany("App/Models/ClienteIdentidade")
    .select(
      'cliente_identidades.id',
      'tipo_identidade_id',
      'nome',
      'numero_digitos',
      'numero_identidade'
    )
    .leftJoin(
      'tipo_identidades', 'tipo_identidades.id', 'cliente_identidades.tipo_identidade_id'
    )
  }

  identidades(){
    return this.hasMany("App/Models/ClienteIdentidade")
    .with('tipo_identidade')
  }

  conta() {
    return this.hasMany("App/Models/Conta")
  }

  tipoCliente() {
    return this.belongsTo("App/Models/TipoCliente");
  }

  tipoIdentidade() {
    return this.belongsTo("App/Models/TipoIdentidade");
  }

  user() {
    return this.belongsTo("App/Models/User");
  }

  municipio() {
    return this.belongsTo("App/Models/Municipio");
  }

  clienteFornecedor() {
    return this.hasMany("App/Models/clienteFornecedor");
  }
  static get rules() {
    return {
      nome: "required",
      //contribuente: "",
      email: "required",
      tipo_identidade_id: "required",
      tipo_cliente_id: "required",
      telefone: "required",
      morada: "required",
    };
  }
  facturas() {
    return this.hasMany("App/Models/Factura");
  }

  static scopeHasFactura(query) {
    return query.has("factura");
  }

  factura() {
    return this.hasOne("App/Models/Factura");
  }

  user() {
    return this.belongsTo("App/Models/User");
  }
  adiantamento() {
    return this.belongsTo("App/Models/Adiantamento");
  }
}

module.exports = Cliente;
