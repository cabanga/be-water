'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProdutoImposto extends Model {

  produto() {
    return this.belongsTo('App/Models/Produto')
  }

  imosto() {
    return this.belongsTo('App/Models/Imposto')
  }
  
  user() {
    return this.belongsTo('App/Models/User')
  }

}

module.exports = ProdutoImposto
