'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const fs = require("fs");
const atob = require('atob');
const moment = require('moment')
global.window = { document: { createElementNS: () => { return {} } } };
global.navigator = {};
global.html2pdf = {};
global.btoa = () => { };
global.atob = atob;


const jsPDF = require('jspdf');


class UnigJsPdf extends Model { 

    static async PDFFactura(factura, produtos, cliente, user, pagamento, servico_conta, facturas_clientes, tecnologia, por_pagar) {
        const doc = new jsPDF();
        const image = fs.readFileSync('public/images/logo_at.jpg');
        const imgFooter = fs.readFileSync('public/images/logo_at_footer.jpg');


        var img = 'data:image/jpeg;base64,' + image.toString('base64');
        var img_footer = 'data:image/jpeg;base64,' + imgFooter.toString('base64');
        doc.addImage(img, 'jpeg', 10, 4, 25, 18);
        doc.setFontSize(7)
        UnigJsPdf.textoLateral(doc);


        var x = 12;
        doc.setFontSize(7);
        doc.setFontType("bold");
        doc.setTextColor(10, 73, 134);
        doc.text(x, 25, ' Apoio ao Cliente: ');
        doc.setFontType("normal");
        doc.text(x + 22, 25, ' 222 700 000 / 800 220 220 / 19150 / 19166 ');
        doc.setFontType("bold");
        doc.text(x, 27.5, ' www.angolatelecom.ao ');

        doc.setFontSize(10);
        doc.setTextColor(0, 48, 135);
        doc.setFontType("bold");
        doc.text(x + 103, 15, ' FACTURA ');

        var somaMonth = new Date(tecnologia.ano + '-' + tecnologia.mes + '-01');
        var dataLimite = somaMonth.setMonth(somaMonth.getMonth() + 1);

        var lastDay = new Date(dataLimite)

        var anoCc = lastDay.getFullYear();
        var mesCc = lastDay.getMonth();

        var LimitePagamento = new Date(anoCc, mesCc + 1, 0);

        doc.setFillColor(0, 48, 135);

        doc.rect(115, 23, 90, 8, 'F')

        doc.setFontSize(10);
        doc.setTextColor(255, 255, 255);
        doc.text(x + 105, 26.5, 'MONTANTE DA FACTURA:  ');
        doc.setFontSize(9);
        doc.text(x + 105, 30, 'Data Limite de Pagamento:');
        doc.text(x + 191, 26.5, this.numberFormat(factura.total) + ' AKZ', 'right');
        doc.text(x + 191, 30, '' + moment(LimitePagamento).format('DD') +
            '-' + moment(LimitePagamento).format('MM') +
            '-' + moment(LimitePagamento).format('YYYY'), 'right');
        doc.setFontSize(7);
        doc.setTextColor(10, 73, 134);
        doc.text(x, 41, 'Titular:');
        doc.text(x, 48, 'NIF:');
        doc.text(x, 52, 'Conta Nº:');
        doc.text(x, 56, 'Nome da conta:');
        doc.text(x, (factura.contaDescricao.length > 36 ? 64 : 60), 'Endereço:');
        //doc.text(x, (factura.contaDescricao.length > 50 ? 68 : 66), 'Filial:');

        doc.text(x + 103, 41, 'Factura Nº: ');
        doc.text(x + 103, 45, 'Data de Emissão:');
        doc.text(x + 103, 49, 'Data de Factura:');
        doc.text(x + 103, 53, 'Período de Facturação:');
        doc.text(x + 103, 57, 'Entidade Bancária:');
        doc.text(x + 103, 61, 'Conta Bancária:');
        doc.setFontStyle('normal');
        doc.text(x + 20, 41, '' + cliente.nome, { maxWidth: 50 });
        doc.text(x + 20, 48, '' + (cliente.contribuente == '999999999' || cliente.contribuente == null || cliente.contribuente == 0 ? 'Consumidor Final' : cliente.contribuente));
        doc.text(x + 20, 52, '' + (factura.numeroExterno == null ? factura.conta_id : factura.numeroExterno));
        doc.text(x + 20, 56, '' + (factura.contaDescricao == null ? '' : factura.contaDescricao), { maxWidth: 60 });
        doc.text(x + 20, (factura.contaDescricao.length > 36 ? 64 : 60), '' + (cliente.morada == null ? '' : cliente.morada));
        //doc.text(x + 20, (factura.contaDescricao.length > 50 ? 68 : 66), '' + (factura.filialNome == null ? '' : factura.filialNome));

        var CurrentDate = new Date();

        var d = new Date(tecnologia.ano + '-' + tecnologia.mes + '-01');

        var anoC = d.getFullYear();
        var mesC = d.getMonth();

        var d2 = new Date(anoC, mesC + 1, 0);

        doc.text(x + 135, 41, '' + factura.factura_sigla);
        doc.text(x + 135, 45, '' + moment(factura.created_at).format('DD') +
            '-' + moment(factura.created_at).format('MM') +
            '-' + moment(factura.created_at).format('YYYY'));
        doc.text(x + 135, 49, '' + moment(factura.created_at).format('DD') +
            '-' + moment(factura.created_at).format('MM') +
            '-' + moment(factura.created_at).format('YYYY'));
        doc.text(x + 135, 53, '01-' + (tecnologia.mes < 10 ? '0' + tecnologia.mes : tecnologia.mes) + '-' + tecnologia.ano + ' a ' + moment(d2).format('DD') +
            '-' + moment(d2).format('MM') +
            '-' + moment(d2).format('YYYY'));
        doc.text(x + 135, 57, '');
        doc.text(x + 135, 61, '');

        doc.setFillColor(0, 48, 135);
        doc.rect(x, 71, 193.5, 5, 'F')

        doc.setFontSize(9);
        doc.setTextColor(255, 255, 255);
        doc.setFontStyle('bold');
        doc.text(x + 1, 75, 'RESUMO DA FACTURA');

        doc.setFillColor(208, 211, 212);
        doc.setFontSize(9);
        doc.rect(x, 76, 193.5, 5, 'F')
        doc.setTextColor(10, 73, 134);
        doc.text(x, 80, 'ID SERVIÇO');
        doc.text(x + 55, 80, 'DESCRIÇÃO');
        doc.text(x + 138, 80, 'VALOR BASE', 'right');
        doc.text(x + 164, 80, 'IVA 14%', 'right');
        doc.text(x + 190, 80, 'VALOR', 'right');

        var total = 0;
        var totalImposto = 0;
        var totalSemImposto = 0;
        var totalUTT = 0;
        var y = 85;
        doc.setFontSize(7);
        doc.setTextColor(0);
        doc.setFontStyle('normal')
        for (var i = 0; i < produtos.length; i++) {

            doc.text(x, y, '' + produtos[i].descricao_operacao);
            doc.text(x + 50, y, (produtos[i].nome == null ? '' : produtos[i].nome), { maxWidth: 50 });
            if (produtos[i].chaveServico != null) {
                doc.setFontSize(5);
                doc.text(x + 50, (produtos[i].nome.length < 50 ? y + 2 : y + 5), '' + produtos[i].chaveServico, { maxWidth: 50 });
                doc.setFontSize(7);
            }
            doc.text(x + 138, y, this.numberFormat(produtos[i].linhaTotalSemImposto), 'right');
            doc.text(x + 162, y, this.numberFormat(produtos[i].valorImposto), 'right');
            doc.text(x + 190, y, this.numberFormat(produtos[i].total), 'right');
            y += 10;
            UnigJsPdf.newPage(doc, y);
            total += produtos[i].total;
            totalImposto += produtos[i].valorImposto;
            totalSemImposto += produtos[i].linhaTotalSemImposto;
            totalUTT += (produtos[i].valor / 10);
        }


        doc.setFillColor(208, 211, 212);
        doc.setFontSize(9);
        doc.rect(x, y - 4, 193.5, 5, 'F')
        doc.setTextColor(10, 73, 134);
        doc.setFontStyle('bold')
        doc.text(x + 1, y - 0.5, 'Total Factura');
        doc.text(x + 138, y - 0.5, '' + this.numberFormat(totalSemImposto), 'right');
        doc.text(x + 162, y - 0.5, '' + this.numberFormat(totalImposto), 'right');
        doc.text(x + 190, y - 0.5, '' + this.numberFormat(total), 'right');

        UnigJsPdf.newPage(doc, y);
        doc.setFillColor(0, 48, 135);
        doc.setFontSize(9);
        doc.rect(x, y + 2, 193.5, 5, 'F');
        doc.setFontStyle('bold');
        doc.setTextColor(255, 255, 255);
        doc.text(x + 3, y + 6, 'UTT/AKZ');
        doc.text(x + 190, y + 6, '10,00', 'right');

        doc.setFontSize(7);
        doc.setTextColor(0);
        doc.setFontStyle('italic');

        doc.text(x, y + 10, 'Obs.: Conserve este documento e valide os dados da sua factura até 10 dias após a recepcão da mesma, findo os quais, a Angola Telecom considerará o ciclo fechado e a informação definitiva.', { maxWidth: 190 });
        UnigJsPdf.newPage(doc, y);
        doc.setFillColor(208, 211, 212);
        doc.setFontSize(9);

        doc.rect(x, y + 20, 193.5, 5, 'F')
        doc.setTextColor(10, 73, 134);

        doc.setFillColor(0, 48, 135);
        doc.rect(x, y + 15, 193.5, 5, 'F');
        doc.setFontStyle('bold');
        doc.setTextColor(255);
        doc.text(x + 1, y + 19, 'EXTRACTO DA CONTA');

        doc.setTextColor(10, 73, 134);
        doc.text(x + 1, y + 24, 'DOCUMENTO');
        doc.text(x + 75, y + 24, 'PERÍODO', 'center');
        doc.text(x + 125, y + 24, 'EM ATRASO (Meses)', 'center');
        doc.text(x + 192, y + 24, 'VALOR', 'right');

        var totalPorPagar = 0;
        var totalLinha = 0;

        doc.setFontSize(7);
        doc.setTextColor(0);
        y += 2;
        for (var i = 0; i < facturas_clientes.length; i++) {

            (facturas_clientes[i].mes < 10 ? "0" + facturas_clientes[i].mes : facturas_clientes[i].mes)

            var dt1 = tecnologia.ano + '/' + tecnologia.mes + '/' + '01';
            var dt2 = facturas_clientes[i].ano + '/' + facturas_clientes[i].mes + '/' + '01';

            var data1 = new Date(dt1);
            var data2 = new Date(dt2);
            var diff = (data2.getFullYear() - data1.getFullYear()) * 12 + (data2.getMonth() - data1.getMonth());

            var posNum = (diff < 0) ? diff * -1 : diff;

            var d = new Date(facturas_clientes[i].ano + '-' + facturas_clientes[i].mes + '-01');

            var anoC = d.getFullYear();
            var mesC = d.getMonth();

            var d2 = new Date(anoC, mesC + 1, 0);

            doc.text(x, y + 26, 'Factura Nº: ' + facturas_clientes[i].factura_sigla);
            doc.text(x + 75, y + 26, '01-' + facturas_clientes[i].mes + '-' + facturas_clientes[i].ano + ' a ' + moment(d2).format('DD') +
                '-' + moment(d2).format('MM') +
                '-' + moment(d2).format('YYYY'), 'center');
            doc.text(x + 125, y + 26, '' + posNum, 'center');
            doc.text(x + 192, y + 26, this.numberFormat(facturas_clientes[i].total), 'right');
            totalPorPagar += facturas_clientes[i].total

            totalLinha = facturas_clientes.length;
            y += 3;
            UnigJsPdf.newPage(doc, y);;

        }


        doc.setFillColor(208, 211, 212);
        doc.setFontSize(9);

        doc.rect(x, y + 24, 193.5, 5, 'F')
        doc.setTextColor(10, 73, 134);
        doc.text(x + 1, y + 27.5, 'Total Pendente de Pagamento');
        doc.text(x + 192, y + 27.5, totalPorPagar == 0 ? '' : this.numberFormat(totalPorPagar) + ' ', 'right');
        doc.setFontSize(7);
        doc.setTextColor(0);
        doc.setFontStyle('italic');
        doc.text(120, y + 32, (totalLinha > 0 ? 'Referência das últimas ' + totalLinha + ' facturas de um total de ' + por_pagar.total_por_pagar + ' facturas por pagar' : ''));


        let impostos = [];
        let validar = 0
        produtos.forEach(element => {

            let response = { descricao: element.descricaoImposto, codigo: element.codigoImposto, valor: element.vImposto, somaImposto: element.valorImposto, id: element.imposto_id }

            if (impostos.length == 0) {
                impostos.push(response);
            } else {
                validar = 0
                for (let j = 0; j < impostos.length; j++) {
                    if (element.imposto_id == impostos[j].id) {
                        response.somaImposto += impostos[j].somaImposto
                        impostos.splice(j, 1);
                        impostos.splice(j, 0, response);
                        validar = 1;
                    }
                }

                if (validar == 0) {
                    impostos.push(response);
                }

            }


        });

        y += 18;
        UnigJsPdf.newPage(doc, y);;
        doc.setFillColor(208, 211, 212);
        doc.setFontSize(9);

        doc.rect(x, y + 20, 193.5, 5, 'F')
        doc.setTextColor(10, 73, 134);

        doc.setFillColor(0, 48, 135);
        doc.rect(x, y + 15, 193.5, 5, 'F');
        doc.setFontStyle('bold');
        doc.setTextColor(255);
        doc.text(x + 1, y + 19, 'RESUMO DOS IMPOSTOS');

        doc.setTextColor(10, 73, 134);
        doc.text(x + 1, y + 24, 'CÓDIGO');
        doc.text(x + 75, y + 24, 'DESCRIÇÃO', 'center');
        doc.text(x + 125, y + 24, 'VALOR IMPOSTO', 'center');


        doc.setFontSize(7);
        doc.setTextColor(0);
        y += 2;

        var outraPage = 0
        impostos.forEach(imposto => {
            doc.text(x, y + 26, '' + imposto.codigo);
            doc.text(x + 75, y + 26, '' + imposto.descricao, 'center');
            doc.text(x + 125, y + 26, '' + this.numberFormat(imposto.somaImposto), 'center');
            y += 3;
            UnigJsPdf.newPage(doc, y);;
        })


        if (factura.observacao != null) {
            UnigJsPdf.newPage(doc, y);;
            doc.setFillColor(208, 211, 212);
            doc.setFontSize(9);

            doc.rect(x, y + 27, 193.5, 5, 'F')
            doc.setTextColor(10, 73, 134);

            doc.setFillColor(0, 48, 135);
            // doc.rect(x, y + 21, 193.5, 5, 'F');
            doc.setFontStyle('bold');
            //doc.setTextColor(255);
            //doc.text(x + 1, y + 25, 'RESUMO DOS IMPOSTOS');

            doc.setTextColor(10, 73, 134);
            doc.text(x + 1, y + 31, 'OBSERVAÇÃO: ' + factura.observacao);
        }

        doc.setFillColor(208, 211, 212);
        doc.setFontSize(9);


        UnigJsPdf.newPage(doc, y);;
        doc.setFillColor(0, 48, 135);
        doc.rect(x, y + 35, 193.5, 5, 'F');
        doc.setFontStyle('bold');
        doc.setFontSize(9);
        doc.setTextColor(255);
        doc.text(x + 1, y + 38.5, 'INFORMAÇÃO PARA PAGAMENTO');

        doc.setFillColor(208, 211, 212);
        doc.rect(x, y + 40, 193.5, 5, 'F')
        doc.setTextColor(0);
        doc.setFontStyle('normal');
        doc.text(x + 1, y + 43.5, 'ANGOLA TELECOM');

        doc.setFontSize(7);
        doc.text(x + 43, y + 43.5, 'Talão de Controlo');

        doc.setFontSize(9);

        doc.setFontStyle('italic');
        y++;
        y += 52;
        UnigJsPdf.newPage(doc, y);;
        doc.text(x, y + 40, 'O talão emitido pela Caixa Automática faz prova de pagamento. Conserve-o.', { maxWidth: 70 });
        doc.setLineDash([0.3, 0.1], 0)
        doc.line(x + 70, y, x + 70, y + 45);

        doc.setFontStyle('normal')

        doc.text(x + 72, y, 'BE     AKZ - IBAN: AO06004500500000947117860 Nº Conta: 9471178');
        doc.text(x + 72, y + 4, 'BCI    AKZ - IBAN: AO06000500000032572710585 Nº Conta: 325727-10-005');
        doc.text(x + 72, y + 8, 'BIC    AKZ - IBAN: AO06005100006803833615136 Nº Conta: 68038336-15-1 ');
        doc.text(x + 72, y + 12, 'BFA   AKZ - IBAN: AO06000600000011913330253 Nº Conta: 119133-30-2');
        doc.text(x + 72, y + 16, 'BPC   AKZ - IBAN: AO06001000010001102001183 Nº Conta: 0001-011020-011');
        doc.text(x + 72, y + 20, '          AKZ - IBAN: AO06001000010001102001571 Nº Conta: 0001-011020-015 ');
        doc.text(x + 72, y + 24, 'SOL   AKZ - IBAN: AO06 00440000349947611512 Nº Conta: 34994761-15-001');
        y += 28;
        doc.text(x + 72, y, 'BAI    AKZ - IBAN: AO06004000004291597610140 Nº Conta: 42915976-10-1 ');
        doc.text(x + 72, y + 4, 'BNI    AKZ - IBAN: AO06005200000009049210184 Nº Conta: 90492-10-001 ');
        doc.text(x + 72, y + 8, 'BMA  AKZ - IBAN: AO06005500000883073110157 Nº Conta: 8830731-10-001 ');
        doc.text(x + 72, y + 12, 'CXA   AKZ - IBAN: AO06000400000029765310239 Nº Conta: 297653 -10-002');
        doc.setTextColor(10, 73, 134);
        y += 26;
        UnigJsPdf.newPage(doc, y);;
        UnigJsPdf.footer(doc, x, img_footer, factura, user);

        var buffer = new Buffer(doc.output('arraybuffer'));
        var filename = factura.documento + "_" + factura.factura_sigla.replace("/", "-");
        

        let dir = 'public/uploads/facturas'
        if (!(fs.existsSync(dir))) {
            fs.mkdirSync(dir);
            fs.appendFile(dir+'/' + filename + '.pdf', buffer, function (err, fd) {  });
        }
        else {
            fs.appendFile('public/uploads/facturas/' + filename + '.pdf', buffer, function (err, fd) {});
        }


        return filename;
    }


    static textoLateral(doc) {
        doc.text(114, 230, 'ANGOLA TELECOM. Empresa de Telecomunicação de Angola, EP. Contribuinte nº 5410000323. Rua das Quipacas nº 186 – Luanda. Telef.: 222 630000 – Fax 222 311288. www.angolatelecom.ao', 'center', 90);
    }
    static newPage(doc, y) {
        if (y + 50 > doc.internal.pageSize.height) {
            y = 10;
            doc.addPage();
            UnigJsPdf.textoLateral(doc);
            //footer();
        }
    }

    static footer(doc, x, img_footer, factura, user) {
        doc.setFontStyle('italic');
        doc.setTextColor(0);
        /*
        doc.text(doc.internal.pageSize.width - 200, doc.internal.pageSize.height - 56, 'Deverá validar os dados da sua factura até 10 dias após a recepcão da mesma, findos os quais a Angola Telecom considerará o ciclo fechado e a informação será definitiva', { maxWidth: 150 })
        */
        //doc.text(doc.internal.pageSize.width - 63, doc.internal.pageSize.height - 46, 'PROCESSADO POR COMPUTADOR')
        doc.text(doc.internal.pageSize.width - 50, doc.internal.pageSize.height - 42, 'Unig versão 1.0')
        doc.addImage(img_footer, x, doc.internal.pageSize.height - 40, 191, 10.3);

        doc.setFontSize(9);
        doc.line(15, doc.internal.pageSize.height - 18, 196, doc.internal.pageSize.height - 18); // vertical line
        var hash = factura.hash.substring(0, 1);
        hash += factura.hash.substring(10, 11);
        hash += factura.hash.substring(20, 21);
        hash += factura.hash.substring(30, 31);
        doc.setFontSize(7);

        doc.text("NIF: " + user.taxRegistrationNumber + " - " + user.companyName + " / " + user.addressDetail + " / " + user.telefone + " / " + user.email, 105, doc.internal.pageSize.height - 14, null, null, 'center');

        doc.setFontSize(8);
        doc.text(hash + '-Processado por programa validado 4/AGT119', 105, doc.internal.pageSize.height - 10, null, null, 'center');

    }

    static numberFormat(number) {
        return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace('€', '').trim();
    }
}

module.exports = UnigJsPdf

