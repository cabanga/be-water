'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Provincia extends Model {
  locais_de_consumo() {
    return this.hasMany("App/Models/Municipio")
      .select(
        'cil',
        'is_predio',
        'predio_nome',
        'predio_andar',
        'moradia_numero',

        'contaDescricao',
        'numero_conta',
        'local_consumo.id as local_consumo_id',
        'local_instalacao.id as local_instalacao_id',
        'moradia_numero',
        'latitude',
        'longitude',
      )
      .innerJoin(
        'bairros as bairro',
        'bairro.municipio_id', 'municipios.id'
      )
      .innerJoin(
        'ruas as rua',
        'rua.bairro_id', 'bairro.id'
      )
      .innerJoin(
        'local_instalacaos as local_instalacao',
        'local_instalacao.rua_id', 'rua.id'
      )
      .innerJoin(
        'local_consumos as local_consumo',
        'local_consumo.local_instalacao_id', 'local_instalacao.id',
      )
      .whereNotNull('contador_id')
      .innerJoin(
        'contas as conta',
        'local_consumo.conta_id', 'conta.id'
      )
      .distinct(
        'contaDescricao',
        'numero_conta',
        'latitude',
        'longitude'
      )

    //.limit(10)

    //.with('locais_de_consumo')
  }


}

module.exports = Provincia
