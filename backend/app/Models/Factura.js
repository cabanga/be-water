'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const DataResponse = use("App/Models/DataResponse");
const Database = use("Database");
const Serie = use("App/Models/Serie");
const Model = use('Model')
const LinhaFactura = use("App/Models/LinhaFactura");
const LocalConsumo = use("App/Models/LocalConsumo");

const Leitura = use("App/Models/Leitura");

const gearAss = use("App/Helpers/gearAss");

var numeral = require("numeral");
var moment = require("moment");

class Factura extends Model {

  static castDates(field, value) {
    if (field == "status_date") {
      return value.format("YYYY-MM-DD h:mm:ss");
    }
    return super.formatDates(field, value);
  }

  cliente() {
    return this.belongsTo("App/Models/Cliente");
  }

  linhasFacturas() {
    return this.hasMany("App/Models/LinhaFactura");
  }

  linhasRecibos() {
    return this.hasMany("App/Models/LinaRecibo");

  }


  recibos() {
    return this.belongsToMany('App/Models/Recibo').pivotModel('App/Models/LinhaRecibo')
  }
  pagamentos() {
    return this.belongsTo('App/Models/Pagamento')
  }
  user() {
    return this.belongsTo("App/Models/User");
  }

  serie() {
    return this.belongsTo("App/Models/Serie");
  }

  conta() {
    return this.belongsTo("App/Models/Conta", 'conta_id');
  }

  contrato() {
    return this.belongsTo("App/Models/Contrato");
  }


  fornecedor() {
    return this.belongsTo("App/Models/Fornecedor");
  }
  lines() {
    return this.hasMany("App/Models/LinhaFactura").select("*", "artigo_id as produto_id", "valor_imposto as valorImposto")
  }

  static get rules_anular() {
    return {
      status_reason: "required",
    };
  }

  static findAllInvoices(search, filters) {
    return Factura.query().select('facturas.*', Database.raw('DATE_FORMAT(facturas.created_at, "%m") as mes'), { nome: 'clientes.nome' })
      .innerJoin("clientes", "clientes.id", "facturas.cliente_id")
      .innerJoin("series", "series.id", "facturas.serie_id")
      .innerJoin("documentos", "documentos.id", "series.documento_id")
      .whereIn('documentos.sigla', filters.documents)
      .where(function () {
        if (filters.typeFilter) {

          if (filters.typeFilter == 'chave_serviço') {
            this.where(`facturas.servico_id`, Database.select('id').from('servicos').where("chaveServico", `${search}`))
          } else if (filters.typeFilter == 'número_de_conta') {
            this.where(`facturas.conta_id`, `${search}`);
          } else if (filters.typeFilter == 'nome_cliente') {
            this.whereIn(`facturas.cliente_id`, Database.select('id').from('clientes').where("nome", "LIKE", `%${search}%`))
          } else if (filters.typeFilter == 'número_de_contribuinte') {
            // const c = await Database.select('id').from('clientes').where("contribuente", search).first();
            console.log("typeFilter");
            this.whereIn(`facturas.cliente_id`, Database.select('id').from('clientes').where("contribuente", `${search}`))
          } else if (filters.typeFilter == "número_da_factura") {
            this.where(`facturas.factura_sigla`, `${search}`);
          }
        } else {
          if (search) {
            if (filters.keys.invoice instanceof Array) { filters.keys.invoice.forEach(key => { this.orWhere(`facturas.${key}`, 'like', "%" + search + "%") }) }
            else { this.where(`facturas.${filters.keys.invoice}`, 'like', "%" + search + "%") }
          } else {
            this.whereBetween(Database.raw('IF(IFNULL(facturas.created_at,0)=0, DATE_FORMAT(facturas.status_date, "%Y-%m-%d"), DATE_FORMAT(facturas.created_at, "%Y-%m-%d"))'), [filters.startDate, filters.endDate])
          }
        }
      })
      .with("serie.documento").with("cliente")
      .orderBy(filters.orderBy, filters.typeOrderBy)
      .paginate(filters.page, filters.perPage)
  }

  /**
   *  factura details.
   * PUT or PATCH clientes/:id
   * @author caniggiamoreira@hotmail.com ou caniggiamoreira@itgest.pt
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */

  static async gerarFactura(id) {
    var factura = await this.findById(id);
    const localconsumo = (factura.contrato == null ? null :
      await LocalConsumo.query()
        .select('local_consumos.contador_id', 'contadores.contador', 'local_instalacaos.cil', 'local_instalacaos.predio_nome', 'local_instalacaos.predio_andar',
          'local_instalacaos.is_predio', 'local_instalacaos.moradia_numero', 'calibres.descricao as calibre',
          'ruas.nome as rua', 'bairros.nome as bairro', 'quarteiraos.nome as quarteirao', 'municipios.nome as municipio')
        .innerJoin('contadores', 'contadores.id', 'local_consumos.contador_id')
        .leftJoin('local_instalacaos', 'local_instalacaos.id', 'local_consumos.local_instalacao_id')
        .leftJoin('ruas', 'ruas.id', 'local_instalacaos.rua_id')
        .leftJoin('bairros', 'bairros.id', 'ruas.bairro_id')
        .leftJoin('quarteiraos', 'quarteiraos.id', 'ruas.quarteirao_id')
        .leftJoin('municipios', 'municipios.id', 'bairros.municipio_id')
        .leftJoin('calibres', 'calibres.id', 'local_instalacaos.calibre_id')
        .where('local_consumos.contrato_id', factura.contrato.id).first());

    const leituras = localconsumo == null ? [] : await Leitura.query().select('leituras.*', Database.raw('DATE_FORMAT(leituras.data_leitura, "%Y-%m-%d") as data_leitura')).where('contador_id', localconsumo.contador_id).orderBy('id', 'DESC').limit(4).fetch()
    //console.log(factura.contrato)   

    /*      let loja_conta_bancarias = await Database.select(
          "clientes.id as cliente_id",
          "clientes.nome",
          "clientes.morada",
          "clientes.municipio_id",
          "conta_bancarias.id",
          "conta_bancarias.nif",
          "conta_bancarias.iban",
          "conta_bancarias.banco_id",
          "conta_bancarias.municipio_id",
          "municipios.nome as municipio",
          "bancos.nome as banco"
        ) 
           .from("clientes")
          .innerJoin("conta_bancarias", "conta_bancarias.municipio_id", "clientes.municipio_id")
          .innerJoin("bancos", "bancos.id", "conta_bancarias.banco_id")
          .innerJoin("municipios", "municipios.id", "conta_bancarias.municipio_id")
          .where("clientes.id", factura.cliente.id)  */

    let lojas = await Database.select(
      "*"
    )
      .from("lojas")
      .where("lojas.municipio_id", factura.cliente.municipio_id)

    let loja_conta_bancarias = await Database.select(
      "lojas.id",
      "lojas.nome as loja",
      "lojas.nif",
      "lojas.horario",
      "lojas.endereco",
      "loja_bancos.loja_id",
      "loja_bancos.banco_id",
      "bancos.abreviatura as banco",
      "bancos.iban",
    )
      .from("loja_bancos")
      .innerJoin("lojas", "lojas.id", "loja_bancos.loja_id")
      .innerJoin("bancos", "bancos.id", "loja_bancos.banco_id")
      .where("lojas.municipio_id", factura.cliente.municipio_id)
      .limit(3)

    /*     console.log(loja_conta_bancarias) */


    let tipos_identidades = await Database
      .select(
        'nome',
        'numero_digitos',
        'numero_identidade'
      )
      .from("cliente_identidades")
      .innerJoin("tipo_identidades", "tipo_identidades.id", "cliente_identidades.tipo_identidade_id")
      .where("cliente_identidades.cliente_id", factura.cliente.id)

    let data = {
      factura: factura,
      pagamentos: factura.pagamentos,
      cliente: factura.cliente,
      tipos_identidades: tipos_identidades,
      lojas: lojas,
      contabancaria: loja_conta_bancarias,
      contrato: factura.contrato == null ? null : Object.assign(factura.contrato, { localconsumo: localconsumo, leituras: leituras }),
      produtos: factura.lines,
      user: factura.user,
      conta_corrente: {
        saldo_anterio: 0,
        pagamentos: 0,
        factura_actual: factura.total
      }
    };

    //console.log(data.contrato.leituras);

    return data;
  }

  static async findFactFromNc(params) {

    var factura = await Factura.findById(params);

    if (factura == null) { return DataResponse.response("error", 500, "Nenhum resultado encontrado", null); }
    else if (factura.status == "A") { return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " está anulada", null); }
    else if (factura.serie.documento.sigla == "NC") { return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + ", não é possivel criar nota de crédito numa nota de crédito", null); }

    if (factura.__meta__.total_recibos > 0 && factura.__meta__.totalReciboAnulados > 0) {
      return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " não pode ser criada nota de crédito porque já possui recebimentos", factura);
    }

    var totalNC = 0;

    factura.lines.forEach(element => { Object.assign(element, { qtd_fixo: element.quantidade }); });

    if (factura.notas_credito.length > 0) {
      for (let index = 0; index < factura.notas_credito.length; index++) {
        const element = factura.notas_credito[index];
        totalNC += (element.status == "N" ? element.total : 0)
      }
      totalNC = numeral(totalNC).format("0.00");

      if (numeral(factura.total).format("0.00") == totalNC) {
        return DataResponse.response("error", 500, factura.serie.documento.nome + " nº " + factura.factura_sigla + " Ja atingiu o limite de nota de crédito", null);
      }

      var ncLines = await Database.raw("SELECT l.factura_id, l.servico_id, l.artigo_id, sum(l.quantidade) as quantidade, SUM(l.total) as total, SUM(l.linhaTotalSemImposto) as linhaTotalSemImposto, SUM(l.valor_imposto) as valorImposto FROM linha_facturas l, facturas f WHERE l.factura_id = f.id AND f.status = 'N' AND f.numero_origem_factura = '" + factura.factura_sigla + "' GROUP BY l.factura_id, l.artigo_id,l.servico_id")
      for (let i = 0; i < ncLines[0].length; i++) {
        const ncLine = ncLines[0][i];
        for (let j = 0; j < factura.lines.length; j++) {
          const ftline = factura.lines[j];
          if (ncLine.artigo_id == ftline.artigo_id && ncLine.servico_id == ftline.servico_id) {
            factura.lines[j].quantidade -= ncLine.quantidade;
            factura.lines[j].qtd_fixo -= ncLine.quantidade;
            factura.lines[j].linhaTotalSemImposto -= ncLine.linhaTotalSemImposto;
            factura.lines[j].valor_imposto -= ncLine.valor_imposto;
            factura.lines[j].valorImposto -= ncLine.valorImposto;
            factura.lines[j].total -= ncLine.total;
            if (factura.lines[j].quantidade <= 0) {
              factura.lines.splice(j, 1);
            }
          }
        }
      }
    }
    let data = {
      factura: factura,
      cliente: factura.cliente,
      produtos: factura.lines,
    };

    return DataResponse.response("success", 200, "", data);
  }

  static async findById(id) {
    var factura = await Factura.query().select('facturas.*', Database.raw('DATE_FORMAT(facturas.created_at, "%Y-%m-%d") as created_at'), 'facturas.created_at as datahora_emissao')
      .with("serie").with("serie.documento").with("cliente")
      .with("cliente.tipoCliente").with("cliente.municipio").with("cliente.municipio.provincia")
      .with("cliente.tipoIdentidade")
      .with('pagamentos.lines.forma_pagamento').with('contrato').with('contrato.tarifa')
      .with('user').with('user.agencia').with('user.empresa')
      .with("lines").with('lines.imposto')
      .with("lines.produto").with("lines.produto.incidencia").with("lines.servico").with('recibos')
      .withCount('recibos as total_recibos')
      .withCount('recibos as totalReciboAnulados', (builder) => { builder.where("status", 'N') })
      .where("id", id).first();
    if (factura == null) {
      return null;
    }
    factura = factura.toJSON();
    factura = { ...factura, notas_credito: await Factura.findNCFromFT(factura.factura_sigla) };


    return factura;
  }

  static async findNCFromFT(factura) {
    var factura = await Factura.query()
      .with("serie").with("serie.documento").with("cliente")
      .with("cliente.tipoCliente").with("cliente.tipoIdentidade")
      .with("lines").with('lines.imposto')
      .with("lines.produto").where("numero_origem_factura", factura).fetch();
    if (factura == null) {
      return [];
    }

    return factura.toJSON()
  }

  static async FacturaCreate(facturacao) {
    var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");

    const numberAndSignature = await Factura.numberAndSignatureInvoice(facturacao.total, facturacao.serie);
    const factura = await Factura.create({
      numero: numberAndSignature.serie.proximo_numero,
      factura_sigla: numberAndSignature.factura_sigla,
      serie_id: facturacao.serie,
      totalComImposto: numeral(facturacao.totalImposto).format("0.00"),
      totalSemImposto: numeral(facturacao.subtotal).format("0.00"),
      total: numeral(facturacao.total).format("0.00"),
      valor_aberto: numberAndSignature.serie.sigla == 'FT' ? numeral(facturacao.total).format("0.00") : 0,
      pago: numberAndSignature.serie.sigla == 'FT' ? 0 : 1,
      status: "N",
      status_date: data,
      systemEntryDate: data,
      hash: numberAndSignature.signature,
      conta_id: facturacao.conta_id,
      //contador_id:facturacao.contador_id,
      hash_control: 1, //l_hash,
      cliente_id: facturacao.cliente_id,
      user_id: facturacao.user_id,
      moeda_iso: facturacao.moeda.codigo_iso,
      moeda_id: facturacao.moeda.id,
      facturacao: "POS-PAGO",
    });

    await Database.table("series").where("id", facturacao.serie).update("proximo_numero", Number(numberAndSignature.serie.proximo_numero) + 1);
    const line = await Factura.linhaFacturaCreate(factura, facturacao.produtos);
    return DataResponse.response("success", 200, "Factura criada com sucesso", factura);
  }

  /**
   *
   * @param {*} factura
   * @param {*} produtos
   */
  static async linhaFacturaCreate(factura, produtos) {
    for (let index = 0; index < produtos.length; index++) {
      const produto = produtos[index];
      await LinhaFactura.create({
        artigo_id: produto.artigo_id,
        factura_id: factura.id,
        user_id: factura.user_id,
        valor: numeral(produto.valor).format("0.00"),
        quantidade: produto.quantidade,
        total: numeral(produto.total).format("0.00"),
        linhaTotalSemImposto: numeral(produto.subtotal).format("0.00"),
        valor_imposto: numeral(produto.valorImposto).format("0.00"),
        valor_desconto: numeral(produto.desconto).format("0.00"),
        imposto_id: produto.imposto_id,
        observacao: produto.observacao,
        moeda_iso: factura.codigo_iso,
        moeda_id: factura.moeda_id,
      });
      await Database.table("charges").where("id", produto.charge_id).update("is_facturado", true);
    }
    return produtos;
  }

  static async numberAndSignatureInvoice(total, serie_id) {
    const serie = await Serie.query("series.*", { sigla: "documentos.sigla" }).innerJoin("documentos", "documentos.id", "series.documento_id").where("series.id", serie_id).first();

    var factura_sigla = serie.sigla + " " + serie.nome + "/" + serie.proximo_numero;

    var data = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
    var InvoiceDate = moment(new Date()).format("YYYY-MM-DD");
    var SystemEntryDate = moment(new Date()).format("YYYY-MM-DD") + "T" + moment(new Date()).format("HH:mm:ss");
    var InvoiceNo = factura_sigla;
    var GrossTotal = numeral(total).format("0.00");

    var hash = InvoiceDate + ";" + SystemEntryDate + ";" + InvoiceNo + ";" + GrossTotal + ";";
    const max_n = await Database.from("facturas").where("serie_id", serie.id).max("numero as total");

    var cc = (max_n[0].total == null ? 0 : max_n[0].total);

    if (cc != 0) {
      const doc_ant = await Database.select("hash as hash").from("facturas").where("serie_id", serie.id).where("numero", cc).first();
      hash = hash + "" + doc_ant.hash;
    }
    const signature = await gearAss(hash);
    return { signature: signature, factura_sigla: factura_sigla, proximo_numero: serie.proximo_numero, serie: serie }
  }


}

module.exports = Factura
