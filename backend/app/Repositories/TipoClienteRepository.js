'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const TipoCliente = use("App/Models/TipoCliente");


class TipoClienteRepository {

    async getAll(page = null, search = null) {
        if (page) {
            const tipos = await TipoCliente.query()
            .where(function (params)  {
                if(search){ this.orWhere('tipoClienteDesc', 'like', "%"+ search + "%" ) }
            })
            .orderBy('tipoClienteDesc', 'asc')
            .paginate(page, 10)
            return tipos
        }

        const tipos = await TipoCliente.query()
        .where(function () {
            if(search){ this.orWhere('tipoClienteDesc', 'like', "%"+ search + "%" ) }
        })
        .orderBy('tipoClienteDesc', 'asc')
        .fetch()
        return {data: tipos} 
    }

    async create (data) {
        try {
            const res = await TipoCliente.create(data)
            return ResponseMessage.response( 200, "Tipo de cliente registado com sucesso.", res )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response(201, "Falha não registar Tipo de cliente, por favor verifica os dados ", null )
        }
    }

    async findById (id) {
        const res = await TipoCliente.find(id)
        return ResponseMessage.response( 200, "", res )
    }

    async update (id, data) {
        try {
            await TipoCliente.query().where('id', id).update(data)
            let res = await this.findById(id)
            return ResponseMessage.response( 200, "Tipo de cliente actualizado com sucesso.", res.data )

        } catch (error) {
            console.log( error )
            return ResponseMessage.response( 201, "Falha não actualizar Tipo de cliente, por favor verifica o erro", null)
        }
    }

    async delete (id) {
        try {
            await TipoCliente.query().where('id', id).update({ is_deleted: true })
            return ResponseMessage.response( 200, "Tipo de cliente eliminado com sucesso.", null )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response( 201, "Falha ao eliminar Tipo de cliente", null )
        }
    }

}

module.exports = TipoClienteRepository