'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const BaseRepository = use("App/Repositories/BaseRepository")
const AgendamentoRoteiro = use('App/Models/AgendamentoRoteiro')
const RotaRun = use('App/Models/RotaRun')


class AgendamentoRoteiroRepository extends BaseRepository {

  async getAll(page = null, search = null) {
    if (page) {
      const response = await RotaRun
      .query()
      .where(function (params)  { if(search){ this.orWhere('data_realizar', 'like', "%"+ search + "%" ) } })
      .with('agendamento')
      .orderBy('id', 'asc')
      .paginate(page, 10)
      return response
    }

    const response = await RotaRun
    .query()
    .where(function () { if(search){ this.orWhere('data_realizar', 'like', "%"+ search + "%" ) } })
    .with('agendamento')
    .orderBy('id', 'asc')
    .fetch()

    return {data: response}





    /*

      .select(
        "id",
        "rota_header_id",
        "local_consumo_id",
        "estado_rota_id",
        "created_at",
        "updated_at",
        "periodicidade_roteiro_id",
        "dia_mes",
        "dia_semana",
        "proxima_data_realizar",
        "nao_leitura",
        "motivo",
        Database.raw('DATE_FORMAT(data_realizar, "%Y-%m") as data_realizar')
      )


    if (page) {
      const response = await AgendamentoRoteiro
      .query()
      .where(function (params)  { if(search){ this.orWhere('data_realizar', 'like', "%"+ search + "%" ) } })
      .with('roteiro')
      .orderBy('id', 'asc')
      .paginate(page, 10)
      return response
    }

    const response = await AgendamentoRoteiro
    .query()
    .where(function () { if(search){ this.orWhere('data_realizar', 'like', "%"+ search + "%" ) } })
    .with('roteiro')
    .orderBy('id', 'asc')
    .fetch()

    return {data: response}
    */
  }

  async create (data) {
    try {
      const response = await AgendamentoRoteiro.create(data)
      return ResponseMessage.response( 200, "Registo feito com sucesso.", response )
    } catch (error) {
      console.log( error )
      return ResponseMessage.response( 201, "Falha não registar, por favor verifica os dados ", null)
    }
  }

  async findById (id) {
    const response = await AgendamentoRoteiro.find(id)
    //await response.load('roteiro')
    return ResponseMessage.response( 200, "", response )
  }

  async update (id, data) {
    try {
      await AgendamentoRoteiro.query().where('id', id).update(data)
      let response = await this.findById(id)
      return ResponseMessage.response( 200, "Actualização feita com sucesso.", response.data )
    } catch (error) {
      console.log( error )
      return ResponseMessage.response(201, "Falha não actualizar, por favor verifica o erro", null)
    }
  }


}

module.exports = AgendamentoRoteiroRepository
