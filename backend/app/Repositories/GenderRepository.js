'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const Gender = use('App/Models/Genero')


class GenderRepository {

    async getAll(page = null, search = null) {
        if (page) {
            const genders = await Gender.query()
            .where(function (params)  {
                if(search){ this.orWhere('descricao', 'like', "%"+ search + "%" ) }
            })
            .orderBy('descricao', 'asc')
            .paginate(page, 10)
            return genders
        }

        const genders = await Gender.query()
        .where(function () {
            if(search){ this.orWhere('descricao', 'like', "%"+ search + "%" ) }
        })
        .orderBy('descricao', 'asc')
        .fetch()
        return {data: genders} 
    }

    async create (data) {
        let slug = await this.generate_slug(data.descricao)

        try {
            const gender = await Gender.create({
                abreviacao: data.abreviacao,
                descricao: data.descricao,
                slug: slug
            })
            return ResponseMessage.response( 200, "Genero registado com sucesso.", gender )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response(
                201,
                "Falha não registar Genero, por favor verifica os dados ",
                null
            )
        }
    }

    async findById (id) {
        const gender = await Gender.find(id)
        return ResponseMessage.response( 200, "", gender )
    }

    async update (id, data) {
        try {
            let slug = await this.generate_slug(data.descricao)

            await Gender.query().where('id', id).update({
                abreviacao: data.abreviacao,
                descricao: data.descricao,
                slug: slug
            })
            let gender = await this.findById(id)

            return ResponseMessage.response( 200, "Genero actualizado com sucesso.", gender.data )

        } catch (error) {
            console.log( error )
            return ResponseMessage.response(
                201,
                "Falha não actualizar Genero, por favor verifica o erro",
                null
            )
        }
    }

    async delete (id) {
        try {
            await Gender.query().where('id', id).update({ is_deleted: true })
            return ResponseMessage.response( 200, "Genero eliminado com sucesso.", null )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response( 201, "Falha ao eliminar Genero", null )
        }
    }

    async generate_slug(str) {
        str = str.replace(/^\s+|\s+$/g, '')
        str = str.toLowerCase()
        let slug = ''
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
        var to   = "aaaaeeeeiiiioooouuuunc------"
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
        }
    
        slug = str.replace(/[^a-z0-9 -]/g, '')
            .replace(/\s+/g, '-')
            .replace(/-+/g, '-')
    
        return slug
    }
}

module.exports = GenderRepository