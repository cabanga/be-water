'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const ClienteIdentidade = use("App/Models/ClienteIdentidade");
const Cliente = use("App/Models/Cliente");


class ClienteIdentidadeRepository {

    async getAll(page = null, search = null) {
      if (page) {
        const tipos = await ClienteIdentidade.query()
        .where(function (params)  {
          if(search){ this.orWhere('numero_identidade', 'like', "%"+ search + "%" ) }
        })
        .orderBy('numero_identidade', 'asc')
        .paginate(page, 10)
        return tipos
      }

      const tipos = await ClienteIdentidade.query()
      .where(function () {
        if(search){ this.orWhere('numero_identidade', 'like', "%"+ search + "%" ) }
      })
      .orderBy('numero_identidade', 'asc')
      .fetch()
      return {data: tipos}
    }

    async create (data) {
      try {
        const res = await ClienteIdentidade.create(data)
        return ResponseMessage.response( 200, "Tipo de Identidade associado com sucesso.", res )
      } catch (error) {
        console.log( error )
        return ResponseMessage.response(201, "Falha ao associar o tipo de identidade, por favor verifica os dados ", null )
      }
    }

    async findById (id) {
      const res = await ClienteIdentidade.find(id)
      return ResponseMessage.response( 200, "", res )
    }

    async identidadesClinete (id) {
      const res = await Cliente.find(id)
      await res.load('lista_identidades')
      return ResponseMessage.response( 200, "", res )
    }

    async update (id, data) {
      try {
        await ClienteIdentidade.query().where('id', id).update(data)
        let res = await this.findById(id)
        return ResponseMessage.response( 200, "Associação actualizado com sucesso.", res.data )

      } catch (error) {
        console.log( error )
        return ResponseMessage.response( 201, "Falha não actualizar à associação, por favor verifica o erro", null)
      }
    }

    async delete (id) {
      try {
        await ClienteIdentidade.query().where('id', id).update({ is_deleted: true })
        return ResponseMessage.response( 200, "Associação removida com sucesso.", null )
      } catch (error) {
        console.log( error )
        return ResponseMessage.response( 201, "Falha ao Associar  o tipo de identidade ao cliente", null )
      }
    }

}

module.exports = ClienteIdentidadeRepository
