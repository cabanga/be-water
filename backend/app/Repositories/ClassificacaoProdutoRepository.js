'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const ClassificacaoProduto = use("App/Models/ClassificacaoProduto");


class ClassificacaoProdutoRepository {

    async getAll() {
        const res = await ClassificacaoProduto.query()
        .orderBy('descricao', 'asc')
        .fetch()
        return {data: res} 
    }

    async findById (id) {
        const res = await ClassificacaoProduto.find(id)
        return ResponseMessage.response( 200, "", res )
    }

    async create (data) {
        try {
            let slug = await this.generate_slug(data.descricao)
            const gender = await ClassificacaoProduto.create({
                descricao: data.descricao,
                slug: slug
            })
            return ResponseMessage.response( 200, "Classificação de produto registado com sucesso.", gender )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response( 201, "Falha não registar Classificação de produto, por favor verifica os dados ", null )
        }
    }

    async update (id, data) {
        try {
            let slug = await this.generate_slug(data.descricao)
            await ClassificacaoProduto.query().where('id', id).update({
                descricao: data.descricao,
                slug: slug
            })
            let res = await this.findById(id)
            return ResponseMessage.response( 200, "Classificação de produto actualizada com sucesso.", res.data )

        } catch (error) {
            console.log( error )
            return ResponseMessage.response(201, "Falha não actualizar Classificação de produto, por favor verifica o erro", null)
        }
    }

    async delete (id) {
        try {
            await ClassificacaoProduto.query().where('id', id).update({ is_deleted: true })
            return ResponseMessage.response( 200, "Classificação de produto eliminada com sucesso.", null )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response( 201, "Classificação de produto ao eliminar Direcção", null )
        }
    }

    async generate_slug(str) {
        str = str.replace(/^\s+|\s+$/g, '')
        str = str.toLowerCase()
        let slug = ''
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
        var to   = "aaaaeeeeiiiioooouuuunc------"
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
        }
    
        slug = str.replace(/[^a-z0-9 -]/g, '')
            .replace(/\s+/g, '-')
            .replace(/-+/g, '-')
    
        return slug
    }

}

module.exports = ClassificacaoProdutoRepository