'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const Direccao = use('App/Models/Direccao')


class DireccaoRepository {

    async getAll(page = null, search = null) {
        if (page) {
            const direccoes = await Direccao.query()
            .where(function (params)  {
                if(search){ this.orWhere('designacao', 'like', "%"+ search + "%" ) }
            })
            .orderBy('designacao', 'asc')
            .paginate(page, 10)
            return direccoes
        }

        const direccoes = await Direccao.query()
        .where(function () {
            if(search){ this.orWhere('designacao', 'like', "%"+ search + "%" ) }
        })
        .orderBy('designacao', 'asc')
        .fetch()
        return {data: direccoes} 
    }

    async create (data) {
        try {
            let slug = await this.generate_slug(data.designacao)
            const gender = await Direccao.create({
                user_id: data.user_id,
                designacao: data.designacao,
                slug: slug
            })
            return ResponseMessage.response( 200, "Direcção registada com sucesso.", gender )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response(
                201,
                "Falha não registar Direcção, por favor verifica os dados ",
                null
            )
        }
    }

    async findById (id) {
        const gender = await Direccao.find(id)
        return ResponseMessage.response( 200, "", gender )
    }

    async update (id, data) {
        try {
            let slug = await this.generate_slug(data.designacao)
            await Direccao.query().where('id', id).update({
                user_id: data.user_id,
                designacao: data.designacao,
                slug: slug
            })
            let direccao = await this.findById(id)
            return ResponseMessage.response( 200, "Direcção actualizada com sucesso.", direccao.data )

        } catch (error) {
            console.log( error )
            return ResponseMessage.response(201, "Falha não actualizar Direcção, por favor verifica o erro", null)
        }
    }

    async delete (id) {
        try {
            await Direccao.query().where('id', id).update({ is_deleted: true })
            return ResponseMessage.response( 200, "Direcção eliminada com sucesso.", null )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response( 201, "Falha ao eliminar Direcção", null )
        }
    }


    async generate_slug(str) {
        str = str.replace(/^\s+|\s+$/g, '')
        str = str.toLowerCase()
        let slug = ''
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
        var to   = "aaaaeeeeiiiioooouuuunc------"
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
        }
    
        slug = str.replace(/[^a-z0-9 -]/g, '')
            .replace(/\s+/g, '-')
            .replace(/-+/g, '-')
    
        return slug
    }
    
}

module.exports = DireccaoRepository