'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const BaseRepository = use("App/Repositories/BaseRepository")
const PeriodicidadeRoteiro = use('App/Models/PeriodicidadeRoteiro')


class PeriodicidadeRoteiroRepository extends BaseRepository {

  async getAll(page = null, search = null) {
    if (page) {
      const response = await PeriodicidadeRoteiro
      .query()
      .where(function (params)  { if(search){ this.orWhere('descricao', 'like', "%"+ search + "%" ) } })
      .orderBy('id', 'asc')
      .paginate(page, 10)

      return response
    }

    const response = await PeriodicidadeRoteiro
    .query()
    .where(function () { if(search){ this.orWhere('descricao', 'like', "%"+ search + "%" ) } })
    .orderBy('id', 'asc')
    .fetch()

    return {data: response}
  }

  async create (data) {
    try {
      let slug = await this.generate_slug(data.descricao)
      const response = await PeriodicidadeRoteiro.create({
        descricao: data.descricao,
        slug: slug
      })

      return ResponseMessage.response( 200, "Registo feito com sucesso.", response )
    } catch (error) {
      console.log( error )
      return ResponseMessage.response( 201, "Falha não registar, por favor verifica os dados ", null)
    }
  }

  async findById (id) {
    const response = await PeriodicidadeRoteiro.find(id)
    return ResponseMessage.response( 200, "", response )
  }

  async getBySlug (slug) {
    const response = await PeriodicidadeRoteiro.findBy('slug', slug)
    return ResponseMessage.response( 200, "", response )
  }

  async update (id, data) {
    try {
      let slug = await this.generate_slug(data.descricao)
      await PeriodicidadeRoteiro.query().where('id', id).update({
        descricao: data.descricao,
        slug: slug
      })

      let response = await this.findById(id)
      return ResponseMessage.response( 200, "Actualização feita com sucesso.", response.data )

    } catch (error) {
      console.log( error )
      return ResponseMessage.response(201, "Falha não actualizar, por favor verifica o erro", null)
    }
  }


}

module.exports = PeriodicidadeRoteiroRepository
