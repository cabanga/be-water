'use strict'

const Database = use('Database')

class BaseRepository {

  async groupBy(array, prop){
    return array.reduce(function(groups, item) {
      const val = item[prop]
      groups[val] = groups[val] || []
      groups[val].push(item)
      return groups
    }, {})
  }

  async locais_ids(){
    let locais = (await Database
      .select('local_consumo_id')
      .from('rota_runs')
      .where('estado_rota_id', 1))
      .map(local => local.local_consumo_id)
    return locais
  }

  async generate_slug(str) {
    str = str.replace(/^\s+|\s+$/g, '')
    str = str.toLowerCase()
    let slug = ''
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
    var to   = "aaaaeeeeiiiioooouuuunc------"
    for (var i=0, l=from.length ; i<l ; i++) {
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
    }

    slug = str.replace(/[^a-z0-9 -]/g, '')
    .replace(/\s+/g, '-')
    .replace(/-+/g, '-')

    return slug
  }

}

module.exports = BaseRepository
