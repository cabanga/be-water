'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const Cliente = use('App/Models/Cliente')


class ClienteRepository {

    async getAll(search, orderBy, pagination) {

      let response = await Cliente.query()
      .select(
        'clientes.*',
        "municipios.nome as municipio_nome",
        "municipios.provincia_id",
        "generos.descricao as genero",
        "tipo_clientes.tipoClienteDesc as tipo_cliente",
        "tipo_clientes.slug as tipo_cliente_slug"
      )
      .where(function () {
        if (search != null) {
          this.where("clientes.id", "like", "%" + search + "%")
          this.orWhere('clientes.nome', 'like', '%' + search + '%')
          this.orWhere('clientes.telefone', 'like', '%' + search + '%')
          this.orWhere('clientes.email', 'like', '%' + search + '%')
        }
      })
      .leftJoin("generos",        "generos.id",       "clientes.genero_id")
      .leftJoin("municipios",     "municipios.id",    "clientes.municipio_id")
      .leftJoin("tipo_clientes",  "tipo_clientes.id", "clientes.tipo_cliente_id")

      .orderBy(orderBy == null ? "clientes.nome" : orderBy, "ASC")
      .paginate(
        pagination.page,
        pagination.perPage
      )

      return response
    }

}

module.exports = ClienteRepository
