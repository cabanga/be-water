'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const GestorConta = use('App/Models/GestorConta')

class GestorContaRepository {

    async getAll(page = null, search = null) {
        if (page) {
            const gestores = await GestorConta.query()
            .where(function (params)  {
                if(search){ this.orWhere('nome', 'like', "%"+ search + "%" ) }
            })
            .orderBy('designacao', 'asc')
            .paginate(page, 10)
            return {data: gestores} 
        }

        const gestores = await GestorConta.query()
        .where(function () {
            if(search){ this.orWhere('nome', 'like', "%"+ search + "%" ) }
        })
        .orderBy('nome', 'asc')
        .fetch()
        return {data: gestores} 
    }

    async create (data) {
        try {
            let slug = await this.generate_slug(data.nome)

            const gestor = await GestorConta.create({
                nome:       data.nome,
                telefone:   data.telefone,
                email:      data.email,
                morada:     data.morada,
                slug:       slug
            })
            return ResponseMessage.response( 200, "Gestor registado com sucesso.", gestor )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response(201,"Falha não registar Gestor, por favor verifica os dados ",null)
        }
    }

    async findById (id) {
        const gestor = await GestorConta.find(id)
        return ResponseMessage.response( 200, "", gestor )
    }

    async update (id, data) {
        try {
            let slug = await this.generate_slug(data.nome)
            await GestorConta.query().where('id', id).update({
                nome:       data.nome,
                telefone:   data.telefone,
                email:      data.email,
                morada:     data.morada,
                slug:       slug
            })
            let gestor = await this.findById(id)
            return ResponseMessage.response( 200, "Gestor actualizado com sucesso.", gestor.data )

        } catch (error) {
            console.log( error )
            return ResponseMessage.response(201, "Falha não actualizar Gestor, por favor verifica o erro", null)
        }
    }

    async delete (id) {
        try {
            await GestorConta.query().where('id', id).update({ is_deleted: true })
            return ResponseMessage.response( 200, "Gestor eliminado com sucesso.", null )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response( 201, "Falha ao eliminar Gestor", null )
        }
    }


    async generate_slug(str) {
        str = str.replace(/^\s+|\s+$/g, '')
        str = str.toLowerCase()

        let slug = ''
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
        var to   = "aaaaeeeeiiiioooouuuunc------"
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
        }
    
        slug = str.replace(/[^a-z0-9 -]/g, '')
            .replace(/\s+/g, '-')
            .replace(/-+/g, '-')
    
        return slug
    }
    
}

module.exports = GestorContaRepository