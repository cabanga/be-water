'use strict'

const DataResponse = use("App/Models/DataResponse");
const Contrato = use('App/Models/Contrato');
const Database = use("Database");


class ContratoRepository {

    async getAll(search = null, orderBy, pagination) {
      const res = await Database.select(
        'contratoes.id AS contrato_id',
        'contratoes.morada_contrato',
        'clientes.nome AS cliente_nome',
        'local_instalacaos.cil',
        'objecto_contratoes.nome AS objecto_nome',
        'tarifarios.descricao AS tarifario_descricao',
        'contadores.numero_serie',
        'contadores.ultima_leitura',
        'estado_contratoes.slug as slug_estado_contrato',
        'estado_contratoes.nome as nome_estado_contrato',
        'tipo_medicaos.nome as tipo_medicao',
        'tipo_contratoes.descricao as tipo_contrato',

        'local_instalacaos.moradia_numero',
        'local_instalacaos.is_predio',
        'local_instalacaos.predio_id',
        'local_instalacaos.predio_nome',
        'local_instalacaos.predio_andar'
      )
      .table('contratoes')
      .leftJoin('contas', 'contratoes.conta_id', 'contas.id')
      .leftJoin('clientes', 'contas.cliente_id', 'clientes.id')

      .leftJoin('local_consumos', 'local_consumos.contrato_id', 'contratoes.id')
      .leftJoin('local_instalacaos', 'local_instalacaos.id', 'local_consumos.local_instalacao_id')
      .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')

      .leftJoin('estado_contratoes', 'contratoes.estado_contrato_id', 'estado_contratoes.id')
      .leftJoin('objecto_contratoes', 'contratoes.objecto_contrato_id', 'objecto_contratoes.id')
      .leftJoin('tarifarios', 'contratoes.tarifario_id', 'tarifarios.id')
      .leftJoin('tipo_medicaos', 'contratoes.tipo_medicao_id', 'tipo_medicaos.id')
      .leftJoin('tipo_contratoes', 'contratoes.tipo_contracto_id', 'tipo_contratoes.id')

      .where(function () {
        if (search != null) {
          this.where('contratoes.data_inicio', 'like', '%' + search + '%')
          this.orWhere('contratoes.data_fim', 'like', '%' + search + '%')
          this.orWhere('contratoes.id', 'like', '%' + search + '%')
          this.orWhere('clientes.nome', 'like', '%' + search + '%')
          this.orWhere('estado_contratoes.nome', 'like', '%' + search + '%')
          this.orWhere('tipo_contratoes.descricao', 'like', '%' + search + '%')
          this.orWhere('tarifarios.descricao', 'like', '%' + search + '%')
          this.orWhere('contas.contaDescricao', 'like', '%' + search + '%')
          this.orWhere('clientes.nome', 'like', '%' + search + '%')
          this.orWhere('contratoes.id', 'like', '%' + search + '%')
        }
      })
      .orderBy(orderBy == null ? "contratoes.created_at" : orderBy, "DESC")
      .paginate(pagination.page, pagination.perPage);

      return DataResponse.response("success", 200, "", res);
    }


    async getContratoById(id) {

        const res = await Database.select(
            'contratoes.id',
            'contratoes.tipo_medicao_id',
            'tipo_medicaos.nome as tipo_medicao',
            'tipo_medicaos.slug as tipo_medicao_slug',
            'contratoes.tipo_contracto_id',
            'tipo_contratoes.descricao as tipo_contrato',
            'contratoes.tipo_facturacao_id',
            'tipo_facturacaos.descricao as tipo_facturacao',
            'contratoes.nivel_sensibilidade_id',
            'nivel_sensibilidades.descricao as nivel_sensibilidade',
            'contratoes.tipologia_cliente_id',
            'tipologia_clientes.descricao as tipologia_cliente',
            'contratoes.objecto_contrato_id',
            'objecto_contratoes.nome as objecto_contrato',
            'contratoes.mensagem_id',
            'mensagems.texto as mensagem',
            'mensagems.tipo_mensagem_id',
            'tipo_mensagems.nome as tipo_mensagem',
            'contratoes.motivo_recisao_id',
            'motivo_recisaos.nome as motivo_recisao',
            'contratoes.estado_contrato_id',
            'estado_contratoes.nome as estado_contrato',
            'estado_contratoes.slug as estado_contrato_slug',
            'contratoes.conta_id as conta_id',
            'contas.numero_conta as numero_conta',
            'contratoes.tarifario_id',
            'tarifarios.descricao as tarifario',
            'local_consumos.local_instalacao_id',
            'local_instalacaos.moradia_numero',
            'local_instalacaos.is_predio',
            'local_instalacaos.predio_id',
            'local_instalacaos.predio_nome',
            'local_instalacaos.predio_andar',
            'local_instalacaos.cil',
            'local_consumos.contador_id',
            "local_instalacaos.rua_id",
            "ruas.nome as rua",
            "ruas.quarteirao_id",
            "quarteiraos.nome as quarteirao",
            "ruas.bairro_id",
            "bairros.nome as bairro",
            "bairros.distrito_id",
            "distritos.nome as distrito",
            "bairros.municipio_id",
            "municipios.nome as municipio",
            "municipios.provincia_id",
            "provincias.nome as provincia",
            'contadores.numero_serie',
            'contadores.ultima_leitura',

            'contas.cliente_id',
            'clientes.nome as cliente',
            'clientes.nome as cliente_nome',
            'clientes.morada as cliente_morada',
            'clientes.telefone as cliente_telefone',
            'clientes.email as cliente_email',
            "clientes.municipio_id",
            "generos.descricao as genero",
            "municipios_cliente.nome as cliente_municipio",
            "municipios_cliente.provincia_id",
            "provincias_cliente.nome as cliente_provincia",
            "clientes.morada",
            "clientes.tipo_identidade_id",
            "tipo_identidades.nome as tipo_identidade",
            "tipo_identidades.numero_digitos",
            "clientes.numero_identificacao",

            'contratoes.morada_correspondencia_flag',
            'contratoes.numero_habitantes',
            'contratoes.numero_utilizadores',
            'contratoes.data_inicio',
            'contratoes.data_fim',
            'contratoes.user_id',
            'users.nome as user',
            'contratoes.created_at'


        )
            .table('contratoes')
            .leftJoin('tipo_medicaos', 'contratoes.tipo_medicao_id', 'tipo_medicaos.id')
            .leftJoin('tipo_contratoes', 'contratoes.tipo_contracto_id', 'tipo_contratoes.id')
            .leftJoin('tipo_facturacaos', 'contratoes.tipo_facturacao_id', 'tipo_facturacaos.id')
            .leftJoin('nivel_sensibilidades', 'contratoes.nivel_sensibilidade_id', 'nivel_sensibilidades.id')
            .leftJoin('tipologia_clientes', 'contratoes.tipologia_cliente_id', 'tipologia_clientes.id')
            .leftJoin('objecto_contratoes', 'contratoes.objecto_contrato_id', 'objecto_contratoes.id')
            .leftJoin('mensagems', 'contratoes.mensagem_id', 'mensagems.id')
            .leftJoin('tipo_mensagems', 'mensagems.tipo_mensagem_id', 'tipo_mensagems.id')
            .leftJoin('motivo_recisaos', 'contratoes.motivo_recisao_id', 'motivo_recisaos.id')
            .leftJoin('estado_contratoes', 'contratoes.estado_contrato_id', 'estado_contratoes.id')
            .leftJoin('contas', 'contratoes.conta_id', 'contas.id')
            .leftJoin('clientes', 'contratoes.cliente_id', 'clientes.id')

            .leftJoin('generos', 'generos.id', 'clientes.genero_id')

            .leftJoin("municipios as municipios_cliente", "municipios_cliente.id", "clientes.municipio_id")
            .leftJoin("provincias as provincias_cliente", "provincias_cliente.id", "municipios_cliente.provincia_id")
            .leftJoin("tipo_identidades", "tipo_identidades.id", "clientes.tipo_identidade_id")

            .leftJoin('tarifarios', 'contratoes.tarifario_id', 'tarifarios.id')
            .leftJoin('local_consumos', 'local_consumos.contrato_id', 'contratoes.id')
            .leftJoin('local_instalacaos', 'local_instalacaos.id', 'local_consumos.local_instalacao_id')
            .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
            .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
            .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
            .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
            .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
            .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
            .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
            .leftJoin('users', 'contratoes.user_id', 'users.id')

            .where("contratoes.id", id)
            .first();

        return res;
    }


    async getContratoByConta(id) {

        return await Database.select(
            'contratoes.id',
            'contratoes.tipo_medicao_id',
            'tipo_medicaos.nome as tipo_medicao',
            'tipo_medicaos.slug as tipo_medicao_slug',
            'contratoes.tipo_contracto_id',
            'tipo_contratoes.descricao as tipo_contrato',
            'contratoes.tipo_facturacao_id',
            'tipo_facturacaos.descricao as tipo_facturacao',
            'contratoes.nivel_sensibilidade_id',
            'nivel_sensibilidades.descricao as nivel_sensibilidade',
            'contratoes.tipologia_cliente_id',
            'tipologia_clientes.descricao as tipologia_cliente',
            'contratoes.objecto_contrato_id',
            'objecto_contratoes.nome as objecto_contrato',
            'contratoes.mensagem_id',
            'mensagems.texto as mensagem',
            'mensagems.tipo_mensagem_id',
            'tipo_mensagems.nome as tipo_mensagem',
            'contratoes.motivo_recisao_id',
            'motivo_recisaos.nome as motivo_recisao',
            'contratoes.estado_contrato_id',
            'estado_contratoes.nome as estado_contrato',
            'estado_contratoes.slug as estado_contrato_slug',
            'contratoes.conta_id as conta_id',
            'contas.numero_conta as numero_conta',
            'contratoes.tarifario_id',
            'tarifarios.descricao as tarifario',
            'local_consumos.local_instalacao_id',
            'local_instalacaos.moradia_numero',
            'local_instalacaos.is_predio',
            'local_instalacaos.predio_id',
            'local_instalacaos.predio_nome',
            'local_instalacaos.predio_andar',
            'local_instalacaos.cil',
            'local_consumos.contador_id',
            "local_instalacaos.rua_id",
            "ruas.nome as rua",
            "ruas.quarteirao_id",
            "quarteiraos.nome as quarteirao",
            "ruas.bairro_id",
            "bairros.nome as bairro",
            "bairros.distrito_id",
            "distritos.nome as distrito",
            "bairros.municipio_id",
            "municipios.nome as municipio",
            "municipios.provincia_id",
            "provincias.nome as provincia",
            'contadores.numero_serie',
            'contadores.ultima_leitura',

            'contas.cliente_id',
            'clientes.nome as cliente',
            'clientes.morada as cliente_morada',
            'clientes.telefone as cliente_telefone',
            'clientes.email as cliente_email',
            "clientes.municipio_id",
            "municipios_cliente.nome as cliente_municipio",
            "municipios_cliente.provincia_id",
            "provincias_cliente.nome as cliente_provincia",
            "clientes.morada",
            "clientes.tipo_identidade_id",
            "tipo_identidades.nome as tipo_identidade",
            "tipo_identidades.numero_digitos",
            "clientes.numero_identificacao",

            'contratoes.morada_correspondencia_flag',
            'contratoes.numero_habitantes',
            'contratoes.numero_utilizadores',
            'contratoes.data_inicio',
            'contratoes.data_fim',
            'contratoes.user_id',
            'users.nome as user',
            'contratoes.created_at'


        )
            .table('contratoes')
            .leftJoin('tipo_medicaos', 'contratoes.tipo_medicao_id', 'tipo_medicaos.id')
            .leftJoin('tipo_contratoes', 'contratoes.tipo_contracto_id', 'tipo_contratoes.id')
            .leftJoin('tipo_facturacaos', 'contratoes.tipo_facturacao_id', 'tipo_facturacaos.id')
            .leftJoin('nivel_sensibilidades', 'contratoes.nivel_sensibilidade_id', 'nivel_sensibilidades.id')
            .leftJoin('tipologia_clientes', 'contratoes.tipologia_cliente_id', 'tipologia_clientes.id')
            .leftJoin('objecto_contratoes', 'contratoes.objecto_contrato_id', 'objecto_contratoes.id')
            .leftJoin('mensagems', 'contratoes.mensagem_id', 'mensagems.id')
            .leftJoin('tipo_mensagems', 'mensagems.tipo_mensagem_id', 'tipo_mensagems.id')
            .leftJoin('motivo_recisaos', 'contratoes.motivo_recisao_id', 'motivo_recisaos.id')
            .leftJoin('estado_contratoes', 'contratoes.estado_contrato_id', 'estado_contratoes.id')
            .leftJoin('contas', 'contratoes.conta_id', 'contas.id')
            .leftJoin('clientes', 'contratoes.cliente_id', 'clientes.id')

            .leftJoin("municipios as municipios_cliente", "municipios_cliente.id", "clientes.municipio_id")
            .leftJoin("provincias as provincias_cliente", "provincias_cliente.id", "municipios_cliente.provincia_id")
            .leftJoin("tipo_identidades", "tipo_identidades.id", "clientes.tipo_identidade_id")

            .leftJoin('tarifarios', 'contratoes.tarifario_id', 'tarifarios.id')
            .leftJoin('local_consumos', 'local_consumos.contrato_id', 'contratoes.id')
            .leftJoin('local_instalacaos', 'local_instalacaos.id', 'local_consumos.local_instalacao_id')
            .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
            .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
            .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
            .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
            .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
            .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
            .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')
            .leftJoin('users', 'contratoes.user_id', 'users.id')

            .where("contratoes.conta_id", id)
            .orderBy('contratoes.created_at', 'DESC');

    }

    async imprimirContrato(contrato_id) {

        const contrato = await Database.select(
          'contratoes.id',
          'contratoes.morada_contrato',
          'clientes.nome AS cliente_nome',
          'clientes.morada AS cliente_morada',
          'clientes.email as cliente_email',
          'clientes.telefone as cliente_telefone',
          'clientes.numero_identificacao as cliente_nif',
          'local_instalacaos.cil',
          'objecto_contratoes.nome AS objecto_nome',
          'tarifarios.descricao AS tarifario_descricao',
          'contadores.numero_serie',
          'contadores.ultima_leitura',
          'estado_contratoes.slug as slug_estado_contrato',
          'tipo_medicaos.nome as tipo_medicao',

          'tipo_contratoes.descricao as tipo_contrato',

          'contratoes.id',
          'contratoes.tipo_medicao_id',
          'contratoes.tipo_contracto_id',
          'contratoes.tipologia_cliente_id',
          'contratoes.objecto_contrato_id',
          'contratoes.nivel_sensibilidade_id',
          'contratoes.tipo_facturacao_id',
          'contratoes.mensagem_id',
          'contratoes.motivo_recisao_id',
          'contratoes.estado_contrato_id',
          'contratoes.tarifario_id',
          'contratoes.conta_id as conta_id',
          'contratoes.created_at',
          'contratoes.morada_correspondencia_flag',
          'contratoes.numero_habitantes',
          'contratoes.numero_utilizadores',
          'contratoes.data_inicio',
          'contratoes.data_fim',
          'contratoes.user_id',
          'tipo_medicaos.nome as tipo_medicao',
          'tipo_medicaos.slug as tipo_medicao_slug',
          'tipo_facturacaos.descricao as tipo_facturacao',
          'nivel_sensibilidades.descricao as nivel_sensibilidade',
          'tipologia_clientes.descricao as tipologia_cliente',
          'objecto_contratoes.nome as objecto_contrato',
          'mensagems.texto as mensagem',
          'mensagems.tipo_mensagem_id',
          'tipo_mensagems.nome as tipo_mensagem',
          'motivo_recisaos.nome as motivo_recisao',
          'estado_contratoes.nome as estado_contrato',
          'estado_contratoes.slug as estado_contrato_slug',
          'contas.numero_conta as numero_conta',
          'tarifarios.descricao as tarifario',

          'local_consumos.local_instalacao_id',
          'local_instalacaos.moradia_numero',
          'local_instalacaos.is_predio',
          'local_instalacaos.predio_id',
          'local_instalacaos.predio_nome',
          'local_instalacaos.predio_andar',
          'local_instalacaos.cil',
          "local_instalacaos.rua_id",

          'local_consumos.contador_id',

          "ruas.nome as rua",
          "ruas.quarteirao_id",
          "ruas.bairro_id",
          "quarteiraos.nome as quarteirao",
          "bairros.nome as bairro",
          "bairros.distrito_id",
          "bairros.municipio_id",

          "distritos.nome as distrito",
          "municipios.nome as municipio",
          "municipios.provincia_id",
          "provincias.nome as provincia",
          'contadores.numero_serie',
          'contadores.ultima_leitura',

          'contas.cliente_id',
          "municipios_cliente.nome as cliente_municipio",
          "municipios_cliente.provincia_id",
          "provincias_cliente.nome as cliente_provincia",

          "tipo_identidades.nome as tipo_identidade",
          "tipo_identidades.numero_digitos",
          'users.nome as user'
        )
        .table('contratoes')
        .leftJoin('contas', 'contratoes.conta_id', 'contas.id')
        .leftJoin('clientes', 'contas.cliente_id', 'clientes.id')

        .leftJoin('local_consumos', 'local_consumos.contrato_id', 'contratoes.id')
        .leftJoin('local_instalacaos', 'local_instalacaos.id', 'local_consumos.local_instalacao_id')
        .leftJoin('contadores', 'local_consumos.contador_id', 'contadores.id')

        .leftJoin('estado_contratoes', 'contratoes.estado_contrato_id', 'estado_contratoes.id')
        .leftJoin('objecto_contratoes', 'contratoes.objecto_contrato_id', 'objecto_contratoes.id')
        .leftJoin('tarifarios', 'contratoes.tarifario_id', 'tarifarios.id')
        .leftJoin('tipo_medicaos', 'contratoes.tipo_medicao_id', 'tipo_medicaos.id')
        .leftJoin('tipo_contratoes', 'contratoes.tipo_contracto_id', 'tipo_contratoes.id')

        .leftJoin('tipo_facturacaos', 'contratoes.tipo_facturacao_id', 'tipo_facturacaos.id')
        .leftJoin('nivel_sensibilidades', 'contratoes.nivel_sensibilidade_id', 'nivel_sensibilidades.id')
        .leftJoin('tipologia_clientes', 'contratoes.tipologia_cliente_id', 'tipologia_clientes.id')
        .leftJoin('mensagems', 'contratoes.mensagem_id', 'mensagems.id')
        .leftJoin('tipo_mensagems', 'mensagems.tipo_mensagem_id', 'tipo_mensagems.id')
        .leftJoin('motivo_recisaos', 'contratoes.motivo_recisao_id', 'motivo_recisaos.id')

        .leftJoin("municipios as municipios_cliente", "municipios_cliente.id", "clientes.municipio_id")
        .leftJoin("provincias as provincias_cliente", "provincias_cliente.id", "municipios_cliente.provincia_id")
        .leftJoin("tipo_identidades", "tipo_identidades.id", "clientes.tipo_identidade_id")

        .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
        .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
        .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
        .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
        .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
        .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
        .leftJoin('users', 'contratoes.user_id', 'users.id')
        .where('contratoes.id',contrato_id).first();


        let tipos_identidades = await Database
        .select(
          'nome',
          'numero_digitos',
          'numero_identidade'
        )
        .from("cliente_identidades")
        .innerJoin("tipo_identidades", "tipo_identidades.id", "cliente_identidades.tipo_identidade_id")
        .where("cliente_identidades.cliente_id", contrato.cliente_id)

        const empresa = await Database.from('empresas').first();
        const data = {
            contrato: contrato,
            tipos_identidades: tipos_identidades,
            empresa: empresa
          }

        return DataResponse.response("success", 200, "", data);
    }
}

module.exports = ContratoRepository
