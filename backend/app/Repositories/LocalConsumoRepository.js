'use strict'

class LocalConsumoRepository {

  async getByRua(rua_id) {
    const res = await Rua.findBy('id', rua_id)
    await res.load('locais_de_consumo')
    return res
  }

  async getByBairro(bairro_id) {
    const res = await Bairro.findBy('id', bairro_id)
    await res.load('locais_de_consumo')
    return res
  }

  async getByQuarterao(quarteirao_id) {
    const res = await Quarteirao.findBy('id', quarteirao_id)
    await res.load('locais_de_consumo')
    return res
  }

  async getByDistrito(distrito_id) {
    const res = await Distrito.findBy('id', distrito_id)
    await res.load('locais_de_consumo')
    return res
  }

  async getByMunicipio(municipio_id) {
    const res = await Municipio.findBy('id', municipio_id)
    //await res.load('locais_by_bairros')
    await res.load('locais_de_consumo')
    return res
  }

  async getByProvincia(provincia_id) {
    const res = await Provincia.findBy('id', provincia_id)
    await res.load('locais_de_consumo')
    return res
  }

}

module.exports = LocalConsumoRepository


