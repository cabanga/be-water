'use strict'
const Database = use("Database");
const Mensagem = use('App/Models/ResponseMessage')
const ClasseTarifario = use('App/Models/ClasseTarifario')


class ClasseTarifarioRepository {

  async getAll(page = null, search) {

    const servicos = await Database.select(
      "produtos.id as servico_id",
      "produtos.nome as servico",
      "produtos.valor as servico_valor",
      "produtos.is_active",
      "produtos.imposto_id",
      "impostos.descricao as imposto",
      "impostos.codigo as imposto_codigo",
      "impostos.valor as imposto_valor",
      "produtos.is_editable",
      "produtos.tipo_produto_id",
      "tipo_produtos.descricao as tipo_produto",
      "produtos.incidencia_id",
      "incidencias.nome as incidencia"
    ).from("produtos")
      .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
      .leftJoin("tipo_produtos", "produtos.tipo_produto_id", "tipo_produtos.id")
      .leftJoin("incidencias", "produtos.incidencia_id", "incidencias.id")
      .innerJoin("produto_classe_tarifarios", "produtos.id", "produto_classe_tarifarios.produto_id")
      .where("produtos.is_active", true)
      .where(function () {
        if (search) { this.orWhere('produtos.nome', 'like', "%" + search + "%") }
        if (page) this.paginate(page, 30)
      });

    //console.log(servicos);

    return servicos;
  }


  async getServicosFacturacao(page = null, search) {

    if(search == null) search = '';

    try {

      const value = "Consumo Água"; // provisorio

      const result = await Database.select(
        "produtos.id as servico_id",
        "produtos.nome as servico",
        "produtos.valor as servico_valor",
        "produtos.is_active",
        "produtos.imposto_id",
        "impostos.descricao as imposto",
        "impostos.codigo as imposto_codigo",
        "impostos.valor as imposto_valor",
        "produtos.is_editable",
        "produtos.tipo_produto_id",
        "tipo_produtos.descricao as tipo_produto",
        "produtos.incidencia_id",
        "incidencias.nome as incidencia"
      ).from("produtos")
        .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
        .leftJoin("tipo_produtos", "produtos.tipo_produto_id", "tipo_produtos.id")
        .leftJoin("incidencias", "produtos.incidencia_id", "incidencias.id")
        .where("produtos.is_active", true)
        .where('produtos.nome', 'like', "%" + value + "%")
        .where('produtos.nome', 'like', "%" + search + "%");


      const servicos = await Database.select(
        "produtos.id as servico_id",
        "produtos.nome as servico",
        "produtos.valor as servico_valor",
        "produtos.is_active",
        "produtos.imposto_id",
        "impostos.descricao as imposto",
        "impostos.codigo as imposto_codigo",
        "impostos.valor as imposto_valor",
        "produtos.is_editable",
        "produtos.tipo_produto_id",
        "tipo_produtos.descricao as tipo_produto",
        "produtos.incidencia_id",
        "incidencias.nome as incidencia"
      ).from("produtos")
        .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
        .leftJoin("tipo_produtos", "produtos.tipo_produto_id", "tipo_produtos.id")
        .leftJoin("incidencias", "produtos.incidencia_id", "incidencias.id")
        .where("produtos.is_active", true)
        .whereNot('produtos.nome', 'like', "%" + value + "%")
        .where('produtos.nome', 'like', "%" + search + "%")
        .where(function () {
          if (page) this.paginate(page, 30)
        });

      //console.log(servicos);

      for (let item of servicos) {

        result.push(item);
      }

      //console.log(result)

      return result;
    } catch (error) {
      //console.log(error)
    }

    return null;
  }




  async getServicoById(id, conta_id) {

    const servico = await Database.select(
      "produtos.id as servico_id",
      "produtos.nome as servico",
      "produtos.valor as servico_valor",
      "produtos.is_active",
      "produtos.imposto_id",
      "impostos.descricao as imposto",
      "impostos.codigo as imposto_codigo",
      "impostos.valor as imposto_valor",
      "produtos.is_editable",
      "produtos.tipo_produto_id",
      "tipo_produtos.descricao as tipo_produto",
      "produtos.incidencia_id",
      "incidencias.nome as incidencia"
    ).from("produtos")
      .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
      .leftJoin("tipo_produtos", "produtos.tipo_produto_id", "tipo_produtos.id")
      .leftJoin("incidencias", "produtos.incidencia_id", "incidencias.id")
      .where("produtos.id", id)
      .first();

    //console.log(servico);

    servico.imposto_valor = parseFloat(parseFloat(servico.imposto_valor / 100).toFixed(2));

    let linha_factura = await Database.select('*')
      .from('linha_facturas')
      .innerJoin('facturas', 'facturas.conta_id', conta_id)
      .where('linha_facturas.servico_id', servico.servico_id).first();

    servico.facturado = (linha_factura != null) ? true : false;

    return servico;
  }


  async getServicosByClasseTarifario(id, conta_id) {

    const result = [];

    let classe_tarifario = await ClasseTarifario.find(id)

    result.push(await this.getServicoById(classe_tarifario.produto_id, conta_id));

    //console.log(result);

    const servicos = await Database.select(
      "produtos.id as servico_id",
      "produtos.nome as servico",
      "produtos.valor as servico_valor",
      "produtos.is_active",
      "produtos.imposto_id",
      "impostos.descricao as imposto",
      "impostos.codigo as imposto_codigo",
      "impostos.valor as imposto_valor",
      "produtos.is_editable",
      "produtos.tipo_produto_id",
      "tipo_produtos.descricao as tipo_produto",
      "produtos.incidencia_id",
      "incidencias.nome as incidencia"
    ).from("produtos")
      .leftJoin("impostos", "impostos.id", "produtos.imposto_id")
      .leftJoin("tipo_produtos", "produtos.tipo_produto_id", "tipo_produtos.id")
      .leftJoin("incidencias", "produtos.incidencia_id", "incidencias.id")
      .innerJoin("produto_classe_tarifarios", "produtos.id", "produto_classe_tarifarios.produto_id")
      .where("produto_classe_tarifarios.classe_tarifario_id", id)
      .where("produtos.is_active", true)

    for (let item of servicos) {
      item.imposto_valor = parseFloat(parseFloat(item.imposto_valor / 100).toFixed(2));

      let linha_factura = await Database.select('*')
        .from('linha_facturas')
        .innerJoin('facturas', 'facturas.conta_id', conta_id)
        .where('linha_facturas.servico_id', item.servico_id).first();

      item.facturado = (linha_factura != null) ? true : false;

      result.push(item);
    }

    return result;
  }


}

module.exports = ClasseTarifarioRepository


