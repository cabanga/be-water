'use strict'
const ResponseMessage = use("App/Models/ResponseMessage")
const TipoIdentidade = use('App/Models/TipoIdentidade')

class tipoIdentidadeRepository {

    async getAll(page = null, search = null) {
        if (page) {
            const tipos = await TipoIdentidade.query()
            .where(function (params)  {
                if(search){ this.orWhere('nome', 'like', "%"+ search + "%" ) }
            })
            .orderBy('nome', 'asc')
            .paginate(page, 10)
            return tipos
        }

        const tipos = await TipoIdentidade.query()
        .where(function () {
            if(search){ this.orWhere('nome', 'like', "%"+ search + "%" ) }
        })
        .orderBy('nome', 'asc')
        .fetch()
        return {data: tipos} 
    }

    async create (data) {

        try {
            const tipo = await TipoIdentidade.create( data )
            return ResponseMessage.response( 200, "Tipo de identificação registado com sucesso.", tipo )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response( 201, "Falha não registar Tipo de identificação, por favor verifica os dados ", error)
        }
    }

    async findById (id) {
        const tipo = await TipoIdentidade.find(id)
        return ResponseMessage.response( 200, "", tipo )
    }

    async update (id, data) {
        try {
            await TipoIdentidade.query().where('id', id).update(data)
            let tipo = await this.findById(id)
            return ResponseMessage.response( 200, "Tipo de identificação actualizado com sucesso.", tipo )

        } catch (error) {
            console.log( error )
            return ResponseMessage.response(201,"Falha não actualizar Tipo de identificação, por favor verifica o erro",null)
        }
    }

    async delete (id) {
        try {
            await TipoIdentidade.query().where('id', id).update({ is_deleted: true })
            return ResponseMessage.response( 200, "Tipo de identificação eliminado com sucesso.", null )
        } catch (error) {
            console.log( error )
            return ResponseMessage.response( 201, "Falha ao eliminar Tipo de identificação", null )
        }
    }   

}

module.exports = tipoIdentidadeRepository