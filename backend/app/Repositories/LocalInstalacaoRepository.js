'use strict'

const DataResponse = use("App/Models/DataResponse");
const LocalInstalacao = use('App/Models/LocalInstalacao');
const Database = use("Database");


class LocalInstalacaoRepository {


    async getLigacoesFinaisByIdLocalInstacao(id) {

        let res = await Database.select(
            "local_instalacaos.id",
            "local_instalacaos.moradia_numero",
            "local_instalacaos.is_predio",
            "local_instalacaos.predio_id",
            "local_instalacaos.predio_nome",
            "local_instalacaos.predio_andar",
            "local_instalacaos.cil",
            "local_instalacaos.is_active",
            "local_instalacaos.latitude",
            "local_instalacaos.longitude",
            "local_instalacaos.saneamento_flag",
            "local_instalacaos.instalacao_sanitaria_qtd",
            "local_instalacaos.reservatorio_flag",
            "local_instalacaos.reservatorio_capacidade",
            "local_instalacaos.piscina_flag",
            "local_instalacaos.piscina_capacidade",
            "local_instalacaos.jardim_flag",
            "local_instalacaos.campo_jardim_id",
            "local_instalacaos.poco_alternativo_flag",
            "local_instalacaos.fossa_flag",
            "local_instalacaos.fossa_capacidade",
            "local_instalacaos.acesso_camiao_flag",
            "local_instalacaos.anexo_flag",
            "local_instalacaos.anexo_quantidade",
            "local_instalacaos.caixa_contador_flag",
            "local_instalacaos.estado_caixa_contador_id",
            "local_instalacaos.abastecimento_cil_id",
            "local_instalacaos.calibre_id",
            "ligacao_ramals.diamentro as ligacao_diametro",
            "ligacao_ramals.comprimento as ligacao_comprimento",
            "ligacao_ramals.profundidade as ligacao_profundidade",
            "ligacao_ramals.ponto_a_id as objecto_ligacao_id",
            "objecto_ligacao_ramals.descricao as objecto_ligacao",
            "objecto_ligacao_ramals.latitude as objecto_ligacao_latitude",
            "objecto_ligacao_ramals.longitude as objecto_ligacao_longitude",
            "tipo_objectos.descricao as tipo_objecto_ligacao",
            "local_instalacaos.rua_id",
            "ruas.nome as rua",
            "ruas.quarteirao_id",
            "quarteiraos.nome as quarteirao",
            "ruas.bairro_id",
            "bairros.nome as bairro",
            "bairros.distrito_id",
            "distritos.nome as distrito",
            "bairros.municipio_id",
            "municipios.nome as municipio",
            "municipios.provincia_id",
            "provincias.nome as provincia",
            "local_instalacaos.user_id",
            "users.nome as user",
            "local_instalacaos.created_at"
        )
            .from("local_instalacaos")
            .leftJoin("ligacao_ramals", "ligacao_ramals.local_instalacao_id", "local_instalacaos.id")
            .leftJoin("objecto_ligacao_ramals", "objecto_ligacao_ramals.id", "ligacao_ramals.ponto_a_id")
            .leftJoin("tipo_objectos", "tipo_objectos.id", "objecto_ligacao_ramals.tipo_objecto_id")
            .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
            .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
            .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
            .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
            .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
            .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
            .leftJoin("users", "users.id", "local_instalacaos.user_id")

            .where("local_instalacaos.rua_id", id)
            .orderBy("local_instalacaos.created_at", "DESC");

        console.log(res);

        return res;
    }

    async getLocalInstalacaoByPredioAndRua(predio_nome, rua_id) {

        return await Database.select(
            "local_instalacaos.id",
            "local_instalacaos.moradia_numero",
            "local_instalacaos.is_predio",
            "local_instalacaos.predio_id",
            "local_instalacaos.predio_nome",
            "local_instalacaos.predio_andar",
            "local_instalacaos.cil",
            "local_instalacaos.is_active",
            "local_instalacaos.latitude",
            "local_instalacaos.longitude",
            "local_instalacaos.saneamento_flag",
            "local_instalacaos.instalacao_sanitaria_qtd",
            "local_instalacaos.reservatorio_flag",
            "local_instalacaos.reservatorio_capacidade",
            "local_instalacaos.piscina_flag",
            "local_instalacaos.piscina_capacidade",
            "local_instalacaos.jardim_flag",
            "local_instalacaos.campo_jardim_id",
            "local_instalacaos.poco_alternativo_flag",
            "local_instalacaos.fossa_flag",
            "local_instalacaos.fossa_capacidade",
            "local_instalacaos.acesso_camiao_flag",
            "local_instalacaos.anexo_flag",
            "local_instalacaos.anexo_quantidade",
            "local_instalacaos.caixa_contador_flag",
            "local_instalacaos.estado_caixa_contador_id",
            "local_instalacaos.abastecimento_cil_id",
            "local_instalacaos.calibre_id",
            "ligacao_ramals.diamentro as ligacao_diametro",
            "ligacao_ramals.comprimento as ligacao_comprimento",
            "ligacao_ramals.profundidade as ligacao_profundidade",
            "ligacao_ramals.ponto_a_id as objecto_ligacao_id",
            "objecto_ligacao_ramals.descricao as objecto_ligacao",
            "objecto_ligacao_ramals.latitude as objecto_ligacao_latitude",
            "objecto_ligacao_ramals.longitude as objecto_ligacao_longitude",
            "tipo_objectos.descricao as tipo_objecto_ligacao",
            "local_instalacaos.rua_id",
            "ruas.nome as rua",
            "ruas.quarteirao_id",
            "quarteiraos.nome as quarteirao",
            "ruas.bairro_id",
            "bairros.nome as bairro",
            "bairros.distrito_id",
            "distritos.nome as distrito",
            "bairros.municipio_id",
            "municipios.nome as municipio",
            "municipios.provincia_id",
            "provincias.nome as provincia",
            "local_instalacaos.user_id",
            "users.nome as user",
            "local_instalacaos.created_at"
        )
            .from("local_instalacaos")
            .leftJoin("ligacao_ramals", "ligacao_ramals.local_instalacao_id", "local_instalacaos.id")
            .leftJoin("objecto_ligacao_ramals", "objecto_ligacao_ramals.id", "ligacao_ramals.ponto_a_id")
            .leftJoin("tipo_objectos", "tipo_objectos.id", "objecto_ligacao_ramals.tipo_objecto_id")
            .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
            .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
            .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
            .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
            .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
            .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
            .leftJoin("users", "users.id", "local_instalacaos.user_id")

            .where("is_predio", true)
            .where("predio_nome", predio_nome.trim())
            .where("rua_id", rua_id)
            .first();
    }


    async getLocalInstalacaoById(id) {

        return await Database.select(
            "local_instalacaos.id",
            "local_instalacaos.moradia_numero",
            "local_instalacaos.is_predio",
            "local_instalacaos.predio_id",
            "local_instalacaos.predio_nome",
            "local_instalacaos.predio_andar",
            "local_instalacaos.cil",
            "local_instalacaos.is_active",
            "local_instalacaos.latitude",
            "local_instalacaos.longitude",
            "local_instalacaos.saneamento_flag",
            "local_instalacaos.instalacao_sanitaria_qtd",
            "local_instalacaos.reservatorio_flag",
            "local_instalacaos.reservatorio_capacidade",
            "local_instalacaos.piscina_flag",
            "local_instalacaos.piscina_capacidade",
            "local_instalacaos.jardim_flag",
            "local_instalacaos.campo_jardim_id",
            "local_instalacaos.poco_alternativo_flag",
            "local_instalacaos.fossa_flag",
            "local_instalacaos.fossa_capacidade",
            "local_instalacaos.acesso_camiao_flag",
            "local_instalacaos.anexo_flag",
            "local_instalacaos.anexo_quantidade",
            "local_instalacaos.caixa_contador_flag",
            "local_instalacaos.estado_caixa_contador_id",
            "local_instalacaos.abastecimento_cil_id",
            "local_instalacaos.calibre_id",
            "ligacao_ramals.diamentro as ligacao_diametro",
            "ligacao_ramals.comprimento as ligacao_comprimento",
            "ligacao_ramals.profundidade as ligacao_profundidade",
            "ligacao_ramals.ponto_a_id as objecto_ligacao_id",
            "objecto_ligacao_ramals.descricao as objecto_ligacao",
            "objecto_ligacao_ramals.latitude as objecto_ligacao_latitude",
            "objecto_ligacao_ramals.longitude as objecto_ligacao_longitude",
            "tipo_objectos.descricao as tipo_objecto_ligacao",
            "local_instalacaos.rua_id",
            "ruas.nome as rua",
            "ruas.quarteirao_id",
            "quarteiraos.nome as quarteirao",
            "ruas.bairro_id",
            "bairros.nome as bairro",
            "bairros.distrito_id",
            "distritos.nome as distrito",
            "bairros.municipio_id",
            "municipios.nome as municipio",
            "municipios.provincia_id",
            "provincias.nome as provincia",
            "local_instalacaos.user_id",
            "users.nome as user",
            "local_instalacaos.created_at"
        )
            .from("local_instalacaos")
            .leftJoin("ligacao_ramals", "ligacao_ramals.local_instalacao_id", "local_instalacaos.id")
            .leftJoin("objecto_ligacao_ramals", "objecto_ligacao_ramals.id", "ligacao_ramals.ponto_a_id")
            .leftJoin("tipo_objectos", "tipo_objectos.id", "objecto_ligacao_ramals.tipo_objecto_id")
            .leftJoin("ruas", "ruas.id", "local_instalacaos.rua_id")
            .leftJoin("quarteiraos", "quarteiraos.id", "ruas.quarteirao_id")
            .leftJoin("bairros", "bairros.id", "ruas.bairro_id")
            .leftJoin("distritos", "distritos.id", "bairros.distrito_id")
            .leftJoin("municipios", "municipios.id", "bairros.municipio_id")
            .leftJoin("provincias", "provincias.id", "municipios.provincia_id")
            .leftJoin("users", "users.id", "local_instalacaos.user_id")
            .where("local_instalacaos.id", id)
            .first();

    }


    async validateLocalInstalacaoChanges(historico, actualizacao) {

        if (historico.instalacao_sanitaria_qtd != actualizacao.instalacao_sanitaria_qtd) return true;
        if (historico.reservatorio_flag != actualizacao.reservatorio_flag) return true;
        if (historico.reservatorio_capacidade != actualizacao.reservatorio_capacidade) return true;
        if (historico.piscina_flag != actualizacao.piscina_flag) return true;
        if (historico.piscina_capacidade != actualizacao.piscina_capacidade) return true;
        if (historico.jardim_flag != actualizacao.jardim_flag) return true;
        if (historico.campo_jardim_id != actualizacao.campo_jardim_id) return true;
        if (historico.poco_alternativo_flag != actualizacao.poco_alternativo_flag) return true;
        if (historico.fossa_flag != actualizacao.fossa_flag) return true;
        if (historico.fossa_capacidade != actualizacao.fossa_capacidade) return true;
        if (historico.acesso_camiao_flag != actualizacao.acesso_camiao_flag) return true;
        if (historico.anexo_flag != actualizacao.anexo_flag) return true;
        if (historico.anexo_quantidade != actualizacao.anexo_quantidade) return true;
        if (historico.caixa_contador_flag != actualizacao.caixa_contador_flag) return true;
        if (historico.abastecimento_cil_id != actualizacao.abastecimento_cil_id) return true;

        return false;
    }

    async setLocalInstalacaoChanges(historico) {

        //console.log("hist");

        let result = await this.getLocalInstalacaoById(historico.local_consumo_id);

        //console.log(result);

        result.instalacao_sanitaria_qtd = historico.instalacao_sanitaria_qtd;
        result.reservatorio_flag = historico.reservatorio_flag;
        result.reservatorio_capacidade = historico.reservatorio_capacidade;
        result.piscina_flag = historico.piscina_flag;
        result.piscina_capacidade = historico.piscina_capacidade;
        result.jardim_flag = historico.jardim_flag;
        result.campo_jardim_id = historico.campo_jardim_id;
        result.poco_alternativo_flag = historico.poco_alternativo_flag;
        result.fossa_flag = historico.fossa_flag;
        result.fossa_capacidade = historico.fossa_capacidade;
        result.acesso_camiao_flag = historico.acesso_camiao_flag;
        result.anexo_flag = historico.anexo_flag;
        result.anexo_quantidade = historico.anexo_quantidade;
        result.caixa_contador_flag = historico.caixa_contador_flag;
        result.abastecimento_cil_id = historico.abastecimento_cil_id;


        return result;
    }


    async generate_slug(str) {
        str = str.replace(/^\s+|\s+$/g, '')
        str = str.toLowerCase()
        let slug = ''
        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;"
        var to = "aaaaeeeeiiiioooouuuunc------"
        for (var i = 0, l = from.length; i < l; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
        }

        slug = str.replace(/[^a-z0-9 -]/g, '')
            .replace(/\s+/g, '-')
            .replace(/-+/g, '-')

        return slug
    }
}

module.exports = LocalInstalacaoRepository