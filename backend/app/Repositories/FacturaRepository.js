'use strict'

const ResponseMessage = use("App/Models/ResponseMessage")
const BaseRepository = use("App/Repositories/BaseRepository")
const RotaHeader = use('App/Models/RotaHeader')
const ClasseTarifario = use('App/Models/ClasseTarifario')
const Charge = use('App/Models/Charge')
const Leitura = use('App/Models/Leitura')
const Database = use("Database");

const moment = require("moment");


class AgendamentoRoteiroRepository extends BaseRepository {

  async charges() {
    const response = await RotaHeader
    .query()
    .with('charges')
    .orderBy('id', 'asc')
    .fetch()

    //.raw('SELECT  COUNT(charge.leitura_id) as quant, rota_header.id as rota_header_id, leitura.consumo, leitura.ultima_leitura, charge.leitura_id as leitura_id, charge.is_facturado as estado FROM  rota_headers as rota_header JOIN charges as charge  ON charge.rota_header_id = rota_header.id  JOIN leituras as leitura  ON charge.leitura_id = leitura.id  WHERE rota_header.id = 1 AND charge.is_facturado = FALSE GROUP BY (charge.leitura_id);')

    return {data: response}
  }

  async rotas(page = null, search = null) {
    if (page) {
      const response = await RotaHeader
      .query()
      .where(function ()  { if(search){ this.orWhere('descricao', 'like', "%"+ search + "%" ) } })
      .with('roteiros')
      .orderBy('id', 'asc')
      .paginate(page, 10)
      return response
    }

    const response = await RotaHeader
    .query()
    .where(function () { if(search){ this.orWhere('descricao', 'like', "%"+ search + "%" ) } })
    .with('roteiros')
    .orderBy('id', 'asc')
    .fetch()

    return {data: response}
  }


  async este_contador_tem_leituras(contador_id){
    const response = await Leitura
    .query()
    .where('contador_id', contador_id)
    .where('nao_leitura', false)
    .orderBy('id', 'asc')
    .fetch()

    let contrato = await Database
    .select(
      'contador.id as contador_id',
      'contrato.id as contrato_id',
      'contrato.media_consumo',
      'contador.ultima_leitura',
      'contador.numero_serie',
      'cliente.nome',
      'cliente.telefone',
      'cliente.morada'
    )
    .table('contadores as contador')
    .leftJoin('clientes as cliente','cliente.id', 'contador.cliente_id')
    .leftJoin('contratoes as contrato','cliente.id', 'contrato.cliente_id')
    .leftJoin('estado_contratoes as estado_contrato','estado_contrato.id', 'contrato.estado_contrato_id')
    .where('.contador.id', contador_id)
    .where('estado_contrato.slug', "ACTIVO")
    .first()

    let leituras = response.toJSON()

    if (leituras.length === 0 && contrato.media_consumo === 0) {
      return {ok :false, data: contrato}
    }

    return {ok :true, data: null}
  }


  async processar_pre_facturacao_leitura(leitura_id, data ){
    let consumo =             data.consumo
    let data_ultima_leitura = data.data_ultima_leitura
    let data_leitura =        data.data_leitura
    let classe_tarifario_id = data.classe_tarifario_id
    let classe_tarifario =    {}

    if (classe_tarifario_id) {
      classe_tarifario = await this.busca_classe_tarifario_pelo_id(classe_tarifario_id)
    } else {
      classe_tarifario = await this.busca_classe_tarifario(consumo)
    }

    let dias = await this.busca_dias_entre_duas_datas(data_leitura, data_ultima_leitura)
    let dias_tarifa_fixa = (Number(dias) + 1)/30
    let tarifa_fixa = (dias_tarifa_fixa * classe_tarifario.tarifa_fixa_mensal)

    let agua = {
      servico:          'Água',
      quantidade:       consumo,
      unidade:          'm3',
      tarifa_unitaria:  classe_tarifario.tarifa_variavel,
      sub_total:        (consumo * classe_tarifario.tarifa_variavel),
      total:            (consumo * classe_tarifario.tarifa_variavel)
    }

    await this.criar_charges(leitura_id, agua)

    let t_fixa = {
      servico:          'Tarifa Fixa',
      quantidade:       dias_tarifa_fixa,
      unidade:          'meses',
      tarifa_unitaria:  classe_tarifario.tarifa_fixa_mensal,
      sub_total:        '',
      total:            tarifa_fixa
    }

    await this.criar_charges(leitura_id, t_fixa)

    // =================== CALCULAR MEDIA DE CONSUMO DESTA FACTURA ================================
    let media_mensal_deste_consumo =  (consumo / (dias + 1)) * 30

    let main_calc = {
      media_mensal_deste_consumo: media_mensal_deste_consumo,
      classe_tarifario:           classe_tarifario.descricao,
      total:                      (agua.total + t_fixa.total),
      tarifa_agua:                agua,
      tarifas_fixas:              t_fixa
    }

    return main_calc


    /*
    if(tem_sameamento){
      let valor_consumo = (consumo * classe_tarifario.tarifa_variavel)
      valor_sameamento = (valor_consumo * 80)/100
    }

    let sameamento_variavel = {
      servico: 'Sameamento Variável',
      quantidade: '',
      unidade: '%',
      tarifa_unitaria: 80,
      sub_total: '',
      total: valor_sameamento
    }

    let sameamento_fixa = {
      servico: 'Sameamento Fixa',
      quantidade: dias_tarifa_fixa,
      unidade: 'meses',
      tarifa_unitaria: classe_tarifario.tarifa_fixa_mensal,
      sub_total: '',
      total: tarifa_fixa
    }

    if (tem_sameamento) {
      itens_da_factura.push(sameamento_variavel)
      itens_da_factura.push(sameamento_fixa)
    }
    */

    //itens_da_factura.push(main_calc)

  }

  async criar_charges(leitura_id, object_calculado){

    var response = await Database
      .select(
        'cliente.nome as cliente_nome',
        'cliente.telefone',
        'cliente.morada',
        'contador.id as contador_id',
        'conta.id as conta_id',
        'roteiro.id as roteiro_id',
        'roteiro.local_consumo_id',
        'rota.id as rota_id',
        'leitura.consumo',
        'leitura.data_leitura',
        'leitura.data_ultima_leitura',
        'classe_tarifario.id as classe_tarifario_id',
        'classe_tarifario.descricao as classe_tarifario_descricao',
        'produto.id as produto_id',
        'produto.nome as produto_nome'
      )
      .table("leituras as leitura")
      .innerJoin(
        'contadores as contador',
        'leitura.contador_id', 'contador.id'
      )
      .innerJoin(
        'clientes as cliente',
        'cliente.id', 'contador.cliente_id'
      )
      .innerJoin(
        'contas as conta',
        'cliente.id', 'conta.cliente_id'
      )
      .innerJoin(
        'rota_runs as roteiro',
        'leitura.rota_run_id', 'roteiro.id'
      )
      .innerJoin(
        'rota_headers as rota',
        'roteiro.rota_header_id', 'rota.id'
      )
      .innerJoin(
        'classe_tarifarios as classe_tarifario',
        'leitura.classe_tarifario_id', 'classe_tarifario.id'
      )
      .innerJoin(
        'produtos as produto',
        'classe_tarifario.produto_id', 'produto.id'
      )

      .where("leitura.id", leitura_id)
      .where("leitura.nao_leitura", false)
      .first()

      let data = {
        conta_id: response.conta_id,
        contador_id: response.contador_id,
        produto_id: response.produto_id,
        local_consumo_id: response.local_consumo_id,
        valor: object_calculado.total,
        valorOriginal: object_calculado.total,
        leitura_id: leitura_id,
        periodo: "",
        rota_header_id: response.rota_id
      }

      await Charge.create(data)
      await Leitura.query().where('id', leitura_id).update({foi_facturado: true})
  }

  async processar_pre_facturacao_nao_leitura(){

  }

  async busca_classe_tarifario(consumo){
    const response = await ClasseTarifario
    .query()
    .orderBy('id', 'asc')
    .fetch()

    let classe_tarifarios = response.toJSON()
    let classe_tarifario = {}

    if( 0 < consumo && consumo <= 5  ){
      classe_tarifario = classe_tarifarios.find(item => item.descricao == 'Doméstico Social')
    }

    if( 5 < consumo && consumo <= 10  ){
      classe_tarifario = classe_tarifarios.find(item => item.descricao == 'Domésticos Escalão 1')
    }

    if( 10 < consumo  ){
      classe_tarifario = classe_tarifarios.find(item => item.descricao == 'Domésticos Escalão 2')
    }

    return classe_tarifario
  }

  async busca_classe_tarifario_pelo_id(id){
    const response = await ClasseTarifario.find(id)
    return response.toJSON()
  }

  async busca_dias_entre_duas_datas(current_date, last_date ){
    let data_ultima_leitura =  moment(last_date, 'DD-MM-YYYY').format()
    let data_leitura = moment(current_date, 'DD-MM-YYYY').format()

    var date1 = moment( data_leitura )
    var date2 = moment( data_ultima_leitura )
    var diff = date2.diff(date1)
    return Math.abs( Math.ceil(diff / (1000 * 3600 * 24)) )
  }

}

module.exports = AgendamentoRoteiroRepository
