"use strict";

/*
|--------------------------------------------------------------------------
| PermissionSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");
const Permission = use("App/Models/Permission");

class PermissionSeeder {
  async run() {
    /**
     * Permissões das operacoes sobre Permissions
     * */

    const permissions = [
      {
        slug: "permission_update_to_role",
        name: "EDITAR PERMISSÕES DO PERFIL",
        description: "Permissão para editar permissões de um perfil"
      },
      {
        slug: "listar_utilizadores",
        name: "Listar Utilizadores",
        description: "Permissão para listar utilizadores"
      },
      {
        slug: "registar_utilizador",
        name: "Registar Utilizador",
        description: "Permissão para Registar utilizador"
      },
      {
        slug: "editar_utilizador",
        name: "Editar Utilizador",
        description: "Permissão para Editar utilizador"
      },
      {
        slug: "eliminar_utilizador",
        name: "Eliminar Utilizador",
        description: "Permissão para Eliminar utilizador"
      },
      {
        slug: "redefinir_senha_utilizador",
        name: "Redefinir Senha Utilizador",
        description: "Permissão para Redefinir senha determiado utilizador"
      },
      {
        slug: "listar_clientes",
        name: "Listar Clientes",
        description: "Permissão para listar clientes"
      },
      {
        slug: "registar_cliente",
        name: "Registar Clientes",
        description: "Permissão para registar clientes"
      },
      {
        slug: "editar_cliente",
        name: "Editar Clientes",
        description: "Permissão para Editar cliente"
      },
      {
        slug: "eliminar_cliente",
        name: "Eliminar Clientes",
        description: "Permissão para Eliminar Cliente"
      },
      {
        slug: "conta_corrente",
        name: "conta corrente Clientes",
        description: "Permissão para conta corrente de clientes"
      },
      {
        slug: "listar_orcamentos",
        name: "Listar Orçamentos",
        description: "Permissão para Listar Orçamentos"
      },
      {
        slug: "registar_orcamentos",
        name: "Registar Orçamentos",
        description: "Permissão para Registra Orçamentos"
      },
      {
        slug: "tarefas",
        name: "tarefas",
        description: "Permissão para tarefas"
      },
      {
        slug: "listar_inventarios",
        name: "Listar inventarios",
        description: "Permissão para Listar inventarios"
      },
      {
        slug: "listar_facturacao",
        name: "Listar Facturação",
        description: "Permissão para Listar Facturação"
      },
      {
        slug: "registar_facturacao",
        name: "Registar Facturação",
        description: "Permissão para Registar Facturação"
      },
      {
        slug: "gerar_factura",
        name: "Gerar factura",
        description: "Permissão para gerar factura"
      },
      {
        slug: "anular_factura",
        name: "Anular factura",
        description: "Permissão para Anular factura"
      },
      {
        slug: "nota_credito_factura",
        name: "Gerar Nota de Credito factura",
        description: "Permissão para Gerar Nota de Credito factura"
      },
      {
        slug: "imprimir_factura",
        name: "Imprimir factura",
        description: "Permissão para Imprimir Factura"
      },
      {
        slug: "listar_armazens",
        name: "listar armazens",
        description: "Permissão para listar armazens"
      },
      {
        slug: "business_intelligence",
        name: "Business Intelligence",
        description: "Permissão para Business Intelligence"
      },
      {
        slug: "listar_compras",
        name: "listar compras",
        description: "Permissão para listar compras"
      },
      {
        slug: "listar_produtos",
        name: "listar produtos",
        description: "Permissão para listar produtos"
      },
      {
        slug: "listar_projectos",
        name: "listar projectos",
        description: "Permissão para listar projectos"
      },
      {
        slug: "gerar_saft",
        name: "gerar saft",
        description: "Permissão para gerar saft"
      },
      {
        slug: "listar_permissions",
        name: "listar permissões",
        description: "Permissão para listar permissões"
      },
      {
        slug: "listar_roles",
        name: "listar roles",
        description: "Permissão para listar roles"
      },
      {
        slug: "listar_impostos",
        name: "listar impostos",
        description: "Permissão para listar impostos"
      },
      {
        slug: "configurar_empresa",
        name: "configurar empresa",
        description: "Permissão para configurar empresa"
      },
      {
        slug: "listar_documentos",
        name: "listar documentos",
        description: "Permissão para listar documentos"
      },
      {
        slug: "register_serie",
        name: "register serie",
        description: "Permissão para register serie"
      },
      {
        slug: "listar_series",
        name: "listar series",
        description: "Permissão para listar series"
      },
      {
        slug: "listar_forma_pagamentos",
        name: "Listar formas de pagamentos",
        description: "Permissão para Listar formas de pagamentos"
      },
      {
        slug: "listar_moedas",
        name: "listar moedas",
        description: "Permissão para listar moedas"
      },
      {
        slug: "listar_bancos",
        name: "listar bancos",
        description: "Permissão para listar bancos"
      },
      {
        slug: "crm",
        name: "crm",
        description: "Permissão para crm"
      },
      {
        slug: "logistica",
        name: "Logística",
        description: "Permissão para logistica"
      },
      {
        slug: "vendas",
        name: "vendas",
        description: "Permissão para vendas"
      },
      {
        slug: "operacoes",
        name: "Operações",
        description: "Permissão para Operações"
      },
      {
        slug: "editar_utilizador",
        name: "Editar Utilizador",
        description: "Permissão para Editar utilizador"
      },
      {
        slug: "configuracoes",
        name: "Configurações",
        description: "Permissão para Configurações"
      },
      {
        slug: "stock_movimento",
        name: "stock movimento",
        description: "Permissão para stock movimento"
      },
      {
        slug: "tarifarios",
        name: "tarifarios",
        description: "Permissão para tarifarios"
      },
      {
        slug: "facturacao_charge",
        name: "facturacao charge",
        description: "Permissão para facturacao charge"
      },
      {
        slug: "contratos",
        name: "contratos de clientes",
        description: "Permissão para contratos de clientes"
      },
      {
        slug: "conta_cliente",
        name: "conta cliente",
        description: "Permissão para contratos de conta clientes"
      },
      {
        slug: "facturacao_automatica",
        name: "Facturação Automatica",
        description: "Permissão Facturação Automaticas"
      },
      {
        slug: "gerar_recibos",
        name: "Gerar Recibos",
        description: "Permissão para Gerar Recibos das facturas não pagas"
      },
      {
        slug: "movimento_caixa",
        name: "Movimento Caixa",
        description: "Permissão para Movimento de caixa"
      },
      {
        slug: "listar_contadores",
        name: "listar_contadores",
        description: "Permite configurar contadores"
      },
      {
        slug: "rotas",
        name: "rotas",
        description: "Permite acessar meu rotas"
      },
      {
        slug: "listar_rotas",
        name: "listar_rotas",
        description: "Permite configura rotas.."
      },
      {
        slug: "configurar_tipo_servico",
        name: "configurar_tipo_servico",
        description: "Configurar Tipo de Servico"
      },
      {
        slug: "configurar_tipo_ramal",
        name: "Configurar Tipos de Ramais",
        description: "Permite configurar tipos de ramais"
      },
      {
        slug: "configurar_estado_contador",
        name: "Configurar Estado Contador",
        description: "Permite configurar estados de um contador"
      },
      {
        slug: "configurar_objecto_tecnico",
        name: "configurar_objecto_tecnico",
        description: "Permite dar acesso as configurações de Objectos Técnicos"
      },
      {
        slug: "operacoes_locais",
        name: "operacoes_locais",
        description: "operacoes_locais"
      },
      {
        slug: "listar_plano_preco",
        name: "listar_plano_preco",
        description: "listar_plano_preco"
      },
      {
        slug: "configurar_tarifario",
        name: "configurar_tarifario",
        description: "Permite configurar tarifários"
      },
      {
        slug: "registar_contrato",
        name: "registar contrato",
        description: "Permite registar contrato"
      },
      {
        slug: "contrato",
        name: "contrato",
        description: "Permite ter acesso ao menu contrato"
      },
      {
        slug: "configurar_tipo_contrato",
        name: "Configurar Tipo de Contrator",
        description: "Permite configurar tipo de contrato, criar, editar, listar"
      },
      {
        slug: "configurar_loja",
        name: "Configurar Loja",
        description: "Permite configurar lojas"
      },
      {
        slug: "configurar_direccao",
        name: "Configurar Direcção",
        description: "Permite configurar direcção"
      },
      {
        slug: "gestao_ramal",
        name: "Gestão de Ramal",
        description: "Permite fazer a gestão de ramais"
      },
      {
        slug: "gestao_tarifario",
        name: "Gestão de Tarifários",
        description: "Permite fazer a gestão de tarifários, classe tarifários, produto classe tarifários"
      },
      {
        slug: "listar_contratos",
        name: "listar_contratos",
        description: "Permite listar contratos"
      },
      {
        slug: "configurar_tipo_contador",
        name: "Configurar Tipo Contador",
        description: "Permite adicionar, editar Tipo Contador"
      },
      {
        slug: "configurar_fabricante",
        name: "Configurar Fabricante",
        description: "Permite adicionar, editar Fabricante"
      },
      {
        slug: "configurar_classe_precisao",
        name: "Configurar Classe Precisão",
        description: "Permite adicionar, editar Classe Precisão"
      },
      {
        slug: "configurar_medicao",
        name: "Configurar Modelo",
        description: "Permite adicionar, editar Modelo de Contador"
      },
      {
        slug: "configurar_marca",
        name: "Configurar Marca",
        description: "Permite adicionar, editar Marca de Contador"
      },
      {
        slug: "configurar_tipo_registo",
        name: "Configurar Tipo Registo",
        description: "Permite cadastrar, editar Tipo Registo de um Contador"
      },
      {
        slug: "configurar_contador",
        name: "Configurar Contador",
        description: "Permite ver todas as configurações do Contador"
      },
      {
        slug: "configurar_tipo_ordem_servico",
        name: "Configurar Tipo Ordem Serviço",
        description: "Permite adicionar, editar Tipo Ordem Serviço"
      },
      {
        slug: "configurar_tipo_facturacao",
        name: "Configurar Tipo Facturação",
        description: "Permite adicionar, editar Tipo Facturação"
      },
      {
        slug: "configurar_estado_conta",
        name: "Configurar Estado Conta",
        description: "Permite adicionar, editar Estado Conta de clientes"
      },
      {
        slug: "leituras",
        name: "leituras",
        description: "leituras"
      },
      {
        slug: "nao_visitas",
        name: "nao_visitas",
        description: "nao_visitas"
      },
      {
        slug: "ocorrencias",
        name: "ocorrencias",
        description: "ocorrencias"
      },
      {
        slug: "configurar_tipo_ocorrencia",
        name: "Configurar Tipo de Ocorrência",
        description: "Perrmite adicionar, alterar tipo de ocorrência"
      },
      {
        slug: "configurar_tipo_nao_visita",
        name: "Configurar Tipo Não Visita",
        description: "Permite adicionar, alterar tipo de não visita"
      }
    ]
    
    for (var i = 0; i < permissions.length; i++) {

      const verifyIfExist = await Permission.query()
        .where("slug", permissions[i].slug)
        .getCount();

      if (verifyIfExist <= 0) {

        await Factory.model("App/Models/Permission").create({
          slug: permissions[i].slug,
          name: permissions[i].name,
          description: permissions[i].description
        });

      }

    }


  }
}

module.exports = PermissionSeeder;
