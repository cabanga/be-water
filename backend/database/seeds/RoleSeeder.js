'use strict'

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')

class RoleSeeder {
  async run() {
    /**
     * Permissões das operacoes sobre Roles
     * */
    await Factory
      .model('App/Models/Permission')
      .create({slug: 'role_criar', name: 'CRIAR PERFIL', description: 'Permissão para criar perfil'})

    await Factory
      .model('App/Models/Permission')
      .create({slug: 'role_editar', name: 'EDITAR PERFIL', description: 'Permissão para editar perfil'})

    await Factory
      .model('App/Models/Permission')
      .create({slug: 'role_listar', name: 'LISTAR PERFIL', description: 'Permissão para listar perfil'})

    await Factory
      .model('App/Models/Permission')
      .create({slug: 'role_eliminar', name: 'ELIMINAR PERFIL', description: 'Permissão para eliminar perfil'})


    // Fim das permissoes
  }
}

module.exports = RoleSeeder
