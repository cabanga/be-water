'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturaUtilitieSchema extends Schema {
  up () {
    this.table('linha_factura_utilities', (table) => {
      // alter table 
      table.double("total").nullable().alter(); 
      table.integer('quantidade').nullable().alter();
      table.double('valor').nullable().alter();
      table.double("valor_imposto").nullable().alter();
      table.double("linhaTotalSemImposto").nullable().alter();
      table.double("valor_desconto").nullable().alter();
  
      table.integer('tarifario_id').unsigned().nullable().alter();
      table.integer('plano_preco_id').unsigned().nullable().alter()
      table.integer('user_id').unsigned().nullable().alter()
      table.integer('imposto_id').unsigned().nullable().alter()
    })
  }

  down () {
    this.table('linha_factura_utilities', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaFacturaUtilitieSchema
