'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojaFormaPagamentoSchema extends Schema {
  up () {
    this.table('loja_forma_pagamentos', (table) => {
      // alter table
      table.boolean("is_active").nullable().defaultTo(true);
    })
  }

  down () {
    this.table('loja_forma_pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LojaFormaPagamentoSchema
