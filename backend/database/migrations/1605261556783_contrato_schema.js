




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up() {
    this.table('contratoes', (table) => {
      // alter table

      table.integer('tipo_juro_id').unsigned().references('id').inTable('tipo_juros').after('tipo_contracto_id')
      table.boolean('morada_correspondencia_flag').defaultTo(false).after('estado_id')
      table.integer('nivel_sensibilidade_id').unsigned().references('id').inTable('nivel_sensibilidades').after('morada_correspondencia_flag')
      table.integer('tipologia_cliente_id').unsigned().references('id').inTable('tipologia_clientes').after('nivel_sensibilidade_id')
      table.integer('objecto_contrato_id').unsigned().references('id').inTable('objecto_contratoes').after('tipologia_cliente_id')
      table.integer('media_consumo_id').unsigned().references('id').inTable('media_consumos').after('objecto_contrato_id')
      table.integer('motivo_recisao_id').unsigned().references('id').inTable('motivo_recisaos').after('media_consumo_id')

    })
  }

  down() {
    this.table('contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContratoesSchema



