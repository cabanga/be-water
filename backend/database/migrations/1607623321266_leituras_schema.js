'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeiturasSchema extends Schema {
  up () {
    this.table('leituras', (table) => {
      //table.enum('estado', {0: 'Por realizar', 1: 'Realizado', 2: 'Facturado'}).defaultTo(0)
      table.integer("estado").notNullable().defaultTo(0)
    })
  }

  down () {
    this.table('leituras', (table) => {
      table.dropColumn('estado')
    })
  }
}

module.exports = LeiturasSchema
