'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContagensSchema extends Schema {
  up () {
    this.create('contagens', (table) => {
      table.increments()     
      
      table.integer('rota_run_id').unsigned().references('id').inTable('rota_runs') 
      table.integer('contador_id').unsigned().references('id').inTable('contadores') 
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.string('leitura',80).nullable()
      table.string('ultima_leitura',80).nullable()      
      table.timestamps()
    })
  }

  down () {
    this.drop('contagens')
  }
}

module.exports = ContagensSchema
