'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CdmaEquipamentoSchema extends Schema {
  up () {
    this.create('cdma_equipamentos', (table) => {
      table.increments()
	  table.integer('IDEquipamentoAKEY', 11).notNullable()
	  table.string('DataEntrada', 30).nullable()
	  table.integer('IDAgente', 11).notNullable()
	  table.integer('Fabricante', 11).notNullable()
	  table.integer('IDModelo', 11).notNullable()
	  table.string('Num_Serie', 15).nullable()
	  table.string('A_Key', 20).nullable()
	  table.string('Data_Criacao', 30).nullable()
	  table.integer('Status', 3).nullable()
	  table.string('Data_Status', 30).nullable()
	  table.integer('IDFornecedor', 11).nullable()
	  table.string('SPCCode', 10).nullable()
	  table.string('LockCode', 10).nullable()
	  table.string('MINCode', 20).nullable()
	  table.integer('AgenciaFilialID', 11).nullable()
	  table.string('A12', 10).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('cdma_equipamentos')
  }
}

module.exports = CdmaEquipamentoSchema
