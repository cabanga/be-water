'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogEstadoContaSchema extends Schema {
  up () {
    this.create('log_estado_contas', (table) => {
      table.increments()
	  table.integer('conta_id').unsigned().references('id').inTable('contas')
	  table.integer('estado_anterior')
	  table.integer('estado_novo')
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('log_estado_contas')
  }
}

module.exports = LogEstadoContaSchema
