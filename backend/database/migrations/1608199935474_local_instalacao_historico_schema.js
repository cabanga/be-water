'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaoHistoricoSchema extends Schema {
  up () {
    this.table('local_instalacao_historicos', (table) => {
      // alter table 

      table.longText('historico').alter();
      table.longText('actualizacao').alter();
    })
  }
  
  down () {
    this.table('local_instalacao_historicos', (table) => {

      // reverse alternations
    })
  }
}

module.exports = LocalInstalacaoHistoricoSchema
