'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalConsumosSchema extends Schema {
  up () {
    this.table('local_consumos', (table) => {
      // alter table
	table.integer('contrato_id').unsigned().references('id').inTable('contratoes').after('conta_id')
    })
  }

  down () {
    this.table('local_consumos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LocalConsumosSchema
