'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      // alter table
	   table.integer('role_id').nullable().after('password');
	   table.integer('empresa_id').nullable().after('role_id');
	   table.boolean('status').notNullable().after('empresa_id');
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = UsersSchema
