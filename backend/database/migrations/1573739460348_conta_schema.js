'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContaSchema extends Schema {
  up () {
    this.table('contas', (table) => {
      // alter table
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();
    })
  }

  down () {
    this.table('contas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContaSchema
