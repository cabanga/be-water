'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresSchema extends Schema {
  up () {
    this.table('contratoes', (table) => {
      table.double("media_consumo").notNullable().defaultTo(0)
    })
  }
  down () {
    this.table('contratoes', (table) => {
      table.dropColumn('media_consumo')
    })
  }
}

module.exports = ContadoresSchema
