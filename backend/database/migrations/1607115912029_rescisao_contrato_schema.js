




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RescisaoContratoesSchema extends Schema {
  up() {
    this.table('rescisao_contratoes', (table) => {
      // alter table

      table.integer('user_id').unsigned().references('id').inTable('users').after('data_rescisao')

    })
  }

  down() {
    this.table('rescisao_contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RescisaoContratoesSchema



