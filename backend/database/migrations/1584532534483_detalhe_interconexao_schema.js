'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DetalheInterconexaoSchema extends Schema {
  up () {
    this.table('detalhe_interconexaos', (table) => {
      // alter table
      table.integer('factura_id').nullable().unsigned().index('factura_id_detalhe_interconexao').references('id').inTable('facturas').after("billing_operator");
    })
  }

  down () {
    this.table('detalhe_interconexaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = DetalheInterconexaoSchema
