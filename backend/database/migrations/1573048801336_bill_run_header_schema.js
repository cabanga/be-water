'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BillRunHeaderSchema extends Schema {
  up () {
    this.table('bill_run_headers', (table) => {
      // alter table
      table.integer('tecnologia_id').unsigned().nullable().references('id').inTable('tecnologias').onUpdate('CASCADE').index();
    })
  }

  down () {
    this.table('bill_run_headers', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BillRunHeaderSchema
