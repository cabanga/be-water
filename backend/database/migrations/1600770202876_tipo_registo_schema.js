'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoRegistoSchema extends Schema {
  up () {
    this.create('tipo_registos', (table) => {
      table.increments()
      table.string('descricao')
      table.string('slug')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_registos')
  }
}

module.exports = TipoRegistoSchema
