'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeriesDocumentosSchema extends Schema {
  up () {
    this.hasTable('users').then(function(exists) {
      if (!exists) {

        this.create('series_documentos', (table) => {
          table.increments()
          table.integer('loja_id').nullable().unsigned().index()
          table.foreign('loja_id').references('id').on('lojas').onDelete('cascade')

          table.integer('serie_id').nullable().unsigned().index()
          table.foreign('serie_id').references('id').on('series').onDelete('cascade')

          table.integer('documento_id').nullable().unsigned().index()
          table.foreign('documento_id').references('id').on('documentos').onDelete('cascade')

          table.boolean("is_active").default(false)

          table.timestamps()
        })

      }
    })
  }

  down () {
    this.hasTable('users').then(function(exists) {
    if (exists) {
        this.drop('series_documentos')
      }
    })
  }
}

module.exports = SeriesDocumentosSchema




