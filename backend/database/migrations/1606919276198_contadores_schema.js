'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresSchema extends Schema {
  up () {
    this.table('contadores', (table) => {
      table.dropColumn('ultima_leitura')
    })
  }

  down () {
    this.table('contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContadoresSchema
