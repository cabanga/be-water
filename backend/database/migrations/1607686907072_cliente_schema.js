'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClienteSchema extends Schema {
  up () {
    this.table('clientes', (table) => {
      // alter table 
      table.integer('gestor_cliente_id').nullable().unsigned().references('id').inTable('users').after('user_id')
    })
  }
  
  down () {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClienteSchema
