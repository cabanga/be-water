'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MunicipiosSchema extends Schema {
  up () {
    this.table('municipios', (table) => {
      // alter table
      table.boolean('has_distrito').defaultTo(false).after('nome')
    })
  }

  down () {
    this.table('municipios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = MunicipiosSchema
