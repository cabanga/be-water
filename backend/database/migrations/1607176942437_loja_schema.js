'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojasSchema extends Schema {
  up () {
    this.table('lojas', (table) => {

      table.integer('tipologia_servico_id').unsigned().references('id').inTable('tipologia_servicos').after('municipio_id')

    })
  }

  down () {
    this.table('lojas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LojasSchema
