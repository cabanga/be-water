'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogsContadoresSchema extends Schema {
  up () {
    this.table('logs_contadores', (table) => {
      // alter table
      table.integer('estado_contador_id').unsigned().references('id').inTable('estado_contadores').after('contador_id')
      table.string('ultima_leitura').after('estado_contador_id')
      table.integer('tipo_registo_id').unsigned().references('id').inTable('tipo_registos').after('ultima_leitura')
      table.string('observacao_old').after('estado_contador_id')
      table.string('observacao_new').after('observacao_old')
    })
  }

  down () {
    this.table('logs_contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LogsContadoresSchema
