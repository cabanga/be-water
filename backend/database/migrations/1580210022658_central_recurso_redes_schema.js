'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CentralRecursoRedesSchema extends Schema {
  up () {
    this.table('central_recurso_redes', (table) => {
      // alter table
	table.integer('provincia_id').unsigned().references('id').inTable('filials').after('descricao')
    })
  }

  down () {
    this.table('central_recurso_redes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CentralRecursoRedesSchema
