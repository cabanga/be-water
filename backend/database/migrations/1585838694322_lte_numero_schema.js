'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LteNumeroSchema extends Schema {
  up () {
    this.create('lte_numeros', (table) => {
      table.increments()
	  table.integer('numero').notNullable().unique()
	  table.integer('filial_id').unsigned().references('id').inTable('filials')
	  table.integer('status').defaultTo(0)
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('lte_numeros')
  }
}

module.exports = LteNumeroSchema
