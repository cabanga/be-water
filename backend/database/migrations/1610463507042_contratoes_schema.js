'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up () {
    this.table('contratoes', (table) => {
      table.integer('leitura').nullable().default(0)
    })
  }

  down () {
    this.table('contratoes', (table) => {
      table.dropColumn('leitura')
    })
  }
}

module.exports = ContratoesSchema
