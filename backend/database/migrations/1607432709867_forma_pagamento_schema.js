'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FormaPagamentoSchema extends Schema {
  up () {
    this.table('forma_pagamentos', (table) => {
      // alter table
      table.boolean('usar_banco').defaultTo(false).after('descricao')
    })
  }

  down () {
    this.table('forma_pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FormaPagamentoSchema
