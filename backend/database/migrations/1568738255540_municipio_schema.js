'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MunicipioSchema extends Schema {
  up () {
    this.create('municipios', (table) => {
      table.increments()
      table.string("nome", 50).notNullable();
      table.boolean('is_active').nullable().defaultTo(true)

      table.integer('provincia_id').nullable().unsigned().index();
      table.foreign('provincia_id').references('id').on('provincias').onDelete('cascade')

      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('municipios')
  }
}

module.exports = MunicipioSchema
