'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RotaHeaderSchema extends Schema {
  up () {
    this.create('rota_headers', (table) => {
      table.increments()

      table.string("nome", 50).notNullable();
      table.integer("periodo", 50).nullable();      

      table.integer('filial_id').unsigned().references('id').inTable('filials')      
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.integer('estado_rota_id').unsigned().references('id').inTable('estado_rotas')

      table.timestamps()
    })
  }

  down () {
    this.drop('rota_headers')
  }
}

module.exports = RotaHeaderSchema
