'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipologiaClientesSchema extends Schema {
  up () {
    this.table('tipologia_clientes', (table) => {
      // alter table
      table.string('juro_mora').after('slug')
      table.boolean('sujeito_corte').after('juro_mora')
      table.integer('nivel_sensibilidade_id').unsigned().references('id').inTable('nivel_sensibilidades').after('sujeito_corte')
      table.integer('caucao').after('nivel_sensibilidade_id')
    })
  }

  down () {
    this.table('tipologia_clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TipologiaClientesSchema
