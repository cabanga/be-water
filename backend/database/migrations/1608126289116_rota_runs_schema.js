'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RotaRunsSchema extends Schema {
  up () {
    this.table('rota_runs', (table) => {
      table.boolean("nao_leitura").notNullable().defaultTo(false)
      table.string("motivo")
    })
  }

  down () {
    this.table('rota_runs', (table) => {
      table.dropColumn('nao_leitura')
      table.dropColumn('motivo')
    })
  }
}

module.exports = RotaRunsSchema
