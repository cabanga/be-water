'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoOrdemServicoSchema extends Schema {
  up () {
    this.create('estado_ordem_servicos', (table) => {
      table.increments()
      
      table.string('nome', 80).notNullable().unique()
      table.string('slug', 80).notNullable().unique()
      table.integer('parent_id')
      table.integer('user_id').unsigned().references('id').inTable('users')
      
      table.timestamps()
    })
  }

  down () {
    this.drop('estado_ordem_servicos')
  }
}

module.exports = EstadoOrdemServicoSchema
