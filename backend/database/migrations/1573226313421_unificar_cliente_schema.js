'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UnificarClienteSchema extends Schema {
  up () {
    this.create('unificar_clientes', (table) => {
      table.increments()
      table.integer("id_unificar_cliente").nullable()
      table.string('nome_cliente_unificar',200).nullable()
      table.integer("id_manter_cliente").nullable()
      table.string("nome_cliente_manter", 200).nullable()
      table.string("unificado").nullable()
      table.integer("user_id").unsigned().notNullable().references("id").inTable("users").onUpdate("CASCADE").index("user_id_index")
      table.timestamps();


    })
  }

  down () {
    this.drop('unificar_clientes')
  }
}

module.exports = UnificarClienteSchema
