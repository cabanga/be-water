'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StokeMovimentoSchema extends Schema {
  up () {
    this.create('stoke_movimentos', (table) => {
      table.increments()
	  table.string('tipo_doc', 10).nullable();
	  table.datetime('data').nullable();
	  table.integer('documento_id').unsigned().nullable().references('id').inTable('documentos').onUpdate('CASCADE').index('documento_id_index');
	  table.integer('artigo').nullable();
	  table.integer('quantidade', 10).nullable();
	  table.string('movimento', 30).nullable();
	  table.string('armazem', 50).nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('stoke_movimentos')
  }
}

module.exports = StokeMovimentoSchema
