'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FlatRateServicoSchema extends Schema {
  up () {
    this.table('flat_rate_servicos', (table) => {
      // alter table
      table.integer('artigo_id').unsigned().nullable().references('id').inTable('produtos').onUpdate('CASCADE').after('servico_id')
    })
  }

  down () {
    this.table('flate_rate_servicos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FlatRateServicoSchema
