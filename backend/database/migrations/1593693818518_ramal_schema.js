'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RamalSchema extends Schema {
  up () {
    this.create('ramals', (table) => {
      table.increments()
      table.string('Material')
      table.string('Diametro_nominal')
      table.string('Comprimento')
      table.string('Profundidade')
      table.integer('tipo_ramal_id').unsigned().references('id').inTable('tipo_ramals')
      table.timestamps()
    })
  }

  down () {
    this.drop('ramals')
  }
}

module.exports = RamalSchema
