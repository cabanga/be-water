'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArmazenSchema extends Schema {
  up () {
    this.create('armazens', (table) => {
      table.increments();
	  table.string('nome', 70).nullable();
    table.string('descricao', 150).nullable();
    table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index')
      table.timestamps();
    })
  }

  down () {
    this.drop('armazens')
  }
}

module.exports = ArmazenSchema
