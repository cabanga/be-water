'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidosSchema extends Schema {
  up () {
    this.table('pedidos', (table) => {
      // alter table
	table.integer('tarifario_id').unsigned().index('tarifario_id').references('id').inTable('tarifarios').after('tipoPedido')
	table.string('telefone',25).notNullable().after('tarifario_id')
	table.integer('user_id').unsigned().index('user_id').references('id').inTable('users').after('observacao')
	
    })
  }

  down () {
    this.table('pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PedidosSchema
