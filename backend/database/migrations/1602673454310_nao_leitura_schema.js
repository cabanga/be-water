'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NaoleiturasSchema extends Schema {
  up () {
    this.table('nao_leituras', (table) => {
      // alter table          

      table.string('latitude', 50).after('tipo_nao_leitura_id')
      table.string('longitude', 50).after('latitude')
    })
  }

  down () {
    this.table('nao_leituras', (table) => {
      // reverse alternations
    })
  }
}

module.exports = NaoleiturasSchema
