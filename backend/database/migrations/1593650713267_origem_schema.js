'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OrigemSchema extends Schema {
  up () {
    this.create('origems', (table) => {
      table.increments()

      table.string('nome')
      table.string('slug')

      table.timestamps()
    })
  }

  down () {
    this.drop('origems')
  }
}

module.exports = OrigemSchema
