'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up () {
    this.table('contratoes', (table) => {
      // alter table
      table.integer('conta_id').unsigned().references('id').inTable('contas').after('id')
      table.integer('tipo_contracto_id').unsigned().references('id').inTable('tipo_contratoes').after('conta_id')
      table.date('data_inicio').after('tipo_contracto_id')
      table.date('data_fim').after('data_inicio')
      table.integer('tarifario_id').unsigned().references('id').inTable('tarifarios').after('data_fim')
      table.integer('estado_id').after('tarifario_id')
    })
  }

  down () {
    this.table('contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContratoesSchema
