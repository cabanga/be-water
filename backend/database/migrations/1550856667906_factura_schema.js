'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaSchema extends Schema {
  up () {
      this.create('facturas', (table) => {
        table.increments();
        table.integer('numero').notNullable();
        table.string('factura_sigla', 50).nullable();
        table.string('hash', 200).nullable().unique();
        table.string('hash_control', 200).nullable(); 
        table.float('totalComImposto').notNullable();
        table.float('totalSemImposto').notNullable();
        table.float('total').notNullable();
        table.boolean('pago').notNullable();
        table.string('status',1).notNullable();
        table.datetime('status_date').notNullable();
        table.string('status_reason', 50).nullable();
        table.string('numero_origem_factura', 50).nullable();
        table.datetime('data_origem_factura').nullable();
        table.string("observacao", 255).nullable();
        table.integer('serie_id').unsigned().notNullable().references('id').inTable('series').onUpdate('CASCADE').index('serie_id_index');
        table.integer('cliente_id').unsigned().notNullable().references('id').inTable('clientes').onUpdate('CASCADE').index('cliente_id_index');
        table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index');
        table.timestamps();
    })
  }

  down () {
    this.drop('facturas')
  }
}

module.exports = FacturaSchema
