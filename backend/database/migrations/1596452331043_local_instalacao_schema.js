'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaoSchema extends Schema {
  up () {
    this.create('local_instalacaos', (table) => {
      table.increments()

      table.string('moradia_numero')
      table.boolean('is_predio').defaultTo(false)
      table.uuid('predio_id')
      table.string('predio_nome')
      table.boolean('is_active').defaultTo(false)
      table.integer('rua_id').unsigned().references('id').inTable('ruas')
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('local_instalacaos')
  }
}

module.exports = LocalInstalacaoSchema
