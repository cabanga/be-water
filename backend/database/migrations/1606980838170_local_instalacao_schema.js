




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaosSchema extends Schema {
  up() {
    this.table('local_instalacaos', (table) => {
      // alter table

      table.string('cil', 50).unique()

    })
  }

  down() {
    this.table('local_instalacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LocalInstalacaosSchema



