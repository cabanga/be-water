'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargeInterconexaoSchema extends Schema {
  up () {
    this.table('charge_interconexaos', (table) => {
      // alter table
      table.string('invoiceText',255).nullable().after("currency");
    })
  }

  down () {
    this.table('charge_interconexaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargeInterconexaoSchema
