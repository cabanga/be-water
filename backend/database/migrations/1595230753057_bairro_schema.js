'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BairrosSchema extends Schema {
  up () {
    this.table('bairros', (table) => {
      // alter table
      table.integer('distrito_id').unsigned().references('id').inTable('distritos').after('municipio_id')
    })
  }

  down () {
    this.table('bairros', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BairrosSchema
