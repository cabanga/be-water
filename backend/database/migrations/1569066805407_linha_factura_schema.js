'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturaSchema extends Schema {
  up () {
    this.table('linha_facturas', (table) => {
      // alter table
      table.double('valorProdutoKwanza', 25,2).nullable();
    })
  }

  down () {
    this.table('linha_facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaFacturaSchema
