'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TecnologiasSchema extends Schema {
  up () {
    this.table('tecnologias', (table) => {
      // alter table
	table.string('rota_insert_dispositivo',75).nullable().after('tipoFacturacao');
	table.string('rota_list_dispositivos',75).nullable().after('rota_insert_dispositivo');
    })
  }

  down () {
    this.table('tecnologias', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TecnologiasSchema
