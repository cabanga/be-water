'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaUtilitieSchema extends Schema {
  up () {
    this.create('factura_utilities', (table) => {
      table.increments();
        table.integer('numero').notNullable();
        table.string('factura_sigla', 50).nullable();
        table.string('hash', 200).nullable().unique();
        table.string('hash_control', 200).nullable(); 
        table.double('totalComImposto').notNullable();
        table.double("totalSemImposto").notNullable();
        table.double("total").notNullable();
        table.boolean('pago').notNullable();
        table.boolean("status").notNullable().defaultTo(true);
        table.datetime('status_date').notNullable();
        table.string('status_reason', 50).nullable();
        table.string('numero_origem_factura', 50).nullable();
        table.datetime('data_origem_factura').nullable();
        table.string("observacao", 255).nullable();
        table.date("data_venciemento").nullable();

        table.double("valor_cambio").notNullable();
        table.string("moeda_iso", 255).nullable();

        table.integer('cliente_id').unsigned().notNullable().references('id').inTable('clientes').onUpdate('CASCADE').index();
        table.integer('conta_id').unsigned().notNullable().references('id').inTable('contas').onUpdate('CASCADE').index();
        table.integer('moeda_id').unsigned().notNullable().references('id').inTable('moedas').onUpdate('CASCADE').index();
        table.integer('cambio_id').unsigned().notNullable().references('id').inTable('cambios').onUpdate('CASCADE').index();
        
        table.integer('serie_id').unsigned().notNullable().references('id').inTable('series').onUpdate('CASCADE').index();
        
        table.integer('pagamento_id').unsigned().nullable().references('id').inTable('pagamentos').onUpdate('CASCADE').index();
        table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();
        
        table.integer('loja_id').unsigned().nullable().references('id').inTable('lojas').onUpdate('CASCADE').index();
        table.integer('caixa_id').unsigned().nullable().references('id').inTable('caixas').onUpdate('CASCADE').index();
        table.timestamps();
    })
  }

  down () {
    this.drop('factura_utilities')
  }
}

module.exports = FacturaUtilitieSchema
