'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StkMovimentoSchema extends Schema {
  up () {
    this.create('stk_movimentos', (table) => {
      table.increments()
	  table.integer('produto_id').unsigned().references('id').inTable('stk_produtos')
	  table.integer('armazem_id').unsigned().references('id').inTable('armazens')
	  table.integer('tipo_movimento_id').unsigned().references('id').inTable('stk_tipo_movimentos')
	  table.integer('user_id').unsigned().references('id').inTable('users')
	  table.integer('quantidade')
      table.timestamps()
    })
  }

  down () {
    this.drop('stk_movimentos')
  }
}

module.exports = StkMovimentoSchema
