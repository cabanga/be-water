'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoRotasSchema extends Schema {
  up () {
    this.table('estado_rotas', (table) => {
      // alter table

      table.string('slug', 45)

    })
  }

  down () {
    this.table('estado_rotas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EstadoRotasSchema
