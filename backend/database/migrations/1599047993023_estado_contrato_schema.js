'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoContratoSchema extends Schema {
  up () {
    this.create('estado_contratoes', (table) => {
      table.increments()
      
      table.string('nome', 45)
      table.string('slug', 45)
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('estado_contratoes')
  }
}

module.exports = EstadoContratoSchema
