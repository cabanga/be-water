'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalConsumoSchema extends Schema {
  up () {
    this.create('local_consumos', (table) => {
	  table.increments()
	 table.integer('conta_id').unsigned().references('id').inTable('contas')
	 table.integer('local_instalacao').unsigned().references('id').inTable('residencias')
	 table.integer('contador_id').unsigned().references('id').inTable('contadores')
	 table.integer('user_id').unsigned().references('id').inTable('users')
	  table.timestamps()
    })
  }

  down () {
    this.drop('local_consumos')
  }
}

module.exports = LocalConsumoSchema
