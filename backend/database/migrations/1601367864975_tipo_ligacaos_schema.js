'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoLigacaosSchema extends Schema {
  up () {
    this.create('tipo_ligacaos', (table) => {
      table.increments()
      table.string('descricao')
      table.string('slug')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_ligacaos')
  }
}

module.exports = TipoLigacaosSchema
