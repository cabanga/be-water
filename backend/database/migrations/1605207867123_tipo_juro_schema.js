'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoJuroSchema extends Schema {
  up () {
    this.create('tipo_juros', (table) => {
      table.increments()

      table.string('dias', 50).notNullable()
      table.string('percentagem', 50).notNullable()
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_juros')
  }
}

module.exports = TipoJuroSchema
