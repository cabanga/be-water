'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DocumentoSchema extends Schema {
  up () {
    this.create('documentos', (table) => {
      table.increments()
      table.string('nome', 20).notNullable().unique();
      table.string('sigla', 10).notNullable().unique();
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index');
      table.timestamps()
    })
  }

  down () {
    this.drop('documentos')
  }
}

module.exports = DocumentoSchema
