'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ParAdslSchema extends Schema {
  up () {
    this.create('par_adsls', (table) => {
      table.increments()
	  table.string('descricao', 150).notNullable()
	  table.integer('central_id').unsigned().references('id').inTable('central_recurso_redes')
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('par_adsls')
  }
}

module.exports = ParAdslSchema
