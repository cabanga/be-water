'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoOcorrenciaSchema extends Schema {
  up () {
    this.create('tipo_ocorrencias', (table) => {
      table.increments()

      table.string('nome')
      table.boolean('is_active').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_ocorrencias')
  }
}

module.exports = TipoOcorrenciaSchema
