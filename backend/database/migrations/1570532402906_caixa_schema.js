'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CaixaSchema extends Schema {
  up () {
    this.table('caixas', (table) => {
      // alter table
       
      table.integer('deposito_id').unsigned().nullable().references('id').inTable('deposito_caixas').onUpdate('CASCADE').index();
    })
  }

  down () {
    this.table('caixas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CaixaSchema
