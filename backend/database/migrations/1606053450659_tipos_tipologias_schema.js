'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TiposTipologiasSchema extends Schema {
  up () {
    this.table('tipos_tipologias', (table) => {
      // alter table
      table.integer('parent_id', 15).after('slug')
    })
  }

  down () {
    this.table('tipos_tipologias', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TiposTipologiasSchema
