'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresSchema extends Schema {
  up () {
    this.table('contadores', (table) => {
      // alter table
      table.string('tipo_fabricacao', 200).after('calibre')
      table.string('centro_distribuicao', 200).after('tipo_fabricacao')
      table.string('ultima_leitura', 200).after('centro_distribuicao')
    })
  }

  down () {
    this.table('contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContadoresSchema
