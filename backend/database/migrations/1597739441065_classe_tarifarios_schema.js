'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClasseTarifariosSchema extends Schema {
  up () {
    this.table('classe_tarifarios', (table) => {
      // alter table
	table.double('valor',10,5).after('tarifario_id')
    })
  }

  down () {
    this.table('classe_tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClasseTarifariosSchema
