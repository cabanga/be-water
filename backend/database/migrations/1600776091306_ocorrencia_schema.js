'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OcorrenciasSchema extends Schema {
  up () {
    this.table('ocorrencias', (table) => {
      // alter table          

      table.string('latitude', 50).after('tipo_ocorrencia_id')
      table.string('longitude', 50).after('latitude')
    })
  }

  down () {
    this.table('ocorrencias', (table) => {
      // reverse alternations
    })
  }
}

module.exports = OcorrenciasSchema
