'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClienteTipoDocumentoSchema extends Schema {
  up () {
    this.table('clientes', (table) => {
      table.integer('clienteID').nullable();
      table.integer('tipo_identidade_id').nullable().unsigned().index();
      table.foreign('tipo_identidade_id').references('id').on('tipo_identidades').onDelete('cascade');

      table.integer('tipo_cliente_id').nullable().unsigned().index();
      table.foreign('tipo_cliente_id').references('id').on('tipo_clientes').onDelete('cascade');
    })
  }

  down () {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClienteTipoDocumentoSchema
