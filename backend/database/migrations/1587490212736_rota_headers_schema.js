'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RotaHeadersSchema extends Schema {
  up () {
    this.table('rota_headers', (table) => {
      // alter table
	 table.datetime('data').after('data_created')
	 table.boolean('estado').defaultTo(false).after('id_estado_rota')
    })
  }

  down () {
    this.table('rota_headers', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RotaHeadersSchema
