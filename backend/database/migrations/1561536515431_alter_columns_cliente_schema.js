'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterColumnsClienteSchema extends Schema {
  up () {
    this.table('clientes', (table) => {
      // alter table
      table.text('observacao').nullable().after('email');
    })
  }

  down () {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterColumnsClienteSchema
