'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeiturasSchema extends Schema {
  up () {
    this.table('leituras', (table) => {
      table.boolean("foi_facturado").notNullable().defaultTo(false)
      table.date("data_ultima_leitura")
    })
  }

  down () {
    this.table('leituras', (table) => {
      table.dropColumn('foi_facturado')
      table.dropColumn('data_ultima_leitura')
    })
  }
}

module.exports = LeiturasSchema
