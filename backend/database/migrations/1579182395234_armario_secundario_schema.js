'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArmarioSecundarioSchema extends Schema {
  up () {
    this.create('armario_secundarios', (table) => {
      table.increments()
	  table.string('descricao', 150).notNullable()
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('armario_secundarios')
  }
}

module.exports = ArmarioSecundarioSchema
