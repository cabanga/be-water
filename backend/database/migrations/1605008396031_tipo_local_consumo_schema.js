'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoLocalConsumoSchema extends Schema {
  up() {
    this.create('tipo_local_consumos', (table) => {
      table.increments()

      table.string('nome', 80).notNullable().unique()
      table.string('slug', 50).notNullable().unique()
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down() {
    this.drop('tipo_local_consumos')
  }
}

module.exports = TipoLocalConsumoSchema
