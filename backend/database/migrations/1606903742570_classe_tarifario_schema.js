




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClasseTarifariosSchema extends Schema {
  up() {
    this.table('classe_tarifarios', (table) => {
      // alter table

      table.double('tarifa_variavel', 25, 2).after('valor');
      table.double('tarifa_fixa_mensal', 25, 2).after('tarifa_variavel');
      table.double('tarifa_intervalo', 25, 2).after('tarifa_fixa_mensal');

    })
  }

  down() {
    this.table('classe_tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClasseTarifariosSchema



