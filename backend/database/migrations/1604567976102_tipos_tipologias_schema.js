'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TiposTipologiasSchema extends Schema {
  up () {
    this.table('tipos_tipologias', (table) => {
      // alter table
      table.string('juro_mora').after('slug')
      table.boolean('sujeito_corte').defaultTo(false).after('juro_mora')
      table.integer('nivel_sensibilidade_id').unsigned().references('id').inTable('nivel_sensibilidades').after('sujeito_corte')
      table.integer('caucao').after('nivel_sensibilidade_id')
    })
  }

  down () {
    this.table('tipos_tipologias', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TiposTipologiasSchema
