'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CdmaFabricanteSchema extends Schema {
  up () {
    this.create('cdma_fabricantes', (table) => {
      table.increments()
	  table.integer('IDFabricante', 11).notNullable()
	  table.string('FabricanteDesc', 50).nullable()
	  table.boolean('Activo').notNullable().defaultTo('false')
      table.timestamps()
    })
  }

  down () {
    this.drop('cdma_fabricantes')
  }
}

module.exports = CdmaFabricanteSchema
