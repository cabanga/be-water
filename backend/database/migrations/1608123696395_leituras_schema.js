'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeiturasSchema extends Schema {
  up () {
    this.table('leituras', (table) => {
      table.boolean("nao_leitura").notNullable().defaultTo(false)
      table.string("motivo")
    })
  }

  down () {
    this.table('leituras', (table) => {
      table.dropColumn('nao_leitura')
      table.dropColumn('motivo')
    })
  }
}

module.exports = LeiturasSchema
