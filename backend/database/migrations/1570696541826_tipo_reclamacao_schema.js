'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoReclamacaoSchema extends Schema {
  up () {
    this.create('tipo_reclamacaos', (table) => {
      table.increments()
	  table.string('designacao',50).notNullable()
	  table.integer('user_id').unsigned().index('user_id').references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_reclamacaos')
  }
}

module.exports = TipoReclamacaoSchema
