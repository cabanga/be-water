'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojaBancosSchema extends Schema {
  up () {
    this.table('loja_bancos', (table) => {
      // alter table
      table.integer('user_id').unsigned().references('id').inTable('users').after('banco_id')
    })
  }

  down () {
    this.table('loja_bancos', (table) => {
      // reverse alternations
      table.dropColumn('user_id')
    })
  }
}

module.exports = LojaBancosSchema
