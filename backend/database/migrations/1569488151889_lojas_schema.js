'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojasSchema extends Schema {
  up () {
    this.table('lojas', (table) => {
      // alter table
      table.integer('serie_id').nullable().unsigned().index();
      table.foreign('serie_id').references('id').on('series').onDelete('cascade')
    })
  }

  down () {
    this.table('lojas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LojasSchema
