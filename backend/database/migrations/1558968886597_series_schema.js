'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SeriesSchema extends Schema {
  up () {
    this.table('series', (table) => {
      // alter table
	  table.integer('armazen_id').unsigned().notNullable().references('id').inTable('armazens').onUpdate('CASCADE').index('armazen_id_index').after('user_id');
    })
  }

  down () {
    this.table('series', (table) => {
      // reverse alternations
    })
  }
}

module.exports = SeriesSchema
