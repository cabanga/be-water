'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RotaRunsSchema extends Schema {
  up () {
    this.table('rota_runs', (table) => {
      table.date("data_realizar")
      table.date("proxima_data_realizar")
    })
  }

  down () {
    this.table('rota_runs', (table) => {
      table.dropColumn('data_realizar')
      table.dropColumn('proxima_data_realizar')
    })
  }
}

module.exports = RotaRunsSchema
