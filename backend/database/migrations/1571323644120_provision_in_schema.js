'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProvisionInSchema extends Schema {
  up () {
    this.create('provision_ins', (table) => {
      table.increments()
	  table.text('desc').nullable()
	  table.integer('status').nullable()
	  table.integer('credmensalindefault').nullable()
	  table.integer('profile_HuaweiIN').nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('provision_ins')
  }
}

module.exports = ProvisionInSchema
