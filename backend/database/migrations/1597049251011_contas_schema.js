'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContasSchema extends Schema {
  up () {
    this.table('contas', (table) => {
      // alter table
      table.string('gestorConta',200).alter()
    })
  }

  down () {
    this.table('contas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContasSchema
