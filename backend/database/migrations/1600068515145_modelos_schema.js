'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ModelosSchema extends Schema {
  up () {
    this.table('modelos', (table) => {
      // alter table
      table.integer('id_marca').unsigned().references('id').inTable('marcas').after('id')
    })
  }

  down () {
    this.table('modelos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ModelosSchema
