'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ObjectoLigacaoRamalSchema extends Schema {
  up() {
    this.create('objecto_ligacao_ramals', (table) => {
      table.increments()
      table.string('descricao', 150)
      table.string('latitude', 70)
      table.string('longitude', 70)
      table.integer('tipo_objecto_id').unsigned().references('id').inTable('tipo_objectos')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down() {
    this.drop('objecto_ligacao_ramals')
  }
}

module.exports = ObjectoLigacaoRamalSchema
