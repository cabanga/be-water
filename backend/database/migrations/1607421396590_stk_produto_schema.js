'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StkProdutoSchema extends Schema {
  up () {
    this.create('stk_produtos', (table) => {
      table.increments()
      table.string('descricao', 125)
	  table.integer('categoria_id').unsigned().references('id').inTable('stk_categoria_produtos')
	  table.integer('user_id').unsigned().references('id').inTable('users')
	  table.boolean('status').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('stk_produtos')
  }
}

module.exports = StkProdutoSchema
