'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoClasseTarifariosSchema extends Schema {
  up () {
    this.table('produto_classe_tarifarios', (table) => {
      // alter table
      table.integer('valor_fixo').after('classe_tarifario_id')
    })
  }

  down () {
    this.table('produto_classe_tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProdutoClasseTarifariosSchema
