'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoSchema extends Schema {
  up () {
    this.create('produtos', (table) => {
      table.increments()
      table.string('nome', 100).notNullable().unique();
      table.string('barcode', 200).nullable().unique();
      table.float('valor').notNullable();
      table.integer('quantidade').nullable();
      table.string('tipo', 7).notNullable();
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index');
      table.integer('imposto_id').unsigned().notNullable().references('id').inTable('impostos').onUpdate('CASCADE').index('imposto_id_index');
      table.timestamps()
    })
  }

  down () {
    this.drop('produtos')
  }
}

module.exports = ProdutoSchema
