'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaoSchema extends Schema {
  up () {
    this.table('local_instalacaos', (table) => {
      // alter table 
      table.dropColumn('instalacao_sanitaria_qtde')
      table.integer('instalacao_sanitaria_qtd').after('saneamento_flag')
    })
  }
  
  down () {
    this.table('local_instalacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LocalInstalacaoSchema
