'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContaBancariasSchema extends Schema {
  up () {
    this.table('conta_bancarias', (table) => {
      // alter table
      table.dropColumn('nif')
    })
  }

  down () {
    this.table('conta_bancarias', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContaBancariasSchema
