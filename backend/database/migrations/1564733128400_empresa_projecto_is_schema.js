'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresaProjectoIsSchema extends Schema {
  up () {
    this.table('empresas', (table) => {
      // alter table
      table.boolean('projecto_isActive').nullable().after('active_tfa').defaultTo(false)
    })
  }

  down () {
    this.table('empresas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EmpresaProjectoIsSchema
