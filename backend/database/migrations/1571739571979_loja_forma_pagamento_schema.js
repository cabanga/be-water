'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojaFormaPagamentoSchema extends Schema {
  up () {
    this.create('loja_forma_pagamentos', (table) => {
      table.increments()
      table.integer('forma_pagamento_id').unsigned().nullable().references('id').inTable('forma_pagamentos').onUpdate('CASCADE').index();
      table.integer('loja_id').unsigned().nullable().references('id').inTable('lojas').onUpdate('CASCADE').index();
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();
      table.timestamps()
    })
  }

  down () {
    this.drop('loja_forma_pagamentos')
  }
}

module.exports = LojaFormaPagamentoSchema
