'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalConsumosSchema extends Schema {
  up() {
    this.table('local_consumos', (table) => {
      // alter table

      table.integer('tipo_local_consumo_id').unsigned().references('id').inTable('tipo_local_consumos').after('contador_id')

    })
  }

  down() {
    this.table('local_consumos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LocalConsumosSchema
