'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RamalSchema extends Schema {
  up () {
    this.table('ramals', (table) => {
      // alter table
      table.string('Bairro', 80).after('Profundidade')
      table.string('Rua', 80).after('Bairro')
    })
  }

  down () {
    this.table('ramals', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RamalSchema
