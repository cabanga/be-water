




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up() {
    this.table('contratoes', (table) => {
      // alter table


      table.dropForeign('ciclo_facturacao_id', "contratoes_ciclo_facturacao_id_foreign")
      table.dropForeign('tipo_juro_id', "contratoes_tipo_juro_id_foreign")
      table.dropForeign('media_consumo_id', "contratoes_media_consumo_id_foreign")
            
      table.integer('tipo_mensagem_id').unsigned().references('id').inTable('tipo_mensagems').after('motivo_recisao_id')
      table.string('mensagem').after('tipo_mensagem_id')

    })
  }

  down() {
    this.table('contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContratoesSchema



