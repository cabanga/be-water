'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PlanoPrecoSchema extends Schema {
  up () {
    this.create('plano_precos', (table) => {
      table.increments()
      table.string("precoDescricao", 255).notNullable();
      table.integer("precoItemCod").nullable();
      table.string("rateTableCod", 255).nullable();
      table.integer("imposto_id");
      table.date("dataEstado").nullable();
      table.boolean('estado').nullable().defaultTo(true); 
      table.timestamps()
    })
  }

  down () {
    this.drop('plano_precos')
  }
}

module.exports = PlanoPrecoSchema
