'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class configuracaosSchema extends Schema {
  up() {
    this.table('configuracaos', (table) => {
      // alter table
      table.integer('contexto_configuracao_id').unsigned().references('id').inTable('contexto_configuracaos').after('valor')
    })
  }

  down() {
    this.table('configuracaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = configuracaosSchema
