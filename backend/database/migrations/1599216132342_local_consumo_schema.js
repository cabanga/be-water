'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalConsumosSchema extends Schema {
  up () {
    this.table('local_consumos', (table) => {
      // alter table

      table.boolean('is_active').defaultTo(false).after('contador_id')

    })
  }

  down () {
    this.table('local_consumos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LocalConsumosSchema
