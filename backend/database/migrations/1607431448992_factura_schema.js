'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table 
      table.integer('anulado_user_id').nullable().unsigned().references('id').inTable('users').after('user_id')
    })
  }
  
  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturaSchema
