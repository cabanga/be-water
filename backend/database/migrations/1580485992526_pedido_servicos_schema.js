'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidoServicosSchema extends Schema {
  up () {
    this.create('pedido_servicos', (table) => {
      table.increments()
	  table.integer('pedido_id').unsigned().references('id').inTable('pedidos')
	  table.integer('servico_id').unsigned().references('id').inTable('servicos')
	  table.string('servico', 200).nullable()
	  table.string('servico_class', 200).nullable()
	  table.string('servico_omg', 200).nullable()
	  table.string('nivel_servico', 200).nullable()
	  table.string('descricao_servico', 200).nullable()
	  table.string('debito_binario', 200).nullable()
	  table.string('interface_dte', 200).nullable()
	  table.string('redundancia', 200).nullable()
	  table.string('cpe_sede_a', 200).nullable()
	  table.string('cpe_sede_b', 200).nullable()
	  table.string('endereco_ponto_a', 200).nullable()
	  table.string('endereco_ponto_b', 200).nullable()
	  table.string('latitude_ponto_a', 200).nullable()
	  table.string('longitude_ponto_a', 200).nullable()
	  table.string('latitude_ponto_b', 200).nullable()
	  table.string('longitude_ponto_b', 200).nullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('pedido_servicos')
  }
}

module.exports = PedidoServicosSchema
