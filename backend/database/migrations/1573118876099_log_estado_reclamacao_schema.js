'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogEstadoReclamacaoSchema extends Schema {
  up () {
    this.create('log_estado_reclamacaos', (table) => {
      table.increments()
	  table.integer('reclamacao_id').unsigned().references('id').inTable('reclamacaos')
	  table.integer('id_estado_anterior').unsigned().references('id').inTable('estado_reclamacaos')
	  table.integer('id_estado_novo').unsigned().references('id').inTable('estado_reclamacaos')
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('log_estado_reclamacaos')
  }
}

module.exports = LogEstadoReclamacaoSchema
