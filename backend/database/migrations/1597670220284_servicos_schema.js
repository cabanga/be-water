'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServicosSchema extends Schema {
  up () {
    this.table('servicos', (table) => {
      // alter table
	table.integer('produto_id').unsigned().references('id').inTable('produtos').after('servico_id')
    })
  }

  down () {
    this.table('servicos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ServicosSchema
