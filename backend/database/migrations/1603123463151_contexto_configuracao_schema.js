'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContextoConfiguracaoSchema extends Schema {
  up() {
    this.table('contexto_configuracaos', (table) => {
      // alter table

      table.boolean('is_delected').defaultTo(false).after('campo')
    })
  }

  down() {
    this.table('contexto_configuracaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContextoConfiguracaoSchema
