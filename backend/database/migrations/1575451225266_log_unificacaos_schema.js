'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogUnificacaosSchema extends Schema {
  up () {
    this.create('log_unificacaos', (table) => {
      table.increments()
      table.timestamps()

      table.integer('cliente_manter_id').notNullable();
      table.integer('cliente_unificar_id').nullable()
      table.string("registro", 254).notNullable();
      table.string('tabela', 20).notNullable()
      table.string('descricao', 254).nullable() 
    })
  }

  down () {
    this.drop('log_unificacaos')
  }
}

module.exports = LogUnificacaosSchema
