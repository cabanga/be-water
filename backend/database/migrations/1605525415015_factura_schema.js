'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaSchema extends Schema {
  up() {
    this.create('facturas', (table) => {
      table.increments()
      table.integer('numero').notNullable();
      table.string('factura_sigla', 50).nullable();
      table.string('hash', 200).nullable().unique();
      table.string('hash_control', 200).nullable();
      table.float('totalComImposto').notNullable();
      table.float('totalSemImposto').notNullable();
      table.float('total').notNullable();
      table.boolean('pago').notNullable();
      table.string('status', 1).notNullable();
      table.datetime('status_date').notNullable();
      table.string('status_reason', 50).nullable();
      table.string('numero_origem_factura', 50).nullable();
      table.datetime('data_origem_factura').nullable();
      table.string("observacao", 255).nullable();
      table.integer('serie_id').unsigned().notNullable().references('id').inTable('series').onUpdate('CASCADE').index('serie_id_index');
      table.integer('cliente_id').unsigned().notNullable().references('id').inTable('clientes').onUpdate('CASCADE').index('cliente_id_index');
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index');


      table.timestamps()

      table.datetime('data_venciemento').notNullable();
      table.integer('moeda_id').unsigned().notNullable().references('id').inTable('moedas');
      table.double("total_contra_valor", 25, 2).nullable();
      table.double("valor_cambio", 25, 2).nullable();
      table.integer('cambio_id').nullable();
      table.boolean('is_nota_credito').nullable().defaultTo(false)
      table.string("moeda_iso", 25).nullable();
      table.double('totalKwanza', 25, 2).nullable();
      table.integer('caixa_id').nullable().unsigned().index();
      table.foreign('caixa_id').references('id').on('caixas').onDelete('cascade')
      table.double("valor_aberto", 35, 2).nullable();
      table.integer('servico_id').nullable().unsigned().index();
      table.foreign('servico_id').references('id').on('servicos').onDelete('cascade')
      table.boolean('is_iplc').nullable().defaultTo(false);
      table.string('facturacao', 15)
      table.double("minutos_interconexao", 255, 2).nullable();
    })
  }

  down() {
    this.drop('facturas')
  }
}

module.exports = FacturaSchema
