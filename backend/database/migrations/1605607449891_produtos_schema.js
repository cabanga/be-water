'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutosSchema extends Schema {
  up () {
    this.table('produtos', (table) => {
      // alter table
      table.integer('classificacao_produto_id').unsigned().references('id').inTable('classificacao_produtos')

    })
  }

  down () {
    this.table('produtos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProdutosSchema
