'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaosSchema extends Schema {
  up() {
    this.table('local_instalacaos', (table) => {
      // alter table

      table.integer('numero_cliente').after('moradia_numero')

    })
  }

  down() {
    this.table('local_instalacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LocalInstalacaosSchema
