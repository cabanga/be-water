




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up() {
    this.table('contratoes', (table) => {
      // alter table

      table.integer('numero_habitantes').after('morada_correspondencia_flag')
      table.integer('ciclo_facturacao_id').unsigned().references('id').inTable('ciclo_facturacaos').after('tipologia_cliente_id')
      
    })
  }

  down() {
    this.table('contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContratoesSchema



