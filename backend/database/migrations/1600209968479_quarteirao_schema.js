'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuarteiraoSchema extends Schema {
  up () {
    this.create('quarteiraos', (table) => {
      table.increments()

      table.string('zona', 80)
      table.uuid('zona_id')
      table.integer('rua_id').unsigned().references('id').inTable('ruas')
      table.boolean('is_active').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users')
            
      table.timestamps()
    })
  }

  down () {
    this.drop('quarteiraos')
  }
}

module.exports = QuarteiraoSchema
