'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StkProdutosSchema extends Schema {
  up () {
    this.table('stk_produtos', (table) => {
      // alter table
	table.integer('valor').after('descricao')
    })
  }

  down () {
    this.table('stk_produtos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = StkProdutosSchema
