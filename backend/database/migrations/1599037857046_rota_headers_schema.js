'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RotaHeadersSchema extends Schema {
  up () {
    this.table('rota_headers', (table) => {
      // alter table
      table.string("descricao", 50).after("id");
      table.date("data_inicio").after("descricao");
      table.date("data_fim").after("data_inicio");
      table.integer('leitor_id').unsigned().references('id').inTable('users').after("estado");
    })
  }

  down () {
    this.table('rota_headers', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RotaHeadersSchema
