'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargesSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      // alter table
	table.integer('servico_id').unsigned().references('id').inTable('servicos').after('id')
	table.integer('produto_id').unsigned().references('id').inTable('produtos').after('servico_id')
	table.integer('local_consumo_id').unsigned().references('id').inTable('local_consumos').after('produto_id') 
    })
  }

  down () {
    this.table('charges', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargesSchema
