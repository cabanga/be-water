'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaoHistoricoSchema extends Schema {
  up () {
    this.table('local_instalacao_historicos', (table) => {
      // alter table 

      //table.dropColumn('registo_anterior')
      //table.dropColumn('registo_actual')

      table.string('historico', 800).after('local_instalacao_id')
      table.string('actualizacao', 800).after('historico')
    })
  }
  
  down () {
    this.table('local_instalacao_historicos', (table) => {

      // reverse alternations
    })
  }
}

module.exports = LocalInstalacaoHistoricoSchema
