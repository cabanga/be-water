'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoSchema extends Schema {
  up () {
    this.table('produtos', (table) => {
      // alter table
       table.boolean("is_trigger").nullable();
      
    })
  }

  down () {
    this.table('produtos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProdutoSchema
