'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClienteIdentidadeSchema extends Schema {
  up () {
    this.create('cliente_identidades', (table) => {
      table.increments()

      table.integer('cliente_id').nullable().unsigned().index()
      table.foreign('cliente_id').references('id').on('clientes').onDelete('cascade')

      table.integer('tipo_identidade_id').nullable().unsigned().index()
      table.foreign('tipo_identidade_id').references('id').on('tipo_identidades').onDelete('cascade')

      table.string('numero_identidade')

      table.timestamps()
    })
  }

  down () {
    this.drop('cliente_identidades')
  }
}

module.exports = ClienteIdentidadeSchema
