'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresSchema extends Schema {
  up () {
    this.table('contadores', (table) => {
      // alter table
      table.integer('estado_contador_id').unsigned().references('id').inTable('estado_contadores').after('servico_id')
    })
  }

  down () {
    this.table('contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContadoresSchema
