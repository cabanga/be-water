




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClasseTarifariosSchema extends Schema {
  up() {
    this.table('classe_tarifarios', (table) => {
      // alter table

      table.integer('ordem').after('valor');

    })
  }

  down() {
    this.table('classe_tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClasseTarifariosSchema



