'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoClasseTarifarioSchema extends Schema {
  up () {
    this.create('produto_classe_tarifarios', (table) => {
      table.increments()
	  table.integer('produto_id').unsigned().references('id').inTable('produtos')
	  table.integer('classe_tarifario_id').unsigned().references('id').inTable('classe_tarifarios')
      table.timestamps()
    })
  }

  down () {
    this.drop('produto_classe_tarifarios')
  }
}

module.exports = ProdutoClasseTarifarioSchema
