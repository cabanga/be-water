'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContaBancariaSchema extends Schema {
  up() {
    this.create('conta_bancarias', (table) => {
      table.increments()
      
      table.string("nif").notNullable().unique();
      table.string("iban").notNullable().unique();
      table.integer('municipio_id').unsigned().references('id').inTable('municipios')
      table.integer('banco_id').unsigned().references('id').inTable('bancos')
      table.boolean("is_active").notNullable();

      table.timestamps()
    })
  }

  down() {
    this.drop('conta_bancarias')
  }
}

module.exports = ContaBancariaSchema
