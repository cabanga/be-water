'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContaCorrenteEstimativaSchema extends Schema {
  up () {
    this.create('conta_corrente_estimativas', (table) => {
      table.increments()
      table.double("valor").notNullable();
      table.string("periodo", 15).notNullable();
      table.string("periodo_usado", 15).nullable();
      table.integer("meses").unsigned().notNullable();
      table.boolean("estado").defaultTo(false);      
      table.integer("contador_id").unsigned().notNullable().references("id").inTable("contadores");
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('conta_corrente_estimativas')
  }
}

module.exports = ContaCorrenteEstimativaSchema
