'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TarifariosSchema extends Schema {
  up () {
    this.table('tarifarios', (table) => {
      // alter table
      table.string('categoria_tarifaria')
      table.string('tarifario_variavel')
      table.string('tarifa_fixa_mensal')
      table.string('regra_aplicacao')
      table.integer('user_id').unsigned().references('id').inTable('users')
    })
  }

  down () {
    this.table('tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TarifariosSchema
