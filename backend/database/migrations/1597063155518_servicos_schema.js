'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ServicosSchema extends Schema {
  up () {
    this.create('servicos', (table) => {
      table.increments()
      table.integer('contrato_id').unsigned().references('id').inTable('contratoes')
      table.integer('servico_id').unsigned().references('id').inTable('produtos')
      table.integer('valorBase')
      table.integer('imposto')
      table.integer('estado')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('servicos')
  }
}

module.exports = ServicosSchema
