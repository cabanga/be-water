'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AgendamentoRoteiroSchema extends Schema {
  up () {
    this.create('agendamento_roteiros', (table) => {
      table.increments()
      table.date("data_realizar")
      table.boolean('foi_realizada').nullable().defaultTo(false)
      table.integer('rota_run_id').nullable().unsigned().index()
      table.foreign('rota_run_id').references('id').on('rota_runs').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('agendamento_roteiros')
  }
}

module.exports = AgendamentoRoteiroSchema
