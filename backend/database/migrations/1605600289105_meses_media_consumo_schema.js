'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MesesMediaConsumoSchema extends Schema {
  up () {
    this.create('meses_media_consumos', (table) => {
      table.increments()
      table.string('descricao', 80)
      table.string('slug', 50)
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('meses_media_consumos')
  }
}

module.exports = MesesMediaConsumoSchema
