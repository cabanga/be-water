'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CdmaEquipamentosSchema extends Schema {
  up () {
    this.table('cdma_equipamentos', (table) => {
      // alter table
	table.integer('user_id').unsigned().references('id').inTable('users').after('A12')
    })
  }

  down () {
    this.table('cdma_equipamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CdmaEquipamentosSchema
