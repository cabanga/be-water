'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeiturasSchema extends Schema {
  up () {

    this.alter('leituras', (table) => {
      table.float('consumo').alter();
    })
  }

  down () {
    this.table('leituras', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LeiturasSchema
