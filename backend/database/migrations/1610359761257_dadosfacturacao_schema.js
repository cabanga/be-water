'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DadosfacturacaoSchema extends Schema {
  up () {
    this.table('dadosfacturacaos', (table) => {
      // alter table
      table.integer('user_id').unsigned().references('id').inTable('users').after('morada')
    })
  }

  down () {
    this.table('dadosfacturacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = DadosfacturacaoSchema
