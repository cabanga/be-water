'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OcorrenciaSchema extends Schema {
  up () {
    this.create('ocorrencias', (table) => {
      table.increments()
      
      table.string('motivo')
      table.integer('rota_run_id').unsigned().references('id').inTable('rota_runs')
      table.integer('tipo_ocorrencia_id').unsigned().references('id').inTable('tipo_ocorrencias')
      table.boolean('is_delected').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('ocorrencias')
  }
}

module.exports = OcorrenciaSchema
