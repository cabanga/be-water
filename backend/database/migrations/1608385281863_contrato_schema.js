
'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up() {
    this.table('contratoes', (table) => {
      // alter table

      table.integer('numero_utilizadores').after('numero_habitantes')
      table.integer('classe_tarifario_id').unsigned().references('id').inTable('classe_tarifarios')
      
    })
  }

  down() {
    this.table('contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContratoesSchema
