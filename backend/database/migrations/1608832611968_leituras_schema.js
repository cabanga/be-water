'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeiturasSchema extends Schema {
  up () {
    this.table('leituras', (table) => {
      table.integer("classe_tarifario_id").notNullable().defaultTo(0)
      table.string("tarifario_descricao")
    })
  }

  down () {
    this.table('leituras', (table) => {
      table.dropColumn('classe_tarifario_id')
      table.dropColumn('tarifario_descricao')

    })
  }
}

module.exports = LeiturasSchema
