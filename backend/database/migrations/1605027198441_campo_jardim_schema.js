'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CampoJardimSchema extends Schema {
  up () {
    this.create('campo_jardims', (table) => {
      table.increments()

      table.string('nome', 80).notNullable().unique()
      table.string('slug', 50).notNullable().unique()
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('campo_jardims')
  }
}

module.exports = CampoJardimSchema
