'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArmarioSecundariosSchema extends Schema {
  up () {
    this.table('armario_secundarios', (table) => {
      // alter table
    table.integer('central_id').unsigned().references('id').inTable('central_recurso_redes').after('descricao')
    })
  }

  down () {
    this.table('armario_secundarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ArmarioSecundariosSchema
