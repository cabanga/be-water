'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogsContadoresSchema extends Schema {
  up () {
    this.create('logs_contadores', (table) => {
      table.increments()
      table.integer('contador_id').unsigned().references('id').inTable('contadores')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.string('operacao', 15)
      table.string('tabela', 15)
      table.timestamps()
    })
  }

  down () {
    this.drop('logs_contadores')
  }
}

module.exports = LogsContadoresSchema
