'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipologiaClientesSchema extends Schema {
  up() {
    this.table('tipologia_clientes', (table) => {
      // alter table

      table.integer('caucao_id').unsigned().references('id').inTable('caucaos').after('nivel_sensibilidade_id')
      table.integer('parent_id').after('caucao')
    
    })
  }

  down() {
    this.table('tipologia_clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TipologiaClientesSchema
