'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadosContratosSchema extends Schema {
  up () {
    this.create('estados_contratos', (table) => {
      table.increments()
      table.string('nivel', 15)
      table.string('nome', 45)
      table.string('slug', 45)
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('estados_contratos')
  }
}

module.exports = EstadosContratosSchema
