'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MensagemSchema extends Schema {
  up () {
    this.create('mensagems', (table) => {
      table.increments()

      table.string('texto', 80).notNullable()
      table.integer('tipo_mensagem_id').unsigned().references('id').inTable('tipo_mensagems')
      table.integer('tipo_cliente_id').unsigned().references('id').inTable('tipo_clientes')
      table.integer('user_id').unsigned().references('id').inTable('users')


      table.timestamps()
    })
  }

  down () {
    this.drop('mensagems')
  }
}

module.exports = MensagemSchema
