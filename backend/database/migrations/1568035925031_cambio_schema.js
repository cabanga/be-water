'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CambioSchema extends Schema {
  up () {
    this.create('cambios', (table) => {
      table.increments();
      
      table.double("valor_cambio",20,2).notNullable();
      table.boolean("is_active").notNullable().defaultTo(true);
      table.date("data_cambio").nullable();

      table.integer('moeda_id').unsigned().index();
      table.foreign('moeda_id').references('id').on('moedas').onDelete('cascade');

      table.integer('user_id').unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade');
      table.timestamps();
    })
  }

  down () {
    this.drop('cambios')
  }
}

module.exports = CambioSchema
