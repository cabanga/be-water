'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReferenciaBancariaSchema extends Schema {
  up () {
    this.create('referencia_bancarias', (table) => {
      table.increments()

      table.text('referencia').nullable()
      table.date("data_pagamento").nullable();
      table.integer('banco_id').unsigned().nullable().references('id').inTable('bancos').onUpdate('CASCADE');
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE');
      
      table.timestamps()
    })
  }

  down () {
    this.drop('referencia_bancarias')
  }
}

module.exports = ReferenciaBancariaSchema
