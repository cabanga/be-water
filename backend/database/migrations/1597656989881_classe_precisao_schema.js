'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClassePrecisaoSchema extends Schema {
  up () {
    this.create('classe_precisaos', (table) => {
      table.increments()
      table.integer('tarifario_id').unsigned().references('id').inTable('tarifarios')
      table.string('valor')
      table.string('consumo_minimo')
      table.string('consumo_maximo')
      table.timestamps()
    })
  }

  down () {
    this.drop('classe_precisaos')
  }
}

module.exports = ClassePrecisaoSchema
