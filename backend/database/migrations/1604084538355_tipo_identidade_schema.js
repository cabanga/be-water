'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoIdentidadesSchema extends Schema {
  up() {
    this.table('tipo_identidades', (table) => {
      // alter table

      table.string('nome', 100).after('id')
      table.integer('numero_digitos').after('nome')

    })
  }

  down() {
    this.table('tipo_identidades', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TipoIdentidadesSchema
