'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogEstadoContratoSchema extends Schema {
  up() {
    this.create('log_estado_contratoes', (table) => {
      table.increments()

      table.integer('contrato_id').unsigned().references('id').inTable('contratoes')
      table.integer('id_estado_anterior').unsigned().references('id').inTable('estado_contratoes')
      table.integer('id_estado_actual').unsigned().references('id').inTable('estado_contratoes')
      table.date('data_operacao')
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down() {
    this.drop('log_estado_contratoes')
  }
}

module.exports = LogEstadoContratoSchema
