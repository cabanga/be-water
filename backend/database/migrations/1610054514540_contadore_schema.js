'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoreSchema extends Schema {
  up () {
    this.table('contadores', (table) => {
      // alter table
      table.date('data_leitura').nullable()
    })
  }

  down () {
    this.table('contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContadoreSchema
