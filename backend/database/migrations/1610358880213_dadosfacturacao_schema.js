'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DadosfacturacaoSchema extends Schema {
  up () {
    this.create('dadosfacturacaos', (table) => {
      table.increments()
      table.integer('municipo_id').unsigned().references('id').inTable('municipios')
      table.string('nif')
      table.string('morada')
      table.timestamps()
    })
  }

  down () {
    this.drop('dadosfacturacaos')
  }
}

module.exports = DadosfacturacaoSchema
