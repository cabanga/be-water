'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoTarifarioSchema extends Schema {
  up () {
    this.create('estado_tarifarios', (table) => {
      table.increments()
      table.string('descricao')
      table.string('slug')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('estado_tarifarios')
  }
}

module.exports = EstadoTarifarioSchema
