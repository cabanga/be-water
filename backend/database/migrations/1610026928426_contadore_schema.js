'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresSchema extends Schema {
  up() {
    this.table('contadores', (table) => {
      // alter table
      table.date('data_ultima_leitura').after('ultima_verificacao')
    })
  }

  down() {
    this.table('contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContadoresSchema
