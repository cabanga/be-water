'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LteCpesSchema extends Schema {
  up () {
    this.table('lte_cpes', (table) => {
      // alter table
	table.integer('servico_id').unsigned().references('id').inTable('servicos').after('tipo')
    })
  }

  down () {
    this.table('lte_cpes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LteCpesSchema
