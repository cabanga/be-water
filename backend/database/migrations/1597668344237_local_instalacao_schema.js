'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaoSchema extends Schema {
  up () {
    this.table('local_instalacaos', (table) => {
      // alter table
          
      table.string('latitude', 50).after('predio_nome')
      table.string('longitude', 50).after('latitude')
   
    })
  }

  down () {
    this.table('local_instalacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LocalInstalacaoSchema
