'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NivelSensibilidadeSchema extends Schema {
  up () {
    this.create('nivel_sensibilidades', (table) => {
      table.increments()
      table.string('descricao')
      table.string('slug')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('nivel_sensibilidades')
  }
}

module.exports = NivelSensibilidadeSchema
