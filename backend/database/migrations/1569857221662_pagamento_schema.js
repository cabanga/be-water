'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentoSchema extends Schema {
  up () {
    this.table('pagamentos', (table) => {
      // alter table
      table.bigInteger("referencia").alter();       

      table.double("valor_recebido", 100, 2).nullable().alter();
      table.double("troco", 100, 2).nullable().alter();
      table.double("total_pago", 100, 2).nullable().alter();

    })
  }

  down () {
    this.table('pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PagamentoSchema
