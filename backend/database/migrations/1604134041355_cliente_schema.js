'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClientesSchema extends Schema {
  up() {
    this.table('clientes', (table) => {
      // alter table
      
      table.string('nome').after('id')
      table.string('numero_identificacao', 100).after('tipo_identidade_id')      
      table.integer('genero_id').unsigned().references('id').inTable('generos').after('telefone') 
      table.integer('municipio_id').unsigned().references('id').inTable('municipios').after('email') 
      table.string('morada').after('municipio_id')      
      table.integer('gestor_conta_id').unsigned().references('id').inTable('gestor_contas').after('direccao_id') 
      table.boolean('is_active').defaultTo(false).after('gestor_conta_id')

    })
  }

  down() {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClientesSchema
