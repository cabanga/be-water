'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MoedaSchema extends Schema {
  up () {
    this.create('moedas', (table) => {
      table.increments()
      table.string("nome", 50).notNullable();
      table.string("codigo_iso", 50).notNullable();
      table.string("descricao", 100).nullable();
      table.string("unidade_fracionaria", 100).nullable(); 
      table.boolean("activo").nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('moedas')
  }
}

module.exports = MoedaSchema
