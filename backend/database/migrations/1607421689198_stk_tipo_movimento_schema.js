'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StkTipoMovimentoSchema extends Schema {
  up () {
    this.create('stk_tipo_movimentos', (table) => {
      table.increments()
	  table.string('descricao', 75)
	  table.string('slug', 75)
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('stk_tipo_movimentos')
  }
}

module.exports = StkTipoMovimentoSchema
