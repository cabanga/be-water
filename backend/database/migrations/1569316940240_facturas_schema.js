'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturasSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
      table.integer('conta_id').nullable().unsigned().index();
      table.foreign('conta_id').references('id').on('contas').onDelete('cascade')
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
    
  }
}

module.exports = FacturasSchema
