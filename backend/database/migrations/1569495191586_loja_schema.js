'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojaSchema extends Schema {
  up () {
    this.table('lojas', (table) => {
      // alter table
      table.integer('filial_id').nullable().unsigned().index();
      table.foreign('filial_id').references('id').on('filials').onDelete('cascade')
    })
  }

  down () {
    this.table('lojas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LojaSchema
