'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CdmaModeloSchema extends Schema {
  up () {
    this.create('cdma_modelos', (table) => {
      table.increments()
	  table.integer('IDModelo', 11).notNullable()
	  table.integer('IDFabricante', 11).notNullable()
	  table.string('DescricaoModelo', 50).nullable()
	  table.boolean('Activo').notNullable().defaultTo('false')
      table.timestamps()
    })
  }

  down () {
    this.drop('cdma_modelos')
  }
}

module.exports = CdmaModeloSchema
