'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoOrdemServicoSchema extends Schema {
  up () {
    this.create('tipo_ordem_servicos', (table) => {
      table.increments()
      table.string('descricao')
      table.string('slug')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_ordem_servicos')
  }
}

module.exports = TipoOrdemServicoSchema
