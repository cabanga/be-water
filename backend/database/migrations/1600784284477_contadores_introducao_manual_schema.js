'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresIntroducaoManualSchema extends Schema {
  up () {
    this.create('contadores_introducao_manuals', (table) => {
      table.increments()
      table.integer('contador_id').unsigned().references('id').inTable('contadores')
      table.integer('tipo_registo_id').unsigned().references('id').inTable('tipo_registos')
      table.string('ultima_leitura')
      table.date('data')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('contadores_introducao_manuals')
  }
}

module.exports = ContadoresIntroducaoManualSchema
