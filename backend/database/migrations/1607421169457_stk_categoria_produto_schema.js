'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StkCategoriaProdutoSchema extends Schema {
  up () {
    this.create('stk_categoria_produtos', (table) => {
      table.increments()
	  table.string('descricao', 75)
	  table.integer('user_id').unsigned().references('id').inTable('users')
	  
      table.timestamps()
    })
  }

  down () {
    this.drop('stk_categoria_produtos')
  }
}

module.exports = StkCategoriaProdutoSchema
