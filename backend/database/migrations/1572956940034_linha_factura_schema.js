'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturaSchema extends Schema {
  up () {
    this.table('linha_facturas', (table) => {
      // alter table
      table.dropForeign('artigo_id', "linha_facturas_artigo_id_foreign")
    })
  }

  down () {
    this.table('linha_facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaFacturaSchema
