'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoIdentidadeSchema extends Schema {
  up () {
    this.create('tipo_identidades', (table) => {
      table.increments()
      table.string("tipoIdentificacao", 120).notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_identidades')
  }
}

module.exports = TipoIdentidadeSchema
