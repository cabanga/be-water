'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NaoVisitasSchema extends Schema {
  up () {
    this.table('nao_visitas', (table) => {
      // alter table
          
      table.string('latitude', 50).after('tipo_nao_visita_id')
      table.string('longitude', 50).after('latitude')
    })
  }

  down () {
    this.table('nao_visitas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = NaoVisitasSchema
