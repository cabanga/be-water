'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidosSchema extends Schema {
  up () {
    this.table('pedidos', (table) => {
      // alter table
	table.integer('cliente_id').unsigned().nullable().alter()
	table.string('tipoPedido').nullable().alter()
	table.string('telefone').nullable().alter()
	table.date('dataPedido').nullable().alter()
    })
  }

  down () {
    this.table('pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PedidosSchema
