
'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BancosSchema extends Schema {
  up() {
    this.table('bancos', (table) => {
      // alter table

      table.boolean("is_active").default(false).after('abreviatura');
      table.string('morada')

      table.dropColumn('numero_conta')
      table.dropColumn('iban')
      table.dropColumn('activo')
      
    })
  }

  down() {
    this.table('bancos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BancosSchema
