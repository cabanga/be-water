'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargesSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      table.integer('leitura_id').nullable().unsigned().index()
      table.foreign('leitura_id').references('id').on('leituras').onDelete('cascade')
    })
  }

  down () {
    this.table('charges', (table) => {
      table.dropColumn('leitura_id')
    })
  }
}

module.exports = ChargesSchema
