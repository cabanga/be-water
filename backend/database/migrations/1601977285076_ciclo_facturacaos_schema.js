'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CicloFacturacaosSchema extends Schema {
  up () {
    this.table('ciclo_facturacaos', (table) => {
      // alter table
	table.boolean('estado').defaultTo(false).after('estado_id')
    })
  }

  down () {
    this.table('ciclo_facturacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CicloFacturacaosSchema
