'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContasSchema extends Schema {
  up () {
    this.table('contas', (table) => {
      table.integer('contrato_id').nullable().unsigned().index()
      table.foreign('contrato_id').references('id').on('contratoes').onDelete('cascade')
    })
  }

  down () {
    this.table('contas', (table) => {
      table.dropColumn('contrato_id')
    })
  }
}

module.exports = ContasSchema
