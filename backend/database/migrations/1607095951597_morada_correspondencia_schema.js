'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MoradaCorrespondenciaSchema extends Schema {
  up () {
    this.create('morada_correspondencias', (table) => {
      table.increments()

      table.integer('rua_id').unsigned().references('id').inTable('ruas')
      table.integer('numero_moradia')
      table.boolean('is_predio').defaultTo(false)
      table.varchar('predio_id', 36)
      table.varchar('predio_nome', 50)
      table.integer('predio_andar')
      table.boolean('is_delected').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users')      

      table.timestamps()
    })
  }

  down () {
    this.drop('morada_correspondencias')
  }
}

module.exports = MoradaCorrespondenciaSchema
