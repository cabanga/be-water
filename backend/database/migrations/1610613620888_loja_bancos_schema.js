'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojaBancosSchema extends Schema {
  up () {
    this.create('loja_bancos', (table) => {
      table.increments()
      table.integer('loja_id').unsigned().references('id').inTable('lojas')
      table.integer('banco_id').unsigned().references('id').inTable('bancos')
      table.timestamps()
    })
  }

  down () {
    this.drop('loja_bancos')
  }
}

module.exports = LojaBancosSchema
