'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddClienteColumnNumberIdentificacaoComercialSchema extends Schema {
  up () {

    this.table('clientes', (table) => {
      // alter table
      table.string('contribuinte_comercial',40).nullable();  
    })
  }

  down () {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddClienteColumnNumberIdentificacaoComercialSchema
