'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturasSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      table.boolean("is_service").default(true)
    })
  }

  down () {
    this.table('facturas', (table) => {
      table.dropColumn('is_service')
    })
  }
}

module.exports = FacturasSchema
