




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TarifariosSchema extends Schema {
  up() {
    this.table('tarifarios', (table) => {
      // alter table

      table.integer('tipo_cliente_id').unsigned().references('id').inTable('tipo_clientes').after('descricao')
      table.integer('tipo_contrato_id').unsigned().references('id').inTable('tipo_contratoes').after('tipo_cliente_id')
    })
  }

  down() {
    this.table('tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TarifariosSchema



