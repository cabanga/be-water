'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContaSchema extends Schema {
  up () {
    this.table('contas', (table) => {
      // alter table
      table.string('numeroExterno',255).nullable()
    })
  }

  down () {
    this.table('contas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContaSchema
