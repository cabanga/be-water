'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PessoaContactoSchema extends Schema {
  up () {
    this.create('pessoa_contactos', (table) => {

      table.increments()

      table.string("nome", 255).nullable();
      table.string("tipo_contacto", 255).nullable();
      table.string("telefone", 25).nullable();
      table.string("email", 255).nullable(); 


      table.integer('cliente_id').nullable().unsigned().index();
      table.foreign('cliente_id').references('id').on('clientes').onDelete('cascade')

      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('pessoa_contactos')
  }
}

module.exports = PessoaContactoSchema
