'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DropColumnsArmazemSerieSchema extends Schema {
  up () {
    this.table('series', (table) => {
      // alter table
      table.dropForeign("armazen_id").dropColumn("armazen_id"); 
    })
  }

  down () {
    this.table('series', (table) => {
      // reverse alternations
    })
  }
}

module.exports = DropColumnsArmazemSerieSchema
