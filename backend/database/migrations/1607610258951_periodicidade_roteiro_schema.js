'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PeriodicidadeRoteiroSchema extends Schema {
  up () {
    this.create('periodicidade_roteiros', (table) => {
      table.increments()

      table.string('descricao')
      table.string('slug')

      table.timestamps()
    })
  }

  down () {
    this.drop('periodicidade_roteiros')
  }
}

module.exports = PeriodicidadeRoteiroSchema
