'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RegraImpostoSchema extends Schema {
  up () {
    this.create('regra_impostos', (table) => {
      table.increments()
      table.string('regra', 50).notNullable().unique();
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index');
      table.timestamps()
    })
  }

  down () {
    this.drop('regra_impostos')
  }
}

module.exports = RegraImpostoSchema
