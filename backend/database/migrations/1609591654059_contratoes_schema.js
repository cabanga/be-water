'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up () {
    this.table('contratoes', (table) => {
      table.string('morada_contrato')
    })
  }

  down () {
    this.table('contratoes', (table) => {
      table.dropColumn('morada_contrato')
    })
  }
}

module.exports = ContratoesSchema
