'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ConfiguracaosSchema extends Schema {
  up() {
    this.table('configuracaos', (table) => {
      // alter table
      table.boolean('is_boolean').defaultTo(false).after('is_active')

    })
  }

  down() {
    this.table('configuracaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ConfiguracaosSchema
