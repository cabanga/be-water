'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojasSchema extends Schema {
  up () {
    this.table('lojas', (table) => {
      table.string('nif').after('numero')
      table.string('horario').after('nif')


    })
  }

  down () {
    this.table('lojas', (table) => {
      table.dropColumn('nif')
      table.dropColumn('horario')
    })
  }
}

module.exports = LojasSchema
