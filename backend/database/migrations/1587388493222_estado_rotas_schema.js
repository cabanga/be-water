'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoRotasSchema extends Schema {
  up () {
    this.create('estado_rotas', (table) => {
      table.increments()
      
      table.string("designacao", 50).notNullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('estado_rotas')
  }
}

module.exports = EstadoRotasSchema
