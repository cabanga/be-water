'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoCaixaContadorSchema extends Schema {
  up () {
    this.create('estado_caixa_contadors', (table) => {
      table.increments()

      table.string('nome', 50).notNullable().unique()
      table.string('slug', 50).notNullable().unique()
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('estado_caixa_contadors')
  }
}

module.exports = EstadoCaixaContadorSchema
