'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeiturasSchema extends Schema {
  up () {
    this.table('leituras', (table) => {
      table.integer('consumo')
    })
  }

  down () {
    this.table('leituras', (table) => {
      table.integer('consumo')
    })
  }
}

module.exports = LeiturasSchema
