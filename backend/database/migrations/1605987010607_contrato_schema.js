




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up() {
    this.table('contratoes', (table) => {
      // alter table

      table.integer('tipo_medicao_id').unsigned().references('id').inTable('tipo_medicaos').after('conta_id')
      table.integer('mensagem_id').unsigned().references('id').inTable('mensagems').after('objecto_contrato_id')

    })
  }

  down() {
    this.table('contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContratoesSchema



