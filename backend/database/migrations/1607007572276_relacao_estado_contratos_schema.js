'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RelacaoEstadoContratosSchema extends Schema {
  up () {
    this.create('relacao_estado_contratos', (table) => {
      table.increments()
      table.integer('estado_pai').unsigned().references('id').inTable('estado_contratoes')
      table.integer('estado_filho').unsigned().references('id').inTable('estado_contratoes')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('relacao_estado_contratos')
  }
}

module.exports = RelacaoEstadoContratosSchema
