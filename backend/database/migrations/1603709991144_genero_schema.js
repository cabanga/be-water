'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GeneroSchema extends Schema {
  up() {
    this.create('generos', (table) => {
      table.increments()

      table.string('abreviacao', 50)
      table.string('slug', 50)
      table.string('descricao', 150)
      table.boolean('is_delected').defaultTo(false)
      table.timestamps()
    })
  }

  down() {
    this.drop('generos')
  }
}

module.exports = GeneroSchema
