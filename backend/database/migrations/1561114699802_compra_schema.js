'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CompraSchema extends Schema {
  up () {
      this.create('compras', (table) => {
      table.increments();
      table.date('dataCompra').notNullable(); 
      table.integer('totalProdutos').nullable(); 
      table.float('total').notNullable();  
      table.integer("fornecedor_id").nullable(); 
      //table.integer('fornecedor_id').unsigned().notNullable().index('fornecedor_id_indexx').references('id').inTable('fornecedors').onUpdate('CASCADE')
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index'); 
      table.timestamps();
    })
  }

  down () {
    this.drop('compras')
  }
}

module.exports = CompraSchema
