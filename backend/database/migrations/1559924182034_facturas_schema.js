'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturasSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
	  table.integer('pagamento_id').unsigned().nullable().references('id').inTable('pagamentos').onUpdate('CASCADE').index('pagamento_id_index').after('serie_id');
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturasSchema
