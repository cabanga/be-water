'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ParCaixasSchema extends Schema {
  up () {
    this.table('par_caixas', (table) => {
      // alter table
	table.integer('central_id').unsigned().references('id').inTable('central_recurso_redes').after('descricao')
    })
  }

  down () {
    this.table('par_caixas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ParCaixasSchema
