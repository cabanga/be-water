'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BancosSchema extends Schema {
  up () {
    this.table('bancos', (table) => {
      table.string('numero_conta')
    });
    this.table('bancos', (table) => {
      table.string('iban')
    });
  }

  down () {
    this.table('bancos', (table) => {
      table.dropColumn('numero_conta')
    });
    this.table('bancos', (table) => {
      table.dropColumn('iban')
    });
  }
}

module.exports = BancosSchema
