'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoContratoesSchema extends Schema {
  up () {
    this.table('estado_contratoes', (table) => {
      // alter table
      table.string('nivel', 15).after('id')
    })
  }

  down () {
    this.table('estado_contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EstadoContratoesSchema
