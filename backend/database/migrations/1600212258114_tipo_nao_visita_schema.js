'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoNaoVisitaSchema extends Schema {
  up () {
    this.create('tipo_nao_visitas', (table) => {
      table.increments()

      table.string('nome')
      table.boolean('is_active').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users')

      
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_nao_visitas')
  }
}

module.exports = TipoNaoVisitaSchema
