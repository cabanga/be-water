'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ParceriaClienteSchema extends Schema {
  up () {
    this.table('parceria_clientes', (table) => {
      // alter table
      table.string("trunk_in ", 200).nullable();
      table.string("trunk_out", 200).nullable();
    })
  }

  down () {
    this.table('parceria_clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ParceriaClienteSchema
