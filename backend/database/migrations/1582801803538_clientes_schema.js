'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClientesSchema extends Schema {
  up () {
    this.table('clientes', (table) => {
      // alter table
	table.integer('direccao_id').unsigned().index('direccao_id').references('id').inTable('direccaos').nullable().after('direccao')
    })
  }

  down () {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClientesSchema
