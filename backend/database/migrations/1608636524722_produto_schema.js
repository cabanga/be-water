
'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutosSchema extends Schema {
  up() {
    this.table('produtos', (table) => {
      // alter table

      table.dropColumn('tipo')
      table.dropColumn('is_trigger')
      //table.dropColumn('classificacao_produto_id')

      table.boolean('is_editable').defaultTo(false).after('moeda_id')
      table.integer('incidencia_id').unsigned().references('id').inTable('incidencias').after('tipo_produto_id')
      
    })
  }

  down() {
    this.table('produtos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProdutosSchema
