'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContasSchema extends Schema {
  up () {
    this.table('contas', (table) => {
      // alter table
	table.integer('tipo_facturacao_id').unsigned().references('id').inTable('tipo_facturacaos').after('tipoFacturacao')
	table.integer('estado_id').unsigned().references('id').inTable('estado_contas').after('estado')
    })
  }

  down () {
    this.table('contas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContasSchema
