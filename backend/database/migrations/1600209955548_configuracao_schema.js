'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ConfiguracaoSchema extends Schema {
  up () {
    this.create('configuracaos', (table) => {
      table.increments()

      table.string('slug', 45)
      table.string('descricao', 100)
      table.string('modulo', 45)
      table.string('modulo_id', 45)
      table.string('valor', 45)
      table.boolean('default').defaultTo(false)

      table.timestamps()
    })
  }

  down () {
    this.drop('configuracaos')
  }
}

module.exports = ConfiguracaoSchema
