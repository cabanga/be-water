'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NumeracaoSchema extends Schema {
  up () {
    this.create('numeracaos', (table) => {
      table.increments()
	  table.integer('numero').notNullable().unique()
	  table.integer('tecnologia_id').unsigned().references('id').inTable('tecnologias')
	  table.integer('filial_id').unsigned().references('id').inTable('filials')
	  table.boolean('status').defaultTo(false)
	  table.integer('user_id').unsigned().references('id').inTable('users') 
      table.timestamps()
    })
  }

  down () {
    this.drop('numeracaos')
  }
}

module.exports = NumeracaoSchema
