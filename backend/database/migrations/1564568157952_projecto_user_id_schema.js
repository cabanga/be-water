'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectoUserIdSchema extends Schema {
  up () {
    this.table('projectos', (table) => {
      // alter table
      table.integer('user_id').unsigned().index('user_id_index').references('id').inTable('users').nullable().after('data_fim');
    })
  }

  down () {
    this.table('projectos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProjectoUserIdSchema
