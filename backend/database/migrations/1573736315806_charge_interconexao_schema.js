'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargeInterconexaoSchema extends Schema {
  up () {
    this.create('charge_interconexaos', (table) => {
      table.increments()

        table.integer("periodo").nullable();
        table.string("event_direction", 5).nullable();
        table.string("billing_operator", 255).nullable();
        table.string("currency", 50).nullable();
        table.double("amount", 255, 2).nullable();
        table.double("mins", 255, 2).nullable();        

        table.boolean('is_facturado').nullable().defaultTo(false);

        table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();

      table.timestamps()
    })
  }

  down () {
    this.drop('charge_interconexaos')
  }
}

module.exports = ChargeInterconexaoSchema
