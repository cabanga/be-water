'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProjectoSchema extends Schema {
  up () {
    this.create('projectos', (table) => {
      table.increments()

      table.text("descricao").notNullable();
      table.text("morada_projecto").nullable();
      table.string("municipio", 120).nullable();
      table.string("provincia", 120).nullable();
      table.string("telefone", 35).notNullable();
      table.double("custo_total").notNullable();
      table.date("data_inicio").nullable();
      table.date("data_fim").nullable(); 
      table.timestamps()
    })
  }

  down () {
    this.drop('projectos')
  }
}

module.exports = ProjectoSchema
