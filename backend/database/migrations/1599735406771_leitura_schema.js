'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeiturasSchema extends Schema {
  up () {
    this.table('leituras', (table) => {
      // alter table
          
      table.string('latitude', 50).after('data_leitura')
      table.string('longitude', 50).after('latitude')

    })
  }

  down () {
    this.table('leituras', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LeiturasSchema
