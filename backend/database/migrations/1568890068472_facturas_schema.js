'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturasSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      table.double('total', 25,2).notNullable().alter();
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturasSchema
