'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RescisaoContratoSchema extends Schema {
  up () {
    this.create('rescisao_contratoes', (table) => {
      table.increments()
      
      table.integer('contrato_id').unsigned().references('id').inTable('contratoes')
      table.integer('motivo_recisao_id').unsigned().references('id').inTable('motivo_recisaos')
      table.integer('leitura_id').unsigned().references('id').inTable('leituras')
      table.string('leitura_origem', 50)
      table.integer('morada_correspondencia_id').unsigned().references('id').inTable('morada_correspondencias')
      table.date('data_rescisao')

      table.timestamps()
    })
  }

  down () {
    this.drop('rescisao_contratoes')
  }
}

module.exports = RescisaoContratoSchema
