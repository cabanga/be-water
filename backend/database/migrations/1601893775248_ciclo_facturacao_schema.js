'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CicloFacturacaoSchema extends Schema {
  up () {
    this.create('ciclo_facturacaos', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('ciclo_facturacaos')
  }
}

module.exports = CicloFacturacaoSchema
