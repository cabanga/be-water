'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArmazensSchema extends Schema {
  up () {
    this.table('armazens', (table) => {
      // alter table
	table.text('localizacao').after('descricao')
	table.boolean('status').defaultTo(false).after('localizacao')
    })
  }

  down () {
    this.table('armazens', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ArmazensSchema
