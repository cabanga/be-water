'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresasSchema extends Schema {
  up () {
    this.table('empresas', (table) => {
      // alter table
	table.integer('width').after('logotipo')
	table.integer('height').after('width')
    })
  }

  down () {
    this.table('empresas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EmpresasSchema
