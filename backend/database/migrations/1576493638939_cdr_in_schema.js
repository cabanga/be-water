"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class CdrInSchema extends Schema {
  up() {
    this.create("cdr_ins", table => {
      table.increments();

      table.integer("normalised_event_id").nullable();
      table.date("last_modified").nullable();
      table.integer("normalised_event_type_id").nullable();
      table.integer("parent_normalised_event_id").nullable();
      table.integer("normalised_event_file_id").nullable();
      table.integer("file_record_nr").nullable();
      table.integer("external_file_record_nr").nullable();
      table.integer("original_file_id").nullable();
      table.integer("original_file_record_nr").nullable();
      table.integer("reprocessed_count").nullable();
      table.string("a_party_id ", 64).nullable();
      table.string("a_party_network_id", 64).nullable();
      table.string("a_party_name", 64).nullable();
      table.integer("a_party_ton_code").nullable();
      table.integer("a_party_cell_id").nullable();
      table.integer("a_party_carrier_code").nullable();
      table.integer("a_party_location_code").nullable();
      table.string("a_party_route", 80).nullable();
      table.string("b_party_id", 64).nullable();
      table.string("b_party_network_id", 64).nullable();
      table.integer("b_party_name", 64).nullable();
      table.integer("b_party_ton_code").nullable();
      table.string("b_party_cell_id", 30).nullable();
      table.integer("b_party_carrier_code").nullable();
      table.integer("b_party_location_code").nullable();
      table.string("b_party_route", 80).nullable();
      table.string("c_party_id", 64).nullable();
      table.string("c_party_network_id", 64).nullable();
      table.string("c_party_name", 64).nullable();
      table.integer("c_party_internal_id").nullable();
      table.integer("c_party_ton_code").nullable();
      table.string("c_party_cell_id", 20).nullable();
      table.integer("c_party_carrier_code").nullable();
      table.integer("c_party_location_code").nullable();
      table.integer("c_party_route").nullable();
      table.integer("full_path").nullable();
      table.integer("cascade_carrier_code").nullable();
      table.date("switch_start_date").nullable();
      table.date("charge_start_date").nullable();
      table.date("period_start_date").nullable();
      table.date("period_end_date").nullable();
      table.string("event_source", 40).nullable();
      table.integer("event_class_code", 2).nullable();
      table.integer("bill_run_id").nullable();
      table.integer("root_customer_node_id").nullable();
      table.integer("event_type_code").nullable();
      table.integer("event_sub_type_code").nullable();
      table.integer("duration").nullable();
      table.integer("volume").nullable();
      table.integer("pulses").nullable();
      table.integer("charge").nullable();
      table.integer("currency_id").nullable();
      table.string("rate_band", 40).nullable();
      table.text("general_1").nullable();
      table.text("general_2").nullable();
      table.text("general_3").nullable();
      table.text("general_4").nullable();
      table.text("general_5").nullable();
      table.text("general_6").nullable();
      table.text("general_7").nullable();
      table.text("general_8").nullable();
      table.text("general_9").nullable();
      table.text("general_10").nullable();
      table.text("general_11").nullable();
      table.text("general_12").nullable();
      table.text("general_13").nullable();
      table.text("general_14").nullable();
      table.text("general_15").nullable();
      table.text("general_16").nullable();
      table.text("general_17").nullable();
      table.text("general_18").nullable();
      table.text("general_19").nullable();
      table.text("general_20").nullable();
      table.string("dummy", 5).nullable();
      table.integer("partition_nr").nullable();

      table.timestamps();
    });
  }

  down() {
    this.drop("cdr_ins");
  }
}

module.exports = CdrInSchema;
