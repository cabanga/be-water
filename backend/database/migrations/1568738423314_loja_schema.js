'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojaSchema extends Schema {
  up () {
    this.create('lojas', (table) => {
      table.increments()
      table.string("nome", 50).notNullable();
      table.string("telefone").nullable();
      table.string("email", 90).nullable();
      table.integer("numero").nullable();
      table.text("endereco").nullable();
      table.boolean('is_active').nullable().defaultTo(true)


      table.integer('municipio_id').nullable().unsigned().index();
      table.foreign('municipio_id').references('id').on('municipios').onDelete('cascade')

      table.integer('provincia_id').nullable().unsigned().index();
      table.foreign('provincia_id').references('id').on('provincias').onDelete('cascade')

      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('lojas')
  }
}

module.exports = LojaSchema
