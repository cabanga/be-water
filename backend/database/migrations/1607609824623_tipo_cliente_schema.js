'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoClientesSchema extends Schema {
  up () {
    this.table('tipo_clientes', (table) => {
      // alter table
      table.string('slug', 50).notNullable().unique().after('tipoClienteDesc')
    })
  }

  down () {
    this.table('tipo_clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TipoClientesSchema
