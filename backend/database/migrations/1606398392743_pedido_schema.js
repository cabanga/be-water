




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidosSchema extends Schema {
  up() {
    this.table('pedidos', (table) => {
      // alter table

      table.integer('estado_ordem_servico_id').unsigned().references('id').inTable('estado_ordem_servicos').after('local_consumo_id')

    })
  }

  down() {
    this.table('pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PedidosSchema



