'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeiturasSchema extends Schema {
  up () {
    this.table('leituras', (table) => {
      // alter table

      table.date('data_leitura').after('ultima_leitura')

    })
  }

  down () {
    this.table('leituras', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LeiturasSchema
