'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up () {
    this.table('contratoes', (table) => {
      // alter table
      table.integer('user_id').unsigned().references('id').inTable('users').after('estado_id')
    })
  }

  down () {
    this.table('contratoes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContratoesSchema
