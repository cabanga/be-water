'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoPedidosSchema extends Schema {
  up () {
    this.create('estado_pedidos', (table) => {
      table.increments()
	  table.string('designacao',70).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('estado_pedidos')
  }
}

module.exports = EstadoPedidosSchema
