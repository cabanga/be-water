'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipologiaServicoSchema extends Schema {
  up () {
    this.create('tipologia_servicos', (table) => {
      table.increments()

      table.string('nome', 50).notNullable().unique()
      table.string('slug', 50).notNullable().unique()
      table.boolean('nova_ligacao_flag').defaultTo(false)
      table.boolean('novo_contrato_flag').defaultTo(false)
      table.boolean('rescisao_contrato_flag').defaultTo(false)

      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('tipologia_servicos')
  }
}

module.exports = TipologiaServicoSchema
