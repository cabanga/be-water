'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DistritoSchema extends Schema {
  up () {
    this.create('distritos', (table) => {
      table.increments()

      table.string('nome')
      table.boolean('is_active').defaultTo(false)
      table.integer('municipio_id').unsigned().references('id').inTable('municipios')
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('distritos')
  }
}

module.exports = DistritoSchema
