'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BillRunEmailHeaderSchema extends Schema {
  up () {
    this.create('bill_run_email_headers', (table) => {
      table.increments()
	  table.integer('bill_run_header_id').unsigned().references('id').inTable('bill_run_headers')
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('bill_run_email_headers')
  }
}

module.exports = BillRunEmailHeaderSchema
