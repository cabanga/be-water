'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoPedidosSchema extends Schema {
  up () {
    this.table('estado_pedidos', (table) => {
      // alter table
	table.string('sigla',2).nullable().after('designacao');
    })
  }

  down () {
    this.table('estado_pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EstadoPedidosSchema
