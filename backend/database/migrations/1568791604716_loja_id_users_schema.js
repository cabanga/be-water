'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojaIdUsersSchema extends Schema {
  up () {
    this.table('users', (table) => {
      // alter table
      table.integer('loja_id').nullable().unsigned().index();
      table.foreign('loja_id').references('id').on('lojas').onDelete('cascade')
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LojaIdUsersSchema
