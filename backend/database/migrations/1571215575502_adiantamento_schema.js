'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdiantamentoSchema extends Schema {
  up () {
    this.create('adiantamentos', (table) => {
      table.increments()

      table.double("valor", 130, 2).nullable();
      table.text("descricao").nullable();  

      table.integer('cliente_id').nullable().unsigned().index();
      table.foreign('cliente_id').references('id').on('clientes').onDelete('cascade')

      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade')

      table.timestamps()
    })
  }

  down () {
    this.drop('adiantamentos')
  }
}

module.exports = AdiantamentoSchema
