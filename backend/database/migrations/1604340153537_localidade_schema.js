'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalidadeSchema extends Schema {
  up () {
    this.create('localidades', (table) => {
      table.increments()

      table.string('nome', 80)
      table.integer('municipio_id').unsigned().references('id').inTable('municipios')
      table.boolean('is_active').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('localidades')
  }
}

module.exports = LocalidadeSchema
