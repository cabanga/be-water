'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class GestorContaSchema extends Schema {
  up () {
    this.create('gestor_contas', (table) => {
      table.increments()
      table.string('nome')
      table.string('telefone')
      table.string('email')
      table.string('morada')
      table.string('slug')
      table.boolean('is_deleted').defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('gestor_contas')
  }
}
module.exports = GestorContaSchema
