'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoJurosSchema extends Schema {
  up () {
    this.table('tipo_juros', (table) => {
      // alter table
      table.integer('valor_dias').after('dias')
    })
  }

  down () {
    this.table('tipo_juros', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TipoJurosSchema
