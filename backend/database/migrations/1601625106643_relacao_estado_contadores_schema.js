'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RelacaoEstadoContadoresSchema extends Schema {
  up () {
    this.create('relacao_estado_contadores', (table) => {
      table.increments()
      table.integer('estado_pai').unsigned().references('id').inTable('estado_contadores')
      table.integer('estado_filho').unsigned().references('id').inTable('estado_contadores')
      table.timestamps()
    })
  }

  down () {
    this.drop('relacao_estado_contadores')
  }
}

module.exports = RelacaoEstadoContadoresSchema
