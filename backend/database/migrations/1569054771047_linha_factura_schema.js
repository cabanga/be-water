'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturaSchema extends Schema {
  up () {
    this.table('linha_facturas', (table) => {
      // alter table
      table.double("valor_cambio", 25, 2).nullable();
      table.integer("cambio_id").nullable();

      table.string("moeda_iso").nullable();
      table.integer("moeda_id").nullable();
    })
  }

  down () {
    this.table('linha_facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaFacturaSchema
