'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoMensagemSchema extends Schema {
  up () {
    this.create('tipo_mensagems', (table) => {
      table.increments()
      
      table.string('nome', 80).notNullable().unique()
      table.string('slug', 50).notNullable().unique()
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_mensagems')
  }
}

module.exports = TipoMensagemSchema
