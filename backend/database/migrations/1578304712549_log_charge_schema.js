'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogChargeSchema extends Schema {
  up () {
    this.create('log_charges', (table) => {
      table.increments()
      table.double("valor_old", 250, 2).notNullable();
      table.double("valor_new", 250, 2).notNullable();
      table.text("observacao").nullable();
      table.integer('charge_id').unsigned().notNullable().references('id').inTable('charges')
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('log_charges')
  }
}

module.exports = LogChargeSchema
