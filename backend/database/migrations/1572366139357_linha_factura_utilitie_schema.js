'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturaUtilitieSchema extends Schema {
  up () {
    this.table('linha_factura_utilities', (table) => {
      // alter table
      table.integer('cambio_id').unsigned().nullable()
      table.integer('moeda_id').unsigned().nullable()
      table.double('valor_cambio',100,2).nullable()
      table.string("moeda_iso",20).nullable()
    })
  }

  down () {
    this.table('linha_factura_utilities', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaFacturaUtilitieSchema
