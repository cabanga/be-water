'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReclamacaoSchema extends Schema {
  up () {
    this.table('reclamacaos', (table) => {
      // alter table
	table.integer('cliente_id').unsigned().index('cliente_id').references('id').inTable('clientes').after('id')
    })
  }

  down () {
    this.table('reclamacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ReclamacaoSchema
