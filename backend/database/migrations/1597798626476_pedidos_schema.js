'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidosSchema extends Schema {
  up () {
    this.table('pedidos', (table) => {
      // alter table
	table.dropColumn('telefone')
	table.dropColumn('cabo')
	table.dropColumn('armario')
	table.dropColumn('central')
	table.dropColumn('caixa')
	table.dropColumn('par_caixa')
	table.dropColumn('armario_secundario')
	table.dropColumn('armario_primario')
	table.dropColumn('par_cabo')
	table.dropColumn('par_adsl')
	table.dropColumn('dataPedido')
	table.dropColumn('capacidade')
	table.dropColumn('origem')
	table.dropColumn('destino')
    })
  }

  down () {
    this.table('pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PedidosSchema
