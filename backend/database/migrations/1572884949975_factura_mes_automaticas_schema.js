'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaMesAutomaticasSchema extends Schema {
  up () {
    this.table('factura_mes_automaticas', (table) => {
      // alter table
      table.integer('tecnologia_id').unsigned().nullable().references('id').inTable('tecnologias').onUpdate('CASCADE').index();
    })
  }

  down () {
    this.table('factura_mes_automaticas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturaMesAutomaticasSchema
