'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RuasSchema extends Schema {
  up () {
    this.table('ruas', (table) => {
      // alter table
      table.integer('quarteirao_id').unsigned().references('id').inTable('quarteiraos').after('bairro_id')
      
    })
  }

  down () {
    this.table('ruas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RuasSchema
