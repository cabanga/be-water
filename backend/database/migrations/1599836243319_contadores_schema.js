'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresSchema extends Schema {
  up () {
    this.table('contadores', (table) => {
      // alter table
      table.dropColumn('marca')
      table.dropColumn('modelo')
      table.dropColumn('medicao')
      table.dropColumn('fabricante')
      table.dropColumn('classe_precisao')
      table.dropColumn('centro_distribuicao')
      table.integer('marca_id').nullable().unsigned().references('id').inTable('marcas')
      table.integer('modelo_id').nullable().unsigned().references('id').inTable('modelos')
      table.integer('medicao_id').nullable().unsigned().references('id').inTable('medicaos')
      table.integer('fabricante_id').nullable().unsigned().references('id').inTable('fabricantes')
      table.integer('tipo_contador_id').nullable().unsigned().references('id').inTable('tipo_contadors')
      table.integer('armazem_id').nullable().unsigned().references('id').inTable('armazens')
    })
  }

  down () {
    this.table('contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContadoresSchema
