'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TarifarioSchema extends Schema {
  up () {
    this.table('tarifarios', (table) => {
      // alter table
      table.integer('tecnologia_id').unsigned().nullable().references('id').inTable('tecnologias').onUpdate('CASCADE').index();
    })
  }

  down () {
    this.table('tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TarifarioSchema
