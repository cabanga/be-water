'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoJurosSchema extends Schema {
  up () {
    this.table('tipo_juros', (table) => {
      // alter table
      table.string('slug').after('percentagem')
    })
  }

  down () {
    this.table('tipo_juros', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TipoJurosSchema
