'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargesSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      // alter table
	table.integer('user_id').unsigned().references('id').inTable('users').after('periodo')
    })
  }

  down () {
    this.table('charges', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargesSchema
