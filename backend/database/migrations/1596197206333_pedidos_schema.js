'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PedidosSchema extends Schema {
  up () {
    this.table('pedidos', (table) => {
      // alter table
	 table.integer('conta_id').unsigned().references('id').inTable('contas').after('tipoPedido')
	 table.integer('tecnologia_id').unsigned().references('id').inTable('tecnologias').after('conta_id')
	 table.integer('residencia_id').unsigned().references('id').inTable('residencias').after('tarifario_id')
    })
  }

  down () {
    this.table('pedidos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PedidosSchema
