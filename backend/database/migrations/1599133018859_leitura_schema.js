'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LeituraSchema extends Schema {
  up () {
    this.create('leituras', (table) => {
      table.increments()
            
      table.integer('rota_run_id').unsigned().references('id').inTable('rota_runs') 
      table.integer('contador_id').unsigned().references('id').inTable('contadores') 
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.integer('periodo')
      table.integer('leitura')
      table.integer('ultima_leitura')      

      table.timestamps()
    })
  }

  down () {
    this.drop('leituras')
  }
}

module.exports = LeituraSchema
