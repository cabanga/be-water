'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AlterColumnsProdutosSchema extends Schema {
  up () {
    this.table('produtos', (table) => {
      // alter table
      table.text('observacao').nullable().after('tipo');
    })
  }

  down () {
    this.table('produtos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AlterColumnsProdutosSchema
