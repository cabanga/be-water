'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BillRunEmailSchema extends Schema {
  up () {
    this.create('bill_run_emails', (table) => {
      table.increments()
	  table.integer('bill_run_email_header_id').unsigned().references('id').inTable('bill_run_email_headers')
	  table.integer('factura_id').unsigned().references('id').inTable('facturas')
      table.timestamps()
    })
  }

  down () {
    this.drop('bill_run_emails')
  }
}

module.exports = BillRunEmailSchema
