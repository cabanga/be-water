'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogsContadoresSchema extends Schema {
  up () {
    this.table('logs_contadores', (table) => {
      // alter table
      table.integer('id_origem').after('user_id')
    })
  }

  down () {
    this.table('logs_contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LogsContadoresSchema
