'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BairrosSchema extends Schema {
  up () {
    this.table('bairros', (table) => {
      // alter table

      table.string('nome', 80).after('id')
      table.boolean('is_active').defaultTo(false).after('nome')
      table.integer('municipio_id').unsigned().references('id').inTable('municipios').after('is_active')
      table.integer('user_id').unsigned().references('id').inTable('users').after('municipio_id')
    })
  }

  down () {
    this.table('bairros', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BairrosSchema
