'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClasseTarifarioSchema extends Schema {
  up() {
    this.create('classe_tarifarios', (table) => {
      table.increments()
      table.integer('tarifario_id').unsigned().references('id').inTable('tarifarios')
      table.decimal('valor')
      table.integer('consumo_minimo')
      table.integer('consumo_maximo')
      table.timestamps()
    })
  }

  down() {
    this.drop('classe_tarifarios')
  }
}

module.exports = ClasseTarifarioSchema
