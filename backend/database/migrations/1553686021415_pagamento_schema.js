'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentoSchema extends Schema {
  up () {
    this.create('pagamentos', (table) => {
      table.increments()
      table.string('descricao', 100).notNullable().unique();
      table.string('imagem', 250).notNullable().unique();
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE');
      table.timestamps()
    })
  }

  down () {
    this.drop('pagamentos')
  }
}

module.exports = PagamentoSchema
