'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class IplcServicoSchema extends Schema {
  up () {
    this.create('iplc_servicos', (table) => {
      table.increments() 

        table.double('valor',255,2).nullable()
        table.string("capacidade",255).nullable()
        table.string("origem",255).nullable()
        table.string("destino",255).nullable()

        table.integer('servico_id').unsigned().nullable().references('id').inTable('servicos').onUpdate('CASCADE').index();
        table.integer('moeda_id').unsigned().nullable().references('id').inTable('moedas').onUpdate('CASCADE').index();
        table.integer('imposto_id').unsigned().nullable().references('id').inTable('impostos').onUpdate('CASCADE').index();
        table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();
      table.timestamps()
    })
  }

  down () {
    this.drop('iplc_servicos')
  }
}

module.exports = IplcServicoSchema
