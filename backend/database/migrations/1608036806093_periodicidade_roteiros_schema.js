'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PeriodicidadeRoteirosSchema extends Schema {
  up () {
    this.table('periodicidade_roteiros', (table) => {
      table.integer("ciclo").notNullable().defaultTo(1)
    })
  }

  down () {
    this.table('periodicidade_roteiros', (table) => {
      table.dropColumn('ciclo')

      // reverse alternations
    })
  }
}

module.exports = PeriodicidadeRoteirosSchema
