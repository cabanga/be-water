'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresSchema extends Schema {
  up () {
    this.table('contadores', (table) => {
      // alter table
      table.string('classe_precisao',80).nullable()
      table.string('calibre',80).nullable()
      table.string('digitos',80).nullable()
      table.integer('ano_fabrico').nullable()
      table.integer('ano_substituicao').nullable()
      table.string('validade').nullable()
      table.string('observacao',80).nullable()
      table.string('fabricante',80).nullable()
      table.string('especificacao',80).nullable()
      table.string('dados_selo',70).notNullable()
    })
  }

  down () {
    this.table('contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContadoresSchema
