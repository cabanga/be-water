'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaPagamentoSchema extends Schema {
  up () {
    this.create('linha_pagamentos', (table) => {

    table.increments()

    table.double("valor_recebido", 100, 2).nullable()
    table.double("troco", 100, 2).nullable()
 
    table.integer('referencia').nullable()
    table.date("data_pagamento").nullable();
 

    table.integer('pagamento_id').unsigned().notNullable().references('id').inTable('pagamentos').onUpdate('CASCADE');
    table.integer('forma_pagamento_id').unsigned().notNullable().references('id').inTable('forma_pagamentos').onUpdate('CASCADE');

    table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE');
    
    table.timestamps()
    })
  }

  down () {
    this.drop('linha_pagamentos')
  }
}

module.exports = LinhaPagamentoSchema
