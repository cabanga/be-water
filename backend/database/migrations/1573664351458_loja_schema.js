'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojaSchema extends Schema {
  up () {
    this.table('lojas', (table) => {
      // alter table
      table.integer('serie_id_recibo').unsigned().nullable().references('id').inTable('series').onUpdate('CASCADE').index('serie_id_recibo').after('serie_id');
    })
  }

  down () {
    this.table('lojas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LojaSchema
