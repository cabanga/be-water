'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CalibreSchema extends Schema {
  up () {
    this.create('calibres', (table) => {
      table.increments()
      table.string('descricao')
      table.string('slug')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('calibres')
  }
}

module.exports = CalibreSchema
