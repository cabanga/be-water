'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoRamalSchema extends Schema {
  up () {
    this.create('tipo_ramals', (table) => {
      table.increments()
      table.string('descricao')
      table.string('slug')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('tipo_ramals')
  }
}

module.exports = TipoRamalSchema
