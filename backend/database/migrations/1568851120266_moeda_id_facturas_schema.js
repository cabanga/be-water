'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MoedaIdFacturasSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
      table.integer('moeda_id').nullable().unsigned().index();
      table.foreign('moeda_id').references('id').on('moedas').onDelete('cascade') 
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = MoedaIdFacturasSchema
