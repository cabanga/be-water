'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StkTipoMovimentosSchema extends Schema {
  up () {
    this.table('stk_tipo_movimentos', (table) => {
      // alter table
	table.string('movimento',2).after('slug')
    })
  }

  down () {
    this.table('stk_tipo_movimentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = StkTipoMovimentosSchema
