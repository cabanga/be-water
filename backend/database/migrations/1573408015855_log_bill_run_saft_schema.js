'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogBillRunSaftSchema extends Schema {
  up () {
    this.create('log_bill_run_safts', (table) => {
      table.increments()
      table.text('nome_saft').nullable();
      table.integer("mes_de").nullable();
      table.integer("mes_para").nullable();
      table.integer('ano').nullable();
      table.integer('estado').defaultTo(0);      
      table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();
      table.timestamps()
    })
  }

  down () {
    this.drop('log_bill_run_safts')
  }
}

module.exports = LogBillRunSaftSchema
