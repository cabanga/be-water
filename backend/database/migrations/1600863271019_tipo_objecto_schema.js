'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipoObjectoSchema extends Schema {
  up() {
    this.create('tipo_objectos', (table) => {
      table.increments()
      table.string('descricao', 150)
      table.string('slug', 70)
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down() {
    this.drop('tipo_objectos')
  }
}

module.exports = TipoObjectoSchema
