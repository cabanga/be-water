'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClasseTarifarioSchema extends Schema {
  up() {
    this.table('classe_tarifarios', (table) => {
      table.integer('produto_id').unsigned().references('id').inTable('produtos').after('tarifario_id')
      table.string('descricao', 120).after('produto_id')
      // alter table
    })
  }

  down() {
    this.table('classe_tarifarios', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClasseTarifarioSchema
