'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RecibosSchema extends Schema {
  up () {
    this.alter('recibos', (table) => {
      table.string('recibo_sigla')
      .notNullable()
      .unique()
      .alter()
    })

    this.alter('recibos', (table) => {
      table.integer('numero')
      .notNullable()
      .unique()
      .alter()
    })
  }

  down () {
    this.table('recibos', (table) => {
      table.string('recibo_sigla')
      table.string('numero')
    })
  }
}

module.exports = RecibosSchema
