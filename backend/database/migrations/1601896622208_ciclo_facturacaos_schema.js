'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CicloFacturacaosSchema extends Schema {
  up () {
    this.table('ciclo_facturacaos', (table) => {
      // alter table
	 table.integer('dia_facturacao').after('id')
	  table.integer('ano').after('dia_facturacao')
	  table.integer('mes').after('ano')
	  table.integer('serie_id').unsigned().references('id').inTable('series').after('mes')
	  table.integer('dia_ultima_leitura').after('serie_id')
	  table.integer('estado_id').unsigned().references('id').inTable('estado_ciclo_facturacaos').after('dia_ultima_leitura')
	  table.integer('user_id').unsigned().references('id').inTable('users').after('estado_id')
    })
  }

  down () {
    this.table('ciclo_facturacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = CicloFacturacaosSchema
