'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClassificacaoProdutoSchema extends Schema {
    up () {
        this.create('classificacao_produtos', (table) => {
            table.increments()
            table.string('slug').nullable()
            table.string('descricao').nullable()

            table.timestamps()
        })
    }

    down () {
        this.drop('classificacao_produtos')
    }
}

module.exports = ClassificacaoProdutoSchema
