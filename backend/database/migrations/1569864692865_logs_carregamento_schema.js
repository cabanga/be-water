'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogsCarregamentoSchema extends Schema {
  up () {
    this.create('logs_carregamentos', (table) => {
      table.increments()
      
       table.text("chaveServico", 255).nullable();
        table.text("tecnologia", 255).nullable(); 
        table.double("valor", 100, 2).nullable(); 
        table.text("str_carregamento", 255).nullable(); 

        table.boolean('is_carregado').nullable().defaultTo(false); 

        table.integer('produto_id').unsigned().nullable().references('id').inTable('produtos').onUpdate('CASCADE').index();
        table.integer('servico_id').unsigned().nullable().references('id').inTable('servicos').onUpdate('CASCADE').index();
        table.integer('factura_id').unsigned().nullable().references('id').inTable('facturas').onUpdate('CASCADE').index();
        table.integer('user_id').unsigned().nullable().references('id').inTable('users').onUpdate('CASCADE').index();

      table.timestamps()
    })
  }

  down () {
    this.drop('logs_carregamentos')
  }
}

module.exports = LogsCarregamentoSchema
