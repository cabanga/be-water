'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TiposTipologiasSchema extends Schema {
  up () {
    this.create('tipos_tipologias', (table) => {
      table.increments()
      table.integer('tipologia_id').unsigned().references('id').inTable('tipologia_clientes')
      table.string('descricao')
      table.string('slug')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('tipos_tipologias')
  }
}

module.exports = TiposTipologiasSchema
