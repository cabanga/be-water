'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContextoConfiguracaoSchema extends Schema {
  up () {
    this.create('contexto_configuracaos', (table) => {
      table.increments()
      
      table.string('nome', 50)
      table.string('campo', 50)

      table.timestamps()
    })
  }

  down () {
    this.drop('contexto_configuracaos')
  }
}

module.exports = ContextoConfiguracaoSchema
