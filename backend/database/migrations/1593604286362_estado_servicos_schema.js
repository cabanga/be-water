'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoServicosSchema extends Schema {
  up () {
    this.table('estado_servicos', (table) => {
      // alter table
      table.string('slug')
      table.integer('user_id').unsigned().references('id').inTable('users').after('slug')
    })
  }

  down () {
    this.table('estado_servicos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EstadoServicosSchema
