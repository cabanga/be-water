'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MovimentoAdiantamentoSchema extends Schema {
  up () {
    this.table('movimento_adiantamentos', (table) => {
      // alter table 
      table.string("referencia").nullable();
      table.date("data_pagamento").nullable();
      table.integer('banco_id').unsigned().nullable()
    })
  }

  down () {
    this.table('movimento_adiantamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = MovimentoAdiantamentoSchema
