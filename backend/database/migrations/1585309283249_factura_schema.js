'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
      table.double('imposto_cativo', 255,2).nullable().defaultTo(0).after('total')
      table.double('valor_cativo', 255,2).nullable().defaultTo(0).after('total')
      
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturaSchema
