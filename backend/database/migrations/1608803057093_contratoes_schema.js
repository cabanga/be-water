'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoesSchema extends Schema {
  up () {
    this.table('contratoes', (table) => {
      table.integer('cliente_id').nullable().unsigned().index()
      table.foreign('cliente_id').references('id').on('clientes').onDelete('cascade')
    })
  }

  down () {
    this.table('contratoes', (table) => {
      table.dropColumn('cliente_id')
    })
  }
}

module.exports = ContratoesSchema
