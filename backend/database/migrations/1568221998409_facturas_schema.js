'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturasSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      //table.integer("recibo_id").nullable().unsigned();
      table.double('totalComImposto', 25,2).notNullable().alter();
 	    table.double('totalSemImposto', 25,2).notNullable().alter();
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturasSchema
