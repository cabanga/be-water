'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaosSchema extends Schema {
  up() {
    this.table('local_instalacaos', (table) => {
      // alter table

      table.boolean('saneamento_flag').defaultTo(false).after('rua_id')

      table.integer('instalacao_sanitaria_qtde').after('saneamento_flag')
      table.boolean('reservatorio_flag').defaultTo(false).after('instalacao_sanitaria_qtde')
      table.integer('reservatorio_capacidade').after('reservatorio_flag')
      table.boolean('piscina_flag').defaultTo(false).after('reservatorio_capacidade')
      table.integer('piscina_capacidade').after('piscina_flag')
      table.boolean('jardim_flag').defaultTo(false).after('piscina_capacidade')
      table.integer('campo_jardim_id').unsigned().references('id').inTable('campo_jardims').after('jardim_flag')
      table.boolean('poco_alternativo_flag').defaultTo(false).after('campo_jardim_id')
      table.boolean('fossa_flag').defaultTo(false).after('poco_alternativo_flag')
      table.integer('fossa_capacidade').after('fossa_flag')
      table.boolean('acesso_camiao_flag').defaultTo(false).after('fossa_capacidade')
      table.boolean('anexo_flag').defaultTo(false).after('acesso_camiao_flag')
      table.integer('anexo_quantidade').after('anexo_flag')
      table.boolean('caixa_contador_flag').defaultTo(false).after('anexo_quantidade')
      table.integer('estado_caixa_contador_id').nullable().unsigned().references('id').inTable('estado_caixa_contadors').after('caixa_contador_flag')
      table.integer('abastecimento_cil_id').unsigned().references('id').inTable('abastecimento_cils').after('estado_caixa_contador_id')
      table.integer('calibre_id').unsigned().references('id').inTable('calibres').after('abastecimento_cil_id')
    
    })
  }

  down() {
    this.table('local_instalacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LocalInstalacaosSchema
