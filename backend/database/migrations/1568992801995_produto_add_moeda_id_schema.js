'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoAddMoedaIdSchema extends Schema {
  up () {
    this.table('produtos', (table) => {
      // alter table
      table.integer('moeda_id').nullable().unsigned().index();
      table.foreign('moeda_id').references('id').on('moedas').onDelete('cascade') 
    })
  }

  down () {
    this.table('produtos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProdutoAddMoedaIdSchema
