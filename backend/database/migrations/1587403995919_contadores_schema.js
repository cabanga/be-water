'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresSchema extends Schema {
  up () {
    this.create('contadores', (table) => {
      table.increments()
      table.string('contador',70).notNullable()
      table.string('marca',80).nullable()
      table.string('modelo',80).nullable()
      table.string('medicao',80).nullable()
      table.string('TipoContadorFact',70).nullable()
      table.integer('filial_id').unsigned().references('id').inTable('filials')
      table.string('numero_serie',70).notNullable()
      table.integer('servico_id').unsigned().references('id').inTable('servicos')
      table.timestamps()
    })  }

  down () {
    this.drop('contadores')
  }
}

module.exports = ContadoresSchema
