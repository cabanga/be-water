'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class MotivoRecisaoSchema extends Schema {
  up () {
    this.create('motivo_recisaos', (table) => {
      table.increments()

      table.string('nome', 50).notNullable().unique()
      table.string('slug', 50).notNullable().unique()
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('motivo_recisaos')
  }
}

module.exports = MotivoRecisaoSchema
