'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ParceriaClienteSchema extends Schema {
  up () {
    this.table('parceria_clientes', (table) => {
      // alter table
      table.integer('imposto_id').nullable().unsigned().index('parceria_clientes_imposto_id').references('id').inTable('impostos').after("moeda_id");
    })
  }

  down () {
    this.table('parceria_clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ParceriaClienteSchema
