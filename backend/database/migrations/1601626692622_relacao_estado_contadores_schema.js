'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RelacaoEstadoContadoresSchema extends Schema {
  up () {
    this.table('relacao_estado_contadores', (table) => {
      // alter table
      table.integer('user_id').unsigned().references('id').inTable('users').after('estado_filho')
    })
  }

  down () {
    this.table('relacao_estado_contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RelacaoEstadoContadoresSchema
