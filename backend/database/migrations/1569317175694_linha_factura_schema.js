'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturaSchema extends Schema {
  up () {
    this.table('linha_facturas', (table) => {
      // alter table
      table.integer('servico_id').nullable().unsigned().index();
      table.foreign('servico_id').references('id').on('servicos').onDelete('cascade')
    })
  }

  down () {
    this.table('linha_facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaFacturaSchema
