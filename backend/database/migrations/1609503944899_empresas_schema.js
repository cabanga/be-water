'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EmpresasSchema extends Schema {
  up () {
    this.table('empresas', (table) => {
      // alter table
	table.string('site',200).after('email')
    })
  }

  down () {
    this.table('empresas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EmpresasSchema
