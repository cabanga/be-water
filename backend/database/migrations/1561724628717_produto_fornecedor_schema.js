'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutoFornecedorSchema extends Schema {
  up () {
    this.create('produto_fornecedors', (table) => {
      table.increments()
      table.string('nome', 100).notNullable()
      table.string('barcode', 200).nullable()
      table.float('preco').notNullable();
      table.integer('quantidade').nullable(); 
      table.integer("fornecedor_id").nullable();
      //table.integer('fornecedor_id').unsigned().index('fornecedor_id').references('id').inTable('fornecedors');
      table.integer('user_id').unsigned().index('user_id_index').references('id').inTable('users').nullable();
      table.timestamps()
    })
  }

  down () {
    this.drop('produto_fornecedors')
  }
}

module.exports = ProdutoFornecedorSchema
