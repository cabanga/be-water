'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DireccaoSchema extends Schema {
  up () {
    this.create('direccaos', (table) => {
      table.increments()
	  table.string('designacao',70)
	  table.string('slug',70).nullable()
	  table.integer('user_id').unsigned().index('user_id').references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('direccaos')
  }
}

module.exports = DireccaoSchema
