'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ArmarioRecursoRedesSchema extends Schema {
  up () {
    this.table('armario_recurso_redes', (table) => {
      // alter table
	table.integer('central_id').unsigned().references('id').inTable('central_recurso_redes').after('descricao')
    })
  }

  down () {
    this.table('armario_recurso_redes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ArmarioRecursoRedesSchema
