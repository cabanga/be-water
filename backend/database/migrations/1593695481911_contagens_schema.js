'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContagensSchema extends Schema {
  up () {
    this.table('contagens', (table) => {
      // alter table
      table.string('longitude').after('latitude')
    })
  }

  down () {
    this.table('contagens', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContagensSchema
