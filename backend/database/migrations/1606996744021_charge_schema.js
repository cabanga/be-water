'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargeSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      // alter table
            table
              .integer("contador_id")
              .unsigned()
              .references("id")
              .inTable("contadores").after('servico_id')

    })
  }

  down () {
    this.table('charges', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargeSchema
