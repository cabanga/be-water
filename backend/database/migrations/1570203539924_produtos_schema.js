'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProdutosSchema extends Schema {
  up () {
    this.table('produtos', (table) => {
      // alter table
	 table.boolean('is_active').after('tipo').defaultTo(true)
    })
  }

  down () {
    this.table('produtos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ProdutosSchema
