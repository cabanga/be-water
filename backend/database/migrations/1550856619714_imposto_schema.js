'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ImpostoSchema extends Schema {
  up () {
    this.create('impostos', (table) => {
      table.increments()
      table.string('descricao', 200).notNullable().unique();
      table.string('codigo', 50).notNullable().unique();
      table.float('valor').notNullable();
      table.boolean('activo').notNullable();
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users');
      table.timestamps()
    })
  }

  down () {
    this.drop('impostos')
  }
}

module.exports = ImpostoSchema
