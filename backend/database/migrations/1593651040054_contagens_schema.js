'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContagensSchema extends Schema {
  up () {
    this.table('contagens', (table) => {
      // alter table
      table.integer('origem_id').unsigned().references('id').inTable('origems').after('user_id')

      table.string('latitude').after('ultima_leitura')
      table.integer('longitude').after('latitude')
    })
  }

  down () {
    this.table('contagens', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContagensSchema
