'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LojasSchema extends Schema {
  up () {
    this.table('lojas', (table) => {
      table.integer('serie_id_factura').nullable().unsigned().index()
      table.foreign('serie_id_factura').references('id').on('series').onDelete('cascade')
    })
  }

  down () {
    this.table('lojas', (table) => {
      table.dropColumn('serie_id_factura')
    })
  }
}

module.exports = LojasSchema
