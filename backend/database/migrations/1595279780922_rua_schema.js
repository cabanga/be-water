'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RuaSchema extends Schema {
  up () {
    this.create('ruas', (table) => {
      table.increments()

      table.string('nome', 80)
      table.integer('bairro_id').unsigned().references('id').inTable('bairros')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.boolean('is_active').defaultTo(false)
      
      table.timestamps()
    })
  }

  down () {
    this.drop('ruas')
  }
}

module.exports = RuaSchema
