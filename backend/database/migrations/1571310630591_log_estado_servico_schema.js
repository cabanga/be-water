'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogEstadoServicoSchema extends Schema {
  up () {
    this.create('log_estado_servicos', (table) => {
      table.increments()
	  table.integer('servico_id').unsigned().references('id').inTable('servicos')
	  table.integer('id_estado_anterior').unsigned().references('id').inTable('estado_servicos')
	  table.integer('id_estado_novo').unsigned().references('id').inTable('estado_servicos')
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('log_estado_servicos')
  }
}

module.exports = LogEstadoServicoSchema
