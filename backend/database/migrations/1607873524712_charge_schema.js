'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargeSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      // alter table
      table.integer('ciclo_facturacao_id').nullable().unsigned().references('id').inTable('ciclo_facturacaos').after('user_id')
    })
  }

  down () {
    this.table('charges', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargeSchema
