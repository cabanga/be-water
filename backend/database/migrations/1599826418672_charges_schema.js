'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargesSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      // alter table
	table.dropColumn('conta_id')
	table.dropColumn('servico_id')
	table.dropColumn('invoiceText')
    })
  }

  down () {
    this.table('charges', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargesSchema
