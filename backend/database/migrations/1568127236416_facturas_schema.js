'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturasSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      // alter table
	table.float('totalComImposto', 20,2).notNullable().alter();
	table.float('totalSemImposto', 20,2).notNullable().alter();
    table.float('total', 20,2).notNullable().alter();
    })
  }

  down () {
    this.table('facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = FacturasSchema
