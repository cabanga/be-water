'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RotaRunSchema extends Schema {
  up () {
    this.create('rota_runs', (table) => {
      table.increments()

      table.integer('rota_header_id').nullable().unsigned().references('id').inTable('rota_headers')
      table.integer('local_consumo_id').unsigned().references('id').inTable('local_consumos')
      table.integer('estado_rota_id').unsigned().references('id').inTable('estado_rotas')

      table.timestamps()
    })
  }

  down () {
    this.drop('rota_runs')
  }
}

module.exports = RotaRunSchema
