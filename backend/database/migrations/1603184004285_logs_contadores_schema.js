'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LogsContadoresSchema extends Schema {
  up () {
    this.table('logs_contadores', (table) => {
      // alter table
      table.integer('conta_id').unsigned().references('id').inTable('contas').after('tipo_registo_id')
      table.integer('local_instalacao_id').unsigned().references('id').inTable('local_instalacaos').after('conta_id')
      table.integer('local_consumo_id').unsigned().references('id').inTable('local_consumos').after('local_instalacao_id')
    })
  }

  down () {
    this.table('logs_contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LogsContadoresSchema
