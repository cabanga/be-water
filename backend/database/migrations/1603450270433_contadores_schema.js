'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContadoresSchema extends Schema {
  up () {
    this.table('contadores', (table) => {
      // alter table
      table.dropColumn('validade')
      table.dropColumn('especificacao')
      table.dropColumn('calibre')
      table.dropColumn('TipoContadorFact')
      table.dropColumn('dados_selo')
      table.dropColumn('tipo_fabricacao')
      table.dropColumn('ano_substituicao')
      table.integer('calibre_id').unsigned().references('id').inTable('calibres')
      table.integer('caudal_id').unsigned().references('id').inTable('caudals')
      table.boolean('selo').defaultTo(false)
    })
  }

  down () {
    this.table('contadores', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContadoresSchema
