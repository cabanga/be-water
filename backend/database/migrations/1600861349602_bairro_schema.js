'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BairrosSchema extends Schema {
  up () {
    this.table('bairros', (table) => {
      // alter table
      table.boolean('has_quarteirao').defaultTo(false).after('nome')
    })
  }

  down () {
    this.table('bairros', (table) => {
      // reverse alternations
    })
  }
}

module.exports = BairrosSchema
