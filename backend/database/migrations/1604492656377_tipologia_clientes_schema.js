'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class TipologiaClientesSchema extends Schema {
  up () {
    this.table('tipologia_clientes', (table) => {
      // alter table
      table.boolean('sujeito_corte').defaultTo(false).alter()
    })
  }

  down () {
    this.table('tipologia_clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = TipologiaClientesSchema
