'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FilialSchema extends Schema {
  up () {
    this.create('filials', (table) => {
      table.increments()
      table.string("nome", 255).nullable();
      table.integer('user_id').nullable().unsigned().index();
      table.foreign('user_id').references('id').on('users').onDelete('cascade')
      table.timestamps()
    })
  }

  down () {
    this.drop('filials')
  }
}

module.exports = FilialSchema
