'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaoHistoricoSchema extends Schema {
  up () {
    this.create('local_instalacao_historicos', (table) => {
      table.increments()

      table.string('operacao', 30)
      table.integer('local_instalacao_id').unsigned().references('id').inTable('local_instalacaos')
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('local_instalacao_historicos')
  }
}

module.exports = LocalInstalacaoHistoricoSchema
