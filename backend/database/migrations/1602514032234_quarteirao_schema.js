'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuarteiraoSchema extends Schema {
  up () {
    this.create('quarteiraos', (table) => {
      table.increments()

      table.string('nome', 80)
      table.integer('bairro_id').unsigned().references('id').inTable('bairros')
      table.boolean('is_active').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('quarteiraos')
  }
}

module.exports = QuarteiraoSchema
