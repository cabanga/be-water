'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChargeSchema extends Schema {
  up () {
    this.table('charges', (table) => {
      // alter table
      table.integer('rota_header_id').nullable().unsigned().references('id').inTable('rota_headers').after('periodo')

    })
  }

  down () {
    this.table('charges', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ChargeSchema
