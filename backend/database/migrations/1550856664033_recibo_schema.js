'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ReciboSchema extends Schema {
  up () {
    this.create('recibos', (table) => {
      table.increments()
      table.integer('numero').notNullable();
      table.float('total').notNullable();
	  table.string('recibo_sigla', 50).nullable();
      table.string('hash', 200).nullable().unique();
      table.string('hash_control', 200).nullable(); 
      table.boolean('pago').notNullable();
      table.string('status',1).notNullable();
      table.datetime('status_date').notNullable();
      table.string('status_reason', 50).nullable();
      table.integer('serie_id').unsigned().notNullable().references('id').inTable('series').onUpdate('CASCADE').index('serie_id_index');
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE');
	    table.integer('cliente_id').unsigned().notNullable().references('id').inTable('clientes').onUpdate('CASCADE').index('cliente_id_index');
      table.timestamps()
    })
  }

  down () {
    this.drop('recibos')
  }
}

module.exports = ReciboSchema
