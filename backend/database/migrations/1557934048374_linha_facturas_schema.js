'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaFacturasSchema extends Schema {
  up () {
    this.table('linha_facturas', (table) => {
      // alter table
	  table.float('linhaTotalSemImposto').notNullable().after('valor_imposto');
    })
  }

  down () {
    this.table('linha_facturas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LinhaFacturasSchema
