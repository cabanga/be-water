'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChaveServicoSchema extends Schema {
  up () {
    this.create('chave_servicos', (table) => {
      table.increments()
      table.integer("chaveServico").notNullable().unique();
      table.timestamps()
    })
  }

  down () {
    this.drop('chave_servicos')
  }
}

module.exports = ChaveServicoSchema
