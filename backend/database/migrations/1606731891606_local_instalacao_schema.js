




'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LocalInstalacaosSchema extends Schema {
  up() {
    this.table('local_instalacaos', (table) => {
      // alter table

      table.integer('predio_andar').after('predio_nome')

    })
  }

  down() {
    this.table('local_instalacaos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = LocalInstalacaosSchema



