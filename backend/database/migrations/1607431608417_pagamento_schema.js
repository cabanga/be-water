'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PagamentoSchema extends Schema {
  up () {
    this.table('pagamentos', (table) => {
      // alter table
      table.boolean('status').defaultTo(false).after('documento_sigla')
      table.string('annulment_reason', 250).nullable().after('documento_sigla')
      table.datetime('annulment_date').nullable().after('documento_sigla')
    })
  }

  down () {
    this.table('pagamentos', (table) => {
      // reverse alternations
    })
  }
}

module.exports = PagamentoSchema
