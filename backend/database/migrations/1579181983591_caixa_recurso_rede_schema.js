'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CaixaRecursoRedeSchema extends Schema {
  up () {
    this.create('caixa_recurso_redes', (table) => {
      table.increments()
	  table.string('descricao', 150).notNullable()
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('caixa_recurso_redes')
  }
}

module.exports = CaixaRecursoRedeSchema
