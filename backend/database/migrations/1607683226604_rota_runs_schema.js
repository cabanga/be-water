'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RotaRunsSchema extends Schema {
  up () {
    this.table('rota_runs', (table) => {
      table.integer("dia_mes").notNullable().defaultTo(1)
      table.string("dia_semana")
      table.integer('periodicidade_roteiro_id').nullable().unsigned().index()
      table.foreign('periodicidade_roteiro_id').references('id').on('periodicidade_roteiros').onDelete('cascade')
    })
  }

  down () {
    this.table('rota_runs', (table) => {
      table.dropColumn('dia_mes')
      table.dropColumn('dia_semana')
      table.dropColumn('periodicidade_roteiro_id')
    })
  }
}

module.exports = RotaRunsSchema
