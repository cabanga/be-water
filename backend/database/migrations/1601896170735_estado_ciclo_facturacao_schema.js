'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EstadoCicloFacturacaoSchema extends Schema {
  up () {
    this.create('estado_ciclo_facturacaos', (table) => {
      table.increments()
	  table.string('descricao', 70)
	  table.string('slug', 30)
	  table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('estado_ciclo_facturacaos')
  }
}

module.exports = EstadoCicloFacturacaoSchema
