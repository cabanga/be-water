'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContratoHistoricoSchema extends Schema {
  up () {
    this.create('contrato_historicos', (table) => {
      table.increments()

      table.string('operacao', 30)
      table.integer('contrato_id').unsigned().references('id').inTable('contratoes')
      table.longText('historico')
      table.longText('actualizacao')
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('contrato_historicos')
  }
}

module.exports = ContratoHistoricoSchema
