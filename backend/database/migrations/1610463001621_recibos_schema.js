'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RecibosSchema extends Schema {
  up () {
    this.table('recibos', (table) => {
      table.integer('caixa_id').nullable().unsigned().index()
      table.foreign('caixa_id').references('id').on('caixas').onDelete('cascade')
    })
  }

  down () {
    this.table('recibos', (table) => {
      table.dropColumn('caixa_id')
    })
  }
}

module.exports = RecibosSchema
