'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LigacaoRamalSchema extends Schema {
  up() {
    this.create('ligacao_ramals', (table) => {
      table.increments()
      table.string('diamentro')
      table.string('profundidade')
      table.string('comprimento')
      table.integer('ponto_a_id').unsigned().references('id').inTable('objecto_ligacao_ramals')
      table.integer('ponto_b_id').unsigned().references('id').inTable('objecto_ligacao_ramals')
      table.integer('local_instalacao_id').unsigned().references('id').inTable('local_instalacaos')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down() {
    this.drop('ligacao_ramals')
  }
}

module.exports = LigacaoRamalSchema
