'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class EntidadeCativadorasSchema extends Schema {
  up () {
    this.table('entidade_cativadoras', (table) => {
      // alter table
	table.double("valor", 255, 2).nullable().alter();
	table.integer('cliente_id').unsigned().nullable().references('id').inTable('clientes').onUpdate('CASCADE').index().after('nome');
    })
  }

  down () {
    this.table('entidade_cativadoras', (table) => {
      // reverse alternations
    })
  }
}

module.exports = EntidadeCativadorasSchema
