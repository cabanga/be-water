'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RotaHeadersSchema extends Schema {
  up() {
    this.table('rota_headers', (table) => {
      // alter table

      table.integer('municipio_id').unsigned().references('id').inTable('municipios').after('data_fim')

    })
  }

  down() {
    this.table('rota_headers', (table) => {
      // reverse alternations
    })
  }
}

module.exports = RotaHeadersSchema
