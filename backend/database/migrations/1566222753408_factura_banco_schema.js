'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturaBancoSchema extends Schema {
  up () {
    this.create('factura_bancos', (table) => {
      table.increments()
      table.integer('factura_id').unsigned().index('factura_id_index').references('id').inTable('facturas');
      table.integer('banco_id').unsigned().index('banco_id_index').references('id').inTable('bancos');
      table.timestamps()
    })
  }

  down () {
    this.drop('factura_bancos')
  }
}

module.exports = FacturaBancoSchema
