'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClientesSchema extends Schema {
  up() {
    this.table('clientes', (table) => {
      // alter table

      table.integer('numero_cliente').after('numero_entidade')

    })
  }

  down() {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClientesSchema
