'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FacturasSchema extends Schema {
  up () {
    this.table('facturas', (table) => {
      table.string('tipo_factura').nullable().default('SERVICO')
    })
  }

  down () {
    this.table('facturas', (table) => {
      table.dropColumn('tipo_factura')
    })
  }
}

module.exports = FacturasSchema
