'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class LinhaReciboSchema extends Schema {
  up () {
    this.create('linha_recibos', (table) => {
      table.increments()
      table.integer('factura_id').unsigned().notNullable().references('id').inTable('facturas').onUpdate('CASCADE').index('factura_id_index');
      table.integer('recibo_id').unsigned().notNullable().references('id').inTable('recibos').onUpdate('CASCADE').index('recibo_id_index');
      table.integer('user_id').unsigned().notNullable().references('id').inTable('users').onUpdate('CASCADE').index('user_id_index'); 
      table.timestamps()
    })
  }

  down () {
    this.drop('linha_recibos')
  }
}

module.exports = LinhaReciboSchema
