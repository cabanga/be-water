'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class NaoVisitaSchema extends Schema {
  up () {
    this.create('nao_visitas', (table) => {
      table.increments()

      table.string('motivo')
      table.integer('rota_run_id').unsigned().references('id').inTable('rota_runs')
      table.integer('tipo_nao_visita_id').unsigned().references('id').inTable('tipo_nao_visitas')
      table.boolean('is_delected').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users')

      table.timestamps()
    })
  }

  down () {
    this.drop('nao_visitas')
  }
}

module.exports = NaoVisitaSchema
