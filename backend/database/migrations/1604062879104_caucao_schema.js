'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CaucaoSchema extends Schema {
  up () {
    this.create('caucaos', (table) => {
      table.increments()

      table.string('nome')
      table.string('slug')
      table.boolean('is_active').defaultTo(false)
      table.integer('user_id').unsigned().references('id').inTable('users')

     table.timestamps()
    })
  }

  down () {
    this.drop('caucaos')
  }
}

module.exports = CaucaoSchema
