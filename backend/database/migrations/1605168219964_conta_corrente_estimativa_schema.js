'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ContaCorrenteEstimativaSchema extends Schema {
  up () {
    this.table('conta_corrente_estimativas', (table) => {
      // alter table
      table.text("descricao", 255).nullable().after("estado");
    })
  }

  down () {
    this.table('conta_corrente_estimativas', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ContaCorrenteEstimativaSchema
