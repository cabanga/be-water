'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ClientesSchema extends Schema {
  up () {
    this.table('clientes', (table) => {
      // alter table
      table.string("nome", 1000).notNullable().alter();
    })
  }

  down () {
    this.table('clientes', (table) => {
      // reverse alternations
    })
  }
}

module.exports = ClientesSchema
