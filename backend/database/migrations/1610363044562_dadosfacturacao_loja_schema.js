'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DadosfacturacaoLojaSchema extends Schema {
  up () {
    this.create('dadosfacturacao_lojas', (table) => {
      table.increments()
      table.integer('dadosfacturacao_id').unsigned().references('id').inTable('dadosfacturacaos')
      table.integer('loja_id').unsigned().references('id').inTable('lojas')
      table.string('horario')
      table.integer('user_id').unsigned().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('dadosfacturacao_lojas')
  }
}

module.exports = DadosfacturacaoLojaSchema
