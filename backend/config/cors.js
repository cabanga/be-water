'use strict'
module.exports = {
  origin: function (currentOrigin) {
    return currentOrigin === 'http://localhost:4200' || '*';
  },
  methods: ['GET', 'PUT', 'PATCH', 'POST', 'DELETE'],
  headers: true,
  exposeHeaders: false,
  credentials: false,
  maxAge: 90
}


