module.exports = function (Route) {

    const ApiRoute = (registerCallback, prefix = '') => {
        return Route.group(registerCallback).prefix(`api/v1/${prefix}`)
    }

    ApiRoute(() => {
        Route.get('/',      'TipoClienteController.index')
        Route.post('/',     'TipoClienteController.store')
        Route.get('/:id',   'TipoClienteController.show')
        Route.patch('/:id', 'TipoClienteController.update')
        Route.put('/:id',   'TipoClienteController.delete')
    },'tipos_de_clientes').middleware(['auth'])

    ApiRoute(() => {
        Route.get('/',      'GeneroController.index')
        Route.post('/',     'GeneroController.store')
        Route.get('/:id',   'GeneroController.show')
        Route.patch('/:id', 'GeneroController.update')
        Route.put('/:id',   'GeneroController.delete')
    },'generos').middleware(['auth'])

    ApiRoute(() => {
        Route.get('/',      'TipoIdentidadeController.index')
        Route.post('/',     'TipoIdentidadeController.store')
        Route.get('/:id',   'TipoIdentidadeController.show')
        Route.patch('/:id', 'TipoIdentidadeController.update')
        Route.put('/:id',   'TipoIdentidadeController.delete')
    },'tipos_de_identificacao').middleware(['auth'])

    ApiRoute(() => {
        Route.get('/',      'DireccaoController.index')
        Route.post('/',     'DireccaoController.store')
        Route.get('/:id',   'DireccaoController.show')
        Route.patch('/:id', 'DireccaoController.update')
        Route.put('/:id',   'DireccaoController.delete')
    },'direccoes').middleware(['auth'])

    ApiRoute(() => {
        Route.get('/',      'GestorContaController.index')
        Route.post('/',     'GestorContaController.store')
        Route.get('/:id',   'GestorContaController.show')
        Route.patch('/:id', 'GestorContaController.update')
        Route.put('/:id',   'GestorContaController.delete')
    },'gestores_contas').middleware(['auth'])

}
