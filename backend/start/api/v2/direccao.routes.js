module.exports = (ApiRoute, Route) =>
// Protected routes
ApiRoute(() => {
  Route.get("/", "DireccaoController.index");
  Route.post("/", "DireccaoController.store").validator("cliente/createCliente")
  Route.get("/:id", "DireccaoController.show");
  Route.put("/:id", "DireccaoController.update");
  Route.delete("/:id", "DireccaoController.destroy");
  Route.get("/findAll", "DireccaoController.selectBox");

  }, "direccaos").middleware(["auth"]);
