 module.exports = (ApiRoute, Route) =>
  
    // Protected routes
    ApiRoute(() => {
       
        Route.get("/", "ServicoController.index"); 

        Route.post("/", "ServicoController.store").validator("Servico/createServico")

        Route.get("/:id", "ServicoController.show");

        Route.put("/:id", "ServicoController.update");

        Route.delete("/:id", "ServicoController.destroy");   
        
  },"servicos").middleware(["auth"]); 