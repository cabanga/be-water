module.exports = (ApiRoute, Route) =>
  // Protected routes
    ApiRoute(() => {
    
    Route.get("/rescisao/:id", "RescisaoContratoController.gerarRescisaoContrato");
        
  }, "report").middleware(["auth"]);