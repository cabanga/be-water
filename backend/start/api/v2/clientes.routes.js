module.exports = (ApiRoute, Route) =>
  // Protected routes
  ApiRoute(() => {
      Route.get("/", "ClienteController.index"); 
      Route.post("/", "ClienteController.store").validator("cliente/createCliente")
      Route.get("/:id", "ClienteController.show");
      Route.put("/:id", "ClienteController.update");
      Route.delete("/:id", "ClienteController.destroy"); 
      
      Route.post("/incumprimento", "ClienteController.incumprimentCliente");
      
  }, "clientes").middleware(["auth"]);
