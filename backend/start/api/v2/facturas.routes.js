
// Facturas Routes

module.exports = (ApiRoute, Route) =>
  // Protected routes
    ApiRoute(() => {

    Route.get("/", "FacturaController.index") //.validator("Factura/listFactura")

    Route.post("/", "FacturaController.store");

    Route.get("/:id", "FacturaController.show");

    Route.put("/anular/:id", "FacturaController.anular").validator("Factura/anularFatura");

    Route.get("/preview/:id", "FacturaController.gerarFactura");

  }, "facturas").middleware(["auth"]);
