module.exports = (ApiRoute, Route) =>
  // Protected routes
  ApiRoute(() => {
    Route.put("/change_password", "UserController.changePassword").validator("ResetPassword");

    Route.get("/", "UserController.index").middleware(["can:list-user"]);
    
    Route.get("/search", "UserController.search").middleware(["can:list-user"]);
    
    Route.get("/:id", "UserController.show").middleware(["can:list-user"]);
    
    Route.post("/", "UserController.store").validator("StoreUser").middleware(["can:create-user"]);

    Route.put("/:id", "UserController.update").validator("UpdateUser").middleware(["can:update-user"]);
    
    Route.put("/status/:id", "UserController.updateStatus").validator("StatusUser").middleware(["can:update-user"]);
      
  }, "users").middleware(["auth"]);
