module.exports = function (Route) {

  const ApiRoute = (registerCallback, prefix = '') => {
    return Route.group(registerCallback).prefix(`api/v1/${prefix}`)
  }

  ApiRoute(() => {
    Route.get('/getByRua/:rua_id', 'LocalConsumoController.getByRua')
    Route.get('/getByBairro/:bairro_id', 'LocalConsumoController.getByBairro')
    Route.get('/getByQuarterao/:quarteirao_id', 'LocalConsumoController.getByQuarterao')
    Route.get('/getByDistrito/:distrito_id', 'LocalConsumoController.getByDistrito')
    Route.get('/getByMunicipio/:municipio_id', 'LocalConsumoController.getByMunicipio')
    Route.get('/getByProvincia/:provincia_id', 'LocalConsumoController.getByProvincia')

  }, 'locais_consumo').middleware(['auth'])

}

