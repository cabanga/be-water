module.exports = function (Route) {

  const ApiRoute = (registerCallback, prefix = '') => {
    return Route.group(registerCallback).prefix(`api/v1/${prefix}`)
  }

  ApiRoute(() => {
    Route.get('/rotas',                          'FacturaController.rotas')
    Route.get('/charges',                        'FacturaController.charges')
    Route.post('/processar_pre_facturacao',      'FacturaController.processar_pre_facturacao')
    Route.post('/validar_pre_facturacao',        'FacturaController.validar_pre_facturacao')
  },'facturas').middleware(['auth'])

}

