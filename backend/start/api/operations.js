module.exports = function (Route) {

  const ApiRoute = (registerCallback, prefix = '') => {
    return Route.group(registerCallback).prefix(`api/v1/${prefix}`)
  }

  ApiRoute(() => {
    Route.get('/',      'ContratoController.index')
  },'contratos').middleware(['auth'])



  ApiRoute(() => {
    Route.get('/',      'ClienteIdentidadeController.index')
    Route.post('/',     'ClienteIdentidadeController.store')
    Route.get('clinete/:id',   'ClienteIdentidadeController.identidadesClinete')
    Route.get('/:id',   'ClienteIdentidadeController.show')
    Route.patch('/:id', 'ClienteIdentidadeController.update')
    Route.put('/:id',   'ClienteIdentidadeController.delete')
  },'clientes-identidades').middleware(['auth'])


  ApiRoute(() => {
    Route.get('/',      'ClassificacaoProdutoController.index')
    Route.post('/',     'ClassificacaoProdutoController.store')
    Route.get('/:id',   'ClassificacaoProdutoController.show')
    Route.patch('/:id', 'ClassificacaoProdutoController.update')
    Route.put('/:id',   'ClassificacaoProdutoController.delete')
  },'classificacao_produtos').middleware(['auth'])

  ApiRoute(() => {
    Route.get('/',                'PeriodicidadeRoteiroController.index')
    Route.post('/',               'PeriodicidadeRoteiroController.store')
    Route.get('/:id',             'PeriodicidadeRoteiroController.show')
    Route.get('/by_slug/:slug',   'PeriodicidadeRoteiroController.getBySlug')
    Route.patch('/:id',           'PeriodicidadeRoteiroController.update')
    Route.put('/:id',             'PeriodicidadeRoteiroController.delete')
  },'periodicidade_dos_roteiros').middleware(['auth'])

  ApiRoute(() => {
    Route.get('/',                'AgendamentoRoteiroController.index')
    Route.post('/',               'AgendamentoRoteiroController.store')
    Route.get('/:id',             'AgendamentoRoteiroController.show')
    Route.patch('/:id',           'AgendamentoRoteiroController.update')
    Route.put('/:id',             'AgendamentoRoteiroController.delete')
  },'agendamentos_dos_roteiros').middleware(['auth'])

  ApiRoute(() => {
    //Route.get('/nao_leituras',        'LeituraController.nao_leituras')
    Route.get('/contador/:id',        'LeituraController.contador')
    Route.get('/motivos_nao_leitura', 'LeituraController.motivos_nao_leitura')
    Route.post('/',                   'LeituraController.index')
    Route.patch('/:id',               'LeituraController.update')
  },'leituras').middleware(['auth'])

  ApiRoute(() => {
    Route.post('/',           'LeituraController.index')
    Route.post('/',           'LeituraController.create')
  },'roteiros').middleware(['auth'])

  ApiRoute(() => {
    Route.put('/actualizar-media-consumo',  'ContratoController.actualizar_media_consumo')
  },'contartos').middleware(['auth'])


}
