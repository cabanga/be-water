'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {

  return {
    greeting: 'UNIG'
  }
})
require("./api/v2")(Route);
require(`./api/crm`)(Route)
require(`./api/facturas`)(Route)
require(`./api/operations`)(Route)
require(`./api/technical_objects`)(Route)


Route.post('/leituras/nao_leituras', 'LeituraController.nao_leituras');



Route.post("/factura/ict", "FacturaUtilitieController.storeFacturacaoInteConexao");

Route.post('/user/authenticate', 'AuthController.authenticate');
Route.get('/user/getuser/:id', 'AuthController.show')
Route.get('/user/perfil/:id', 'UserController.perfil')
Route.get('/user/selectBoxLeitoresWithRotasHeaders', 'UserController.selectBoxLeitoresWithRotasHeaders')

Route.post('/email/send', 'SendMailController.store');

Route.get("/user/selectBox/:id", "UserController.selectBox");
Route.get("/user/operador", "UserController.selectBoxOperadores");
Route.get("/factura/arquivo", "FacturaController.criarArquivoSaft");


Route.post("/privatekey", "FacturaController.privateKey");


Route.post("/stream", "BillRunHeaderController.stream");

Route.post("/saftAo/getstream/:id", "SaftController.getStream");

Route.group(() => {

  Route.post("/charge/listar", "ChargeController.index");
  Route.post("/charge/update", "ChargeController.update");

  Route.post("/saftAo", "SaftController.criarArquivoSaft");
  Route.post("/safts", "SaftController.index");


  Route.post('/user/update/:id', 'AuthController.update');

  Route.post("/password", "AuthController.resetPassword")//.middleware(['can:reset_password']);
  Route.post("/redefinir-password/", "AuthController.changePassword")//.middleware(['can:redefinir_password']);
  Route.post("/isAlterPassword/", "AuthController.isAlterPassword");//.middleware(['can:redefinir_password']);

  Route.post("/user/register", "UserController.store");


  Route.post('/artigo/teste/pdf', 'ArtigoController.pdfTeste');

  Route.post('/fornecedor/register', 'FornecedorController.store');
  Route.post('/fornecedor/listar', 'FornecedorController.index');
  Route.post('/fornecedor/update/:id', 'FornecedorController.update');
  Route.get('/fornecedor/selectFornecedores', 'FornecedorController.selectFornecedores');

  //#region artigos
  Route.post('/artigo/create', 'ArtigoController.store');
  Route.post('/artigo/listar', 'ArtigoController.index')
  Route.get('/produtos/listar', 'ArtigoController.get_all')
  Route.post('/artigo/pesquisar', 'ArtigoController.show');
  Route.post('/artigo/update/:id', 'ArtigoController.update');
  Route.get('/artigo/selectProdutos', 'ArtigoController.selectProdutos');
  Route.post('/artigo/search', 'ArtigoController.searchArtigo');
  //#endregion


  //#region clientes
  Route.post('cliente/listagem', 'ClienteController.listagem');
  Route.get("cliente/getClienteById/:id", "ClienteController.getClienteById");
  Route.get("cliente/getClienteByConta/:id", "ClienteController.getClienteByConta");

  Route.post('/cliente/register', 'ClienteController.store');
  Route.post('/cliente/pesquisar', 'ClienteController.show');

  Route.post('/cliente/update/:id', 'ClienteController.update');

  Route.post("/cliente/search-cliente", "ClienteController.searchCliente");
  Route.post("/cliente/searchClienteInFacturas", "ClienteController.searchClienteInFacturas");
  Route.post("/cliente/searchClienteTelefone", "ClienteController.searchClienteTelefone");
  Route.get('/cliente/search-cliente-agt/:id', 'ClienteController.serviceAGT');
  Route.post("/cliente/searchClienteFacturaEmail", "ClienteController.searchClienteFacturaEmail");
  Route.get("/cliente/clienteAll", "ClienteController.clienteAll");

  //TIPO CLIENTE
  Route.get("/tipo-cliente/listagem", "TipoClienteController.index");
  Route.get("/tipo-cliente/selectBox", "TipoClienteController.selectBox");

  //TIPO IDENTIDADE
  Route.get("tipo-identidade/listagem", "TipoIdentidadeController.index");
  Route.get("tipo-identidade/selectBox", "TipoIdentidadeController.selectBox");

  //GESTOR CONTA
  Route.get("gestor-conta/listagem", "GestorContaController.index");
  Route.get("gestor-conta/selectBox", "GestorContaController.selectBox");




  //Busca Cliente para Mudança de Titularidade ou Conta
  Route.post("/search/cliente/:id", "ClienteController.getCliente");
  Route.post("/search/clienteContas/:id", "ContaController.getClienteContasSearch");

  //Unificar Clientes
  Route.post("/unificar/clientes", "UnificarClienteController.unificarCliente");
  //#region empresa
  Route.post('/empresa/listar', 'EmpresaController.index');
  Route.post('/empresa/update/:id', 'EmpresaController.update');
  Route.post('/empresa/upload/:id', 'EmpresaController.upload');
  Route.post('/empresa/register', 'EmpresaController.store');
  Route.get('/empresa/select-option', 'EmpresaController.selectOptionEmpresas');
  Route.get('/empresa/getEmpresa', 'EmpresaController.getEmpresa');
  Route.get('/empresa/empresa-user', 'EmpresaController.userEmpresa');
  //#endregion

  //#region armazém
  Route.post('/armazem/listar', 'ArmazenController.index');
  Route.post('/armazem/register', 'ArmazenController.create');
  Route.post('/armazem/update/:id', 'ArmazenController.update');
  Route.post('/armazem/selectBox', 'ArmazenController.selectBox');
  Route.post('/armazem/destino/selectBox', 'ArmazenController.selectBoxArmazemDestino');
  //#endregion

  //#region stock movimento
  Route.post('/stockMovimento/nova_entrada', 'StkMovimentoController.store');
  Route.post('/stockMovimento/listar/:artigo_id', 'StkMovimentoController.index');
  Route.post("/stock/existencia/armazem/qtd_produto", "StkMovimentoController.existenciaStockQtd");
  Route.post("/stock_movimento/armazem/existencia", "StkMovimentoController.existenciaStock");
  Route.post("/stock_movimento/nova_saida", "StkMovimentoController.saida");
  Route.post('/stockMovimento/transferencia', 'StkMovimentoController.transferencia');
  //#endregion

  //#region stock categoria produto
  Route.post('/categoria-produto/listar', 'StkCategoriaProdutoController.index');
  Route.post('/categoria-produto/register', 'StkCategoriaProdutoController.store');
  Route.post('/categoria-produto/update/:id', 'StkCategoriaProdutoController.update');
  Route.get('/stk-categoria/select-option', 'StkCategoriaProdutoController.selectOption');
  //#endregion

  //#region stock tipo movimento
  Route.post('/tipo-movimento/listar', 'StkTipoMovimentoController.index');
  Route.post('/tipo-movimento/register', 'StkTipoMovimentoController.store');
  Route.post('/tipo-movimento/update/:id', 'StkTipoMovimentoController.update');
  //#endregion

  //#region stock produtos
  Route.post('/stk-produtos/listar', 'StkProdutoController.index');
  Route.post('/stk-produtos/register', 'StkProdutoController.store');
  Route.post('/stk-produtos/update/:id', 'StkProdutoController.update');
  Route.post('/artigos/prod/selectBox', 'StkProdutoController.selectBox');
  Route.post('/material/by/armazem', 'StkProdutoController.selectBoxMaterialByArmazem');
  //#endregion

  Route.get("/armazem/all", "ArmazenController.getAll");
  Route.get("/armazem/selectBox", "ArmazenController.selectBox");

  //#region tarefas
  Route.get("/tarefa/listar", "TarefaController.index");
  Route.post("/tarefa/registar", "TarefaController.store");
  //#endregion


  //#region compra
  Route.post('/compra/listar', 'CompraController.index');
  Route.post('/compra/update/:id', 'CompraController.update');
  Route.post('/compra/register', 'CompraController.store');
  //#endregion


  // SendMail



  //#region users
  Route.post('/user/listar', 'AuthController.index');
  //#endregion

  //#region impostos
  Route.post('/imposto/listar', 'ImpostoController.index');
  Route.post('/imposto/create', 'ImpostoController.store');
  Route.post('/imposto/update/:id', 'ImpostoController.update');
  Route.post('/imposto/getall', 'ImpostoController.getAll');
  //#endregion

  //#region regra impostos
  Route.post('/imposto/regras/listar', 'RegraImpostoController.index');
  Route.post('/imposto/create', 'RegraImpostoController.store');
  //#endregion


  //#region regra pagamentos
  Route.post('/tipopagamento/listar', 'TipoPagamentoController.index');
  Route.post('/tipopagamento/create', 'TipoPagamentoController.store');
  //#endregion

  //#region formas Pagamentos
  Route.post('/formaPagamento/register', 'FormaPagamentoController.store');
  Route.post('/formaPagamento/listar', 'FormaPagamentoController.index');
  Route.post('/formaPagamento/update/:id', 'FormaPagamentoController.update');
  Route.get('/formaPagamento/formas', 'FormaPagamentoController.formas');
  //#endregion

  //#region recibo
  Route.post('/recibo/gerarRecibo', 'ReciboController.store');
  Route.get('/recibos/printerRecibo/:id', 'ReciboController.printerRecibo');
  //#endregion

  //#region facturas
  Route.post('/factura/create', 'FacturaController.store');
  Route.post('/factura/facturarServicosContrato', 'FacturaController.facturarServicosContrato');

  Route.post('/factura/facturarServicosContrato', 'FacturaController.facturarServicosContrato');
  Route.post('/factura/list', 'FacturaController.index');
  Route.post('/orcamento/list', 'FacturaController.listaOrcamento');
  Route.post('/factura/notpay', 'FacturaController.getFacturasNaoPagas');
  Route.post('/factura/arquivo', 'FacturaController.criarArquivo');
  Route.post('/factura/notacredito/artigos', 'FacturaController.getFacturaNotaCrediro');
  Route.post('/factura/notacredito/linhas', 'FacturaController.getLinhasFacturaNotaCrediro');

  Route.post('/factura/anular/:id', 'FacturaController.anular').validator("/factura/anularFatura");
  Route.post('/factura/gerarFactura/:id', 'FacturaController.gerarFactura');
  Route.post("/factura/findFactFromNc/:id", "FacturaController.findFactFromNc");

  Route.post("/factura/gerar-saft-xml", "FacturaController.saftXMLDocument");
  Route.get("/factura/dashboard", "FacturaController.dashboardFacturacao");
  //#endregion


  //Start Rotas para Contrato
  Route.post("/contrato/listagem", "ContratoController.listagem");
  Route.get("/contrato/getContratoById/:id", "ContratoController.getContratoById");
  Route.get("/contrato/dashboard", "ContratoController.dashboardEstadoContrato");
  Route.post("/contrato/create", "ContratoController.store");
  Route.post("/contrato/denunciar-rescindir/:id", "ContratoController.rescindirContrato");
  Route.post("/conta/contratoByConta/:id", "ContratoController.contratoByConta");
  Route.post("/contratoByLocalConsumo/:id", "ContratoController.contratoByLocalConsumo");

  Route.get("/contrato/imprimir/:id", "ContratoController.imprimirPDFContrato");
  Route.get("/contrato/:id", "ContratoController.show");

  //End Rotas para Contrato

  //Rotas para pedidos
  Route.post("/pedido/register", "PedidoController.store");
  Route.get("/pedido/listar/:id", "PedidoController.index");
  Route.post('/pedido/pdf/instalacao/:id', 'PedidoController.gerarPdfInstalacao');
  Route.post('/pedido/listagem', 'PedidoController.listagem');
  Route.post("/pedido/detalheLocalInstalacao/:id", "PedidoController.detalheLocalInstalacao");
  Route.post("/conta/pedidosByConta/:id", "PedidoController.pedidoByConta");
  Route.post('/pedido/update_test', 'PedidoController.updatePedidoCliente');

  //

  //Bill Run header
  Route.post('/billRunHeader/listagem', 'BillRunHeaderController.listagem');
  Route.get("/enviar/factura/email/:id", "BillRunHeaderController.findFacturasToSend");
  Route.get("/enviar/factura/email/one/:id", "BillRunHeaderController.findOneFacturasToSend");
  Route.post("/save/factura/base64", "BillRunHeaderController.downloadPDF");

  //#region documentos
  Route.post('/documento/listar', 'DocumentoController.index');
  Route.post('/documento/create', 'DocumentoController.store');
  Route.post('/documento/show', 'DocumentoController.show');
  Route.post("/documento/update/:id", "DocumentoController.update");
  //#endregion

  //#region tarifários
  Route.post('/planoPreco/show', 'PlanoPrecoController.show');
  Route.post('/tarifario/listagem', 'TarifarioController.listagem');
  Route.post('/tarifario/create', 'TarifarioController.store');
  Route.post('/tarifario/update/:id', 'TarifarioController.update');
  Route.get("/planoPreco/selectBox", "PlanoPrecoController.planoPreco");
  Route.get("/tarifario/selectBox", "TarifarioController.selectBox");

  Route.get("/tarifario/planoPrecoTecnologiaSelectBox/:t", "TarifarioController.planoPrecoTecnologiaSelectBox");

  Route.get("/tecnologia/nome/:id", "TecnologiaController.selectTecnologiaNome");

  Route.post("/tarifario/selectBoxTarifarioNovo", "TarifarioController.selectBoxTarifarioNovo");
  //#endregion


  //#region local-instalacao
  Route.post('/local-instalacao/listagem', 'LocalInstalacaoController.listagem');
  Route.post('/local-instalacao/create', 'LocalInstalacaoController.store');
  Route.post('/local-instalacao/update/:id', 'LocalInstalacaoController.update');
  Route.get('/local-instalacao/getLocaisMapa', 'LocalInstalacaoController.getLocaisMapa');
  Route.get('/local-instalacao/selectBoxByRua/:id', 'LocalInstalacaoController.selectBoxByRua');
  Route.post('/local-instalacao/getLocalInstalacaoByRuaAndMoradia', 'LocalInstalacaoController.getLocalInstalacaoByRuaAndMoradia');
  Route.post('/local-instalacao/getCurrentGeolocation/:id', 'LocalInstalacaoController.getCurrentGeolocation');


  Route.get('/local-instalacao/getLocalInstalacaoById/:id', 'LocalInstalacaoController.getLocalInstalacaoById')
  Route.post('/local-instalacao/getSelectLocalInstalacaosByRua', 'LocalInstalacaoController.getSelectLocalInstalacaosByRua');
  Route.post('/local-instalacao/getLocalInstalacaosByCILAndMoradia', 'LocalInstalacaoController.getLocalInstalacaosByCILAndMoradia');
  Route.post('/local-instalacao/getSelectResidenciaInLocalConsumoByRua', 'LocalInstalacaoController.getSelectResidenciaInLocalConsumoByRua');
  // NEW LIGAÇÃO RAMAL
  Route.post('/local-instalacao/ligacaoRamLocalInstalacaosByRua', 'LocalInstalacaoController.ligacaoRamLocalInstalacaosByRua');
  //#endregion


  //#region local-instalacao-historico
  Route.get('/local-instalacao-historico/getHistoricoById/:id', 'LocalInstalacaoHistoricoController.getHistoricoById');
  //#endregion


  //#region local-instalacao
  Route.post('/local-consumo/listagem', 'LocalConsumoController.listagem');
  Route.post('/local-consumo/create', 'LocalConsumoController.store');
  Route.post('/local-consumo/update/:id', 'LocalConsumoController.update');
  Route.get('/local-consumo/getLocaisMapa', 'LocalConsumoController.getLocaisMapa');
  Route.get('/local-consumo/selectBoxByRua/:id', 'LocalConsumoController.selectBoxByRua');
  Route.post('/local-consumo/getLocalInstalacaoByRuaAndMoradia', 'LocalConsumoController.getLocalInstalacaoByRuaAndMoradia');
  Route.post('/local-consumo/getCurrentGeolocation/:id', 'LocalInstalacaoController.getCurrentGeolocation');
  Route.post('/local-consumo/getLocalConsumoWithContrato', 'LocalConsumoController.getLocalConsumoWithContrato');

  Route.post("/local-consumo/localByConta/:id", "LocalConsumoController.localByConta")
  Route.post("/local-consumo/getLocaisConsumoByContrato/:id", "LocalConsumoController.getLocaisConsumoByContrato");

  //#region CONFIGURACAO
  Route.post('configuracao/listagem', 'ConfiguracaoController.listagem')
  Route.post('configuracao/create', 'ConfiguracaoController.store');
  Route.post('configuracao/auto-create', 'ConfiguracaoController.autoStore');
  Route.post('configuracao/update/:id', 'ConfiguracaoController.update');
  Route.post('configuracao/getConfiguracaobySlug/:slug', 'ConfiguracaoController.getConfiguracaobySlug');
  Route.get('configuracao/getSelectBoxModulos', 'ConfiguracaoController.getSelectBoxModulos');
  //#endregion


  //#region CONFIGURACAO
  Route.post('contexto-configuracao/listagem', 'ContextoConfiguracaoController.listagem')
  Route.post('contexto-configuracao/create', 'ContextoConfiguracaoController.store');
  Route.post('contexto-configuracao/update/:id', 'ContextoConfiguracaoController.update');
  Route.get('contexto-configuracao/selectBoxContextoConfiguracaos', 'ContextoConfiguracaoController.selectBoxContextoConfiguracaos');
  Route.get('contexto-configuracao/getInformationSchema', 'ContextoConfiguracaoController.getInformationSchema');
  Route.get('contexto-configuracao/getTableDescription/:table', 'ContextoConfiguracaoController.getTableDescription');
  Route.post('contexto-configuracao/getDataContexto', 'ContextoConfiguracaoController.getDataContexto');
  Route.post('contexto-configuracao/getDataContextoById/:id', 'ContextoConfiguracaoController.getDataContextoById');



  //#endregion


  //#region RUA
  Route.post('/rua/listagem', 'RuaController.listagem');
  Route.post('/rua/create', 'RuaController.store');
  Route.post('/rua/update/:id', 'RuaController.update');
  Route.get('/rua/getRuaById/:id', 'RuaController.getRuaById');
  Route.get('/rua/getRuasByBairro/:id', 'RuaController.getRuasByBairro');
  Route.get('rua/getRuasByQuarteirao/:id', 'RuaController.getRuasByQuarteirao');
  Route.get('/rua/selectBoxByBairro/:id', 'RuaController.selectBoxByBairro');
  //#endregion


  //#region QUARTEIRAO
  Route.post('/quarteirao/listagem', 'QuarteiraoController.listagem')
  Route.post('/quarteirao/create', 'QuarteiraoController.store');
  Route.post('/quarteirao/update/:id', 'QuarteiraoController.update');
  Route.post('/quarteirao/getQuarteiraoById/:id', 'QuarteiraoController.getQuarteiraoById');
  Route.get('/quarteirao/getQuarteiraosByBairro/:id', 'QuarteiraoController.getQuarteiraosByBairro');
  Route.get('/quarteirao/selectBoxByBairro', 'QuarteiraoController.selectBoxByBairro');
  //#endregion */


  //#region BAIRRO
  Route.post('/bairro/listagem', 'BairroController.listagem');
  Route.post('/bairro/create', 'BairroController.store');
  Route.post('/bairro/update/:id', 'BairroController.update');
  Route.get('/bairro/getBairroById/:id', 'BairroController.getBairroById');
  Route.get('/bairro/getBairrosByMunicipio/:id', 'BairroController.getBairrosByMunicipio');
  Route.get('/bairro/selectBoxByMunicipio/:id', 'BairroController.selectBoxByMunicipio');
  //#endregion BAIRRO

  //#region DISTRITO
  Route.post('/distrito/listagem', 'DistritoController.listagem');
  Route.post('/distrito/create', 'DistritoController.store');
  Route.post('/distrito/update/:id', 'DistritoController.update');
  Route.get('/distrito/selectBox', 'DistritoController.selectBox');
  Route.get('/distrito/:id', 'DistritoController.show');
  Route.post('/distrito/getDistritoById/:id', 'DistritoController.getDistritoById');
  Route.get('/distrito/selectBoxByMunicipio/:id', 'DistritoController.selectBoxByMunicipio');
  Route.get('/distrito/getDistritosByMunicipio/:id', 'DistritoController.getDistritosByMunicipio');
  //#endregion


  //#region MUNICIPIO
  Route.post('/municipio/listagem', 'MunicipioController.listagem');
  Route.post('/municipio/create', 'MunicipioController.store');
  Route.post('/municipio/update/:id', 'MunicipioController.update');
  Route.get('/municipio/selectBox', 'MunicipioController.selectBox');
  Route.get('/municipio/getMunicipioById/:id', 'MunicipioController.getMunicipioById');
  Route.get('/municipio/selectBoxByProvincia/:id', 'MunicipioController.selectBoxByProvincia');
  Route.get('/municipio/getMunicipiosByProvincia/:id', 'MunicipioController.getMunicipiosByProvincia');
  //#endregion MUNICIPIO

  //#region PROVINCIA
  Route.post('/provincia/listagem', 'ProvinciaController.listagem');
  Route.post('/provincia/create', 'ProvinciaController.store');
  Route.post('/provincia/update/:id', 'ProvinciaController.update');
  Route.get('/provincia/selectBox', 'ProvinciaController.selectBox');
  Route.get('/municipios/selectBox', 'ProvinciaController.selectBoxMunicipios');
  //#endregion


  //#region ORIGEM
  Route.get('/origem/selectBox', 'OrigemController.selectBox');
  //#endregion

  //#region NAO LEITURA
  Route.post('/nao-leitura/listagem', 'NaoLeituraController.listagem');
  Route.post('/nao-leitura/create', 'NaoLeituraController.store');
  Route.post('/nao-leitura/update/:id', 'NaoLeituraController.update');
  Route.get('/nao-leitura/selectBox', 'NaoLeituraController.selectBox');
  //#endregion


  //#region TIPO NAO LEITURA
  Route.post('/tipo-nao-leitura/listagem', 'TipoNaoLeituraController.listagem');
  Route.post('/tipo-nao-leitura/create', 'TipoNaoLeituraController.store');
  Route.post('/tipo-nao-leitura/update/:id', 'TipoNaoLeituraController.update');
  Route.get('/tipo-nao-leitura/selectBox', 'TipoNaoLeituraController.selectBox');
  //#endregion


  //#region OCORRENCIA
  Route.post('/ocorrencia/listagem', 'OcorrenciaController.listagem');
  Route.post('/ocorrencia/create', 'OcorrenciaController.store');
  Route.post('/ocorrencia/update/:id', 'OcorrenciaController.update');
  Route.get('/ocorrencia/selectBox', 'OcorrenciaController.selectBox');
  //#endregion


  //#region TIPO OCORRENCIA
  Route.post('/tipo-ocorrencia/listagem', 'TipoOcorrenciaController.listagem');
  Route.post('/tipo-ocorrencia/create', 'TipoOcorrenciaController.store');
  Route.post('/tipo-ocorrencia/update/:id', 'TipoOcorrenciaController.update');
  Route.get('/tipo-ocorrencia/selectBox', 'TipoOcorrenciaController.selectBox');
  //#endregion


  //#region CAUCAO
  Route.post('/caucao/listagem', 'CaucaoController.listagem');
  Route.post('/caucao/create', 'CaucaoController.store');
  Route.post('/caucao/update/:id', 'CaucaoController.update');
  Route.get('/caucao/getCaucaoById/:id', 'CaucaoController.getCaucaoById');
  Route.get('/caucao/selectBox', 'CaucaoController.selectBox');

  //#endregion


  //#region rota-run
  Route.post('/leitura/listagem', 'LeituraController.listagem');
  Route.post('/leitura/create', 'LeituraController.store');
  Route.post('/leitura/update/:id', 'LeituraController.update');
  //#endregion

  //#region rota-run
  Route.post('/rota-run/listagem', 'RotaRunController.listagem');
  Route.post('/rota-run/create', 'RotaRunController.store');
  Route.post('/rota-run/update/:id', 'RotaRunController.update');

  Route.get("/rota-run/getRotasRunByRotaHeader/:id", "RotaRunController.getRotasRunByRotaHeader");
  Route.get("/rota-run/getRotasRunPendentesByRotaHeader/:id", "RotaRunController.getRotasRunPendentesByRotaHeader");
  Route.get("/rota-run/selectBoxRotasRunPendentesByRotaReader/:id", "RotaRunController.selectBoxRotasRunPendentesByRotaReader");
  Route.post('/rota-run/show', 'RotaRunController.show');
  //#endregion


  //#region contagens
  Route.post('/contador/show', 'ContadoreController.show');
  Route.post('/contador/getContadoresByCliente/:id', 'ContadoreController.getContadoresByCliente');
  Route.post('/user/selectBoxAllUsers', 'AuthController.selectBoxAllUsers');
  //#endregion



  //#region series
  Route.post('/serie/listar', 'SerieController.index');
  Route.post('/serie/create', 'SerieController.store');
  Route.post('/serie/listagem', 'SerieController.listagem');
  Route.post("/serie/update/:id", "SerieController.update");
  Route.get("/serie/selectBoxSerieOrcamento", "SerieController.selectBoxSerieOrcamento");
  Route.post("/serie/selectBoxSeries", "SerieController.selectBoxSeries");
  Route.post("/serie/searchSerie", "SerieController.searchSerie");
  //#endregion





  //#region stock moviemnto
  Route.post('/stockMovimento/listar', 'StockMovimentoController.index');
  //#endregion


  //#region conta corrente
  Route.get("/contaCorrente/contas/:id", "ContaCorrenteController.contas");
  Route.get("/contaCorrente/report/:id", "ContaCorrenteController.contaConrrenteReport");
  Route.post("/contaCorrente/contas", "ContaCorrenteController.contas");
  Route.post("/contaCorrente/report", "ContaCorrenteController.contaConrrenteReport");
  //#endregion




  //#region modulos
  Route.post('/modulo/listar', 'ModuleController.index');
  Route.post('/modulo/registar', 'ModuleController.store');
  Route.post('/modulo/update/:id', 'ModuleController.update');
  Route.get('/modulo/select-option', 'ModuleController.selectOptionModulo');

  Route.get('/modulo/moduloPermissions', 'ModuleController.moduloPermissions');
  //#endregion


  //#region roles
  Route.post('/role/listar', 'RoleController.index');
  Route.post('/role/registar', 'RoleController.store');
  Route.post('/role/update/:id', 'RoleController.update');
  Route.get('/role/select-option', 'RoleController.selectOptionRole');
  Route.post('/role/adicionarPermissionRole', 'RoleController.adicionarPermissionRole');

  Route.get('/role/getAllPermissionsOfRole/:id', 'PermissionController.getAllPermissionsOfRole');

  Route.post('/roles/reportDiario', 'RoleController.roleReportDiario');

  Route.post('/roles/reportDiarioAuto', 'RoleController.roleReportDiarioAuto');

  //Route.post("/enviar/reportDiario/email/:id", "UserController.usersByRole");
  Route.post("/get/RoleSlug/:id", "RoleController.getRoleSlug");

  Route.post("/save/reportDiario/base64", "BillRunHeaderController.saveReportDiarioPDF");
  //#endregion



  //#region permissionss
  Route.post('/permission/listar', 'PermissionController.index');
  Route.post('/permission/registar', 'PermissionController.store');
  Route.post('/permission/update/:id', 'PermissionController.update');
  Route.get('/permission/show/:id', 'PermissionController.show');
  //#endregion


  //#region inventario
  Route.post('/inventario/register', 'InventarioController.store');
  Route.post('/inventario/listar', 'InventarioController.index');
  //#endregion


  //#region moeda
  Route.post("/moeda/register", "MoedaController.store");
  Route.get("/moeda/listar", "MoedaController.index");
  Route.post("/moeda/moedas", "MoedaController.moedas");
  Route.get("/moeda/moeda", "MoedaController.moeda");
  Route.post("/moeda/update/:id", "MoedaController.update");
  // cambio
  Route.post("/moeda/cambio/register", "CambioController.store");
  Route.get("/moeda/cambio/listar/:id", "CambioController.index");
  //#endregion

  //Bill Run header
  Route.post('/billRunHeader/listagem', 'BillRunHeaderController.listagem');
  Route.get("/enviar/factura/email/:id", "BillRunHeaderController.findFacturasToSend");
  Route.get("/enviar/factura/email/one/:id", "BillRunHeaderController.findOneFacturasToSend");
  Route.post('/billRunHeader/meses', 'BillRunHeaderController.meses');

  //#region projectos
  Route.post("/projecto/register", "ProjectoController.store");
  Route.post("/projecto/listar", "ProjectoController.index");
  Route.post("/projecto/update/:id", "ProjectoController.update");
  Route.post("/projecto/eliminar/:id", "ProjectoController.destroy");

  Route.post("/projecto/serie/adicionar", "ProjectoController.adicionarSerieProjecto");
  Route.post("/projecto/serie/listar", "ProjectoController.serieProjecto");
  //#endregion

  Route.get("direccaos/findAll", "DireccaoController.selectBox");
  //#region banco
  Route.post('/banco/create', 'BancoController.store');
  Route.get("/banco/listar", "BancoController.index");
  Route.post('/banco/update/:id', 'BancoController.update');

  Route.get('/dashboard/listar', 'DashboardController.index');
  //#endregion


  Route.get("/planoPreco/listar", "PlanoPrecoController.index");
  Route.get("/planoPreco/planos", "PlanoPrecoController.planoPreco");
  Route.post("/planoPreco/register", "PlanoPrecoController.store");
  Route.post("/planoPreco/update/:id", "PlanoPrecoController.update");
  //Tarifários
  Route.get("/tarifario/tarifas/:id", "TarifarioController.tarifarios");
  Route.get("/tarifario/tecnologia/:id", "TarifarioController.tecnologias");


  //Serviços
  Route.post("/tarifario/servico/register", "ServicoController.store");
  Route.post("/pedido/servico/conta", "ServicoController.servicoPedidoCreate");
  Route.get("/tarifario/servico/listar", "ServicoController.index");
  Route.post("/servicos/conta/cliente/:id", "ServicoController.servicosClientes");
  Route.post('/servico/contrato/:id', 'ServicoController.gerarContracto');
  Route.post('/servico/storeServicoPospago', 'ServicoController.storeServicoPospago');


  //LOCAL CONSUMO
  Route.post("/pedido/finalizar", "LocalConsumoController.store");
  Route.post("pedido/update/", "PedidoController.update");

  //END LOCAL SONSUMO

  Route.post("/pedido/anular", "PedidoController.anular");


  Route.post('/servico/update/estado/:id', 'ServicoController.updatEstado');
  Route.post('/mudanca/conta/servico', 'ServicoController.mudancaContaServico');

  Route.get("/getServico/:id", "ServicoController.getServico");
  //#region Logs Carregamentos
  Route.get("/servicos/ver/carregamentos/:id", "LogsCarregamentoController.getCarregamentos");

  Route.post("/servico/mudarTarifarioServico", "ServicoController.mudarTarifarioServico");
  //#endregion

  //#region Route.get("/servico/chaveServicos", "ChaveServicoController.selectBox");
  Route.post("/servico/chaveServicos", "WimaxController.validate");
  //#endregion

  //#region CDMA Equipamento
  Route.post("/cdmaEquip/validate/serie", "CdmaEquipamentoController.validateSerie");
  //#endregion


  //#region CLIENTE
  Route.get("/cliente/conta/listar/:id", "ContaController.index");
  Route.post("/cliente/conta/register", "ContaController.store");
  Route.post("/cliente/conta/update/:id", "ContaController.update");
  Route.get("/getContaEstado/:id", "ContaController.getContaEstado");
  Route.post('/conta/update/estado/:id', 'ContaController.updateEstado');
  //#endregion

  //#region Contactos clientes
  Route.get("/cliente/contacto/listar/:id", "PessoaContactoController.index");
  Route.post("/cliente/contacto/register", "PessoaContactoController.store");
  //#endregion

  //#region lojas
  Route.post("/loja/listar", "LojaController.index");
  Route.post("/loja/create", "LojaController.store");
  Route.post("/loja/update/:id", "LojaController.update");
  Route.get("/loja/selectBox", "LojaController.selectBox");
  Route.get("/loja/selectBox/:id", "LojaController.selectBoxById");
  Route.get("/lojas/selectBox/:id", "LojaController.selectBoxAgenciasById");
  Route.get('/loja_bancos/getbancos/:id', 'LojaController.getBancosAssociados');
  Route.post('/loja_bancos/addbancos/:id', 'LojaController.adicionarBancos');
  //#endregion



  //#region OCORRENCIA
  Route.post('/ocorrencia/listagem', 'OcorrenciaController.listagem');
  Route.post('/ocorrencia/create', 'OcorrenciaController.store');
  Route.post('/ocorrencia/update/:id', 'OcorrenciaController.update');
  Route.get('/ocorrencia/selectBox', 'OcorrenciaController.selectBox');
  //#endregion

  //#region TIPOS DE PEDIDOS
  Route.get("/tipoPedidos/selectBox", "TipoPedidoController.selectBox");
  Route.post('/tipos/pedidos/listagem', 'TipoPedidoController.listagem');
  Route.post('/tipos/pedido/create', 'TipoPedidoController.store');
  Route.post("/tipos/pedido/update/:id", "TipoPedidoController.update");
  //#endregion

  //#region
  Route.post("/loja/listarFormaPagamentoPorLoja", "LojaFormaPagamentoController.index");
  Route.post("/loja/forma/selectBox", "LojaFormaPagamentoController.selectBox");
  Route.post("/loja/storeFormaPagamentoLoja", "LojaFormaPagamentoController.store");
  Route.post("/loja/formaPagamentoLoja/active", "LojaFormaPagamentoController.update");
  //#endregion

  //TIPOLOGIA DE SERVICOS
  Route.post('/tipologia-servico/listagem', 'TipologiaServicoController.index');
  Route.post('/tipologia-servico/create', 'TipologiaServicoController.store');
  Route.post('/tipologia-servico/update/:id', 'TipologiaServicoController.update');
  Route.get('/tipologia-servico/selectBox', 'TipologiaServicoController.selectBox');

  /*
  // Reclamação
  Route.get("/tipoReclamacao/selectBox", "ReclamacaoController.tiposelectBox");
  Route.get("/reclamacao/listar/:id", "ReclamacaoController.index");
  Route.post("/reclamacao/register", "ReclamacaoController.store");
  Route.post('/reclamacao/listagem', 'ReclamacaoController.listagemReclamacoes');
  // Reclamação
  Route.post('/tipo_reclamacao/listagem', 'ReclamacaoController.listagemTipoReclamacao');
  Route.post('/tipo_reclamacao/create', 'ReclamacaoController.storeTipoReclamacao');
  Route.post('/tipo_reclamacao/update/:id', 'ReclamacaoController.updateTipoReclamacao');
  Route.get("/reclamacao/estados", "ReclamacaoController.estadosReclamacao");
  Route.get("/reclamacao/estado/selectBox/:id", "ReclamacaoController.selectBox");
  Route.post('/reclamacao/update/estado/:id', 'ReclamacaoController.updatEstado');
*/
  Route.get("/historico/reclamacao/:id", "LogEstadoReclamacaoController.index");

  // Prioridade
  Route.get("/prioridade/selectBox", "PrioridadeController.selectBox");
  Route.post('/prioridade/listagem', 'PrioridadeController.listagem');
  Route.post('/prioridade/create', 'PrioridadeController.store');
  Route.post('/prioridade/update/:id', 'PrioridadeController.update');

  Route.post("/loja/addChefe", "LojaController.addChefeLoja");
  Route.get("/filial/selectBox", "FilialController.selectBox");
  Route.get("/filial/selectBoxFilialPorLojas/:id", "FilialController.selectBoxFilialPorLojas");
  Route.get("/filial/indicativo/phone/:id", "FilialController.selectPhoneIndicativo");

  //lojas
  Route.post("/loja/listar", "LojaController.index");
  Route.post("/loja/create", "LojaController.store");
  Route.post("/loja/update/:id", "LojaController.update");
  Route.get("/loja/selectBox", "LojaController.selectBox");
  Route.get("/loja/selectBoxAgenciasByTipologiaServico/:id", "LojaController.selectBoxAgenciasByTipologiaServico");

  //CMDA NÚMERO

  Route.get("/serie/loja/selectSerieLojaUserBox", "SerieController.selectSerieLojaUserBox");
  Route.get("/serie/recibos", "SerieController.selectSerieRecibosBox");
  Route.post("/recibo/consultarRecibo/:id", "ReciboController.consultarRecibo");
  Route.post("/recibo/listarRecibo", "ReciboController.index");
  Route.post("/factura/consultarFacturaPosPagoVoz/:id", "BillRunHeaderController.consultarFacturasPospagoVoz");
  Route.post("/recibo/anular", "ReciboController.reciboAnular");

  Route.get("/cdma/selectBox", "CdmaNumeroController.selectBox");

  Route.post("/cdma/search-cdma-numero", "CdmaNumeroController.searchCdmaNumero");

  // SIM CARD

  Route.post("/search-serie-sim", "SimCardController.searchSerieSim");

  // USER SEARCH

  Route.post("/search-utilizador", "UserController.searchUtilizador");


  //LTE

  Route.post("/lte-search/numero-serie", "LteCpeController.searchNumeroSerie");
  Route.post("/lte-search/telefone", "LteCpeController.searchTelefone");
  Route.post("/lte-search/fabricante", "LteCpeController.searchFabricante");
  Route.post("/lte-search/modelo", "LteCpeController.searchModelo");
  Route.post("/lte-search/tipo", "LteCpeController.searchTipo");

  Route.post('/LteCpe/create', 'LteCpeController.store');

  Route.post('/LteCpe/listagem', 'LteCpeController.listagem');

  Route.post("/lteTelefone/validate/telefone", "LteCpeController.validateTelefoneLTE");

  //CONTADOR
  Route.post("/contador/validate/serie", "ContadoreController.validateSerie");
  Route.post('/contadores/listagem', 'ContadoreController.listagem');
  Route.post('/contador/create', 'ContadoreController.store');
  Route.post('/contador/viewinfo/:id', 'ContadoreController.viewinfo');
  Route.post('/contador/update/:id', 'ContadoreController.update');
  Route.post('/contador-leitura/create/:id', 'ContadoreController.storeIntroducaoManual');
  Route.get('/contador-leitura/leitura/:id', 'ContadoreController.LeiturabyContador');
  Route.get("/getContador/:id", "ContadoreController.getContador");
  Route.post('/contador/update/estado/:id', 'ContadoreController.updateEstado');
  Route.get('/contador/update/getestados/:id', 'ContadoreController.getEstadoFilhosByEstadoPai');
  Route.get("/historico/estado/contador/:id", "LogsContadoreController.index");

  //ROTA HEADER

  Route.post('/rota-header/listagem', 'RotaHeaderController.listagem');
  Route.post('/rota-header/create', 'RotaHeaderController.store');
  Route.post('/rota-header/update/:id', 'RotaHeaderController.update');
  Route.get('/rota-header/selectBoxRotaHeaders', 'RotaHeaderController.selectBoxRotaHeaders');
  Route.get('/rota-header/getRotasHeaderByLeitor/:id', 'RotaHeaderController.getRotasHeaderByLeitor');
  Route.get('/rota-header/getRotasHeaderWithRunsByLeitor/:id', 'RotaHeaderController.getRotasHeaderWithRunsByLeitor');
  Route.get('/rota-header/:id', 'RotaHeaderController.show');
  Route.get('/rota-header/getRotasHeaderWithRuns', 'RotaHeaderController.getRotasHeaderWithRuns');


  //WIMAX DISPOSITIVOS
  Route.post('/wimax/listagem', 'WimaxController.listagem');
  Route.post('/wimax/create', 'WimaxController.store');
  Route.post('/wimax/update/:id', 'WimaxController.update');

  //CDMA DISPOSITIVOS
  Route.post('/cdma/equipamento/listagem', 'CdmaEquipamentoController.listagem');
  Route.post('/cdma/equipamento/create', 'CdmaEquipamentoController.store');
  Route.post('/cdma/equipamento/update/:id', 'CdmaEquipamentoController.update');


  Route.get("/provincia/selectBox", "ProvinciaController.selectBox");
  Route.get("/municipio/selectBox/:id", "MunicipioController.selectBox");

  Route.post("/caixa/abertura", "CaixaController.store").middleware(['can:abrir_caixa'])
  Route.post("/caixa/fecho", "CaixaController.update").middleware(['can:fechar_caixa'])
  Route.post("/caixa/deposito", "DepositoCaixaController.store").middleware(['can:fechar_caixa'])
  Route.post('/deposito/listagem', 'DepositoCaixaController.listagemDepositos');

  Route.post("/caixa/movimentoCaixa", "CaixaController.index").middleware(['can:movimento_caixa']);
  Route.get("/caixa/selectBox", "CaixaController.selectBox");
  Route.get("/caixa/selectBoxCaixasFechados/:data", "CaixaController.selectBoxCaixasFechados");
  Route.post("/caixa/totalVendas", "CaixaController.totalVendas");
  Route.get("/caixa/open", "CaixaController.getCaixaAberto");

  //relatorios de venda caixas
  Route.post("/diario/vendas", "CaixaController.getDiarioVendas");

  Route.get("/serie/loja/selectSerieLojaBox", "SerieController.selectSerieLojaBox");

  Route.get("/serie/loja/selectSeriesRecibosNotInLojasBox", "SerieController.selectSeriesRecibosNotInLojasBox");

  Route.post("/cliente/conta/ContaPosPagoCliente", "ContaController.ContaPosPagoCliente");

  Route.post("/cliente/parceria/create", "ClienteController.createParceriaCliente");

  Route.post("/cliente/parceria/listar", "ParceriaClienteController.index");

  Route.post("/factura/processarContas", "FacturaUtilitieController.processarContas");
  Route.post("/factura/posPago", "FacturaUtilitieController.store");
  Route.post("/factura/preFacturacao", "FacturaUtilitieController.preFacturacao");




  Route.post("/get/cliente/contas/:id", "ContaController.getClienteContas");
  Route.post("/get/cliente/contas/pedido/:id", "ContaController.getClienteContasPedidos");

  Route.get("/cliente/conta/selectBox/:id", "ContaController.selectBoxContasCliente");
  Route.get("/tarifario/servico/selectBoxServicosConta/:id", "ServicoController.selectBoxServicosConta");
  Route.get("/conta/contratos/:conta_id", "ContratoController.selectBoxContratosConta");

  Route.get("/servico/estado/selectBox", "EstadoServicoController.selectBox");

  //ESTADO SERVIÇO
  Route.post('/estado-servico/listagem', 'EstadoServicoController.Listagem');
  Route.post('/estado-servico/create', 'EstadoServicoController.AdicionarServico');
  Route.post('/estado-servico/update/:id', 'EstadoServicoController.EditarServico');

  //ESTADO CONTADOR
  Route.post('/estado-contador/listagem', 'EstadoContadoreController.listagem');
  Route.post('/estado-contador/create', 'EstadoContadoreController.store');
  Route.post('/estado-contador/update/:id', 'EstadoContadoreController.update');
  Route.get('/estado-contador/selectBox', 'EstadoContadoreController.selectBox');
  Route.post('/relacao-estado-contador/relacao/:id', 'EstadoContadoreController.AssociarEstados');
  Route.get('/estado-contador/update/getestados/:id', 'EstadoContadoreController.getEstadoFilhosByEstadoPai');

  //MARCA
  Route.post('/marca/listagem', 'MarcaController.index');
  Route.post('/marca/create', 'MarcaController.store');
  Route.post('/marca/update/:id', 'MarcaController.update');
  Route.get('/marca/selectBox', 'MarcaController.selectBox');
  Route.get('/selectBox/modelobymarca/:id', 'MarcaController.ModelosbyMarca');

  //NIVEL SENSIBILIDADE
  Route.post('/nivel-sensibilidade/listagem', 'NivelSensibilidadeController.index');
  Route.post('/nivel-sensibilidade/create', 'NivelSensibilidadeController.store');
  Route.post('/nivel-sensibilidade/update/:id', 'NivelSensibilidadeController.update');
  Route.get('/nivel-sensibilidade/selectBox', 'NivelSensibilidadeController.selectBox');

  //ABASTECIMENTO CIL
  Route.post('/abastecimento-cil/listagem', 'AbastecimentoCilController.listagem');
  Route.post('/abastecimento-cil/create', 'AbastecimentoCilController.store');
  Route.post('/abastecimento-cil/update/:id', 'AbastecimentoCilController.update');
  Route.get('/abastecimento-cil/selectBox', 'AbastecimentoCilController.selectBox');

  //TIPO MENSAGEM
  Route.post('/tipo-mensagem/listagem', 'TipoMensagemController.listagem');
  Route.post('/tipo-mensagem/create', 'TipoMensagemController.store');
  Route.post('/tipo-mensagem/update/:id', 'TipoMensagemController.update');
  Route.get('/tipo-mensagem/selectBox', 'TipoMensagemController.selectBox');

  //TIPO MENSAGEM
  Route.post('/campo-jardim/listagem', 'CampoJardimController.listagem');
  Route.post('/campo-jardim/create', 'CampoJardimController.store');
  Route.post('/campo-jardim/update/:id', 'CampoJardimController.update');
  Route.get('/campo-jardim/selectBox', 'CampoJardimController.selectBox');

  //MENSAGEM
  Route.post('/mensagem/listagem', 'MensagemController.listagem');
  Route.post('/mensagem/create', 'MensagemController.store');
  Route.post('/mensagem/update/:id', 'MensagemController.update');
  Route.post('/mensagem/getMensagemByTipoMensagem/:id', 'MensagemController.getMensagemByTipoMensagem');
  Route.get('/mensagem/selectBox', 'MensagemController.selectBox');


  //MOTIVOS DENUNCIA
  Route.post('/motivo-rescisao/listagem', 'MotivoRecisaoController.index');
  Route.post('/motivo-rescisao/create', 'MotivoRecisaoController.store');
  Route.post('/motivo-rescisao/update/:id', 'MotivoRecisaoController.update');
  Route.get('/motivo-rescisao/selectBox', 'MotivoRecisaoController.selectBox');
  Route.get('/motivo-rescisao/selectBoxEstado', 'MotivoRecisaoController.selectBoxEstado');

  //MESES MEDIA DE CONSUMO
  Route.post('/meses-consumo/listagem', 'MesesMediaConsumoController.index');
  Route.post('/meses-consumo/create', 'MesesMediaConsumoController.store');
  Route.post('/meses-consumo/update/:id', 'MesesMediaConsumoController.update');
  Route.get('/meses-consumo/selectBox', 'MesesMediaConsumoController.selectBox');

  //ESTADO CONTRATO
  Route.post('/estado-contrato/listagem', 'EstadoContratoController.index');
  Route.post('/estado-contrato/create', 'EstadoContratoController.store');
  Route.post('/estado-contrato/update/:id', 'EstadoContratoController.update');
  Route.get('/estado-contrato/selectBox', 'EstadoContratoController.selectBox');

  //OBJECTO DE CONTRATO
  //TIPO JURO
  Route.post('/tipo-juro/listagem', 'TipoJuroController.index');
  Route.post('/tipo-juro/create', 'TipoJuroController.store');
  Route.post('/tipo-juro/update/:id', 'TipoJuroController.update');
  Route.get('/tipo-juro/selectBox', 'TipoJuroController.selectBox');

  //ESTADO CAIXA CONTADOR
  Route.post('/estado-caixa-contador/listagem', 'EstadoCaixaContadorController.index');
  Route.post('/estado-caixa-contador/create', 'EstadoCaixaContadorController.store');
  Route.post('/estado-caixa-contador/update/:id', 'EstadoCaixaContadorController.update');
  Route.get('/estado-caixa-contador/selectBox', 'EstadoCaixaContadorController.selectBox');

  //OBJECTO CONTRATO
  Route.post('/objecto-contrato/listagem', 'ObjectoContratoController.index');
  Route.post('/objecto-contrato/create', 'ObjectoContratoController.store');
  Route.post('/objecto-contrato/update/:id', 'ObjectoContratoController.update');
  Route.get('/objecto-contrato/selectBox', 'ObjectoContratoController.selectBox');

  //MEDIA CONSUMO
  Route.post('/media-consumo/listagem', 'MediaConsumoController.index');
  Route.post('/media-consumo/create', 'MediaConsumoController.store');
  Route.post('/media-consumo/update/:id', 'MediaConsumoController.update');
  Route.get('/media-consumo/selectBox', 'MediaConsumoController.selectBox');

  //MOTIVO RECISAO
  Route.post('/motivo-recisao/listagem', 'MotivoRecisaoController.index');
  Route.post('/motivo-recisao/create', 'MotivoRecisaoController.store');
  Route.post('/motivo-recisao/update/:id', 'MotivoRecisaoController.update');
  Route.get('/motivo-recisao/selectBox', 'MotivoRecisaoController.selectBox');

  //RECISAO CONTRATO
  Route.post('/recisao-contrato/listagem', 'RecisaoContratoController.index');
  Route.post('/recisao-contrato/create', 'RecisaoContratoController.store');
  Route.post('/recisao-contrato/update/:id', 'RecisaoContratoController.update');
  Route.get('/recisao-contrato/selectBox', 'RecisaoContratoController.selectBox');

  //MORADA CORRESPONDECIA
  Route.post('/morada-correspondencia/listagem', 'MoradaCorrespondenciaController.index');
  Route.post('/morada-correspondencia/create', 'MoradaCorrespondenciaController.store');
  Route.post('/morada-correspondencia/update/:id', 'MoradaCorrespondenciaController.update');
  Route.get('/morada-correspondencia/selectBox', 'MoradaCorrespondenciaController.selectBox');

  //TIPO JUROS
  Route.post('/tipo-juro/listagem', 'TipoJuroController.index');
  Route.post('/tipo-juro/create', 'TipoJuroController.store');
  Route.post('/tipo-juro/update/:id', 'TipoJuroController.update');
  Route.get('/tipo-juro/selectBox', 'TipoJuroController.selectBox');


  //TIPOLOGIA DE CLIENTES
  Route.post('/genero/listagem', 'GeneroController.index');
  Route.post('/genero/create', 'GeneroController.store');
  Route.post('/genero/update/:id', 'GeneroController.update');
  Route.get('/genero/selectBox', 'GeneroController.selectBox');


  //TIPOLOGIA DE CLIENTES
  Route.post('/tipologia-cliente/listagem', 'TipologiaClienteController.index');
  Route.post('/tipologia-cliente/create', 'TipologiaClienteController.store');
  Route.post('/tipologia-cliente/update/:id', 'TipologiaClienteController.update');
  Route.get('/tipologia-cliente/selectBox', 'TipologiaClienteController.selectBox');
  Route.get('/tipologia-cliente/selectGroupBox', 'TipologiaClienteController.selectGroupBox');
  Route.post('/tipologia-cliente/associartipostipologia/:id', 'TipologiaClienteController.AssociarTiposTipologia');
  Route.get('/tipologia-cliente/update/getTipos/:id', 'TipologiaClienteController.getTiposbyTipologia');

  //TIPOS DE TIPOLOGIA
  Route.post('/tipos-tipologia/listagem', 'TiposTipologiaController.index');
  Route.post('/tipos-tipologia/create', 'TiposTipologiaController.store');
  Route.post('/tipos-tipologia/update/:id', 'TiposTipologiaController.update');
  Route.get('/tipos-tipologia/selectBox', 'TiposTipologiaController.selectBox');

  //TIPO DE PRODUTO
  Route.post('/tipo-produto/listagem', 'TipoProdutoController.index');
  Route.post('/tipo-produto/create', 'TipoProdutoController.store');
  Route.post('/tipo-produto/update/:id', 'TipoProdutoController.update');
  Route.get('/tipo-produto/selectBox', 'TipoProdutoController.selectBox');

  //ESTADO TARIFARIO
  Route.post('/estado-tarifario/listagem', 'EstadoTarifarioController.index');
  Route.post('/estado-tarifario/create', 'EstadoTarifarioController.store');
  Route.post('/estado-tarifario/update/:id', 'EstadoTarifarioController.update');
  Route.get('/estado-tarifario/selectBox', 'EstadoTarifarioController.selectBox');

  //CAUDAL
  Route.post('/caudal/listagem', 'CaudalController.index');
  Route.post('/caudal/create', 'CaudalController.store');
  Route.post('/caudal/update/:id', 'CaudalController.update');
  Route.get('/caudal/selectBox', 'CaudalController.selectBox');

  //CALIBRE
  Route.post('/calibre/listagem', 'CalibreController.index');
  Route.post('/calibre/create', 'CalibreController.store');
  Route.post('/calibre/update/:id', 'CalibreController.update');
  Route.get('/calibre/selectBox', 'CalibreController.selectBox');

  //MODELO
  Route.post('/modelo/listagem', 'ModeloController.index');
  Route.post('/modelo/create', 'ModeloController.store');
  Route.post('/modelo/update/:id', 'ModeloController.update');
  Route.get('/modelo/selectBox', 'ModeloController.selectBox');
  Route.get('/selectBox/modelobyid/:id', 'ModeloController.ModelobyId');

  //MEDICAO
  Route.post('/medicao/listagem', 'MedicaoController.index');
  Route.post('/medicao/create', 'MedicaoController.store');
  Route.post('/medicao/update/:id', 'MedicaoController.update');
  Route.get('/medicao/selectBox', 'MedicaoController.selectBox');

  //FABRICANTE
  Route.post('/fabricante/listagem', 'FabricanteController.index');
  Route.post('/fabricante/create', 'FabricanteController.store');
  Route.post('/fabricante/update/:id', 'FabricanteController.update');
  Route.get('/fabricante/selectBox', 'FabricanteController.selectBox');

  //CLASSE PRECISAO
  Route.post('/classe-precisao/listagem', 'ClassePrecisaoController.index');
  Route.post('/classe-precisao/create', 'ClassePrecisaoController.store');
  Route.post('/classe-precisao/update/:id', 'ClassePrecisaoController.update');
  Route.get('/classe-precisao/selectBox', 'ClassePrecisaoController.selectBox');

  //TIPO OBJECTO TÉCNICO
  Route.post('/tipo-objecto-tecnico/listagem', 'TipoObjectoController.index');
  Route.post('/tipo-objecto-tecnico/create', 'TipoObjectoController.store');
  Route.post('/tipo-objecto-tecnico/update/:id', 'TipoObjectoController.update');
  Route.get('/tipo-objecto-tecnico/selectBox', 'TipoObjectoController.selectBox');

  //Objecto Ligação Ramal
  Route.post('/objecto-ligacao-ramal/listagem', 'ObjectoLigacaoRamalController.index');
  Route.post('/objecto-ligacao-ramal/create', 'ObjectoLigacaoRamalController.store');
  Route.post('/objecto-ligacao-ramal/update/:id', 'ObjectoLigacaoRamalController.update');
  Route.post('/objecto-ligacao-ramal/selectBoxSearchPontoA', 'ObjectoLigacaoRamalController.selectBoxSearchPontoA');
  Route.post('/objecto-ligacao-ramal/selectBoxSearchPontoB', 'ObjectoLigacaoRamalController.selectBoxSearchPontoB');
  Route.get('/objecto-ligacao-ramal/selectBoxByTipoLigacao/:id', 'ObjectoLigacaoRamalController.selectBoxByTipoLigacao');


  // NEW ligação Ramal

  Route.post('/search/inicio/ligacao', 'ObjectoLigacaoRamalController.searchInicioLigacao');
  Route.post('/search/fim/ligacao', 'ObjectoLigacaoRamalController.searchFimLigacao');

  //END new ligação Ramal

  //Ligacao Ramal
  Route.post('/ligacao-ramal/listagem', 'LigacaoRamalController.index');
  Route.post('/ligacao-ramal/getLigacaoRamals', 'LigacaoRamalController.getLigacaoRamals');
  Route.post('/ligacao-ramal/create', 'LigacaoRamalController.store');
  Route.post('/ligacao-ramal/update/:id', 'LigacaoRamalController.update');
  Route.get('/ligacao-ramal/getLigacoesFinaisByIdLocalInstacao/:id', 'LigacaoRamalController.getLigacoesFinaisByIdLocalInstacao');
  Route.get('/ligacao-ramal/getLigacaoLocalInstalacaosById/:id', 'LigacaoRamalController.getLigacaoLocalInstalacaosById');


  //TIPO CONTADOR
  Route.post('/tipo-contador/listagem', 'TipoContadorController.index');
  Route.post('/tipo-contador/create', 'TipoContadorController.store');
  Route.post('/tipo-contador/update/:id', 'TipoContadorController.update');
  Route.get('/tipo-contador/selectBox', 'TipoContadorController.selectBox');

  //TIPO REGISTO
  Route.post('/tipo-registo/listagem', 'TipoRegistoController.index');
  Route.post('/tipo-registo/create', 'TipoRegistoController.store');
  Route.post('/tipo-registo/update/:id', 'TipoRegistoController.update');
  Route.get('/tipo-registo/selectBox', 'TipoRegistoController.selectBox');

  //CICLO FACTURAÇÃO
  Route.post('ciclo-facturacao/listagem', 'CicloFacturacaoController.index');
  Route.post('/ciclo-facturacao/create', 'CicloFacturacaoController.store');
  Route.post('/ciclo-facturacao/update/:id', 'CicloFacturacaoController.update');
  Route.post('/ciclo-facturacao/getMesByAno', 'CicloFacturacaoController.selectMesByAno');
  Route.get('/ciclo-facturacao/selectBoxAno', 'CicloFacturacaoController.selectBoxAno');
  Route.get('/ciclo-facturacao/selectBox', 'CicloFacturacaoController.selectBox');

  //ESTADO CICLO FACTURAÇÃO
  Route.post('estado-ciclo-facturacao/listagem', 'EstadoCicloFacturacaoController.index');
  Route.post('/estado-ciclo-facturacao/create', 'EstadoCicloFacturacaoController.store');
  Route.post('/estado-ciclo-facturacao/update/:id', 'EstadoCicloFacturacaoController.update');
  Route.get('/estado-ciclo-facturacao/selectBox', 'EstadoCicloFacturacaoController.selectBox');

  //TIPO ORDEM SERVICO
  Route.post('/tipo-ordem-servico/listagem', 'TipoOrdemServicoController.index');
  Route.post('/tipo-ordem-servico/create', 'TipoOrdemServicoController.store');
  Route.post('/tipo-ordem-servico/update/:id', 'TipoOrdemServicoController.update');
  Route.get('/tipo-ordem-servico/selectBox', 'TipoOrdemServicoController.selectBox');

  //ORDEM SERVICO
  Route.post('/estado-ordem-servico/listagem', 'EstadoOrdemServicoController.index');
  Route.post('/estado-ordem-servico/create', 'EstadoOrdemServicoController.store');
  Route.post('/estado-ordem-servico/update/:id', 'EstadoOrdemServicoController.update');
  Route.get('/estado-ordem-servico/selectBox', 'EstadoOrdemServicoController.selectBox');
  Route.get('/estado-ordem-servico/selectGroupBox', 'EstadoOrdemServicoController.selectGroupBox');
  Route.get('/estado-ordem-servico/getEstadoOrdemServicoById/:id', 'EstadoOrdemServicoController.getEstadoOrdemServicoById');

  //TIPO FACTURACAO
  Route.post('/tipo-facturacao/listagem', 'TipoFacturacaoController.index');
  Route.post('/tipo-facturacao/create', 'TipoFacturacaoController.store');
  Route.post('/tipo-facturacao/update/:id', 'TipoFacturacaoController.update');
  Route.get('/tipo-facturacao/selectBox', 'TipoFacturacaoController.selectBox');

  //TIPO LIGACAO
  Route.post('/tipo-ligacao/listagem', 'TipoLigacaoController.index');
  Route.post('/tipo-ligacao/create', 'TipoLigacaoController.store');
  Route.post('/tipo-ligacao/update/:id', 'TipoLigacaoController.update');
  Route.get('/tipo-ligacao/selectBox', 'TipoLigacaoController.selectBox');

  //TIPO ESTADO CONTA
  Route.post('/estado-conta/listagem', 'EstadoContaController.index');
  Route.post('/estado-conta/create', 'EstadoContaController.store');
  Route.post('/estado-conta/update/:id', 'EstadoContaController.update');
  Route.get('/estado-conta/selectBox', 'EstadoContaController.selectBox');

  //CONTADORES LEITURAS
  Route.post('/contador-leitura/listagem', 'ContadoresLeituraController.index');
  Route.post('/contador-leitura/create', 'ContadoresLeituraController.store');

  //TIPO RAMAL
  Route.post('/tipo-ramal/listagem', 'TipoRamalController.listagem');
  Route.post('/tipo-ramal/create', 'TipoRamalController.store');
  Route.post('/tipo-ramal/update/:id', 'TipoRamalController.update');
  Route.post('/tipo-ramal/selectBox', 'TipoRamalController.selectBox');

  //CLASSE TARIFARIO
  Route.post('/classe-tarifario/listagem', 'ClasseTarifarioController.listagem');
  Route.post('/classe-tarifario/getProdutoClasseTarifario', 'ClasseTarifarioController.getProdutoClasseTarifario');
  Route.post('/classe-tarifario/create', 'ClasseTarifarioController.store');
  Route.get('/classe-tarifario/selectBox', 'ClasseTarifarioController.selectBox');
  Route.get('/selectBox/classe-tarifarioBytarifario/:id', 'ClasseTarifarioController.classeTartifarioByTarifario');
  Route.get('selectBox/artigo-valorByProduto/:id', 'ArtigoController.ValorByProduto');

  Route.post('/produto-classe-tarifario/getServicosByClasseTarifario/:id', 'ProdutoClasseTarifarioController.getServicosByClasseTarifario');
  Route.post('/produto-classe-tarifario/getServicoById/:id', 'ProdutoClasseTarifarioController.getServicoById');

  //PRODUTO CLASSE TARIFARIO
  Route.post('/produto-classe-tarifario/listagem', 'ProdutoClasseTarifarioController.listagem');
  Route.post('/produto-classe-tarifario/create', 'ProdutoClasseTarifarioController.store');
  Route.post('/produto-classe-tarifario/update/:id', 'ProdutoClasseTarifarioController.update');
  Route.get('/produto-classe-tarifario/selectBoxT', 'ProdutoClasseTarifarioController.selectBoxTarifario');
  Route.get('/produto-classe-tarifario/selectBox', 'ProdutoClasseTarifarioController.selectBoxProduto');
  Route.get('/produto-classe-tarifario/selectBoxCT', 'ProdutoClasseTarifarioController.selectBoxClasseTarifario');
  Route.get("/produto-classe-tarifario/:id", "ProdutoClasseTarifarioController.getClassePrecisaobyTarifario");


  //RAMAL
  Route.post('/gestao-ramal/listagem', 'RamalController.listagem');
  Route.post('/gestao-ramal/create', 'RamalController.store');
  Route.post('/gestao-ramal/update/:id', 'RamalController.update');

  // TIPO CONTRATO
  Route.post('/tipo-contrato/listagem', 'TipoContratoController.listagem');
  Route.post('/tipo-contrato/create', 'TipoContratoController.store');
  Route.post('/tipo-contrato/update/:id', 'TipoContratoController.update');
  Route.get('/tipo-contrato/selectBox', 'TipoContratoController.selectBox');
  Route.post('/info-tipo-contrato/:id', 'TipoContratoController.infoTipoContrato');


  // TIPO MEDICAO
  Route.post('/tipo-medicao/listagem', 'TipoMedicaoController.listagem');
  Route.post('/tipo-medicao/create', 'TipoMedicaoController.store');
  Route.post('/tipo-medicao/update/:id', 'TipoMedicaoController.update');
  Route.get('/tipo-medicao/selectBox', 'TipoMedicaoController.selectBox');

  // TARIFARIO
  Route.post('/tarifarios/listagem', 'TarifarioController.Listagem');
  Route.post('/tarifarios/create', 'TarifarioController.storeTarifario');
  Route.post('/tarifarios/update/:id', 'TarifarioController.update');
  Route.get('/tarifarios/selectBox', 'TarifarioController.selectBox');

  // CONTAS BANCARIAS
  Route.post('/conta-bancaria/listagem', 'ContaBancariaController.index');
  Route.post('/conta-bancaria/create', 'ContaBancariaController.store');
  Route.post('/conta-bancaria/update/:id', 'ContaBancariaController.update');
  Route.get('/conta-bancaria/selectBox', 'ContaBancariaController.selectBox');

  //DADOS FACTURAÇÃO
  Route.post('/dados-facturacao/listagem', 'DadosfacturacaoController.index');
  Route.post('/dados-facturacao/create', 'DadosfacturacaoController.store');
  Route.post('/dados-facturacao/relacao/:id', 'DadosfacturacaoController.associarlojas');
  Route.post('/dados-facturacao/update/getlojas/:id', 'DadosfacturacaoController.getLojasAssociadas');
  Route.post('/dados-facturacao/update/getContasBancarias/:id', 'DadosfacturacaoController.getContasAssociadas');
  Route.post('/dados-facturacao/update/:id', 'DadosfacturacaoController.update');
  Route.get('/dados-facturacao/selectBox', 'DadosfacturacaoController.selectBox');

  Route.post("/adiantamento/create", "AdiantamentoController.store");

  Route.get("/selectBoxTarifarioTecnologiaInterconexao", "TarifarioController.selectBoxTarifarioTecnologiaInterconexao");

  Route.get("/pedido/estados", "PedidoController.estadosPedidos");

  Route.get("/pedido/totalDashBoard", "PedidoController.totalDashBoard");

  Route.get("/pedido/estadosFNRJ", "PedidoController.estadosPedidosFNRJ");
  Route.get("/pedido/estado/selectBox", "PedidoController.selectBox");

  Route.get("/estadoByPedido/:id", "PedidoController.estadoByPedido");

  Route.get("/estadosPedido/estadoNotAberto", "EstadoPedidoController.estadoNotAberto");

  Route.get("/estadoPedidoValue/:id", "EstadoPedidoController.estadoPedidoValue");

  Route.get("/rotaTipoPedido/:id", "TipoPedidoController.rotaTipoPedido");

  Route.post('/pedido/update/estado/:id', 'PedidoController.updatEstado');

  Route.post('/pedido/update/:id', 'PedidoController.updatePedido');

  Route.get("/historico/estado/pedido/:id", "LogEstadoPedidoController.index");

  Route.get("/factura/dashboard", "FacturaController.dashboardFacturacao");

  Route.post("/vendas/resumoRecebimentoPorCaixa", "CaixaController.reportResumoRecebimentoPorCaixa");

  Route.post("/vendas/resumoRecebimentoPorCaixa", "CaixaController.reportResumoRecebimentoPorCaixa");
  Route.post("/vendas/fechoCaixaResumoRecebimentoVendas", "CaixaController.fechoCaixaResumoRecebimentoVendas");

  //Tecnologias
  Route.post("/tecnologia/selectBox", "TecnologiaController.selectBox");
  Route.post('/tecnologias/listagem', 'TecnologiaController.listagem');
  Route.post('/tecnologias/prepago', 'TecnologiaController.TecnologiasPrepago');
  Route.post('/tecnologia/create', 'TecnologiaController.store');
  Route.post('/tecnologia/update/:id', 'TecnologiaController.update');

  //Numeração
  Route.post('/numeracao/listagem', 'NumeracaoController.index');
  Route.post('/numeracao/create', 'NumeracaoController.store');
  Route.get("/tecnologia/numeracao/selectBox", "TecnologiaController.selectPrePago");
  Route.post('/numeracao/update/:id', 'NumeracaoController.update');
  Route.post('/numeracao/disponibilizar/:id', 'NumeracaoController.disponibilizar');

  //Estado Reclamação
  Route.post('/estado/reclamacao/listagem', 'EstadoReclamacaoController.listagem');
  Route.post('/estado/reclamacao/create', 'EstadoReclamacaoController.store');
  Route.post('/estado/reclamacao/update/:id', 'EstadoReclamacaoController.update');

  //ROTAS DIRECCAO
  Route.post('/direccao/listagem', 'DireccaoController.listagem');
  Route.post('/direccao/create', 'DireccaoController.store');
  Route.post('/direccao/update/:id', 'DireccaoController.update');
  Route.get('/direccao/selectBox', 'DireccaoController.selectBox');

  //Estado Reclamação
  Route.post('/estado/pedidos/listagem', 'EstadoPedidoController.listagem');
  Route.post('/estado/pedidos/create', 'EstadoPedidoController.store');
  Route.post('/estado/pedidos/update/:id', 'EstadoPedidoController.update');

  // Unificação clientes
  Route.post("/unificar/getContas", "UnificarClienteController.client_a_Unificar");
  Route.post("/unificar/clienteUnificados", "UnificarClienteController.getClente_a_Unificar");
  Route.post("/unificar/getConta_Cliente_manter", "UnificarClienteController.conta_cliente_manter");
  Route.post("/unificar/getConta_Cliente_unificar", "UnificarClienteController.conta_cliente_unificar");
  Route.post("/unificar/delete_unificado", "UnificarClienteController.delete_unificar_cliente");
  Route.post("/unificar/update_unificado", "UnificarClienteController.upDateClienteUnificar");
  Route.post("/unificar/desfazer_unificado", "UnificarClienteController.desfazerUnificacao");
  Route.get("/unificar/masssa", "UnificarClienteController.unificar_em_massa");

  Route.post("/unificar/clientes", "UnificarClienteController.unificarCliente");

  Route.post("/referencia/validation", "ReferenciaBancariaController.validarReferenciaBancaria");

  Route.post("/referencia/validation", "ReferenciaBancariaController.validarReferenciaBancaria");


  Route.post("/entidade-cativadora/listar", "EntidadeCativadoraController.index");
  Route.post("/entidade-cativadora/registar", "EntidadeCativadoraController.store");

  Route.post("/entidade-cativadora/search-cliente", "EntidadeCativadoraController.searchClientes");

  Route.get("/entidade-cativadora/tipoEntidadeCativadoraSelectBox", "TipoEntidadeCativadoraController.tipoEntidadeCativadoraSelectBox");
  Route.post("/tipo-entidade-cativadora/registar", "TipoEntidadeCativadoraController.store");

  //RECURSOS DE REDE

  Route.get("/central/selectBox", "RecursosRedeController.centralListagem");
  Route.get("/armarioRecursoRede/selectBox", "RecursosRedeController.armarioListagem");

  Route.get("/caixaRecursoRede/selectBox", "RecursosRedeController.caixaRecursoRedeListagem");
  Route.get("/caboRecursoRede/selectBox", "RecursosRedeController.caboRecursoRedeListagem");
  Route.get("/parCabo/selectBox", "RecursosRedeController.parCaboListagem");
  Route.get("/armarioPrimario/selectBox", "RecursosRedeController.armarioPrimarioListagem");
  Route.get("/armarioSecundario/selectBox", "RecursosRedeController.armarioSecundarioListagem");
  Route.get("/parCaixa/selectBox", "RecursosRedeController.parCaixaListagem");

  Route.post('/central/listaDataTable', 'RecursosRedeController.centralDataTable');

  Route.post('/central/register', 'CentralRecursoRedeController.storeCentral');
  Route.post('/central/update/:id', 'CentralRecursoRedeController.updateCentral');
  Route.get("/select-armario/armarioByCentral/:id", "RecursosRedeController.armarioByCentral");
  Route.get("/select/idCaixaByCentral/:id", "RecursosRedeController.idCaixaByCentral");
  Route.get("/select/idCaboByCentral/:id", "RecursosRedeController.idCaboByCentral");
  Route.get("/select/parCaboByCentral/:id", "RecursosRedeController.parCaboByCentral");
  Route.get("/select/armarioPrimarioByCentral/:id", "RecursosRedeController.armarioPrimarioByCentral");
  Route.get("/select/armarioSecundarioByCentral/:id", "RecursosRedeController.armarioSecundarioByCentral");
  Route.get("/select/parCaixaByCentral/:id", "RecursosRedeController.parCaixaByCentral");
  Route.get("/select/parADSLByCentral/:id", "RecursosRedeController.parADSLByCentral");
  Route.post("/idArmario/central/register", "ArmarioRecursoRedeController.store");
  Route.post("/idCaixa/central/register", "CaixaRecursoRedeController.store");
  Route.post("/idCabo/central/register", "CaboRecursoRedeController.store");
  Route.post("/parCabo/central/register", "ParCaboController.store");
  Route.post("/armarioPrimario/central/register", "ArmarioPrimarioController.store");
  Route.post("/armarioSecundario/central/register", "ArmarioSecundarioController.store");
  Route.post("/parCaixa/central/register", "ParCaixaController.store");
  Route.post("/parADSL/central/register", "ParAdslController.store");

  // relatorios


  //#region Flat_Rate

  Route.post('/flate_rate/visualizar', "FlatRateServicoController.show");
  Route.post('/flate_rate/update', "FlatRateServicoController.update");
  //#endregion

}).middleware(['auth']);

Route.post("/reclamacao/register_async", "ReclamacaoController.store_portal");

Route.post("/relatorio/financeira/cobranca-global", "RelatorioController.relatorioFacturacaoRealizadaCobrancaGlobal");
Route.post("/relatorio/financeira/pagamento", "RelatorioController.relatorioFacturacaopagamentoGlobal");
Route.post("/relatorio/financeira/porgestor", "RelatorioController.relatorioFacturacaoporGestorGlobal");/*
Route.post("/relatorio/financeira/cobranca-global", "RelatorioController.relatorioFacturacaoRealizadaCobrancaGlobal"); */
Route.post("/relatorio/financeira/cobranca-global-pago", "RelatorioController.relatorioFacturacaoRealizadaCobrancaGlobalPago")
Route.post("/relatorio/financeira/loja", "RelatorioController.relatorioLoja");
Route.post("/relatorio/financeira/detalhada", "RelatorioController.relatorioFacturacaoRealizadaDetalhada");
Route.post("/relatorio/financeira/iva", "RelatorioController.relatorioIVA");
Route.post("/relatorio/financeira/servicoContratados", "RelatorioController.relatorioServicoContratados");
Route.post("/relatorio/clientes", "RelatorioController.relatorioClients");
Route.post("/relatorio-extraccao/clientes", "RelatorioController.relatorioExtraccaoClients");

Route.post("/adiantamento/factura/adiantamentoFactura", "AdiantamentoController.adiantamentoFactura");
Route.post("/adiantamento/factura/adiantamentoContaCliente", "ContaCorrenteController.adiantamentos");
Route.get("/direccao/selectBox", "ClienteController.direccaosSelectBox");


Route.get("/gestor/selectBox", "ClienteController.gestoresSelectBox");



Route.get('/tfa/setup', 'TfaController.setupGet');
Route.post('/tfa/setup', 'TfaController.setupPost');
Route.delete('/tfa/setup', 'TfaController.setupDelete');
Route.post('/tfa/verify', 'TfaController.setupVerify');

Route.post("/login", "TfaController.loginTFA");

Route.post("/transactions-facturacao", "FacturaController.transactionFacturacaoStore");



// Endpoints API

Route.get("api/cliente/getClienteById/:id", "ClienteController.getClienteById");


Route.get("api/ocorrencia/getOcorrenciasByRotaRun/:id", "OcorrenciaController.getOcorrenciasByRotaRun");


Route.post("api/leitura/listagem", "LeituraController.listagem");
Route.get("api/leitura/getLeiturasByLeitor/:id", "LeituraController.getLeiturasByLeitorAPI");
Route.get("api/leitura/getRotasRunPendentesByRotaHeader/:id", "RotaRunController.getRotasRunPendentesByRotaHeader");
Route.post("api/leitura/create", "LeituraController.storeAPI");
Route.post("api/leitura/array-create", "LeituraController.storeArrayAPI");


Route.get("api/rota-header/getRotasHeaderByLeitor/:id", "RotaHeaderController.getRotasHeaderByLeitor");
Route.get("api/rota-run/getRotasRunByRotHeader/:id", "RotaRunController.getRotasRunByRotaHeader");
// -----------------------------------------------------------



const speakeasy = require('speakeasy');
const QRCode = require('qrcode');
const commons = use('App/Models/Commons');



Route.post('/register', (request, response) => {
  console.log(`DEBUG: Received request to register user`);
  /*console.log(request._body);
      const result = request.body;

      if ((!result.uname && !result.upass) || (result.uname.trim() == "" || result.upass.trim() == "")) {
          return response.send({
              "status": 400,
              "message": "Username/ password is required"
          });
      }*/

  commons.userObject.uname = 'caniggia'// result.uname;
  commons.userObject.upass = '123'//result.upass;
  delete commons.userObject.tfa;

  return {
    "status": 200,
    "message": "User is successfully registered"
  };
});

Route.get('/rota-run/getRotas', 'RotaRunController.getRotas');
Route.get('/rotas/roteiros', 'RotaRunController.getRoteiros');
