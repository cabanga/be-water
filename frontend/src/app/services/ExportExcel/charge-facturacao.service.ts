import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as excelprop from "exceljs";
import * as Excel from "exceljs/dist/exceljs.min.js";

@Injectable({
  providedIn: 'root'
})
export class ChargeFacturacaoService {

  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  constructor() { }


  public ExcelExportFacturacaoCharge( Data:any,sName = 'Planilha_1',excelFileName){

    
    var footer = ["UNIG - AT"];
    var header = ["Pré-Facturacão"];
    
    var  keys =[
      { key: 'id',width: 17 },
      { key: 'cliente' ,width: 40, style: { font: { name: 'Calibri' } }},
      { key: 'direccao',width: 15 },
      { key: 'gestor_conta',width: 23 },
      { key: 'conta_id' ,width: 20},
      { key: 'chaveServico',width: 20},
      { key: 'invoiceText',width: 20},
      { key: 'valor',width: 20,style: { numFmt: '#,##0.00', }},
      { key: 'periodo',width: 16},
      { key: 'facturado',width: 14}
    ];
    var  Cols =['#','Cliente','Direcção','Gestor','Nº Conta','Serviço','InvoiceText','Valor Actual','Periodo','Estado']

      var workbook = new Excel.Workbook();
      workbook.creator = 'Web';
      workbook.lastModifiedBy ='Web';
      workbook.created = new Date();
      workbook.modified = new Date();
      workbook.addWorksheet(sName, { views: [{ state: 'frozen', ySplit: 6, xSplit: 3, activeCell: 'C2', showGridLines: true }] })
      var sheet = workbook.getWorksheet(1);

      sheet.addRow(header);
      sheet.getCell('A1').font = { family: 4,name: 'Calibri', size: 25, bold: true ,underline: true};
      sheet.addRow("");
      sheet.getRow(6).values = Cols;
      sheet.columns = keys;
      sheet.getRow(6).fill = {
        type: 'pattern',
        pattern:'solid',
        fgColor:{ argb:'cccccc' }
        }
      sheet.addRows(Data);
  
      sheet.eachRow({ includeEmpty: true }, function(row, rowNumber){
        row.eachCell(function(cell, colNumber){
         cell.font = {
           name: 'Arial',
           family: 2,
           bold: false,
           size: 10,
           
         };
         cell.alignment = {
           vertical: 'middle', horizontal: 'left'
         };
         if (rowNumber <= 6) {
           row.height = 20;
           cell.font = {
             bold: true
           };
          }
          if (rowNumber >= 6 ) {
            
          
            cell.alignment = {
              vertical: 'middle', horizontal: 'center'
            };
            if (rowNumber >= 10 ) {
             
            row.getCell(10).alignment = {
            vertical: 'middle', horizontal: 'right','color': {'argb': 'FFFF6600'}
          };
        }
           for (var i = 1; i < 11; i++) {
             if (rowNumber == 6 ) {
              cell.font = {
              color:{argb:'FFFFFF'},
              bold: true
              };
               row.height = 25;
               row.getCell(i).fill = {
                 type: 'pattern',
                 pattern:'solid',
                 fgColor:{argb:'0066CC'}
               };
             }
             row.getCell(i).border = {
             top: {style:'thin'},
             left: {style:'thin'},
             bottom: {style:'thin'},
             right: {style:'thin'}
           };
         }
        }
       });
      });
      sheet.addRow(footer);
      workbook.xlsx.writeBuffer().then(Data => {
        var blob = new Blob([Data], { type: this.blobType });
       

        var url = window.URL.createObjectURL(blob);
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.href = url;
        a.download = excelFileName;
        a.click();
      });
    

  }

}
