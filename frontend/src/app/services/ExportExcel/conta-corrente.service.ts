import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as excelprop from "exceljs";
import * as Excel from "exceljs/dist/exceljs.min.js";

@Injectable({
  providedIn: 'root'
})
export class ContaCorrenteService {

  blobType: string = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';

  constructor() { }


  public ExcelExportContaCorrente(Data: any, total, cliente, empresa, excelFileName) {

    //var footer = ["UNIG -[ BWATER]"];
    var header = 'CONTA CORRENTE - ' + cliente;
    var keys = [
      { key: 'contaNumero', width: 30 },
      { key: 'contaDescricao', width: 60, style: { font: { name: 'Calibri' } } },
      { key: 'factura_sigla', width: 30 },
      { key: 'estado', width: 23 },
      { key: 'documentoDesc', width: 16 },
      { key: 'data_format', width: 20, style: { numFmt: 'dd/mm/yyyy', } },
      { key: 'total', width: 20, style: { numFmt: '#,##0.00', } },
      { key: 'valor_aberto', width: 20, style: { numFmt: '#,##0.00', } }
    ];

    var keys_FR = [
      { key: 'factura_sigla', width: 30 },
      { key: 'documentoDesc', width: 30, style: { font: { name: 'Calibri' } } },
      { key: 'data_format', width: 30 },
      { key: 'total', width: 20, style: { numFmt: '#,##0.00', } },
    ];

    var keys_NC = [
      { key: 'factura_sigla', width: 30 },
      { key: 'numero_origem_factura', width: 30 },
      { key: 'documentoDesc', width: 30, style: { font: { name: 'Calibri' } } },
      { key: 'data_format', width: 30 },
      { key: 'total', width: 20, style: { numFmt: '#,##0.00', } },
    ];

    var keys_RC = [
      { key: 'recibo_sigla', width: 30 },
      { key: 'tipo', width: 30, style: { font: { name: 'Calibri' } } },
      { key: 'data_format', width: 30 },
      { key: 'total', width: 20, style: { numFmt: '#,##0.00', } },
    ];



    var Cols_FT = ['Nº Conta', 'Conta', 'Factura Número', 'Estado', 'Tipo', 'Data', 'Total', 'Valor Em Aberto']
    var Cols_FR = ['Factura Número', 'Tipo', 'Data', 'Total']
    var Cols_NC = ['Nota de Crédito Número', 'Factura Origem', 'Tipo', 'Data', 'Total']
    var Cols_RC = ['Recibo Número', 'Tipo', 'Data', 'Total']

    var workbook = new Excel.Workbook();
    workbook.creator = 'Web';
    workbook.lastModifiedBy = 'Web';
    workbook.created = new Date();
    workbook.modified = new Date();
    var imageId1 = workbook.addImage({
      base64: empresa.logotipo,
      extension: 'png',
    });

    workbook.addWorksheet('FACTURAS', { views: [{ state: 'frozen', ySplit: 6, xSplit: 1, activeCell: 'A6', showGridLines: true }] })
    workbook.addWorksheet('FACTURAS-RECIBO', { views: [{ state: 'frozen', ySplit: 6, xSplit: 1, activeCell: 'A6', showGridLines: true }] })
    workbook.addWorksheet('RECIBOS', { views: [{ state: 'frozen', ySplit: 6, xSplit: 1, activeCell: 'A6', showGridLines: true }] })
    workbook.addWorksheet('NOTAS DE CRÉDITO', { views: [{ state: 'frozen', ySplit: 6, xSplit: 1, activeCell: 'A6', showGridLines: true }] })

    var sheet_FT = workbook.getWorksheet(1);
    var sheet_FR = workbook.getWorksheet(2);
    var sheet_RC = workbook.getWorksheet(3);
    var sheet_NC = workbook.getWorksheet(4);

    sheet_FT.addImage(imageId1, 'A1:A5');
    //sheet_FT.addRow(header);
    sheet_FT.mergeCells('A1', 'H5')
    sheet_FT.getCell('B3').value = header;
    sheet_FT.getCell('B3').font = { family: 4, name: 'Calibri', size: 25, bold: true, underline: true };
    sheet_FT.addRow("");
    sheet_FT.getRow(6).values = Cols_FT;
    sheet_FT.columns = keys;
    sheet_FT.addRows(Data.facturas);


    let totalRows: number = sheet_FT.rowCount
    //console.log("total number of rows : "+totalRows)
    totalRows = totalRows + 1;
    sheet_FT.getCell('G' + totalRows).value = { formula: 'SUM(G7:G' + (totalRows - 1) + ')' };
    sheet_FT.getCell('H' + totalRows).value = { formula: 'SUM(H7:H' + (totalRows - 1) + ')' };

    sheet_FT.eachRow({ includeEmpty: true }, function (row, rowNumber) {
      row.eachCell(function (cell, colNumber) {
        cell.font = {
          name: 'Arial',
          family: 2,
          bold: false,
          size: 10,
        };
        cell.alignment = {
          vertical: 'middle', horizontal: 'left'
        };
        if (rowNumber <= 6) {
          row.height = 20;
          cell.font = {
            bold: true,
            size: 11,
          };
          cell.alignment = {
            vertical: 'middle', horizontal: 'center'
          };
        }
        if (rowNumber >= 6) {


          cell.alignment = {
            vertical: 'middle', horizontal: 'center'
          };
          if (rowNumber >= 7) {

            row.getCell(7).alignment = {
              vertical: 'middle', horizontal: 'right', 'color': { 'argb': 'FFFF6600' }
            };

            row.getCell(8).alignment = {
              vertical: 'middle', horizontal: 'right', 'color': { 'argb': 'FFFF6600' }
            };
          }
          for (var i = 1; i < 9; i++) {
            if (rowNumber == 6) {
              cell.font = {
                color: { argb: 'FFFFFF' },
                bold: true
              };
              row.height = 25;
              row.getCell(i).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: '0099FF' }
              };
            }
            if (rowNumber == totalRows) {
              cell.font = {
                bold: true
              };
              row.height = 25;

              sheet_FT.getCell('A' + totalRows).value = "TOTAL";
              sheet_FT.getCell('A' + totalRows).font = { family: 3, name: 'Calibri', size: 11, bold: true };
              sheet_FT.getCell('A' + totalRows).alignment = { vertical: 'middle', horizontal: 'center' };
              // sheet_FT.getCell('G'+totalRows).value = total;
              row.getCell(i).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'CCCCCC' }
              };
            }
            row.getCell(i).border = {
              top: { style: 'thin' },
              left: { style: 'thin' },
              bottom: { style: 'thin' },
              right: { style: 'thin' }
            };
          }
        }
      });
    });
   // sheet_FT.addRow(footer);
    //////// SHEET 02 - FACTURAS RECIBOS /////////////////////

    //sheet_FR.addRow(header);
    sheet_FR.addImage(imageId1, 'A1:A5');
    sheet_FR.mergeCells('A1', 'D5');
    sheet_FR.getCell('B3').value = header
    sheet_FR.getCell('B3').font = { family: 4, name: 'Calibri', size: 25, bold: true, underline: true };
    sheet_FR.addRow("");
    sheet_FR.getRow(6).values = Cols_FR;
    sheet_FR.columns = keys_FR;
    sheet_FR.addRows(Data.facturas_recibo);


    totalRows = sheet_FR.rowCount
    //console.log("total number of rows : "+totalRows)
    totalRows = totalRows + 1;
    sheet_FR.getCell('D' + totalRows).value = { formula: 'SUM(D7:D' + (totalRows - 1) + ')' };
    sheet_FR.getCell('D' + totalRows).value == null ? '00' : { formula: 'SUM(D7:D' + (totalRows - 1) + ')' };

    sheet_FR.eachRow({ includeEmpty: true }, function (row, rowNumber) {
      row.eachCell(function (cell, colNumber) {
        cell.font = {
          name: 'Arial',
          family: 2,
          bold: false,
          size: 10,

        };
        cell.alignment = {
          vertical: 'middle', horizontal: 'left'
        };
        if (rowNumber <= 6) {
          row.height = 20;
          cell.font = {
            bold: true,
            size: 10,
          };
          cell.alignment = {
            vertical: 'middle', horizontal: 'center'
          };
        }
        if (rowNumber >= 6) {
          cell.alignment = {
            vertical: 'middle', horizontal: 'center'
          };
          if (rowNumber >= 7) {

            row.getCell(4).alignment = {
              vertical: 'middle', horizontal: 'right', 'color': { 'argb': 'FFFF6600' }
            };
          }
          for (var i = 1; i < 5; i++) {
            if (rowNumber == 6) {
              cell.font = {
                color: { argb: 'FFFFFF' },
                bold: true
              };
              row.height = 25;
              row.getCell(i).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: '0099FF' }
              };
            }
            if (rowNumber == totalRows) {
              cell.font = {
                bold: true
              };
              row.height = 25;

              sheet_FR.getCell('A' + totalRows).value = "TOTAL";
              sheet_FR.getCell('A' + totalRows).font = { family: 3, name: 'Calibri', size: 11, bold: true };
              sheet_FR.getCell('A' + totalRows).alignment = { vertical: 'middle', horizontal: 'center' };
              // sheet_FR.getCell('G'+totalRows).value = total;
              row.getCell(i).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'CCCCCC' }
              };
            }
            row.getCell(i).border = {
              top: { style: 'thin' },
              left: { style: 'thin' },
              bottom: { style: 'thin' },
              right: { style: 'thin' }
            };
          }
        }
      });
    });
   // sheet_FR.addRow(footer);

    ////////SHEET 03 - RECIBOS //////////////////

    // sheet_RC.addRow(header);
    sheet_RC.addImage(imageId1, 'A1:A5');
    sheet_RC.mergeCells('A1', 'D5');
    sheet_RC.getCell('B3').value = header
    sheet_RC.getCell('B3').font = { family: 4, name: 'Calibri', size: 25, bold: true, underline: true };
    sheet_RC.addRow("");
    sheet_RC.getRow(6).values = Cols_RC;
    sheet_RC.columns = keys_RC;
    
    sheet_RC.addRows(Data.recibos);


    totalRows = sheet_RC.rowCount
    totalRows = totalRows + 1;
    sheet_RC.getCell('D' + totalRows).value = { formula: 'SUM(D7:D' + (totalRows - 1) + ')' };
    sheet_RC.getCell('D' + totalRows).value == null ? '00' : { formula: 'SUM(D7:D' + (totalRows - 1) + ')' };


    sheet_RC.eachRow({ includeEmpty: true }, function (row, rowNumber) {
      row.eachCell(function (cell, colNumber) {
        cell.font = {
          name: 'Arial',
          family: 2,
          bold: false,
          size: 10,

        };
        cell.alignment = {
          vertical: 'middle', horizontal: 'left'
        };
        if (rowNumber <= 6) {
          row.height = 20;
          cell.font = {
            bold: true,
            size: 10,
          };
          cell.alignment = {
            vertical: 'middle', horizontal: 'center'
          };
        }
        if (rowNumber >= 6) {
          cell.alignment = {
            vertical: 'middle', horizontal: 'center'
          };
          if (rowNumber >= 7) {

            row.getCell(4).alignment = {
              vertical: 'middle', horizontal: 'right', 'color': { 'argb': 'FFFF6600' }
            };
          }
          for (var i = 1; i < 5; i++) {
            if (rowNumber == 6) {
              cell.font = {
                color: { argb: 'FFFFFF' },
                bold: true
              };
              row.height = 25;
              row.getCell(i).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: '0099FF' }
              };
            }
            if (rowNumber == totalRows) {
              cell.font = {
                bold: true
              };
              row.height = 25;

              sheet_RC.getCell('A' + totalRows).value = "TOTAL";
              sheet_RC.getCell('A' + totalRows).font = { family: 3, name: 'Calibri', size: 11, bold: true };
              sheet_RC.getCell('A' + totalRows).alignment = { vertical: 'middle', horizontal: 'center' };
              // sheet_FR.getCell('G'+totalRows).value = total;
              row.getCell(i).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'CCCCCC' }
              };
            }
            row.getCell(i).border = {
              top: { style: 'thin' },
              left: { style: 'thin' },
              bottom: { style: 'thin' },
              right: { style: 'thin' }
            };
          }
        }
      });
    });
   // sheet_RC.addRow(footer);

    ////////////SHEET 04 - NOTAS DE CRÉDITO ///////////////


    // sheet_NC.addRow(header);
    sheet_NC.addImage(imageId1,'A1:A5'
    );
    sheet_NC.mergeCells('A1', 'E5');
    sheet_NC.getCell('B3').value = header
    sheet_NC.getCell('B3').font = { family: 4, name: 'Calibri', size: 25, bold: true, underline: true };
    sheet_NC.addRow("");
    sheet_NC.getRow(6).values = Cols_NC;
    sheet_NC.columns = keys_NC;
    sheet_NC.addRows(Data.notaCreditos);


    totalRows = sheet_NC.rowCount
    // console.log("total number of rows : "+totalRows)
    totalRows = totalRows + 1;
    sheet_NC.getCell('E' + totalRows).value = { formula: 'SUM(E7:E' + (totalRows - 1) + ')' };

    sheet_NC.eachRow({ includeEmpty: true }, function (row, rowNumber) {
      row.eachCell(function (cell, colNumber) {
        cell.font = {
          name: 'Arial',
          family: 2,
          bold: false,
          size: 10,

        };
        cell.alignment = {
          vertical: 'middle', horizontal: 'left'
        };
        if (rowNumber <= 6) {
          row.height = 20;
          cell.font = {
            bold: true,
            size: 10,
          };
          cell.alignment = {
            vertical: 'middle', horizontal: 'center'
          };
        }
        if (rowNumber >= 6) {
          cell.alignment = {
            vertical: 'middle', horizontal: 'center'
          };
          if (rowNumber >= 7) {

            row.getCell(5).alignment = {
              vertical: 'middle', horizontal: 'right', 'color': { 'argb': 'FFFF6600' }
            };
          }
          for (var i = 1; i < 6; i++) {
            if (rowNumber == 6) {
              cell.font = {
                color: { argb: 'FFFFFF' },
                bold: true,
              };
              row.height = 25;
              row.getCell(i).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: '0099FF' }
              };
            }
            if (rowNumber == totalRows) {
              cell.font = {
                bold: true
              };
              row.height = 25;

              sheet_NC.getCell('A' + totalRows).value = "TOTAL";
              sheet_NC.getCell('A' + totalRows).font = { family: 3, name: 'Calibri', size: 11, bold: true };
              sheet_NC.getCell('A' + totalRows).alignment = { vertical: 'middle', horizontal: 'center' };
              // sheet_FR.getCell('G'+totalRows).value = total;
              if (sheet_NC.getCell('E' + totalRows).value == null) {
                sheet_NC.getCell('E' + totalRows).value == 'teste'
              }
              row.getCell(i).fill = {
                type: 'pattern',
                pattern: 'solid',
                fgColor: { argb: 'CCCCCC' }
              };
            }
            row.getCell(i).border = {
              top: { style: 'thin' },
              left: { style: 'thin' },
              bottom: { style: 'thin' },
              right: { style: 'thin' }
            };
          }
        }
      });
    });
    //sheet_NC.addRow(footer);



    workbook.xlsx.writeBuffer().then(Data => {
      var blob = new Blob([Data], { type: this.blobType });
      var url = window.URL.createObjectURL(blob);
      var a = document.createElement("a");
      document.body.appendChild(a);
      a.href = url;
      a.download = excelFileName;
      a.click();
    });


  }

}
