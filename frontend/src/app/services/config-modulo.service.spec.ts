import { TestBed } from '@angular/core/testing';

import { ConfigModuloService } from './config-modulo.service';

describe('ConfigModuloService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigModuloService = TestBed.get(ConfigModuloService);
    expect(service).toBeTruthy();
  });
});
