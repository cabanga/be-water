import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { AuthService } from 'src/app/providers/auth/auth.service';


@Injectable({
  providedIn: 'root'
})
export class ConfigModuloService {

  private currentUser;

  public modulo = {
    GESTAO_CLIENTES: 'gestao_clientes',
    LOGISTICA: 'logistica',
    VENDAS: 'vendas',
    ROTEIROS: 'roteiros',
    OPERACOES: 'operacoes',
    CONFIGURACOES: 'configuracoes'
  }

  public provincia_default: string = "provincia_default";
  public quarteirao_view: string = "quarteirao_view";
  public distrito_view: string = "distrito_view";
  public municipio_default: string = "municipio_default";
  public tipo_identidade_default: string = "tipo_identidade_default";
  public caucao_default: string = "caucao_default";
  public tipo_facturacao_default: string = "tipo_facturacao_default";
  public media_consumo_default: string = "media_consumo_default";
  public gestor_cliente_view: string = "gestor_cliente_view";
  public direccao_view: string = "direccao_view";
  public nivel_sensibilidade_default: string = "nivel_sensibilidade_default";
  public tarifa_fixa_modal_view: string = "tarifa_fixa_modal_view";
  public prozo_para_leituras_max_default: string = "prozo_para_leituras_max_default";
  public prozo_para_resolucao_anomalias_default: string = "prozo_para_resolucao_anomalias_default";
  public prozo_para_facturacao_default: string = "prozo_para_facturacao_default";
  public meses_facturacao_media_consumo: string = "meses_facturacao_media_consumo";
  public moeda_id_default: string = "moeda_id_default";
  public estado_rescisao_default: string = "estado_rescisao_default";


  private Configuracaos = [
    { slug: this.meses_facturacao_media_consumo, modulo: this.modulo.CONFIGURACOES },
    { slug: this.provincia_default, modulo: this.modulo.CONFIGURACOES },
    { slug: this.quarteirao_view, modulo: this.modulo.CONFIGURACOES },
    { slug: this.distrito_view, modulo: this.modulo.CONFIGURACOES },
    { slug: this.municipio_default, modulo: this.modulo.ROTEIROS },
    { slug: this.tipo_identidade_default, modulo: this.modulo.GESTAO_CLIENTES },
    { slug: this.caucao_default, modulo: this.modulo.CONFIGURACOES },
    { slug: this.tipo_facturacao_default, modulo: this.modulo.GESTAO_CLIENTES },
    { slug: this.media_consumo_default, modulo: this.modulo.GESTAO_CLIENTES },
    { slug: this.gestor_cliente_view, modulo: this.modulo.GESTAO_CLIENTES },
    { slug: this.direccao_view, modulo: this.modulo.GESTAO_CLIENTES },
    { slug: this.nivel_sensibilidade_default, modulo: this.modulo.GESTAO_CLIENTES },
    { slug: this.tarifa_fixa_modal_view, modulo: this.modulo.GESTAO_CLIENTES },
    { slug: this.prozo_para_leituras_max_default, modulo: this.modulo.ROTEIROS },
    { slug: this.prozo_para_resolucao_anomalias_default, modulo: this.modulo.ROTEIROS },
    { slug: this.prozo_para_facturacao_default, modulo: this.modulo.ROTEIROS },
    { slug: this.moeda_id_default, modulo: this.modulo.CONFIGURACOES },
    { slug: this.estado_rescisao_default, modulo: this.modulo.GESTAO_CLIENTES },

  ];

  constructor(private http: HttpService, private auth: AuthService) {
    this.currentUser = this.auth.currentUserValue;
  }
  /*
    public async getConfig(modulo, slug, callback: Function) {

      var result = null;

      console.log(slug);

      this.http.__call('configuracao/getConfiguracaobySlug/' + slug, null).subscribe(
        response => {

          console.log(Object(response));

          if (Object(response).code != 200) {
            this.saveConfig(slug, modulo);
            result = null;
          }
          else {
            result = Object(response).data.valor;
          }

          //callback();

        }
      );

      return result;

    }*/


  public saveConfig(slug, modulo, valor) {

    /* console.log("save config");
    console.log(slug);
    console.log(modulo);
    console.log(valor);
    console.log(this.currentUser); */

    this.http.__call('configuracao/auto-create/',
      {
        slug: slug,
        is_active: true,
        modulo: modulo,
        descricao: null,
        valor: valor,
        required: false,
        default: false,
        user_id: (this.currentUser == null) ? '1' : this.currentUser.user.id
      }).subscribe(

        response => {

          //console.log(Object(response));

        });

  }

  public initConfiguracaos() {

    for (let index = 0; index < this.Configuracaos.length; index++) {

      //console.log(this.Configuracaos[index].slug);

      this.http.__call('configuracao/auto-create/',
        {
          slug: this.Configuracaos[index].slug,
          is_active: true,
          modulo: this.Configuracaos[index].modulo,
          descricao: null,
          valor: null,
          required: false,
          default: false,
          is_boolean: false,
          user_id: (this.currentUser == null) ? '1' : this.currentUser.user.id
        }).subscribe(

          response => {

            //console.log(Object(response));

          }
        );
    }

    return;


  }


}
