import { TestBed } from '@angular/core/testing';

import { ExcelAutoService } from './excel-auto.service';

describe('ExcelAutoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExcelAutoService = TestBed.get(ExcelAutoService);
    expect(service).toBeTruthy();
  });
});
