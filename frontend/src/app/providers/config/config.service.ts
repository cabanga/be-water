import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ReportComercialService } from 'src/app/components/relatorios/report-diario/report-comercial.service';
import { ContratoService } from 'src/app/components/reports/contrato/contrato.service';
import { RescisaoService } from 'src/app/components/reports/rescisao/rescisao.service';
import { FacturaCicloService } from "src/app/components/reports/factura-ciclo/factura-ciclo.service";
import { FacturaServicoService } from "src/app/components/reports/factura-servico/factura-servico.service";
import { NotaCreditoServicoService } from "src/app/components/reports/nota-credito-servico/nota-credito-servico.service";
import { ReciboService } from "src/app/components/report-at/recibo/recibo.service";
import { ApiService } from 'src/app/providers/http/api.service';

import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
import 'sweetalert2/src/sweetalert2.scss'

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  public alertEvent = new EventEmitter<Object>();
  public loaddingEvent = new EventEmitter<Object>();
  public pdfEvent = new EventEmitter<Object>();

  constructor(private http: HttpService, private httpApiService: ApiService, private reportRecibo: ReciboService,  private RescisaoService: RescisaoService, private reportFactura: FacturaCicloService, private reportFacturaServico: FacturaServicoService, private reportNotaCreditoServico:NotaCreditoServicoService ,private ContratoService: ContratoService, private ReportComercialService: ReportComercialService) { }

  public showAlert(message: string, cls: string, show: boolean) {
    this.alertEvent.emit({ message: message, class: cls, show: show });
  }

  public loaddinStarter(type: string) {
    this.loaddingEvent.emit({ type: type });
  }


  public clearFormInputs(e) {
    for (var i = 0; i < e.target.elements.length; i++) {
      e.target.elements[i].value = "";
    }
  }

  empresa = {
    id: null,
    companyName: null,
    telefone: null,
    taxRegistrationNumber: null,
    city: null,
    province: null,
    addressDetail: null,
    email: null,
    active_tfa: 0,
    logotipo: null
  }

  public convertToSlug(text) {
    const a = 'àáäâãèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
    const b = 'aaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
    const p = new RegExp(a.split('').join('|'), 'g')
    return text.toString().toLowerCase().trim()
      .replace(p, c => b.charAt(a.indexOf(c))) // Replace special chars
      .replace(/&/g, '-and-') // Replace & with 'and'
      .replace(/[\s\W-]+/g, '-') // Replace spaces, non-word characters and dashes with a single dash (-)
  }

  public dataURIBase64toBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0) {
      byteString = atob(dataURI.split(',')[1]);
    } else {
      byteString = unescape(dataURI.split(',')[1]);
    }
    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], { type: mimeString });
  }

  openOrDownloadDocument(b64Data, title: string, click: boolean = true) {
    const blob = this.dataURIBase64toBlob(b64Data);
    var link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.title = title;
    var responseType = b64Data.split(',')[0].split(':')[1].split(';')[0].split("/")[1];

    if (click) {
      link.target = "__blick";
    } else {
      link.download = title + "." + responseType;
    }
    link.click();
  }




  public gerarImprimirFacturaPDF(factura: any, produtos: any[], cliente: any, user: any, pagamento: any) {
    this.pdfEvent.emit({ factura: factura, produtos: produtos, cliente: cliente, user: user, pagamento: pagamento });

  }

  public caixas = [];
  public getCaixas() {
    this.loaddinStarter('start');
    this.http.call_get('caixa/selectBox', null).subscribe(
      response => {
        this.caixas = Object(response).data
        this.loaddinStarter('stop');
      }
    );
  }



  public documents = []
  /**
  * @name "Listar Documentos"
  * @descriptio "Esta Função permite Listar todos Documentos"
  * @author "caniggia.moreira@itgest.pt"
  * @param start
  * @param end
  */
  public listarDocumentos() {
    this.loaddinStarter('start');
    this.http.__call('documento/listar', null).subscribe(
      data => {
        this.documents = Object(data).data;
        this.loaddinStarter('stop');
      }
    );
  }



  public clientAgt: any;
  public searchClienteAGT(contribuite: any) {
    let client: any = [];
    this.http.call_get('cliente/search-cliente-agt/' + contribuite, null).subscribe(
      response => {
        this.clientAgt = Object(response).data;
      }
    );
    return this.clientAgt
  }

  async imprimirContrato(item) {
    await this.ContratoService.imprimirContrato(item)
  }

/*   public imprimirRescisao() {
    this.RescisaoService.imprimirRescisao(item)
  } */


  public gerarReportDiario(id) {
    this.http.__call('get/RoleSlug/' + id, null).subscribe(
      response => {
        const dados = Object(response).data;
        console.log(dados);
        if (dados.role.slug == "Financeira") {
          //console.log("Financeira")
          this.ReportComercialService.reportDiario(dados.role);
        } else if (dados.role.slug == "chefe_loja") {
          console.log("chefe_loja")

        } else {
          console.log("not Found")
        }
      }
    );
  }

  openFactura() {


    const urlServer = 'factura/preview/'
    return this.http.__call(urlServer, null)
      .subscribe(
        (response) => {
          const b64Data = Object(response).data
          this.openOrDownloadDocument(b64Data, "Factura");

        });
  }




  public numberFormat(number) {
    return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace('€', '').trim();
  }

  public onConection() {
    if (navigator.onLine) {

      console.log('online');
    } else {
      alert('Sem conexão com a intenet');
      console.log('offline');
    }
  }


  public imprimirFactura(id, via, report:string='imprimir') {
    this.httpApiService.get('facturas/preview/' + id, null).subscribe(
      response => {
        const dados = Object(response).data;

        if (dados.factura.tipo_factura == "CICLO" && !dados.factura.is_service) {
          this.reportFactura.imprimirFacturaCiclo(
            dados.factura,
            dados.produtos,
            dados.cliente,
            dados.user,
            dados.pagamentos,
            dados.contrato,
            dados.leituras,
            report,
            via
          )
        }
        else if (dados.factura.tipo_factura == "SERVICO" && dados.factura.is_service) {
          if(dados.factura.serie.documento.sigla == "FT" || dados.factura.serie.documento.sigla == "FR"){
            this.reportFacturaServico.imprimirFacturaServico(
              dados.factura,
              dados.produtos,
              dados.cliente,
              dados.tipos_identidades,
              dados.user,
              dados.pagamentos,
              dados.contrato,
              dados.leituras,
              dados.contabancaria,
              dados.lojas,
              report,
              via
            )
          }else if(dados.factura.serie.documento.sigla == "NC"){
            this.reportNotaCreditoServico.imprimirNotaCreditoServico(
              dados.factura,
              dados.produtos,
              dados.cliente,
              dados.tipos_identidades,
              dados.user,
              dados.pagamentos,
              dados.contrato,
              dados.leituras,
              dados.contabancaria,
              dados.lojas,
              report,
              via
            );
          }
        }
      }
    );
  }


  public swalTest: boolean = false

  public confirmar() {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    })

    swalWithBootstrapButtons.fire({
      title: 'Você tem certeza?',
      text: "Você não poderá reverter isso!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim, Exclua!',
      cancelButtonText: 'Não, Cancele!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        swalWithBootstrapButtons.fire(
          'Excluído!',
          'Seu arquivo foi excluído.',
          'success'
        )
        this.swalTest = true
      } else if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'Seu arquivo foi cancelado:)',
          'error'
        )
        this.swalTest = false
      }
    })
  }

  public salvar() {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false,
    })

    swalWithBootstrapButtons.fire({
      title: 'Você tem certeza?',
      text: "Você não poderá reverter isso!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sim, Salvo!',
      cancelButtonText: 'Não, Cancele!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        swalWithBootstrapButtons.fire(
          'Salvado!',
          'Seu arquivo foi salvo.',
          'success'
        )
        this.swalTest = true
      } else if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelado',
          'Seu arquivo foi cancelado:)',
          'error'
        )
        this.swalTest = false
      }
    })
  }


  public bancos: any = [];
  /**
  * @name "Listar bancos"
  * @descriptio "Esta Função permite Listar todas bancos"
  * @author "caniggia.moreira@itgest.pt"
  * @param start
  * @param end
  */
  public listarBancos() {

    this.loaddinStarter('start');
    this.http.call_get('banco/listar', null).subscribe(
      response => {
        this.bancos = Object(response).data;
        this.loaddinStarter('stop');
      }
    );
  }



  public empresaUser() {


    this.http.call_get('empresa/empresa-user', null).subscribe(

      response => {

        this.empresa.id = Object(response).data[0].id
        this.empresa.companyName = Object(response).data[0].companyName
        this.empresa.taxRegistrationNumber = Object(response).data[0].taxRegistrationNumber
        this.empresa.telefone = Object(response).data[0].telefone
        this.empresa.addressDetail = Object(response).data[0].addressDetail
        this.empresa.city = Object(response).data[0].city
        this.empresa.province = Object(response).data[0].province
        this.empresa.active_tfa = Object(response).data[0].active_tfa
        this.empresa.logotipo = Object(response).data[0].logotipo
        this.empresa.email = Object(response).data[0].email


      }
    );
  }

  public caixa: any = [];

  public getOpenCaixa() {

    this.loaddinStarter('start');
    this.http.call_get('caixa/open', null).subscribe(
      response => {
        this.caixa = Object(response).data;
        this.loaddinStarter('stop');
      }
    );
  }




  public moedas = []
  /**
  * @name "Listar moedas"
  * @descriptio "Esta Função permite Listar todas moedas"
  * @author "caniggia.moreira@itgest.pt"
  * @param start
  * @param end
  */
  public listarMoedas() {

    this.loaddinStarter('start');
    this.http.call_get('moeda/listar', null).subscribe(
      response => {
        this.moedas = Object(response).data;
        this.loaddinStarter('stop');
      }
    );
  }

  public logotipoBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGEAAACGCAYAAADNR+SZAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAACCeSURBVHhe7Z0HeFVVtscdnarf9NHpvahv9M2o8/TZRp1RVMDewTL6BBUU6UhTAUVBeu8gvYTeaxokECD0mkCogVRaArm5SdZbv33PiScn52Ia5ATz/7715d5z9mnrv/Zaa6+978llUotqRy0JPkAtCT5ALQk+QLWTUFQkUlBoffmKwhc94Vx+keTlW1++gvAFCdoZ5ORZkbxg6PtXDb6JCfkFImmntEd8BYnwVWA+ebZIjpwoVELoG18d+IqEQtV96slCJaLoKxWsfUUCOKsBen96oRxTMiDlqwDfkQAyThfJnuOFkn76q9EdfEkCrmif9obdxwokO/fS7w6+JAGcVOXvSi2QXUrEKQ3YlzJ8SwLx4EBmoWw7Uih7jhXK2cClS0S1kxBU18MYwQu5qnhI2Hq4UJLTCi/ZjKnaSTiq44Ij2eG1y/7Nh0KSklFoRteXGqqVhDPnRLZbln5KP3uBETRtEg+oHCyU4zqqvtRQbSTgWpLUxdhWvldT0mAYt0Q5Y4OSsFFJ2KRyOgxhNRXVRgIWvVEVu0kJQLB0RstegLAdqYWSkBIS4kTgEqoxVQsJZDqb1QVtUKvGum2hRxCMvZCdUyTrlIC1KnH7NVBnXDpu6aKTwCROsg7E1qki16tCncI23BJtvLBb961OLpQ1+wolVv9eKvHhopOQlSPGmsNJvBKRccZbuTkBkTglACJik5Q0bX8mr+YTcVFJwLfjhtaoElEmglU7ZbUKrincmCA5vUgi9xZKjJIQpX+3anwI13NqCi4aCejpUFaRsWAsGUGR248Wyi4dEfPZWLi1PSXTW7NMhUJe5J4QCfyl9F2TcdFIOJevrgQFW1aMROtnpjVPnQuRw3cE5UJIOFfDoG35rgJZtbtAVqrEJBXomKPmEnFxSFD97FRrX7VbFYwFq/B5i7ome85gi7qVlY79fCYt9QKLAuKSC2TZzgJZoWQs1b+bDhXU2NH0RSEhU9PLyD0hq7Wtl7/OAEzJmu1uCRekKe4t2R4iAhKW7CiQozXULV1wErB0Aq1ttbblJuqYwKkygiu9Yakqc7nuR/i8XgdxFPnc4Lyr1Q0t2hZUMoKySCVaia6J1dYLTgLWaVusbbUol97hRpZuc7ZBFumxB7O83dKR7CKZvyUoC5UIyODzrjAuzM+4oCRQWiBoLlQrXbwjJAtUWfQMr/ljNtFDFm7VthyjgoJxZV5LYag1rdbYMHezEqDHIJBxoobNxl1QEpJ0ZIxiUCQCASg27XR4JREDaENb+zjOwZyzFw5rb4AEW2YnBnXkrUG6BvFwwUg4p6NbXAsuYoEqEUFJcfvClEod2HCgQOY4rHuengPXlKvndANlR+0JykxV/mxIUOEz8xA1BReMBAZhszep4lWBTksNl+04kaXuhOMgAuG4mXouRtteYLAWsTFflZ8vs1T4HLk7WGNm4i4ICad0AIZvnqWKs60zQq0zfl/QMxa4QRNcysyNeiznUMG66VVe/p7YELkrKNMS8mXGhpBMV9mvg7qagAtCQqIOnFAClolEqMzelK+xoOxKST9TqL0gZNX2eThngroqLx4PZxfK1PX5Ms2SKevyZbkmAjVh3qHKScBSURwKs60S5cTuLb821iQHZSrWrcdPt4TzZXq4NHoYAX2SKn+KHoNMiM+XlBow71ClJBAk49WNTFZF2BZpy/HzZEThQPyABKdMWguh3sGdbGyC7ocIBBIWqAvze2yoUhIy1YXQAyarsoqtUZUSo72gIoowo2IdB3AOzolMUjGknip9QoqE8zT+fL5GrxuXL+MtYTWfn1GlJJB+jlfrQ1HIRFyDKizc3HFZcOxUkSFzvBIx0ZJxqlhKFl5BfovGo7GrA/J5XEjG6Oel6qbCLSLwA6qMBOaAcQFYHm4AGacWuVLHCpVFjI6Yx+q5bMtGuWNVMrTnuUGFFaLGrM5XMkJ/kWMePccvqBISiAWxSUEZ7bDAcSrj4wNVMg+crvGEc3FOW7jWKh0LeI2M16UUyLCofBkdG5Lh0aFxg19RJSTYvcBYn1osMlIffoXm7lURFHE7UdobRsR8Ydmj9Pz0Cq/YQFlkXFxQhmt7jhmmJNA7cnw6H10lJKzdrw+sD4rVoZxR+uC4Avx5VYFzEQtQKgQjQ/Wa0Rr0va6yWAeLgyNDBCCDVuWb9U1+RKVJwLrw/5CAgpCh6gpWaS+oaqzQ+DJEFTuCa6lwzTHa67xW5DFahoQhei/IQCVhlo66A0H/9YZKk0Cxbag+LApB8MWkiMwNVDU4p02yLVj4Wh2buIGySVMHrAy1QfhcltrVxUalSDibX6Q5e9A8oK0ULI45hAsBYgPBuL8qEytHBuj1Jq8Leq7cY+DYe1m+aY/0Wa4DvQt0b5VBpUhgDVC/FSUtbaRaas4FnGLEkvHxKBXCEZTrtSgg9UShcV/s5z4hZIoS5rcRdKVIiNgYNA+H8hEeNt5jviAYLJT0zBw5V8ZfiheoltIyzkhWdq61pSSYU0ahXBvhulMTglLoUi7p6yRVes8lIQJ6LQ31nqMn/eWSKkxCmqaGPFBfhyLw11k5JTWRkZUjM+dvkydeHievNJkqUWv2WXu8sXVHqjTvMEfue2yYdPlsmSxYtlNOn8mz9obACLyvXq+PKpbr8pfvRz1+bMJ0aQ8loZe2+UxJ+GRRvlk84CdUmARWNvRYHFIAVoa1Ld9Rshdgb2dyA5J6/LTsP5AlY6esl/oNxsiE6RtDDVxYGZMsDz49UvoMjZE9yemSqQTuO5BpelGOY1oNdzJnU1A+0eujXORT/czsmxv86PAzvTeIQD5WEijqeQ3yqgsVIiFHDXN0bKib2xYGEfyY48uweVuq1H9htHT4aLEsWbVHDh05IdFx+6RH/0h54MnhMn/pDqtlSZw7FyxBBEU53Av30BMLVxJG6j2xms8JlsvgNj9aGCKKdv1X6rlKdq5qRYVIQAF0a6MAFT5P31C2WTOQevyUfNBjqbzQeJK83W62vPjmZHnnvdmybecxq4U3gtoFihwmPG5NULrrtVEugqJZ1+oGC5C7zA/1AqSbtqMU4hdUiATmfD+2LAvpuiDfzCm7sSo2SXoNjpZeg6Lls0FRMm/JDg2eXzw8nwMBspWSx65JOGCOGTVxnfTW42fM3aIEl1Ya10S53fUekG56H0yDulseyCxSYwnofQa0TUCPCcjWw/5JVctNwln1CEMig8aasEIefGi0ugHHD77PaF9v2Xme1FO307jFDA2wy6VJ61nySMOx0ki/p2fkWC1LIvdsvnzYc5l1XIT0HRYjLTrONUH9dT0uOSXTahnCSXU9pKBdVLEfLQwp+LOlgVJrlJjipN0H80JEvD83YCaI/IJyk7BT8/GPF+kD89AqH+qD4XNtEEj/03SqupgpkrQvw9oawpHUk8VK3bE7zdoawv6DWdJUXdOz/zdBdu4puS/7xFlp88ECqfv8aFm9NsXaGgKT/ygV5dqWzq9B3ZisKWznOaH77TibKmwNJoG1pJ0cD03X5leYwWCBURSpZefuiyXrhHeOz1ih58AouV+D8CtNpki/YbEaD+bIA0+NkHc7zNWMyPs4xg6fDY4y7V5oNEkWrdil7qzQGEVXvYcPEUvBuEs3GCmzD8I66F8GfH4ZtJWLBF4GxRxvJ8ui6N64gdOaaWRm58hETT2dlkoQPZZ2WpL2Z8ph7QUFjniQkHhIPp+6QT7pt0pGTUqQ2Pj9RtE2jutxKYey5dDRE5LvmBZjHDFxRqIM/zxe3VdAArqrx+KAsXIUjKIppee5CnUHs4rkvZmh/e1nBUyMKM/qjwuJcpFAAQ3r76wPi3TUB6dMwWsR3IFzw6ZD8lqzafJvtfgnX/ncWPDzjSbqYC3ZauGNzdtTpUmbWXLf48PkzVYzzbjh6VfHy9LIPSUyI8BXtjBo7KCKxThQMj3DvUye1/fQAxBIoF243z9cbJSLhPQzReYBeFiknVoWE+tuzFqwTe6qO9gE2bj1B2R3Urqx/E8HRModDw8ygzYvrIjeK/c8MlRadZova9alSLL2oPVKZl8dvN1Vb7AMGL7aM0ticRj3YiuZz/yaxwlqTuxrq/vY32pGwPyO2g8oFwksN3Q+bGt9EEbO4OixUxKvCu+qmdAdDw2U6XO2mO1uLFZffnf9IdKu6yJV9AEThBMSD0sPJeifun3MpPWlLB7ExO3XwdwIeUt7Ca5r++7jhtwCdVWUJlpOV+WqgSAt9b6IFU6wVqnTnDy95zxpE5EnzaflyfoUf6Sp5SIhQW+6jT4gvhWBBF53ALDW21X5/3l7msRpnu+EexyAX2/SeqYhg7T1X08MlwaNJ8uq1SVdlXNMAXYnpUkrTX3ve2yoPPTcKHn1nWmSkXFakjUJa6FKbavKRfjsLllDQofZmjpPz9NekCfNpuaZpZZ+QLlI4McePASW1Fql3cw884oDQE9AbHeBAinWMVh7V9PST/uvkuVReyXgeK/OcVUgPeFw6gkNyl8chwvrPzxWPu6zUj7uvdJkQqd4G4mF9Mwz5jiCNr2GZfPNUa7eE/KuksBvHJwgntETmqvyIanp5Dwz3+AHlIsEShM8IJYEGZDg7vYgQ9PM97oulP99aJA0eGOSyYBe1nT0fx8caILuwSMnrJYlwSDvvW6L5O56QzSYj5OBo9doz5pq4kvDNyeXGqzZ2H2sUN6dEnIxyNuqYOKEE5BATzDtlIgmE5WEMizTvxgoFwnMojXTh2ipD9pCH6SNkrHDVa7Yob66wRuT5cmXP5eNW45YW0PYsee4KWc/roO1dRsPWltDSNqfIa+9M13uVVfjdmcM5CCxzjMjTdxxAxK4L5SLvD2pNAn8PKv9rEuABMrHWBkPwcNAhj06HacZDwU5BmuNms8wrskLjH5bdJpnsqBXdGT9RsuZJpWt8/QIefa1CaVG2TZOnT6n8WC+3PPoUHnxrSnyul6DLCpVXRkxwU0Cv2twguyoHa7KatdU23jNTVcHykUCyxzfsUlQ4cHXHwg9yLwlO01KGjF/a4lBWcrBbFkVm1zKlSxZuVs+6as+X/3+R31WyOSIRDl77gvFHU8/bdJaeogzPBNncG9kYUPGxMkZjRV70oqMUm0S+GxnbTaompIZcd+04d43Wvde3SgXCVQjbRIQujQ/ifICruilN6eYQRoups5TI42lR2kG5JWC2ti07ai8pYO0OjpIo8T90LOh4+Yt3uE5RgC4lSYOEvjsrh+xEtDuBRBAkD50nte+XUyUiwQmbZwk0O3Hx5WeR5i9cLvc+sAAU0HFwmPi9plydPOO8+Tm+/rJqAkJZrTrBlnQLf/qJ//R1HOSHrd2w0GZs2i7dOq+RP56Vy+TBnthTGzA3Evxfek98jofJ3hXhn3v9JTeywKS7w8OykcCXZr0zn5YLKuzWhTLTchsErceVYUtNgRQhnZP7DPhP27KBrmtzkBp9f582bD5sOzamyZbtqea+Yab7usr3futNDUhNyDxtjoDpFn7OSaF3b7ruORrusvMWZd5geKYwD210czN/dtnJnbseAYJrNbwC8pFAuv/B+rN2w9jP3RSepH6/iyj/LrPjTaT8+dDZGySqSf999195K939pL/uqOX1Ht+tEyakWi18EakurKGmnlxDOXw3Nw8HSMUSSs1DNvX0yNYeMA7t51g/sC+b9qwVskvKBcJgAzpLY0FNgk8EOtPweGjJ+Wk9VpHegHlhT5Do0021F2DMAH6DG+OUrCCYt+BLNN79mpGdILXvSiomMYlHDQ9iYDdrddyE8TJjkBubsBUV5kiBZPWBksEZeIUa4vc6L9CXZZFAoZDWusXlJsEFtWSmjqDHPn3IUf3P55+xqSTWGz9F8ZIY01Dsdwb9HvjlhFm5YUXIOa9rovk7/f0NYG5/UeLTQX1pnv7mh4AWU7wW+X2OmB0uiL+sjTTidPnikxllZhAj2mh7srrtQ7VhXKTQNWB+r0zQGOJE+NDvQGLZbxACXvJyj2aQuZJIFAgOdoDYrRnPNJgjJF1GnSJETZ2JaWb8cXtOsqOmLvVEGKOU8snBjzScIzJtBjI2TWl6Tp4dPaCd5QElJ3v6gi84NC4LN3PfQ+O1Db+yE4Nyk0CYDl6U4dLQrAw6vO5uefko94rJNE1Wrax/0CmISmUPUXIhz2WadY011RQqZIyNvACk0IUB8mQSHFxJ6ZHOu4BV7TSYzU45XbbhUIaK/j8hAqRQPfuviiUCtoKwMI+Xhgo8ap9Ju7JYtZtPCRbdx4rzpbIfsZP2yivaSpKiQPlDhgRK2mZoQUAFPMo0G3ZkWrEXoFHz2AalWt0X1SyN4aun1fqx+a5edpW7wuXhbyn7sudvlY3KkQCYIDEg9t+GMHKhmhXD3mZIhkxfq3cVW+IKdzdWXewqf8sWLrLHG/jrKZcTrVt3HxE3mw907R/5rXxZhIIkih/A4qtJAJON0Rv4F74bZsbvOjcJoskou/ygG/mlm1UmAQUxw81cAG2MhCUg5IYO2SqZUfM2yrRa/aZNURM0BNk+2nm454rAKS2t/y7nzz24jjpMyRa1iSkyLBx8WYuOqhOnKUrn8dpqukgwL7mF+SXBKvF7fa0W5Pso2BgocIkAIpidtbhVApuakhUwLxk0AkGdP3V7dx4d2+zsmL9psOSrY0YsHX+ZIkS1E/eaj3LrD11AzcDuW7SuRb34LWibq/2grZM4Fiu6P25ecaV+g2VIgEQIFtrymenibZgdawBYjbOjamzNptM5+/39JH/ub+/6R13PDxYPu0fWWqUDTYfLjDncroghGu20mu7y+kAl8MqcbsX8Lcqfs57IVBpEkDC/oJipTiVZPeQETEB88oDp36PHjtpVmSQDZlqqWsMwFIWCobDowMmvXT3Nq5FCSUmzCsWeDukfT8ca5bm+PRt81VCAsDisUq3sgjcWCEK6aejVlJI1pDme3gFSg07UgvMjw4HrwplNBzrDP6IfY1wP31iPpl1UcVlCv3rl7kDL1QZCWD70QLjn5u43IYtKA+fTl3/E00xWVLP8nYk9AOOgJmtI6d3k2kLLqnLvDxJPOitVII3vafYDel5iCV+Gpy5UaUkAMrd/NgbJaIA52DKFnvkyn6nsM1t9aa9ir1/pGZk/EooHOYy+2cRQHvmDc73zj0/oMpJAKSKG9Q9Ud4w7kiV4VZsWYVjOQe9hCUq4SyajHfJtqAhjPa4Mnqc1+8V/IYLQoINXvbBoA7/TokBC3VaPYpySoneYVnzwJUBk9s7fqRTCpA+f3No/tv0MhWCNv+ToSbggpJgAz/N2tBVu3g/RUA+VatmIqbDrNCyGYTPLDDG4lkxvXxn0Kz488hYS4DxA+4PEu2RMz2AjK2m4KKQ4AYuhQW6KZmFZkCF8Bnf/WVKd4LXOlOvIljTA/gLkX5Z6FtWVAsJlcWh7CLzhhdSYuPalAB6Ai848XsQ9kKNIQHVHtTewqvXmMghjSXdxf1QJcX/e9WOagJ8TQIrMpiwZ/U005OMId6ckCdvqLDwl6Adr0Gbue+aDF+RgNJ5dQ+K5TWe1H6o/+NqWHaJ4qncsrCLEnWYZUg1Dr4i4VywyARp/ksIs3fRe4KyQUfGuCHeLsYPwD0q4DUeNTIwX2q4qCTwa8uCgoJiyc/PDyvBYLBE2/MtnazpqDISUNKZnBzJysqSjIwMSU9PLyVpaWlyPO14SI4fl2PHjp1XituqcKw5T8YX5+M6yImTJyQQOM+Q2ueoUhJyz56VU6dOycmTJ+XEiROSrZKVnSWZWZkhhVmKs8ngbzixlW8rm7+ZmZmG5OzsbHN+hGudPn1aAvm1JHwpIAl3ZFxS4Rdu5svEPgYpKrEk4NJBbWD2AWpJ8AFqSfABKkwCKWRqaqocPny4hBw6dMgESkAcIFCTCbHP3u6FsxrUDxw4YALvl4HzIaSyThCojxzxXn5J24MHWf8avky7fft2iYqK8hTuzQskCdHR0cXt+BwfHy+bN2+W3Fzvl6W4UWESeKBbb71Vfvvb38pvfvObYvnVr34lAwcONG0iZs6Ue+65R66//nr5/e9/L3fddZe0aNFCkpKSzH4n5s+fL7/85S+lSZMm1pbS4CFfeOEFc77rrrtO6tevL5GRkdZekW7dusmf//xnmTNnjrXlC+zZs0d+97vfyf79+60tpVGvXj257LLLPKVjx45Wq5JYsmSJZ3ukQ4cO5yXdRoVJSE5Olquuukruvfdec7F27doZadOmjSxbtsy0YTs3M2LECJk7d640bdpUfvCDHxglHtKeYYMe89BDD8mVV14pP//5z41FurF48WL58Y9/bMjs3LmzkZtuuknmzZtntRB55513zPX+8Y9/GAt1YuvWrWbf7t27rS2lUadOHXn44YcNUW5xn8/G0qVL5Wtf+5oMHjy4RPuGDRrKN77xDdm2bZvVMjwqTMK+ffvkO9/5jgwZMsTaUhoo6ic/+Yn1LQSUyU136tTJ2sLvEs6Yc7Vu3Vouv/xymTp1qrUnBAZiTzzxhPz0pz81YwYnnC6pWbNmcs01Vxtl9+3b19oaAspgOz0iHB544AF59tlnrW9lAyRw3tmzZ1tbQmCwyfbz6cdGpUkYMGCAtaU0IAHrdYIYgQUjNkaPHi3f/e53TQ+67vqQm3GCOHG9bnce44W3335b/va3v8nrr78uP/zhDyX1WGgRMSgrCU8//bT1rWywSYiIiLC2hIC7Zvsbb7xhbQmPSpMwdNhQa0tpeJHACPeGG26Qm2++2doi8vzzz5vvZ7RH4NJwS86gRk/AX0PU+vXer+kBkPCXv/zFtMHtEX9slJWEJ598UvLy8koJg0Uv2CQQ05zAJbF90aJF1pbwqBQJKOXV1141/h5LmBExwwRF20V4uaNZs2aZm+vdu7f5TjZFwGzUqJH5Trdm/8SJE813GwTAK6+6Ur73ve9Jn759zEO6QUyAQNCocSP55je/WeyTy0LC448/btwhxzmFa7qVbAMSOKZt27bm3m255ZZb5Be/+IXR05ehUiRcffXVxr9//etflyuuuEKuuPwK4wZIFcEHH3xgHmLChAnmIbBU9t9+++3FgW7hwoXmHLZSc3Jy5NrrrpUXX3zRfHdixYoVcueddxplkpXh9wnqNmwSCgsKJSUlxSjvkUceMfsI9hy3d+9eY9mky7bYRkNQvu2222Ty5MklZNq0aWFTX1woAZhz28Lz/OxnP5O1a9darc6PSpHAA/fo0cO4EbuYhtiK+fDDD81NEVDtrIjgSdCyQUpKmxEjRxrr56FvvPFG41YYh7iBwpYvX27SY44bOCiUDgObBLui+v7775s2kEc2x+ejR49K+/btQ0ZjyaRJk0x73BGusTyw3dHw4cPN2AWyMDDGPWVFpWOCPSbwAkr4/ve/bwYuDGCcygcoC1eE4jjXt7/9bSO4OZSzTJUdDgTrP/7xj3LttdcWxw+bBCwdUHnl/Fj4xo0bjbLocZAIEWRo/OX+QGUCM262oqg0CV+WHbljghMxMTGm6w4dOtQQYottta1atbJaeoMgTjtK3MBNAiDzog2uC2LPN06AhGeeecb6VjaEy47Kg0qTgALDwSs7cuLdd9+VH/3oR7JrV8nfsaHEu+++W37961+bcjbuzalYwDbiw5/+9CfjDoEXCefOnZN//vOf5lz47i8brJGF4VLcQq+yM6SdO3cWE19WEnCDDBhtV+lEpWPCK6+8EsqMZswwMmXKFFm3bp1pw1CfWOAFYgdBkFKGM7jaINbQSzgXPpbBWpcuXcwImetwXR7eORhiRP6tb32rFGELFiwoDp7nI6Fu3bqmjZeQ7XDPgEpBnz59zGcGn+wneJ8PZH+0Q29uVJgEsg/qNFg6gfeaa64xwne7/tO9e3djqV7AR9O2Z8+e1paSiI2NNedD8QTj5557zrSnNkVpgzoT8QhLt4F7YrubBHoTx5OZ4erCYeXKlcZ9jR07toSMGTPGEGlbMYF8x47QvxjAwseOHRO2wGeDmMi57V7rRIVJ4EFJ+xITE41CbdmwYYMhCBCI7TzdDW6G9uEqqxS+OD/jCBsE4y1bthh34FS+DSq17PcaWNGbuJ6bID+gwiTUoupQS4IPUK0kkHVUFvbovCajQiTge5lBIrVk4BMTG2NqQWQyBDKCEHMI+OG4uDijKDIo/DGjSrIb/Do1FrIbgjCpLvsoVTNRwwCKGDBu3DhzPO1JBznWjjnUgXr16mWCJEU7rs/nVI1F1LAgmbJCWWbrqhMVIoGaSN9+fSV+bby89NJL8thjj8kf/vAHGTZsmNR58EGT8dStV9csyhpsKZnJHupEEEd5d+bMmSZdfOqpp8yECOMCAjGzbqSKpIOQwjiC7UuXLZWEhARDWlR0lLkPBnsPam6/es0aQ0L//v1NMZH7o041ZMhgcy/b9Hg/o0IkkGVg+YmbEqVBgwbycfePTcVw1apV0kbTxJ6f9ZSXX37ZpG0btC3zA6R5KIkCGoM0LB3SmH0bNGiQKS2gPLKbli1bms+RUZEmrYuOiTZVVD6TstILya7oFZ9oGjxh4kRzDka7bGNyiBoUKSzZGsf5GRUigbybFJE0EmWQx9tVUVwO+/lOqsh33AltybOd6SPWzn6E89CGY2lHPehsbqgIxneECSGENsAuknEvVF8RjmMFn3092nils35ChUjgofDR5Pj4W3wvCkUBuBAePC09zVg9isnKzDL7GDfQjiJairWygv2cC5eDr8fP5wfzTVkA8lBousYJtjFmYPWevbIDodKKC/O7os+HCpHAAK1rt65GMYMHD5KN+r1Dx46m+3fq3NlUJgm0uAKm+eo/Ut/4atwRo1rcDecYNnyYCay4qubNm5s2DP/x/QT49h3am2PYt2TpEunatatxN8wDcw7mohs3bmxclz0nUBNRIRJQUtt2bY0b2bRpk/HHlCreevNNaaYKa9K0qSnekSWt0aB56223mjYjR440cwwNGzY029clrJNHH33UBGZiyOrVq812egVZF8qmLYGcEghLWgjk99//b7ONySKWwBAjuJeaigqRgHVj6bghLPrTTz81Vk9QxapR5Pjx401xi4Ie7ofCG1aO1fbr18+koSxz5y91GVJYAvuoUaPM+XFLnI95YjsFJbuivsMECvUbZutoz/HuVRg1CRUioRZVi1oSfIBaEnyAWhJ8gFoSfIBaEnyAWhKqHSL/Dx+VRyUIUR+qAAAAAElFTkSuQmCC'

}
