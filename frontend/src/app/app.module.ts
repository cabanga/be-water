import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import {  RxReactiveFormsModule } from "@rxweb/reactive-form-validators"
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { JwtInterceptor } from "./interceptors/jwt.interceptor";
import { ErroInterceptor } from "./interceptors/erro-interceptor";

import { ReactiveFormsModule, FormsModule } from "@angular/forms";
//  Importar o módulo da biblioteca
import { NgxSpinnerModule } from "ngx-spinner";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";
import { ArchwizardModule } from "angular-archwizard";
import { Select2Module } from "ng2-select2";

import { NgxMaskModule, IConfig } from 'ngx-mask'
export const options: Partial<IConfig> | (() => Partial<IConfig>) = null;;

import { AppRoutingModule, routeComponents } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LayoutComponent } from "./components/layout/layout.component";
import { AsidenavbarComponent } from "./components/layout/asidenavbar/asidenavbar.component";
import { TopnavbarComponent } from "./components/layout/topnavbar/topnavbar.component";
import { FooternavbarComponent } from "./components/layout/footernavbar/footernavbar.component";
import { SetingsnavbarComponent } from "./components/layout/setingsnavbar/setingsnavbar.component";
import { FornecedorComponent } from "./components/fornecedor/fornecedor.component";
import { UtilizadorComponent } from "./components/utilizador/utilizador.component";
import { ListarProdutosComponent } from "./components/listar-produtos/listar-produtos.component";
import { ImpostoComponent } from "./components/imposto/imposto.component";
import { from } from "rxjs";
import { DocumentoComponent } from "./components/documento/documento.component";
import { SerieComponent } from "./components/serie/serie.component";
import { ListarFacturacaoComponent } from "./components/listar-facturacao/listar-facturacao.component";

import { LoginComponent } from "./components/autenticacao/login/login.component";
import { ReciboComponent } from "./components/recibo/recibo.component";
import { LoadingComponent } from "./components/loading/loading.component";
import { AlertComponent } from "./components/alert/alert.component";
import { SaftComponent } from "./components/saft/saft.component";

// providers imports
import { AuthService } from "./providers/auth/auth.service";
import { HttpService } from "./providers/http/http.service";
import { ApiService } from "./providers/http/api.service";
import { ConfigService } from "./providers/config/config.service";
import { PDFComponent } from "./components/pdf/pdf.component";
import { EmpresaComponent } from "./components/empresa/empresa.component";
import { StockMovimentoComponent } from "./components/stock-movimento/stock-movimento.component";
import { ContaCorrenteComponent } from "./components/conta-corrente/conta-corrente.component";
import { PermissionsComponent } from "./components/permissions/permissions.component";
import { RoleComponent } from "./components/role/role.component";
import { ModulosComponent } from "./components/modulos/modulos.component";

//import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from "./components/register/register.component";

import { SummaryComponent } from "./components/board/card/summary/summary.component";
import { DetailComponent } from "./components/board/card/detail/detail.component";
import { BoardComponent } from "./components/board/board/board.component";
import { HeaderComponent } from "./components/common/header/header.component";
import { ListComponent } from "./components/board/list/list.component";

import { ContentEditDirective } from "./directives/common/content-edit.directive";
import { LocalService } from "./providers/board/local/local.service";
import { TarefaComponent } from "./components/tarefa/tarefa.component";
import { BiComponent } from "./bi/bi.component";

import { ContextMenuComponent } from "./components/common/contextmenu/context-menu.component";
import { AprovisionamentoComponent } from "./components/aprovisionamento/aprovisionamento.component";
import { ProdutoFornecedorComponent } from "./components/produto-fornecedor/produto-fornecedor.component";
import { MoedaComponent } from "./components/moeda/moeda.component";
import { BancoComponent } from "./components/banco/banco.component";
import { ProjectoComponent } from "./components/projecto/projecto.component";

import { P404Component } from "./components/error/404.component";
import { P500Component } from "./components/error/500.component";
import { P403Component } from "./components/error/403.component";
import { TarifarioComponent } from "./components/tarifario/tarifario.component";
import { ContratoComponent } from "./components/contrato/contrato.component";
import { FacturacaoChargeComponent } from "./components/facturacao-charge/facturacao-charge.component";
import { PlanoPrecoComponent } from "./components/plano-preco/plano-preco.component";
import { NgSelectModule } from "@ng-select/ng-select";
import { NgxImageCompressService } from 'ngx-image-compress';

import { NgxPaginationModule } from "ngx-pagination";
import { TestPaginationComponent } from "./components/test-pagination/test-pagination.component";
import { MovimentoCaixaComponent } from "./components/caixa/movimento-caixa/movimento-caixa.component";
import { LojaComponent } from "./components/loja/loja.component";
import { AberturaComponent } from "./components/caixa/abertura/abertura.component";
import { FechoComponent } from "./components/caixa/fecho/fecho.component";
import { FacturacaoContaComponent } from "./components/facturacao-conta/facturacao-conta.component";
import { RedefinirPasswordComponent } from "./components/utilizador/redefinir-password/redefinir-password.component";
import { PedidoReportService } from "./components/report-at/pedido/pedido-report.service";
import { UtilizadorAlterarPasswordComponent } from "./components/utilizador/utilizador-alterar-password/utilizador-alterar-password.component";
import { DepositoCaixaComponent } from "./components/caixa/deposito-caixa/deposito-caixa.component";
import { DiarioVendasService } from "./components/report-at/vendas/diario-vendas.service";
import { PrioridadeComponent } from "./components/reclamacoes/prioridade/prioridade.component";
import { TipoReclamacoesComponent } from "./components/reclamacoes/tipo-reclamacoes/tipo-reclamacoes.component";
import { ReclamacoesComponent } from "./components/reclamacoes/reclamacoes/reclamacoes.component";
import { PedidosComponent } from "./components/pedidos/pedidos/pedidos.component";
import { ConsultarReciboComponent } from "./components/recibo/consultar-recibo/consultar-recibo.component";
import { AdiantamentoComponent } from "./components/adiantamento/adiantamento.component";
import { ListaDepositosComponent } from "./components/caixa/lista-depositos/lista-depositos.component";

import { NgxLoadingModule, ngxLoadingAnimationTypes } from "ngx-loading";
import { FacturacaoAutomaticaComponent } from "./components/facturacao/facturacao-automatica/facturacao-automatica.component";
import { CicloFacturacaoComponent } from "./components/facturacao/ciclo-facturacao/ciclo-facturacao.component";

import { TecnologiasComponent } from "./components/tecnologias/tecnologias.component";
import { EstadoReclamacoesComponent } from "./components/reclamacoes/estado-reclamacoes/estado-reclamacoes.component";
import { EstadoPedidosComponent } from "./components/pedidos/estado-pedidos/estado-pedidos.component";
import { TiposPedidoComponent } from "./components/pedidos/tipos-pedido/tipos-pedido.component";

import { UnificarclienteComponent } from "./components/terceiro/unificarcliente/unificarcliente.component";
import { ClientesunificadosComponent } from "./components/terceiro/clientesUnificados/clientesunificados/clientesunificados.component";
import { BillRunHeaderComponent } from "./components/relatorios/bill-run-header/bill-run-header.component";
import { GetPdfEmailComponent } from "./components/get-pdf-email/get-pdf-email.component";
import { EnvioRelatorioDiarioComponent } from "./components/relatorios/envio-relatorio-diario/envio-relatorio-diario.component";
import { ReportDiarioAutoComponent } from "./components/relatorios/report-diario-auto/report-diario-auto.component";
import { CriarParceriaComponent } from "./components/interconexao/criar-parceria/criar-parceria.component";
import { ListarParceriaComponent } from "./components/interconexao/listar-parceria/listar-parceria.component";
import { VisaoGeralComponent } from "./components/conta-corrente/visao-geral/visao-geral.component";
// Import ng-circle-progress
import { NgCircleProgressModule } from "ng-circle-progress";
import { RelatorioFinanceiroComponent } from "./components/relatorios/financeiro/relatorio-financeiro/relatorio-financeiro.component";
import { EntidadeCativadoraComponent } from "./components/entidade-cativadora/entidade-cativadora.component";
import { VisualizarRelatorioComponent } from "./components/visualizar-relatorio/visualizar-relatorio.component";
import { RedefinirPasswordFirstAcessComponent } from "./components/utilizador/redefinir-password-first-acess/redefinir-password-first-acess.component";
import { RecursosRedeComponent } from "./components/recursos-rede/recursos-rede.component";
import { EmitirNotaCreditoComponent } from "./components/facturacao/emitir-nota-credito/emitir-nota-credito.component";
import { LeituraContradorComponent } from "./components/leitura-contrador/leitura-contrador.component";

import { CobreFormComponent } from "./components/formularios/pedidos/cobre-form/cobre-form.component";
import { CircuitoEmpresarialFormComponent } from "./components/formularios/pedidos/circuito-empresarial-form/circuito-empresarial-form.component";
import { RejectFormComponent } from "./components/formularios/pedidos/reject_fom/reject-form/reject-form.component";
import { PedidoCircuitoEmpresarialComponent } from "./components/formularios/pedidos/circuito-empresarial-form/pedido-circuito-empresarial/pedido-circuito-empresarial.component";
import { GeralFormComponent } from "./components/formularios/pedidos_crm/geral-form/geral-form.component";
import { FormCircuitoEmpresarialCRMComponent } from "./components/formularios/pedidos_crm/form-circuito-empresarial-crm/form-circuito-empresarial-crm.component";
import { ProgressBarModule } from "angular-progress-bar";

import { ReportCotrancaGlobalComponent } from './components/relatorios/financeiro/report-cotranca-global/report-cotranca-global.component';

import { ExcelService } from 'src/app/services/excel.service';
import { ConfigModuloService } from 'src/app/services/config-modulo.service';
import { RelCobracaGlobalService } from 'src/app/components/report-at/relatorios/financeira/rel-cobraca-global.service';
import { ReportLojaService } from 'src/app/components/report-at/relatorios/financeira/report-loja.service';
import { ReportIVAService } from 'src/app/components/report-at/relatorios/financeira/report-iva.service';
import { ReportServicoContradosService } from 'src/app/components/report-at/relatorios/financeira/report-servico-contrados.service';
import { ReportLojaComponent } from './components/relatorios/financeiro/report-loja/report-loja.component';
import { ReportIVAComponent } from './components/relatorios/financeiro/report-iva/report-iva.component';
import { ReportServicosContratadosComponent } from './components/relatorios/financeiro/report-servicos-contradados/report-servicos-contradados.component';
import { DireccaoComponent } from './components/direccao/direccao.component';
import { PedidosRelatorioComponent } from './components/relatorios/pedidos-relatorio/pedidos-relatorio.component';
import { ReclamacoesRelatorioComponent } from './components/relatorios/reclamacoes-relatorio/reclamacoes-relatorio.component';
import { CreateOrEditClientComponent } from './components/terceiro/create-or-edit-client/create-or-edit-client.component';
import { PontoVendaComponent } from './components/ponto-venda/ponto-venda.component';
import { CreateOrEditImpostoComponent } from './components/imposto/create-or-edit-imposto/create-or-edit-imposto.component';
import { ReportClienteService } from './components/report-at/relatorios/report-cliente.service';
import { CreateOrEditProdutoComponent } from './components/listar-produtos/create-or-edit-produto/create-or-edit-produto.component';
import { CreateOrEditTarifarioComponent } from './components/tarifario/create-or-edit-tarifario/create-or-edit-tarifario.component';
import { CreateOrEditUtilizadorComponent } from './components/utilizador/create-or-edit-utilizador/create-or-edit-utilizador.component';
import { EditFlatRateServicoComponent } from './components/terceiro/edit-flat-rate-servico/edit-flat-rate-servico.component';
import { VisualizarFlatRateComponent } from './components/terceiro/conta/servico/flat-rate/visualizar-flat-rate/visualizar-flat-rate.component';
import { UpdateFateRateComponent } from './components/terceiro/conta/servico/flat-rate/update-fate-rate/update-fate-rate.component';
import { ModalexportarcontacorrenteexelComponent } from './components/conta-corrente/modalexportarcontacorrenteexel/modalexportarcontacorrenteexel.component';

import { CreateOrEditSerieComponent } from './components/serie/create-or-edit-serie/create-or-edit-serie.component';
import { ClienteRelatorioComponent } from './components/relatorios/cliente-relatorio/cliente-relatorio.component';



import { DispositivosComponent } from './components/dispositivos/dispositivos.component';
import { LtePrePagoFormComponent } from './components/dispositivos/formularios/lte-pre-pago-form/lte-pre-pago-form.component';

import { PreFacturacaoComponent } from "./components/facturacao/pre-facturacao/pre-facturacao.component";
import { WimaxDispositivoFormComponent } from './components/dispositivos/formularios/wimax-dispositivo-form/wimax-dispositivo-form.component';
import { CmdaDispositivoFormComponent } from './components/dispositivos/formularios/cmda-dispositivo-form/cmda-dispositivo-form.component';
import { NumeracaoInventarioRedeComponent } from './components/numeracao-inventario-rede/numeracao-inventario-rede.component';

import { PerfilComponent } from './components/utilizador/perfil/perfil.component';
import { ReportPagamentoGlobalComponent } from './components/relatorios/financeiro/report-pagamento-global/report-pagamento-global.component';
import { ReportFacturacaoGestorComponent } from './components/relatorios/financeiro/report-facturacao-gestor/report-facturacao-gestor.component';
import { ContadoresComponent } from './components/contadores/contadores.component';
import { ConfEstadoServicoComponent } from './components/conf-estado-servico/conf-estado-servico.component';
import { ConfEstadoContadorComponent } from './components/conf-estado-contador/conf-estado-contador.component';
import { ConfTipoRamalComponent } from './components/conf-tipo-ramal/conf-tipo-ramal.component';
import { RamalComponent } from './components/ramal/ramal.component';
import { ConfRuaComponent } from './components/conf-rua/conf-rua.component';
import { ConfBairroComponent } from './components/conf-bairro/conf-bairro.component';
import { ConfMunicipioComponent } from './components/conf-municipio/conf-municipio.component';
import { ConfProvinciaComponent } from './components/conf-provincia/conf-provincia.component';
import { CreateOrEditMunicipioComponent } from './components/conf-municipio/create-or-edit-municipio/create-or-edit-municipio.component';
import { ConfDistritoComponent } from './components/conf-distrito/conf-distrito.component';
import { CreateOrEditBairroComponent } from './components/conf-bairro/create-or-edit-bairro/create-or-edit-bairro.component';
import { CreateOrEditRuaComponent } from './components/conf-rua/create-or-edit-rua/create-or-edit-rua.component';
import { CreateOrEditDistritoComponent } from './components/conf-distrito/create-or-edit-distrito/create-or-edit-distrito.component';
import { CreateOrEditProvinciaComponent } from './components/conf-provincia/create-or-edit-provincia/create-or-edit-provincia.component';
import { ConfTarifarioComponent } from './components/conf-tarifario/conf-tarifario.component';
import { LocalInstalacaoComponent } from './components/locais/local-instalacao/local-instalacao.component';
import { LocalConsumoComponent } from './components/locais/local-consumo/local-consumo.component';
import { CreateOrEditLocalInstalacaoComponent } from './components/locais/local-instalacao/create-or-edit-local-instalacao/create-or-edit-local-instalacao.component';
import { CreateOrEditContadoresComponent } from './components/contadores/create-or-edit-contadores/create-or-edit-contadores.component';
import { ConfTipoContratoComponent } from './components/conf-tipo-contrato/conf-tipo-contrato.component';
import { ClasseTarifarioComponent } from './components/classe-tarifario/classe-tarifario.component';
import { ProdutoclasseTarifarioComponent } from './components/produto-classe-tarifario/produto-classe-tarifario.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown-angular7';
import { BsModalService, BsModalRef, ModalDirective, ModalModule } from 'ngx-bootstrap/modal';

import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';

import { FloatLinkComponent } from './components/layout/float-link/float-link.component';
import { ListarContratoComponent } from './components/contrato/listar-contrato/listar-contrato.component';
import { ConfModeloComponent } from './components/conf-modelo/conf-modelo.component';
import { ConfMarcaComponent } from './components/conf-marca/conf-marca.component';
import { ConfMedicaoComponent } from './components/conf-medicao/conf-medicao.component';
import { ConfClassePrecisaoComponent } from './components/conf-classe-precisao/conf-classe-precisao.component';
import { ConfFabricanteComponent } from './components/conf-fabricante/conf-fabricante.component';
import { ConfTipoContadorComponent } from './components/conf-tipo-contador/conf-tipo-contador.component';
import { MapaRotaComponent } from './components/mapa-rota/mapa-rota.component';
import { ConfQuarteiraoComponent } from './components/conf-quarteirao/conf-quarteirao.component';
import { ConfiguracaoComponent } from './components/configuracao/configuracao.component';
import { TipoObjectoComponent } from './components/tipo-objecto/tipo-objecto.component';
import { LigacaoRamalComponent } from './components/ligacao-ramal/ligacao-ramal.component';
import { ObjectoLigacaoRamalComponent } from './components/objecto-ligacao-ramal/objecto-ligacao-ramal.component';
import { NaoLeituraComponent } from './components/nao-leitura/nao-leitura.component';
import { TipoNaoLeituraComponent } from './components/tipo-nao-leitura/tipo-nao-leitura.component';
import { TipoOcorrenciaComponent } from './components/tipo-ocorrencia/tipo-ocorrencia.component';
import { OcorrenciaComponent } from './components/ocorrencia/ocorrencia.component';
import { CreateOrEditQuarteiraoComponent } from './components/conf-quarteirao/create-or-edit-quarteirao/create-or-edit-quarteirao.component';
import { ConfTipoRegistoComponent } from './components/conf-tipo-registo/conf-tipo-registo.component';
import { ConfTipoOrdemServicoComponent } from './components/conf-tipo-ordem-servico/conf-tipo-ordem-servico.component';
import { ConfTipoFacturacaoComponent } from './components/conf-tipo-facturacao/conf-tipo-facturacao.component';
import { ConfEstadoContaComponent } from './components/conf-estado-conta/conf-estado-conta.component';
import { NewLigacaoRamalComponent } from './components/new-ligacao-ramal/new-ligacao-ramal.component';
import { ConfTipoLigacaoComponent } from './components/conf-tipo-ligacao/conf-tipo-ligacao.component';
import { ConfEstadoCicloFacturacaoComponent } from './components/conf-estado-ciclo-facturacao/conf-estado-ciclo-facturacao.component';
import { ContextoConfiguracaoComponent } from './components/contexto-configuracao/contexto-configuracao.component';
import { ConfEstadoTarifarioComponent } from './components/conf-estado-tarifario/conf-estado-tarifario.component';
import { ConfCaudalComponent } from './components/conf-caudal/conf-caudal.component';
import { ConfCalibreComponent } from './components/conf-calibre/conf-calibre.component';
import { ConfTipologiaClienteComponent } from './components/conf-tipologia-cliente/conf-tipologia-cliente.component';
import { ConfTiposTipologiaComponent } from './components/conf-tipos-tipologia/conf-tipos-tipologia.component';
import { ConfNivelSensibilidadeComponent } from './components/conf-nivel-sensibilidade/conf-nivel-sensibilidade.component';
import { ConfTipoProdutoComponent } from './components/conf-tipo-produto/conf-tipo-produto.component';
import { CaucaoComponent } from './components/caucao/caucao.component';

import { GeneroComponent } from './components/config-crm/generos/genero.component';
import { TipoIdentificacaoComponent } from './components/config-crm/tipo-de-identificacao/tipo-de-identificacao.component';
import { GestorContaComponent } from './components/config-crm/gestores-contas/gestor-conta.component';
import { TipoClienteComponent } from './components/config-crm/tipo-de-cliente/tipo-de-cliente.component';
import { CreateOrEditTipoClienteComponent } from './components/config-crm/tipo-de-cliente/create-or-edit-tipo-cliente/create-or-edit-tipo-cliente.component';

import { ValidationFormsService } from './providers/validation/validation-forms.service';
import { ConfTipoLocalInstalacaoComponent } from './components/conf-tipo-local-instalacao/conf-tipo-local-instalacao.component';
import { ConfTipoLocalConsumoComponent } from './components/conf-tipo-local-consumo/conf-tipo-local-consumo.component';
import { ConfTipoClienteComponent } from './components/conf-tipo-cliente/conf-tipo-cliente.component';
import { CreateOrEditConfigTarifarioComponent } from './components/conf-tarifario/create-or-edit-tarifario/create-or-edit-tarifario.component';
import { ConfCampoJardimComponent } from './components/conf-campo-jardim/conf-campo-jardim.component';
import { ConfAbastecimentoCilComponent } from './components/conf-abastecimento-cil/conf-abastecimento-cil.component';

import { ConfTipoJuroComponent } from './components/conf-tipo-juro/conf-tipo-juro.component';
import { ConfObjectoContratoComponent } from './components/conf-objecto-contrato/conf-objecto-contrato.component';

import {MatRadioModule, DateAdapter, MAT_DATE_FORMATS, MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule, MatIconModule, MatButtonModule, MatSelectModule, MatOptionModule } from "@angular/material";
import { ConfMotivoDenunciaComponent } from './components/conf-motivo-denuncia/conf-motivo-denuncia.component';
import { ConfEstadoContratoComponent } from './components/conf-estado-contrato/conf-estado-contrato.component';
import { ConfMesesMediaConsumoComponent } from './components/conf-meses-media-consumo/conf-meses-media-consumo.component';
import { ClassificacaoProdutoComponent } from './components/config.components/classificacao-produto/classificacao-produto.component';
import { CreateOrEditTipoClassificacaoComponent } from './components/config.components/classificacao-produto/create-or-edit-classificacao/create-or-edit-tipo-classificacao.component';
import { ConfTipoMedicaoComponent } from './components/conf-tipo-medicao/conf-tipo-medicao.component';
import { ConfTipoMensagemComponent } from './components/conf-tipo-mensagem/conf-tipo-mensagem.component';
import { FilterRoteiroPipe } from './providers/filters/filter.roteiros.pipe';
import { StockProdutosComponent } from './components/stock/stock-produtos/stock-produtos.component';
import { CreateOrEditArmazemComponent } from './components/stock/create-or-edit-armazem/create-or-edit-armazem.component';
import { CreateOrEditProdutosComponent } from './components/stock/create-or-edit-produtos/create-or-edit-produtos.component';
import { StockCategoriaProdutosComponent } from './components/stock/stock-categoria-produtos/stock-categoria-produtos.component';
import { StockTipoMovimentosComponent } from './components/stock/stock-tipo-movimentos/stock-tipo-movimentos.component';
import { CreateOrEditCategoriaStkComponent } from './components/stock/create-or-edit-categoria-stk/create-or-edit-categoria-stk.component';
import { CreateOrEditTipoMovimentoComponent } from './components/stock/create-or-edit-tipo-movimento/create-or-edit-tipo-movimento.component';


import { FormatDateAdapter, FORMAT_DATE_FORMATS } from "./directives/date-adapter";
import { CreateRoteiroComponent } from './components/config-rotas/rota-header/create-roteiro/create-roteiro';
import { RotaHeaderComponent } from './components/config-rotas/rota-header/rota-header.component';
import { PeriodicidadeRoteiroComponent } from './components/config-rotas/periodicidade-roteiro/periodicidade-roteiro.component';
import { RotasRoutingModule } from './components/config-rotas/rotas-routing.module';
import { AgendamentoRoteiroComponent } from './components/config-rotas/agendamento-roteiro/agendamento-roteiro.component';

import { FullCalendarModule } from '@fullcalendar/angular';
import { LeituraComponent } from "./components/config-rotas/leituras/leitura.component";
import { EntradaStockComponent } from './components/stock/entrada-stock/entrada-stock.component';
import { MovimentacaoStockComponent } from './components/stock/movimentacao-stock/movimentacao-stock.component';
import { MenuReportComponent } from './components/layout/menu-report/menu-report.component';
import { SidebarReportingComponent } from './components/layout/menu-report/sidebar-reporting/sidebar-reporting.component';
import { SaidaStockComponent } from './components/stock/saida-stock/saida-stock.component';
import { StockExistenteComponent } from './components/stock/stock-existente/stock-existente.component';
import { ConfContaBancariaComponent } from './components/conf-conta-bancaria/conf-conta-bancaria.component';
import { CreateOrEditContaBancariaComponent } from './components/conf-conta-bancaria/create-or-edit-conta-bancaria/create-or-edit-conta-bancaria.component';
import { FacturacaoDetalhadaPosPagoComponent } from './components/relatorios/financeiro/facturacao-detalhada-pos-pago/facturacao-detalhada-pos-pago.component';
import { ConfDadosFacturacaoComponent } from './components/conf-dados-facturacao/conf-dados-facturacao.component';
import { ReportResumoContaCorrenteComponent } from './components/relatorios/financeiro/report-resumo-conta-corrente/report-resumo-conta-corrente.component';
import { BancosComponent } from "./components/institucional/bancos/bancos.component";
import { ExtraccaoClientesComponent } from "./components/relatorios/cliente-relatorio/extraccao-clientes/extraccao-clientes.component";
import { ReportMovimentosCaixaComponent } from "./components/relatorios/financeiro/report-movimentos-caixa/report-movimentos-caixa.component";
import { TransferenciaComponent } from "./components/stock/transferencia/transferencia.component";
import { PagamentoParcelarFacturaComponent } from "./components/recibo/pagamento-parcelar-factura/pagamento-parcelar-factura.component";

@NgModule({
  declarations: [
    FilterRoteiroPipe,
    CreateRoteiroComponent,
    RotaHeaderComponent,
    AgendamentoRoteiroComponent,

    CreateOrEditTipoClassificacaoComponent,
    ClassificacaoProdutoComponent,
    CreateOrEditTipoClienteComponent,
    TipoClienteComponent,
    CreateOrEditConfigTarifarioComponent,
    GeneroComponent,
    TipoIdentificacaoComponent,
    GestorContaComponent,
    CobreFormComponent,
    CreateOrEditSerieComponent,
    CircuitoEmpresarialFormComponent,
    RejectFormComponent,
    PedidoCircuitoEmpresarialComponent,
    GeralFormComponent,
    FormCircuitoEmpresarialCRMComponent,
    ReportCotrancaGlobalComponent,
    AppComponent,
    routeComponents,
    LayoutComponent,
    AsidenavbarComponent,
    TopnavbarComponent,
    FooternavbarComponent,
    SetingsnavbarComponent,
    FornecedorComponent,
    UtilizadorComponent,
    ListarProdutosComponent,
    ImpostoComponent,
    DocumentoComponent,
    SerieComponent,
    ListarFacturacaoComponent,
    AlertComponent,
    ReciboComponent,
    LoadingComponent,
    SaftComponent,
    EmpresaComponent,
    StockMovimentoComponent,
    ContaCorrenteComponent,
    PermissionsComponent,
    RoleComponent,
    ModulosComponent,

    //HomeComponent,
    LoginComponent,
    RegisterComponent,

    SummaryComponent,
    DetailComponent,
    BoardComponent,
    HeaderComponent,
    ListComponent,
    ContentEditDirective,
    ContextMenuComponent,
    AprovisionamentoComponent,
    ProdutoFornecedorComponent,
    MoedaComponent,
    BancoComponent,
    ProjectoComponent,

    P404Component,
    P500Component,
    P403Component,
    TarifarioComponent,
    ContratoComponent,
    FacturacaoChargeComponent,
    PlanoPrecoComponent,
    TestPaginationComponent,
    MovimentoCaixaComponent,
    LojaComponent,
    AberturaComponent,
    FechoComponent,
    FacturacaoContaComponent,
    RedefinirPasswordComponent,
    UtilizadorAlterarPasswordComponent,
    CicloFacturacaoComponent,
    PontoVendaComponent,

    DepositoCaixaComponent,
    PrioridadeComponent,
    TipoReclamacoesComponent,
    ReclamacoesComponent,
    PedidosComponent,
    ConsultarReciboComponent,
    AdiantamentoComponent,
    ListaDepositosComponent,
    FacturacaoAutomaticaComponent,
    TecnologiasComponent,
    EstadoReclamacoesComponent,
    EstadoPedidosComponent,
    TiposPedidoComponent,

    DepositoCaixaComponent,
    PrioridadeComponent,
    TipoReclamacoesComponent,
    ReclamacoesComponent,
    PedidosComponent,
    ConsultarReciboComponent,
    AdiantamentoComponent,
    ListaDepositosComponent,
    FacturacaoAutomaticaComponent,
    TecnologiasComponent,
    EstadoReclamacoesComponent,
    EstadoPedidosComponent,
    UnificarclienteComponent,
    ClientesunificadosComponent,
    BillRunHeaderComponent,
    GetPdfEmailComponent,
    EnvioRelatorioDiarioComponent,
    ReportDiarioAutoComponent,
    CriarParceriaComponent,
    ListarParceriaComponent,
    VisaoGeralComponent,
    RelatorioFinanceiroComponent,
    EntidadeCativadoraComponent,
    VisualizarRelatorioComponent,
    RedefinirPasswordFirstAcessComponent,
    RecursosRedeComponent,
    EmitirNotaCreditoComponent,
    LeituraContradorComponent,
    ReportLojaComponent,
    ReportIVAComponent,
    ReportServicosContratadosComponent,
    DireccaoComponent,
    PedidosRelatorioComponent,
    ReclamacoesRelatorioComponent,
    CreateOrEditClientComponent,
    CreateOrEditImpostoComponent,
    CreateOrEditProdutoComponent,
    CreateOrEditTarifarioComponent,
    CreateOrEditUtilizadorComponent,
    EditFlatRateServicoComponent,
    VisualizarFlatRateComponent,
    UpdateFateRateComponent,
    ModalexportarcontacorrenteexelComponent,
    ClienteRelatorioComponent,

    DispositivosComponent,
    LtePrePagoFormComponent,
    PreFacturacaoComponent,
    WimaxDispositivoFormComponent,
    CmdaDispositivoFormComponent,
    NumeracaoInventarioRedeComponent,

    PerfilComponent,
    ReportPagamentoGlobalComponent,
    ReportFacturacaoGestorComponent,
    ContadoresComponent,
    ConfEstadoServicoComponent,
    ConfEstadoContadorComponent,
    ConfTipoRamalComponent,
    RamalComponent,
    ConfRuaComponent,
    ConfBairroComponent,
    ConfMunicipioComponent,
    ConfProvinciaComponent,
    CreateOrEditMunicipioComponent,
    ConfDistritoComponent,
    CreateOrEditBairroComponent,
    CreateOrEditRuaComponent,
    CreateOrEditDistritoComponent,
    CreateOrEditProvinciaComponent,
    ConfTarifarioComponent,
    LocalInstalacaoComponent,
    LocalConsumoComponent,
    CreateOrEditLocalInstalacaoComponent,
    CreateOrEditContadoresComponent,
    ConfTipoContratoComponent,
    ClasseTarifarioComponent,
    ProdutoclasseTarifarioComponent,
    FloatLinkComponent,
    ListarContratoComponent,
    LeituraComponent,
    ConfModeloComponent,
    ConfMarcaComponent,
    ConfMedicaoComponent,
    ConfClassePrecisaoComponent,
    ConfFabricanteComponent,
    ConfTipoContadorComponent,
    MapaRotaComponent,
    ConfQuarteiraoComponent,
    ConfiguracaoComponent,
    TipoObjectoComponent,
    LigacaoRamalComponent,
    ObjectoLigacaoRamalComponent,
    NaoLeituraComponent,
    TipoNaoLeituraComponent,
    TipoOcorrenciaComponent,
    OcorrenciaComponent,
    CreateOrEditQuarteiraoComponent,
    OcorrenciaComponent,
    ConfTipoRegistoComponent,
    ConfTipoOrdemServicoComponent,
    ConfTipoFacturacaoComponent,
    ConfEstadoContaComponent,
    NewLigacaoRamalComponent,
    ConfTipoLigacaoComponent,
    ConfEstadoCicloFacturacaoComponent,
    ContextoConfiguracaoComponent,
    ConfEstadoTarifarioComponent,
    ConfCaudalComponent,
    ConfCalibreComponent,
    ConfTipologiaClienteComponent,
    ConfTiposTipologiaComponent,
    ConfNivelSensibilidadeComponent,
    ConfTipoProdutoComponent,
    CaucaoComponent,
    GestorContaComponent,
    ConfTipoLocalInstalacaoComponent,
    ConfTipoLocalConsumoComponent,
    ConfTipoClienteComponent,
    ConfCampoJardimComponent,
    ConfAbastecimentoCilComponent,
    ConfTipoJuroComponent,
    ConfObjectoContratoComponent,
    ConfMotivoDenunciaComponent,
    ConfEstadoContratoComponent,
    ConfMesesMediaConsumoComponent,
    ConfTipoMedicaoComponent,
    ConfTipoMensagemComponent,
    StockProdutosComponent,
    CreateOrEditArmazemComponent,
    CreateOrEditProdutosComponent,
    StockCategoriaProdutosComponent,
    StockTipoMovimentosComponent,
    CreateOrEditCategoriaStkComponent,
    CreateOrEditTipoMovimentoComponent,
    PeriodicidadeRoteiroComponent,
    EntradaStockComponent,
    MovimentacaoStockComponent,
    MenuReportComponent,
    SidebarReportingComponent,
    SaidaStockComponent,
    StockExistenteComponent,
    PagamentoParcelarFacturaComponent,
    ExtraccaoClientesComponent,
    TransferenciaComponent,
    ReportMovimentosCaixaComponent,
    ConfContaBancariaComponent,
    CreateOrEditContaBancariaComponent,
    FacturacaoDetalhadaPosPagoComponent,
    ConfDadosFacturacaoComponent,
    ReportResumoContaCorrenteComponent,
    BancosComponent
  ],
  imports: [
    MatRadioModule,
    RotasRoutingModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    ModalModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 15000,
      progressBar: true,
      progressAnimation: "increasing",
      preventDuplicates: true,
      newestOnTop: true,
      closeButton: true,
      maxOpened: 1,
    }),
    NgxMaskModule.forRoot(options),
    ArchwizardModule,
    Select2Module,
    NgSelectModule,
    NgxSpinnerModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgxLoadingModule.forRoot({
      backdropBorderRadius: "3px",
      backdropBackgroundColour: "rgba(255, 255, 255, 0.78)",
      primaryColour: "#20a8d8",
      secondaryColour: "#20a8d8",
      tertiaryColour: "#20a8d8",
    }),
    // Specify ng-circle-progress as an import
    NgCircleProgressModule.forRoot({
      backgroundGradient: true,
      backgroundColor: "#ffffff",
      backgroundGradientStopColor: "#c0c0c0",
      backgroundPadding: -10,
      radius: 60,
      maxPercent: 100,
      outerStrokeWidth: 10,
      outerStrokeColor: "#61A9DC",
      innerStrokeWidth: 0,
      subtitleColor: "#444444",
      showInnerStroke: false,
      startFromZero: false,
    }),
    ProgressBarModule,
    NgMultiSelectDropDownModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyDUfCKari6bAJFFAvToSFQq8KKoCSXPT9Y",
    }),
    AgmJsMarkerClustererModule,
    ProgressBarModule,
    NgMultiSelectDropDownModule.forRoot(),
    FullCalendarModule,
    RxReactiveFormsModule
  ],
  exports: [
    LayoutComponent,
    AsidenavbarComponent,
    TopnavbarComponent,
    FooternavbarComponent,
    SetingsnavbarComponent,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErroInterceptor, multi: true },
    {
      provide: DateAdapter,
      useClass: FormatDateAdapter,
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: FORMAT_DATE_FORMATS,
    },
    HttpService,
    NgxImageCompressService,
    ApiService,
    AuthService,
    ConfigService,
    LocalService,
    PedidoReportService,
    DiarioVendasService,
    ExcelService,
    ConfigModuloService,
    RelCobracaGlobalService,
    ReportLojaService,
    ReportIVAService,
    ReportClienteService,
    ReportServicoContradosService,
    ValidationFormsService
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
