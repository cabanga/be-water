import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfMedicaoComponent } from './conf-medicao.component';

describe('ConfMedicaoComponent', () => {
  let component: ConfMedicaoComponent;
  let fixture: ComponentFixture<ConfMedicaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfMedicaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfMedicaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
