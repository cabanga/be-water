import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfDadosFacturacaoComponent } from './conf-dados-facturacao.component';

describe('ConfDadosFacturacaoComponent', () => {
  let component: ConfDadosFacturacaoComponent;
  let fixture: ComponentFixture<ConfDadosFacturacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfDadosFacturacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfDadosFacturacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
