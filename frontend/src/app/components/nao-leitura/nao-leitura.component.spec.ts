import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NaoLeituraComponent } from './nao-leitura.component';

describe('NaoLeituraComponent', () => {
  let component: NaoLeituraComponent;
  let fixture: ComponentFixture<NaoLeituraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NaoLeituraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NaoLeituraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
