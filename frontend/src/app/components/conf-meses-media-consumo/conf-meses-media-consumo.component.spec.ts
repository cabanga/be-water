import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfMesesMediaConsumoComponent } from './conf-meses-media-consumo.component';

describe('ConfMesesMediaConsumoComponent', () => {
  let component: ConfMesesMediaConsumoComponent;
  let fixture: ComponentFixture<ConfMesesMediaConsumoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfMesesMediaConsumoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfMesesMediaConsumoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
