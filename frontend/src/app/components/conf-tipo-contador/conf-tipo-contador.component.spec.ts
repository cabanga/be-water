import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoContadorComponent } from './conf-tipo-contador.component';

describe('ConfTipoContadorComponent', () => {
  let component: ConfTipoContadorComponent;
  let fixture: ComponentFixture<ConfTipoContadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoContadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoContadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
