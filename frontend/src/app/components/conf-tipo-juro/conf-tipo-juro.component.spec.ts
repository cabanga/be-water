import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoJuroComponent } from './conf-tipo-juro.component';

describe('ConfTipoJuroComponent', () => {
  let component: ConfTipoJuroComponent;
  let fixture: ComponentFixture<ConfTipoJuroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoJuroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoJuroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
