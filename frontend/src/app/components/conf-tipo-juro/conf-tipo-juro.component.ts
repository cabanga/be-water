import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

@Component({​
    selector: 'app-conf-tipo-juro',
    templateUrl: './conf-tipo-juro.component.html',
    styleUrls: ['./conf-tipo-juro.component.css']
  }​)
  export class ConfTipoJuroComponent implements OnInit {​

  private tipojuro = {
    id: null,
    dias: null,
    valor_dias: null,
    percentagem: null,
    slug: null
  }


  private items: any = [];

  constructor(private http: HttpService, private configService: ConfigService) { }

  ngOnInit() {
    this.getPageFilterData(1);
  }


  private listatipojuro() {

    this.configService.loaddinStarter('start');

    this.http.__call('tipo-juro/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.listatipojuro();
  }


  private register(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    
    if (this.tipojuro.dias == null) {
      this.configService.showAlert("O campo Dia é obrigatório", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
    this.http.__call('tipo-juro/create', this.tipojuro).subscribe(
      res => {
        if (Object(res).code == 201) {
          this.configService.showAlert(Object(res).message, 'alert-danger', true);
          this.configService.loaddinStarter('stop');
        } else {
          this.configService.showAlert(Object(res).message, 'alert-success', true);
          this.clearFormInputs(e);
          this.listatipojuro()
          this.configService.loaddinStarter('stop');
        }
      }
    )
  };
}

  private clearFormInputs(e) {
    e.target.elements[0].value = null;
    e.target.elements[1].value = null;
    e.target.elements[2].value = null;
  }

  private refresh(id, dias, valor_dias, percentagem, slug) {
    this.tipojuro.id = id;
    this.tipojuro.dias = dias;
    this.tipojuro.valor_dias = valor_dias;
    this.tipojuro.percentagem = percentagem;
    this.tipojuro.slug = slug;
  }

  private editar(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();

    if (this.tipojuro.dias == null) {
      this.configService.showAlert("O campo Dia é obrigatório", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
      this.http.__call('tipo-juro/update/' + this.tipojuro.id, this.tipojuro).subscribe(
        res => {
          if (Object(res).code == 201) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
          } else {
            //this.configService.clearFormInputs(e);
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.listatipojuro();

          }
        }
      );
    }
    this.configService.loaddinStarter('stop');
  }


  private ini() {
    this.tipojuro = {
      id: null,
      dias: null,
      valor_dias: null,
      percentagem: null,
      slug: null
    }
  }





}
