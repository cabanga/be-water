import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FechoComponent } from './fecho.component';

describe('FechoComponent', () => {
  let component: FechoComponent;
  let fixture: ComponentFixture<FechoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FechoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FechoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
