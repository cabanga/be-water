import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntidadeCativadoraComponent } from './entidade-cativadora.component';

describe('EntidadeCativadoraComponent', () => {
  let component: EntidadeCativadoraComponent;
  let fixture: ComponentFixture<EntidadeCativadoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntidadeCativadoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntidadeCativadoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
