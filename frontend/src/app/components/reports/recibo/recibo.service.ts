import { Injectable, Input, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/providers/http/api.service';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Injectable({
  providedIn: 'root'
})
export class ReciboService {

  @Input() app_environment: null;

  constructor(private http: ApiService, private auth: AuthService) {
    this.app_environment = this.auth.getAppEnvironment();
    if (this.app_environment == null) {
      var url = require('url');
      var app_url = url.parse(environment.app_url, true);
      this.app_environment = app_url.host;
    }
  }

  public imprimirRecibo(id, via = 'Original', report = 'imprimir') {
    this.http.get('recibos/printerRecibo/' + id).subscribe(
      response => {
        const dados = Object(response).data;

        this.imprimirPDFRecibo(
          dados.recibo,
          dados.factura,
          dados.cliente,
          dados.tipos_identidades,
          dados.user,
          dados.linha_pagamentos,
          dados.contrato,
          dados.leituras,
          dados.contas_bancarias,
          dados.lojas,
          report,
          via
        )
      }, error => {

      }
    );
  }

  public imprimirPDFRecibo(
    recibo: any,
    facturas: any[],
    cliente: any,
    tipos_identidades: any,
    user: any,
    pagamentos: any,
    contrato: any,
    leituras: [],
    contas_bancarias: any,
    lojas: any,
    report: string = 'imprimir',
    original: any = 'Original'
  )
  {
    var numberPage = 1;
    var img_logotipo = user.logotipo;
    var doc = new jsPDF();
    doc.addImage(img_logotipo, 'JPEG', 8, 14, 28, 24)

    doc.setFontSize(8);
    doc.setFont("calibri");
    doc.setTextColor(0);
    doc.text('' + user.companyName, 37, 20);
    doc.setTextColor(0);
    doc.text('' + user.addressDetail, 37, 25);
    doc.text('NIF: ' + user.taxRegistrationNumber, 37, 30);
    doc.text('Email: ' + user.email, 37, 35);
    doc.text('Telefone: ', 95, 30);
    doc.text('' + user.telefone, 107, 30);
    doc.text('WebSite: ', 95, 35);
    doc.setTextColor(0, 0, 255);
    doc.text(''+ (user.site == null ? '' : user.site), 107, 35);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(145, 14, 48, 23, 'B');

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('RECIBO N.º', 148, 20);
    doc.text('Data Emissão:', 148, 25);
    doc.text('Via: ' + original, 148, 35);

    doc.setFontSize(8);
    doc.setFontType("normal");
    doc.text('' + recibo.recibo_sigla, 168, 20);
    doc.text('' + moment(recibo.created_at).format("DD/MM/YYYY"), 168, 25);

    doc.setFontSize(8);
    doc.setFont("calibri");
    doc.setFontType("normal");
    doc.text(cliente.nome.trimStart(), 120, 52);
    doc.text(cliente.morada, 120, 57);

    let desc_municipio = cliente.municipio ? cliente.municipio.nome : ''
    doc.text('' + desc_municipio, 120, 62);
    /*
    doc.text(cliente.nome.trimStart(), 148, 52);
    doc.text(cliente.morada, 148, 57);
    */

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, 75, 40.5, 3.5, 'B');
    doc.setFontType("bold");
    doc.text('CLIENTE N.º: ' + cliente.id, 13, 77.5);
    doc.text(cliente.nome.trimStart(), 13, 83);
    doc.text('NIF: ', 13, 87);
    doc.setFontType("normal");
    let nif = tipos_identidades.find(obj => obj.nome == 'NIF')
    doc.text('' + (nif ? nif.numero_identidade : ""), 21, 87);

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, 100, 182, 5, 'B');
    doc.setFontSize(8);
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('RECIBO  N.º', 13, 103.5);
    doc.text(' ' + recibo.recibo_sigla + ' de  ' + moment(recibo.created_at).format("DD/MM/YYYY"), 32, 103.5);
    doc.setFontType("normal");

    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(0.1);
    doc.line(12, 104.5, 194, 104.5); // horizontal line

    doc.setFontType("bold");
    doc.text('Nº Factura', 13, 108);
    doc.text('Valor Imposto.', 60, 108);
    doc.text('Valor Em Aberto', 90, 108);
    doc.text('Total a Pagar', 140, 108);
    doc.text('Estado Factura', 175, 108);
    doc.setTextColor(0);
    doc.setFontType("normal");

    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(0.1);
    doc.line(12, 109, 194, 109); // horizontal line
    doc.setTextColor(0);

    var posY = 115;
    var totalImposto = 0;

    for (var i = 0; i < facturas.length; i++) {
      const element = facturas[i];

      doc.text('' + element.factura_sigla, 13, posY);
      doc.text('' + element.totalComImposto + " AOA", 78, posY, 'right');
      doc.text('' + this.numberFormat((element.novo_valor_aberto == null ? element.valor_aberto : element.novo_valor_aberto)) + " AOA", 110, posY, 'right');
      doc.text('' + this.numberFormat(element.total) + " AOA", 155, posY, 'right');
      doc.text('' + (element.novo_valor_aberto == null ? (element.pago == 0 ? 'Não Saldado' : 'Saldado') : (element.novo_valor_aberto == 0 ? 'Saldado' : 'Não Saldado')), 193, posY, 'right');
      posY += 5;

      totalImposto += element.valor_imposto;
      const hasReachedPageLimit = doc.internal.pageSize.height < (posY + 20)

      if (hasReachedPageLimit) {
        posY = 10;
        doc.addPage();
      }
    }


    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(0.1);
    doc.line(12, posY + 4.5, 194, posY + 4.5); // horizontal line

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, posY + 5, 182, 5, 'B');
    doc.text('TOTAL RECIBO', 13, posY + 8);
    doc.text('' + this.numberFormat(recibo.total) + " AOA", 155, posY + 8, 'right');

    //RESUMO DOS IMPOSTOS

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, posY + 22, 182, 5, 'B');
    doc.text('MEIOS DE PAGAMENTOS', 13, posY + 25);
    doc.text('DESCRIÇÃO', 13, posY + 30);
    doc.text('VALOR RECEBIDO', 155, posY + 30, 'right');
    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(0.1);
    doc.line(12, posY + 31, 194, posY + 31); // horizontal line
    var posYLinhaPagamento = posY + 1;
    pagamentos.forEach(pagament => {
      doc.text('' + pagament.designacao, 13, posYLinhaPagamento + 35);
      doc.text('' + (pagament.designacao == "Numerário" ? this.numberFormat(pagament.valor_recebido - recibo.troco) : this.numberFormat(pagament.valor_recebido)) + " AOA", 155, (posYLinhaPagamento + 35), 'right');
      posYLinhaPagamento += 4
    })


    posYLinhaPagamento += 18
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, posYLinhaPagamento + 25, 182, 5, 'B');
    doc.text('TOTAL RECEBIDO', 13, posYLinhaPagamento + 28);
    doc.text('' + this.numberFormat(recibo.total_pago) + " AOA", 155, posYLinhaPagamento + 28, 'right');

    // END RESUMO DOS IMPOSTOS
    posY = 130;

    //ANULAÇÃO
    if (recibo.status == 'A') {
      doc.setDrawColor(0);
      doc.setFillColor(220, 220, 220);
      doc.rect(10, posY + 99, 52, 5, 'B');
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.text('Estado: ', 12, posY + 102.5);
      doc.setTextColor('red');
      doc.text('Anulação', 24, posY + 102.5);
      doc.setTextColor(0);
    }

    //REGIME DE IMPOSTO
    doc.setDrawColor(0);
    doc.setFillColor(220, 220, 220);
    doc.rect(160, posY + 99, 35, 5, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Regime Simplificado', 192, posY + 102.5, 'right');
    doc.setTextColor(0);

    doc.setFontType("bold")
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(10, posY + 106, 185.5, 5, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");

    doc.setDrawColor(0);
    doc.rect(10, posY + 105, 185, 20, 'B');

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);

    doc.setTextColor(0);
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.text('Contas Bancárias (IBAN):', 12, posY + 109.5);
    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(10, posY + 111, 185, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("normal");
    
      var posX = 12;
    for(let conta of contas_bancarias){
      doc.text('' + conta.banco, posX, posY + 115);
      doc.text('' + conta.iban, posX + 7, posY + 115);
      posX += 60;
    } 

/*     var posX = 12;
    for (var i = 0; i < contas_bancarias.length; i++) {

      doc.text('' + contas_bancarias[i].banco, posX, posY + 105);
      doc.text('' + contas_bancarias[i].iban, posX + 7, posY + 105);

      posX += 60;
    }  */
    

    //======================================================================================================================

/*     doc.text('BPC', 12, posY + 115);
    doc.text('AO06 0010 0505 0253832801194', 19, posY + 115);
    doc.text('SOL', 65, posY + 115);
    doc.text('AO06 0044 0000 0500085210185', 72, posY + 115);
    doc.text('BAI', 122, posY + 115);
    doc.text('AO06 0040 0000 4375910310161', 130, posY + 115); */
    doc.text('Sempre que efectuar uma transferência ou depósito, deve indicar o número das recibos a liquidar.', 12, posY + 125.7);
    //======================================================================================================================


    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Lojas EPASNAMIBE:', 10, posY + 135);
    doc.setFontType("normal");

    //======================================================================================================================
/*     doc.text('Av. Eduardo Mondlane, n.º 139 (de segunda a sexta-feira das 8h às 15h | sábado das 8h às 12h)', 10, posY + 143);
    doc.text('Centralidade 5 de Abril, Bloco H100, Apartamento 2 (de segunda a sexta-feira das 8h às 12h)', 10, posY + 146);
    doc.text('Centralidade Praia Amélia, Bloco K1, Apartamento 2 (de segunda a sexta-feira das 8h às 12h)', 10, posY + 149); */
    //======================================================================================================================

    
    for(let loja of lojas){
      doc.text('' + loja.nome + ', '+ loja.endereco + ', ' + (loja.horario ? loja.horario : ''),11, posY + 138);
      posY += 3;
    }


    doc.text(moment(recibo.created_at).format("DD/MM/YYYY") + ' ' + moment(recibo.created_at).format("H:m") + '/ Versão: ' + original, 11, 285);
    doc.text('Página: ' + numberPage, 184, 285);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(0);


    doc.line(10, doc.internal.pageSize.height - 9, 194, doc.internal.pageSize.height - 9); // vertical line
    var hash = recibo.hash.substring(0, 1);
    hash += recibo.hash.substring(10, 11);
    hash += recibo.hash.substring(20, 21);
    hash += recibo.hash.substring(30, 31);
    doc.setFontSize(6);

    doc.text("NIF: " + user.taxRegistrationNumber + " - " + user.companyName + " / " + user.addressDetail + " / " + user.telefone + " / " + user.email, 105, doc.internal.pageSize.height - 6, null, null, 'center');

    doc.setFontSize(8);
    doc.text(hash + '-Processado por programa validado nº 4/AGT119', 105, doc.internal.pageSize.height - 3, null, null, 'center');

    if (report == 'imprimir') {
      doc.autoPrint();
      window.open(doc.output("bloburl")); //opens the data uri in new window
      // doc.output("dataurlnewwindow"); //opens the data uri in new window
    } else {
      doc.save(recibo.recibo_sigla + '.pdf'); /* download the file immediately on loading */
    }

  }
  public numberFormat(number) {
    return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace('€', '').trim();
  }
}
