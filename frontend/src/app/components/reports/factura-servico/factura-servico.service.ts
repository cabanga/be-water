import { Injectable, Input, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/providers/http/api.service';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { count } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FacturaServicoService {

  @Input() app_environment: null;

  public pdfEvent = new EventEmitter<Object>();

  constructor(private auth: AuthService) {
    this.app_environment = this.auth.getAppEnvironment();

    if (this.app_environment == null) {
      var url = require('url');
      var app_url = url.parse(environment.app_url, true);
      this.app_environment = app_url.host;
    }
  }

  public imprimirFacturaServico(
    factura: any,
    produtos: any[],
    cliente: any,
    tipos_identidades: any,
    user: any,
    pagamento: any,
    contrato: any,
    leituras: [],
    contabancaria: any[],
    lojas: any[],
    report: string = 'imprimir',
    original: any = 'Original'
  )
  {
    var numberPage = 1;
    var posY = 125;
    var doc = new jsPDF();

    var img_logotipo = user.empresa.logotipo;
    var doc = new jsPDF();
    doc.addImage(img_logotipo, 'JPEG', 8, 14, 28, 24)

    doc.setFontSize(8);
    doc.setFont("calibri");
    doc.setTextColor(0);
    doc.text('' + user.empresa.companyName, 37, 20);
    doc.setTextColor(0);
    doc.text('' + user.empresa.addressDetail, 37, 25);
    doc.text('NIF: ' + user.empresa.taxRegistrationNumber, 37, 30);
    doc.text('Email: ' + user.empresa.email, 37, 35);
    doc.text('Telefone: ', 95, 30);
    doc.text('' + user.empresa.telefone, 107, 30);
    doc.text('WebSite: ', 95, 35);
    doc.setTextColor(0, 0, 255);
    doc.text('' + (user.empresa.site == null ? '' : user.empresa.site), 107, 35);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);

    doc.rect(145, 14, 48, 23, 'B');

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('FACTURA N.º', 148, 20);
    doc.text('Data Emissão:', 148, 25);
    doc.text('Vencimento:', 148, 30);
    doc.text('Via: ' + original, 148, 35);

    doc.setFontSize(8);
    doc.setFontType("normal");
    doc.text('' + factura.factura_sigla, 168, 20);
    doc.text('' + moment(factura.created_at).format("DD/MM/YYYY"), 168, 25);
    doc.text('' + (factura.data_vencimento == null ? '' : moment(factura.data_vencimento).format('DD/MM/YYYY')), 168, 30);
    doc.setFontSize(8);
    doc.setFont("calibri");
    doc.setFontType("normal");
    doc.text(cliente.nome.trimStart(), 120, 52);
    doc.text(cliente.morada, 120, 57);
    doc.text('' + cliente.municipio.nome, 120, 62);

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, 75, 40.5, 3.5, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setFont("calibri");
    doc.text('CONTRATO N.º: ' + (contrato == null || contrato == "null" ? "" : contrato.id), 13, 77.5);
    doc.text('CIL: ' + (contrato == null ? "" : contrato.localconsumo == null ? '' : contrato.localconsumo.cil), 13, 83);
    doc.text('Morada Local de Consumo:', 13, 88);
    doc.setFontType("normal");

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(85, 75, 34.5, 3.5, 'B');
    doc.setFontType("bold");
    doc.text('CLIENTE N.º: ' + cliente.id, 86, 77.5);
    doc.text('NIF: ', 86, 88);
    doc.text('Tarifa: ', 86, 93);
    doc.setFontType("normal");
    doc.text(cliente.nome.trimStart(), 86, 83);


    let nif = tipos_identidades.find(obj => obj.nome == 'NIF')
    doc.text('' + (nif ? nif.numero_identidade : ""), 92, 88); 
    doc.text('' + (contrato == null ? "" : contrato.tarifa == null ? '' : contrato.tarifa.descricao), 96, 93);

    var totalNumberPage = 1;
    totalNumberPage=Math.ceil(produtos.length/8);

    header();

    doc.setFontSize(8);
    doc.setTextColor(0);

    var totalImposto = 0;
    var totalIncidencia = 0;
    var count_prod = 0;
    var total_transporte = 0;

    footer();
    posY = 115

    for (var i = 0; i < produtos.length; i++) {
      const data = produtos[i];
      doc.setFontSize(8);
      doc.text('' + data.produto.nome, 13, posY);
      doc.setFontSize(7);
      doc.text('' + data.quantidade, 80, posY, 'right');
      doc.text('' + (data.produto.incidencia == null ? '' : data.produto.incidencia.abreviacao), 89, posY, 'right');
      doc.text('' + numberFormat(data.valor) + " AOA", 109, posY, 'right');
      doc.text('' + numberFormat((data.quantidade * data.valor)) + " AOA", 133, posY, 'right');
      doc.text('' + (produtos[0].imposto.codigo == null ? 'IVA (14%)' : produtos[0].imposto.codigo), 151, posY, 'right');
      doc.text('' + numberFormat(data.valor_imposto) + " AOA", 170, posY, 'right');
      doc.text('' + numberFormat(data.total) + " AOA", 193, posY, 'right');

      doc.text('' + (data.produto.incidencia == null ? '' : data.produto.incidencia.abreviacao), 89, posY, 'right');
      doc.text('' + numberFormat(data.valor) + " AOA", 109, posY, 'right');
      doc.text('' + numberFormat((data.quantidade * data.valor)) + " AOA", 133, posY, 'right');
      doc.text('' + (produtos[0].imposto.codigo == null ? 'IVA (14%)' : produtos[0].imposto.codigo), 151, posY, 'right');
      doc.text('' + numberFormat(data.valor_imposto) + " AOA", 170, posY, 'right');
      doc.text('' + numberFormat(data.total) + " AOA", 193, posY, 'right');
      posY += 5;

      total_transporte += data.total;
      totalImposto += data.valor_imposto;
      totalIncidencia += data.quantidade * data.valor;

      count_prod++;

      if (count_prod == 8) {
        if (numberPage == 1 && count_prod == 8) {
          //total_transporte = 0; //SEG 01 = 0  SEG 02 = 0
          posY += 5;
          total();
          //total_transporte = 0; //SEG 01 = 98715  SEG 02 = 0
        }
        numberPage++;
        newPage(count_prod);
        posY += 10;
        //total_transporte = 0; //SEG 01 = 98715  SEG 02 = 0
        total();
        //total_transporte = 0; //SEG 01 = 98715  SEG 02 = 98715
        count_prod = 0;
        posY -= 30;
      } else {
        posY += 5;
      }
    }

    doc.text('' + numberFormat(totalImposto), 175, posY + 10);
    doc.setFontSize(8);
    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(0.1);
    doc.line(12, posY + 4.5, 194, posY + 4.5);

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, posY + 5, 182, 5, 'B');
    doc.text('TOTAL FACTURA', 13, posY + 8);
    doc.text('' + numberFormat(factura.total) + " AOA", 193, posY + 8, 'right');

    //======================= RESUMO DOS IMPOSTOS =======================

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, posY + 22, 182, 5, 'B');
    doc.text('RESUMO DOS IMPOSTOS', 13, posY + 25);
    doc.text('CÓDIGO', 13, posY + 30);
    doc.text('INCIDÊNCIA', 52, posY + 30);
    doc.text('DESCRIÇÃO', 85, posY + 30);
    doc.text('VALOR IMPOSTO', 115, posY + 30);
    doc.setDrawColor(0, 0, 0);
    doc.setLineWidth(0.1);
    doc.line(12, posY + 31, 194, posY + 31); // horizontal line
    doc.text('' + (produtos[0].imposto.codigo == null ? 'IVA' : produtos[0].imposto.codigo), 13, posY + 35);
    doc.text('' + numberFormat(factura.totalSemImposto) + " AOA", 68, posY + 35, 'right');// adicionar o valor total sem o IVA
    doc.text('' + (produtos[0].imposto.descricao == null ? 'IVA (14%)' : produtos[0].imposto.descricao), 80, posY + 35);
    doc.text('' + numberFormat(factura.totalComImposto) + " AOA", 138, posY + 35, 'right');

    // END RESUMO DOS IMPOSTOS

    var somaY = 0;

    //======================= LEITURA =======================
    if (factura.leitura != null) {
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.setDrawColor(250);
      doc.setFillColor(220, 220, 220);
      doc.rect(12, posY + 41, 182, 5, 'B');
      doc.text('LEITURA: ' + (factura.leitura == null ? '' : factura.leitura), 13, posY + 45);

      somaY = 4;
    }

    // ======================= OBSERVAÇÃO =======================
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, posY + 45 + somaY, 182, 5, 'B');
    doc.text('OBSERVAÇÃO: ' + (factura.observacao == null ? '' : factura.observacao), 13, posY + 49 + somaY);
    doc.setFontType("bold")


    if (report == 'imprimir') {
      doc.autoPrint();
      window.open(doc.output("bloburl")); //opens the data uri in new window
    } else {
      doc.save(factura.factura_sigla + '.pdf'); /* download the file immediately on loading */
    }

    function newPage(count) {
      doc.addPage();
      header();
      footer();

    }

    function total() {
      doc.setFontSize(6);
      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.1);
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.setDrawColor(250);
      doc.setFillColor(220, 220, 220);
      doc.rect(12, posY +2, 182, 5, 'B');
      doc.text('SUBTOTAL', 13, posY + 5);
      doc.text('' + numberFormat(total_transporte) + " AOA", 193, posY + 5, 'right');
      doc.setFontType("normal");
    }

    function footer() {
      posY = 140;

      var somaY = 0;

      //MEIOS DE PAGAMENTOS
      if (factura.serie.documento.sigla === 'FR') {
        posY += 20
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.setDrawColor(250);
        doc.setFillColor(220, 220, 220);
        doc.rect(12, posY + 22, 182, 5, 'B');
        doc.text('MEIOS DE PAGAMENTOS', 13, posY + 25);
        doc.text('DESCRIÇÃO', 13, posY + 30);
        doc.text('VALOR RECEBIDO', 155, posY + 30, 'right');
        doc.setDrawColor(0, 0, 0);
        doc.setLineWidth(0.1);
        doc.line(12, posY + 31, 194, posY + 31); // horizontal line
        var posYLinhaPagamento = posY + 1;
        pagamento.lines.forEach(pagament => {
          doc.text('' + pagament.forma_pagamento.designacao, 13, posYLinhaPagamento + 35);
          doc.text('' + (pagament.forma_pagamento.designacao == "Numerário" ? numberFormat(pagament.valor_recebido - pagamento.troco) : numberFormat(pagament.valor_recebido)) + " AOA", 155, (posYLinhaPagamento + 35), 'right');
          posYLinhaPagamento += 4
        })
      }
      ///////////////////////////////////////////////////

      //REGIME DE IMPOSTO
      //doc.text('Regime Simplificado', 12, posY + 90, 'right');
      posY = 135;
      doc.setDrawColor(0);
      doc.setFillColor(220, 220, 220);
      doc.rect(160, posY + 89, 35, 5, 'B');
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.text('Regime Simplificado', 192, posY + 92.5, 'right');
      doc.setTextColor(0);
      doc.setDrawColor(250);
      doc.setFillColor(220, 220, 220);
      doc.rect(10, posY + 106, 185.5, 5, 'B');
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.setDrawColor(0);
      doc.rect(10, posY + 95, 185, 20, 'B');
      doc.setDrawColor(250);
      doc.setFillColor(220, 220, 220);
      doc.setTextColor(0);
      doc.setDrawColor(250);
      doc.setFillColor(220, 220, 220);
      doc.text('Contas Bancárias (IBAN):', 12, posY + 99.5);
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.setFontSize(8);
      doc.setDrawColor(0);
      doc.setFillColor(255);
      doc.rect(10, posY + 101, 185, 20, 'B');
      doc.setTextColor(0);
      doc.setFontType("normal");

      //===================================================================================================
 /*      doc.text('BPC', 12, posY + 105);
      doc.text('AO06 0010 0505 0253832801194', 19, posY + 105);
      doc.text('SOL', 65, posY + 105);
      doc.text('AO06 0044 0000 0500085210185', 72, posY + 105);
      doc.text('BAI', 122, posY + 105);
      doc.text('AO06 0040 0000 4375910310161', 130, posY + 105); */
      //===================================================================================================

      
      var posX = 12;
      for (var i = 0; i < contabancaria.length; i++) {
        doc.text('' + contabancaria[i].banco, posX, posY + 105);
        doc.text('AO06 ' + contabancaria[i].iban, posX + 7, posY + 105);
        posX += 60;
      }


      doc.text('Sempre que efectuar uma transferência ou depósito, deve indicar o número das facturas a liquidar.', 12, posY + 115.7);
      doc.setDrawColor(250);
      doc.setFillColor(220, 220, 220);
      doc.rect(10, posY + 124.5, 52, 5, 'B');
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.text('Data Limite de Pagamento: ' + (factura.data_vencimento == null ? '' : moment(factura.data_vencimento).format('DD/MM/YYYY')), 11, posY + 128);

      doc.text('Lojas EPASNAMIBE:', 10, posY + 135);
      //===================================================================================================
/*       doc.setFontType("normal");
      doc.text('Av. Eduardo Mondlane, n.º 139 (de segunda a sexta-feira das 8h às 15h | sábado das 8h às 12h)', 11, posY + 138);
      doc.text('Centralidade 5 de Abril, Bloco H100, Apartamento 2 (de segunda a sexta-feira das 8h às 12h)', 11, posY + 141);
      doc.text('Centralidade Praia Amélia, Bloco K1, Apartamento 2 (de segunda a sexta-feira das 8h às 12h)', 11, posY + 144); */
      //===================================================================================================

      doc.setFontType("normal");

      for (let loja of lojas) {
        doc.text('' + loja.nome + ', ' + loja.endereco + ', ' + (loja.horario ? loja.horario : ''), 11, posY + 138);
        posY += 3;
      }

      doc.text(moment(factura.datahora_emissao).format("DD/MM/YYYY") + ' ' + moment(factura.datahora_emissao).format("H:m") + '/ Versão: ' + original, 11, 285);
      doc.text('Página: ' + numberPage + '/' + totalNumberPage, 180, 285);

      doc.setFontSize(8);
      doc.setDrawColor(0);
      doc.setFillColor(0);


      doc.line(10, doc.internal.pageSize.height - 9, 194, doc.internal.pageSize.height - 9); // vertical line
      var hash = factura.hash.substring(0, 1);
      hash += factura.hash.substring(10, 11);
      hash += factura.hash.substring(20, 21);
      hash += factura.hash.substring(30, 31);
      doc.setFontSize(6);

      doc.text("NIF: " + user.empresa.taxRegistrationNumber + " - " + user.empresa.companyName + " / " + user.empresa.addressDetail + " / " + user.empresa.telefone + " / " + user.empresa.email, 105, doc.internal.pageSize.height - 6, null, null, 'center');
      doc.setFontSize(7);
      doc.text(hash + '-Processado por programa validado nº 4/AGT119', 105, doc.internal.pageSize.height - 3, null, null, 'center');

      if (factura.status == 'A') {
        doc.setDrawColor(0);
        doc.setFillColor(220, 220, 220);
        doc.rect(10, posY + 89, 52, 5, 'B');
        doc.setTextColor(0);
        doc.setFontType("bold");
        doc.text('Estado: ', 12, posY + 92.5);
        doc.setTextColor('red');
        doc.text('Anulação', 24, posY + 92.5);
        doc.setTextColor(0);
      }
    }

    function header() {
      doc.setProperties({
        title: '' + factura.factura_sigla,
        subject: '' + factura.serie.documento.nome,
        author: 'Unig',
        keywords: '',
        creator: '' + user.empresa.companyName
      });
      var img_logotipo = user.empresa.logotipo;
      doc.addImage(img_logotipo, 'JPEG', 8, 14, 28, 24)
      doc.setFontSize(8);
      doc.setFont("calibri");
      doc.setTextColor(0);
      doc.text('' + user.empresa.companyName, 37, 20);
      doc.setTextColor(0);
      doc.text('' + user.empresa.addressDetail, 37, 25);
      doc.text('NIF: ' + user.empresa.taxRegistrationNumber, 37, 30);
      doc.text('Email: ' + user.empresa.email, 37, 35);
      doc.text('Telefone: ', 95, 30);
      doc.text('' + user.empresa.telefone, 107, 30);
      doc.text('WebSite: ', 95, 35);
      doc.setTextColor(0, 0, 255);
      doc.text('' + (user.empresa.site == null ? '' : user.empresa.site), 107, 35);
      doc.text('___________________', 107, 35);

      doc.setFontSize(8);
      doc.setDrawColor(0);
      doc.setFillColor(255);

      doc.rect(145, 14, 48, 23, 'B');

      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.text('FACTURA N.º', 148, 20);
      doc.text('Data Emissão:', 148, 25);
      doc.text('Vencimento:', 148, 30);
      doc.text('Via: ' + original, 148, 35);

      doc.setFontSize(8);
      doc.setFontType("normal");
      doc.text('' + factura.factura_sigla, 168, 20);
      doc.text('' + moment(factura.created_at).format("DD/MM/YYYY"), 168, 25);
      doc.text('' + (factura.data_vencimento == null ? '' : moment(factura.data_vencimento).format('DD/MM/YYYY')), 168, 30);
      doc.setFontSize(8);
      doc.setFont("calibri");
      doc.setFontType("normal");
      doc.text(cliente.nome.trimStart(), 120, 52);
      doc.text(cliente.morada, 120, 57);
      doc.text('' + cliente.municipio.nome, 120, 62);

      doc.setDrawColor(250);
      doc.setFillColor(220, 220, 220);
      doc.rect(12, 75, 40.5, 3.5, 'B');
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.setFont("calibri");
      doc.text('CONTRATO N.º: ' + (contrato == null || contrato == "null" ? "" : contrato.id), 13, 77.5);
      doc.text('CIL: ' + (contrato == null ? "" : contrato.localconsumo == null ? '' : contrato.localconsumo.cil), 13, 83);
      doc.text('Morada Local de Consumo:', 13, 88);
      doc.setFontType("normal");

      doc.setDrawColor(250);
      doc.setFillColor(220, 220, 220);
      doc.rect(85, 75, 34.5, 3.5, 'B');
      doc.setFontType("bold");
      doc.text('CLIENTE N.º: ' + cliente.id, 86, 77.5);
      doc.text('NIF: ', 86, 88);
      doc.text('Tarifa: ', 86, 93);
      doc.setFontType("normal");
      doc.text(cliente.nome.trimStart(), 86, 83);

/*       doc.text( '' + (nif ? nif.numero_identidade : "") , 92, 88); */
      doc.text('' + (contrato == null ? "" : contrato.tarifa == null ? '' : contrato.tarifa.descricao), 96, 93);

      posY = -10;
      doc.setDrawColor(250);
      doc.setFillColor(220, 220, 220);
      doc.rect(12, 110 + posY, 182, 5, 'B');
      doc.setFontSize(8);
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.text('Resumo da', 13, 113.5 + posY);
      doc.text('' + factura.serie.documento.nome + ' ' + factura.factura_sigla, 29, 113.5 + posY);
      doc.setFontType("normal");

      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.1);
      doc.line(12, 114.5 + posY, 194, 114.5 + posY); // horizontal line

      doc.setFontType("bold");
      doc.text('Descrição', 13, 118 + posY);
      doc.text('Qtd.', 77, 118 + posY);
      doc.text('Un.', 85, 118 + posY);
      doc.text('V. Unit', 100, 118 + posY);
      doc.text('Total S/Iva', 120, 118 + posY);
      doc.text('Cod.Imp.', 140, 118 + posY);
      doc.text('V.Imp.', 162, 118 + posY);
      doc.text('TOTAL', 183, 118 + posY);
      doc.setTextColor(0);
      doc.setFontType("normal");
      doc.setDrawColor(0, 0, 0);
      doc.setLineWidth(0.1);
      doc.line(12, 119 + posY, 194, 119 + posY); // horizontal line

      function numberFormat(number) {
        return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace('€', '').trim();
      }
    }

    function numberFormat(number) {
      return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace('€', '').trim();
    }
  }
}


