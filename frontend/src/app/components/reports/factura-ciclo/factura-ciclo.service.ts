import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/providers/http/api.service';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Injectable({
  providedIn: 'root'
})
export class FacturaCicloService {

  public pdfEvent = new EventEmitter<Object>();

  constructor(private http: ApiService) { }


  public imprimirFactura(id, report = 'imprimir', via = '2º Via') {
    this.http.get('facturas/preview/' + id).subscribe(
      response => {
        const dados = Object(response).data;
        this.imprimirFacturaCiclo(dados.factura, dados.produtos, dados.cliente, dados.user, dados.pagamentos, dados.contrato, dados.leituras, report, via)
      }, error => {

      }
    );
  }

  public imprimirFacturaCiclo(factura: any, produtos: any[], cliente: any, user: any, pagamento: any, contrato: any, leituras: [], report: string = 'imprimir', original: any) {

    var img_logotipo = user.empresa.logotipo;

    var img_telefone = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAAAoCAYAAABuIqMUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAKFSURBVFhH7Zg9a2JBFIYNWkUDWmiMYBOM6Sw06C8QsUpjEyVqEbCzEASRoK2tgfwFwcZKCztBtLCISIokWlv41YlfqGc57tzFjcPu3KvOZVkfeBvnnOEZcGbuvQr4hznJy8VJXi7+H/l0Og0qlQoUCgU8Pj7CZDIhI/LALJ/JZDbS2wkGgzCdTkkFf5jkG40GnJ+f78hjQqEQzGYzUskXJvlwOEwVF4LjciyASf7y8pIqvZ2npydYLBakgw9M8jRZWkqlEungw0Hlc7kc6eDDweQdDgf0+33SwQcmea1WSxUWYrPZoN1uk2p+MMl7PB6qNObm5gY6nQ6p5AuT/OvrK1UcY7FYYDAYkEq+MMl3u10wGo1UeUw8HieVfGGSR56fn6niGI1GA/V6nVTyg1m+1+tt/t80eczd3R2MRiNSzQdmeaRQKIBSqaTKYx4eHrg+JoiSR2KxGFVcSCQSgfl8TqqPi2j58XgMXq+XKi4kEAjAcDgkHb/z9fUFLy8vUCwWYb1ek1+lIVoewdPHbrdTxYU4nU6o1Wqk4yeVSgWurq4242dnZ5BIJPZagCR55OPjA6xW6470di4uLiCZTG7ugWq1CgaDYacGx6UuQLI80mw24fr6ekfoe/Aio4kLwWNYygL2kkfe3t7g9vaWKiUmqVQKVqsVmZWNveWRz89PcLlcVCkxwfdkMRxEHsHHYb/fT5ViDe4RMRxMHlkul5DNZkGv11Pl/haz2UxmYuOg8gKtVgt8Pt8fb+Pvwa8T+XyezMDGUeQRPD3K5TLc39+DWq2mCgvBs1/KK+TR5Ld5f3/fbEa32w06ne6XtMlkgmg0utnwUuAifyxO8nJxkpeLk7w8APwA6uMHnXdcNTgAAAAASUVORK5CYII='
    var img_contador = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAAnCAYAAACyhj57AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAdFSURBVGhD7ZkJUxNbEIXfT/dZyBIgkIQdZClMHpsLFiBbSNg3QdkFBAX3hVVRAmi/+7UZah4VyESSyKviVN0aMjOZ3Dndffrcy19yjZi4JuYcXBNzDq4cMScnJ7K3tydv376Vzc1Nef36tezs7Mjx8XH0jvTgShBz8O2brCyvyNjIqHR3dcvg4KBMTU3J7OysHoeGhqSzs1OPS0tLsr+/H/1m6vDHiPn586dmRrCvT3Jzc8Xn8cg//oD0dPdIMBhUcp48eSLv3r2T79+/63c+ffokMzMz0tXVJWNjY/L161c9z7OSjbQTQ6m8fPlSRkZGpLu7W3p7e5WIR48eycOHD+X+/fvy4MED6ejokOHhYb0WCoVkcnLyNFMODw81c7hneXlZzyUbaSXm8+fPEg6HZWBgQPrD/VJXUyvuvHwpLSuT2tpaaWxslJaWFgkEAlJdXS2ewkLxFBRKS3OLEgmJZMzR0ZE+7+DgQEkjeyA8mUgbMaurq5ohZMHtqtuS68qVYG9QBfa8UqCEFubnlajs7GzNJEjtM+W3u7ur9/BdiEF/fvz4oeeSgbQQMzc3J/39/dLV2SWZGbf0+OXLl+hVZ3j16pVUVlZqFkFElxFjMtAC5x4/fhz9dHmknJhnz56ZCIdUO3JycuT58+fRK4mDEiLrMm9lytjoqHYqBBzQzrm2sbGhny+LlBLz/v176enpkY72dsnJztHPyQBlk52VJaOmvVNWVgnhdxDkSCSiny+DlBFDBOkoI8MjcisjQzY3NqNXkoOOtnYpKipSkuaNDlmgnBYXF6Offh8pI4Y2ihfxGn+CvgB8B96E8fTpUz1OT0/rwMxZ5+g81t8YPOsehtWeI0cRKS4ulra2Nu1WlmYhymTpZYU4JcTQOpkcNe8xxFjtdW1tTVMdm49Jc7lc2q0WFhYkz3SpcSOgiCxdqLy8XPViempa/r5xQ+bn5lWfaOdWF1tdWZHcHJcGwJ41fL5s2TomhgjwwhwvGuDFixcyZCZXXFQskxMTeg5ADBkAyBAizkt+M0uCYl+RvHnzRq9BaFNTk/69vbUtLqNPeBYAodbvcKyoqNBz+BnLy6wYwiAZ2Od20ThrGRwRA/tEgXrGh1w0MGJEdXBg0GRBnuzu/PIbgIhbxFAuPp/vlBifx6uZBOg2mD2wtbUl2ZlZp8TgkHkRC8yrqqpK50b7ptQ62jukoaFBRk3nijXHWAPjabcQcYnBfjMZoskLMMF4I9wflnbTifz1d6JP+QXKhExgEnfv3pXS0lItOYioLK9QveCa3++XmpoazQLOeQ1pnWYO3Nva2hp92i8QtPz8fA1I0GgNXYqlA/OONbdYg3vRQStjQVxi0AAryk5ANOlG9+7d02EH1xBgBJIjE8KHME6OT/Ql0RscL4PzvGTE3Le/t6+f+Y4dBKvAXSDDQ8Piznf/9sobg8hWh4W4xBAJ+xfigRci0pQCte8UHz9+1GhjBOlIlmDHA56FMuwzy4t8s+46S5xTJEwMq1rWM05BBPtDYQn4A5o5TkGZkJkIKOXGFoMT4JeKjHAHe3q1s1lbFIkiYWJIb8uHOAETJWOam5u1NTsF+sO2AxkKSU5Xy5HDiHgLPaYLDonP6zsV8EQxPj6emMbQNVB+GMUrsCC8aFAGkIJI0p3s4FkMqz3aByXA89lnSeTl6CRut1u7EvPE7JF5dL1Y84s1+F1W7nbvE5cYwKQRYRaEtMOLBvfgQ9AXthfse7V0AM6TGXSbs4NOxrZCIsAzQQyksAEGUfX19SoB+JlYczw7mDNWwq5rjohJFESdCOZkZcuHDx+iZ0X/5jwTiDUoH8ovEZ3gfto75Q4pdKVEtO08pIQYCGCiNdXV0m3rTLRbMsbaOyHlOdoH2WbPsosAgXSkUF9IMwZMGKdNYC6LlBADEGDIYefNvtsGORBHByB7rD1fBqJLvdud7UVAMNm44ndYbvBsyvFKbzsgoJDDvq5l7+3At+B4ETz7QGuceJHt7W0lnd+AVGwCz/tfbFSxBiGq7NyR4nbgUxBhhO90LC5Jq+kO8YhBj6pvV0vgjl9C4ZAGgd85+xuXQUqJwfajGdR/RkbGf5YWlAvpzyrbapuzM7NxI06ZBIzYlpWWqeexCOFvSjVZSCkxAD1hEYoO3Lx5UxeJTs3bWeyY8qmsqJDSklKZnJjU56JLiLlTXXICAplyYgCOkm7EQs9jXGplRaWsr607jjD+h7LMdbmkqaFRJ85/DDBllGCykTZiAGJJB6IT4Yrz8vKUoInxCSWOjkKbJpvQGPZh1tfXNSsKCgp0Uytk1mB+oyuZmZlK1O+upOMhrcQARJN9XLoIL0ZrZU/GbV68pKREysvKpMwMznm9XnW0dXV1uuqmBBHmYUMsHSiVQPvSSowFsoO1DC9LBkEUmYEZ7DWaQdtlWNfRJ5YkTtp4svBHiLFABiHORAhjh/NFVFmIoh10KMxhMruNU/xRYq4yrok5B9fExITIv+V+x3Wdjxh+AAAAAElFTkSuQmCC'
    var doc = new jsPDF();

    doc.setProperties({
      title: '' + factura.factura_sigla,
      subject: 'Factura',
      author: 'Unig',
      keywords: '',
      creator: user.empresa.companyName
    });

    doc.addImage(img_logotipo, 'JPEG', 10, 17, 24, 20)
    //doc.addImage(img_codigobarras, 'PNG', 128, 39, 69, 10)
    factura.created_at = moment(factura.created_at).format('DD/MM/YYYY');
    doc.setFontSize(9);
    doc.setFont("arial");
    doc.setTextColor(0);
    doc.text('' + user.empresa.companyName, 37, 20);
    doc.setTextColor(0);
    doc.text('' + user.empresa.addressDetail, 37, 25);
    doc.text('NIF: ' + user.empresa.taxRegistrationNumber, 37, 30);
    doc.text('Email: ' + user.empresa.email, 37, 34);
    doc.text('Telefone: ' + user.empresa.telefone, 106, 27);
    doc.text('WebSite: ', 100, 31);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(149, 16, 45, 15, 'B'); // filled red square
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('FACTURA N.º', 150, 20);
    doc.text('Data Emissão:', 150, 24);
    doc.text('Data de Vencimento:', 150, 28);

    doc.setFontSize(8);
    doc.setFontType("normal");
    doc.text('' + factura.factura_sigla, 169, 20);
    doc.text('' + factura.created_at, 170, 24);
    doc.text('' + (factura.data_vencimento == null ? '' : moment(factura.data_vencimento).format('DD/MM/YYYY')), 179, 28);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(10, 41, 105, 26, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.addImage(img_telefone, 'PNG', 15, 42, 4, 12)
    doc.text('APOIO AO CLIENTE', 20, 50);
    doc.setFontType("normal");
    doc.text(user.empresa.addressDetail + ': ' + user.empresa.telefone, 15, 55);
    doc.text('APOIO COMERCIAL: ' + user.empresa.telefone, 15, 58.5);
    doc.text('GERAL: ' + user.empresa.telefone, 83, 58.5);
    doc.text('EMAIL: ' + user.empresa.email, 15, 61.5);
    doc.text('WEBSITE E PORTAL DO CLIENTE: ', 15, 64.5);

    doc.setFontSize(8);
    doc.setFont("arial");
    doc.setFontType("normal");
    doc.text('' + cliente.nome, 130, 51);
    doc.text('' + cliente.morada, 130, 55);
    //doc.text('Número 016, Apartamento 10', 130, 59);
    doc.text('' + cliente.municipio.nome, 130, 59);

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setFont("arial");
    doc.text('CONTRATO N.º: ', 10, 75);
    doc.text(' ' + (contrato == null || contrato == "null" ? "" : contrato.id), 32, 75);
    doc.text('CIL: ' + (contrato == null ? "" : contrato.localconsumo == null ? '' : contrato.localconsumo.cil), 10, 79);
    doc.text('Morada Local de Consumo:', 10, 83);
    doc.setFontType("normal");
    doc.text('Bairro '+(contrato==null? "": contrato.localconsumo.bairro+', Quarteirão '+contrato.localconsumo.quarteirao), 10, 87);
    //doc.text('Número 025, Apartamento 05', 10, 90);
    doc.text(''+(contrato==null? "": contrato.localconsumo.municipio), 10, 93);

    doc.setFontType("bold");
    doc.text('CLIENTE N.º: ' + cliente.id, 85, 75);
    doc.text('NIF: ', 85, 83);
    doc.text('Tarifa: ', 85, 87);
    doc.setFontType("normal");
    doc.text('' + cliente.nome, 85, 79);
    doc.text(' ' + cliente.numero_identificacao, 95, 83);
    doc.text('' + (contrato == null ? "" : contrato.tarifa == null ? '' : contrato.tarifa.descricao), 95, 87);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(129, 71, 65, 22, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Conta Corrente:', 133, 75);
    doc.setFontType("normal");
    doc.text('Saldo Anterior', 133, 79);
    doc.text('xx/xx/xxxx', 157, 79);
    doc.text('', 177, 79);
    doc.text('Pagamentos', 133, 83);
    doc.text('', 176, 83);
    doc.text('Factura Actual', 133, 87);
    doc.text('' + factura.created_at, 157, 87);
    doc.text('' + factura.total, 178, 87);
    doc.setFontType("bold");
    doc.text('Saldo a Pagamento', 133, 91);
    doc.text('', 177, 91);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(12, 97, 80, 18, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.addImage(img_contador, 'PNG', 17, 98, 12, 7)
    doc.text('Comunique a sua leitura:', 31, 104);
    doc.setFontType("bold");
    doc.text('De xx/xx/xxxx a xx/xx/xxxx', 15, 110);
    doc.text('Contador n.º ' + (contrato == null ? "" : contrato.localconsumo == null ? "" : contrato.localconsumo.contador), 15, 114);
    doc.text('Calibre: ' + (contrato == null ? "" : contrato.localconsumo == null ? "" : contrato.localconsumo.calibre), 65, 114);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(12, 115, 80, 18, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Período Facturado:', 15, 119);
    doc.setFontType("normal");
    doc.text('Período Facturação:', 15, 123);
    doc.text('xx dias' + ' - ' + 'xx/xx/xxxx' + ' a ' + 'xx/xx/xxxx', 42, 123);
    doc.text('Período Consumo:', 15, 126);
    doc.text('xx dias' + ' - ' + 'xx/xx/xxxx' + ' a ' + 'xx/xx/xxxx', 42, 126);
    doc.setFontType("bold");
    doc.text('Consumo Facturado xx m3:', 15, 129);
    doc.setFontType("normal");
    doc.text('Medido: xx m3', 15, 132);
    doc.text('Estimado: xx m3', 65, 132);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(97, 97, 36, 41, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Histórico de Leituras', 101, 101);
    doc.setFontType("normal");
    doc.text('Data:', 101, 105);
    doc.text('Leitura:', 120, 105);
    if (contrato != null) {
      if (contrato.leituras.length > 0) {
        var posYL = 110;
        for (let index = 0; index < contrato.leituras.length; index++) {
          const leitura = contrato.leituras[index];
          doc.text('' + moment(leitura.data_leitura).format('DD/MM/YYYY'), 99, posYL);
          doc.text(leitura.leitura + ' m3', 118, posYL);
          posYL += 5;
        }
      }
    }
    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(137, 97, 57, 6, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Gráfico de Consumos', 149, 101);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(137, 103, 57, 35, 'B');

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, 140, 182, 5, 'B');


    doc.setDrawColor(30, 144, 255);
    doc.setFillColor(30, 144, 255);
    var jan = 2;
    doc.rect(140.5, 117 - jan, 2, 18 + jan, 'B');
    var fev = 2;
    doc.rect(144, 117 - fev, 2, 18 + fev, 'B');
    var mar = 3;
    doc.rect(148, 117 - mar, 2, 18 + mar, 'B');
    var abr = 2;
    doc.rect(152, 117 - abr, 2, 18 + abr, 'B');
    var mai = 11;
    doc.rect(156, 117 - mai, 2, 18 + mai, 'B');
    var jun = 9;
    doc.rect(160, 117 - jun, 2, 18 + jun, 'B');
    var jul = 0;
    doc.rect(164, 117 - jul, 2, 18 + jul, 'B');
    var ago = 3;
    doc.rect(168, 117 - ago, 2, 18 + ago, 'B');
    var set = 3;
    doc.rect(172, 117 - set, 2, 18 + set, 'B');
    var out = 3;
    doc.rect(176, 117 - out, 2, 18 + out, 'B');
    var nov = 6;
    doc.rect(180, 117 - nov, 2, 18 + nov, 'B');
    var dez = 4;
    doc.rect(184, 117 - dez, 2, 18 + dez, 'B');
    var jan2 = 2;
    doc.rect(188, 117 - jan2, 2, 18 + jan2, 'B');

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('__________________________________', 139, 135);

    doc.setFontSize(3)
    doc.text('JAN', 140.5, 137)
    doc.text('FEV', 144, 137);
    doc.text('MAR', 148, 137);
    doc.text('ABR', 152, 137);
    doc.text('MAI', 156, 137);
    doc.text('JUN', 160, 137);
    doc.text('JUL', 164, 137);
    doc.text('AGO', 168, 137);
    doc.text('SET', 172, 137);
    doc.text('OUT', 176, 137);
    doc.text('NOV', 180, 137);
    doc.text('DEZ', 184, 137);
    doc.text('JAN', 188, 137);
    doc.setFontSize(5)
    doc.text('10', 140, 114)
    doc.text('10', 144, 114);
    doc.text('12', 148, 113);
    doc.text('10', 152, 114);
    doc.text('22', 156, 105);
    doc.text('16', 160, 107);
    doc.text('4', 164.5, 116);
    doc.text('12', 167.5, 113.5);
    doc.text('12', 171.5, 113.5);
    doc.text('12', 175.5, 113.5);
    doc.text('16', 179.5, 110);
    doc.text('14', 184, 112);
    doc.text('10', 188, 114.5);

    doc.setFontSize(8)
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('RESUMO DA FACTURA', 13, 143.5);
    /*     doc.text('XXXXXXXXXX', 32, 143.5);
        doc.text('de xx/xx/xxxx', 53, 143.5); */
    doc.setFontType("normal");
    doc.text('_________________________________________________________________________________________________________________________________', 12, 144.6);
    doc.setFontType("bold");
    doc.text('Descrição', 25, 148);
    doc.text('Facturação', 70, 148);
    doc.text('Dedução Estimativas Anteriores', 105, 148);
    doc.text('TOTAL (Kz)', 155, 148);
    doc.setTextColor(0);
    doc.setFontType("normal");
    doc.text('__________________________________________________________________________________________________', 56, 149);

    doc.text('Qtd.', 61, 153);
    doc.text('V. Unit.', 74, 153);
    doc.text('(Kz)', 76, 156);
    doc.text('Valor (Kz)', 88, 153);

    doc.text('Qtd.', 108, 153);
    doc.text('V. Unit.', 120, 153);
    doc.text('(Kz)', 122, 156);
    doc.text('Valor (Kz)', 135, 153);

    var vectItens1 = [
      ['Água Doméstico Esc. 1', '5 m3', '95', '475', '7 m3', '95', '665', '-190,00'],
      ['Água Doméstico Esc. 2', '5 m3', '100', '500', '', '', '', '500,00'],
      ['Água Doméstico Esc. 3', '3 m3', '117', '351', '', '', '', '351,00'],
      ['Tarifa Fixa Água', '30 dias', '8,67', '260', '', '', '', '260,00']
    ]

    var vectItens2 = [
      ['San. Águas Residuais Variável', '90%', '1.041', '936,90', '', '', '598,50', '338,00'],
      ['San. Águas Residuais Fixo', '30 dias', '8,67', '260', '', '', '', '260,00']
    ]

    var vectItens3 = [
      ['Outros 1', '', '', '', '', '', '', '1000,00'],
      ['Outros 2', '', '', '', '', '', '', '1000,00'],
      ['IVA (14%)', '', '', '', '', '', '', '497,72'],
    ]
    doc.setFontType("normal");
    doc.text('_________________________________________________________________________________________________________________________________', 12, 159);
    doc.setTextColor(0);

    var posY = 163;
    /*for (var l = 0; l < vectItens1.length; l++) {
      doc.text(vectItens1[l][0], 20, posY);
      doc.text(vectItens1[l][1], 61, posY);
      doc.text(vectItens1[l][2], 74, posY);
      doc.text(vectItens1[l][3], 88, posY);
      doc.text(vectItens1[l][4], 108, posY);
      doc.text(vectItens1[l][5], 120, posY);
      doc.text(vectItens1[l][6], 135, posY);
      doc.text(vectItens1[l][7], 173, posY);
      posY += 5;

      const hasReachedPageLimit = doc.internal.pageSize.height < (posY+20)

      if(hasReachedPageLimit)
      {    posY= 10;
           doc.addPage();
      }
    }


    doc.text('_________________________________________________________________________________________________________________________________', 12, 179);
*/
    for (var i = 0; i < produtos.length; i++) {
      const data = produtos[i];

      doc.text('' + 'NOME DO SERVICO', 20, posY);
      doc.text('' + data.quantidade, 61, posY);
      doc.text('' + this.numberFormat(data.valor), 74, posY);
      /*doc.text(data., 88, posY);
      doc.text(data., 108, posY);
      doc.text(data., 120, posY);
      doc.text(data., 135, posY);*/
      doc.text('' + this.numberFormat(data.total), 173, posY);
      posY += 5;

    }

    doc.text('_________________________________________________________________________________________________________________________________', 12, posY - 4);

    /*for (var l = 0; l < vectItens3.length; l++) {
      doc.text(vectItens3[l][0], 20, posY);
      doc.text(vectItens3[l][1], 61, posY);
      doc.text(vectItens3[l][2], 74, posY);
      doc.text(vectItens3[l][3], 88, posY);
      doc.text(vectItens3[l][4], 108, posY);
      doc.text(vectItens3[l][5], 120, posY);
      doc.text(vectItens3[l][6], 135, posY);
      doc.text(vectItens3[l][7], 173, posY);
      posY += 5;

    }*/

    doc.text('_________________________________________________________________________________________________________________________________', 12, posY - 4.5);
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setDrawColor(255, 0, 0);
    doc.setFillColor(250, 0, 0);
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, posY - 3.5, 182, 5, 'B');
    doc.text('TOTAL FACTURA', 13, posY);
    doc.text('' + this.numberFormat(factura.total), 183, posY)
    doc.text('_________________________________________________________________________________________________________________________________', 12, posY + 1);
    doc.text('Mensagens Importantes', 10, posY + 11);
    doc.setFontType("normal");
    doc.text('_____________________', 10, posY + 11);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(10, posY + 12, 66, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Débito Directo:', 12, posY + 16.5);
    doc.setFontType("normal");
    doc.text('Efectue os seus pagamentos por Débito Directo.', 12, posY + 20);
    doc.text('Directo. É automático e sem preocupações. ', 12, posY + 23);
    doc.text('Adira já.', 12, posY + 26);
    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(76, posY + 12, 61, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Factura Digital:', 77, posY + 16.5);
    doc.setFontType("normal");
    doc.text('Evite deslocações desnecessárias. Receba a', 77, posY + 20);
    doc.text('sua factura por email ou WhatsApp e efectue', 77, posY + 23);
    doc.text('os seus pagamentos no Multicaixa ou ', 77, posY + 26);
    doc.text('Multicaixa Express. Adira já.', 77, posY + 29);

    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(135, posY + 12, 60, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Poupe Água:', 136, posY + 16.5);
    doc.setFontType("normal");
    doc.text('Feche as torneiras e não deixe mangueiras ', 136, posY + 20);
    doc.text('largadas com água aberta. Não desperdice ', 136, posY + 23);
    doc.text('água.', 136, posY + 26);
    doc.text('-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------', 10, posY + 35);

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(10, posY + 36, 185.5, 5, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Referências Multicaixa (use apenas 1 opção):', 12, posY + 39);
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(115, posY + 36, 80, 5, 'B');
    doc.setTextColor(0);
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.text('Contas Bancárias (IBAN):', 118, posY + 39);
    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(10, posY + 41, 66, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Total Conta Corrente:', 12, posY + 45);
    doc.setFontType("normal");
    doc.text('Entidade: ', 12, posY + 49);
    doc.text('xxx', 25, posY + 49);
    doc.text('Referência: ', 12, posY + 52);
    doc.text('xxx xxx xxx', 27, posY + 52);
    doc.text('Valor: ', 12, posY + 55);
    doc.text(' 19.012,12 Kz', 19, posY + 55);
    doc.setFontSize(8);

    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(76, posY + 41, 61, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Factura Actual:', 77, posY + 45);
    doc.setFontType("normal");
    doc.text('Entidade: ', 77, posY + 49);
    doc.text('xxx', 90, posY + 49);
    doc.text('Referência: ', 77, posY + 52);
    doc.text('xxx xxx xxx', 92, posY + 52);
    doc.text('Valor: ', 77, posY + 55);
    doc.text('  4.012,12 Kz', 84, posY + 55);

    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(115, posY + 41, 80, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("normal");
    doc.text('BPC', 118, posY + 44);
    doc.text('AO06 0010 0505 0253832801194', 125, posY + 44);
    doc.text('SOL', 118, posY + 47);
    doc.text('AO06 0044 0000 0500085210185', 125, posY + 47);
    doc.text('BNA', 118, posY + 50);
    doc.text('AO06 0000 0561 5555555555555', 125, posY + 50);
    doc.text('Sempre que efectuar uma transferência ou depósito,', 118, posY + 53);
    doc.text('deve indicar o número das facturas a liquidar.', 118, posY + 56);

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(10, posY + 61.2, 52, 3.5, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Data Limite de Pagamento:', 10, posY + 63);
    doc.text(' xx/xx/xxxx', 46, posY + 63);
    doc.text('Lojas ' + user.empresa.companyName + ':', 10, posY + 68.5);
    //doc.addImage(img_codigoQR, 'PNG', 179, 274, 17, 15)

    doc.setFontType("normal");
    doc.text(user.empresa.addressDetail + '(de segunda a sexta-feira das 8h às 15h | sábado das 8h às 12h)', 10, posY + 72);
    doc.text('Centralidade 5 de Abril, Bloco K01, Apartamento 02 (de segunda a sexta-feira das 8h às 12h)', 10, posY + 75);
    doc.text('Centralidade Praia Amélia, Bloco K100, Apartamento 02 (de segunda a sexta-feira das 8h às 12h)', 10, posY + 78);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(0);
    doc.line(10, doc.internal.pageSize.height - 9, 200, doc.internal.pageSize.height - 9); // vertical line
    var hash = factura.hash.substring(0, 1);
    hash += factura.hash.substring(10, 11);
    hash += factura.hash.substring(20, 21);
    hash += factura.hash.substring(30, 31);
    doc.setFontSize(6);

    doc.text("NIF: " + user.empresa.taxRegistrationNumber + " - " + user.empresa.companyName + " / " + user.empresa.addressDetail + " / " + user.empresa.telefone + " / " + user.empresa.email, 105, doc.internal.pageSize.height - 6, null, null, 'center');

    doc.setFontSize(8);
    doc.text(hash + '-Processado por programa validado nº 4/AGT119', 105, doc.internal.pageSize.height - 3, null, null, 'center');

    if (factura.status == 'A') {
      doc.setDrawColor(0);
      doc.setFillColor(220, 220, 220);
      doc.rect(10, posY + 89, 52, 5, 'B');
      doc.setTextColor(0);
      doc.setFontType("bold");
      doc.text('Estado: ', 12, posY + 92.5);
      doc.setTextColor('red');
      doc.text('Anulação', 24, posY + 92.5);
      doc.setTextColor(0);

  }

    if (report == 'imprimir') {
      doc.autoPrint();
      window.open(doc.output("bloburl")); //opens the data uri in new window
      // doc.output("dataurlnewwindow"); //opens the data uri in new window
    } else {
      doc.save(factura.factura_sigla + '.pdf'); /* download the file immediately on loading */
    }

  }

  public numberFormat(number) {
    return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace('€', '').trim();
  }


}
