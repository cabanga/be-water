import { TestBed } from '@angular/core/testing';

import { FaturaCicloService } from './factura-ciclo.service';

describe('FaturaCicloService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FaturaCicloService = TestBed.get(FaturaCicloService);
    expect(service).toBeTruthy();
  });
});
