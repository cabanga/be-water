import { Injectable, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/providers/http/api.service';
import { HttpService } from 'src/app/providers/http/http.service';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Injectable({
  providedIn: 'root'
})
export class ContratoService {

  public pdfEvent = new EventEmitter<Object>();

  private report: any;

  constructor(private http: HttpService) { }

  public imprimirPDFContrato(id, report = 'imprimir') {
    this.http._get('contrato/imprimir/' + id).subscribe(
      response => {
        const dados = Object(response).data;
        this.imprimirContrato(dados, report)
      }, error => {
        console.log(error)
      }
    );
  }


  public imprimirContrato(item, report = 'imprimir') {


    const contrato = item.contrato;
    const tipos_identidades = item.tipos_identidades;
    const empresa = item.empresa;


    var img_logotipo = empresa.logotipo;
    var doc = new jsPDF()
    doc.addImage(img_logotipo, 'JPEG', 10, 17, 24, 20)

    doc.setFontSize(8);
    doc.setFont("calibri");
    doc.setTextColor(0);
    doc.text('' + empresa.companyName, 37, 20);
    doc.setTextColor(0);
    doc.text('' + empresa.addressDetail, 37, 25);
    doc.text('NIF: ' + empresa.taxRegistrationNumber, 37, 30);
    doc.text('Email: ' + empresa.email, 37, 35);
    doc.text('Telefone: ', 95, 30);
    doc.text('' + empresa.telefone, 107, 30);
    doc.text('WebSite: ', 95, 35);
    doc.setTextColor(0, 0, 255);
    doc.text('' + (empresa.site == null ? '' : empresa.site), 107, 35);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(145, 14, 48, 23, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('CONTRATO N.º', 148, 20);
    doc.text('CIL', 148, 30);
    doc.setFontSize(8);
    doc.setFontType("normal");
    doc.text('' + contrato.id, 170, 20);
    doc.text('' + (contrato.cil == null ? '' : contrato.cil), 155, 30);
    doc.setFontSize(11);
    doc.setFontType("bold");
    doc.text('CONTRATO DE ' + contrato.objecto_contrato.toUpperCase(), 10, 46);

    doc.setFontSize(8);
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setFont("arial");
    doc.text('LOCAL DE CONSUMO: ' + contrato.id, 10, 58);
    doc.text('CIL: ' + (contrato.cil == null ? '' : contrato.cil), 10, 61.5);
    doc.text('Morada Local de Consumo:', 10, 65);
    doc.setFontType("normal");
    doc.text('Bairro ' + (contrato.bairro == null ? '' : contrato.bairro) + ', Quarteirão ' + (contrato.quarteirao == null ? '' : contrato.quarteirao), 10, 68);
    doc.text('' + ((contrato.is_predio ? 'Prédio ' + (contrato.predio_nome == null ? '' : contrato.predio_nome) + ', ' + (contrato.predio_andar == null ? '' : contrato.predio_andar) + 'ºAndar - Porta ' : 'Residência ') + contrato.moradia_numero == null ? '' : contrato.moradia_numero), 10, 71);
    doc.text('' + (contrato.distrito == null ? '' : contrato.distrito), 10, 74);


    doc.setFontType("bold");
    doc.text('CLIENTE N.º: ' + contrato.cliente_id, 85, 58);
    doc.text('NIF: ', 85, 65);
    doc.text('Telefone: ' + (contrato.cliente_telefone == null ? '' : contrato.cliente_telefone), 85, 69);
    doc.setFontType("normal");
    doc.text('EMAIL: ' + (contrato.cliente_email == null ? '' : contrato.cliente_email), 85, 73);
    doc.setFontType("normal");
    doc.text('' + contrato.cliente_nome.toUpperCase(), 85, 61.5);

    let nif = tipos_identidades.find(obj => obj.nome == 'NIF')
    doc.text(' ' + (nif ? nif.numero_identidade : ""), 91, 65);

    doc.setFontSize(8)
    doc.setTextColor(0);
    doc.setFont("arial");
    doc.setFontType("normal");

    let identidade_text = nif ? `, portador do ${nif.nome} n.º ${nif.numero_identidade}, ` : ", "
    doc.text('O presente contrato de ' + contrato.objecto_contrato.toUpperCase() + ' é celebrado entre a ' + empresa.companyName + ', NIF 5161162392, e o/a Cliente ' + contrato.cliente_nome.toUpperCase() + identidade_text + 'o Local de Consumo acima identificado.', 10, 88, { maxWidth: 170, align: 'justify' });

    doc.setFontType("bold");
    doc.text('Tarifa: ', 10, 103);
    doc.text((contrato.numero_serie != null) ? 'Contador: ' : '', 10, 109);

    if (contrato.morada_correspondencia_id == null) doc.text('Morada de Correspondência:', 10, 115);


    doc.text('Tipo de Utilização: ', 72, 103);

    doc.text('Leitura: ', 72, 109);
    doc.text('Data Inicio Contrato:', 144, 103);
    doc.text('Consumo Máximo Previsto:', 144, 109);
    doc.text('Período de Facturação:', 144, 115);

    doc.setFontType("Normal");
    doc.text('' + contrato.tarifario, 19, 103);
    doc.text((contrato.numero_serie != null) ? contrato.numero_serie : '', 24, 109);
    doc.text('' + contrato.tipo_contrato, 98, 103);

    if (contrato.morada_correspondencia_id == null) {
      doc.text('Província de ' + contrato.cliente_provincia + ', Município de ' + contrato.cliente_municipio, 10, 119);
      doc.text(contrato.cliente_morada, 10, 122);
    }

    doc.text('' + (contrato.tipo_medicao_slug == 'LEITURA') ? contrato.ultima_leitura == null ? '' : contrato.ultima_leitura + ' m³' : '', 83, 109);
    doc.text('' + moment(contrato.data_inicio).format("DD/MM/YYYY"), 171, 103);
    doc.text('Mensal', 173, 115);
    doc.setFontSize(8)
    doc.setTextColor(0);
    doc.setFont("arial");
    doc.setFontType("bold");
    doc.text('O Cliente declara que deseja receber as suas Facturas no seguinte email: ', 10, 135);
    doc.setFontType("normal");
    doc.setTextColor(0, 0, 255);
    doc.text(' ' + (contrato.cliente_email == null ? '' : contrato.cliente_email), 100, 135);
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('O Cliente declara que deseja pagar por Débito Directo através da sua conta com IBAN ', 10, 140);
    doc.setFontType("normal");
    doc.text('AO06 0044 0000 0500085215555', 119, 140);
    doc.setFontType("bold");
    doc.text('O Cliente declara ter conhecimento das condições gerais do presente contrato que constam no seu verso.', 10, 145);
    doc.text('Moçâmedes, ', 10, 154);
    doc.text('' + moment(contrato.data_inicio).format("DD/MM/YYYY"), 27, 154);
    doc.text('O Cliente ', 10, 164);
    doc.text('_____________________________________________________________________________________________________________', 24, 164);
    doc.text('A EPASNAMIBE-E.P. ', 10, 190);

    doc.text('___________________________________', 10, 215);
    doc.setFontType("normal");
    doc.text('(Eng. Arlindo Mendes Tavares)', 10, 223);
    doc.text('Presidente do Conselho de Administração', 10, 230);

    doc.setFontType("bold");
    doc.text('___________________________________', 125, 215);
    doc.setFontType("normal");
    doc.text('(Dra. Alzira da Conceição Cunha de Sá)', 125, 223);
    doc.text('Administradora Área Administrativa e Financeira', 125, 230);

    var numberPage = 1;
    var totalNumberPage = 1;
    var dataActual = moment(new Date()).format("DD/MM/YYYY");

    doc.line(10, doc.internal.pageSize.height - 9, 194, doc.internal.pageSize.height - 9); // vertical line
    doc.text(dataActual + ' ' + moment().format("H:m"), 11, 285);
    doc.text('Página: ' + numberPage + '/1' , 180, 285);
    doc.setFontSize(6);

    doc.text("NIF: " + empresa.taxRegistrationNumber + " - " + empresa.companyName + " / " + empresa.addressDetail + " / " + empresa.telefone + " / " + empresa.email, 105, doc.internal.pageSize.height - 6, null, null, 'center');
    doc.setFontSize(7);
    doc.text('Processado por programa validado nº 4/AGT119', 105, doc.internal.pageSize.height - 3, null, null, 'center');


    doc.addPage();
    doc.setFontSize(8)
    doc.setTextColor(0);
    doc.setFont("arial");
    doc.setFontType("bold");
    doc.text('Cláusulas Gerais do Contrato', 75, 25);
    doc.setFontType("bold");
    doc.text('Cláusula 1ª Contrato 1)', 10, 32);
    doc.setFontType("normal");
    doc.text('O presente contrato tem por objectivo o', 42, 32);
    doc.text('fornecimento de água por parte da EPASNAMIBE-E.P..', 10, 35);
    doc.setFontType("bold");
    doc.text('2) ', 80, 35);
    doc.setFontType("normal");
    doc.text('A', 83, 35);
    doc.text('instalação de contador é decidida pela EPASNAMIBE-E.P., em lugar', 10, 38);
    doc.text('escolhido pela EPASNAMIBE-E.P. e, sempre que possível, acessível a', 10, 41);
    doc.text('uma fácil leitura regular, com protecção adequada à sua eficiente ', 10, 44);
    doc.text('conservação e normal funcionamento, não podendo ser mudado de', 10, 47);
    doc.text('local, em quaisquer circunstâncias, pelo consumidor. ', 10, 50);
    doc.setFontType("bold");
    doc.text('3) ', 77, 50);
    doc.setFontType("normal");
    doc.text('O contador', 80, 50);
    doc.text('instalado é propriedade da EPASNAMIBE-E.P., devendo o ', 10, 53);
    doc.text('Consumidor, na qualidade de fiel depositário, comunicar à', 10, 56);
    doc.text('EPASNAMIBE E.P. anomalias que verificar.', 10, 59);
    doc.setFontType("bold");
    doc.text('4)', 66, 59);
    doc.setFontType("normal");
    doc.text('O Consumidor', 69, 59);
    doc.text('responde por danos ou perda do contador, excepto se a ', 10, 62);
    doc.text('deterioração ou perda resultar da normal utilização ou de facto que', 10, 65);
    doc.text('não lhe seja imputável, desde que dê conhecimento da situação à ', 10, 68);
    doc.text('EPASNAMIBE E.P. até 5 dias após a verificação do facto. ', 10, 71);
    doc.setFontType("bold");
    doc.text('Cláusula 2ª', 83, 71);
    doc.text('_________', 83, 71);
    doc.text('Duração', 10, 74);
    doc.setFontType("normal");
    doc.text('O contrato será celebrado por tempo indeterminado. ', 22, 74);
    doc.setFontType("bold");
    doc.text('Cláusula 3ª', 10, 77);
    doc.text('_________', 10, 77);
    doc.text('Deveres do Consumidor', 26, 77.5);
    doc.setFontType("normal");
    doc.text('Os consumidores estão', 59, 77.5);
    doc.text('obrigados a: ', 10, 80.5);
    doc.setFontType("bold");
    doc.text('1) ', 26, 80.5);
    doc.setFontType("normal");
    doc.text('Cumprir as disposições do contrato em vigor, ', 29, 80.5);
    doc.text('relacionado com os sistemas públicos e prediais de distribuição de', 10, 83);
    doc.text('água bem como as recomendações e orientações dadas pela ', 10, 86);
    doc.text('EPASNAMIBE-E.P.; ', 10, 89);
    doc.setFontType("bold");
    doc.text('2)', 36, 89);
    doc.setFontType("normal");
    doc.text('Pagar pontualmente a importância devida, nos', 39, 89);
    doc.text('termos do presente contrato;', 10, 92);
    doc.setFontType("bold");
    doc.text('3)', 47, 92);
    doc.setFontType("normal");
    doc.text('Não fazer uso indevido ou danificar', 50, 92);
    doc.text('qualquer obra ou equipamento dos sistemas públicos;', 10, 95);
    doc.setFontType("bold");
    doc.text('4)', 78, 95);
    doc.setFontType("normal");
    doc.text('Não', 81, 95);
    doc.text('proceder à execução de ligação ao sistema público sem autorização ', 10, 98);
    doc.text('da EPASNAMIBE-E.P.; ', 10, 101);
    doc.setFontType("bold");
    doc.text('5)', 40, 101);
    doc.setFontType("normal");
    doc.text('Não alterar o ramal de ligação da água de', 43, 101);
    doc.text('abastecimento, estabelecido entre rede geral e rede predial;', 10, 104);
    doc.setFontType("bold");
    doc.text('6)', 86, 104);
    doc.setFontType("normal");
    doc.text('Abster-se de actos que possam provocar contaminação da água;', 10, 107);
    doc.setFontType("bold");
    doc.text('7)', 93, 107);
    doc.setFontType("normal");
    doc.text('Informar a EPASNAMIBE-E.P., sempre que se aperceba de mau ', 10, 110);
    doc.text('funcionamento da instalação, ou seja, efectuar comunicação de ', 10, 113);
    doc.text('anomalias detectadas; ', 10, 116);
    doc.setFontType("bold");
    doc.text('8) ', 10, 119);
    doc.setFontType("normal");
    doc.text('Permitir o acesso de colaboradores da', 13, 119);
    doc.text('EPASNAMIBE-E.P., devidamente credenciados, para recolha de ', 10, 122);
    doc.text('leituras, substituição de contador, fiscalização, colheita de amostras ', 10, 125);
    doc.text('de controlo e outros fins relacionados com o âmbito do presente  ', 10, 128);
    doc.text('contrato;  ', 10, 131);
    doc.setFontType("bold");
    doc.text('9) ', 22, 131);
    doc.setFontType("normal");
    doc.text('Entregar na EPASNAMIBE-E.P., caso ainda não o tenha', 25, 131);
    doc.text('feito, os documentos em falta (croquis de localização, título de ', 10, 134);
    doc.text('propriedade ou contrato de arrendamento, certidão matricial).  ', 10, 137);
    doc.setFontType("bold");
    doc.text('Cláusula 4ª', 10, 140);
    doc.text('_________', 10, 140);
    doc.setFontType("bold");
    doc.text('Rescisão de contrato 1)', 26, 140);
    doc.setFontType("normal");
    doc.text('A rescisão de contrato deve ser', 59, 140);
    doc.text('obrigatoriamente comunicada pelo consumidor à EPASNAMIBE-E.P.', 10, 143);
    doc.text('com pelo menos, 30 dias de antecedência sobre a data da respectiva', 10, 146);
    doc.text('produção de efeitos.', 10, 149);
    doc.setFontType("bold");
    doc.text('2)', 36, 149);
    doc.setFontType("normal");
    doc.text('O consumidor mantém-se obrigado ao', 39, 149);
    doc.text('pagamento das facturas até efectiva rescisão do contrato, o que ', 10, 152);
    doc.text('implica acesso ao contador e suspensão de abastecimento. ', 10, 155);
    doc.setFontType("bold");
    doc.text('3)', 86, 155);
    doc.setFontType("normal");
    doc.text('Caso', 89, 155);
    doc.text('não seja cumprido o estipulado no número 9 da cláusula anterior, a ', 10, 158);
    doc.text('EPASNAMIBE-E.P. poderá proceder à rescisão unilateral deste  ', 10, 161);
    doc.text('contrato e consequentemente interromper o abastecimento de', 10, 164);
    doc.text('água.', 10, 167);
    doc.setFontType("bold");
    doc.text('Cláusula 5ª Continuidade e suspensão do fornecimento 1)', 18, 167);
    doc.text('_________', 18, 167);
    doc.setFontType("normal");
    doc.text('O', 97, 167);
    doc.text('fornecimento de água poderá ser suspenso nas seguintes situações:', 10, 170);
    doc.setFontType("bold");
    doc.text('a)', 10, 173);
    doc.setFontType("normal");
    doc.text('Exigência de serviço público;', 13, 173);
    doc.setFontType("bold");
    doc.text('b)', 50, 173);
    doc.setFontType("normal");
    doc.text('Existência de avarias ou obras nas', 53, 173);
    doc.text('redes gerais de distribuição ou nas canalizações anteriores;', 10, 176);
    doc.setFontType("bold");
    doc.text('c)', 86, 176);
    doc.setFontType("normal");
    doc.text('Casos', 89, 176);
    doc.text('de força maior; ', 10, 179);
    doc.setFontType("bold");
    doc.text(' d)', 29, 179);
    doc.setFontType("normal");
    doc.text('Actos imputáveis ao consumidor.', 33, 179);
    doc.setFontType("bold");
    doc.text(' 2)', 74, 179);
    doc.setFontType("normal");
    doc.text('Para efeitos', 78, 179);
    doc.text('da ultima alínea do ponto anterior, são considerados actos ', 10, 182);
    doc.text('imputáveis ao consumidor: ', 10, 185);
    doc.setFontType("bold");
    doc.text(' a)', 44, 185);
    doc.setFontType("normal");
    doc.text(' Falta de condições de salubridade das', 47, 185);
    doc.text(' canalizações;', 9, 188);
    doc.setFontType("bold");
    doc.text(' b)', 27, 188);
    doc.setFontType("normal");
    doc.text(' Falta de pagamentos das contas de consumo ou', 30, 188);
    doc.text('outras dívidas relacionadas com o abastecimento ou com o contrato ', 10, 191);
    doc.text('estabelecido; ', 10, 194);
    doc.setFontType("bold");
    doc.text(' c)', 27, 194);
    doc.setFontType("normal");
    doc.text(' Recusa da entrada do funcionário da EPASNAMIBE-', 30, 194);
    doc.text('E.P. devidamente credenciado para a inspecção das canalizações ', 10, 197);
    doc.text('interiores e impedimento do mesmo para leitura, verificação, ', 10, 200);
    doc.text('substituição ou levantamento do contador; ', 10, 203);
    doc.setFontType("bold");
    doc.text(' c)', 63, 203);
    doc.setFontType("bold");
    doc.text(' d)', 63, 203);
    doc.setFontType("normal");
    doc.text(' Viciação do contador', 66, 203);
    doc.text('ou utilização do meio fraudulento para consumir água;', 10, 206);
    doc.setFontType("bold");
    doc.text(' e)', 78, 206);
    doc.setFontType("normal");
    doc.text(' Efectuar', 81, 206);
    doc.text('venda de água a terceiros sem o consentimento da EPASNAMIBE-', 10, 209);
    doc.text('E.P.;', 10, 212);
    doc.setFontType("bold");
    doc.text(' 3)', 17, 212);
    doc.setFontType("normal");
    doc.text(' A suspensão do fornecimento por causas imputáveis ao ', 20, 212);
    doc.text('consumidor não o isenta do pagamento da tarifa fixa mensal;', 10, 215);
    doc.setFontType("bold");
    doc.text(' 4)', 87, 215);
    doc.setFontType("normal");
    doc.text('suspensão do fornecimento da EPASNAMIBE-E.P. por falta de', 10, 218);
    doc.text('pagamentos das contas de consumo ou por outras dívidas ', 10, 221);
    doc.text('advertência notificada por escrito ao consumidor com antecedência ', 10, 224);
    doc.text('mínima de 5 dias da data em que a suspensão venha a ter lugar. ', 10, 227);
    doc.setFontType("bold");
    doc.text('Cláusula 6ª Leitura 1)', 10, 230);
    doc.setFontType("normal");
    doc.text('Para efeitos de leitura ou verificação deve ser', 40, 230);
    doc.text('permitido ao pessoal credenciado da EPASNAMIBE-E.P., livre acesso', 10, 233);
    doc.text('ao contador.', 10, 236);
    doc.setFontType("bold");
    doc.text('2)', 27, 236);
    doc.setFontType("normal");
    doc.text('A leitura ou verificação será efectuada dentro do ', 30, 236);
    doc.text('horário normal de trabalho da EPASNAMIBE-E.P.. ', 10, 239);
    doc.setFontType("bold");
    doc.text('3)', 74, 239);
    doc.setFontType("normal");
    doc.text('Quando, por', 77, 239);
    doc.text('motivo imputável ao consumidor, não tenha sido efectuada leitura ', 10, 242);
    doc.text('do contador, é-lhe aplicável um valor por estimativa.', 10, 245);
    doc.setFontType("bold");
    doc.text('Cláusula 7ª Determinação do consumo por estimativa 1)', 10, 248);
    doc.text('_________', 10, 248);
    doc.setFontType("normal");
    doc.text('O', 86, 248);
    doc.text('consumo por estimativa aplicar-se-á quando não for possível a ', 10, 251);
    doc.text('leitura real do contador pela EPASNAMIBE-E.P..', 10, 254);
    doc.setFontType("bold");
    doc.text('2)', 72, 254);
    doc.setFontType("normal");
    doc.text('A estimativa é', 75, 254);
    doc.text('calculada face ao histórico de consumos do consumidor (média ', 10, 257);
    doc.text('obtida das duas últimas leituras) sendo o acerto realizado na factura ', 10, 260);
    doc.text('seguinte, com base na leitura real. ', 10, 263);
    doc.setFontType("bold");
    doc.text('3)', 54, 263);
    doc.setFontType("normal");
    doc.text('Sempre que não exista', 58, 263);
    doc.text('histórico de consumo, a estimativa a aplicar será calculada com base ', 10, 266);
    doc.text('da média de consumo dos consumidores da mesma tipologia. ', 10, 269);
    doc.setFontType("bold");
    doc.text('Cláusula 8ª Preços', 10, 272);
    doc.text('_________', 10, 272);
    doc.setFontType("normal");
    doc.text('A EPASNAMIBE-E.P. informará ao consumidor os', 36, 272);
    doc.text('preços da água fornecida de acordo com o sistema tarifário', 10, 275);
    doc.text('aprovado pela Entidade competente.', 10, 278);
    doc.setFontType("bold");
    doc.text('Cláusula 9ª Facturas', 57, 278);
    doc.text('_________', 57, 278);
    doc.setFontType("normal");
    doc.text('As', 86, 278);
    doc.text('facturas são emitidas com periodicidade mensal, devendo o ', 10, 281);
    doc.text('consumidor dirigir-se aos serviços de atendimento, para entrega e ', 10, 284);
    doc.text('pagamento da mesma. ', 120, 32);
    doc.setFontType("bold");
    doc.text('Cláusula 10ª Pagamentos 1)', 150, 32);
    doc.text('_________', 150, 32)
    doc.setFontType("normal");
    doc.text('O', 188, 32);
    doc.text('pagamento da factura deve ser efectuado de acordo com', 120, 35)
    doc.text('o valor da respectiva factura dentro do prazo limite de pa-', 120, 38)
    doc.text('gamento, nos locais e modalidades especificamente defini-', 120, 41)
    doc.text('das para o efeito pela EPASNAMIBE-E.P..  ', 120, 44)
    doc.setFontType("bold");
    doc.text(' 2)', 174, 44);
    doc.setFontType("normal");
    doc.text('A liquidação ', 178, 44);
    doc.text('da factura poderá ser feita nas seguintes modalidades: ', 120, 47);
    doc.setFontType("bold");
    doc.text('a)', 120, 50);
    doc.setFontType("normal");
    doc.text('Pagamento', 123, 50);
    doc.text('em dinheiro nos serviços de atendimento da', 138, 50);
    doc.text('EPASNAMIBE-E.P.;', 120, 53);
    doc.setFontType("bold");
    doc.text('b)', 147, 53);
    doc.setFontType("normal");
    doc.text('Pagamento no terminal multicaixa', 150, 53);
    doc.text('disponível nos serviços de atendimento', 120, 56);
    doc.text('da EPASNAMIBE-', 170, 56);
    doc.text('E.P.;', 120, 59);
    doc.setFontType("bold");
    doc.text('c)', 127, 59);
    doc.setFontType("normal");
    doc.text(' Pagamento num terminal multicaixa, utilizando para', 129, 59);
    doc.text('para o efeito', 120, 62);
    doc.text('a referência multicaixa indicada na respectiva fa-', 135, 62);
    doc.text('ctura;', 120, 65);
    doc.setFontType("bold");
    doc.text('d)', 128, 65);
    doc.setFontType("normal");
    doc.text('Pagamento por transferência bancária para a conta', 132, 65);
    doc.text('indicada na factura, com apresentação do respectivo compro-', 120, 68);
    doc.text('vativo nos serviços de atendimento da EPASNAMIBE-E.P... ', 120, 71);
    doc.setFontType("bold");
    doc.text('Cláusula 11ª Multas 1)', 120, 74);
    doc.text('_________', 120, 74);
    doc.setFontType("normal");
    doc.text('Para além das penalizações imputá-', 150, 74);
    doc.text('veis ao consumidor previstas nos números anteriores, está ', 120, 77);
    doc.text('também o consumidor sujeito ao ', 120, 80);
    doc.text('pagamento de uma multa ', 162, 80);
    doc.text('sempre que se verifiquem situações de esbanjamento ou ', 120, 83);
    doc.text('desperdício de água por parte deste, bem como nos casos', 120, 86);
    doc.text('de danificação do contador ou da rede pública de água. O', 120, 89);
    doc.text('valor da multa a aplicar será conforme a gravidade dos da-', 120, 92);
    doc.text('nos causados.', 120, 95);
    doc.setFontType("bold");
    doc.text('2)', 139, 95);
    doc.setFontType("normal");
    doc.text('Está igualmente prevista uma multa defini-', 142, 95);
    doc.text('da como taxa de religação após corte por atraso de pagame-', 120, 98);
    doc.text('nto, conforme tarifário em vigor.', 120, 101);
    doc.setFontType("bold");
    doc.text('3)', 160, 101);
    doc.setFontType("normal");
    doc.text('Para além das multas refe-', 163, 101);
    doc.text('ridas nos números anteriores, o consumidor pode também ', 120, 104);
    doc.text('estar sujeito a um processo judicial instaurado pela EPASNA-', 120, 107);
    doc.text('MIBE-E.P. contra si.', 120, 110);
    doc.setFontType("bold");
    doc.text(' Cláusula 12ª Reclamações 1)', 146, 110);
    doc.text('__________', 147, 110);
    doc.setFontType("normal");
    doc.text('Aos co-', 186, 110);
    doc.text('nsumidores assiste o direito de reclamar, por qualquer meio,', 120, 113);
    doc.text('perante a EPASNAMIBE-E.P., contra qualquer acto ou omi-', 120, 116);
    doc.text('ssão desta ou dos respectivos serviços ou agentes, que', 120, 119);
    doc.text('tenham lesado os seus direitos ou interesses legítimos legal-', 120, 122);
    doc.text('mente protegidos.', 120, 125);
    doc.setFontType("bold");
    doc.text(' 2)', 143, 125);
    doc.setFontType("normal");
    doc.text('Os serviços de atendimento ao público', 147, 125);
    doc.text('dispõem de formulário próprio onde os consumidores podem ', 120, 128);
    doc.text('apresentar as suas reclamações.', 120, 131);
    doc.setFontType("bold");
    doc.text(' 3)', 162, 131);
    doc.setFontType("normal");
    doc.text(' A reclamação é apreciada', 165, 131);
    doc.text('pela EPASNAMIBE-E.P. no prazo de 22 dias úteis, informando', 120, 134);
    doc.text('o reclamante da respectiva resposta devidamente fundamenta', 120, 137);
    doc.text('da.', 120, 140);
    doc.setFontType("bold");
    doc.text('Cláusula 13ª Vigência', 125, 140);
    doc.text('__________', 125, 140);
    doc.setFontType("normal");
    doc.text('O presente contrato considera-se', 155, 140);
    doc.text('em vigor a partir da data da sua assinatura, da ligação da água', 120, 143);
    doc.text('ou da compra ou arrendamento do ', 120, 146);
    doc.text('local de consumo (este últi-', 164, 146);
    doc.text('mo apenas aplicável a transferências de titularidade).', 120, 149);
    doc.setFontType("bold");
    doc.text('Cláusula 14ª Disposições finais', 120, 152);
    doc.text('__________', 120, 152);
    doc.setFontType("normal");
    doc.text(' As partes obrigam-se ao', 163, 152);
    doc.text('ao cumprimento do presente contrato.', 120, 155);
    doc.setFontType("bold");
    doc.text('Cláusula 15ª Foro 1)', 168, 155);
    doc.text('__________', 168, 155);
    doc.setFontType("normal");
    doc.text('Em caso de eventuais litígios, as partes primarão pela resolu-', 120, 158);
    doc.text('ção amigável.', 120, 161);
    doc.setFontType("bold");
    doc.text('2)', 138, 161);
    doc.setFontType("normal");
    doc.text('Na impossibilidade do disposto no número ', 142, 161);
    doc.text('anterior, será competente o tribunal de Moçâmedes.', 120, 164);

    doc.line(10, doc.internal.pageSize.height - 9, 194, doc.internal.pageSize.height - 9); // vertical line
/*     doc.text(dataActual + ' ' + moment().format("H:m"), 11, 285); */
    doc.text('Página: ' + numberPage + '/2' , 180, 285);
    doc.setFontSize(6);

    doc.text("NIF: " + empresa.taxRegistrationNumber + " - " + empresa.companyName + " / " + empresa.addressDetail + " / " + empresa.telefone + " / " + empresa.email, 105, doc.internal.pageSize.height - 6, null, null, 'center');
    doc.setFontSize(7);
    doc.text('Processado por programa validado nº 4/AGT119', 105, doc.internal.pageSize.height - 3, null, null, 'center');

    if (report == 'imprimir') {
      doc.autoPrint();
      window.open(doc.output("bloburl")); //opens the data uri in new window
    } else {
      doc.save(contrato.id + '.pdf'); /* download the file immediately on loading */
    }
  }
}
