import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgendamentoRoteiroComponent } from './agendamento-roteiro/agendamento-roteiro.component';
import { LeituraComponent } from './leituras/leitura.component';
import { PeriodicidadeRoteiroComponent } from './periodicidade-roteiro/periodicidade-roteiro.component';
import { CreateRoteiroComponent } from './rota-header/create-roteiro/create-roteiro';

const routes: Routes = [
  { path: 'rotas',
    children: [
      {  path: 'roteiros',        component: CreateRoteiroComponent },
      {  path: 'agendamentos',    component: AgendamentoRoteiroComponent },
      {  path: 'periodicidade',   component: PeriodicidadeRoteiroComponent },
      {  path: 'leituras',        component: LeituraComponent },

    ]
  }

]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class RotasRoutingModule { }
