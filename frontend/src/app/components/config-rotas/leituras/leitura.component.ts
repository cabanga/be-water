import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { ExcelService } from 'src/app/services/excel.service';
import { AuthService } from 'src/app/providers/auth/auth.service';

import * as moment from 'moment';
import { RotaService } from '../rotas.service';

@Component({
  selector: 'app-leitura',
  templateUrl: './leitura.component.html',
  styleUrls: ['./leitura.component.css']
})
export class LeituraComponent implements OnInit {

  @ViewChild("search") search;
  @ViewChild("orderBy") orderBy;
  @ViewChild("searchData") searchData;
  @ViewChild("mostrarResultado") mostrarResultado;

  public currentUser: any;
  private loading: boolean = false;

  private has_rotas_header: boolean = false;

  private rotas_header: any = [];
  private rotaRuns: any = [];

  private corridasLeiturasArray = [];
  private leituraToProcess: boolean = true;

  private rota_header = {
    id: null,
    descricao: null,
    data_inicio: null,
    data_fim: null,
    leitor_id: null,
    user_nome: null,
    estado: null,
  }

  private rotarun = {
    id: null,
    nome_cliente: null,
    rota_header_id: null,
    conta_id: null,
    servico_id: null,
    id_servico: null,
    provincia_id: null,
    municipio_id: null,
    bairro_id: null,
    has_distrito: null,
  }

  private items: any = [];

  view_user = false

  leitura: any = {
    leitura: null,
    ultima_leitura: null,
    data_leitura: null,
    nao_leitura: null,
    motivo: null
  }

  motivos_nao_leitura_list: any = []

  constructor(
    private auth: AuthService,
    private http: HttpService,
    private _rotaService: RotaService,
    private configService: ConfigService,
    private excelService: ExcelService
  ) {
    this.currentUser = this.auth.currentUserValue;
    this._rotaService.getMotivosNaoLeitura()
    .subscribe(
      response => {
        this.motivos_nao_leitura_list = response.data
      }
    )
  }

  exportAsPDF(): void {

  }

  imprimirPDF(): void {

  }


  ngOnInit() {
    this.getPageFilterData(1)
  }

  onReset(){
    this.leitura = {
      leitura: null,
      ultima_leitura: null,
      data_leitura: null,
      nao_leitura: null,
      motivo: null
    }
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      this.http.filters.pagination.page = 1;
      return;
    }
    this.http.filters.pagination.page = page;
    this.listarLeituras();
  }


  private listarLeituras() {
    this.loading = true
    this.http.__call('leitura/listagem', this.http.filters).subscribe(
      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;
        this.items = Object(response).data.data;
        this.loading = false
      }
    );
  }

  private listarRotaRunByLeitor() {
    this.http.call_get('rota-header/getRotasHeaderWithRunsByLeitor/' + this.currentUser.user.id, null).subscribe(
      response => {
        this.rotas_header = Object(response).data;
        if (Object(response).data.length > 0) {
          this.has_rotas_header = true;
        }
        else {
          this.has_rotas_header = false;
        }

      }
    );

  }

  private listarRotaRunByRotaHeader() {
    this.configService.loaddinStarter('start');
    this.http.call_get('rota-run/getRotasRunPendentesByRotaHeader/' + this.rota_header.id, null).subscribe(
      response => {
        this.rotaRuns = Object(response).data;
        this.configService.loaddinStarter('stop');
      }
    );
  }


  private setNullUser() {
    this.rota_header.leitor_id = null;
    this.rota_header.user_nome = null;
    this.view_user = false;
  }


  private setDataRotaHeader(item: any) {

    for (let index = 0; index < this.rotas_header.length; index++) {

      if (this.rotas_header[index].id == this.rota_header.id) {
        this.rota_header.descricao = this.rotas_header[index].nome;
        this.rota_header.data_inicio = this.rotas_header[index].data_inicio;
        this.rota_header.data_fim = this.rotas_header[index].data_fim;
        this.rota_header.leitor_id = this.rotas_header[index].leitor_id;
      }
    }
  }

  private setDataLeiturasRotaRun() {
    //console.log(this.rotaRuns);

    this.corridasLeiturasArray = [];

    for (let index = 0; index < this.rotaRuns.length; index++) {
      if (this.rotaRuns[index].estado_rota_slug == "PENDENTE" && this.rotaRuns[index].leitura != null) {

        //console.log(this.rotaRuns[index].leitura);
        const data = {
          id: this.rotaRuns[index].id,
          rota_header_id: this.rotaRuns[index].rota_header_id,
          rota_header: this.rotaRuns[index].rota_header,
          leitor_id: this.rotaRuns[index].leitor_id,
          local_consumo_id: this.rotaRuns[index].local_consumo_id,
          contador_id: this.rotaRuns[index].contador_id,
          numero_serie: this.rotaRuns[index].numero_serie,
          local_instalacao_id: this.rotaRuns[index].local_instalacao_id,
          conta_id: this.rotaRuns[index].conta_id,
          moradia_numero: this.rotaRuns[index].moradia_numero,
          is_predio: this.rotaRuns[index].is_predio,
          predio_id: this.rotaRuns[index].predio_id,
          predio_nome: this.rotaRuns[index].predio_nome,
          cliente_id: this.rotaRuns[index].cliente_id,
          cliente: this.rotaRuns[index].cliente,
          cliente_morada: this.rotaRuns[index].cliente_morada,
          cliente_telefone: this.rotaRuns[index].cliente_telefone,
          leitura: this.rotaRuns[index].leitura,
          ultima_leitura: this.rotaRuns[index].ultima_leitura,
          periodo: this.rotaRuns[index].periodo,
          data_leitura: this.rotaRuns[index].data_leitura,
          estado_rota_id: this.rotaRuns[index].estado_rota_id,
          estado_rota: this.rotaRuns[index].estado_rota,
          estado_rota_slug: this.rotaRuns[index].estado_rota_slug
        }

        this.corridasLeiturasArray.push({ ...data });

      }

    }

    console.log(this.corridasLeiturasArray);
  }


  private saveLeiturasRotaRun() {

    for (let index = 0; index < this.corridasLeiturasArray.length; index++) {

      this.http.__call('leitura/create', {
        rota_run_id: this.corridasLeiturasArray[index].id,
        contador_id: this.corridasLeiturasArray[index].contador_id,
        leitura: this.corridasLeiturasArray[index].leitura,
        ultima_leitura: this.corridasLeiturasArray[index].ultima_leitura,
        data_leitura: this.corridasLeiturasArray[index].data_leitura,
        user_id: this.corridasLeiturasArray[index].leitor_id
      }).subscribe(
        res => {
          if (Object(res).code == 200) {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.clearFormInputs();
            this.listarRotaRunByRotaHeader();
            this.configService.loaddinStarter('stop');
          } else {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
            this.configService.loaddinStarter('stop');
          }
        }
      )

    }

    this.corridasLeiturasArray = [];
  }

  private clearFormInputs() {
    this.rota_header.id = null,
      this.rota_header.descricao = null,
      this.rota_header.data_inicio = null,
      this.rota_header.data_fim = null,
      this.rota_header.leitor_id = null,
      this.rota_header.user_nome = null
  }

  private clearFormRotaRun() {

    this.rotarun.nome_cliente = null,
    this.rotarun.conta_id = null,
    this.rotarun.servico_id = null,
    this.rotarun.id_servico = null
  }

  _update(){
    this._rotaService.updateLeitura(this.leitura)
    .subscribe((response) => {

      this.listarLeituras()
      this._rotaService._closeModal("closeModalUpdateLeitura")

      this.leitura.leitura = {
        leitura: null,
        ultima_leitura: null,
        data_leitura: null,
        nao_leitura: null,
        motivo: null
      }

    })
  }

  _updateNaoLeituraToLeitura(){
    this.leitura.nao_leitura = false
    this.leitura.motivo = ''

    this._rotaService.updateLeitura(this.leitura)
    .subscribe((response) => {
      this._rotaService._closeModal("closeModalNaoLeitura")
      this._rotaService._closeModal("closeModalUpdateLeitura")
      this.listarLeituras()

      this.leitura = {
        leitura: null,
        ultima_leitura: null,
        data_leitura: null,
        nao_leitura: null,
        motivo: null
      }
    })
  }


  private setDataLeitura(leitura) {
    this.leitura = leitura
  }

}
