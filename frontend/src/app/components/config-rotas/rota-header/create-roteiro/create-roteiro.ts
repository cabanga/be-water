
import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { ConfigModuloService } from 'src/app/services/config-modulo.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { Pagination } from 'src/app/models/pagination';
import { ActivatedRoute, Router } from '@angular/router'
import { RotaService } from '../../rotas.service';

@Component({
  selector: 'app-create-roteiro',
  templateUrl: './create-roteiro.html',
  styleUrls: ['./create-roteiro.css']
})

export class CreateRoteiroComponent implements OnInit {

  public pagination = new Pagination();


  dias = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
    11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31
  ]
  has_quarteirao: boolean = false
  quarteiroes: any = []
  distritos: any = []
  bairros: any = []
  ruas: any = []
  locais: any = []
  novos_locais: any = []
  locais_processar: any = []
  periodicidades: any = []

  new_periodo: any = {
    periodicidade_roteiro_id: null,
    ciclo: null,
    dia_mes: 1
  }

  check_all: boolean
  header_id: number
  rota_header: any = {}
  municipio: any = {}
  searchText = ''

  private filtroServicos = {
    search: null,
    orderBy: null,
    pagination: {
      perPage: 11,
      page: 1,
      lastPage: null,
      total: null
    },
    filter: null
  }

  constructor(
    private configService: ConfigService,
    private config: ConfigModuloService,
    private _rotaService: RotaService,
    private http: HttpService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
      this.header_id = params['slug']
    })
    this._loadingDependencies()
  }

  ngOnInit() {
    this.check_all = false
    this.getPageFilterData(1)
  }


  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this._loadingDependencies();
  }

  getPageFilterDataServicos(page: number) {
    if (this.filtroServicos.pagination.perPage == null) {
      return;
    }
    this.filtroServicos.pagination.page = page;
  }


  _loadingDependencies() {
    this._rotaService.getPeriodicidades()
    .subscribe( response => {
      this.periodicidades = response.data
    })

    this.http.call_get(`rota-header/${this.header_id}`, null).subscribe(
      response => {
        let rota = Object(response).data
        this.rota_header = rota
        this.municipio = rota.municipio
        this._getDistritoOfMunicipio(rota.municipio.id)
        this._getLocaisByMunicipio(rota.municipio.id)
      }
    )
  }

  _changeDistrito(id: number) {
    this._getLocaisByDistrito(id)

    this.http.call_get(`bairro/getBairrosByMunicipio/${id}`, null)
    .subscribe(
      (response: any) => {
        this.bairros = response.data
      }
    )
  }

  _getDistritoOfMunicipio(id) {
    this.http.call_get(`distrito/getDistritosByMunicipio/${id}`, null)
    .subscribe(
      response => {
        let data = Object(response).data
        this.distritos = data
      }
    )
  }

  _changeBairro(id) {
    this._getLocaisByBairro(id)

    this.http.call_get(`bairro/getBairroById/${id}`, null)
    .subscribe(
      (response: any) => {
        this.has_quarteirao = response.data.has_quarteirao

        if(response.data.has_quarteirao){
          this._getQuarteirosDoBairro(id)
        }else{
          this._getRuasDoBairro(id)
        }
      }
    )
  }

  _getQuarteirosDoBairro(id){
    this.http.call_get(`quarteirao/getQuarteiraosByBairro/${id}`, null)
    .subscribe(
      response => {
        this.quarteiroes = Object(response).data
      }
    )
  }

  _getRuasDoBairro(id){
    this.http.call_get(`rua/selectBoxByBairro/${id}`, null)
    .subscribe(
      response => {
        this.ruas = Object(response)
      }
    )
  }

  _changeQuarteirao(id){
    this._getLocaisByQuarteirao(id)

    this.http.call_get(`rua/getRuasByQuarteirao/${id}`, null)
    .subscribe(
      response => {
        this.ruas = Object(response).data
      }
    )
  }

  _changeRua(id){
    this._getLocaisByRua(id)
  }

  //=================================================================
  _getLocaisByMunicipio(municipio_id) {
    this._rotaService.getLocaisByMunicipio(municipio_id)
    .subscribe(response => {
      this._init_locais(response.locais_de_consumo)
    }),
    (error) => {
      if (!error.ok) {
        console.log(error)
      }
    }
  }

  _getLocaisByDistrito(distrito_id) {
    this._rotaService.getLocaisByDistrito(distrito_id)
    .subscribe(response => {
      this._init_locais(response.locais_de_consumo)
    }),
    (error) => {
      if (!error.ok) {
        console.log(error)
      }
    }
  }

  _getLocaisByBairro(bairro_id) {
    this._rotaService.getLocaisByBairro(bairro_id)
      .subscribe(response => {
        this._init_locais(response.locais_de_consumo)
      }),
      (error) => {
        if (!error.ok) {
          console.log(error)
        }
      }
  }

  _getLocaisByQuarteirao(quarteirao_id) {
    this._rotaService.getLocaisByDistrito(quarteirao_id)
    .subscribe(response => {
      this._init_locais(response.locais_de_consumo)
    }),
    (error) => {
      if (!error.ok) {
        console.log(error)
      }
    }
  }

  _getLocaisByRua(rua_id) {
    this._rotaService.getLocaisByRua(rua_id)
    .subscribe(response => {
      this._init_locais(response.locais_de_consumo)
    }),
    (error) => {
      if (!error.ok) {
        console.log(error)
      }
    }
  }

  _init_locais(locais) {
    this.configService.loaddinStarter('start')

    let locais_merge = []

    for(let local of locais){
      var prop = { checkbox: false }
      locais_merge.push ({...local, ...prop})
    }

    this.locais = locais_merge
    this.pagination.total = locais.length

    this.http.filters.pagination.lastPage = locais.lastPage;
    this.http.filters.pagination.page = locais.page;
    this.http.filters.pagination.total = locais.total;
    this.http.filters.pagination.perPage = locais.perPage;

    this.configService.loaddinStarter('stop')
  }

  //===========================================================================

  _addMultiples(){
    for(let local of this.locais){
      if(local.checkbox){
        this._addNewRota(local)
      }
    }
  }

  _checkAll(event){
    this.check_all = event.target.checked

    for(let local of this.locais){
      local.checkbox = event.target.checked
    }
  }

  _addNewRota(local) {
    this._initNewColletion( local )

    this.novos_locais.push( this._initNewColletion(local))
    var index = this.locais.indexOf(local)
    if (index !== -1) { this.locais.splice(index, 1) }
    this.pagination.total = this.locais.length
  }

  _removeRota(local) {
    this.locais.push(local)
    var index = this.novos_locais.indexOf(local)
    if (index !== -1) { this.novos_locais.splice(index, 1) }
    this.pagination.total = this.locais.length
  }

  _initNewColletion(local){
    let new_local = {
      cil: local.cil,
      contaDescricao: local.contaDescricao,
      is_predio: local.is_predio,
      latitude: local.latitude,
      local_consumo_id: local.local_consumo_id,
      local_instalacao_id: local.local_instalacao_id,
      longitude: local.longitude,
      moradia_numero: local.moradia_numero,
      numero_conta: local.numero_conta,
      predio_andar: local.predio_andar,
      predio_nome: local.predio_nome,
    }

    return new_local
  }

  _saveAndContinue(rota_header_id){
    this._createProcess(rota_header_id)
    this._closeModal("closeModal")
  }

  _save(rota_header_id){
    this._createProcess(rota_header_id)
    this._closeModal("closeModal")
    this.router.navigate(['/Rotas'])
  }

  _createProcess(rota_header_id) {
    this.configService.loaddinStarter('start');

    for (let local of this.novos_locais) {
      let new_obj = {
        rota_header_id: rota_header_id,
        local_consumo_id: local.local_consumo_id,
        estado_rota_id: 1,
        dia_semana: 2,
        dia_mes: this.new_periodo.dia_mes,
        periodicidade_roteiro_id: this.new_periodo.periodicidade_roteiro_id,
        ciclo: this.periodicidades.find(periodo => periodo.id == this.new_periodo.periodicidade_roteiro_id).ciclo
      }

      this.http.__call('rota-run/create', new_obj).subscribe(
        res => {
          if (Object(res).code == 200) {
            this.configService.showAlert("Rotas registadas com sucesso", 'alert-success', true)
          } else {
            this.configService.showAlert("Erro ao registar rotas", 'alert-danger', true);
          }
        }
      )
    }

    this.configService.loaddinStarter('stop')
    this.novos_locais = []
    this._updateListRotasRun(rota_header_id)
  }

  _updateListRotasRun(id:number){
    this.http.call_get(`rota-run/getRotasRunByRotaHeader/${id}`, null).subscribe(
      response => {
        let rotaRuns = Object(response).data
        this._rotaService.listRotasHeadersChange.emit(rotaRuns)
      }
    )
  }

  _closeModal(closeModal){
    var action = document.getElementById(closeModal);
    action.click();
  }



}
