import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RotaHeaderComponent } from './rota-header.component';


const routes: Routes = [
  {
    path: '',
    component: RotaHeaderComponent,
    data: { title: 'Lista das Rotas' },
    children: [
      {
        path: '',
        component: RotaHeaderComponent,
        data: {title: 'Lista das Rotas'}
      }
    ]
  }
]



@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class RotaRoutingModule { }
