import { Injectable, EventEmitter } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment'
import { HttpService } from 'src/app/providers/http/http.service'


@Injectable({
  providedIn: 'root'
})

export class RotaService {

  private token = sessionStorage.getItem('sessionToken')
  listRotasHeadersChange = new EventEmitter<any>()

  private headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Authorization', `Bearer ${this.token}`)

  constructor(
    private http: HttpService,
    private _http_client: HttpClient
  ) { }


  getConfig(slug) {
    return this.http.__call(`configuracao/getConfiguracaobySlug/${slug}`, null)
  }

  getMunicipio(id: number) {
    return this._http_client.get(`municipio/getMunicipioById/${id}`)
  }

  getLocaisByProvincia(id: number) {
    return this._http_client.get<any>(`${environment.app_url}api/${environment.apiVersion}/locais_consumo/getByProvincia/${id}`, { 'headers': this.headers })
  }

  getLocaisByMunicipio(id: number) {
    return this._http_client.get<any>(`${environment.app_url}api/${environment.apiVersion}/locais_consumo/getByMunicipio/${id}`, { 'headers': this.headers })
  }

  getLocaisByDistrito(id: number) {
    return this._http_client.get<any>(`${environment.app_url}api/${environment.apiVersion}/locais_consumo/getByDistrito/${id}`, { 'headers': this.headers })
  }

  getLocaisByQuarteirao(id: number) {
    return this._http_client.get<any>(`${environment.app_url}api/${environment.apiVersion}/locais_consumo/getByQuarterao/${id}`, { 'headers': this.headers })
  }

  getLocaisByBairro(id: number) {
    return this._http_client.get<any>(`${environment.app_url}api/${environment.apiVersion}/locais_consumo/getByBairro/${id}`, { 'headers': this.headers })
  }

  getLocaisByRua(id: number) {
    return this._http_client.get<any>(`${environment.app_url}api/${environment.apiVersion}/locais_consumo/getByRua/${id}`, { 'headers': this.headers })
  }

  //=================================================================================================

  getPeriodicidades() {
    return this._http_client.get<any>(
      `${environment.app_url}api/${environment.apiVersion}/periodicidade_dos_roteiros`,
      { 'headers': this.headers }
    )
  }

  CreatePeriodicidade(object: any) {
    return this._http_client.post<any>(
      `${environment.app_url}api/${environment.apiVersion}/periodicidade_dos_roteiros`,
      object,
      { 'headers': this.headers }
    )
  }

  getPeriodicidade(id: number) {
    return this._http_client.get<any>(
      `${environment.app_url}api/${environment.apiVersion}/periodicidade_dos_roteiros/${id}`,
      { 'headers': this.headers }
    )
  }

  getPeriodicidadeBySlug(slug: string) {
    return this._http_client.get<any>(
      `${environment.app_url}api/${environment.apiVersion}/periodicidade_dos_roteiros/by_slug/${slug}`,
      { 'headers': this.headers }
    )
  }

  updatePeriodicidade(object: any) {
    return this._http_client.patch<any>(
      `${environment.app_url}api/${environment.apiVersion}/periodicidade_dos_roteiros/${object.id}`,
      object,
      { 'headers': this.headers }
    )
  }

  //=================================================================================================

  getAgendamentos() {
    return this._http_client.get<any>(
      `${environment.app_url}api/${environment.apiVersion}/agendamentos_dos_roteiros`,
      { 'headers': this.headers }
    )
  }

  CreateAgendamento(object: any) {
    return this._http_client.post<any>(
      `${environment.app_url}api/${environment.apiVersion}/agendamentos_dos_roteiros`,
      object,
      { 'headers': this.headers }
    )
  }

  getAgendamento(id: number) {
    return this._http_client.get<any>(
      `${environment.app_url}api/${environment.apiVersion}/agendamentos_dos_roteiros/${id}`,
      { 'headers': this.headers }
    )
  }

  updateAgendamento(object: any) {
    return this._http_client.patch<any>(
      `${environment.app_url}api/${environment.apiVersion}/agendamentos_dos_roteiros/${object.id}`,
      object,
      { 'headers': this.headers }
    )
  }

  updateLeitura(object: any) {
    return this._http_client.patch<any>(
      `${environment.app_url}api/${environment.apiVersion}/leituras/${object.id}`,
      object,
      { 'headers': this.headers }
    )
  }

  getMotivosNaoLeitura() {
    return this._http_client.get<any>(
      `${environment.app_url}api/${environment.apiVersion}/leituras/motivos_nao_leitura`,
      { 'headers': this.headers }
    )
  }


  _closeModal(closeModal){
    var action = document.getElementById(closeModal);
    action.click();
  }

}
