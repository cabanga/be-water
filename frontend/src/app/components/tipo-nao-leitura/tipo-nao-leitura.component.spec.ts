import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoNaoLeituraComponent } from './tipo-nao-leitura.component';

describe('TipoNaoLeituraComponent', () => {
  let component: TipoNaoLeituraComponent;
  let fixture: ComponentFixture<TipoNaoLeituraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoNaoLeituraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoNaoLeituraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
