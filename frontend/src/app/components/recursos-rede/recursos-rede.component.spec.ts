import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecursosRedeComponent } from './recursos-rede.component';

describe('RecursosRedeComponent', () => {
  let component: RecursosRedeComponent;
  let fixture: ComponentFixture<RecursosRedeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecursosRedeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecursosRedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
