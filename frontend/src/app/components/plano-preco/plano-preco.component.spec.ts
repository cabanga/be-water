import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanoPrecoComponent } from './plano-preco.component';

describe('PlanoPrecoComponent', () => {
  let component: PlanoPrecoComponent;
  let fixture: ComponentFixture<PlanoPrecoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanoPrecoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanoPrecoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
