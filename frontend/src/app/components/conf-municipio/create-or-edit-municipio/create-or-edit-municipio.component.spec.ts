import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditMunicipioComponent } from './create-or-edit-municipio.component';

describe('CreateOrEditMunicipioComponent', () => {
  let component: CreateOrEditMunicipioComponent;
  let fixture: ComponentFixture<CreateOrEditMunicipioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditMunicipioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditMunicipioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
