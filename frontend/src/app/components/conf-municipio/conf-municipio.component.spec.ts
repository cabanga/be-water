import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfMunicipioComponent } from './conf-municipio.component';

describe('ConfMunicipioComponent', () => {
  let component: ConfMunicipioComponent;
  let fixture: ComponentFixture<ConfMunicipioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfMunicipioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfMunicipioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
