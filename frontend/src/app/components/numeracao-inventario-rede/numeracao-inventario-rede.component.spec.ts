import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumeracaoInventarioRedeComponent } from './numeracao-inventario-rede.component';

describe('NumeracaoInventarioRedeComponent', () => {
  let component: NumeracaoInventarioRedeComponent;
  let fixture: ComponentFixture<NumeracaoInventarioRedeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumeracaoInventarioRedeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumeracaoInventarioRedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
