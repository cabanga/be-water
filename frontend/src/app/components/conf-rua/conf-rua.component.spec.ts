import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfRuaComponent } from './conf-rua.component';

describe('ConfRuaComponent', () => {
  let component: ConfRuaComponent;
  let fixture: ComponentFixture<ConfRuaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfRuaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfRuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
