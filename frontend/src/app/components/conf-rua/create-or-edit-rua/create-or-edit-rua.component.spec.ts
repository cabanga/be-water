import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditRuaComponent } from './create-or-edit-rua.component';

describe('CreateOrEditRuaComponent', () => {
  let component: CreateOrEditRuaComponent;
  let fixture: ComponentFixture<CreateOrEditRuaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditRuaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditRuaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
