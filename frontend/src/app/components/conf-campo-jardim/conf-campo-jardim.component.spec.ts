import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfCampoJardimComponent } from './conf-campo-jardim.component';

describe('ConfCampoJardimComponent', () => {
  let component: ConfCampoJardimComponent;
  let fixture: ComponentFixture<ConfCampoJardimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfCampoJardimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfCampoJardimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
