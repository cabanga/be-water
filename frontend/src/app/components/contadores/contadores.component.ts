import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

@Component({
  selector: 'app-contadores',
  templateUrl: './contadores.component.html',
  styleUrls: ['./contadores.component.css']
})
export class ContadoresComponent implements OnInit {

  private contador = {
    id: null,
    contador: null,
    contador_ID: null,
    marca_id: null,
    modelo_id: null,
    medicao_id: null,
    estado_contador_id: null,
    numero_serie: null,
    id_classe_precisao: null,
    tipo_contador_id: null,
    calibre_id: null,
    caudal_id: null,
    digitos: null,
    ano_fabrico: null,
    ano_instalacao: null,
    observacao: null,
    fabricante_id: null,
    selo: null,
    armazem_id: null,
    ultima_leitura: null,
    ultima_verificacao: null,
    estadoContadorSlug: null
  }

  private contadores_leituras = {
    id: null,
    tipo_registo_id: null,
    data: null,
    ultima_leitura: null
  }

  private contadores_estado = {
    id: null,
    estado_contador_id: null,
    observacao: null
  }

  private items: any = [];
  private filiais: any = [];
  private Modelo: any = [];
  private Marca: any = [];
  private Medicao: any = [];
  private ClassePrecisao: any = [];
  private Fabricante: any = [];
  private TipoContador: any = [];
  private TipoRegisto: any = [];
  private Armazen: any = [];
  private historicos: any = [];
  private EstadoContadores: any = [];
  private Caudal: any = [];
  private Calibre: any = [];
  private estadosContadores = []


  constructor(private http: HttpService, private configService: ConfigService) { }

  ngOnInit() {
    this.getPageFilterData(1);
    this.listarModelo();
    this.listarMarca();
    this.listarArmazen();
    this.listarTipoContador();
    this.listarFabricante();
    this.listarClassePrecisao();
    this.listarMedicao();
    this.listarEstadoContador();
    this.listarCaudal();
    this.listarCalibre();
  }

  private listaContadores() {

    this.configService.loaddinStarter('start');

    this.http.__call('contadores/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.listaContadores();
  }

  private carregando = {
    filial: 'Selecione a filial',
    tecnologia: 'Selecione a tecnologia',
  }

  private clearFormInputs(e) {
    e.target.elements[0].value = null;
    e.target.elements[1].value = null;
    e.target.elements[2].value = null;
  }

  onReset() {
    this.contadores_estado.observacao = null;
    this.contadores_estado.estado_contador_id = null;
    this.contadores_leituras.data = null;
    this.contadores_leituras.tipo_registo_id = null;
    this.contadores_leituras.ultima_leitura = null;
  }

  private registerleitura(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    {
      if (this.contadores_leituras.tipo_registo_id == null) {
        this.configService.showAlert("É obrigatório selecionar o Tipo de Registo", 'alert-danger', true);
        this.configService.loaddinStarter('stop');
      } else {
        if (this.contadores_leituras.data == null) {
          this.configService.showAlert("É obrigatório selecionar Data", 'alert-danger', true);
          this.configService.loaddinStarter('stop');
        } else {
          this.http.__call('contador-leitura/create/' + this.contador.id, this.contadores_leituras).subscribe(
            res => {
              if (Object(res).code == 500) {
                this.configService.showAlert(Object(res).message, 'alert-danger', true);
                this.configService.loaddinStarter('stop');
              } else {
                this.configService.showAlert(Object(res).message, 'alert-success', true);
                this.listaContadores();
                this.clearFormInputs(e);
                this.configService.loaddinStarter('stop');
              }
            }
          )
        }
      }
    };
  }

  private LeiturabyContador() {
    this.http.call_get('contador-leitura/leitura/' + this.contador.id, null).subscribe(
      data => {
        this.contadores_leituras.ultima_leitura = Object(data).data.ultima_leitura;
      }
    );
  }

  private getEstadoFilhosByEstadoPai() {
    this.loadingEstado.estado = 'Carregando...';
    this.http.call_get('contador/update/getestados/' + this.contador.id, null).subscribe(
      response => {
        this.estadosContadores = Object(response).data;
        this.loadingEstado.estado = 'Selecione o Estado';
      }
    );
  }

  private listarFilial() {
    this.carregando.filial = 'Carregando...';
    this.http.call_get('filial/selectBox', null).subscribe(
      response => {
        this.filiais = Object(response).data;
        this.carregando.filial = 'Selecione a Filial';
      }
    );
  }

  private listarModelo() {
    this.http.call_get('modelo/selectBox', null).subscribe(
      response => {
        this.Modelo = Object(response);
      }
    );
  }

  private listarMarca() {
    this.http.call_get('marca/selectBox', null).subscribe(
      response => {
        this.Marca = Object(response);
      }
    );
  }

  private listarMedicao() {
    this.http.call_get('medicao/selectBox', null).subscribe(
      response => {
        this.Medicao = Object(response);
      }
    );
  }

  private listarClassePrecisao() {
    this.http.call_get('classe-precisao/selectBox', null).subscribe(
      response => {
        this.ClassePrecisao = Object(response);
      }
    );
  }

  private listarFabricante() {
    this.http.call_get('fabricante/selectBox', null).subscribe(
      response => {
        this.Fabricante = Object(response);
      }
    );
  }

  private listarTipoContador() {
    this.http.call_get('tipo-contador/selectBox', null).subscribe(
      response => {
        this.TipoContador = response;
      }
    );
  }

  private listarArmazen() {
    this.http.call_get('armazem/selectBox', null).subscribe(
      response => {
        this.Armazen = Object(response);
      }
    );
  }

  private listarEstadoContador() {
    this.http.call_get('estado-contador/selectBox', null).subscribe(
      response => {
        this.EstadoContadores = Object(response);
      }
    );
  }
  private listarCaudal() {
    this.http.call_get('caudal/selectBox', null).subscribe(
      response => {
        this.Caudal = Object(response);
      }
    );
  }
  private listarCalibre() {
    this.http.call_get('calibre/selectBox', null).subscribe(
      response => {
        this.Calibre = Object(response);
      }
    );
  }

  private setDataContador(contador: any) {
    this.contador = contador;
  }

  private loadingEstado = {
    estado: 'Selecione o estado',
  }

  private editarEstadoContador(e) {

    if (this.contadores_estado.estado_contador_id == null) {
      this.configService.showAlert("É obrigatório selecionar o Estado", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else if (this.contadores_estado.observacao == null) {
      this.configService.showAlert("É obrigatório descrever Observação sobre a atualização o estado do contador", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
      this.configService.loaddinStarter('start');
      this.http.__call('contador/update/estado/' + this.contador.id, this.contadores_estado).subscribe(
        res => {
          if (Object(res).code == 500) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
          } else {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.listaContadores();
            this.clearFormInputs(e);
            this.configService.loaddinStarter('stop');
            this.getPageFilterData(1);

          }
        }
      );
    }
  }

  private getHistoricoEstado() {

    this.configService.loaddinStarter('start');

    this.http.call_get('historico/estado/contador/' + this.contador.id, null).subscribe(
      response => {
        this.historicos = Object(response).data;
        this.configService.loaddinStarter('stop');
      }
    )
  }

  private ini() {
    this.contador = {
      id: null,
      contador: null,
      contador_ID: null,
      marca_id: null,
      modelo_id: null,
      medicao_id: null,
      numero_serie: null,
      id_classe_precisao: null,
      tipo_contador_id: null,
      calibre_id: null,
      caudal_id: null,
      digitos: null,
      ano_fabrico: null,
      ano_instalacao: null,
      observacao: null,
      fabricante_id: null,
      selo: null,
      armazem_id: null,
      ultima_leitura: null,
      ultima_verificacao: null,
      estado_contador_id: null,
      estadoContadorSlug: null
    }
  }
}
