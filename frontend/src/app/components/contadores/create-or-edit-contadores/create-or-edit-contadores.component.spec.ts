import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditContadoresComponent } from './create-or-edit-contadores.component';

describe('CreateOrEditContadoresComponent', () => {
  let component: CreateOrEditContadoresComponent;
  let fixture: ComponentFixture<CreateOrEditContadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditContadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditContadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
