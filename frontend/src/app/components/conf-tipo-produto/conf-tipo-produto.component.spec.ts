import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoProdutoComponent } from './conf-tipo-produto.component';

describe('ConfTipoProdutoComponent', () => {
  let component: ConfTipoProdutoComponent;
  let fixture: ComponentFixture<ConfTipoProdutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoProdutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoProdutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
