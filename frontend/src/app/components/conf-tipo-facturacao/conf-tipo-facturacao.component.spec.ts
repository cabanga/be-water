import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoFacturacaoComponent } from './conf-tipo-facturacao.component';

describe('ConfTipoFacturacaoComponent', () => {
  let component: ConfTipoFacturacaoComponent;
  let fixture: ComponentFixture<ConfTipoFacturacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoFacturacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoFacturacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
