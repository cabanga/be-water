import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvencaComponent } from './avenca.component';

describe('AvencaComponent', () => {
  let component: AvencaComponent;
  let fixture: ComponentFixture<AvencaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvencaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvencaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
