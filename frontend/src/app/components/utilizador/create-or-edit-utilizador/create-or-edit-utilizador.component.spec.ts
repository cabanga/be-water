import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditUtilizadorComponent } from './create-or-edit-utilizador.component';

describe('CreateOrEditUtilizadorComponent', () => {
  let component: CreateOrEditUtilizadorComponent;
  let fixture: ComponentFixture<CreateOrEditUtilizadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditUtilizadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditUtilizadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
