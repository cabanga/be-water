import { TestBed } from '@angular/core/testing';

import { UtilizadorFormValidationService } from './utilizador-form-validation.service';

describe('UtilizadorFormValidationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UtilizadorFormValidationService = TestBed.get(UtilizadorFormValidationService);
    expect(service).toBeTruthy();
  });
});
