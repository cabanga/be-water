import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedefinirPasswordFirstAcessComponent } from './redefinir-password-first-acess.component';

describe('RedefinirPasswordFirstAcessComponent', () => {
  let component: RedefinirPasswordFirstAcessComponent;
  let fixture: ComponentFixture<RedefinirPasswordFirstAcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedefinirPasswordFirstAcessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedefinirPasswordFirstAcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
