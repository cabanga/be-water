import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UtilizadorAlterarPasswordComponent } from './utilizador-alterar-password.component';

describe('UtilizadorAlterarPasswordComponent', () => {
  let component: UtilizadorAlterarPasswordComponent;
  let fixture: ComponentFixture<UtilizadorAlterarPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UtilizadorAlterarPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UtilizadorAlterarPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
