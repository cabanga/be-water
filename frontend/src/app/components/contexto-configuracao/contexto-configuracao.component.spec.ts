import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContextoConfiguracaoComponent } from './contexto-configuracao.component';

describe('ContextoConfiguracaoComponent', () => {
  let component: ContextoConfiguracaoComponent;
  let fixture: ComponentFixture<ContextoConfiguracaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContextoConfiguracaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContextoConfiguracaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
