import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PontoVendaComponent } from './ponto-venda.component';

describe('PontoVendaComponent', () => {
  let component: PontoVendaComponent;
  let fixture: ComponentFixture<PontoVendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PontoVendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PontoVendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
