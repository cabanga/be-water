import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoLocalInstalacaoComponent } from './conf-tipo-local-instalacao.component';

describe('ConfTipoLocalInstalacaoComponent', () => {
  let component: ConfTipoLocalInstalacaoComponent;
  let fixture: ComponentFixture<ConfTipoLocalInstalacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoLocalInstalacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoLocalInstalacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
