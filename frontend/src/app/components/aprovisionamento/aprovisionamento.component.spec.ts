import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AprovisionamentoComponent } from './aprovisionamento.component';

describe('AprovisionamentoComponent', () => {
  let component: AprovisionamentoComponent;
  let fixture: ComponentFixture<AprovisionamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AprovisionamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AprovisionamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
