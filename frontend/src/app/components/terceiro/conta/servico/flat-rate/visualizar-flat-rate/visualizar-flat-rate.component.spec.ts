import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarFlatRateComponent } from './visualizar-flat-rate.component';

describe('VisualizarFlatRateComponent', () => {
  let component: VisualizarFlatRateComponent;
  let fixture: ComponentFixture<VisualizarFlatRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizarFlatRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarFlatRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
