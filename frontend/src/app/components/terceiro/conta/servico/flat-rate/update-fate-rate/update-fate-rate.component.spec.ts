import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateFateRateComponent } from './update-fate-rate.component';

describe('UpdateFateRateComponent', () => {
  let component: UpdateFateRateComponent;
  let fixture: ComponentFixture<UpdateFateRateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateFateRateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateFateRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
