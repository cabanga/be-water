import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesunificadosComponent } from './clientesunificados.component';

describe('ClientesunificadosComponent', () => {
  let component: ClientesunificadosComponent;
  let fixture: ComponentFixture<ClientesunificadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesunificadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesunificadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
