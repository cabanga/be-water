import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnificarclienteComponent } from './unificarcliente.component';

describe('UnificarclienteComponent', () => {
  let component: UnificarclienteComponent;
  let fixture: ComponentFixture<UnificarclienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnificarclienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnificarclienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
