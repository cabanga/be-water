import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFlatRateServicoComponent } from './edit-flat-rate-servico.component';

describe('EditFlatRateServicoComponent', () => {
  let component: EditFlatRateServicoComponent;
  let fixture: ComponentFixture<EditFlatRateServicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFlatRateServicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFlatRateServicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
