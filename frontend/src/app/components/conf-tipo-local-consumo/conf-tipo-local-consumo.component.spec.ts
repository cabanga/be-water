import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoLocalConsumoComponent } from './conf-tipo-local-consumo.component';

describe('ConfTipoLocalConsumoComponent', () => {
  let component: ConfTipoLocalConsumoComponent;
  let fixture: ComponentFixture<ConfTipoLocalConsumoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoLocalConsumoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoLocalConsumoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
