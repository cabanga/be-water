import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTarifarioComponent } from './conf-tarifario.component';

describe('ConfTarifarioComponent', () => {
  let component: ConfTarifarioComponent;
  let fixture: ComponentFixture<ConfTarifarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTarifarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTarifarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
