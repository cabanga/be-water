import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewLigacaoRamalComponent } from './new-ligacao-ramal.component';

describe('NewLigacaoRamalComponent', () => {
  let component: NewLigacaoRamalComponent;
  let fixture: ComponentFixture<NewLigacaoRamalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewLigacaoRamalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewLigacaoRamalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
