import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmdaDispositivoFormComponent } from './cmda-dispositivo-form.component';

describe('CmdaDispositivoFormComponent', () => {
  let component: CmdaDispositivoFormComponent;
  let fixture: ComponentFixture<CmdaDispositivoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmdaDispositivoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmdaDispositivoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
