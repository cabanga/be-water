import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LtePrePagoFormComponent } from './lte-pre-pago-form.component';

describe('LtePrePagoFormComponent', () => {
  let component: LtePrePagoFormComponent;
  let fixture: ComponentFixture<LtePrePagoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LtePrePagoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LtePrePagoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
