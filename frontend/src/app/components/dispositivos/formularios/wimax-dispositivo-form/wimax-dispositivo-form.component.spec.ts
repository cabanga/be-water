import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WimaxDispositivoFormComponent } from './wimax-dispositivo-form.component';

describe('WimaxDispositivoFormComponent', () => {
  let component: WimaxDispositivoFormComponent;
  let fixture: ComponentFixture<WimaxDispositivoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WimaxDispositivoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WimaxDispositivoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
