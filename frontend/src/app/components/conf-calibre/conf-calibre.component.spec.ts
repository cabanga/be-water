import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfCalibreComponent } from './conf-calibre.component';

describe('ConfCalibreComponent', () => {
  let component: ConfCalibreComponent;
  let fixture: ComponentFixture<ConfCalibreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfCalibreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfCalibreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
