import { TestBed } from '@angular/core/testing';

import { ContratoLTEService } from './contrato-lte.service';

describe('ContratoLTEService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContratoLTEService = TestBed.get(ContratoLTEService);
    expect(service).toBeTruthy();
  });
});
