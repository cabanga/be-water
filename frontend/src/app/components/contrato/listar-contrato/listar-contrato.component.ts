import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';

import { ConfigModuloService } from 'src/app/services/config-modulo.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { RescisaoService } from "src/app/components/reports/rescisao/rescisao.service";
import { ContratoService } from "src/app/components/reports/contrato/contrato.service";

import { AuthService } from 'src/app/providers/auth/auth.service';

import { ExcelService } from 'src/app/services/excel.service';
import * as moment from 'moment';

import { Router } from '@angular/router';
@Component({
  selector: 'app-listar-contrato',
  templateUrl: './listar-contrato.component.html',
  styleUrls: ['./listar-contrato.component.css']
})
export class ListarContratoComponent implements OnInit {


  loadingListarFacturacao = false;

  public currentUser: any;

  private contratos: any = [];
  private cliente = {
    id: null,
    nome: null,
    telefone: null,
    morada: null,
    numero_identidade: null,
    tipo_identidade_id: null,
    tipo_cliente_id: null,
    email: null,
    provincia_id: null,
    provincia: null,
    municipio_id: null,
    municipio: null,
    genero_id: null,
    genero: null,

    conta_id: null,
    numero_conta: null,
    direccao_id: null,
    direccao: null,

    tipo_identidade: null,
    numero_digitos: null,
    numero_identificacao: null,
    tipo_cliente: null,

    gestor_cliente_id: null,
    gestor_cliente: null,
    gestor_cliente_telefone: null
  }

  private contrato = {

    id: null,
    tipo_contracto_id: null,
    tipo_contrato: null,
    tipo_medicao_id: null,
    tipo_medicao: null,
    tipo_medicao_slug: null,
    tipo_facturacao_id: null,
    tipo_facturacao: null,
    tipologia_cliente_id: null,
    tipologia_cliente: null,
    tipologia_cliente_juro_mora: null,
    tipologia_cliente_sujeito_corte: null,
    tipologia_cliente_caucao: null,
    nivel_sensibilidade_id: null,
    nivel_sensibilidade: null,
    objecto_contrato_id: null,
    objecto_contrato: null,
    tarifario: null,
    classe_tarifario: null,
    classe_tarifario_consumo_minimo: null,
    classe_tarifario_consumo_maximo: null,
    numero_habitantes: null,
    data_inicio: null,
    data_fim: null,
    rua_id:null,
    morada_correspondencia_flag: false,
    estado_contrato_id: null,
    estado_contrato: null,
    estado_contrato_slug: null,

    data_rescisao: null,
    motivo_recisao_id: null,
    estado_rescisao_id: null,
    motivo_recisao: null,
    motivo_recisao_flag: null,

    contador_id: null,
    numero_serie: null,

    instalacao_sanitaria_qtd: null,
    reservatorio_flag: null,
    reservatorio_capacidade: null,
    piscina_flag: null,
    piscina_capacidade: null,
    jardim_flag: null,
    campo_jardim_id: null,
    campo_jardim: null,
    poco_alternativo_flag: null,
    fossa_flag: null,
    fossa_capacidade: null,
    acesso_camiao_flag: null,
    anexo_flag: null,
    anexo_quantidade: null,
    caixa_contador_flag: null,
    abastecimento_cil_id: null,
    abastecimento_cil: null,

    cliente_id: null,
    cliente: null,
    info_source: null,
  };

  private contador = {
    marca: null,
    modelo: null,
    numero_serie: null,
    precisao: null,
    medicao: null,
    calibre: null,
    leitura: null,
    data_leitura: null,
    leitura_origem: null,
  }

  private estadosContrato: any = [];
  private municipios: any = [];

  private local_consumos: any = [];

  private motivo_rescisaos: any = [];
  private estado_rescisaos: any = [];

  private estadoActual: string = null;

  clienteNome: string = null;
  cliente_id: string = null;

  private local_instalacao: any;
  private empresa: any;

  private factura = {
    factura_id: null,
    factura_sigla: null,
    status_reason: null,
  }

  private dashboard = {
    countTemporario: null,
    countAtivo: null,
    countContrato: null,
    countInativo: null,
    countCancelado: null
  }
  constructor(
    private auth: AuthService,
    private http: HttpService,
    private configService: ConfigService,
    private _route: Router,
    private excelService: ExcelService,
    private config: ConfigModuloService,
    private reportRescisao: RescisaoService,
   private contratoService: ContratoService
  ) {

    this.currentUser = this.auth.currentUserValue;
  }

  ngOnInit() {
    this.getPageFilterData(1);
    this.dashboardFacturacao();



    this.getEmpresa();
  }

  exportAsXLSX(): void {
/*     var CurrentDate = new Date();
    var nameFile = "Lista_Contratos -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSX")[0], nameFile, '9');
 */  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }


  private dashboardFacturacao() {
    this.http.call_get('contrato/dashboard', null).subscribe(
      response => {
        this.dashboard.countTemporario = this.configService.numberFormat(Object(response).data.countTemporario)
        this.dashboard.countAtivo = this.configService.numberFormat(Object(response).data.countAtivo)
        this.dashboard.countContrato = this.configService.numberFormat(Object(response).data.countContrato)
        this.dashboard.countInativo = this.configService.numberFormat(Object(response).data.countInativo)
        this.dashboard.countCancelado = this.configService.numberFormat(Object(response).data.countCancelado)
      }
    )
  }


  private imprimirContrato(id) {
    this.contratoService.imprimirPDFContrato(id);
  }


  private imprimirPDFRescisao(id) {
    this.reportRescisao.imprimirRescisao(id);
  }


  async getEmpresa() {
    await this.http.call_get('empresa/empresa-user', null).subscribe(
      response => {
        this.empresa = Object(response);
      }
    )
  }

  async getClienteById(id) {
    await this.http.call_get('cliente/getClienteById/' + id, null).subscribe(
      response => {

        //console.log(response);
        this.cliente = Object(response);
        console.log(this.cliente);

      }
    )
  }

  async getLocalInstalacaoById(id) {
    await this.http.call_get('local-instalacao/getLocalInstalacaoById/' + id, null).subscribe(
      response => {
        //console.log(response);
        this.local_instalacao = Object(response);
        console.log(this.local_instalacao);
      }
    )
  }

  private listarContratos() {

    this.loadingListarFacturacao = true
    this.http.__call('contrato/listagem', this.http.filters).subscribe(
      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;
        this.contratos = Object(response).data.data;
        this.loadingListarFacturacao = false
      }
    );
  }



  private carregando = {
    estado: 'Selecione o estado',
  }

  private selectBoxEstadosContrato(id: any) {
    this.carregando.estado = 'Carregando...';

    this.http.call_get('estadocontrato/selectBox/' + id, null).subscribe(
      response => {
        this.estadosContrato = Object(response).data;
        this.carregando.estado = 'Selecione o estado';

      }
    );

  }

  private updateContrato() {

    /*     console.log(this.contrato); */

    this.http.__call('contrato/updatePrecosEstado/' + this.contrato.id, {
      data_inicio: this.contrato.data_inicio,
      data_fim: this.contrato.data_fim,
      estado_contrato_id: this.contrato.estado_contrato_id
    }).subscribe(
      res => {
        if (Object(res).code == 200) {
          this.listarContratos();

          this.configService.showAlert(Object(res).message, "alert-success", true);
          this.getPageFilterData(1);
        } else {
          this.configService.showAlert(Object(res).message, "alert-danger", true);
        }
        this.configService.loaddinStarter('stop');
      }
    );
  }



  //--------------------------------------------------------------------------

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.listarContratos();
  }
  //--------------------------------------------------------------------------


  private getFactura(factura: any) {
    this.factura = factura;
  }




  /**
   * @name "Anualar Factura"
   * @descriptio "Esta Função permite fazer a anulação de uma deteminada factura"
   * @author "caniggia.moreira@itgest.pt"
   * @param start
   * @param end
   */
  private anularFactura(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (e.target.elements[0].value == "" || e.target.elements[2].value == "") {
      this.configService.showAlert('É obrigatório fornecer o nº da factura e o motivo', 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
      this.factura.status_reason = e.target.elements[2].value;
      this.http.__call('factura/anular/' + this.factura.factura_id, this.factura).subscribe(
        res => {
          if (Object(res).code == 500) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
          } else {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.listarContratos();
            this.dashboardFacturacao();
            this.configService.clearFormInputs(e);
          }
          this.configService.loaddinStarter('stop');
        }
      );
    }
  }

  goToPageCreateNotaCredito(factura: any) {
    this._route.navigate(['/facturacao/emitir-nota-de-credito', factura.factura_id]);
  }

  private imprimirFactura(id) {
    //this.configService.imprimirFactura(id, "2ª Via");
  }

  calcularPercentual(f: any) {
    var calc = f.total - f.valor_aberto;
    return (f.pago === 0 && f.status === 'N' && f.valor_aberto == null ? 0 : (calc * 100) / f.total) //(f.total === f.valor_aberto ? 0 : f.pago == 1 ? 100 : (calc * 100) / f.total);
  }


  private setDataContratoCliente(contrato) {

    this.contrato = contrato;
    this.contrato.info_source = contrato.info_source;
    this.contrato.estado_contrato_id = null;

    /*
    if (contrato.estado_contrato_slug == "CANCELADO") this.estadoActual = "Cancelado";
    if (contrato.estado_contrato_slug == "TEMPORARIO") this.estadoActual = "Temporário";
    if (contrato.estado_contrato_slug == "ACTIVO") this.estadoActual = "Activo";
    if (contrato.estado_contrato_slug == "CONTRATO") this.estadoActual = "Contrato";
    if (contrato.estado_contrato_slug == "CANCELADO") this.estadoActual = "Cancelado";
    */

    this.estadoActual = contrato.estado_contrato;

  }


  public getMotivosRescisao() {
    this.configService.loaddinStarter('start');
    this.http.call_get('motivo-rescisao/selectBox/', null).subscribe(
      response => {
        this.motivo_rescisaos = Object(response);
        this.configService.loaddinStarter('stop');
      }
    );
  }

  public getEstadosRescisao() {
    this.configService.loaddinStarter('start');
    this.http.call_get('motivo-rescisao/selectBoxEstado/', null).subscribe(
      response => {
        this.estado_rescisaos = Object(response);
        this.configService.loaddinStarter('stop');
      }
    );
  }

  public getLocaisConsumo(id: number) {

    console.log(id);

    this.configService.loaddinStarter('start');
    this.http.__call('local-consumo/getLocaisConsumoByContrato/' + id, null).subscribe(
      response => {

        this.local_consumos = Object(response);

        console.log(this.local_consumos);

        this.configService.loaddinStarter('stop');
      }
    );

  }

  loadingRescisao = false;
  private saveRescisaoContrato() {
    if(this.contrato.data_rescisao ==null){
      this.configService.showAlert('Data de rescisão é obrigatorio', 'alert-danger', true);
      return;
    }else if(this.contrato.motivo_recisao_id==null || this.contrato.motivo_recisao_id==''){
      this.configService.showAlert('Motivo Recisão é obrigatorio', 'alert-danger', true);
      return;
    }else if(this.contrato.estado_rescisao_id==null || this.contrato.estado_rescisao_id==''){
      this.configService.showAlert('Estado Rescisao é obrigatorio', 'alert-danger', true);
      return;
    }
    this.loadingRescisao = true;
    this.http.__call('contrato/denunciar-rescindir/' + this.contrato.id, {
      contrato_id: this.contrato.id,
      morada_correspondencia_flag: this.contrato.morada_correspondencia_flag,
      tipo_medicao_slug: this.contrato.tipo_medicao_slug,
      contador_id: this.contrato.contador_id,
      motivo_recisao_id: this.contrato.motivo_recisao_id,
      estado_rescisao_id: this.contrato.estado_rescisao_id,
      data_rescisao: this.contrato.data_rescisao,

      /*rua_id: this.contrato.rua_id,
      numero_moradia: this.local_instalacao.numero,
      is_predio: this.local_instalacao.is_predio,
      predio_nome: this.local_instalacao.predio_nome,
      predio_andar: this.local_instalacao.predio_andar,*/

      leitura: this.contador.leitura,
      data_leitura: this.contador.data_leitura,
      leitura_origem: this.contador.leitura_origem

    }).subscribe(
      res => {
        if (Object(res).code == 500) {
          this.configService.showAlert(Object(res).message, 'alert-danger', true);
        } else if (Object(res).code == 201) {
          this.configService.showAlert(Object(res).message, 'alert-warning', true);
          this.configService.loaddinStarter('stop');

        } else {
          this.configService.showAlert(Object(res).message, 'alert-success', true);
          this.configService.loaddinStarter('stop');
          this.listarContratos();
        }
        this.loadingRescisao = false;
      },error =>{
        this.loadingRescisao = false;
      }
      );
  }

  private selectBoxMunicipiosByProvincia(id) {

    this.local_instalacao.distrito_id = null;
    this.local_instalacao.bairro_id = null;
    this.local_instalacao.quarteirao_id = null;
    this.local_instalacao.rua_id = null;

    this.http.call_get('municipio/getMunicipiosByProvincia/' + id, null).subscribe(
      response => {
        this.municipios = Object(response).data;
      }
    );
  }

  loadingRecisao = null;
  private findByIdContrato(id) {
    this.loadingRecisao ="Carregando dados";
    this.http.call_get('contrato/'+id, null).subscribe(response => {
        this.contrato = Object(response).data;
        this.loadingRecisao ='';
    });
  }

  public getConfiguracaos() {

    let result = null;

    const slugs = [
      this.config.provincia_default,
      this.config.estado_rescisao_default,
    ];


    for (let index = 0; index < slugs.length; index++) {

      this.http.__call('configuracao/getConfiguracaobySlug/' + slugs[index], null).subscribe(
        response => {
          if (Object(response).code != 200) {
            result = null;
          }
          else {

            result = Object(response).data;

            if (slugs[index] == this.config.provincia_default) {
              this.local_instalacao.provincia_id = result.valor;
              this.selectBoxMunicipiosByProvincia(result.valor);
            }

            if (slugs[index] == this.config.estado_rescisao_default) {
              this.contrato.estado_rescisao_id = result.valor;
              console.log(this.contrato.estado_rescisao_id)
            }
          }
        });
    }

  }


}
