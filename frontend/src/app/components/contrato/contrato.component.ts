import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from '../../providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { ConfigModuloService } from 'src/app/services/config-modulo.service';
import { ContratoService } from "src/app/components/reports/contrato/contrato.service";
import * as moment from 'moment';
import { BsModalService, BsModalRef, ModalDirective, ModalModule } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-contrato',
  templateUrl: './contrato.component.html',
  styleUrls: ['./contrato.component.css']
})
export class ContratoComponent implements OnInit {


  @ViewChild("search") search;
  @ViewChild("qtd") qtd;
  @ViewChild("valor") valor;
  @ViewChild("serie") serie;
  @ViewChild("observacao") observacao;
  @ViewChild('modalAppendServico') modalAppendServico: ModalDirective;

  public currentUser: any;
  private dataContratoSaved: any;
  private tarifa_fixa_modal_view: boolean = false;
  private clientes: any = [];
  private cliente: any;
  private tipo_contratos: any = [];
  private tipo_medicaos: any = [];
  private niveis_sensibilidade: any = [];
  private tarifarios: any = [];
  private tipologia_clientes: any = [];
  private objecto_contratos: any = [];
  private motivo_recisaos: any = [];

  private tipo_mensagems: any = [];
  private campo_jardims: any = [];
  private abastecimento_cils: any = [];

  private mensagem_title: string = "Mensagem";
  private servico_modal_title: string = "Associar Serviço";

  private edit_servico_flag: boolean = false;
  private clienteId: number;
  private serieId: number;
  private formasPagamentos: any = [];
  private validarFacturaRecibo: boolean = false
  private addValorClasse: boolean = true;
  private empresa: any;

  private url_params: any;

  private local_instalacao = {
    id: null,
    moradia_numero: null,
    is_predio: null,
    predio_id: null,
    predio_nome: null,
    latitude: null,
    longitude: null,
    rua_id: null,
    is_active: null,
    user_id: null,
  };

  private provincias: any = [];
  private municipios: any[];
  private distritos: any[];
  private quarteiraos: any[];
  private bairros: any[];

  private local_consumos: any[];
  private ruas: any[];
  private contas_clientes: any = [];
  private servicos_conta_cliente: any = [];
  private numero_telefone: string;
  private searchCliente: string;
  private isFinalizar: boolean = false;
  private contrato_validated: boolean = false;
  private dados_validated: boolean = false;


  private contrato = {
    id: null,
    cliente_id: null,
    conta_id: null,
    tipo_contracto_id: null,
    tipo_contrato: null,
    tipo_medicao_id: null,
    tipo_medicao: null,
    tipo_medicao_slug: null,
    tipo_facturacao_id: null,
    tipo_facturacao: null,
    tipologia_cliente_id: null,
    tipologia_cliente: null,
    tipologia_cliente_juro_mora: null,
    tipologia_cliente_sujeito_corte: null,
    tipologia_cliente_caucao: null,
    nivel_sensibilidade_id: null,
    nivel_sensibilidade: null,
    objecto_contrato_id: null,
    objecto_contrato: null,
    tarifario: null,
    classe_tarifario: null,
    classe_tarifario_consumo_minimo: null,
    classe_tarifario_consumo_maximo: null,
    numero_habitantes: null,
    numero_utilizadores: null,
    data_inicio: null,
    data_fim: null,
    morada_correspondencia_flag: false,
    morada_contrato: null,

    classe_tarifario_id: null,

    motivo_recisao_id: null,
    motivo_recisao: null,
    motivo_recisao_flag: null,
    novo_contrato_cil: null,
    rescindir_contrato_cil: null,

    tipo_mensagem_id: null,
    tipo_mensagem: null,
    tipo_mensagem_slug: null,
    mensagem: null,

    instalacao_sanitaria_qtd: null,
    reservatorio_flag: null,
    reservatorio_capacidade: null,
    piscina_flag: null,
    piscina_capacidade: null,
    jardim_flag: null,
    campo_jardim_id: null,
    campo_jardim: null,
    poco_alternativo_flag: null,
    fossa_flag: null,
    fossa_capacidade: null,
    acesso_camiao_flag: null,
    anexo_flag: null,
    anexo_quantidade: null,
    caixa_contador_flag: null,
    abastecimento_cil_id: null,
    abastecimento_cil: null
  };

  private dadosContratoValid: boolean = false;
  private dadosLocalConsumoValid: boolean = false;

  private conta = {
    id: null,
    numero_conta: null
  };

  private tarifario = {
    id: null,
    classe_tarifario_id: null
  };

  private local_consumo = {
    id: null,
    moradia_numero: null,
    is_predio: null,
    predio_nome: null,
    predio_andar: null,
    cil: null,
    cil_flag: true,
    rua_id: null,
    rua: null,
    bairro_id: null,
    bairro: null,
    has_quarteirao: false,
    quarteirao_id: null,
    has_distrito: false,
    municipio_id: null,
    distrito_id: null,
    provincia_id: null,
    is_active: null,
    user_id: null,
  };

  private facturacao = {
    cliente: {
      id: null,
      nome: null,
      telefone: null,
      tipo_identidade_id: null,
      tipo_identidade: null,
      tipo_facturacao_id: null,
      tipo_facturacao: null,
      numero_identificacao: null,
      genero: null,
      email: null,
      tipo_cliente_id: null,
      tipo_cliente: null,
      tipo_cliente_slug: null,
      municipio: null,
      morada: null,
      direccao: null,
      direccao_id: null,
      gestor_cliente_id: null,
      gestor_cliente: null,
      gestor_cliente_telefone: null,

      conta_id: null,
      servico_id: null
    },
    servicos: [],
    pagamento: {
      forma: null,
      valor_recebido: 0.0,
      total_valor_recebido: 0.0,
      troco: 0.0,
      total_pago: null,
      referencia: null,
      data_pagamento: null,
      forma_pagamento_id: null,
      bancos: [],
      adiantamento: [],
      is_adiantamento: 0,
      linha_pagamentos: []
    },
    total_com_imposto: 0.0,
    total_sem_imposto: 0.0,
    valor_total_imposto: 0.0,
    processado: false,
    facturaGerada: null,
    data_vencimento: null,
    moeda: null
  };

  private detalhe_contrato = {
    flag: false,
    collapse: "row collapsed",
    class: "panel-collapse collapse"
  }

  private servico = {
    servico_id: null,
    servico: null,
    servico_valor: null,
    servico_valor_load: null,
    imposto_id: null,
    imposto: null,
    imposto_codigo: null,
    imposto_valor: null,
    is_editable: false,
    facturado: false,
    tipo_produto_id: null,
    tipo_produto: null,
    incidencia_id: null,
    incidencia: null
  }

  private classe_tarifario = {
    id: null,
    tarifario_id: null,
    produto_id: null,
    descricao: null,
    valor: null,
    ordem: null,
    tarifa_variavel: null,
    tarifa_fixa_mensal: null,
    tarifa_intervalo: null,
    consumo_minimo: null,
    consumo_maximo: null
  }

  private predio_flag: boolean = false;
  private sem_produto_show: boolean = false;

  private showCheckServicos_flag: boolean = false;
  private checkAllServicos_flag: boolean = false;


  private disabledPagamento = true;
  private disabledPagamentoAdiantamento = true


  private series: any = [];
  private classe_tarifarios: any = [];
  private activeButtom = {
    cliente: false,
    servico: false
  }

  private items_servicos: any = [];
  private pagination = {
    start: 1,
    end: 10,
    search: null,
    servico: null
  };

  private produto_selecionado = null;
  private preco: number;
  //private servico_id: number;
  private servicos: any = [];




  constructor(
    private auth: AuthService,
    private configService: ConfigService,
    private http: HttpService,
    private config: ConfigModuloService,
    private contratoService: ContratoService
  ) {
    this.currentUser = this.auth.currentUserValue;
  }

  ngOnInit() {
    this.ListarClientes(1, 10, null);
    this.local_consumo.cil_flag = true;
    this.configService.empresaUser();
    this.getEmpresa();

    this.getParams();
  }


  private getParams() {

    const url_search = new URLSearchParams(window.location.search);

    const params = {
      id: url_search.get('id'),
      conta_id: url_search.get('conta_id'),
      to_edit: url_search.get('to_edit'),
    }

    if (params.id != null) this.getContratoById(params.id);

    if (params.conta_id != null) {
      
      console.log("edit contrato");
      this.getClienteByConta(params.conta_id)

      this.selectedCliente(this.cliente);

      this.activeButtom.cliente = (params.conta_id == null) ? false : true;

      this.conta.id = params.conta_id;
      this.conta.numero_conta = this.cliente.numero_conta;

      this.facturacao.cliente.conta_id = params.conta_id;
      this.contrato.tipo_facturacao_id = this.cliente.tipo_facturacao_id;
      this.contrato.tipo_facturacao = this.cliente.tipo_facturacao;
    }

    //console.log(url_params);
    //console.log(params);
  }


  changeClasseTarifario(classe_tarifario_id) {
    this.contrato.classe_tarifario_id = classe_tarifario_id
  }

  private ListarClientes(start, end, searchCliente) {
    this.configService.loaddinStarter('start');
    this.pagination.start = start;
    this.pagination.end = end;
    this.pagination.search = searchCliente

    this.http.__call('cliente/search-cliente', this.pagination).subscribe(
      res => {
        this.clientes = Object(res).data.data;
        this.configService.loaddinStarter('stop');
      }
    );
  }

  private getClienteByConta(id) {

    this.configService.loaddinStarter('start');

    this.http.call_get('cliente/getClienteByConta/' + id, null).subscribe(
      res => {
        this.cliente = Object(res);
        this.configService.loaddinStarter('stop');

        console.log(this.cliente)
      }
    );

  }

  private getContratoById(id) {


    console.log(id);

    this.configService.loaddinStarter('start');

    this.http.call_get('contrato/getContratoById/' + id, null).subscribe(
      res => {
        this.contrato = Object(res);
        this.configService.loaddinStarter('stop');

        console.log(this.contrato)
      }
    );

  }

  private getCliente(e) {
    this.searchCliente
    this.ListarClientes(1, 10, this.searchCliente);
  }


  private getProduto() {
    this.produtos(this.search.nativeElement.value);
  }


  private getSlugTipoContrato() {
    if (this.contrato.tipo_contracto_id == null || this.contrato.tipo_contracto_id == "") {
      return;
    }

    this.http.__call('info-tipo-contrato/' + this.contrato.tipo_contracto_id, null).subscribe(
      res => {
        this.contrato.tipo_medicao_slug = Object(res).data.slug;
        this.tarifario.id = null;
        this.tarifario.classe_tarifario_id = null;
        this.facturacao.valor_total_imposto = 0;
        this.classe_tarifarios = [];
        this.facturacao.servicos = [];
      }
    )
  }


  private setServicos(response: any) {

    this.facturacao.valor_total_imposto = 0;
    let validar = 0;
    if (response.code == 400) {
      this.configService.showAlert(response.message, "alert-danger", true);
    } else {
      if (this.facturacao.servicos.length >= 1) {
        if (response.valor_original) {
          for (let index = 0; index < this.facturacao.servicos.length; index++) {
            const servico = this.facturacao.servicos[index];
            if (servico.servico_id == response.servico_id) {

              this.facturacao.servicos.splice(index, 1);
              this.facturacao.servicos.splice(index, 0, response);
              validar = 1;
            }
          }
        }
        if (validar == 0) {
          this.facturacao.servicos.push(response);
        }
      } else {
        this.facturacao.servicos.push(response);
      }
      //Calcula o Total da Factura
      this.getValorTotalServicos();
    }
  }


  private showCheckServicos() {
    this.showCheckServicos_flag = !this.showCheckServicos_flag;
    if (this.showCheckServicos_flag) {
      for (let index = 0; index < this.facturacao.servicos.length; index++) {
        if (!this.facturacao.servicos[index].facturar)
          this.facturacao.servicos[index].facturar = false;
      }
    }
  }

  private checkAllServicos() {
    this.checkAllServicos_flag = !this.checkAllServicos_flag;
    for (let index = 0; index < this.facturacao.servicos.length; index++) {

      if (!this.facturacao.servicos[index].facturado)
        this.facturacao.servicos[index].facturar = this.checkAllServicos_flag;
    }
  }

  private detalheContratoCollapse() {
    this.detalhe_contrato.flag = !this.detalhe_contrato.flag;
    this.detalhe_contrato.class = (this.detalhe_contrato.flag) ? "panel-collapse collapse show" : "panel-collapse collapse";
    this.detalhe_contrato.collapse = (this.detalhe_contrato.flag) ? "row" : "row collapsed";
  }


  private getServicosToAdd() {

    this.configService.loaddinStarter('start');

    this.http.call_get('produto-classe-tarifario/selectBox', null).subscribe(
      res => {
        this.servicos = Object(res)

        this.configService.loaddinStarter('stop');
      }
    );

    this.servico_modal_title = "Associar Serviço";
    this.edit_servico_flag = false;
    this.modalAppendServico.show()

  }


  private setClasseTarifario() {

    this.addValorClasse = true;

    for (let index = 0; index < this.classe_tarifarios.length; index++) {

      if (this.classe_tarifarios[index].id == this.tarifario.classe_tarifario_id) {
        this.classe_tarifario.id = this.classe_tarifarios[index].servico_id;
        this.classe_tarifario.tarifario_id = this.classe_tarifarios[index].tarifario_id;
        this.classe_tarifario.produto_id = this.classe_tarifarios[index].produto_id;
        this.classe_tarifario.descricao = this.classe_tarifarios[index].descricao;
        this.classe_tarifario.valor = this.classe_tarifarios[index].valor;
        this.classe_tarifario.ordem = this.classe_tarifarios[index].ordem;
        this.classe_tarifario.tarifa_variavel = this.classe_tarifarios[index].tarifa_variavel;
        this.classe_tarifario.tarifa_fixa_mensal = this.classe_tarifarios[index].tarifa_fixa_mensal;
        this.classe_tarifario.tarifa_intervalo = this.classe_tarifarios[index].tarifa_intervalo;
        this.classe_tarifario.consumo_minimo = this.classe_tarifarios[index].consumo_minimo;
        this.classe_tarifario.consumo_maximo = this.classe_tarifarios[index].consumo_maximo;
      }
    }

    this.servico_modal_title = "Editar Serviço " + this.servico.servico;

    console.log(this.servico.servico);
    console.log(this.classe_tarifarios);
    this.edit_servico_flag = true;
  }




  private getServicosToEdit(id) {

    this.addValorClasse = true;

    for (let index = 0; index < this.facturacao.servicos.length; index++) {

      if (this.facturacao.servicos[index].servico_id == id) {
        this.servico.servico_id = this.facturacao.servicos[index].servico_id;
        this.servico.servico = this.facturacao.servicos[index].servico;
        this.servico.servico_valor = this.facturacao.servicos[index].servico_valor;
        this.servico.imposto_id = this.facturacao.servicos[index].imposto_id;
        this.servico.imposto = this.facturacao.servicos[index].imposto;
        this.servico.imposto_codigo = this.facturacao.servicos[index].imposto_codigo;
        this.servico.imposto_valor = this.facturacao.servicos[index].imposto_valor;
        this.servico.is_editable = this.facturacao.servicos[index].is_editable;
        this.servico.facturado = this.facturacao.servicos[index].facturado;
        this.servico.tipo_produto_id = this.facturacao.servicos[index].tipo_produto_id;
        this.servico.tipo_produto = this.facturacao.servicos[index].tipo_produto;
        this.servico.incidencia_id = this.facturacao.servicos[index].incidencia_id;
        this.servico.incidencia = this.facturacao.servicos[index].incidencia;
      }
    }

    this.servico.servico_valor_load = (this.servico.servico_valor == null) ? 0 : this.servico.servico_valor;
    this.servico_modal_title = "Editar Serviço " + this.servico.servico;
    this.edit_servico_flag = true;
    this.modalAppendServico.show()
  }

  private appendServicos() {

  }

  private getValorTotalServicos() {
    this.facturacao.valor_total_imposto = 0;
    this.facturacao.total_sem_imposto = 0;
    this.facturacao.total_com_imposto = 0;

    //Calcula o Total da Factura
    for (let index = 0; index < this.facturacao.servicos.length; index++) {
      let valor = (this.facturacao.servicos[index].servico_valor) ? this.facturacao.servicos[index].servico_valor : 0;
      this.facturacao.total_sem_imposto += valor;
      let valor_imposto = ((this.facturacao.servicos[index].imposto_valor) ? this.facturacao.servicos[index].imposto_valor : 0) * valor;
      this.facturacao.valor_total_imposto += valor_imposto;
      this.facturacao.total_com_imposto += valor + valor_imposto;
    }

    this.facturacao.pagamento.total_pago = this.facturacao.valor_total_imposto;

  }

  deleteRow(servico: any) {
    for (let i = 0; i < this.facturacao.servicos.length; ++i) {
      if (this.facturacao.servicos[i].servico_id === servico.servico_id) {
        this.facturacao.servicos.splice(i, 1);
        this.getValorTotalServicos();
      }
    }
  }

  appendServico() {

    if (this.edit_servico_flag) {

      for (let i = 0; i < this.facturacao.servicos.length; ++i) {
        if (this.facturacao.servicos[i].servico_id === this.servico.servico_id) {
          let valor = (this.servico.servico_valor) ? this.servico.servico_valor : 0;
          this.facturacao.total_sem_imposto += valor;
          let valor_imposto = ((this.servico.imposto_valor) ? this.servico.imposto_valor : 0) * valor;
          this.facturacao.valor_total_imposto += valor_imposto;
          this.facturacao.total_com_imposto += valor + valor_imposto;
          this.facturacao.servicos[i].servico_valor = this.servico.servico_valor_load;
        }
      }

    } else {
      this.configService.loaddinStarter('start');

      this.http.__call('produto-classe-tarifario/getServicoById/' + this.servico.servico_id,
        { conta_id: this.facturacao.cliente.conta_id }).subscribe(
          response => {
            let servico = Object(response);
            if (servico != null) this.facturacao.servicos.push(servico);
            let valor = (servico.servico_valor) ? servico.servico_valor : 0;
            this.facturacao.total_sem_imposto += valor;
            let valor_imposto = ((servico.imposto_valor) ? servico.imposto_valor : 0) * valor;
            this.facturacao.valor_total_imposto += valor_imposto;
            this.facturacao.total_com_imposto += valor + valor_imposto;
            this.configService.loaddinStarter('stop');
          }
        );

    }

    this.getValorTotalServicos();

    this.modalAppendServico.hide();
  }

  private refreshSerieId() {
    this.serieId = this.serie.nativeElement.value;
    if (Object(this.serieId) == "") {
      this.serieId = 0;
    }

  }

  private selectedTarifario() {
    this.tarifario.id = this.tarifario.id;
  }

  private selectedCliente(cliente) {
    //this.currente_cliente = cliente // this.clientes.find(obj => obj.id = id)
    this.facturacao.cliente.id = cliente.id;

    this.getConfiguracaos();
    this.getTiposContratos();
    this.getTiposMedicaos();
    this.getObjectosContratos();
    this.getNiveisSensibilidade();
    this.getTipologiaClientes();

    this.facturacao.cliente = cliente
    this.activeButtom.cliente = true
    this.getContas();
  }

  private produtos(search) {
    this.items_servicos = [];
    this.configService.loaddinStarter('start');
    this.pagination.search = (search == "" || search == null ? 'a' : search);
    this.pagination.servico = (this.facturacao.cliente.servico_id == "" || this.facturacao.cliente.servico_id == null ? null : this.facturacao.cliente.servico_id);
    this.http.__call('artigo/search', this.pagination).subscribe(
      res => {
        this.items_servicos = Object(res).data
        this.configService.loaddinStarter('stop');
      }
    );
  }


  /*
  getAddProduto() {

    if (this.quantidade <= 0 || isNaN(this.quantidade)) {
      this.configService.showAlert("Não foi informado uma quantidade valida", "alert-danger", true);
      return;
    }
    if (this.http.canActivateRouterLink('atribuir_desconto')) {
      if (!(this.desconto <= 100 && this.desconto >= 0)) {
        this.configService.showAlert("Desconto valido, informe um valor no intervalo entre 0 e 100%", "alert-danger", true);
        return;
      }
    }
    this.configService.loaddinStarter('start');
    this.http.__call('artigo/pesquisar',
      {
        servico_id: this.servico_id,
        quantidade: (this.quantidade < 0 || isNaN(this.quantidade) ? 1 : this.quantidade),
        desconto: this.desconto,
        cliente_id: this.clienteId,
        observacao: null,
        preco: this.preco
      }
    ).subscribe(
      res => {
        this.setServicos(Object(res).data);
        if (this.produto_selecionado == 0) {
          this.produtos(this.search.nativeElement.value);
        }
        this.quantidade = 1;
        this.desconto = 0;
        this.configService.loaddinStarter('stop');
      }
    );

  }
*/

  private validateTipoContrato(id) {

  }

  private getAddProdutoClasseTarifario() {
    this.configService.loaddinStarter('start');
    this.http.__call('classe-tarifario/getProdutoClasseTarifario',
      {
        conta_id: this.facturacao.cliente.conta_id,
        classe_tarifario_id: this.tarifario.classe_tarifario_id,
        preco: this.preco
      }
    ).subscribe(
      res => {
        this.setServicos(Object(res).data);
        this.getValorTotalServicos();
        this.preco = null;
        this.closeModal();

        this.configService.loaddinStarter('stop');
      }
    );
  }

  private resetTarifario() {
    this.tarifario.id = null;
    this.tarifario.classe_tarifario_id = null;
    this.facturacao.servicos = [];
  }

  private getServicosByClasseTarifario() {
    if ((this.contrato.tipo_medicao_slug == "ESTIMATIVA" && this.addValorClasse === true) && (this.preco == null)) {
      this.configService.showAlert("Digite o valor", "alert-danger", true);
      return
    } else {
      this.configService.loaddinStarter('start');

      this.http.__call('produto-classe-tarifario/getServicosByClasseTarifario/' + this.tarifario.classe_tarifario_id,
        { conta_id: this.facturacao.cliente.conta_id })
        .subscribe(
          response => {
            this.facturacao.servicos = Object(response).data;
            //this.setServicos(Object(response).data);

            if (this.facturacao.servicos.length == 0) {
              this.sem_produto_show = true;
            } else {
              this.sem_produto_show = false;
            }

            this.getValorTotalServicos();

            if (this.contrato.tipo_medicao_slug == "ESTIMATIVA" && this.addValorClasse === true) {
              this.getAddProdutoClasseTarifario();
            }

            this.closeModal()
            this.configService.loaddinStarter('stop');
          }
        );

    }
  }


  private openModal() {


    if (this.contrato.tipo_medicao_slug == "ESTIMATIVA") {
      this.configService.loaddinStarter('start');

      this.http.__call('classe-tarifario/getProdutoClasseTarifario',
        {
          conta_id: this.facturacao.cliente.conta_id,
          classe_tarifario_id: this.tarifario.classe_tarifario_id,
          preco: this.preco,
          flag: this.tarifa_fixa_modal_view
        }
      ).subscribe(
        res => {
          if (Object(res).code === 404) {
            this.addValorClasse = false;
            //this.modaltwo.show()
            this.configService.showAlert('Classe tarifário sem serviço associado', 'alert-warning', true);
            this.configService.loaddinStarter('stop');
          } else {
            this.addValorClasse = true;

            this.setClasseTarifario();

            this.configService.loaddinStarter('stop');
          }

          if (this.tarifa_fixa_modal_view) {
            //this.modaltwo.show();
            this.modalAppendServico.show();
          }
        }
      );
    } else {
      this.getServicosByClasseTarifario();
    }

  }

  private closeModal() {
    this.modalAppendServico.hide();
  }

  private validateMensagem() {

    for (let index = 0; index < this.tipo_mensagems.length; index++) {

      if (this.contrato.tipo_mensagem_id == this.tipo_mensagems[index].id) {
        this.contrato.tipo_mensagem = this.tipo_mensagems[index].nome;
        this.contrato.tipo_mensagem_slug = this.tipo_mensagems[index].slug;
      }
    }

    //console.log(this.facturacao.cliente);

    if (this.contrato.tipo_mensagem_slug == "GERAL") {
      this.mensagem_title = "Mensagem Geral"

      this.http.__call('mensagem/getMensagemByTipoMensagem/' + this.contrato.tipo_mensagem_id, { tipo_cliente_id: null }).subscribe(
        res => {
          console.log(Object(res));
          this.contrato.mensagem = Object(res).texto;
        }
      );
    } else if (this.contrato.tipo_mensagem_slug == "TIPO_CLIENTE") {
      this.mensagem_title = "Mensagem de Tipo de Cliente(" + this.facturacao.cliente.tipo_cliente + ")";

      this.http.__call('mensagem/getMensagemByTipoMensagem/' + this.contrato.tipo_mensagem_id, { tipo_cliente_id: this.facturacao.cliente.tipo_cliente_id }).subscribe(
        res => {
          console.log(Object(res));
          this.contrato.mensagem = Object(res).texto;
        }
      );
    }
    else {
      this.mensagem_title = "Mensagem Individual"
    }

  }

  private revertValidateDadosContrato() {
    this.dados_validated = false;
  }

  private validateDadosContrato() {

    if (this.contrato.data_inicio != null && this.contrato.data_fim != null) {

      var tokenize_date_1 = this.contrato.data_inicio.split("-");
      var tokenize_date_2 = this.contrato.data_fim.split("-");

      var data_1 = new Date(tokenize_date_1[2], tokenize_date_1[1] - 1, tokenize_date_1[0]);
      var data_2 = new Date(tokenize_date_2[2], tokenize_date_2[1] - 1, tokenize_date_2[0]);
    }

    if (this.contrato.data_inicio != null) {
      var tokenize_date_1 = this.contrato.data_inicio.split("-");
      var data_1 = new Date(tokenize_date_1[2], tokenize_date_1[1] - 1, tokenize_date_1[0]);
    }

    var result: boolean = true;

    if (this.contrato.tipo_medicao_id == null) {
      this.configService.showAlert('O tipo de medição não foi seleccionado!', 'alert-danger', true);
      result = false;
    } else if (this.contrato.tipo_contracto_id == null) {
      this.configService.showAlert('O tipo de contrato não foi seleccionado!', 'alert-danger', true);
      result = false;
    } else if (this.contrato.tipo_facturacao_id == null) {
      this.configService.showAlert('O tipo de facturação não foi seleccionado!', 'alert-danger', true);
      result = false;
    } else if (this.contrato.tipologia_cliente_id == null) {
      this.configService.showAlert('A tipologia de cliente não foi seleccionado!', 'alert-danger', true);
      result = false;
    } else if (this.contrato.objecto_contrato_id == null) {
      this.configService.showAlert('O objecto de ligação não foi seleccionado!', 'alert-danger', true);
      result = false;
    } else if (this.contrato.data_inicio == null) {
      this.configService.showAlert('A data ínicio não foi inserida!', 'alert-danger', true);
    } else if (data_2 != null && data_1 > data_2) {
      this.configService.showAlert('A data ínicio não pode ser maior que a data fim', 'alert-danger', true);
    }
    else {
      this.configService.showAlert('Os dados estão válidos', 'alert-success', true);
    }

    this.dados_validated = result;

    return result;
  }

  private validateDadosLocalConsumo() {

    var result: boolean = true;

    /*
  if (this.local_consumo.id == null) {
    this.configService.showAlert('Insira a residência do local de instalação!', 'alert-danger', true);
    result = false;
  } else if (this.contrato.abastecimento_cil_id == null) {
    this.configService.showAlert('O Abastecimento CIL não foi seleccionado!', 'alert-danger', true);
    result = false;
  } else if (this.contrato.instalacao_sanitaria_qtd == null) {
    this.configService.showAlert('Insira a Quantidade de Instalações!', 'alert-danger', true);
    result = false;
  } else if (this.contrato.tipo_mensagem_id == null) {
    this.configService.showAlert('O tipo de mensagem não foi seleccionado!', 'alert-danger', true);
    result = false;
  } else if (this.contrato.reservatorio_flag == true && this.contrato.reservatorio_capacidade == null) {
    this.configService.showAlert('Insira a capacidade do reservário em m³!', 'alert-danger', true);
    result = false;
  } else if (this.contrato.piscina_flag == true && this.contrato.piscina_capacidade == null) {
    this.configService.showAlert('Insira a capacidade da piscina em m³!', 'alert-danger', true);
    result = false;
  } else if (this.contrato.fossa_flag == true && this.contrato.fossa_capacidade == null) {
    this.configService.showAlert('Insira a capacidade da fossa em m³!', 'alert-danger', true);
    result = false;
  } else if (this.contrato.anexo_flag == true && this.contrato.anexo_quantidade == null) {
    this.configService.showAlert('Insira a quantidade de anexos!', 'alert-danger', true);
    result = false;
  } else if (this.contrato.jardim_flag == true && this.contrato.campo_jardim_id == null) {
    this.configService.showAlert('O campo de jardim não foi seleccionado!', 'alert-danger', true);
    result = false;
  }

    */
    return result;
  }


  private validateLocalConsumo() {

    this.http.__call('local-consumo/getLocalConsumoWithContrato',
      {
        id: this.local_consumo.id,
        conta_id: this.conta.id
      }).subscribe(
        response => {
          let res = Object(response);
          if (res != null) this.contrato.motivo_recisao_flag = true;
          this.configService.loaddinStarter('stop');
        }
      );
  }

  private cilAvailable(flag) {

    if (flag) {
      this.contrato.rescindir_contrato_cil = false;
      this.contrato.novo_contrato_cil = true;
    }
    else {
      this.contrato.rescindir_contrato_cil = true;
      this.contrato.novo_contrato_cil = false;
    }
  }

  private facturarServicos() {

    let valor_aberto = 0;
    let valor_facturar = 0;

    for (let servico of this.facturacao.servicos) {
      let valor = (servico.servico_valor) ? this.servico.servico_valor : 0;
      let valor_imposto = ((servico.imposto_valor) ? servico.imposto_valor : 0) * valor;

      if (servico.facturar) {
        valor_facturar += servico.valor + valor_imposto;
      }
      else {
        valor_aberto += servico.valor + valor_imposto;
      }
    }

    let data = {
      cliente_id: this.facturacao.cliente.id,
      gestor_cliente_id: this.facturacao.cliente.gestor_cliente_id,
      conta_id: this.facturacao.cliente.conta_id,
      servicos: this.facturacao.servicos,
      total_sem_imposto: this.facturacao.total_sem_imposto,
      total_com_imposto: this.facturacao.total_com_imposto,
      valor_total_imposto: this.facturacao.valor_total_imposto,
      valor_aberto: valor_aberto,
      valor_facturar: valor_facturar
    }

    /*
    for (let index = 0; index < this.facturacao.servicos.length; index++) {

      let item = this.facturacao.servicos[index];

      let valor = (item.servico_valor) ? this.servico.servico_valor : 0;
      let valor_imposto = ((item.imposto_valor) ? item.imposto_valor : 0) * valor;

      if (item.facturar) {
        valor_facturar += item.valor + valor_imposto;
      }
      else {
        valor_aberto += item.valor + valor_imposto;
      }
    }
    */


    this.http.__call('factura/facturarServicosContrato', data)
      .subscribe(
        res => {
          if (Object(res).code == 200) {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.showCheckServicos_flag = false;

            this.facturacao.servicos = Object(res).data.servicos
          }
        })


  }

  private imprimirFacturaServicos() {

  }

  private validateProcess() {
    if (this.validateDadosContrato() && this.validateDadosLocalConsumo()) {
      this.contrato_validated = true;
    }
  }


  private imprimirContrato(id) {
    this.contratoService.imprimirPDFContrato(id);
  }

  async getEmpresa() {
    await this.http.call_get('empresa/empresa-user', null).subscribe(
      response => {
        //console.log(response);
        this.empresa = Object(response);
        //console.log(this.empresa);
      }
    )
  }

  private saveContrato() {

    if (this.validateDadosContrato() && this.validateDadosLocalConsumo()) {
      this.configService.loaddinStarter('start');
      if (this.contrato.id == null) {
        this.http.__call('contrato/create', {

          conta_id: this.conta.id,
          cliente_id: this.facturacao.cliente.id,
          tipo_contracto_id: this.contrato.tipo_contracto_id,
          tipo_medicao_id: this.contrato.tipo_medicao_id,
          tipo_facturacao_id: this.contrato.tipo_facturacao_id,
          tipologia_cliente_id: this.contrato.tipologia_cliente_id,
          nivel_sensibilidade_id: this.contrato.nivel_sensibilidade_id,
          objecto_contrato_id: this.contrato.objecto_contrato_id,
          motivo_recisao_id: this.contrato.motivo_recisao_id,
          data_inicio: this.contrato.data_inicio,
          data_fim: this.contrato.data_fim,
          morada_correspondencia_flag: this.contrato.morada_correspondencia_flag,
          numero_habitantes: this.contrato.numero_habitantes,
          numero_utilizadores: this.contrato.numero_utilizadores,

          morada_contrato: this.contrato.morada_contrato,
          classe_tarifario_id: this.contrato.classe_tarifario_id,

          tipo_mensagem_id: this.contrato.tipo_mensagem_id,
          mensagem: this.contrato.mensagem,

          //servicos: this.facturacao.servicos,
          tarifario_id: this.tarifario.id,

          local_consumo_id: this.local_consumo.id,
          instalacao_sanitaria_qtd: this.contrato.instalacao_sanitaria_qtd,
          reservatorio_flag: this.contrato.reservatorio_flag,
          reservatorio_capacidade: this.contrato.reservatorio_capacidade,
          piscina_flag: this.contrato.piscina_flag,
          piscina_capacidade: this.contrato.piscina_capacidade,
          jardim_flag: this.contrato.jardim_flag,
          campo_jardim_id: this.contrato.campo_jardim_id,
          poco_alternativo_flag: this.contrato.poco_alternativo_flag,
          fossa_flag: this.contrato.fossa_flag,
          fossa_capacidade: this.contrato.fossa_capacidade,
          acesso_camiao_flag: this.contrato.acesso_camiao_flag,
          anexo_flag: this.contrato.anexo_flag,
          anexo_quantidade: this.contrato.anexo_quantidade,
          caixa_contador_flag: this.contrato.caixa_contador_flag,
          abastecimento_cil_id: this.contrato.abastecimento_cil_id
        }).subscribe(
          res => {
            if (Object(res).code == 500) {
              this.configService.showAlert(Object(res).message, 'alert-danger', true);
            } else if (Object(res).code == 201) {
              this.configService.showAlert(Object(res).message, 'alert-warning', true);
              this.configService.loaddinStarter('stop');

              this.contrato_validated = false;

              this.dataContratoSaved = Object(res).data;

            } else {
              this.configService.showAlert(Object(res).message, 'alert-success', true);
              this.facturacao.processado = true;
              this.configService.loaddinStarter('stop');

              this.dataContratoSaved = Object(res).data;


            }

            console.log(this.dataContratoSaved);
          });
      }
    }

  }

  private carregando = {
    tarifario: 'Selecione o tarifário',
    classeTarifario: 'Selecione a classe tarifário'
  }

  private listarseries() {
    this.carregando.tarifario = 'Carregando...';

    this.http.call_get('tarifario/selectBox', null).subscribe(
      response => {
        this.series = Object(response).data;
        this.carregando.tarifario = 'Selecione o tarifário';
      }
    );
  }


  private classeTarifarioBytarifario() {
    this.carregando.classeTarifario = 'Carregando...';
    this.http.call_get('selectBox/classe-tarifarioBytarifario/' + this.tarifario.id, null).subscribe(
      response => {
        this.classe_tarifarios = Object(response).data;
        this.carregando.classeTarifario = 'Selecione a classe tarifário';
        this.tarifario.classe_tarifario_id = null;
        this.facturacao.valor_total_imposto = 0;
        this.facturacao.servicos = [];
      }
    );
  }

  private getTiposContratos() {
    this.http.call_get('tipo-contrato/selectBox', null).subscribe(
      response => {
        this.tipo_contratos = Object(response);
      }
    );
  }


  private getTiposMedicaos() {
    this.http.call_get('tipo-medicao/selectBox', null).subscribe(
      response => {
        this.tipo_medicaos = Object(response);
      }
    );
  }

  private getNiveisSensibilidade() {
    this.http.call_get('nivel-sensibilidade/selectBox', null).subscribe(
      response => {
        this.niveis_sensibilidade = Object(response);
      }
    );
  }

  private getAbastecimentoCILs() {
    this.http.call_get('abastecimento-cil/selectBox', null).subscribe(
      response => {
        this.abastecimento_cils = Object(response);
      }
    );
  }

  private getTipoMensagems() {
    this.http.call_get('tipo-mensagem/selectBox', null).subscribe(
      response => {
        this.tipo_mensagems = Object(response);
      }
    );
  }

  private getCampoJardims() {
    this.http.call_get('campo-jardim/selectBox', null).subscribe(
      response => {
        this.campo_jardims = Object(response);
      }
    )
  }

  private getTarifarios() {
    this.http.call_get('tarifario/selectBox', null).subscribe(
      response => {
        this.tarifarios = Object(response);
      }
    );
  }

  private getTipologiaClientes() {
    this.http.call_get('tipologia-cliente/selectGroupBox', null).subscribe(
      response => {
        this.tipologia_clientes = Object(response);
      }
    );
  }

  private getObjectosContratos() {
    this.http.call_get('objecto-contrato/selectBox', null).subscribe(
      response => {
        this.objecto_contratos = Object(response);
      }
    );
  }

  private getMotivosRecisaos() {
    this.http.call_get('motivo-recisao/selectBox', null).subscribe(
      response => {
        this.motivo_recisaos = Object(response);
      }
    );
  }


  private getProvincias() {

    if (this.local_consumo.cil_flag == false) {
      this.local_consumo.municipio_id = null;
      this.local_consumo.distrito_id = null;
      this.local_consumo.bairro_id = null;
      this.local_consumo.quarteirao_id = null;
      this.local_consumo.rua_id = null;
    }

    this.http.call_get('provincia/selectBox', null).subscribe(
      response => {
        this.provincias = Object(response).data
      }
    );
  }

  private selectBoxMunicipiosByProvincia(id) {

    if (this.local_consumo.cil_flag == false) {
      this.local_consumo.distrito_id = null;
      this.local_consumo.bairro_id = null;
      this.local_consumo.quarteirao_id = null;
      this.local_consumo.rua_id = null;
    }

    this.http.call_get('municipio/getMunicipiosByProvincia/' + id, null).subscribe(
      response => {
        this.municipios = Object(response).data;
      }
    );
  }

  private selectBoxDistritosByMunicipio() {

    if (this.local_consumo.cil_flag == false) {
      this.local_consumo.bairro_id = null;
      this.local_consumo.quarteirao_id = null;
      this.local_consumo.rua_id = null;

    }

    for (let i = 0; i < this.municipios.length; ++i) {
      if (this.municipios[i].id == this.local_consumo.municipio_id)
        this.local_consumo.has_distrito = this.municipios[i].has_distrito;
    }

    if (this.local_consumo.has_distrito) {
      this.http.call_get('distrito/getDistritosByMunicipio/' + this.local_consumo.municipio_id, null).subscribe(
        response => {
          this.distritos = Object(response).data;
        }
      );
    }

    if (!this.local_consumo.has_distrito) {
      this.selectBoxBairrosByMunicipio();
    }

  }


  private selectBoxQuarteiraosByBairro() {

    if (this.local_consumo.cil_flag == false) {
      this.local_consumo.quarteirao_id = null;
      this.local_consumo.rua_id = null;
    }

    for (let i = 0; i < this.bairros.length; ++i) {
      if (this.bairros[i].id == this.local_consumo.bairro_id)
        this.local_consumo.has_quarteirao = this.bairros[i].has_quarteirao;
    }

    if (this.local_consumo.has_quarteirao) {
      this.http.call_get('quarteirao/getQuarteiraosByBairro/' + this.local_consumo.bairro_id, null).subscribe(
        response => {
          this.quarteiraos = Object(response).data;
        }
      );
    }

    if (!this.local_consumo.has_quarteirao) {
      this.selectBoxRuasByBairro();
    }
  }

  private selectBoxRuasByQuarteirao() {
    this.http.call_get('rua/getRuasByQuarteirao/' + this.local_consumo.quarteirao_id, null).subscribe(
      response => {
        this.ruas = Object(response).data;
      }
    );
  }

  private selectBoxRuasByBairro() {
    this.http.call_get('rua/selectBoxByBairro/' + this.local_consumo.bairro_id, null).subscribe(
      response => {
        this.ruas = Object(response);
      }
    );
  }

  private selectBoxBairrosByMunicipio() {

    if (this.local_consumo.cil_flag == false) {
      this.local_consumo.quarteirao_id = null;
      this.local_consumo.rua_id = null;
    }

    this.http.call_get('bairro/selectBoxByMunicipio/' + this.local_consumo.municipio_id, null).subscribe(
      response => {

        this.bairros = Object(response);
      }
    );
  }

  private getSelectBoxResidenciasByRua() {
    this.getResidencia();
  }

  view_local_consumo = false;
  view_local_consumo_cil = false;


  private getResidencia() {

    if (this.local_consumo.predio_nome == "") {
      this.setNullResidencia();
    }

    this.view_local_consumo = true;
    this.http.__call('local-instalacao/getSelectLocalInstalacaosByRua', { rua_id: this.local_consumo.rua_id, start: 1, end: 15, search: this.local_consumo.moradia_numero }).subscribe(
      response => {
        this.local_consumos = Object(response).data.data;

        this.configService.loaddinStarter('stop');
      }
    );
  }


  private getLocalInstalacaosByCILAndMoradia() {

    if (this.local_consumo.cil == "") {
      this.setNullResidencia();
    }

    this.view_local_consumo_cil = true;
    this.http.__call('local-instalacao/getLocalInstalacaosByCILAndMoradia', { start: 1, end: 15, search: this.local_consumo.cil }).subscribe(
      response => {
        this.local_consumos = Object(response).data.data;

        this.configService.loaddinStarter('stop');
      }
    );
  }

  private setLocalConsumo(item: any) {
    this.local_consumo.id = item.id;
    this.local_consumo.moradia_numero = item.moradia_numero;
    this.local_consumo.predio_nome = ((item.cil != null) ? '[' + item.cil + '] ' : '[0000-000-000] ') + ((item.is_predio) ? 'Prédio ' + item.predio_nome + ', ' + item.predio_andar + 'ºAndar - Porta ' : 'Residência ') + item.moradia_numero;
    this.local_consumo.is_predio = item.is_predio;
    this.local_consumo.cil = (this.local_consumo.cil_flag) ? this.local_consumo.predio_nome : item.cil;
    this.view_local_consumo = false;
    this.view_local_consumo_cil = false;


    this.contrato.instalacao_sanitaria_qtd = item.instalacao_sanitaria_qtd,
      this.contrato.reservatorio_flag = item.reservatorio_flag,
      this.contrato.reservatorio_capacidade = item.reservatorio_capacidade,
      this.contrato.piscina_flag = item.piscina_flag,
      this.contrato.piscina_capacidade = item.piscina_capacidade,
      this.contrato.jardim_flag = item.jardim_flag,
      this.contrato.campo_jardim_id = item.campo_jardim_id,
      this.contrato.poco_alternativo_flag = item.poco_alternativo_flag,
      this.contrato.fossa_flag = item.fossa_flag,
      this.contrato.fossa_capacidade = item.fossa_capacidade,
      this.contrato.acesso_camiao_flag = item.acesso_camiao_flag,
      this.contrato.anexo_flag = item.anexo_flag,
      this.contrato.anexo_quantidade = item.anexo_quantidade,
      this.contrato.caixa_contador_flag = item.caixa_contador_flag,
      this.contrato.abastecimento_cil_id = item.abastecimento_cil_id
  }

  private setNullResidencia() {
    this.local_consumo.id = null;
    this.local_consumo.moradia_numero = null;
    this.local_consumo.predio_nome = null;
    this.local_consumo.is_predio = null,
      this.local_consumo.cil = null,

      this.view_local_consumo = false;
    this.view_local_consumo_cil = false;

    this.predio_flag = false;
  }

  private reloadFacturacao() {
    location.reload();
  }

  private getContas() {
    this.activeButtom.cliente = false
    this.http.call_get('cliente/conta/selectBox/' + this.facturacao.cliente.id, null).subscribe(
      response => {
        this.contas_clientes = Object(response).data
      })
  }

  private getServicosConta() {
    this.activeButtom.cliente = false;
    this.configService.loaddinStarter('start');
    this.http.call_get('tarifario/servico/selectBoxServicosConta/' + this.facturacao.cliente.conta_id, null).subscribe(
      response => {
        this.servicos_conta_cliente = Object(response).data
        this.configService.loaddinStarter('stop');
      })

  }

  confirmarConta() {
    this.activeButtom.cliente = false
    if (this.conta.id == null) {
      this.configService.showAlert('Selecione a conta', "alert-danger", true);
    } else {
      this.activeButtom.cliente = true
    }

    this.contas_clientes.forEach(e => {

      if (e.id == this.conta.id) this.conta.numero_conta = e.numero_conta;

      this.facturacao.cliente.conta_id = e.id;
      this.contrato.tipo_facturacao_id = e.tipo_facturacao_id;
      this.contrato.tipo_facturacao = e.tipo_facturacao;

    });

  }


  private contaServicoSeleciona() {
    this.servicos_conta_cliente.forEach(element => {
      if (element.id == this.facturacao.cliente.servico_id) {
        this.servico = element;
      }
    });
  }

  private clienteSemOrComConta = 0

  selectedClienteComConta(n: number) {

    this.clienteSemOrComConta = n
    this.listarseries();

    this.getTiposContratos();

    this.getProvincias();
    this.facturacao.servicos = [];

  }

  selectedClienteSemConta(n: number) {
    this.clienteSemOrComConta = n
    this.listarseries();
    this.facturacao.servicos = []
    this.servico = null
    this.facturacao.cliente.conta_id = null
    this.facturacao.cliente.servico_id = null


  }



  private joinLocalInstacaoToContrato(item: any) {

    this.http.__call('local-instalacao/getLocalInstalacaoByRuaAndMoradia/', {
      rua_id: item.rua_id,
      moradia_numero: item.moradia_numero
    }).subscribe(
      response => {

        let result = Object(response);

        this.local_consumo.id = result.id;
        this.local_consumo.moradia_numero = result.moradia_numero;
        this.local_consumo.predio_nome = ((result.is_predio) ? 'Prédio ' + result.predio_nome + ', ' + result.predio_andar + 'ºAndar - Porta ' : 'Residência ') + result.moradia_numero;
        this.local_consumo.is_predio = result.is_predio;

        if (Object(response).code == 200) {
          this.configService.showAlert(Object(response).message, "alert-success", true);
          this.local_instalacao = null;
        } else {
          this.configService.showAlert(Object(response).message, "alert-danger", true);
        }

      }
    );

  }

  onReset() {

    this.local_instalacao.id = null;
    this.local_instalacao.moradia_numero = null;
    this.local_instalacao.is_predio = null;
    this.local_instalacao.predio_id = null;
    this.local_instalacao.predio_nome = null;
    this.local_instalacao.rua_id = null;
    this.local_instalacao.is_active = null;
    this.local_instalacao.user_id = null;
  }

  cancelarConta() {
    this.activeButtom.cliente = false;
    this.facturacao.cliente.conta_id = null
    this.facturacao.cliente.servico_id = null
    this.facturacao.cliente.id = null;
    this.servico = null
  }


  private getContratoDetails() {

    //console.log("entry validate");

    if (this.validateDadosContrato()) {

      //console.log("validated!");

      this.getDetailTipoMedicao();
      this.getDetailTipoContrato();
      this.getDetailNivelSensibilidade();
      this.getDetailTipologiaCliente();
      this.getDetailObjectoContrato();
      //this.getDetailMotivoRecisao();
      this.getDetailTarifario();
      this.getDetailClasseTarifario();


      this.getAbastecimentoCILs();
      this.getTipoMensagems();
      this.getCampoJardims();

    }

  }

  private getDetailTipoMedicao() {

    for (let index = 0; index < this.tipo_medicaos.length; index++) {
      if (this.contrato.tipo_medicao_id == this.tipo_medicaos[index].id) {
        this.contrato.tipo_medicao = this.tipo_medicaos[index].nome;
        this.contrato.tipo_medicao_slug = this.tipo_medicaos[index].slug;
      }
    }
  }

  private getDetailTipoContrato() {
    for (let index = 0; index < this.tipo_contratos.length; index++) {
      if (this.contrato.tipo_contracto_id == this.tipo_contratos[index].id) {
        this.contrato.tipo_contrato = this.tipo_contratos[index].descricao;
      }
    }
  }

  private getDetailNivelSensibilidade() {
    for (let index = 0; index < this.niveis_sensibilidade.length; index++) {
      if (this.contrato.nivel_sensibilidade_id == this.niveis_sensibilidade[index].id) {
        this.contrato.nivel_sensibilidade = this.niveis_sensibilidade[index].nome;
      }
    }
  }

  private getDetailTarifario() {
    for (let index = 0; index < this.tarifarios.length; index++) {
      if (this.tarifario.id == this.tarifarios[index].id) {
        this.contrato.tarifario = this.tarifarios[index].descricao;
      }
    }
  }

  private getDetailClasseTarifario() {
    for (let index = 0; index < this.classe_tarifarios.length; index++) {
      if (this.tarifario.classe_tarifario_id == this.classe_tarifarios[index].id) {
        this.contrato.classe_tarifario = this.classe_tarifarios[index].descricao;
        this.contrato.classe_tarifario_consumo_minimo = this.classe_tarifarios[index].consumo_minimo;
        this.contrato.classe_tarifario_consumo_maximo = this.classe_tarifarios[index].consumo_maximo;
      }
    }
  }

  private getDetailTipologiaCliente() {
    for (let index = 0; index < this.tipologia_clientes.length; index++) {
      for (let k = 0; k < this.tipologia_clientes[index].childs.length; k++) {
        if (this.contrato.tipologia_cliente_id == this.tipologia_clientes[index].childs[k].id) {
          this.contrato.tipologia_cliente = this.tipologia_clientes[index].childs[k].descricao;
          this.contrato.tipologia_cliente_juro_mora = this.tipologia_clientes[index].childs[k].juro_mora;
          this.contrato.tipologia_cliente_sujeito_corte = this.tipologia_clientes[index].childs[k].sujeito_corte;
          this.contrato.tipologia_cliente_caucao = this.tipologia_clientes[index].childs[k].caucao;
        }
      }
    }
  }

  private getDetailObjectoContrato() {
    for (let index = 0; index < this.objecto_contratos.length; index++) {
      if (this.contrato.objecto_contrato_id == this.objecto_contratos[index].id) {
        this.contrato.objecto_contrato = this.objecto_contratos[index].nome;
      }
    }
  }

  private getDetailAbastecimentoCIL() {
    for (let index = 0; index < this.abastecimento_cils.length; index++) {
      if (this.contrato.abastecimento_cil_id == this.abastecimento_cils[index].id) {
        this.contrato.abastecimento_cil = this.abastecimento_cils[index].nome;
      }
    }
  }

  private getDetailCampoJardim() {
    for (let index = 0; index < this.campo_jardims.length; index++) {
      if (this.contrato.campo_jardim_id == this.campo_jardims[index].id) {
        this.contrato.campo_jardim = this.campo_jardims[index].nome;
      }
    }
  }

  private getDetailTipoMensagem() {
    for (let index = 0; index < this.tipo_mensagems.length; index++) {
      if (this.contrato.tipo_medicao_id == this.tipo_mensagems[index].id) {
        this.contrato.tipo_mensagem = this.tipo_mensagems[index].nome;
      }
    }
  }

  private getDetailMotivoRecisao() {
    for (let index = 0; index < this.motivo_recisaos.length; index++) {
      if (this.contrato.motivo_recisao_id == this.motivo_recisaos[index].id) {
        this.contrato.motivo_recisao = this.motivo_recisaos[index].nome;
      }
    }
  }

  public getConfiguracaos() {

    let result = null;

    const slugs = [
      this.config.provincia_default,
      this.config.tipo_facturacao_default,
      this.config.media_consumo_default,
      this.config.nivel_sensibilidade_default,
      this.config.tarifa_fixa_modal_view
    ];
    //console.log(slugs)

    for (let index = 0; index < slugs.length; index++) {

      this.http.__call('configuracao/getConfiguracaobySlug/' + slugs[index], null).subscribe(
        response => {

          //console.log(Object(response));

          if (Object(response).code != 200) {
            //this.config.saveConfig(slugs[index], this.config.modulo.CONFIGURACOES, null);
            result = null;
          }
          else {

            result = Object(response).data;

            if (slugs[index] == this.config.provincia_default) {
              this.local_consumo.provincia_id = result.valor;
              this.selectBoxMunicipiosByProvincia(result.valor);
            }

            if (slugs[index] == this.config.tipo_facturacao_default) {
              this.contrato.tipo_facturacao_id = result.valor;
            }

            if (slugs[index] == this.config.nivel_sensibilidade_default) {
              this.contrato.nivel_sensibilidade_id = result.valor;
            }

            if (slugs[index] == this.config.tarifa_fixa_modal_view) this.tarifa_fixa_modal_view = Boolean(result.valor);
          }
        });
    }

  }


}

