import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { Router } from '@angular/router'
import { ConfigService } from 'src/app/providers/config/config.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  tfa: any = {};
  authcode: string = "";
  errorMessage: string = null;

  constructor(private auth: AuthService, private router: Router, private configService: ConfigService) {
    //this.getAuthDetails();
  }

  ngOnInit() {
  }
/*
  getAuthDetails() {

    this.auth.getAuth().subscribe((data) => {
      const result = Object(data); 
      if (result.status === 200) {
        if (result.body.data === null) {
          this.setup(); 
        } else {
          this.tfa = result.body.data;
          localStorage.setItem('tempSecret', result.body.data.tempSecret);
        }
      }
    });
  }

  setup() {
    this.auth.setupAuth().subscribe(data => {
      const result = Object(data);
      
      if (result.status === 200) {
        this.tfa = result.body.data;
      } else if (result.status === 204) {
        console.log(result.status);
        location.reload();
      }
    });
  }

  confirm() {
    this.configService.loaddinStarter('start');
    this.auth.verifyAuth(this.authcode).subscribe(data => {
      const result = Object(data).body;
      console.log(result)
      if (result.code == 200) {
        this.errorMessage = null;
        this.tfa.secret = this.tfa.tempSecret;
        this.tfa.tempSecret = "";
        this.configService.loaddinStarter('stop');
      } else if (result.code === 403) {
        this.errorMessage = result['message'];
        this.configService.loaddinStarter('stop');
      }
    });
  }

  disabledTfa() {
    this.auth.deleteAuth().subscribe(data => {
      const result = Object(data).data;
      if (Object(data).code === 200) {
        this.authcode = "";
        this.getAuthDetails();
      }
    });
  }
*/
}
