import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportDiarioAutoComponent } from './report-diario-auto.component';

describe('ReportDiarioAutoComponent', () => {
  let component: ReportDiarioAutoComponent;
  let fixture: ComponentFixture<ReportDiarioAutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportDiarioAutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportDiarioAutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
