import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReclamacoesRelatorioComponent } from './reclamacoes-relatorio.component';

describe('ReclamacoesRelatorioComponent', () => {
  let component: ReclamacoesRelatorioComponent;
  let fixture: ComponentFixture<ReclamacoesRelatorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReclamacoesRelatorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReclamacoesRelatorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
