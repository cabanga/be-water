import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import * as jsPDF from 'jspdf';
import * as CryptoJS  from 'crypto-js';

@Component({
  selector: 'app-envio-relatorio-diario',
  templateUrl: './envio-relatorio-diario.component.html',
  styleUrls: ['./envio-relatorio-diario.component.css']
})
export class EnvioRelatorioDiarioComponent implements OnInit {
  


  private credencials = {
    last_name: null
  }

  private dataType = {
    data: null,
    report_name: null,
    factura_id: null
  }

  private items: any = [];
  private users: any = [];

  constructor(private http: HttpService, private configService: ConfigService) { }

  ngOnInit() {
    this.getPageFilterData(1);
  }


  private listarTipoPerfil() {

    this.configService.loaddinStarter('start');

    this.http.__call('roles/reportDiario', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');
        

      }
    );
  }

  private enviarFactura(id: number) {
      //this.http.__call('enviar/reportDiario/email/' + id, null).subscribe(
        this.configService.gerarReportDiario(id)
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.listarTipoPerfil();
  }



}
