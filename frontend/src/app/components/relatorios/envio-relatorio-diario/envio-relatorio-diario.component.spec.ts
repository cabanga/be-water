import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvioRelatorioDiarioComponent } from './envio-relatorio-diario.component';

describe('EnvioRelatorioDiarioComponent', () => {
  let component: EnvioRelatorioDiarioComponent;
  let fixture: ComponentFixture<EnvioRelatorioDiarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvioRelatorioDiarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvioRelatorioDiarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
