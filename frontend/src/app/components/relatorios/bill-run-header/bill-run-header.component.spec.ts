import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BillRunHeaderComponent } from './bill-run-header.component';

describe('BillRunHeaderComponent', () => {
  let component: BillRunHeaderComponent;
  let fixture: ComponentFixture<BillRunHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillRunHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillRunHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
