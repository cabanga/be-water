import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteRelatorioComponent } from './cliente-relatorio.component';

describe('ClienteRelatorioComponent', () => {
  let component: ClienteRelatorioComponent;
  let fixture: ComponentFixture<ClienteRelatorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteRelatorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteRelatorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
