import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { ExcelService } from 'src/app/services/excel.service';
import { ReportClienteService } from 'src/app/components/report-at/relatorios/report-cliente.service';
import * as moment from 'moment';
import { ExcelAutoService } from 'src/app/services/excel/excel-auto.service';
import { NullVisitor } from '@angular/compiler/src/render3/r3_ast';

@Component({
  selector: 'app-extraccao-clientes',
  templateUrl: './extraccao-clientes.component.html',
  styleUrls: ['./extraccao-clientes.component.css']
})
export class ExtraccaoClientesComponent implements OnInit {

  loading: boolean = false;
  disableButton: boolean = true;

   private filter = {
    tipo_identificacao: null,
    tipo_cliente: null,

    tipo_facturacao: null,
    tipo_contrato: null,
    estado_contrato: null,
    objecto_contrato: null,

    genero: null,
  } 

  private localUrl: any;
  private altura: any;
  private largura: any;
  private estado: boolean = false;
  items: any = [];
  generos: any = [];
  tipoclientes: any = [];
  tipoidentidades: any = [];
  tipofacturacao: any = [];
  tipocontrato: any = [];
  estadocontrato: any = [];
  objectocontrato: any = [];

  public filters = {
    search: null, // ordem de pesquisa de elemento
    orderBy: null, // Ordenação
    pagination: {
      perPage: 5,// Entrada - define o limite de resultados a serem gerados
      page: 1, //  define a pagina inicial ou proxima,
      lastPage: null,
      total: null // defini o total de registo da BD
    },
    filter: null // subelemente do filter
    , is_allexel: false,
  }

  constructor(private http: HttpService, private configService: ConfigService, private excelService: ExcelAutoService, private reportClientes: ReportClienteService) { }

  ngOnInit() {
    this.getGeneros();
    this.getTipoCliente();
    this.getTipoIdentidade();
    this.getTipoFacturacao();
    this.getTipoContrato();
    this.getEstadoContrato();
    this.getObjectoContrato();
    this.getPageFilterData(1);
    this.empresaUser()
  }

  exportAsPDF(): void {
    var file = document.getElementsByClassName("exportAsXLSXExtraccaoCliente")[0]
    this.reportClientes.relatorioExtraccaoClientes('save', file,);
  }

  imprimirPDF(): void {
    var file = document.getElementsByClassName("exportAsXLSXExtraccaoCliente")[0];
    this.reportClientes.relatorioExtraccaoClientes('print', file,);
  }



  private empresaUser() {

    this.configService.loaddinStarter('start');

    this.http.call_get('empresa/empresa-user', null).subscribe(

      response => {
        this.localUrl = Object(response).data[0].logotipo
        this.altura = Object(response).data[0].width
        this.largura = Object(response).data[0].height

        this.configService.loaddinStarter('stop');
      }
    );
  }

  setTrue() {
    this.filters.pagination.page = this.filters.pagination.page
    this.estado = true
    this.listaextraccao_cliente()
  }

  private listaextraccao_cliente() {
    this.configService.loaddinStarter('start');

    this.loading = true

    this.filters.filter = this.filter

    this.http.__call('relatorio-extraccao/clientes', this.filters).subscribe(
      response => {
        this.filters.pagination.lastPage = Object(response).data.lastPage;
        this.filters.pagination.page = Object(response).data.page;
        this.filters.pagination.total = Object(response).data.total;
        this.filters.pagination.perPage = Object(response).data.perPage;

        if (this.estado == false) {
          this.items = Object(response).data.data;
        } else {
          this.items = Object(response).data.data;
          this.exportAsXLSX(this.items)
        }
        this.estado = false
        this.loading = false
        this.configService.loaddinStarter('stop');

      }
    );
  }

  exportAsXLSX(data: any): void {
    var CurrentDate = new Date();
    var keys = [
      { key: 'cliente_id', width: 10 },
      { key: 'numero_conta', width: 20 },
      { key: 'agencia', width: 30 },
      { key: 'cliente_nome', width: 30 },
      { key: 'tipo_facturacao', width: 30 },
      { key: 'tipo_cliente', width: 30 },
      { key: 'numero_cliente', width: 30 },
      { key: 'tipo_identificacao', width: 30 },
      { key: 'genero', width: 30 },
      { key: 'telefone', width: 30 },
      { key: 'email', width: 30 },
      { key: 'municipio', width: 30 },
      { key: 'morada', width: 30 },
      { key: 'tipo_contrato', width: 30 },
      { key: 'estado_contrato', width: 30 },
      { key: 'objecto_contrato', width: 30 },
      { key: 'data_inicio', width: 30 },
      { key: 'data_fim', width: 30 },
      { key: 'nivel_sensibilidade', width: 30 },
      { key: 'classe_tarifario', width: 30 },
      { key: 'tipo_medicao', width: 30 },
    ];

    var Cols = ['ID', 'Conta', 'Agência', 'Cliente', 'Tipo Facturação', 'Tipo Cliente', 'Nº Cliente',
      'Tipo Identificação', 'Gênero', 'Telefone', 'Email', 'Município', 'Morada', 'Tipo Contrato',
      'Estado Contrato', 'Objecto Contrato', 'Data Início', 'Data Fim', 'Nível Sensibilidade', 'Classe Tárifario', 'Tipo Medição']
    var title = 'Relatório de Extracção de Clientes, Contas, Contratos'
    var nameFile = "Relatorio_Extraccao_Clientes_Contas_Contratos -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.excels(data, nameFile, this.localUrl, keys, Cols, title, 5, 21, 40,5)
  }

  getPageFilterData(page: number) {
    if (this.filters.pagination.perPage == null) {
      return;
    }
    this.filters.pagination.page = page;

    this.listaextraccao_cliente();
  }

  private getGeneros() {
    this.http.call_get('genero/selectBox', null).subscribe(
      response => {
        this.generos = Object(response);
      }
    );
  }

  private getTipoCliente() {
    this.http.call_get('tipo-cliente/listagem', null).subscribe(
      response => {
        this.tipoclientes = Object(response).data;
      }
    );
  }

  private getTipoIdentidade() {
    this.http.call_get('tipo-identidade/listagem', null).subscribe(
      response => {
        this.tipoidentidades = Object(response).data;
      }
    );
  }

  private getTipoFacturacao() {
    this.http.call_get('tipo-facturacao/selectBox', null).subscribe(
      response => {
        this.tipofacturacao = Object(response);
      }
    );
  }

  private getTipoContrato() {
    this.http.call_get('tipo-contrato/selectBox', null).subscribe(
      response => {
        this.tipocontrato = Object(response)
          ;
      }
    );
  }

  private getEstadoContrato() {
    this.http.call_get('estado-contrato/selectBox', null).subscribe(
      response => {
        this.estadocontrato = Object(response);
      }
    );
  }

  private getObjectoContrato() {
    this.http.call_get('objecto-contrato/selectBox', null).subscribe(
      response => {
        this.objectocontrato = Object(response);
      }
    );
  }





}
