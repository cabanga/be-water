import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExtraccaoClientesComponent } from './extraccao-clientes.component';

describe('ExtraccaoClientesComponent', () => {
  let component: ExtraccaoClientesComponent;
  let fixture: ComponentFixture<ExtraccaoClientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExtraccaoClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtraccaoClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
