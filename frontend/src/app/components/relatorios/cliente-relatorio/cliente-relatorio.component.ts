import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { Response } from 'selenium-webdriver/http';

import { ReportClienteService } from 'src/app/components/report-at/relatorios/report-cliente.service';
import { ExcelService } from 'src/app/services/excel.service';
import * as moment from 'moment';

@Component({
  selector: 'app-cliente-relatorio',
  templateUrl: './cliente-relatorio.component.html',
  styleUrls: ['./cliente-relatorio.component.css']
})

export class ClienteRelatorioComponent implements OnInit {


  loading: boolean = false;
  disableButton: boolean = true;

  private filter = {
    tipo_identidade: null,
    tipo_cliente: null,
    gestor_conta: null,
    genero: null,
    direccao: null,
    email: null,
  }

  items: any = [];
  direccoes: any = [];
  gestores: any = [];
  generos: any = [];
  tipoclientes: any = [];
  tipoidentidades: any = [];


  constructor(private http: HttpService, private configService: ConfigService, private excelService: ExcelService, private reportClientes: ReportClienteService) {

  }

  ngOnInit() {
    this.getGestores();
    this.getGeneros();
    this.getTipoCliente();
    this.getTipoIdentidade();
    this.getDireccao();
    /* this.getPageFilterData(1); */
  }

  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "Lista_clientes-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    var file = document.getElementsByClassName("exportAsXLSXCliente")[0]
    this.reportClientes.relatorioClientes('save', file,);

  }

  imprimirPDF(): void {
    var file = document.getElementsByClassName("exportAsXLSXCliente")[0];
    /*     this.reportClientes.relatorioClientes(this.items,file,'print'); */
    this.reportClientes.relatorioClientes('print', file,);
    // this.reportCliente.relatorioClientes(this.items);
  }


  private listClientes() {
    this.loading = true
    this.http.filters.filter = this.filter;
    this.http.__call('relatorio/clientes', this.http.filters).subscribe(
      response => {

        /*  this.http.filters.pagination.lastPage = Object(response).data.lastPage;
         this.http.filters.pagination.page = Object(response).data.page;
         this.http.filters.pagination.total = Object(response).data.total;
         this.http.filters.pagination.perPage = Object(response).data.perPage; */
        this.items = Object(response).data;
        this.loading = false;
        if (this.items != 0) {
          this.disableButton = false;
        }
      }
    );
  }

  /*  getPageFilterData(page: number) {
     if (this.http.filters.pagination.perPage == null) {
       return;
     }
     this.http.filters.pagination.page = page;
     this.listClientes();
   } */

  private getDireccao() {
    this.http.call_get('direccao/selectBox', null).subscribe(
      response => {
        this.direccoes = Object(response);
      }
    );
  }

  private getGeneros() {
    this.http.call_get('genero/selectBox', null).subscribe(
      response => {
        this.generos = Object(response);
      }
    );
  }

  private getGestores() {
    this.http.call_get('gestor-conta/listagem', null).subscribe(
      response => {
        this.gestores = Object(response).data;
      }
    );
  }

  private getTipoCliente() {
    this.http.call_get('tipo-cliente/listagem', null).subscribe(
      response => {
        this.tipoclientes = Object(response).data;
      }
    );
  }

  private getTipoIdentidade() {
    this.http.call_get('tipo-identidade/listagem', null).subscribe(
      response => {
        this.tipoidentidades = Object(response).data;

      }
    );
  }

}


