import { TestBed } from '@angular/core/testing';

import { ReportComercialService } from './report-comercial.service';

describe('ReportComercialService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportComercialService = TestBed.get(ReportComercialService);
    expect(service).toBeTruthy();
  });
});
