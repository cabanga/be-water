import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosRelatorioComponent } from './pedidos-relatorio.component';

describe('PedidosRelatorioComponent', () => {
  let component: PedidosRelatorioComponent;
  let fixture: ComponentFixture<PedidosRelatorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosRelatorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosRelatorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
