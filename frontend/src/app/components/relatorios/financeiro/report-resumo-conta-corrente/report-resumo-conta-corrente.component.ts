import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl  } from '@angular/forms';

import { ExcelService } from 'src/app/services/excel.service';
import { ReportResumoContaCorrenteService } from 'src/app/components/report-at/relatorios/financeira/report-resumo-conta-corrente.service';

import * as moment from 'moment';
import { ExcelAutoService } from 'src/app/services/excel/excel-auto.service';

@Component({
  selector: 'app-report-resumo-conta-corrente',
  templateUrl: './report-resumo-conta-corrente.component.html',
  styleUrls: ['./report-resumo-conta-corrente.component.css']
})
export class ReportResumoContaCorrenteComponent implements OnInit {

  public titulo="Facturação Detalhada ";
  loading: boolean = false;

  private meses = [
    { nome: "Janeiro", numero: "01" },
    { nome: "Fevereiro", numero: "02" },
    { nome: "Março", numero: "03" },
    { nome: "Abril", numero: "04" },
    { nome: "Maio", numero: "05" },
    { nome: "Junho", numero: "06" },
    { nome: "Julho", numero: "07" },
    { nome: "Agosto", numero: "08" },
    { nome: "Setembro", numero: "09" },
    { nome: "Outubro", numero: "10" },
    { nome: "Novembro", numero: "11" },
    { nome: "Dezembro", numero: "12" }
  ];
  private cobrancas: any = [];
  private lojas: any = []
  private filials: any = []
  private provincias: any = [];
  private servicos: any = []
  private anos: any = []
  private direccaos: any=[];
  private gestores:any=[];

  private disabledButton = true

  submitted = false;
  simpleForm: FormGroup;
  tipoFacturacao:string;
  loadingLojas: string;
  private filtros = {
    servico: null,
    servico_id: null,
    gestor: null,
    ano: null,
    mes: null,
    tipoFacturacao:null,
    mes_nome: null,
    cliente: null,
    cliente_id: null,
    direccao: null,
    moeda_id: null,
    moeda: null,
    data1:null,
    data:null
  }

  public filters = {
    search: null, // ordem de pesquisa de elemento
    orderBy: null, // Ordenação
    pagination: {
      perPage: 5,// Entrada - define o limite de resultados a serem gerados
      page: 1, //  define a pagina inicial ou proxima,
      lastPage: null,
      total: null // defini o total de registo da BD
    },
    filter: null // subelemente do filter
    ,is_allexel:false
  }

  private localUrl: any;
  private largura: any;
  private altura: any;

  constructor(private formBuilder: FormBuilder, private http: HttpService, private route: ActivatedRoute,private configService: ConfigService, private excelService: ExcelAutoService, private RelResumoContaCorrrente: ReportResumoContaCorrenteService) {
    this.createForm();
  }

  ngOnInit() {

    this.getProvincias();
    this.getLojas();
    /* this.getServicos(); */
    this.gerarAno();
    this.empresaUser()
    this.direccaosSelectBox();
  /*   this.getGestores() */
  }



  createForm() {
    this.simpleForm = this.formBuilder.group({/*
      gestor: [null, Validators.required], */
      municipio: [null],
      cliente: [null],
      cliente_id: [null],
      data1: [null, Validators.required],
      data2: [null, [this.matchValidator.bind(this)]],
       direccao: [null],
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.simpleForm.controls;
  }
  matchValidator(control: AbstractControl): { [key: string]: boolean } | null {
    const fromValue = control.value;
    if(fromValue) {

     // console.log(this.simpleForm.controls)
      const startDate = (<FormGroup>this.simpleForm.get('data1')).value;
      const endDate = (<FormGroup>this.simpleForm.get('data2')).value;
      if (startDate <= endDate) {
        //console.log('Control: ', control);
       return null;
      }
      //console.log('Control: ', control);
      return { 'invalidDate' : true };
    }

  }
  private empresaUser() {
    this.configService.loaddinStarter('start');

    this.http.call_get('empresa/empresa-user', null).subscribe(
      response => {
        this.localUrl = Object(response).data[0].logotipo
        this.altura = Object(response).data[0].width
        this.largura = Object(response).data[0].height
        this.configService.loaddinStarter('stop');
      }
    );
  }

  setTrue(){
    this.filters.pagination.page=this.filters.pagination.page
    this.filters.is_allexel=true
    this.onSubmit()
  }
  onSubmit() {

    this.submitted = true;
    // parar aquei se o simpleFormulário for inválido
    if (this.simpleForm.invalid) {
      return;
    }
    const uri = 'relatorio/financeira/resumocontacorrente';
    this.createOrEdit(uri, this.simpleForm);
  }

  createOrEdit(uri: any, simpleFormulario: FormGroup) {
    this.cobrancas = []
    this.loading = true;
    this.disabledButton = true;
    // TODO: usado para fazer a requisição com a api de criação de organismo
    this.http.__call(uri, simpleFormulario.value).subscribe(
      response => {
        this.cobrancas = Object(response).data;
        this.loading = false;
        if (this.cobrancas.length != 0) {
          this.disabledButton = false;

          this.somaTotais();

        }
        if(this.filters.is_allexel==false){
          this.cobrancas = Object(response).data;
        }else{
          this.cobrancas = Object(response).data;
          this.exportAsXLSX(this.cobrancas)
        }
        this.filters.is_allexel=false
        this.loading = false;
      }
    );
  }

  exportAsXLSX(data:any):void {
    var CurrentDate = new Date();

 var keys= [
    { key: 'id', width:30, style: { font: { name: 'Calibri' } } },
    { key: 'nome', width: 50 },
    { key: 'telefone', width: 30 },
    { key: 'morada', width:50, style: { font: { name: 'Calibri' } } },
    { key: 'recibo_sigla', width: 50 },
    { key: 'descricao', width: 50 },
    { key: 'user', width: 50 },
    { key: 'statu', width: 15 },
    { key: 'total', width:30, style: { font: { name: 'Calibri' } } },
    { key: 'data', width:20, style: { font: { name: 'Calibri' } } },
  ];

    var Cols = ["Numero Cliente", "Cliente",	"Telefone",	"Morada",	"Recibo",	"Tarifário",	"Operador",	"estado",	"VALOR TOTAL","Data"]
    var title='Resumo de Contas Correntes'
    var nameFile = "Resumo-de-Contas-Correntes -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
     this.excelService.excels(data,nameFile,this.localUrl,keys,Cols,title,5,10,30,3)
  }


  exportAsPDF(): void {
  var file =  document.getElementsByClassName("tableCobrancaPDF")[0]
  this.RelResumoContaCorrrente.relatorioContaCorrente('save',file,this.localUrl);
  }

  imprimirPDF(): void {
  var file =  document.getElementsByClassName("tableCobrancaPDF")[0]
  this.RelResumoContaCorrrente.relatorioContaCorrente('imprimir',file,this.localUrl);
  }
/*
  private getLojas() {
    this.http.call_get('loja/selectBox', null).subscribe(
      response => {
        this.lojas = Object(response).data
      }
    );
  } */
  private getLojas() {
    this.loadingLojas = 'Carregando...';
    const id = this.simpleForm.getRawValue().filial;
    if (id != "" || id != null || id != "T") {
      this.http.call_get('lojas/selectBox/' + id, null).subscribe(
        response => {
          this.lojas = Object(response)
          this.loadingLojas = 'Loja';
        }
      );
    }
  }

/*
  private getFilials(id) {
      this.http.call_get('municipio/getMunicipioById/'+id, null).subscribe(
        response => {
        this.filials = Object(response)
        console.log(this.filials)
      }
    );
  } */
 /*  private getServicos() {
    this.http.call_get('artigo/selectServicos', null).subscribe(
      response => {
        this.servicos = Object(response).data
      }
    );
  } */

  changeLoja() {
    this.simpleForm.patchValue({
      loja_nome: null
    });
    this.lojas.forEach(element => {
      if (element.id == this.simpleForm.getRawValue().loja) {
        this.simpleForm.patchValue({
          loja_nome: element.nome
        });
      }
    });
  }

  private gerarAno() {
    var fecha = new Date();
    var anyo = fecha.getFullYear();

    let j = 0;
    for (let i = anyo; i >= 2000; i--) {
      this.anos[j] = i;
      j++;
    }
  }

  private getProvincias() {
    this.http.call_get('provincia/selectBox', null).subscribe(
      response => {
        this.provincias = Object(response).data
      }
    );
  }

  private direccaosSelectBox() {
    this.http.call_get('direccao/selectBox', null).subscribe(
      response => {
        this.direccaos = Object(response);
      }
    );
  }

  private clientes: any = [];
  view_client = false;

  private getCliente() {
    this.view_client = true;
    this.http.__call('cliente/search-cliente', { start: 1, end: 10, search: this.simpleForm.getRawValue().cliente }).subscribe(
      response => {
        this.clientes = Object(response).data.data;
      }
    );
  }

  private setCliente(client: any) {
    this.view_client = false;
    this.simpleForm.patchValue({
        cliente_id: client.id,
        cliente: client.nome
    });
/*
    this.getFilials(client.municipio_id) */
  }

 private totais = {
  totals: 0,
  }
  private somaTotais() {
    var totals: number = 0;
    this.cobrancas.forEach(element => {
      totals+= element.total;

    });
    this.totais.totals = totals;
  }

 /*  private getGestores() {
    this.http.call_get('gestor/selectBox', null).subscribe(
      response => {
        this.gestores = Object(response).data
        console.log(this.gestores)
      }
    );
  } */

}
