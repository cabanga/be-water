import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportResumoContaCorrenteComponent } from './report-resumo-conta-corrente.component';

describe('ReportResumoContaCorrenteComponent', () => {
  let component: ReportResumoContaCorrenteComponent;
  let fixture: ComponentFixture<ReportResumoContaCorrenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportResumoContaCorrenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportResumoContaCorrenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
