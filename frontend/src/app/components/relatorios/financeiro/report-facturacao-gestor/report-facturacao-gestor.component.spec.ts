import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportFacturacaoGestorComponent } from './report-facturacao-gestor.component';

describe('ReportFacturacaoGestorComponent', () => {
  let component: ReportFacturacaoGestorComponent;
  let fixture: ComponentFixture<ReportFacturacaoGestorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportFacturacaoGestorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportFacturacaoGestorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
