import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ExcelService } from 'src/app/services/excel.service';
import { RelFacturacaoPorGestorService } from 'src/app/components/report-at/relatorios/financeira/rel-facturacao-por-gestor.service';

import * as moment from 'moment';
@Component({
  selector: 'app-report-facturacao-gestor',
  templateUrl: './report-facturacao-gestor.component.html',
  styleUrls: ['./report-facturacao-gestor.component.css']
})
export class ReportFacturacaoGestorComponent  implements OnInit {

  public titulo="Facturação Por Gestor ";
  loading: boolean = false;
  private meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

  private cobrancas: any = [];
  private direccaos: any = []
  private lojas: any = []
  private produtos: any = []
  private anos: any = [];
  private provincias: any = [];
  private gestores:any=[];

  private disabledButton = true

  submitted = false;
  simpleForm: FormGroup;
  tipoFacturacao:string;

  //v = [ "tipoFacturacao": "POS-PAGO", "direccaos": [{ "direccao": "DVE", "saldos": [{ "nome_mes": "Janeiro", "numero_mes": "01", "valor": 548139161.4 }, { "nome_mes": "Fevereiro", "numero_mes": "02", "valor": 0 }, { "nome_mes": "Março", "numero_mes": "03", "valor": 0 }, { "nome_mes": "Abril", "numero_mes": "04", "valor": 0 }, { "nome_mes": "Maio", "numero_mes": "05", "valor": 0 }, { "nome_mes": "Junho", "numero_mes": "06", "valor": 0 }, { "nome_mes": "Julho", "numero_mes": "07", "valor": 0 }, { "nome_mes": "Agosto", "numero_mes": "08", "valor": 0 }, { "nome_mes": "Setembro", "numero_mes": "09", "valor": 0 }, { "nome_mes": "Outobro", "numero_mes": "10", "valor": 0 }, { "nome_mes": "Novembro", "numero_mes": "11", "valor": 0 }, { "nome_mes": "Dezembro", "numero_mes": "12", "valor": 0 }] }, { "direccao": "DVG", "saldos": [{ "nome_mes": "Janeiro", "numero_mes": "01", "valor": 630715684.93 }, { "nome_mes": "Fevereiro", "numero_mes": "02", "valor": 0 }, { "nome_mes": "Março", "numero_mes": "03", "valor": 0 }, { "nome_mes": "Abril", "numero_mes": "04", "valor": 0 }, { "nome_mes": "Maio", "numero_mes": "05", "valor": 0 }, { "nome_mes": "Junho", "numero_mes": "06", "valor": 0 }, { "nome_mes": "Julho", "numero_mes": "07", "valor": 0 }, { "nome_mes": "Agosto", "numero_mes": "08", "valor": 0 }, { "nome_mes": "Setembro", "numero_mes": "09", "valor": 0 }, { "nome_mes": "Outobro", "numero_mes": "10", "valor": 0 }, { "nome_mes": "Novembro", "numero_mes": "11", "valor": 0 }, { "nome_mes": "Dezembro", "numero_mes": "12", "valor": 0 }] }, { "direccao": "DVR", "saldos": [{ "nome_mes": "Janeiro", "numero_mes": "01", "valor": 2455715.64 }, { "nome_mes": "Fevereiro", "numero_mes": "02", "valor": 0 }, { "nome_mes": "Março", "numero_mes": "03", "valor": 0 }, { "nome_mes": "Abril", "numero_mes": "04", "valor": 0 }, { "nome_mes": "Maio", "numero_mes": "05", "valor": 0 }, { "nome_mes": "Junho", "numero_mes": "06", "valor": 0 }, { "nome_mes": "Julho", "numero_mes": "07", "valor": 0 }, { "nome_mes": "Agosto", "numero_mes": "08", "valor": 0 }, { "nome_mes": "Setembro", "numero_mes": "09", "valor": 0 }, { "nome_mes": "Outobro", "numero_mes": "10", "valor": 0 }, { "nome_mes": "Novembro", "numero_mes": "11", "valor": 0 }, { "nome_mes": "Dezembro", "numero_mes": "12", "valor": 0 }] }] }, { "tipoFacturacao": "PRE-PAGO", "direccaos": [{ "direccao": "DVE", "saldos": [{ "nome_mes": "Janeiro", "numero_mes": "01", "valor": 65306787.51 }, { "nome_mes": "Fevereiro", "numero_mes": "02", "valor": 2756379.1 }, { "nome_mes": "Março", "numero_mes": "03", "valor": 0 }, { "nome_mes": "Abril", "numero_mes": "04", "valor": 0 }, { "nome_mes": "Maio", "numero_mes": "05", "valor": 0 }, { "nome_mes": "Junho", "numero_mes": "06", "valor": 0 }, { "nome_mes": "Julho", "numero_mes": "07", "valor": 0 }, { "nome_mes": "Agosto", "numero_mes": "08", "valor": 0 }, { "nome_mes": "Setembro", "numero_mes": "09", "valor": 0 }, { "nome_mes": "Outobro", "numero_mes": "10", "valor": 0 }, { "nome_mes": "Novembro", "numero_mes": "11", "valor": 0 }, { "nome_mes": "Dezembro", "numero_mes": "12", "valor": 0 }] }, { "direccao": "DVG", "saldos": [{ "nome_mes": "Janeiro", "numero_mes": "01", "valor": 39296712 }, { "nome_mes": "Fevereiro", "numero_mes": "02", "valor": 433200 }, { "nome_mes": "Março", "numero_mes": "03", "valor": 0 }, { "nome_mes": "Abril", "numero_mes": "04", "valor": 0 }, { "nome_mes": "Maio", "numero_mes": "05", "valor": 0 }, { "nome_mes": "Junho", "numero_mes": "06", "valor": 0 }, { "nome_mes": "Julho", "numero_mes": "07", "valor": 0 }, { "nome_mes": "Agosto", "numero_mes": "08", "valor": 0 }, { "nome_mes": "Setembro", "numero_mes": "09", "valor": 0 }, { "nome_mes": "Outobro", "numero_mes": "10", "valor": 0 }, { "nome_mes": "Novembro", "numero_mes": "11", "valor": 0 }, { "nome_mes": "Dezembro", "numero_mes": "12", "valor": 0 }] }, { "direccao": "DVR", "saldos": [{ "nome_mes": "Janeiro", "numero_mes": "01", "valor": 36892844.35 }, { "nome_mes": "Fevereiro", "numero_mes": "02", "valor": 6691634.13 }, { "nome_mes": "Março", "numero_mes": "03", "valor": 0 }, { "nome_mes": "Abril", "numero_mes": "04", "valor": 0 }, { "nome_mes": "Maio", "numero_mes": "05", "valor": 0 }, { "nome_mes": "Junho", "numero_mes": "06", "valor": 0 }, { "nome_mes": "Julho", "numero_mes": "07", "valor": 0 }, { "nome_mes": "Agosto", "numero_mes": "08", "valor": 0 }, { "nome_mes": "Setembro", "numero_mes": "09", "valor": 0 }, { "nome_mes": "Outobro", "numero_mes": "10", "valor": 0 }, { "nome_mes": "Novembro", "numero_mes": "11", "valor": 0 }, { "nome_mes": "Dezembro", "numero_mes": "12", "valor": 0 }] }] }], "mesesCalculoHorizontal": [{ "nome_mes": "Janeiro", "numero_mes": "01", "valor": 1322806905.83 }, { "nome_mes": "Fevereiro", "numero_mes": "02", "valor": 9881213.23 }, { "nome_mes": "Março", "numero_mes": "03", "valor": 0 }, { "nome_mes": "Abril", "numero_mes": "04", "valor": 0 }, { "nome_mes": "Maio", "numero_mes": "05", "valor": 0 }, { "nome_mes": "Junho", "numero_mes": "06", "valor": 0 }, { "nome_mes": "Julho", "numero_mes": "07", "valor": 0 }, { "nome_mes": "Agosto", "numero_mes": "08", "valor": 0 }, { "nome_mes": "Setembro", "numero_mes": "09", "valor": 0 }, { "nome_mes": "Outobro", "numero_mes": "10", "valor": 0 }, { "nome_mes": "Novembro", "numero_mes": "11", "valor": 0 }, { "nome_mes": "Dezembro", "numero_mes": "12", "valor": 0 }]

  constructor(private formBuilder: FormBuilder, private http: HttpService, private route: ActivatedRoute,private configService: ConfigService, private excelService: ExcelService, private relatoriofacturacao: RelFacturacaoPorGestorService) {
    this.route.paramMap.subscribe(params => {
      this.tipoFacturacao =(params.get('name') =='pre-pago'? 'PRE-PAGO' : 'POS-PAGO'); 
    }); 
    this.createForm();
  }

  ngOnInit() {

    this.getProvincias();
    this.gerarAno();
    this.getLojas();
    this.getProdutos();
    this.direccaosSelectBox();
    this.getGestores();
  }



  createForm() {
    this.simpleForm = this.formBuilder.group({
      ano: [null, Validators.required],
      tipoFacturacao: [null],
      loja: [null, Validators.required],
      produto: [null, Validators.required],
      loja_nome: [null],
      produto_nome: [null],
      gestor:[null],
      province: [null, Validators.required]
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.simpleForm.controls;
  }
  onSubmit() {

    this.submitted = true;
    // parar aquei se o simpleFormulário for inválido
    if (this.simpleForm.invalid) {
      return;
    }
    this.simpleForm.patchValue({ tipoFacturacao: this.tipoFacturacao});
    const uri = 'relatorio/financeira/porgestor';
    this.createOrEdit(uri, this.simpleForm);
  }

  /**
  * @name "relatorioFacturacaoRealizadaCobrancaGlobal"
  * @descriptio "Esta Função permite gerar relatorio Facturacao Realizada Cobrança Global"
  * @author "caniggiamoreira@gmail.com"
  * @param start
  * @param end
  */
  createOrEdit(uri: any, simpleFormulario: FormGroup) {
    this.cobrancas = []
    this.loading = true;
    this.disabledButton = true;
    // TODO: usado para fazer a requisição com a api de criação de organismo
    this.http.__call(uri, simpleFormulario.value).subscribe(
      response => {
        this.cobrancas = Object(response).data;
        this.loading = false;
        if (this.cobrancas.length != 0) {
          this.disabledButton = false;
        }
      }
    );
  }

  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "FPG" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " " 
      + moment(CurrentDate).format('H') + ":"+moment(CurrentDate).format('m')
    console.log(document.getElementsByClassName("tableCobranca")[0]);
    this.excelService.exportAsExcelFile(document.getElementsByClassName("tableCobranca")[0], nameFile);
  }
  exportAsPDF(): void {
    this.relatoriofacturacao.relatorioFacturacaoporGestor(this.cobrancas, this.simpleForm.value,this.titulo,'save');
  }

  imprimirPDF(): void {
    this.relatoriofacturacao.relatorioFacturacaoporGestor(this.cobrancas, this.simpleForm.value, this.titulo);
  }


  private direccaosSelectBox() {
    this.http.call_get('direccao/selectBox', null).subscribe(
      response => {
        this.direccaos = Object(response).data;
      }
    );
  }

  private gerarAno() {
    var fecha = new Date();
    var anyo = fecha.getFullYear();

    let j = 0;
    for (let i = anyo; i >= 2000; i--) {
      this.anos[j] = i;
      j++;
    }
  }

  private getLojas() {
    this.http.call_get('loja/selectBox', null).subscribe(
      response => {
        this.lojas = Object(response).data
      }
    );
  }

  private getProdutos() {
    this.http.call_get('artigo/selectProdutos', null).subscribe(
      response => {
        this.produtos = Object(response).data
      }
    );
  }

  changeProduto() {
    this.simpleForm.patchValue({
      loja_nome: null
    });
    this.produtos.forEach(element => {
      if (element.id == this.simpleForm.getRawValue().produto) {
        this.simpleForm.patchValue({
          produto_nome: element.nome
        });
      }
    });
  }
  changeLoja() {
    this.simpleForm.patchValue({
      loja_nome: null
    });
    this.lojas.forEach(element => {
      if (element.id == this.simpleForm.getRawValue().loja) {
        this.simpleForm.patchValue({
          loja_nome: element.nome
        });
      }
    });
  }

  private getProvincias() {
    this.http.call_get('provincia/selectBox', null).subscribe(
      response => {
        this.provincias = Object(response).data
      }
    );
  }

  private getGestores() {
    this.http.call_get('gestor/selectBox', null).subscribe(
      response => {
        this.gestores = Object(response).data
      }
    );
  }

}
