import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportDetalhadaDiariaGlobalComponent } from './report-detalhada-diaria-global.component';

describe('ReportDetalhadaDiariaGlobalComponent', () => {
  let component: ReportDetalhadaDiariaGlobalComponent;
  let fixture: ComponentFixture<ReportDetalhadaDiariaGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportDetalhadaDiariaGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportDetalhadaDiariaGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
