import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl  } from '@angular/forms';

import { ExcelService } from 'src/app/services/excel.service';/*
import { ReportFacturacaoDiariaGlobalService } from 'src/app/components/report-at/relatorios/financeira/report-facturacao-diaria-global.service';
 */
import * as moment from 'moment';

@Component({
  selector: 'app-report-detalhada-diaria-global',
  templateUrl: './report-detalhada-diaria-global.component.html',
  styleUrls: ['./report-detalhada-diaria-global.component.css']
})
export class ReportDetalhadaDiariaGlobalComponent implements OnInit {



  loading: boolean = false;

  private direccaos: any=[];
  private cobrancas: any = [];
  private lojas: any = [];
  private loadingDocumentos: string = "Documento"

  private filials: any = [];
  private loadingLojas: string = "Loja"

  private disabledButton = true

  submitted = false;
  simpleForm: FormGroup;
  tipoFacturacao:string;
  tipoFacturacaoview:string;

  constructor(private formBuilder: FormBuilder, private http: HttpService, private route: ActivatedRoute, private configService: ConfigService, private excelService: ExcelService) {
    this.route.paramMap.subscribe(params => {
      this.tipoFacturacao =(params.get('name') =='pre-pago'? 'PRE-PAGO' : 'POS-PAGO');
      this.tipoFacturacaoview =(params.get('name') =='pre-pago'? 'Pré-Pago' : 'Pós-Pago');
    });
    this.createForm();
  }

  ngOnInit() {
    this.direccaosSelectBox();
    this.getFilials();

  }



  createForm() {
    this.simpleForm = this.formBuilder.group({
      data1: [null, Validators.required],
      data2: [null, [this.matchValidator.bind(this)]],
      direccao: [null, Validators.required],
      tipoFacturacao: [null],
      loja: [null, Validators.required],
      loja_nome: [null],
      filial: [null, Validators.required]
    });

    //console.log(this.simpleForm.controls)


  }
  // convenience getter for easy access to form fields
  get f() {
    return this.simpleForm.controls;
  }

  matchValidator(control: AbstractControl): { [key: string]: boolean } | null {
    const fromValue = control.value;
    if(fromValue) {

     // console.log(this.simpleForm.controls)
      const startDate = (<FormGroup>this.simpleForm.get('data1')).value;
      const endDate = (<FormGroup>this.simpleForm.get('data2')).value;
      if (startDate <= endDate) {
        //console.log('Control: ', control);
       return null;
      }
      //console.log('Control: ', control);
      return { 'invalidDate' : true };
    }

  }



  onSubmit() {

    this.submitted = true;
    // parar aquei se o simpleFormulário for inválido
    if (this.simpleForm.invalid) {
      return;
    }
    this.simpleForm.patchValue({ tipoFacturacao: this.tipoFacturacao});
    const uri = 'relatorio/facturacao/diaria';
    this.createOrEdit(uri, this.simpleForm);
  }

  /**
  * @name "relatorioFacturacaoRealizadaCobrancaGlobal"
  * @descriptio "Esta Função permite gerar relatorio Facturacao Realizada Cobrança Global"
  * @author "caniggiamoreira@gmail.com"
  * @param start
  * @param end
  */
  createOrEdit(uri: any, simpleFormulario: FormGroup) {
    this.cobrancas = []
    this.loading = true;
    this.totais.total = 0;
    this.totais.valor_total_aberto = 0
    this.disabledButton = true;

    // TODO: usado para fazer a requisição com a api de criação de organismo
    this.http.__call(uri, simpleFormulario.value).subscribe(
      response => {
        this.cobrancas = Object(response).data;
        this.loading = false;
        if (this.cobrancas.length != 0) {
          this.disabledButton = false;
          this.somaTotais();
        }
      }
    );


  }
/*
  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "RL" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    console.log(document.getElementsByClassName("tableCobranca")[0]);
    this.excelService.exportAsExcelFile(document.getElementsByClassName("tableCobranca")[0], nameFile);
  }
  exportAsPDF(): void {
    var file =  document.getElementsByClassName("tableCobrancaPDF")[0]
    this.ReportFacturacaoDiariaGlobalService.relatorioPDF(file, this.tipoFacturacao,'save');
  }

  imprimirPDF(): void {
    var file =  document.getElementsByClassName("tableCobrancaPDF")[0]
    this.ReportFacturacaoDiariaGlobalService.relatorioPDF(file, this.tipoFacturacao,'print');
  }
 */

  private getLojas() {
    this.loadingLojas = 'Carregando...';
    const id = this.simpleForm.getRawValue().filial;
    if (id != "" || id != null || id != "T") {
      this.http.call_get('filial/selectBoxFilialPorLojas/' + id, null).subscribe(
        response => {
          this.lojas = Object(response).data
          this.loadingLojas = 'Loja';
        }
      );
    }
  }

  private getFilials() {
    this.http.call_get('filial/selectBox', null).subscribe(
      response => {
        this.filials = Object(response).data
      }
    );
  }

  private direccaosSelectBox() {
    this.http.call_get('direccao/selectBox', null).subscribe(
      response => {
        this.direccaos = Object(response).data;
      }
    );
  }


  private totais = {
    total: 0,
    total_iva: 0,
    valor_total_aberto: 0
  }
  private somaTotais() {
    var valor_aberto: number = 0;
    var total: number = 0;
    var total_iva: number = 0;
    this.cobrancas.forEach(element => {
      total += element.total;
      valor_aberto += (element.pago == 1 ? (element.total) : (element.total - element.valor_aberto));
      total_iva += element.totalComImposto;
    });
    this.totais.total = total;
    this.totais.valor_total_aberto = valor_aberto
    this.totais.total_iva = total_iva;
  }




}
