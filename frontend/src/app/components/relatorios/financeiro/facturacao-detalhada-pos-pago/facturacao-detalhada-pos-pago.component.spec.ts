import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacturacaoDetalhadaPosPagoComponent } from './facturacao-detalhada-pos-pago.component';

describe('FacturacaoDetalhadaPosPagoComponent', () => {
  let component: FacturacaoDetalhadaPosPagoComponent;
  let fixture: ComponentFixture<FacturacaoDetalhadaPosPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacturacaoDetalhadaPosPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacturacaoDetalhadaPosPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
