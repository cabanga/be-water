
import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

import { Router,ActivatedRoute } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ExcelService } from 'src/app/services/excel.service';
import { RelCobracaGlobalService } from 'src/app/components/report-at/relatorios/financeira/rel-cobraca-global.service';
import {  FacturacaoDetalhadaPosPagoService } from 'src/app/components/report-at/relatorios/financeira/facturacao-detalhada-pos-pago.service';
import { RelDetalhadaPosPagoService } from 'src/app/components/report-at/relatorios/financeira/rel-detalhada-pos-pago.service';
import * as moment from 'moment';
import { ReportLojaService } from 'src/app/components/report-at/relatorios/financeira/report-loja.service';
import { ExcelAutoService } from 'src/app/services/excel/excel-auto.service';
@Component({
  selector: 'app-facturacao-detalhada-pos-pago',
  templateUrl: './facturacao-detalhada-pos-pago.component.html',
  styleUrls: ['./facturacao-detalhada-pos-pago.component.css']
})
export class FacturacaoDetalhadaPosPagoComponent implements OnInit {


  public titulo = "Facturação Detalhada ";
  loading: boolean = false;

  private meses = [
    { nome: "Janeiro", numero: "01" },
    { nome: "Fevereiro", numero: "02" },
    { nome: "Março", numero: "03" },
    { nome: "Abril", numero: "04" },
    { nome: "Maio", numero: "05" },
    { nome: "Junho", numero: "06" },
    { nome: "Julho", numero: "07" },
    { nome: "Agosto", numero: "08" },
    { nome: "Setembro", numero: "09" },
    { nome: "Outubro", numero: "10" },
    { nome: "Novembro", numero: "11" },
    { nome: "Dezembro", numero: "12" }
  ];
  private meses_ = [];

  private filtros = {
    servico: null,
    servico_id: null,
    gestor: null,
    ano: null,
    mes: null,
    tipoFacturacao:null,
    mes_nome: null,
    cliente: null,
    cliente_id: null,
    direccao: null,
    moeda_id: null,
    moeda: null
  }
  public filters = {
    search: null, // ordem de pesquisa de elemento
    orderBy: null, // Ordenação
    pagination: {
      perPage: 5,// Entrada - define o limite de resultados a serem gerados
      page: 1, //  define a pagina inicial ou proxima,
      lastPage: null,
      total: null // defini o total de registo da BD
    },
    filter: null // subelemente do filter
    ,is_allexel:false
  }

  private localUrl: any;
  private largura: any;
  private altura: any;
  private cobrancas: any = [];
  private lojas: any = [];
  private moedas: any = []
  private provincias: any = [];
  private servicos: any = []
  private anos: any = []
  private direccaos: any = [];
  private gestores: any = [];
  private mesesbill: any = [];
  private mesesbill2: any = [];
  private disabledButton = true;
  private resetFlag = 0;
  operadores:any = [];
  submitted = false;
  simpleForm: FormGroup;



  constructor(private formBuilder: FormBuilder, private http: HttpService, private route: ActivatedRoute, private configService: ConfigService, private excelServices: FacturacaoDetalhadaPosPagoService,private relCobracaGlobal: RelDetalhadaPosPagoService,private excelService: ExcelAutoService) {

    this.createForm();
  }

  ngOnInit() {
    this.empresaUser()
    this.filtros = null;
    this.gerarAno();
    this.direccaosSelectBox();
    this.simpleForm.get('mes');
    this.simpleForm.get('mes_nome');

    /* this.simpleForm.get('moeda_id').disable(); */
    this.simpleForm.get('direccao');/*
    this.simpleForm.get('gestor').disable();
    this.simpleForm.get('servico_id').disable();
    this.simpleForm.get('servico').disable(); */
   this.simpleForm.get('tipoFacturacao');
    this.simpleForm.get('cliente');
    this.simpleForm.get('cliente_id');
    this.getoperadores()
  }



  createForm() {
    this.simpleForm = this.formBuilder.group({
      ano: [null],
      mes: [null],
      mes_nome: [null],
      moeda: [null],
      tipoFacturacao: [null],
      data1: [null, Validators.required],
      data2: [null, [this.matchValidator.bind(this)]],
      /*
      gestor: [null],
      servico: [null],
      servico_id: [null], */
      cliente: [null],
      cliente_id: [null],
      direccao: [null],
      moeda_id: [null],
      operador: [null],
    });
  }

  resetCampos(flag) {
    // if(this.resetFlag == 1) {
    switch (flag) {
      case 1: // ano
       // this.getServicos();
       // this.getGestores();
       // this.direccaosSelectBox();
        this.simpleForm.get('mes').reset();
        /*this.simpleForm.get('moeda_id').reset();
        this.simpleForm.get('direccao').reset();
        this.simpleForm.get('gestor').reset();
        this.simpleForm.get('servico_id').reset(); */
        this.simpleForm.get('cliente').reset();
        this.simpleForm.get('cliente_id').reset();
        this.simpleForm.get('direccao');
        /*this.simpleForm.get('moeda_id').disable();
        this.simpleForm.get('gestor').disable();
        this.simpleForm.get('servico_id').disable(); */
        this.simpleForm.get('cliente');


        this.simpleForm.get('mes');

        break;
      case 2: // mes
       // this.getServicos(); this.getGestores(); this.change_mes();
        this.simpleForm.get('moeda_id').reset();
        this.getMoedas();
        this.simpleForm.get('direccao').reset();/*
        this.simpleForm.get('gestor').reset(); *//*
        this.simpleForm.get('servico_id').reset(); */
        this.simpleForm.get('cliente').reset();
        this.simpleForm.get('cliente_id').reset();

      /*   this.simpleForm.get('moeda_id').disable(); */
        this.simpleForm.get('direccao');/*
        this.simpleForm.get('gestor').disable();
        this.simpleForm.get('servico_id').disable(); */
        /* this.simpleForm.get('cliente').disable(); */

        /* this.simpleForm.get('moeda_id').enable(); */
        break;
      case 3: // moeda
       // this.changeMoeda()
        this.simpleForm.get('direccao').reset();
        /* this.direccaosSelectBox(); *//*
        this.simpleForm.get('gestor').reset(); */
       /*  this.simpleForm.get('servico_id').reset(); */
        this.simpleForm.get('cliente').reset();
        this.simpleForm.get('cliente_id').reset();
/*
        this.simpleForm.get('gestor').disable();
        this.simpleForm.get('servico_id').disable(); */
        this.simpleForm.get('cliente');
        this.simpleForm.get('cliente_id');


        this.simpleForm.get('direccao');

        break;
      case 4: // direcção
/*
        this.simpleForm.get('gestor').reset();
         this.getGestores();
        this.simpleForm.get('servico_id').reset(); */
        this.simpleForm.get('cliente').reset();
        this.simpleForm.get('cliente_id').reset();
/*
        this.simpleForm.get('servico_id').disable(); */

        this.simpleForm.get('cliente_id');
        this.simpleForm.get('cliente');
/*
        this.simpleForm.get('gestor').enable(); */
        break;
      case 5: // gestor
      /*
        this.simpleForm.get('servico_id').reset();
        this.simpleForm.get('servico').reset();
         this.getServicos();
        this.simpleForm.get('servico_id').enable(); */

        this.simpleForm.get('cliente').reset();
        this.simpleForm.get('cliente_id').reset();

        break;

        case 6: // serviço
        this.simpleForm.get('cliente').reset();
        this.simpleForm.get('cliente_id').reset();
        this.simpleForm.get('cliente_id');
        this.simpleForm.get('cliente');/*
        this.simpleForm.get('tipoFacturacao').reset(); */
        break;
        case 7:
          if(this.simpleForm.get('tipoFacturacao')!=null){
            this.simpleForm.get('tipoFacturacao').reset();
          }else{

            this.simpleForm.get('tipoFacturacao');
          }
        break;
      default:
        break;
    }
    //}

  }

  // convenience getter for easy access to form fields
  get f() {
    return this.simpleForm.controls;
  }
  matchValidator(control: AbstractControl): { [key: string]: boolean } | null {
    const fromValue = control.value;
    if(fromValue) {

     // console.log(this.simpleForm.controls)
      const startDate = (<FormGroup>this.simpleForm.get('data1')).value;
      const endDate = (<FormGroup>this.simpleForm.get('data2')).value;
      if (startDate <= endDate) {
        //console.log('Control: ', control);
       return null;
      }
      //console.log('Control: ', control);
      return { 'invalidDate' : true };
    }

  }
  onSubmit() {

    this.submitted = true;
    // parar aquei se o simpleFormulário for inválido
    if (this.simpleForm.invalid) {
      return;
    }

    /* this.simpleForm.patchValue({ tipoFacturacao: this.tipoFacturacao}); */
    const uri = 'relatorio/financeira/detalhada';
    this.createOrEdit(uri, this.simpleForm);
    this.resetFlag = 1;
  }
  getValuesFatura(data:any){
    this.simpleForm.patchValue({
      tipoFacturacao: data
    });
  }

  private totais = {
    totals: 0,
    }
    private somaTotais() {
      var totals: number = 0;
      this.cobrancas.forEach(element => {
        totals+= element.total;

      });
      this.totais.totals = totals;
    }

  private empresaUser() {
    this.configService.loaddinStarter('start');

    this.http.call_get('empresa/empresa-user', null).subscribe(
      response => {
        this.localUrl = Object(response).data[0].logotipo
        this.altura = Object(response).data[0].width
        this.largura = Object(response).data[0].height
        this.configService.loaddinStarter('stop');
      }
    );
  }

  setTrue(){
    this.filters.pagination.page=this.filters.pagination.page
    this.filters.is_allexel=true
    this.onSubmit()
  }
  createOrEdit(uri: any, simpleFormulario: FormGroup) {
    this.filtros = this.simpleForm.value;
    this.cobrancas = []
    this.loading = true;
    this.disabledButton = true;
    this.totais.totals = 0;
    console.log(simpleFormulario)
    // TODO: usado para fazer a requisição com a api de criação de organismo
    this.http.__call(uri, simpleFormulario.value).subscribe(
      response => {
        this.cobrancas = Object(response).data;
        this.loading = false;
        if (this.cobrancas.length != 0) {
          this.disabledButton = false;

          this.somaTotais();

        }
        if(this.filters.is_allexel==false){
          this.cobrancas = Object(response).data;
        }else{
          this.cobrancas = Object(response).data;
          this.exportAsXLSX(this.cobrancas)
        }
        this.filters.is_allexel=false
        this.loading = false;
      }
    );
  }

  exportAsXLSX(data:any):void {
    var CurrentDate = new Date();

 var keys= [
    { key: 'cliente_id', width:30, style: { font: { name: 'Calibri' } } },
    { key: 'cliente', width: 50 },
    { key: 'factura_sigla', width: 30 },
    { key: 'pagos', width:15, style: { font: { name: 'Calibri' } } },
    { key: 'descricao', width: 50 },
    { key: 'user', width: 50 },
    { key: 'statu', width: 15 },
    { key: 'valor_aberto', width:30, style: { font: { name: 'Calibri' } } },
    { key: 'total', width: 30 },
    { key: 'data', width:20, style: { font: { name: 'Calibri' } } },
  ];

    var Cols = ["Número Cliente",	"Nome Cliente"	,"Factura","Saldado",	"Tarifário","Operador","estado","VALOR EM ABERTO","VALOR TOTAL","Data Factura"]
    var title='FATURAÇÃO DETALHADA'
    var nameFile = "facturação_detalhada -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
     this.excelService.excels(data,nameFile,this.localUrl,keys,Cols,title,5,10,50,3)
  }

  exportAsPDF(): void {
    console.log(this.filtros)
    var file = document.getElementsByClassName("tableCobranca")[0]
    this.relCobracaGlobal.relatorioFacturacaoPospago(file, 'save', this.filtros,this.localUrl);
  }

  imprimirPDF(): void {
    var file = document.getElementsByClassName("tableCobranca")[0]
    this.relCobracaGlobal.relatorioFacturacaoPospago(file, 'print', this.filtros,this.localUrl);
  }
 /*  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "FDP" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.ExcelExportFacturacaoDetalhada(this.cobrancas, this.filtros, nameFile);
  }




  /* private getServicos() {
    this.filtros = this.simpleForm.value;
    this.http.__call('artigo/selectServicos', { filtros: this.simpleForm.value }).subscribe(
      response => {
        this.servicos = Object(response).data
      }
    );
  } */

  private getmeses() {

    //var  ano = this.simpleForm.value.ano;

    this.http.__call('billRunHeader/meses', this.simpleForm.value.ano).subscribe(
      response => {
        this.mesesbill = Object(response).data

        this.meses.forEach(element => {
          if (element.numero == this.mesesbill.mes) {
            this.mesesbill2 = this.meses[element.numero]
          }
        });
        console.log(this.mesesbill2);

      }
    );
  }

  /* changeServico() {
    this.simpleForm.patchValue({
      servico: null
    });
    this.servicos.forEach(element => {
      if (element.id == this.simpleForm.getRawValue().servico_id) {
        this.simpleForm.patchValue({
          servico: element.nome
        });
      }
    });
  } */

  changeMoeda() {
    this.simpleForm.patchValue({
      moeda: null
    });
    this.moedas.forEach(element => {
      if (element.id == this.simpleForm.getRawValue().moeda_id) {
        this.simpleForm.patchValue({
          moeda: element.nome
        });
      }
    });
  }

  change_mes() {
    this.simpleForm.patchValue({
      mes_nome: null
    });
    this.meses.forEach(element => {
      if (element.numero == this.simpleForm.getRawValue().mes) {
        this.simpleForm.patchValue({
          mes_nome: element.nome
        });
      }
    });
  }

  private gerarAno() {
    var fecha = new Date();
    var anyo = fecha.getFullYear();

    let j = 0;
    for (let i = anyo; i >= 2019; i--) {
      this.anos[j] = i;
      j++;
      console.log( this.anos[j])
    }

  }

  private direccaosSelectBox() {
    //this.filtros = this.simpleForm.value;
    this.configService.loaddinStarter('start');
    this.http.call_get('direccao/selectBox', null).subscribe(
      response => {
        this.direccaos = Object(response);
        this.configService.loaddinStarter('stop');
      }
    );
  }

  private clientes: any = [];
  view_client = false;

  private getCliente() {
    // this.filtros = this.simpleForm.value;
    if (this.simpleForm.getRawValue().cliente == "") {
      this.view_client = false;
      this.simpleForm.get('cliente_id').reset();
      this.simpleForm.get('cliente').reset();
      this.clientes = [];
    } else {
      this.view_client = true;
      this.http.__call('cliente/searchClienteFacturaEmail', { start: 1, end: 10, search: this.simpleForm.getRawValue().cliente, filtros: this.simpleForm.value }).subscribe(
        response => {
          this.clientes = Object(response).data.data;
        }
      );
    }
  }

  private setCliente(client: any) {
    this.view_client = false;
    this.simpleForm.patchValue({
      cliente_id: client.id,
      cliente: client.nome
    });
    this.clientes = [];
  }

  private setMes(mes: any) {
    this.simpleForm.patchValue({
      mes_nome: mes,
    });
  }


  private setServico(servico: any) {
    this.simpleForm.patchValue({
      servico: servico.nome,
    });
  }


  private getoperadores() {
    this.http.call_get('user/operador', null).subscribe(
      response => {
        this.operadores = Object(response).data
      }
    );
  }
  private getMoedas() {
    //this.filtros = this.simpleForm.value;
    this.http.call_get('moeda/moeda',null).subscribe(
      response => {
        this.moedas = Object(response).data
      }
    );
  }
  /* private getGestores() {
    // this.filtros = this.simpleForm.value;
    this.http.__call('gestor/selectBox', { filtros: this.simpleForm.value }).subscribe(
      response => {
        this.gestores = Object(response).data
      }
    );
  }
 */
}
