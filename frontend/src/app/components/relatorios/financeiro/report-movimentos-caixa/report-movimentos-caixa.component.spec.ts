import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportMovimentosCaixaComponent } from './report-movimentos-caixa.component';

describe('ReportMovimentosCaixaComponent', () => {
  let component: ReportMovimentosCaixaComponent;
  let fixture: ComponentFixture<ReportMovimentosCaixaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportMovimentosCaixaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportMovimentosCaixaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
