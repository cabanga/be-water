import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { Response } from 'selenium-webdriver/http';
import { FormBuilder, FormGroup, Validators,AbstractControl  } from '@angular/forms';
/*
import { RelMovimentoCaixaService } from 'src/app/components/report-at/relatorios/financeira/rel-movimento-caixa.service'; */
import { ExcelService } from 'src/app/services/excel.service';
import * as moment from 'moment';
import { ExcelAutoService } from 'src/app/services/excel/excel-auto.service';
import { ReportLojaService } from 'src/app/components/report-at/relatorios/financeira/report-loja.service';

@Component({
  selector: 'app-report-movimentos-caixa',
  templateUrl: './report-movimentos-caixa.component.html',
  styleUrls: ['./report-movimentos-caixa.component.css']
})
export class ReportMovimentosCaixaComponent implements OnInit {



  loading: boolean = false;

  items:any = [];
  dias:any = [];
  operadores:any = [];
  lojas:any = [];
  movimentos:any = [];

  private disabledButton = true

  submitted = false;
  public filters = {
    search: null, // ordem de pesquisa de elemento
    orderBy: null, // Ordenação
    pagination: {
      perPage: 5,// Entrada - define o limite de resultados a serem gerados
      page: 1, //  define a pagina inicial ou proxima,
      lastPage: null,
      total: null // defini o total de registo da BD
    },
    filter: null // subelemente do filter
    ,is_allexel:false
  }

  private localUrl: any;
  private largura: any;
  private altura: any;
  simpleForm: FormGroup;
  tipoFacturacao:string;
  private loadingLojas: string = "Agência"

  constructor(private formBuilder: FormBuilder,private http: HttpService,private excelService: ExcelAutoService, private configService: ConfigService,private reportLoja: ReportLojaService) {
    this.createForm();
  }

  ngOnInit() {
    /* this.getLojas(); */
    this.getoperadores();
    this.empresaUser()

  }

 /*  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "Movimentos-Caixa-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  } */
 /*  exportAsPDF(): void {
    var file =  document.getElementsByClassName("exportAsXLSXCliente")[0]
    this.reportMovimentoCaixa.relatoriomovimentocaixa(file,'save');
  }

  imprimirPDF(): void {
    var file =  document.getElementsByClassName("exportAsXLSXCliente")[0]
    this.reportMovimentoCaixa.relatoriomovimentocaixa(file,'print');
  } */

  createForm() {
    this.simpleForm = this.formBuilder.group({
      data1: [null, Validators.required],
      data2: [null, [this.matchValidator.bind(this)]],
      operador: [null, Validators.required],
      loja: [null],
      dia: [null,],
      loja_nome: [null],
    });
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.simpleForm.controls;
  }
  onSubmit() {

    this.submitted = true;
    // parar aquei se o simpleFormulário for inválido
    if (this.simpleForm.invalid) {
      return;
    }
    const uri = 'relatorio/financeira/movimentocaixa';
    this.createOrEdit(uri, this.simpleForm);
  }
  private empresaUser() {
    this.configService.loaddinStarter('start');

    this.http.call_get('empresa/empresa-user', null).subscribe(
      response => {
        this.localUrl = Object(response).data[0].logotipo
        this.altura = Object(response).data[0].width
        this.largura = Object(response).data[0].height
        this.configService.loaddinStarter('stop');
      }
    );
  }

  setTrue(){
    this.filters.pagination.page=this.filters.pagination.page
    this.filters.is_allexel=true
    this.onSubmit()
  }
  createOrEdit(uri: any, simpleFormulario: FormGroup) {
    this.movimentos = []
    this.loading = true;
    this.disabledButton = true;
    this.totais.valor_abertura = 0;
    this.totais.valor_vendas = 0;
    this.totais.valor_fecho = 0
    // TODO: usado para fazer a requisição com a api de criação de organismo
    this.http.__call(uri, simpleFormulario.value).subscribe(
      response => {
        this.movimentos = Object(response).data;
        console.log(this.movimentos)
        this.loading = false;
        if (this.movimentos.length != 0) {
          this.disabledButton = false;
          this.somaTotais();
        }
        if(this.filters.is_allexel==false){
          this.movimentos = Object(response).data;
        }else{
          this.movimentos = Object(response).data;
          this.exportAsXLSX(this.movimentos)
        }
        this.filters.is_allexel=false
        this.loading = false;
      }
    );
  }

  exportAsXLSX(data:any):void {
    var CurrentDate = new Date();
     for(let i=0;i<data.length;i++){
       if(data[i].is_active==1){
           data[i].is_active="Aberto"
       }else{
           data[i].is_active="Fechado"
       }
     }
    var keys = [
      { key: 'loja', width:40, style: { font: { name: 'Calibri' } } },
      { key: 'operador', width: 50 },
      { key: 'is_active', width: 15 },
      { key: 'data_abertura', width:20, style: { font: { name: 'Calibri' } } },
      { key: 'hora_abertura', width:20, style: { font: { name: 'Calibri' } } },
      { key: 'data_fecho', width: 20 },
      { key: 'hora_fecho', width: 20 },
      { key: 'valor_abertura', width: 30 },
      { key: 'valor_vendas', width:40, style: { font: { name: 'Calibri' } } },
      { key: 'valor_fecho', width: 50 },
    ];

    var Cols = ["Loja","Operador","Estado","Data Abertura","hora Abertura","Data Fecho","hora Fecho","Valor Abertura","Total Vendas","Valor Fecho"]
    var title='MOVIMENTOS DE CAIXA'
    var nameFile = "MOVIMENTOS_DE_CAIXA -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
     this.excelService.excels(data,nameFile,this.localUrl,keys,Cols,title,5,10,40,3)
  }
  exportAsPDF(): void {
    this.reportLoja.relatorioLoja(this.movimentos, this.simpleForm.value, 'save',this.localUrl);
  }

  imprimirPDF(): void {
    this.reportLoja.relatorioLoja(this.movimentos, this.simpleForm.value,'save',this.localUrl);
  }
  matchValidator(control: AbstractControl): { [key: string]: boolean } | null {
    const fromValue = control.value;
    if(fromValue) {

     // console.log(this.simpleForm.controls)
      const startDate = (<FormGroup>this.simpleForm.get('data1')).value;
      const endDate = (<FormGroup>this.simpleForm.get('data2')).value;
      if (startDate <= endDate) {
        //console.log('Control: ', control);
       return null;
      }
      //console.log('Control: ', control);
      return { 'invalidDate' : true };
    }

  }

  changeProduto() {
    this.simpleForm.patchValue({
      loja_nome: null
    });
    this.operadores.forEach(element => {
      if (element.id == this.simpleForm.getRawValue().produto) {
        this.simpleForm.patchValue({
          produto_nome: element.nome
        });
      }
    });
  }

  changeLoja() {
    this.simpleForm.patchValue({
      loja_nome: null
    });
    this.lojas.forEach(element => {
      if (element.id == this.simpleForm.getRawValue().loja) {
        this.simpleForm.patchValue({
          loja_nome: element.nome
        });
      }
    });
  }

 /* private listClientes() {
    this.loading= true
    this.http.filters.filter = this.filter;
    this.http.__call('relatorio/clientes', this.http.filters).subscribe(
      response => {
        this.items = Object(response).data;
        this.loading = false;
      }
    );
  }

  getPageFilterData() {
    this.listClientes();
  }*/
  private getLojas(id) {
    this.loadingLojas = 'Carregando...';
    this.http.call_get('lojas/selectBox/'+id,null).subscribe(
      response => {
        this.lojas = Object(response)
      }
    );
  }

  private getoperadores() {
    this.http.call_get('user/operador', null).subscribe(
      response => {
        this.operadores = Object(response).data
      }
    );

    const id = this.simpleForm.getRawValue().operador;
    this.operadores.map(res=>{
      if(res.id==id){
        this.getLojas(res.loja_id)
      }else{
        return
      }
   })
  }
  private totais = {
    valor_abertura: 0,
    valor_vendas: 0,
    valor_fecho: 0
  }
  private somaTotais() {
    var totalva: number = 0;
    var totalvv: number = 0;
    var totalvf: number = 0;
    this.movimentos.forEach(element => {
      totalva += element.valor_abertura;
      totalvv += element.valor_vendas;
      totalvf += element.valor_fecho;
    });
    this.totais.valor_abertura = totalva;
    this.totais.valor_vendas = totalvv;
    this.totais.valor_fecho = totalvf
  }
}
