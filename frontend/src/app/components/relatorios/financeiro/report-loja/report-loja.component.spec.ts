import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportLojaComponent } from './report-loja.component';

describe('ReportLojaComponent', () => {
  let component: ReportLojaComponent;
  let fixture: ComponentFixture<ReportLojaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportLojaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportLojaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
