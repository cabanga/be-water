import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportServicosContratadosComponent } from './report-servicos-contradados.component';

describe('ReportServicosContratadosComponent', () => {
  let component: ReportServicosContratadosComponent;
  let fixture: ComponentFixture<ReportServicosContratadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportServicosContratadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportServicosContratadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
