import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportIVAComponent } from './report-iva.component';

describe('ReportIVAComponent', () => {
  let component: ReportIVAComponent;
  let fixture: ComponentFixture<ReportIVAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportIVAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportIVAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
