import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportPagamentoGlobalComponent } from './report-pagamento-global.component';

describe('ReportPagamentoGlobalComponent', () => {
  let component: ReportPagamentoGlobalComponent;
  let fixture: ComponentFixture<ReportPagamentoGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportPagamentoGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportPagamentoGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
