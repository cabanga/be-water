import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportCotrancaGlobalComponent } from './report-cotranca-global.component';

describe('ReportCotrancaGlobalComponent', () => {
  let component: ReportCotrancaGlobalComponent;
  let fixture: ComponentFixture<ReportCotrancaGlobalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportCotrancaGlobalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportCotrancaGlobalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
