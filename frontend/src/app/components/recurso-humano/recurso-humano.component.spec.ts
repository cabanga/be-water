import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecursoHumanoComponent } from './recurso-humano.component';

describe('RecursoHumanoComponent', () => {
  let component: RecursoHumanoComponent;
  let fixture: ComponentFixture<RecursoHumanoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecursoHumanoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecursoHumanoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
