import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiposPedidoComponent } from './tipos-pedido.component';

describe('TiposPedidoComponent', () => {
  let component: TiposPedidoComponent;
  let fixture: ComponentFixture<TiposPedidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiposPedidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiposPedidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
