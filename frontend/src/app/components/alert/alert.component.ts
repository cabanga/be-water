import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/providers/config/config.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  private message = null;
  private show = false;
  private class = null;

  constructor(private config: ConfigService) { }

  ngOnInit() {
    this.config.alertEvent.subscribe(
      res => {
        this.showAlert(res.message, res.class, res.show);
      }
    );

  }


  public showAlert(message :string, cls:string, show: boolean) {
    this.message = message;
    this.class = cls;
    this.show = show;
    
    setTimeout(() => {
      this.show = false;
    }, 5000);
  }

}
