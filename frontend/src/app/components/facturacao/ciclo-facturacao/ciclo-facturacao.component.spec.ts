import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CicloFacturacaoComponent } from './ciclo-facturacao.component';

describe('CicloFacturacaoComponent', () => {
  let component: CicloFacturacaoComponent;
  let fixture: ComponentFixture<CicloFacturacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CicloFacturacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CicloFacturacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
