import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacturacaoAutomaticaComponent } from './facturacao-automatica.component';

describe('FacturacaoAutomaticaComponent', () => {
  let component: FacturacaoAutomaticaComponent;
  let fixture: ComponentFixture<FacturacaoAutomaticaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacturacaoAutomaticaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacturacaoAutomaticaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
