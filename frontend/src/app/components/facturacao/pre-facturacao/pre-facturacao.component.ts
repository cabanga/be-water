import { Component, OnInit } from "@angular/core";
import { ConfigService } from "src/app/providers/config/config.service";
import { FacturaService } from "../factura.service";

@Component({
selector: "app-pre-facturacao",
templateUrl: "./pre-facturacao.component.html",
styleUrls: ["./pre-facturacao.component.css"]
})
export class PreFacturacaoComponent implements OnInit {

  loading: boolean
  rotas: any = []
  rota: any = {}
  roteiros: any = []
  contratos: any = []

  constructor(
    private configService: ConfigService,
    private _facturaService: FacturaService,
  ) {
    this._loadingDependencies()
  }

  ngOnInit() {
    this.loading = false
  }

  _getRoteirosByRotaId(id: number){
    let rota = this.rotas.find(obj => obj.id == id)
    this.rota = rota
    this.roteiros = rota.roteiros
  }

  _initProcess(){
    this.loading = true
    this._facturaService.validarPreFactura( this.roteiros )
    .subscribe(
      response => {
        if (response.open_modal) {
          this.contratos = response.data.map(obj => obj.data)
          this._facturaService._openModal('media_consumo_btn')
        } else {
          this._processamentoPrefacturacao()
        }
        this.loading = false
      }
    )
  }

  _loadingDependencies(){
    this._facturaService.getRotas()
    .subscribe(
      response => {
        let data = response.data
        this.rotas = data.filter(item => item.roteiros.length)
      }
    )
  }

  _updateMediaConsumo(){
    this.loading = true
    this._facturaService.updateMediaConsumo(this.contratos)
    .subscribe(response => {
      this._facturaService._closeModal('closeModalMediaConsumo')
      this._processamentoPrefacturacao()
      this.loading = false
    })
  }

  _processamentoPrefacturacao(){

    for(let leitura of this.roteiros){
      this._facturaService.precessPreFactura(leitura)
      .subscribe(
        response => {
          this.configService.showAlert("Pré-facturação processada com sucesso", "alert-success", true);
        }
      )
    }

    this.loading = false

    //this.rotas = []
  }

}
