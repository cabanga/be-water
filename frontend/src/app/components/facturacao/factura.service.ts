import { Injectable, EventEmitter } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment'
import { HttpService } from 'src/app/providers/http/http.service'


@Injectable({
  providedIn: 'root'
})

export class FacturaService {

  private token = sessionStorage.getItem('sessionToken')

  private headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Authorization', `Bearer ${this.token}`)

  constructor(
    private http: HttpService,
    private _http_client: HttpClient
  ) { }


  //=================================================================================================

  validarPreFactura(leituras: any) {
    return this._http_client.post<any>(
      `${environment.app_url}api/${environment.apiVersion}/facturas/validar_pre_facturacao`,
      {leituras: leituras},
      { 'headers': this.headers }
    )
  }

  precessPreFactura(leitura: any) {
    return this._http_client.post<any>(
      `${environment.app_url}api/${environment.apiVersion}/facturas/processar_pre_facturacao`,
      leitura,
      { 'headers': this.headers }
    )
  }

  getRotas() {
    return this._http_client.get<any>(
      `${environment.app_url}api/${environment.apiVersion}/facturas/rotas`,
      { 'headers': this.headers }
    )
  }

  getCharges() {
    return this._http_client.get<any>(
      `${environment.app_url}api/${environment.apiVersion}/facturas/charges`,
      { 'headers': this.headers }
    )
  }

  updateMediaConsumo(contratos: any) {
    return this._http_client.put<any>(
      `${environment.app_url}api/${environment.apiVersion}/contartos/actualizar-media-consumo`,
      {contratos: contratos},
      { 'headers': this.headers }
    )
  }

  _closeModal(closeModal){
    var action = document.getElementById(closeModal)
    action.click()
  }

  _openModal(closeModal){
    var action = document.getElementById(closeModal)
    action.click()
  }

}
