import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeituraContradorComponent } from './leitura-contrador.component';

describe('LeituraContradorComponent', () => {
  let component: LeituraContradorComponent;
  let fixture: ComponentFixture<LeituraContradorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeituraContradorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeituraContradorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
