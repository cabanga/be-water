import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfMotivoDenunciaComponent } from './conf-motivo-denuncia.component';

describe('ConfMotivoDenunciaComponent', () => {
  let component: ConfMotivoDenunciaComponent;
  let fixture: ComponentFixture<ConfMotivoDenunciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfMotivoDenunciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfMotivoDenunciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
