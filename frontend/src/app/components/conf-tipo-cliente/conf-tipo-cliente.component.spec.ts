import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoClienteComponent } from './conf-tipo-cliente.component';

describe('ConfTipoClienteComponent', () => {
  let component: ConfTipoClienteComponent;
  let fixture: ComponentFixture<ConfTipoClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
