import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoOcorrenciaComponent } from './tipo-ocorrencia.component';

describe('TipoOcorrenciaComponent', () => {
  let component: TipoOcorrenciaComponent;
  let fixture: ComponentFixture<TipoOcorrenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoOcorrenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoOcorrenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
