import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfEstadoContratoComponent } from './conf-estado-contrato.component';

describe('ConfEstadoContratoComponent', () => {
  let component: ConfEstadoContratoComponent;
  let fixture: ComponentFixture<ConfEstadoContratoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfEstadoContratoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfEstadoContratoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
