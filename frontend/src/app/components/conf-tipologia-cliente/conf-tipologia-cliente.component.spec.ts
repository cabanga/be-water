import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipologiaClienteComponent } from './conf-tipologia-cliente.component';

describe('ConfTipologiaClienteComponent', () => {
  let component: ConfTipologiaClienteComponent;
  let fixture: ComponentFixture<ConfTipologiaClienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipologiaClienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipologiaClienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
