import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

@Component({
  selector: 'app-conf-tipologia-cliente',
  templateUrl: './conf-tipologia-cliente.component.html',
  styleUrls: ['./conf-tipologia-cliente.component.css']
})
export class ConfTipologiaClienteComponent implements OnInit {

  private tipologiacliente = {
    id: null,
    descricao: null,
    slug: null,
    juro_mora: null,
    sujeito_corte: null,
    caucao: null,
    nivel_sensibilidade_id: null
  }

  private tipostipologia = {
    id: null,
    descricao: null,
    slug: null,
    tipologia_id: null,
    juro_mora: null,
    sujeito_corte: null,
    caucao: null,
    nivel_sensibilidade_id: null
  }


  private items: any = [];
  private NivelSensibilidade: any = [];
  private TiposbyTipologias: any = [];
  private has_caucao: boolean = false;
  private has_juro: boolean = false;
  private has_corte: boolean = false;

  constructor(private http: HttpService, private configService: ConfigService) { }

  ngOnInit() {
    this.getPageFilterData(1);
    this.listarNivelSensibilidade();
  }


  private listatipologiacliente() {

    this.configService.loaddinStarter('start');

    this.http.__call('tipologia-cliente/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.listatipologiacliente();
  }


  private register(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();

    if (this.tipologiacliente.descricao == "") {
      this.configService.showAlert("O campo Descrição é obrigatório", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
      this.http.__call('tipologia-cliente/create', this.tipologiacliente).subscribe(
        res => {
          if (Object(res).code == 201) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
            this.configService.loaddinStarter('stop');
          } else {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.clearFormInputs(e);
            this.listatipologiacliente()
            this.configService.loaddinStarter('stop');
          }
        }
      )
    };
  }


  private clearFormInputs(e) {
    e.target.elements[0].value = null;
    e.target.elements[1].value = null;
    e.target.elements[2].value = null;
    e.target.elements[3].value = null;
    e.target.elements[4].value = null;
    e.target.elements[5].value = null;
    e.target.elements[6].value = null;
  }

  private refresh(id, descricao, slug, caucao, sujeito_corte, juro_mora, nivel_sensibilidade_id) {
    this.tipologiacliente.id = id;
    this.tipologiacliente.descricao = descricao;
    this.tipologiacliente.slug = slug;
    this.tipologiacliente.caucao = caucao;
    this.tipologiacliente.sujeito_corte = sujeito_corte;
    this.tipologiacliente.juro_mora = juro_mora;
    this.tipologiacliente.nivel_sensibilidade_id = nivel_sensibilidade_id;
  }

  private editar(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (this.tipologiacliente.descricao == "") {
      this.configService.showAlert("O campo Descrição é obrigatório", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
      if (this.tipologiacliente.sujeito_corte == null) {
        this.configService.showAlert("O campo Sujeito a Corte é obrigatório", 'alert-danger', true);
        this.configService.loaddinStarter('stop');
      } else {
        this.http.__call('tipologia-cliente/update/' + this.tipologiacliente.id, this.tipologiacliente).subscribe(
          res => {
            if (Object(res).code == 201) {
              this.configService.showAlert(Object(res).message, 'alert-danger', true);
            } else {
              this.configService.clearFormInputs(e);
              this.configService.showAlert(Object(res).message, 'alert-success', true);
              this.listatipologiacliente();

            }
          }
        );
      }
      this.configService.loaddinStarter('stop');
    }
  }

  private cadastrar(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (this.tipostipologia.descricao == "") {
      this.configService.showAlert("Os campos Descrição e Slug são obrigatórios", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
      this.http.__call('tipologia-cliente/associartipostipologia/' + this.tipologiacliente.id, this.tipostipologia).subscribe(
        res => {
          if (Object(res).code == 201) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
            this.configService.loaddinStarter('stop');
          } else {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.clearFormInputs(e);
            this.getTiposbyTipologia()
            this.configService.loaddinStarter('stop');
          }
        }
      )
    };
  }

  private listarNivelSensibilidade() {
    this.http.call_get('nivel-sensibilidade/selectBox', null).subscribe(
      response => {
        this.NivelSensibilidade = Object(response);
      }
    );
  }

  private setCaucao(): void {
    this.has_caucao = !this.has_caucao;
  }
  private setCorte(): void {
    this.has_corte = !this.has_corte;
  }
  private setJuro(): void {
    this.has_juro = !this.has_juro;
  }

  onReset() {
    this.has_caucao = null;
    this.has_juro = null;
    this.has_corte = null;
    this.tipologiacliente.descricao = null;
    this.tipostipologia.tipologia_id = null;
  }

/*   private ValorByProduto() {

    this.http.call_get('selectBox/artigo-valorByProduto/' + this.tipologiacliente.id, null).subscribe(
      data => {
        this.tipostipologia.juro_mora = Object(data).data.juro_mora;
        this.tipostipologia.sujeito_corte = Object(data).data.sujeito_corte;
        this.tipostipologia.caucao = Object(data).data.caucao;
      }
    );
  } */

  private getTiposbyTipologia() {
    this.http.call_get('tipologia-cliente/update/getTipos/' + this.tipologiacliente.id, null).subscribe(
      response => {
        this.TiposbyTipologias = Object(response).data;
      }
    );
  }

  private ini() {
    this.tipologiacliente = {
      id: null,
      descricao: null,
      slug: null,
      juro_mora: null,
      sujeito_corte: null,
      caucao: null,
      nivel_sensibilidade_id: null
    }
  }





}
