import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfEstadoContaComponent } from './conf-estado-conta.component';

describe('ConfEstadoContaComponent', () => {
  let component: ConfEstadoContaComponent;
  let fixture: ComponentFixture<ConfEstadoContaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfEstadoContaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfEstadoContaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
