import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/providers/config/config.service';
import { HttpService } from 'src/app/providers/http/http.service';

@Component({
  selector: 'app-objecto-ligacao-ramal',
  templateUrl: './objecto-ligacao-ramal.component.html',
  styleUrls: ['./objecto-ligacao-ramal.component.css']
})
export class ObjectoLigacaoRamalComponent implements OnInit {
  private objecto_ligacao = {
    id: null,
    descricao: null,
    latitude: null,
    longitude: null,
    tipo_objecto_id: null,
    tipo_objecto_tecnico: null,
  }

  private items: any = [];
  private tipo_objecto: any = [];

  constructor(private http: HttpService, private configService: ConfigService) { }

  ngOnInit() {
    this.getPageFilterData(1);
    this.getTiposRamal();
  }

  private getTiposRamal() {
    this.http.call_get('tipo-objecto-tecnico/selectBox', null).subscribe(
      response => {
        this.tipo_objecto = Object(response).data;
      }
    );
  }


  private ListarObjectoLigacao() {

    this.configService.loaddinStarter('start');

    this.http.__call('objecto-ligacao-ramal/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.ListarObjectoLigacao();
  }


  private register(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (this.objecto_ligacao.descricao == "") {
      this.configService.showAlert("O campo Material é obrigatório", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    }
    else {
      this.http.__call('objecto-ligacao-ramal/create', this.objecto_ligacao).subscribe(
        res => {
          if (Object(res).code == 500) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
            this.configService.loaddinStarter('stop');
          }
          else {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.clearFormInputs(e);
            this.ListarObjectoLigacao()
            this.configService.loaddinStarter('stop');
          }
        }
      )
    };
  }


  private clearFormInputs(e) {
    e.target.elements[0].value = null;
    e.target.elements[1].value = null;
    e.target.elements[2].value = null;
  }

  private refresh(id, descricao, latitude, longitude, tipo_objecto_id) {
    this.objecto_ligacao.id = id;
    this.objecto_ligacao.descricao = descricao;
    this.objecto_ligacao.latitude = latitude;
    this.objecto_ligacao.longitude = longitude;
    this.objecto_ligacao.tipo_objecto_id = tipo_objecto_id;
  }

  private editar(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (this.objecto_ligacao.descricao == "" || this.objecto_ligacao.descricao == null) {
      this.configService.showAlert("O campo Material é obrigatórios", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    }
    else {
      this.http.__call('objecto-ligacao-ramal/update/' + this.objecto_ligacao.id, this.objecto_ligacao).subscribe(
        res => {
          if (Object(res).code == 500) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
          }
          else {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.ListarObjectoLigacao();

          }
        }
      );
    }
    this.configService.loaddinStarter('stop');
  }


  private ini() {
    this.objecto_ligacao = {
      id: null,
      descricao: null,
      latitude: null,
      longitude: null,
      tipo_objecto_id: null,
      tipo_objecto_tecnico: null
    }
  }
}

