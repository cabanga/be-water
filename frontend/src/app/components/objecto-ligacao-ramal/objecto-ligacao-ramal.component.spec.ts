import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObjectoLigacaoRamalComponent } from './objecto-ligacao-ramal.component';

describe('ObjectoLigacaoRamalComponent', () => {
  let component: ObjectoLigacaoRamalComponent;
  let fixture: ComponentFixture<ObjectoLigacaoRamalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObjectoLigacaoRamalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObjectoLigacaoRamalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
