import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoMedicaoComponent } from './conf-tipo-medicao.component';

describe('ConfTipoMedicaoComponent', () => {
  let component: ConfTipoMedicaoComponent;
  let fixture: ComponentFixture<ConfTipoMedicaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoMedicaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoMedicaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
