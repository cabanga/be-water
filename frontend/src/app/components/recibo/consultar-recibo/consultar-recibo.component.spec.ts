import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultarReciboComponent } from './consultar-recibo.component';

describe('ConsultarReciboComponent', () => {
  let component: ConsultarReciboComponent;
  let fixture: ComponentFixture<ConsultarReciboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarReciboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarReciboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
