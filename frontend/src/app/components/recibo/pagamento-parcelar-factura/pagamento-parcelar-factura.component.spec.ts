import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagamentoParcelarFacturaComponent } from './pagamento-parcelar-factura.component';

describe('PagamentoParcelarFacturaComponent', () => {
  let component: PagamentoParcelarFacturaComponent;
  let fixture: ComponentFixture<PagamentoParcelarFacturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagamentoParcelarFacturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagamentoParcelarFacturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
