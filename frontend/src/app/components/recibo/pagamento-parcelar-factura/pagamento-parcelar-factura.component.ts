 


import { Component, OnInit, Input,SimpleChange,EventEmitter ,OnChanges, Output} from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'; 
import { ReciboService } from 'src/app/components/reports/recibo/recibo.service' 
import * as moment from 'moment';
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'
import 'sweetalert2/src/sweetalert2.scss'

@Component({
  selector: 'app-pagamento-parcelar-factura',
  templateUrl: './pagamento-parcelar-factura.component.html',
  styleUrls: ['./pagamento-parcelar-factura.component.css']
})

export class PagamentoParcelarFacturaComponent implements OnInit, OnChanges {
 
  @Output() private loadList = new EventEmitter<any>();
  @Input() factura: any = null;
  @Input() loadingDataFactura = false;
  loading = false;
  private search: string;

  submitted = false;
  disable = false;

  

  private recibo: any;


  gerarReciboParcialForm: FormGroup;

  private carregando = {
    serie: 'Selecione a serie',
    forma_pagamento: 'Selecione a forma de pagamento',
    banco: 'Selecione conta da bancaria'
  }

  constructor(private http: HttpService, private configService: ConfigService, private formBuilder: FormBuilder, private PDFrecibo: ReciboService) {

    this.listarFormaPagamentos();
    this.configService.listarBancos();
    this.createForm();

  }


  ngOnInit() {
    this.setDefault()
  }

  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
    if (!this.loadingDataFactura) {   
      this.getSeriesRecibos();
      this.listarFormaPagamentos(); 
    }
  }

  createForm() {
    this.gerarReciboParcialForm = this.formBuilder.group({
      nome: [''],
      cliente_id: [null, Validators.required],
      serie_id: [null, Validators.required],

      forma_pagamento_id: [{ value: null}],
      banco_id: [{ value: null, disabled: true }],
      referencia: [{ value: null, disabled: true }],
      data_pagamento: [{ value: null, disabled: true }],

      valor_recebido: [''],
      total_valor_recebido: ['', Validators.required],
      troco: [{ disabled: true }],
      total_pago: [{ disabled: true }],
      total_saldado: [{ disabled: true }],
      facturas: [{ value: null }],
      linha_pagamentos: [{ value: null }],
    });
   
    this.facturasSeleciondas = []
    this.linha_pagamentos = []
    this.view_facturas = false
    this.configService.loaddinStarter('stop');
    this.loading = false;
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.gerarReciboParcialForm.controls;
  }

  onReset() {

    this.submitted = false;
    //this.gerarReciboParcialForm.reset();
    this.gerarReciboParcialForm.get('serie_id').reset();
    this.gerarReciboParcialForm.get('forma_pagamento_id').reset();
    this.gerarReciboParcialForm.get('valor_recebido').reset();
    this.gerarReciboParcialForm.get('data_pagamento').reset();
    this.gerarReciboParcialForm.get('referencia').reset();
    this.gerarReciboParcialForm.get('banco_id').reset();

    this.gerarReciboParcialForm.get('troco').reset();
    this.gerarReciboParcialForm.get('total_valor_recebido').reset();
    this.gerarReciboParcialForm.get('total_saldado').reset(); 
  
    this.linha_pagamentos = [] 
    this.facturasSeleciondas = []
    this.view_facturas = false
    this.configService.loaddinStarter('stop');
    this.loading = false;

  }


  onSubmit() {

    this.submitted = true;
    console.log(this.gerarReciboParcialForm.value);
    // parar aquei se o gerarReciboParcialFormulário for inválido
    if (this.gerarReciboParcialForm.invalid) {
      return;
    }

    if (this.facturasSeleciondas.length == 0) {
      this.configService.showAlert('Nenhuma factura selecionada', "alert-danger", true);
      return;
    } else if (this.linha_pagamentos.length == 0) {
      this.configService.showAlert('Nenhuma forma de pagamento selecionada', "alert-danger", true);
      return;
    }

    this.loading = true
    this.gerarReciboParcialForm.patchValue({
      facturas: this.facturasSeleciondas, 
      linha_pagamentos: this.linha_pagamentos, 
      /* serie_id: this.default */
    });
    //this.configService.loaddinStarter('start');
    const uri = 'recibo/gerarRecibo';
    this.createOrEdit(uri, this.gerarReciboParcialForm, true);

  }


  createOrEdit(uri: any, gerarReciboParcialFormulario: FormGroup, isCreate: boolean) {
    var nome: string;
    // TODO: usado para fazer a requisição com a api de criação de organismo
    this.http.__call(uri, gerarReciboParcialFormulario.value).subscribe(
      res => {

        if (Object(res).code === 200) {
          this.submitted = false;
          if (isCreate) {
            this.printer_recibo = false;
            nome = gerarReciboParcialFormulario.value.nome
            this.recibo = Object(res).data.recibo.id;
            this.gerarReciboParcialForm.patchValue({
              cliente_id: Object(res).data.recibo.cliente_id,
              nome: nome
            }); 
            this.closeModal();
            this.findAllLoadList();
            this.sucessoRecibo(Object(res).data); 
            this.onReset();
          }

        } else {
          this.errorReferenciasRecibo(Object(res).data)
        }
        this.loading = false
        this.configService.showAlert(Object(res).message, 'alert-success', true);
      }
    );
    this.configService.loaddinStarter('stop');
  }


  private activarAdiantamento = false
  /**
   *
   */
  handleChangeFormaPagamento() { 
    this.gerarReciboParcialForm.patchValue({
      valor_recebido: '',
      troco: 0, 
    }); 
    this.activarAdiantamento = false
    this.gerarReciboParcialForm.get("valor_recebido").enable();
    if (this.gerarReciboParcialForm.getRawValue().forma_pagamento_id != null  && !this.verificaForma(this.gerarReciboParcialForm.getRawValue().forma_pagamento_id)) {

      this.gerarReciboParcialForm.get('data_pagamento').enable();
      this.gerarReciboParcialForm.get('referencia').enable();
      this.gerarReciboParcialForm.get('banco_id').enable();
    } else {

      this.gerarReciboParcialForm.get('data_pagamento').reset();
      this.gerarReciboParcialForm.get('data_pagamento').disable();
      this.gerarReciboParcialForm.get('referencia').reset();
      this.gerarReciboParcialForm.get('referencia').disable();
      this.gerarReciboParcialForm.get('banco_id').reset();
      this.gerarReciboParcialForm.get('banco_id').disable();
    }
 
  }


  printer_recibo: boolean = true;
  private series: any = []
  private default: any = []

  private getSeriesRecibos() {
    this.carregando.serie = 'Carregando...'
    this.http.call_get('serie/recibos', null).subscribe(
      response => {
        let data = Object(response).data
        this.series = data;
        this.default = this.series[0].id  
        this.carregando.serie = 'Selecione a serie'
      });
  }

   private setDefault() {
    this.gerarReciboParcialForm.patchValue({
      serie_id: this.default
    })
  }

  private formasPagamentos: any = [];

  private listarFormaPagamentos() {
    this.carregando.forma_pagamento = 'Carregando...'
    this.http.call_get('formaPagamento/formas', null).subscribe(
      response => {
        this.formasPagamentos = Object(response).data;
        this.configService.loaddinStarter('stop'); 
        this.carregando.forma_pagamento = 'Selecione a forma de pagamento'
      }
    );
  }
  

  view_client = false;

 


  view_facturas = false; 

  private totalApagar() {
    var ve = this.gerarReciboParcialForm.getRawValue().total_valor_recebido;
    this.printer_recibo = true;
    var total = 0;
    var total_saldado = 0
    this.gerarReciboParcialForm.patchValue({
      total_pago: null,
      total_saldado: null
    });

    this.facturasSeleciondas.forEach(element => {
      total += element.valor_aberto;
      var factura = element;
      factura.valor_aberto = (factura.valor_aberto == null && factura.pago==0 ? factura.total : factura.valor_aberto);// Atribui o valor total caso o valor em aberto for null
      var va = factura.valor_aberto; // valor actual em aberto da factura
      var saldado = 0  // recebe o valor saldado na factura
      if (ve > 0) {

        ve = ve - va;
        saldado = (ve < 0 ? ve - va * -1 : va) // calcula o valor saldado
        total_saldado += saldado
      }
    });
    this.gerarReciboParcialForm.patchValue({
      total_pago: total,
      total_saldado: total_saldado
    });
  }

  private calcularTroco() {
    this.gerarReciboParcialForm.patchValue({
      troco: 0
    });


    if (this.gerarReciboParcialForm.getRawValue().total_pago > 0) {
      this.gerarReciboParcialForm.patchValue({
        troco: this.gerarReciboParcialForm.getRawValue().total_valor_recebido - this.gerarReciboParcialForm.getRawValue().total_pago
      });
      if (this.gerarReciboParcialForm.getRawValue().total_valor_recebido <= this.gerarReciboParcialForm.getRawValue().total_pago) {
        this.gerarReciboParcialForm.patchValue({
          troco: 0
        });
      }
    } else {
      this.gerarReciboParcialForm.patchValue({
        troco: 0
      });
    }
  }


  private btnImprimirRecibo() {
    this.PDFrecibo.imprimirRecibo(this.recibo, 'Original');
  }

  private facturasSeleciondas = []

  private selecionarFactura() {
    this.gerarReciboParcialForm.patchValue({
      cliente_id: this.factura.cliente_id
    });
    const factura = this.factura;
    if (this.facturasSeleciondas.length == 0) {
      factura.valor_aberto = (factura.valor_aberto == null && factura.pago==0? factura.total : factura.valor_aberto);
      this.facturasSeleciondas.push(factura);
    } else {
      var validar = 0
      for (let i = 0; i < this.facturasSeleciondas.length; ++i) {
        if (this.facturasSeleciondas[i].id === factura.id) {
          this.facturasSeleciondas.splice(i, 1);
          validar = 1;
        }
      }
      if (validar === 0) {
        factura.valor_aberto = (factura.valor_aberto == null && factura.pago==0? factura.total : factura.valor_aberto);
        this.facturasSeleciondas.push(factura);
      }
    }

    //Calcula o Total da Factura
    this.totalApagar();
    this.calcularTroco()
  }
  
  sucessoRecibo(response: any) {
    var facts = ''
    response.facts.forEach(element => {
      facts += '<tr>'
        + '<td>' + element.factura_sigla + '</td>'
        + '<td>' + this.configService.numberFormat(element.saldado) + '</td>'
        + '<td>' + this.configService.numberFormat(element.va_ant) + '</td>'
        + '<td>' + this.configService.numberFormat(element.va_new) + '</td>'
        + '</tr>'
    });
    var html = '<div style="text-align: left;margin: 13px 20px;width: 700px;padding: 9px 23px;background: #f8f9fa;border-radius: 5px;">' 
      + '<h4>Estatística</h4><hr/>'
      + '<table class="table table-hover table-bordered table-striped text-center">'
      + '<thead>'
      + '<tr>'
      + '<th> Factura </th>'
      + '<th> V.Saldado </th>'
      + '<th> V.Aberto Anterior </th>'
      + '<th> V.Aberto Novo </th>'
      + '</tr>'
      + '</thead>'
      + '<tbody>'
      + facts +
      + ' </tbody>'
      + '</table>'
      + '<b>Factura Saldada:</b> ' + (response.contTotalFacturaSaldadas) + '<br>'
      + '<b>Factura em aberto: </b>' + (response.contTotalFacturaAbertas) + "<br>"
      + '</div>'

    const swalWithBootstrapButtons = Swal.mixin({

      customClass: {
        container: 'container-class',
        popup: 'popup-class',
        header: 'header-class',
        title: 'title-class',
        closeButton: 'close-button-class',
        icon: 'icon-class',
        content: 'content-class',
        input: 'input-class',
        actions: 'actions-class',
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-default',
        footer: 'footer-class'
      },
      width: '49em',
      buttonsStyling: false,
    })

    swalWithBootstrapButtons.fire({

      html: html,
      title: 'Recibo Gerado',
      text: "Você não poderá reverter isso!",
      type: 'success',
      showCancelButton: true,
      confirmButtonText: 'Sair',
      cancelButtonText: 'Imprimir',
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        //window.location.reload()
      } else if (
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Aguarde',
          'Aguarde, estamos a gerar a impressão do recibo',
          'success'
        )
        this.btnImprimirRecibo();
        //window.location.reload()
      }
    })

  }

  errorReferenciasRecibo(response: any) {
    var facts = ''
    response.facts.forEach(element => {
      facts += '<tr>'
                + '<td>' + element.banco_nome + '</td>'
                + '<td>' + element.referencia_banco + '</td>'
            + '</tr>'
    });
    var html = '<div style="text-align: left;margin: 13px 20px;width: 700px;padding: 9px 23px;background: #f8f9fa;border-radius: 5px;">'
      + '<h4>As Referencias Bancarias listadas abaixo já foram utilizadas</h4><hr/>'
      + '<table class="table table-hover table-bordered table-striped text-center">'
      + '<thead>'
      + '<tr>'
      + '<th> Banco </th>'
      + '<th> Referencia </th>'
      + '</tr>'
      + '</thead>'
      + '<tbody>'
      + facts +
      + ' </tbody>'
      + '</table>'
      + '</div>'

    const swalWithBootstrapButtons = Swal.mixin({

      customClass: {
        container: 'container-class',
        popup: 'popup-class',
        header: 'header-class',
        title: 'title-class',
        closeButton: 'close-button-class',
        icon: 'icon-class',
        content: 'content-class',
        input: 'input-class',
        actions: 'actions-class',
        confirmButton: 'btn btn-success',
        footer: 'footer-class'
      },
      width: '49em',
      buttonsStyling: false,
    })

    swalWithBootstrapButtons.fire({

      html: html,
      title: 'Erro Referencias Bancarias',
      text: "Você não poderá reverter isso!",
      type: 'error',
      showCancelButton: false,
      confirmButtonText: 'Continuar',
      reverseButtons: true,
    }).then((result) => {
      if (result.value) {
        //window.location.reload()
      }
    })

  }


  //--------------------------------------------------------------
  private linha_pagamentos: any = []

  deleteRowFormaPagamento(linha: any) {
    for (let i = 0; i < this.linha_pagamentos.length; ++i) {
      if (this.linha_pagamentos[i].id === linha.id) { 
        this.linha_pagamentos.splice(i, 1);
        //this.facturacao.pagamento.total_valor_recebido -= linha.valor_entrada;
        this.calcularValorTotalRecebido();
        this.totalApagar()
        this.calcularTroco() 
      }
    }

  }

  private addLinhaPagamentos() {
    var validar = 0;
    var forma = null
    var banco_nome = null

    this.configService.bancos.forEach(element => {
      if (this.gerarReciboParcialForm.getRawValue().banco_id == element.id) {
        banco_nome = element.abreviatura + ' - ' + element.numero_conta;
      }
    });

    this.formasPagamentos.forEach(element => {
      if (this.gerarReciboParcialForm.getRawValue().forma_pagamento_id == element.id) {
        forma = element.designacao;
      }
    });

    if (forma == null) {
      this.configService.showAlert('Informa uma forma pagamento valida', "alert-danger", true);
      return;
    } else if (this.gerarReciboParcialForm.getRawValue().valor_recebido == '' || this.gerarReciboParcialForm.getRawValue().valor_recebido < 0) {
      this.configService.showAlert('Informa Valor de Entrada valido', "alert-danger", true);
      return;
    } else {
      if ((this.gerarReciboParcialForm.getRawValue().referencia == null || this.gerarReciboParcialForm.getRawValue().data_pagamento == null || this.gerarReciboParcialForm.getRawValue().banco_id == null) && !this.verificaForma(this.gerarReciboParcialForm.getRawValue().forma_pagamento_id)) {
        if (forma !== "Adiantamento") {
          this.configService.showAlert('Informa Nº conta da bancaria, referencia a bancaria e a data de pagamento', "alert-danger", true);
          return;
        }

      }

      if ((this.gerarReciboParcialForm.getRawValue().referencia != null || this.gerarReciboParcialForm.getRawValue().referencia != "") && !this.verificaForma(this.gerarReciboParcialForm.getRawValue().forma_pagamento_id)) {

        var code = 0

        this.linha_pagamentos.forEach(element => {
          if (this.gerarReciboParcialForm.getRawValue().referencia === element.referencia_banco && this.gerarReciboParcialForm.getRawValue().banco_id === element.banco_id) {
            code++;
          }
        });


        this.validationReferenciaBancaria(this.gerarReciboParcialForm.getRawValue().referencia, this.gerarReciboParcialForm.getRawValue().banco_id);
        if (this.codeReferencia == 0 || code!=0 && (this.gerarReciboParcialForm.getRawValue().referencia != null || this.gerarReciboParcialForm.getRawValue().banco_id != null)) {
          this.configService.showAlert("A refêrencia bancaria já foi utilizada", "alert-danger", true);
          return;
        }


      }

      var linha = {
        designacao: forma,
        valor_entrada: this.gerarReciboParcialForm.getRawValue().valor_recebido,
        data_pagament: this.gerarReciboParcialForm.getRawValue().data_pagamento,
        id: this.gerarReciboParcialForm.getRawValue().forma_pagamento_id,
        referencia_banco: this.gerarReciboParcialForm.getRawValue().referencia,
        banco_id: this.gerarReciboParcialForm.getRawValue().banco_id,
        banco_nome: banco_nome
      }

      if (this.linha_pagamentos.length >= 1) {
        for (let index = 0; index < this.linha_pagamentos.length; index++) {
          const l = this.linha_pagamentos[index];
          if (l.id == linha.id) {
            this.linha_pagamentos.splice(index, 1);
            this.linha_pagamentos.splice(index, 0, linha);
            validar = 1;
          }
        }
        if (validar == 0) {
          this.linha_pagamentos.push(linha);
        }
      } else {
        this.linha_pagamentos.push(linha);
      }

      this.calcularValorTotalRecebido();
      this.totalApagar()
      this.calcularTroco();


      this.gerarReciboParcialForm.get('forma_pagamento_id').reset();
      this.gerarReciboParcialForm.get('valor_recebido').reset();
      this.gerarReciboParcialForm.get('data_pagamento').reset();
      this.gerarReciboParcialForm.get('referencia').reset();
      this.gerarReciboParcialForm.get('banco_id').reset();

      this.gerarReciboParcialForm.get('referencia').disable();
      this.gerarReciboParcialForm.get('data_pagamento').disable();
      this.gerarReciboParcialForm.get('banco_id').disable();

    }
  }

  private calcularValorTotalRecebido() {
    var total_valor_recebido = 0
    this.linha_pagamentos.forEach(element => {
      total_valor_recebido += ((element.valor_entrada));
    });
    this.gerarReciboParcialForm.patchValue({
      total_valor_recebido: total_valor_recebido
    });
  }


  private codeReferencia: any;
  private validationReferenciaBancaria(ref, bank) {
    this.http.__call('referencia/validation', { referencia: ref, banco_id: bank }).subscribe(
      response => {
        this.codeReferencia = Object(response);
      }
    );
  }

  private verificaForma(item) { 
    if(item==""){
      return true
    }
    var chaves = [];
    this.formasPagamentos.forEach(element => {
        if(element.usar_banco === 0){
          chaves.push(""+element.id);
        }
    })
    if(chaves.length > 0){
      if (chaves.indexOf(item) > -1) {
        return true
      } else {
          return false
      }
    }else{
      return false
    }
  }

  
  closeModal(closeModal="closeModal"){
    var action = document.getElementById(closeModal)
    action.click()
    this.loadingDataFactura = true;
    this.onReset();
  }

  public findAllLoadList() {
    this.loadList.emit(null);
  }

}

