import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoObjectoComponent } from './tipo-objecto.component';

describe('TipoObjectoComponent', () => {
  let component: TipoObjectoComponent;
  let fixture: ComponentFixture<TipoObjectoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoObjectoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoObjectoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
