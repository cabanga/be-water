import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfEstadoServicoComponent } from './conf-estado-servico.component';

describe('ConfEstadoServicoComponent', () => {
  let component: ConfEstadoServicoComponent;
  let fixture: ComponentFixture<ConfEstadoServicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfEstadoServicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfEstadoServicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
