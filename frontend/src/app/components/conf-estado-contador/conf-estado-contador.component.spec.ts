import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfEstadoContadorComponent } from './conf-estado-contador.component';

describe('ConfEstadoContadorComponent', () => {
  let component: ConfEstadoContadorComponent;
  let fixture: ComponentFixture<ConfEstadoContadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfEstadoContadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfEstadoContadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
