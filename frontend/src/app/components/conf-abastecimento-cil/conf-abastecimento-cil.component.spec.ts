import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfAbastecimentoCilComponent } from './conf-abastecimento-cil.component';

describe('ConfAbastecimentoCilComponent', () => {
  let component: ConfAbastecimentoCilComponent;
  let fixture: ComponentFixture<ConfAbastecimentoCilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfAbastecimentoCilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfAbastecimentoCilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
