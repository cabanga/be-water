import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoRamalComponent } from './conf-tipo-ramal.component';

describe('ConfTipoRamalComponent', () => {
  let component: ConfTipoRamalComponent;
  let fixture: ComponentFixture<ConfTipoRamalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoRamalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoRamalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
