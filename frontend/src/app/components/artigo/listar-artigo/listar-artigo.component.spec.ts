import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarArtigoComponent } from './listar-artigo.component';

describe('ListarArtigoComponent', () => {
  let component: ListarArtigoComponent;
  let fixture: ComponentFixture<ListarArtigoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarArtigoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarArtigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
