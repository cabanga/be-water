import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CobreFormComponent } from './cobre-form.component';

describe('CobreFormComponent', () => {
  let component: CobreFormComponent;
  let fixture: ComponentFixture<CobreFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CobreFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CobreFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
