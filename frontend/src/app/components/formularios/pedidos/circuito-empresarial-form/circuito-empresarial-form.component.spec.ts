import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CircuitoEmpresarialFormComponent } from './circuito-empresarial-form.component';

describe('CircuitoEmpresarialFormComponent', () => {
  let component: CircuitoEmpresarialFormComponent;
  let fixture: ComponentFixture<CircuitoEmpresarialFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CircuitoEmpresarialFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CircuitoEmpresarialFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
