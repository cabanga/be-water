import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidoCircuitoEmpresarialComponent } from './pedido-circuito-empresarial.component';

describe('PedidoCircuitoEmpresarialComponent', () => {
  let component: PedidoCircuitoEmpresarialComponent;
  let fixture: ComponentFixture<PedidoCircuitoEmpresarialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidoCircuitoEmpresarialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidoCircuitoEmpresarialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
