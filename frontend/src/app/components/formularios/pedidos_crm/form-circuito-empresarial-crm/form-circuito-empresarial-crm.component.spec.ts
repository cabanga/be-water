import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormCircuitoEmpresarialCRMComponent } from './form-circuito-empresarial-crm.component';

describe('FormCircuitoEmpresarialCRMComponent', () => {
  let component: FormCircuitoEmpresarialCRMComponent;
  let fixture: ComponentFixture<FormCircuitoEmpresarialCRMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormCircuitoEmpresarialCRMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCircuitoEmpresarialCRMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
