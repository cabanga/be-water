import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeralFormComponent } from './geral-form.component';

describe('GeralFormComponent', () => {
  let component: GeralFormComponent;
  let fixture: ComponentFixture<GeralFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeralFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeralFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
