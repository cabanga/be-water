
//import { ExcelService } from 'src/app/services/excel.service'; 
import { ChargeFacturacaoService } from 'src/app/services/ExportExcel/charge-facturacao.service';
import * as moment from 'moment';
import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

import { of, timer, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged,tap, switchMap, finalize} from 'rxjs/operators';

@Component({
  selector: 'app-facturacao-charge',
  templateUrl: './facturacao-charge.component.html',
  styleUrls: ['./facturacao-charge.component.css']
})
export class FacturacaoChargeComponent implements OnInit {

  submitted = false;
  loading = false;
  loadingCharge = false;
  direccoes:any = [];
  gestores:any = [];
  servicos:any = [];
  anos:any = []; 
  private contas: any = [];
  loadingServico = false;
  clientes: any= [];
  flatrates: any=[]
  view_client = false;
  
  private charges: any;

  private loadingData = {
    conta: "Conta",
    servico: "Serviço",
    produto: "Produto",
    estado: "Estado",
    flatrate:"flatrate"
  }

  private filter = {  
    servico:null,
    gestor: null, 
    direccao: null, 
    mes:null,
    ano:null,
    chaveServico:null,
    is_facturado:null
  }
  private meses = [
    { nome: "Janeiro", numero: "01" },
    { nome: "Fevereiro", numero: "02" },
    { nome: "Março", numero: "03" },
    { nome: "Abril", numero: "04" },
    { nome: "Maio", numero: "05" },
    { nome: "Junho", numero: "06" },
    { nome: "Julho", numero: "07" },
    { nome: "Agosto", numero: "08" },
    { nome: "Setembro", numero: "09" },
    { nome: "Outubro", numero: "10" },
    { nome: "Novembro", numero: "11" },
    { nome: "Dezembro", numero: "12" }
  ];

  private charge = {
    nome:null,
    charge_id: null,
    valor_new: null,
    valor_old: null,
    observacao: null,
    chaveServico:null,
    invoiceText:null,
    conta_id:null,
    cliente_id:null,
    servico_id:null,
    flatrate_id:null,
    capacidade:null,
    origem_destino:null,
    ano:null,
    mes:null
  }

  private subjectListarCharge: Subject<number> = new Subject();

  constructor(private http: HttpService, private configService: ConfigService,private excelService: ChargeFacturacaoService,) {
     
  }



  ngOnInit() { 
    this.listarCharge(); 
    this.gerarAno()

    
  } 

  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "Lista_charge_facturação-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m') 
    this.excelService.ExcelExportFacturacaoCharge(this.charges,'',nameFile);
  }

  /**
   * @name "Listar Charges"
   * @descriptio "Esta Função permite Listar todas Charges a serem facturadas"
   * @author "caniggia.moreira@itgest.pt"
   * @param start
   * @param end
   */
  private listarCharge() { 

    this.http.filters.filter = this.filter; 
    this.subjectListarCharge.pipe(debounceTime(1000),distinctUntilChanged(),
        tap(() => {  this.loading = true;  }),            
        switchMap(param => this.http.__call('charge/listar', this.http.filters).pipe(finalize(() => { this.loading = false }))
      )).subscribe(response => {
            this.http.filters.pagination.lastPage = Object(response).data.lastPage;
            this.http.filters.pagination.page = Object(response).data.page;
            this.http.filters.pagination.total = Object(response).data.total;
            this.http.filters.pagination.perPage = Object(response).data.perPage;
            this.charges = Object(response).data.data;
      }); 
      this.getPageFilterData(this.http.filters.pagination.page)
    }

  

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page; 
    this.subjectListarCharge.next(page);
  }


 
  
  private setCharge(charge: any) {
    this.charge.nome = charge.cliente;
    this.charge.chaveServico = charge.chaveServico;
    this.charge.invoiceText = charge.invoiceText;
    this.charge.charge_id = charge.id;
    this.charge.valor_old = charge.valor;
  }
  private updateValorCharge() {

    if (this.charge.valor_new == null) {
      this.configService.showAlert('É obrigatório fornecer Valor Novo', "alert-danger", true);
      return;
    } else if (this.charge.valor_new < 0) {
      this.configService.showAlert('É obrigatório fornecer Valor Novo superior que zero', "alert-danger", true);
      return;
    } else if (this.charge.valor_new == this.charge.valor_old) {
      this.configService.showAlert('É obrigatório fornecer Valor Novo diferente do valor Actual', "alert-danger", true);
      return;
    }
    else if (this.charge.observacao == null) {
      this.configService.showAlert('É obrigatório Escrever uma Observação sobre a actualização do valor da change', "alert-danger", true);
      return;
    }
    this.loadingCharge = true
    this.http.__call('charge/update', this.charge).subscribe(
      response => {
        this.charge.valor_old = this.charge.valor_new;
        this.charge.valor_new = null
        this.listarCharge();
        this.loadingCharge = false
      }
    );
  }

  private listarDireccao() {
    this.configService.loaddinStarter('start');
    this.http.call_get('direccao/selectBox', null).subscribe(
      response => {
        this.direccoes = Object(response).data;
        this.configService.loaddinStarter('stop');
      }
    );
  }

  private getGestores() {
    this.http.call_get('gestor/selectBox', null).subscribe(
      response => {
        this.gestores = Object(response).data
      }
    );
  }

 
  private gerarAno() {
    var fecha = new Date();
    var anyo = fecha.getFullYear();

    let j = 0;
    for (let i = anyo; i >= 2000; i--) {
      this.anos[j] = i;
      j++;
    }
  }


  private numeroTelefone() {
    this.configService.loaddinStarter('start');
    this.http.call_get('servico/getServicoToChaveServico/'+this.filter.chaveServico, null).subscribe(
      res => { 
          Object(res).data;
          this.http.filters.search = Object(res).data.nome
          this.listarCharge();
      }
    );
  }


  private anularCharge() { 
     
    if (this.charge.observacao == null) {
      this.configService.showAlert('É obrigatório Escrever o motivo da anulação', "alert-danger", true);
      return;
    }
    this.loadingCharge = true
    this.http.__call('charge/anular', this.charge).subscribe(
      response => { 
        this.listarCharge();
        this.loadingCharge = false
      }
    );
  }


  private getContas(cliente: any) {
    this.contas = []
    this.charge.servico_id = null
    this.loadingData.conta = "Carregando..."
    this.http.call_get('cliente/conta/selectBox/' + this.charge.cliente_id, null).subscribe(
      response => {
        this.contas = Object(response).data
        this.loadingData.conta = "Conta"
      })
  }

  private getServicosConta() { 
    this.servicos = []
    this.charge.flatrate_id = null
    this.charge.invoiceText = null
    this.charge.capacidade = null
    this.charge.valor_new = null 
    this.charge.origem_destino = null
    this.loadingData.servico = "Carregando..."
    this.http.call_get('tarifario/servico/selectBoxServicosConta/' + this.charge.conta_id, null).subscribe(
      response => {
        this.servicos = Object(response).data 
        this.loadingData.servico = "Serviço"
      })

  }

  private getFlatrateServico() { 
    this.flatrates = [] 
    this.charge.flatrate_id = null
    this.charge.invoiceText = null
    this.charge.capacidade = null
    this.charge.valor_new = null 
    this.charge.origem_destino = null
    this.loadingData.flatrate = "Carregando..."
    this.http.call_get('servico/flate_rate/getAllflateRate/' + this.charge.servico_id, null).subscribe(
      response => {
        this.flatrates = Object(response).data 
        this.loadingData.flatrate = "flatrate"
      })

  }

  private getCliente() { 
    this.view_client = true;
    this.http.__call('cliente/search-cliente', { start: 1, end: 10, search: this.charge.nome }).subscribe(
      response => {
        this.clientes = Object(response).data.data; 
      }
    );
  }

  

  private setCliente(client: any) {
      this.view_client = false;    
      this.charge.nome= client.nome
      this.charge.cliente_id = client.id 
      this.charge.conta_id = null
      this.charge.servico_id = null
      this.charge.flatrate_id = null
      this.charge.invoiceText = null
      this.charge.capacidade = null
      this.charge.valor_new = null 
      this.charge.origem_destino = null
      this.getContas(client)
  }

  flatrate:any = null

  private setFlatrate(event) {
    //Equant Network Systems Ltd
    this.flatrate = null
    for (let index = 0; index < this.flatrates.length; index++) {
      const element = this.flatrates[index]; 
      if(this.charge.flatrate_id == element.id){ 
        this.charge.invoiceText = element.artigo 
        this.charge.capacidade = element.capacidade 
        this.charge.valor_new = element.valor 
        this.charge.origem_destino = element.origem+" - "+element.destino 
      }       
    } 
  }

  private registarChargeFlatRate() { 
     
    if (this.charge.ano ==null && this.charge.mes ==null && this.charge.conta_id == null && this.charge.servico_id == null && this.charge.cliente_id == null && this.charge.flatrate_id == null) {
      this.configService.showAlert('É obrigatório preencher os campos: periodo(mes e ano), cliente, conta, serviço e flatRate', "alert-danger", true);
      return;
    }
    this.loadingCharge = true
    this.http.__call('charge/registar', this.charge).subscribe(
      response => { 
        if(Object(response).code == 200){

          this.charge.nome = null
          this.charge.cliente_id = null
          this.charge.conta_id = null
          this.charge.servico_id = null
          this.charge.flatrate_id = null
          this.charge.invoiceText = null
          this.charge.capacidade = null
          this.charge.valor_new = null 
          this.charge.origem_destino = null
          this.charge.ano = null
          this.charge.mes = null

          this.listarCharge();
        }
        
        this.loadingCharge = false
      }
    );
  }

  reset(){
    this.charge = {
      nome:null,
      charge_id: null,
      valor_new: null,
      valor_old: null,
      observacao: null,
      chaveServico:null,
      invoiceText:null,
      conta_id:null,
      cliente_id:null,
      servico_id:null,
      flatrate_id:null,
      capacidade:null,
      origem_destino:null,
      ano:null,
      mes:null
    }
  }
}

