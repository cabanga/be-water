import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacturacaoChargeComponent } from './facturacao-charge.component';

describe('FacturacaoChargeComponent', () => {
  let component: FacturacaoChargeComponent;
  let fixture: ComponentFixture<FacturacaoChargeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacturacaoChargeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacturacaoChargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
