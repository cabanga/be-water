import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { ConfigModuloService } from 'src/app/services/config-modulo.service';
import { environment } from 'src/environments/environment';
import { HttpService } from 'src/app/providers/http/http.service';

@Component({
  selector: 'app-asidenavbar',
  templateUrl: './asidenavbar.component.html',
  styleUrls: ['./asidenavbar.component.css']
})
export class AsidenavbarComponent {

  @Input() app_environment: null;
  private quarteirao_view: boolean = false;
  private distrito_view: boolean = false;

  public currentUser: any;
  constructor(
    private auth: AuthService,
    private configService: ConfigService,
    private config: ConfigModuloService,
    private http: HttpService
  ) {
    this.currentUser = this.auth.currentUserValue;

    this.app_environment = this.auth.getAppEnvironment();

    if (this.app_environment == null) {

      var url = require('url');
      var app_url = url.parse(environment.app_url, true);

      this.app_environment = app_url.host;

    }

    this.getConfiguracaos();
    //console.log("this.app_environment " + this.app_environment);

  }

  canActivateRouterLink(permission: string): boolean {
    return this.auth.canActivateRouterLink(permission);
  }

  /* 
    ngOnInit() {
  
      //this.app_environment = this.auth.getAppEnvironment();
   
      if (this.app_environment == null) {
  
        var url = require('url');
        var app_url = url.parse(environment.app_url, true);
  
        this.app_environment = app_url.host;
  
        this.config.saveConfig(app_url.host, this.config.modulo.CONFIGURACOES, app_url.host);
      }
  
      console.log("this.app_environment " + this.app_environment);
     }
   */


  public getConfiguracaos() {

    let result = null;

    const slugs = [
      this.config.quarteirao_view,
      this.config.distrito_view
    ];
    //console.log(slugs);

    for (let index = 0; index < slugs.length; index++) {

      //console.log(slugs[index]);
      this.http.__call('configuracao/getConfiguracaobySlug/' + slugs[index], null).subscribe(
        response => {

/*           console.log(Object(response));
          console.log(slugs[index]);
 */
          if (Object(response).code != 200) {
            result = null;
          }
          else {

            result = Object(response).data;

            if (slugs[index] == this.config.quarteirao_view) this.quarteirao_view = Boolean(result.valor);

            if (slugs[index] == this.config.distrito_view) this.distrito_view = Boolean(result.valor);             
          }
        });
    }
  }


}
