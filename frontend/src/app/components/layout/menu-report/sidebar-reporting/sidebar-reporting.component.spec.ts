import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarReportingComponent } from './sidebar-reporting.component';

describe('SidebarReportingComponent', () => {
  let component: SidebarReportingComponent;
  let fixture: ComponentFixture<SidebarReportingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarReportingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
