import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/providers/auth/auth.service'; 
import { ConfigService } from 'src/app/providers/config/config.service';

@Component({
  selector: 'app-sidebar-reporting',
  templateUrl: './sidebar-reporting.component.html',
  styleUrls: ['./sidebar-reporting.component.css']
})
export class SidebarReportingComponent implements OnInit {

  @Input() data: any[];
  
  @Output() ChangedMenuEmitter = new EventEmitter();

  public currentUser: any; 
  constructor( private auth: AuthService, private configService: ConfigService) {
    this.currentUser = this.auth.currentUserValue;
  }
 
  canActivateRouterLink(permission: string): boolean {
    return this.auth.canActivateRouterLink(permission); 
  }

  ngOnInit() {
  }

  toggleMenuSecound(event: any, data: any[]): void {
    let currentAction = event.currentTarget;

    $(".report-nav").removeClass("active");
    $(".direct-chat-text").removeClass("active");
    
    $(currentAction).addClass("active");
    $(currentAction).children(":first").children(":first").addClass("active");
   
    this.ChangedMenuEmitter.emit(data);
  }

}
