import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetingsnavbarComponent } from './setingsnavbar.component';

describe('SetingsnavbarComponent', () => {
  let component: SetingsnavbarComponent;
  let fixture: ComponentFixture<SetingsnavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetingsnavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetingsnavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
