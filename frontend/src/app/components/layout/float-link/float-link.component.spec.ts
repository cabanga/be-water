import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FloatLinkComponent } from './float-link.component';

describe('FloatLinkComponent', () => {
  let component: FloatLinkComponent;
  let fixture: ComponentFixture<FloatLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FloatLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FloatLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
