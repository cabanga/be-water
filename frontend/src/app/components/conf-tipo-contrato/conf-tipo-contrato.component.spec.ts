import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoContratoComponent } from './conf-tipo-contrato.component';

describe('ConfTipoContratoComponent', () => {
  let component: ConfTipoContratoComponent;
  let fixture: ComponentFixture<ConfTipoContratoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoContratoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoContratoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
