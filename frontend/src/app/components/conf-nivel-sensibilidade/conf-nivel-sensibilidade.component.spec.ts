import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfNivelSensibilidadeComponent } from './conf-nivel-sensibilidade.component';

describe('ConfNivelSensibilidadeComponent', () => {
  let component: ConfNivelSensibilidadeComponent;
  let fixture: ComponentFixture<ConfNivelSensibilidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfNivelSensibilidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfNivelSensibilidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
