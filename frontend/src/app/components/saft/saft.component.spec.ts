import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaftComponent } from './saft.component';

describe('SaftComponent', () => {
  let component: SaftComponent;
  let fixture: ComponentFixture<SaftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
