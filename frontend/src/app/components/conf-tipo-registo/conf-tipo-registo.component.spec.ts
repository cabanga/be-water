import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoRegistoComponent } from './conf-tipo-registo.component';

describe('ConfTipoRegistoComponent', () => {
  let component: ConfTipoRegistoComponent;
  let fixture: ComponentFixture<ConfTipoRegistoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoRegistoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoRegistoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
