import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoOrdemServicoComponent } from './conf-tipo-ordem-servico.component';

describe('ConfTipoOrdemServicoComponent', () => {
  let component: ConfTipoOrdemServicoComponent;
  let fixture: ComponentFixture<ConfTipoOrdemServicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoOrdemServicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoOrdemServicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
