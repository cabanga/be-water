import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarRelatorioComponent } from './visualizar-relatorio.component';

describe('VisualizarRelatorioComponent', () => {
  let component: VisualizarRelatorioComponent;
  let fixture: ComponentFixture<VisualizarRelatorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizarRelatorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarRelatorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
