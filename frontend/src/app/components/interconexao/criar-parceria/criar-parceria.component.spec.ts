import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarParceriaComponent } from './criar-parceria.component';

describe('CriarParceriaComponent', () => {
  let component: CriarParceriaComponent;
  let fixture: ComponentFixture<CriarParceriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriarParceriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarParceriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
