import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarParceriaComponent } from './listar-parceria.component';

describe('ListarParceriaComponent', () => {
  let component: ListarParceriaComponent;
  let fixture: ComponentFixture<ListarParceriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarParceriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarParceriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
