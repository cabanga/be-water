import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfEstadoTarifarioComponent } from './conf-estado-tarifario.component';

describe('ConfEstadoTarifarioComponent', () => {
  let component: ConfEstadoTarifarioComponent;
  let fixture: ComponentFixture<ConfEstadoTarifarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfEstadoTarifarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfEstadoTarifarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
