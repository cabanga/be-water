import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'src/app/providers/config/config.service';
import { ContaCorrenteReportService } from 'src/app/components/conta-corrente/conta-corrente-report.service';
import { ExcelService } from 'src/app/services/excel.service';
import { ContaCorrenteService } from 'src/app/services/ExportExcel/conta-corrente.service';
import * as moment from 'moment';
import { Chart, ChartOptions } from 'chart.js';
import { resolve } from 'url';

@Component({
  selector: 'app-conta-corrente',
  templateUrl: './conta-corrente.component.html',
  styleUrls: ['./conta-corrente.component.css']
})

export class ContaCorrenteComponent implements OnInit {

  //public barChartPlugins = [ChartDataLabels]; 

  @ViewChild("serie") serie;
  @ViewChild("orderBy") orderBy;
  @ViewChild("observacao") observacao;
  @ViewChild("serieRecibo") serieRecibo;
  private totalFacturacao = 0;
  private totalSemImposto = 0;
  private totalComImposto = 0;
  private serieId: number;
  private serieReciboId: number;
  private series: any = [];
  private totalAdiantamento: any;

  private btnClickBoleon = false;
  CurrentDate = new Date();
  mes = moment(this.CurrentDate).format("MM")

  private factura = {
    factura_id: null,
    factura_sigla: null,
    status_reason: null,
    status_date: null,
    status: null,
    numero: null,
    pago: null,
    created_at: null,
    cliente_id: null,
    cliente_nome: null,
    serie: null,
    documento: null,
    sigla: null,
    total: null,
    totalComImposto: null,
    totalSemImposto: null,
  }
  private cliente = {
    id: null,
    nome: null,
    telefone: null,
    morada: null,
    contribuente: null
  }

  private pagination = {
    total: null,
    perPage: 20,
    page: 1,
    lastPage: null,

    start: 1,
    end: 10,
    search: "a",
    order: "created_at"
  };

  private conta_corrente = {
    cliente: {
      id: null,
      nome: null,
      telefone: null,
      morada: null,
      contribuente: null
    },
    search: ""

  }

  private selectBox = {
    pago: null
  } 
  private clientes: any = [];
  private contas: any = [];
 // private FacturasExportExcel: any = [];
  private facturas: any = [];
  private dadosFacturaId: any = [];
  private produtosNotaCredito: any = [];

  private contaCorrente;
  private clienteNome;
  private clienteContribuinte;
  private contaCorrenteDados: any = [];
  private arr: any = []

  private somaNotacredito = 0.0;
  private somaFactura = 0.0;
  private somaRecibo = 0.0;
  private somaFacturas_Recibo = 0.0;

  private somaNotacreditoExcel = 0.0;
  private somaFacturaExcel = 0.0;
  private somaReciboExcel = 0.0;
  private totalAberto = 0.0;

  private valorTotal = 0.0;
  private factura_aberto = 0;
  private numeroAdiantamentos = 0;



  private validacao = false;

  public pieChartLabels = ['Sales Q1', 'Sales Q2', 'Sales Q3', 'Sales Q4'];
  public pieChartData = [120, 150, 180, 90];
  public pieChartType = 'pie';

  myChart: any = []
  id;
  constructor(private http: HttpService, private configService: ConfigService, private ContaCorrenteReportService: ContaCorrenteReportService, private route: ActivatedRoute, private execel: ContaCorrenteService) {
    this.route.paramMap.subscribe(params => {
      this.id = +params.get('id');
    });
  }

  ngOnInit() {
    this.ListarClientes(this.pagination.page, this.pagination.perPage, this.conta_corrente.search, 'nome');
    this.listarseries(1, 1000);
  }

  private chartts() {

    let factura_nao_paga = 0;
    let facturas = 0;
    let factura_nuladas = 0
    let nota_credito = 0;
    let recibo = 0;
    let facturas_recibo = 0;
    this.factura_aberto = 0;

    //console.log(this.contas.facturas_recibo);
    // facturas 
    if (this.contas.facturas.length == 0) {
      facturas = 0;
      factura_nao_paga = 0;
      factura_nuladas = 0;
      var fact = 0;

    } else {


      for (let index = 0; index < this.contas.facturas.length; index++) {

        //console.log(this.contas.facturas[index].pago);
        if (this.contas.facturas[index].pago == 0) {
          factura_nao_paga++;
          this.factura_aberto = factura_nao_paga;

        } if (this.contas.facturas[index].sigla == "FT") {
          facturas++;
        }
        if (this.contas.facturas[index].status == "A") {
          factura_nuladas++;
        }
      }
    }
    // Notas de Credito
    if (this.contas.notaCreditos.length == 0) {
      nota_credito = 0;
    } else {
      nota_credito = this.contas.notaCreditos.length;
    }

    // recibos
    if (this.contas.recibos.length == 0) {
      recibo = 0
    } else {
      recibo = this.contas.recibos.length;
    }

    if (this.contas.facturas_recibo.length == 0) {
      facturas_recibo = 0
    } else {
      facturas_recibo = this.contas.facturas_recibo.length;
    }

    function ConvertNum(num) {
      return (
        num
          .toFixed(2) // duas casas decimais
          .replace('.', ',') // separadores,
          .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
      ) // use . as a separador
    }

    var somas = [ConvertNum(this.somaFacturas_Recibo), ConvertNum(this.somaFactura), ConvertNum(this.somaRecibo), ConvertNum(this.somaNotacredito)]
    this.myChart = new Chart('myChart', {
      type: 'bar',
      data: {
        labels: ['Facturas Recibo', 'Facturas', 'Recibos', 'Notas de Credito'],
        datasets: [{

          data: [this.somaFacturas_Recibo, this.somaFactura, this.somaRecibo, this.somaNotacredito],
          backgroundColor: [
            'rgb(0, 102, 255)',
            'rgb(143, 170, 220)',
            'rgba(0, 112, 192)',
            'rgba(255, 99, 132, 1)'],
          borderColor: [
            'rgb(0, 102, 255)',
            'rgb(143, 170, 220)',
            'rgba(0, 112, 192)',
            'rgba(255, 99, 132, 1)'],
          borderWidth: 1
        }]
      },
      options: {
        tooltips: {
          enabled: true
        },
        animation: {

          onComplete: function () {
            var chartInstance = this.chart,
              ctx = chartInstance.ctx;
            ctx.textAlign = 'center';
            ctx.fillStyle = "rgba(0, 0, 0, 1)";
            ctx.textBaseline = 'bottom';
            ctx.font = '14px calibri';

            this.data.datasets.forEach(function (dataset, i) {
              var meta = chartInstance.controller.getDatasetMeta(i);
              meta.data.forEach(function (bar, index) {
                var data = somas[index];
                ctx.fillText(data, bar._model.x, bar._model.y - 5);

              });
            });
          }
        },
        title: {
          display: true,
          text: 'Totais de Facturação (em AOA)',
          fontSize: 16,
          padding: 20
        },
        legend: { display: false },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });
  }
  private pesquisarCliente(e) {
    this.pagination.page = 1;
    this.ListarClientes(this.pagination.page, this.pagination.perPage, this.conta_corrente.search, 'nome');
  }

  private ListarClientes(start, end, search, orderBy) {
    this.validacao = false;

    if(this.id!=0 && search == ""){
      this.configService.loaddinStarter('start');
      this.http.__call('cliente/search-cliente', { start: start, end: end, search: this.id }).subscribe(
        response => {

          this.clientes = Object(response).data.data;
          this.configService.loaddinStarter('stop');
          this.refreshClienteId(this.id);
          this.adiantamentos();
        }
      );
      
    }
    if (search != "") {
      this.configService.loaddinStarter('start');
      this.http.__call('cliente/search-cliente', { start: start, end: end, search: search }).subscribe(
        response => {

          this.clientes = Object(response).data.data;
          this.configService.loaddinStarter('stop');
        }
      );
    } else {
      this.clientes = [];
    }
  }

  private ListarClientess(start, end, search, orderBy) {

    this.configService.loaddinStarter('start');
    this.pages = [];
    this.pagination.start = start;
    this.pagination.end = end;
    this.pagination.search = search;
    this.pagination.order = orderBy;

    this.http.__call('cliente/listar', this.pagination).subscribe(

      response => {
        //console.log(response)
        this.pagination.lastPage = Object(response).data.lastPage;
        this.pagination.page = Object(response).data.page;
        this.pagination.total = Object(response).data.total;
        this.pagination.perPage = Object(response).data.perPage;

        this.clientes = Object(response).data.data;
        this.gerarPages();
        this.configService.loaddinStarter('stop');

      }
    );
  }

  private refreshDataCliente(id, nome, contribuente, telefone, morada) {
    this.cliente.id = id;
    this.cliente.nome = nome;
    this.cliente.contribuente = contribuente;
    this.cliente.telefone = telefone;
    this.cliente.morada = morada;
    //console.log(this.cliente);

  }

  private mostrarResultados() {
    this.ListarClientes(this.pagination.page, this.pagination.perPage, this.conta_corrente.search, 'nome');

  }

  private pages = [];
  private nextProvPageNumber = 1;

  private gerarPages() {
    for (var i = 1; i <= this.pagination.lastPage; i++) {
      this.pages.push(i);
    }
  }
  private nextPage() {
    this.nextProvPageNumber++;
    if (this.nextProvPageNumber < 0) {
      this.nextProvPageNumber = this.pagination.page;
    }
    this.ListarClientes(this.pagination.page, this.pagination.perPage, this.conta_corrente.search, 'nome');

  }
  private prevPage() {
    this.nextProvPageNumber--
    if (this.nextProvPageNumber > 0) {
      this.ListarClientes(this.pagination.page, this.pagination.perPage, this.conta_corrente.search, 'nome');
      this.nextProvPageNumber = this.pagination.page;
    }
  }

  private refreshPaginate(page) {
    this.pagination.page = page;
    this.ListarClientes(this.pagination.page, this.pagination.perPage, this.conta_corrente.search, 'nome');
  }

  private deleteClienteNoSeleted(id) {

    this.clientes.forEach(element => {
      if (element.id == id) {
        this.conta_corrente.cliente.id = element.id;
        this.conta_corrente.cliente.nome = element.nome;
        this.conta_corrente.cliente.contribuente = element.contribuente;
        this.conta_corrente.cliente.telefone = element.telefone;
        this.conta_corrente.cliente.morada = element.morada;
        this.conta_corrente.search = element.nome;
      }
    });
    /*for (let i = 0; i < this.clientes.length; ++i) {
      if (this.clientes[i].id == id) {
      this.clientes.splice(i, 1);
      }
    }*/
    this.clientes = [];
  }

  private changeSelectValue() {
    this.selectBox.pago = this.selectBox.pago;

    //console.log(this.selectBox.pago)
  }

  private refreshClienteId(id) {
    this.cliente.id = id;

    //console.log(this.selectBox.pago)
    this.deleteClienteNoSeleted(id);
    this.contaCorrentes();
    //this.ExportaContaCorrente();
  }


  private contaCorrentes() {
    this.configService.loaddinStarter('start');
    this.http.__call('contaCorrente/contas', { cliente_id: this.cliente.id, estado_factura: this.selectBox.pago }).subscribe(
      res => {
        if (Object(res).code == 500) {
          this.configService.showAlert(Object(res).message, "alert-danger", true);
          this.configService.loaddinStarter('stop');
        } else {
          this.validacao = true;
          this.contas = Object(res).data;
        // this.FacturasExportExcel = Object(res).data.exportcontacorrentexcel;
          // console.log(this.FacturasExportExcel)



          this.clienteNome = Object(res).data.cliente.nome;
          this.clienteContribuinte = Object(res).data.cliente.contribuente;

          this.calcular();
          this.chartts();
          this.configService.loaddinStarter('stop');
        }
      }
    );
  }


  private ExportaContaCorrente() {
    this.http.__call('contaCorrente/report ', { cliente_id: this.cliente.id, estado_factura: this.selectBox.pago }).subscribe(response => {
      this.contaCorrente = Object(response).data.facturas;
      this.clienteNome = Object(response).data.cliente.nome;
      this.clienteContribuinte = Object(response).data.cliente.contribuente;
      this.contaCorrenteDados = [];
      this.arr = [];
      for (let i = 0; i < this.contaCorrente.length; i++) {

        for (let index = 0; index < this.contaCorrente[i].contacorrente.length; index++) {



          this.contaCorrenteDados.push(this.contaCorrente[i].contacorrente[index])
          this.arr.push(this.contaCorrente[i].clientefilhos)
          this.contaCorrenteDados[index].contas = this.arr[index];

          this.somaReciboExcel += (this.contaCorrente[i].contacorrente[index].sigla == 'RC' ? this.contaCorrente[i].contacorrente[index].valor : 0)
          this.somaFacturaExcel += (this.contaCorrente[i].contacorrente[index].sigla == 'FT' ? this.contaCorrente[i].contacorrente[index].valor : 0)
          this.somaNotacreditoExcel += (this.contaCorrente[i].contacorrente[index].sigla == 'NC' ? this.contaCorrente[i].contacorrente[index].valor : 0)


        }



      }

      this.valorTotal = ((this.somaFacturaExcel - this.somaNotacreditoExcel) - this.somaReciboExcel)
      //console.log(this.contaCorrente)




    })

  }

  exportAsXLSX(): void {

    var total = ((this.somaFactura - this.somaNotacredito) - this.somaRecibo)
    var CurrentDate = new Date();
    var nameFile = "Conta_Corrente-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    //console.log(total)
    this.execel.ExcelExportContaCorrente(this.contas, this.totalAberto, this.clienteNome,this.contas.empresa, nameFile + '.xlsx')

    /* var CurrentDate = new Date();
      var nameFile = "Conta_Corrente-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
        + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m') 
      this.execel.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXContaCorrent")[0], nameFile);*/
  }

  private adiantamentos() {
    //console.log(this.contas);

    this.http.__call("adiantamento/factura/adiantamentoContaCliente", { cliente_id: this.cliente.id }).subscribe(response => {

      // console.log(response);

      if (Object(response).data.length == 0) {
        // console.log("undefined")
        this.totalAdiantamento = 0;
        this.numeroAdiantamentos = 0;

      } else {
        //console.log("valor encontrado")
        this.totalAdiantamento = Object(response).data[0].valor;
        this.numeroAdiantamentos = Object(response).data.length;
      }


    })


  }

  private calcular() {
    this.somaRecibo = 0.0;
    this.somaFactura = 0.0;
    this.somaNotacredito = 0.0;
    this.somaFacturas_Recibo = 0.0;
    this.totalAberto = 0.0;

    this.contas.recibos.forEach(element => {
      this.somaRecibo += element.total;
    });
    this.contas.facturas.forEach(element => {
      this.somaFactura += element.total;
    });
    this.contas.notaCreditos.forEach(element => {
      this.somaNotacredito += element.total;
    });
    this.contas.facturas_recibo.forEach(element => {
      this.somaFacturas_Recibo += element.total;
    });
    this.contas.facturas_em_Aberto.forEach(element => {
      this.totalAberto += element.valor_aberto;
    });
  }


  private anularFactura(e) {

    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (e.target.elements[0].value == "" || e.target.elements[2].value == "") {
      this.configService.showAlert('É obrigatório fornecer o nº da factura e o motivo', 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
      this.factura.status_reason = e.target.elements[2].value;
      this.http.__call('factura/anular/' + this.factura.factura_id, this.factura).subscribe(
        res => {
          if (Object(res).code == 500) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
          } else {
            this.contaCorrentes()
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.configService.clearFormInputs(e);
          }
          this.configService.loaddinStarter('stop');
        }
      );
    }
  }


  private btnImprimirFactura(id) {
    //console.log(this.contas)
    this.configService.imprimirFactura(id, "2ª Via");

  }

  private ImprimirContaCorrente(cliente) {
    var table = document.getElementsByClassName("exportAsXLSXContaCorrent")[0]; 
    this.ContaCorrenteReportService.contaCorrenteReport(cliente, this.selectBox.pago,table,this.contas.empresa,'print'); 
  }

  private ExportarContaCorrente(cliente) {
    var table = document.getElementsByClassName("exportAsXLSXContaCorrent")[0]; 
    this.ContaCorrenteReportService.contaCorrenteReport(cliente, this.selectBox.pago,table,this.contas.empresa,'save'); 
  }



  refreshFacturaAnual(facturas: any) {
    this.factura = facturas;
    this.factura.factura_id = facturas.id;
  }

  // nota de credito
  private closeModal() {
    this.totalFacturacao = 0;
    this.totalComImposto = 0;
    this.dadosFacturaId = [];
    this.produtosNotaCredito = [];
    this.btnClickBoleon = false;

    this.configService.loaddinStarter('stop');
  }

  private refreshFacturaNotaCredito(factura_id) {

    // console.log(this.series);
    this.configService.loaddinStarter('start');
    this.http.__call('factura/gerarFactura/' + factura_id, null).subscribe(
      response => {
        //this.listarseries(1, 100);
        this.dadosFacturaId = Object(response).data;
        this.btnClickBoleon = true;
        this.configService.loaddinStarter('stop');
      }
    );

  }

  private btnAddProdutosNotaCredito(produto_id) {
    this.dadosFacturaId.produtos.forEach(element => {
      if (element.produto_id == produto_id) {
        this.addProdutosNotaCredito(element);
      }
    });
  }

  private addProdutosNotaCredito(produto: any) {
    this.totalFacturacao = 0;
    let validar = 0;
    if (this.produtosNotaCredito.length >= 1) {
      for (let index = 0; index < this.produtosNotaCredito.length; index++) {
        const produt = this.produtosNotaCredito[index];
        if (produt.produto_id == produto.produto_id) {
          this.produtosNotaCredito.splice(index, 1);
          this.produtosNotaCredito.splice(index, 0, produto);
          validar = 1;
        }

      }
      if (validar == 0) {
        this.produtosNotaCredito.push(produto);
      }
    } else {
      this.produtosNotaCredito.push(produto);
    }
    //Calcula o Total da Factura
    this.calcularTotal();
  }

  private calcularTotal() {
    this.totalFacturacao = 0;
    this.totalSemImposto = 0;
    this.totalComImposto = 0;
    //Calcula o Total da Factura
    for (let index = 0; index < this.produtosNotaCredito.length; index++) {
      this.totalSemImposto += this.produtosNotaCredito[index].linhaTotalSemImposto;
      this.totalComImposto += this.produtosNotaCredito[index].valorImposto;

      this.totalFacturacao += this.produtosNotaCredito[index].total;
    }
  }


  deleteRow(id) {

    for (let i = 0; i < this.produtosNotaCredito.length; ++i) {
      if (this.produtosNotaCredito[i].produto_id === id) {
        this.produtosNotaCredito.splice(i, 1);
        this.calcularTotal();
      }
    }
  }


  private refreshSerieId() {
    this.serieId = this.serie.nativeElement.value;
    if (Object(this.serieId) == "") {
      this.serieId = 0;
    }

  }
  private refreshSerieReciboId() {
    this.serieReciboId = this.serieRecibo.nativeElement.value;
    if (Object(this.serieReciboId) == "") {
      this.serieReciboId = 0;
    }

  }

  /**
   * @name "Listar series"
   * @descriptio "Esta Função permite Listar todas series"
   * @author "caniggia.moreira@itgest.pt"
   * @param start
   * @param end
   */
  private listarseries(start, end) {
    this.http.__call('serie/listar', { start: start, end: end }).subscribe(
      data => {
        data.forEach(element => {
          // console.log(element)
          if (element.sigla == 'NC') {
            this.series.push(element);
            //console.log(this.series); 

          }


        });

      }
    );


  }

  private finalizarNotaCredito() {

    this.configService.loaddinStarter('start');
    if (this.serieId == undefined) {
      this.configService.showAlert('É obrigatório fornecer uma serie', "alert-danger", true);
    } else if (this.produtosNotaCredito.length == 0) {
      this.configService.showAlert('É obrigatório fornecer produtos para a nota de crédito', "alert-danger", true);
    } else {
      this.http.__call('factura/create', {
        produtos: this.produtosNotaCredito, documento: 'Nota de Crédito',
        cliente: this.dadosFacturaId.cliente.id, total: this.totalFacturacao,
        serie_id: this.serieId, totalComImposto: this.totalComImposto, totalSemImposto: this.totalSemImposto,
        observacao: this.observacao.nativeElement.value,
        numero_origem_factura: this.dadosFacturaId.factura.factura_sigla,
        data_origem_factura: this.dadosFacturaId.factura.created_at,
        moeda: null
      }).subscribe(
        res => {
          if (Object(res).code == 200) {
            this.contaCorrentes();
            this.configService.showAlert(Object(res).message, "alert-success", true);
            this.btnClickBoleon = false;
            this.configService.imprimirFactura(Object(res).data.id, "Original");

            this.totalFacturacao = 0;
            this.totalComImposto = 0;
            this.dadosFacturaId = [];
            this.produtosNotaCredito = [];

          } else {
            this.configService.showAlert(Object(res).message, "alert-danger", true);
          }
        }
      );

    }
    this.configService.loaddinStarter('stop');
  }


}

