import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalexportarcontacorrenteexelComponent } from './modalexportarcontacorrenteexel.component';

describe('ModalexportarcontacorrenteexelComponent', () => {
  let component: ModalexportarcontacorrenteexelComponent;
  let fixture: ComponentFixture<ModalexportarcontacorrenteexelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalexportarcontacorrenteexelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalexportarcontacorrenteexelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
