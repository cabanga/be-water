import { TestBed } from '@angular/core/testing';

import { ContaCorrenteReportService } from './conta-corrente-report.service';

describe('ContaCorrenteReportService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContaCorrenteReportService = TestBed.get(ContaCorrenteReportService);
    expect(service).toBeTruthy();
  });
});
