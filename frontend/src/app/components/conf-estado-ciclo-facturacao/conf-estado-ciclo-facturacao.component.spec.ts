import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfEstadoCicloFacturacaoComponent } from './conf-estado-ciclo-facturacao.component';

describe('ConfEstadoCicloFacturacaoComponent', () => {
  let component: ConfEstadoCicloFacturacaoComponent;
  let fixture: ComponentFixture<ConfEstadoCicloFacturacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfEstadoCicloFacturacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfEstadoCicloFacturacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
