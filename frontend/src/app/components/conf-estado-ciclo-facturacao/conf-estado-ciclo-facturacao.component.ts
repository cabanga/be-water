import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

@Component({
  selector: 'app-conf-estado-ciclo-facturacao',
  templateUrl: './conf-estado-ciclo-facturacao.component.html',
  styleUrls: ['./conf-estado-ciclo-facturacao.component.css']
})
export class ConfEstadoCicloFacturacaoComponent implements OnInit {

  private estadociclofacturacao = {
    id: null,
    descricao: null,
    slug: null
  }


  private items: any = [];
  private CicloFacturacao: any = [];

  constructor(private http: HttpService, private configService: ConfigService) { }

  ngOnInit() {
    this.getPageFilterData(1);
  }


  private listaestadociclofacturacao() {

    this.configService.loaddinStarter('start');

    this.http.__call('estado-ciclo-facturacao/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.listaestadociclofacturacao();
  }


  private register(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (this.estadociclofacturacao.descricao == "" || this.estadociclofacturacao.slug == null) {
      this.configService.showAlert("Os campos Descrição e Slug são obrigatórios", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
    this.http.__call('estado-ciclo-facturacao/create', this.estadociclofacturacao).subscribe(
      res => {
        if (Object(res).code == 201) {
          this.configService.showAlert(Object(res).message, 'alert-danger', true);
          this.configService.loaddinStarter('stop');
        } else {
          this.configService.showAlert(Object(res).message, 'alert-success', true);
          this.clearFormInputs(e);
          this.listaestadociclofacturacao()
          this.configService.loaddinStarter('stop');
        }
      }
    )
  };
}


  private clearFormInputs(e) {
    e.target.elements[0].value = null;
    e.target.elements[1].value = null;
    e.target.elements[2].value = null;
  }

  private refresh(id, descricao, slug) {
    this.estadociclofacturacao.id = id;
    this.estadociclofacturacao.descricao = descricao;
    this.estadociclofacturacao.slug = slug;
  }

  private editar(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (this.estadociclofacturacao.descricao == "" || this.estadociclofacturacao.slug == null) {
      this.configService.showAlert("Os campos Descrição e Slug são obrigatórios", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
      this.http.__call('estado-ciclo-facturacao/update/' + this.estadociclofacturacao.id, this.estadociclofacturacao).subscribe(
        res => {
          if (Object(res).code == 201) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
          } else {
            //this.configService.clearFormInputs(e);
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.listaestadociclofacturacao();

          }
        }
      );
    }
    this.configService.loaddinStarter('stop');
  }

  private loadingCiclo = {
    estado: 'Selecione o Ciclo',
  }

  private getCicloFacturacao() {
    this.loadingCiclo.estado = 'Carregando...';
    
      this.http.call_get('estado-ciclo-facturacao/selectBox' , null).subscribe(
      response => {
        this.CicloFacturacao = Object(response);
        this.loadingCiclo.estado = 'Selecione o Estado do Ciclo';
      }
    );
  }


  private ini() {
    this.estadociclofacturacao = {
      id: null,
      descricao: null,
      slug: null
    }
  }





}
