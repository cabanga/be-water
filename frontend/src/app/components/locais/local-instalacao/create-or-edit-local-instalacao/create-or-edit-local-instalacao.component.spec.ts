import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditLocalInstalacaoComponent } from './create-or-edit-local-instalacao.component';

describe('CreateOrEditLocalInstalacaoComponent', () => {
  let component: CreateOrEditLocalInstalacaoComponent;
  let fixture: ComponentFixture<CreateOrEditLocalInstalacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditLocalInstalacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditLocalInstalacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
