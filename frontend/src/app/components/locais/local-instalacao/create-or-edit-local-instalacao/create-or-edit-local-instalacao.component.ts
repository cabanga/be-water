import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange, NgModule } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/providers/http/http.service';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { first } from 'rxjs/operators';
import { AppComponent } from 'src/app/app.component';
import { AgmCoreModule } from '@agm/core';
import { stream } from 'xlsx/types';

@Component({
  selector: 'app-create-or-edit-local-instalacao',
  templateUrl: './create-or-edit-local-instalacao.component.html',
  styleUrls: ['./create-or-edit-local-instalacao.component.css']
})
export class CreateOrEditLocalInstalacaoComponent implements OnInit {

  public currentUser: any;
  @Input() title: string = "Registar Local de Instalação";
  @Input() contrato_has_joined: boolean = false;

  @Input() local_instalacao = {
    id: null,
    moradia_numero: null,
    is_predio: null,
    predio_id: null,
    predio_andar: null,
    predio_nome: null,
    cil: null,
    rua_id: null,
    rua: null,
    bairro_id: null,
    bairro: null,
    has_quarteirao: null,
    quarteirao_id: null,
    has_distrito: false,
    municipio_id: null,
    distrito_id: null,
    provincia_id: null,
    latitude: null,
    longitude: null,
    is_active: null,
    user_id: null,

    ligacao_ramal_id: null,
    tipo_objecto_id: null,
    objecto_ligacao_id: null,
    diamentro: null,
    comprimento: null,
    profundidade: null,


    instalacao_sanitaria_qtd: null,
    reservatorio_flag: null,
    reservatorio_capacidade: null,
    piscina_flag: null,
    piscina_capacidade: null,
    jardim_flag: null,
    campo_jardim_id: null,
    campo_jardim: null,
    poco_alternativo_flag: null,
    fossa_flag: null,
    fossa_capacidade: null,
    acesso_camiao_flag: null,
    anexo_flag: null,
    anexo_quantidade: null,
    caixa_contador_flag: null,
    estado_caixa_contador_id: null,
    abastecimento_cil_id: null,
    abastecimento_cil: null,
    calibre_id: null
  };


  /*   @Input() municipio = {
      id: null,
      predio_nome: null
    }; */

  @Input() local_instalacaos: any = [];

  private provincias: any = [];
  @Input() municipios: any = [];
  @Input() distritos: any[];
  @Input() bairros: any[];
  @Input() quarteiraos: any[];
  @Input() ruas: any[];

  @Input() tipo_objectos: any[];

  private moradia_title: string = "Moradia";

  private objectos_ramais: any = [];

  private ligacao_ramals: any = [];

  private estado_caixa_contadors: any = [];
  private objecto_contratos: any = [];
  private campo_jardims: any = [];
  private abastecimento_cils: any = [];

  private local_instalacaosArray: any = [];

  private local_instalacaoRow = {
    id: null,
    moradia_numero: null,
    is_predio: null,
    predio_id: null,
    predio_andar: null,
    predio_nome: null,
    cil: null,
    rua_id: null,
    rua: null,
    bairro_id: null,
    bairro: null,
    has_quarteirao: null,
    quarteirao_id: null,
    quarteirao: null,
    has_distrito: null,
    distrito_id: null,
    distrito: null,
    municipio_id: null,
    municipio: null,
    provincia_id: null,
    provincia: null,
    latitude: null,
    longitude: null,
    user_id: null,
    user: null,

    ligacao_ramal_id: null,
    tipo_objecto_id: null,
    objecto_ligacao_id: null,
    diamentro: null,
    comprimento: null,
    profundidade: null,

    instalacao_sanitaria_qtd: null,
    reservatorio_flag: null,
    reservatorio_capacidade: null,
    piscina_flag: null,
    piscina_capacidade: null,
    jardim_flag: null,
    campo_jardim_id: null,
    campo_jardim: null,
    poco_alternativo_flag: null,
    fossa_flag: null,
    fossa_capacidade: null,
    acesso_camiao_flag: null,
    anexo_flag: null,
    anexo_quantidade: null,
    caixa_contador_flag: null,
    estado_caixa_contador_id: null,
    abastecimento_cil_id: null,
    abastecimento_cil: null
  };

  private predio_flag: boolean = false;
  private geolocation_flag: boolean = false;
  private geolocation_updated: boolean = false;

  private geolocalizacaoEndModal: boolean = false;

  private data_ligacao_flag: boolean = false;
  private selected_local_instalacao: string = null;

  private local_instalacaoRowValid: boolean = false;
  @Input() addRows: boolean = true;

  @Input() showCreateLocalInstalacao: boolean = true;

  @Input() local_instalacaoModal: boolean = false;


  texto: string = 'Planeamento';
  lat = -12.3757287;
  lng = 13.5610451;
  zoom = 10;

  submitted = false;
  private loading: boolean = false;

  //@Output() private loadListLocalInstalacao = new EventEmitter<any>();

  constructor(private auth: AuthService, private http: HttpService, private configService: ConfigService, private formBuilder: FormBuilder) {
    //this.createForm();
    this.currentUser = this.auth.currentUserValue;
  }


  ngOnInit() {

    //console.log(this.local_instalacao);
    //console.log(this.municipios);

    if (this.provincias.length <= 0) {
      this.local_instalacao.provincia_id = 29;
      this.selectBoxProvincias();
    }

    if (this.addRows == false) {
      this.selectBoxProvincias();
    }

    this.onReset();
    this.local_instalacao.user_id = this.currentUser.user.id;


    this.selectBoxTipoObjectos();
    //console.log(this.local_instalacao);

    this.getPageFilterLigacaoRamal(1);

    //console.log(this.ligacao_ramals);


    this.getEstadoCaixaContadors();
    this.getObjectosContratos();
    this.getCampoJardims();
    this.getAbastecimentoCILs();

  }


  private getLigacaoRamals() {

    this.data_ligacao_flag = false;

    this.http.__call('ligacao-ramal/getLigacaoRamals', {
      tipo_objecto_id: this.local_instalacao.tipo_objecto_id,
      objecto_ligacao_id: this.local_instalacao.objecto_ligacao_id,
      pagination: this.http.filters
    }).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.ligacao_ramals = Object(response).data.data;

      }
    );
  }


  private getPageFilterLigacaoRamal(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }

    this.http.filters.pagination.page = page;

    this.getLigacaoRamals();
  }


  private setLigacaoRamal(item) {

    this.resetLigacao();

    this.local_instalacao.ligacao_ramal_id = item.id;

    if (item.local_instalacao_id != null) {
      this.selected_local_instalacao = "a " + ((item.is_predio) ? 'Prédio ' + item.predio_nome + ', Porta ' : 'Residência ') + item.moradia_numero;
    } else {
      this.selected_local_instalacao = "a " + item.ponto_b_tipo_objecto + " - " + item.ponto_b;
    }

    this.data_ligacao_flag = true;

  }

  private resetLigacao() {

    this.data_ligacao_flag = false;

    this.selected_local_instalacao = null;

    this.local_instalacao.ligacao_ramal_id = null;
    this.local_instalacao.comprimento = null;
    this.local_instalacao.diamentro = null;
    this.local_instalacao.profundidade = null;
  }


  private validateLigacaoRamal(item) {

    var result: boolean = true;

    if (this.local_instalacao.comprimento == null) {
      this.configService.showAlert('Insira o comprimento da ligação!', 'alert-danger', true);
      result = false;
    } else if (this.local_instalacao.diamentro == null) {
      this.configService.showAlert('Insira o diâmetro da ligação!', 'alert-danger', true);
      result = false;
    } else if (this.local_instalacao.profundidade == null) {
      this.configService.showAlert('Insira a profundidade da ligação!', 'alert-danger', true);
      result = false;
    }

    return result;
  }

  save() {

    if (!this.addRows) {
      console.log(this.local_instalacao);
      this.local_instalacaosArray.unshift({ ...this.local_instalacao });

      //console.log(this.local_instalacaosArray);
    }

    for (let i = 0; i < this.local_instalacaosArray.length; ++i) {

      if (this.local_instalacaosArray[i].id == null) {
        this.http.__call('local-instalacao/create', this.local_instalacaosArray[i]).subscribe(
          response => {

            if (Object(response).code == 200) {
              this.configService.showAlert(Object(response).message, "alert-success", true);
              this.local_instalacaosArray = [];
              this.onReset();

              this.resetLigacao();

            } else {
              this.configService.showAlert(Object(response).message, "alert-danger", true);
            }

          }
        );

      }
      else {

        this.http.__call('local-instalacao/update/' + this.local_instalacaosArray[i].id, this.local_instalacaosArray[i]).subscribe(
          response => {

            if (Object(response).code == 200) {
              this.configService.showAlert(Object(response).message, "alert-success", true);
              this.local_instalacaosArray = [];
            } else {
              this.configService.showAlert(Object(response).message, "alert-danger", true);
            }

          }
        );
      }
    }

  }
  /* 
    createLocalInstalacao(item) {
      this.http.__call('local-instalacao/create', item).subscribe(
        response => {    
              
          if (Object(response).code == 200) {
            this.configService.showAlert(Object(response).message, "alert-success", true);
            this.local_instalacaosArray = [];
            this.onReset();
  
          } else {
            this.configService.showAlert(Object(response).message, "alert-danger", true);
          }
  
  
        }
      );
    }
   */

  createOrEdit(uri: any, formulario: any, isCreate: boolean) {

    // TODO: usado para fazer a requisição com a api de criação de object
    this.http.__call(uri, formulario).pipe(first()).subscribe(
      response => {
        this.submitted = false;
        this.loading = false;

        if (Object(response).code == 200) {
          this.openEndLocalInstalacaoModal(true);
          this.reloadLocalInstalacaos();
        }

        if (isCreate) {
          formulario.reset();
        }

        //this.local_instalacaos = Object(response).data;
      },
      error => {
        this.submitted = false;
        this.loading = false;
      });
  }

  setDataLocalInstalacao({ item }) {

    if (this.provincias.length <= 0) {
      this.selectBoxProvincias();
    }
    //console.log(this.local_instalacao);
    //console.log(item);

    if (item.id > 0) {
      this.title = "Editar Local de Instalação";

      this.local_instalacao.id = item.id;
      this.local_instalacao.moradia_numero = item.moradia_numero;
      this.local_instalacao.is_predio = item.is_predio;
      this.local_instalacao.predio_id = item.predio_id;
      this.local_instalacao.predio_andar = item.predio_andar;
      this.local_instalacao.predio_nome = item.predio_nome;
      this.local_instalacao.cil = item.cil;
      this.local_instalacao.rua_id = item.rua_id;
      this.local_instalacao.rua = item.rua;
      this.local_instalacao.bairro_id = item.bairro_id;
      this.local_instalacao.bairro = item.bairro;
      this.local_instalacao.has_quarteirao = item.quarteirao;
      this.local_instalacao.quarteirao_id = item.quarteirao_id;
      this.local_instalacao.has_distrito = item.has_distrito;
      this.local_instalacao.distrito_id = item.distrito_id;
      this.local_instalacao.municipio_id = item.municipio_id;
      this.local_instalacao.provincia_id = item.provincia_id;
      this.local_instalacao.latitude = item.latitude;
      this.local_instalacao.longitude = item.longitude;
      this.local_instalacao.is_active = item.is_active;
      this.local_instalacao.user_id = item.user_id;

      this.local_instalacao.ligacao_ramal_id = item.ligacao_ramal_id;
      this.local_instalacao.tipo_objecto_id = item.tipo_objecto_id;
      this.local_instalacao.objecto_ligacao_id = item.objecto_ligacao_id;
      this.local_instalacao.diamentro = item.diamentro;
      this.local_instalacao.comprimento = item.comprimento;
      this.local_instalacao.profundidade = item.profundidade;

      this.local_instalacao.instalacao_sanitaria_qtd = item.instalacao_sanitaria_qtd;
      this.local_instalacao.reservatorio_flag = item.reservatorio_flag;
      this.local_instalacao.reservatorio_capacidade = item.reservatorio_capacidade;
      this.local_instalacao.piscina_flag = item.piscina_flag;
      this.local_instalacao.piscina_capacidade = item.piscina_capacidade;
      this.local_instalacao.jardim_flag = item.jardim_flag;
      this.local_instalacao.campo_jardim_id = item.campo_jardim_id;
      this.local_instalacao.poco_alternativo_flag = item.poco_alternativo_flag;
      this.local_instalacao.fossa_flag = item.fossa_flag;
      this.local_instalacao.fossa_capacidade = item.fossa_capacidade;
      this.local_instalacao.acesso_camiao_flag = item.acesso_camiao_flag;
      this.local_instalacao.anexo_flag = item.anexo_flag;
      this.local_instalacao.anexo_quantidade = item.anexo_quantidade;
      this.local_instalacao.caixa_contador_flag = item.caixa_contador_flag;
      this.local_instalacao.estado_caixa_contador_id = item.estado_caixa_contador_id;
      this.local_instalacao.abastecimento_cil_id = item.abastecimento_cil_id;

    } else {
      this.title = "Registar Local de Instalação";
      this.onReset();
    }

    //console.log(this.local_instalacao);
  }

  cleanLigacaoRamal() {

    this.local_instalacao.moradia_numero = null;

    this.local_instalacao.ligacao_ramal_id = null;
    this.local_instalacao.tipo_objecto_id = null;
    this.local_instalacao.objecto_ligacao_id = null;
    this.local_instalacao.diamentro = null;
    this.local_instalacao.comprimento = null;
    this.local_instalacao.profundidade = null;
  }

  onReset() {
    this.submitted = false;

    this.local_instalacao.id = null;
    this.local_instalacao.moradia_numero = null;
    this.local_instalacao.is_predio = null;
    this.local_instalacao.predio_id = null;
    this.local_instalacao.predio_andar = null;
    this.local_instalacao.predio_nome = null;
    this.local_instalacao.cil = null;
    this.local_instalacao.rua_id = null;
    this.local_instalacao.rua = null;
    this.local_instalacao.bairro_id = null;
    this.local_instalacao.bairro = null;
    this.local_instalacao.has_quarteirao = false;
    this.local_instalacao.quarteirao_id = null;
    this.local_instalacao.has_distrito = false;
    this.local_instalacao.distrito_id = null;
    this.local_instalacao.municipio_id = null;
    this.local_instalacao.is_active = null;
    this.local_instalacao.user_id = null;

    this.local_instalacao.ligacao_ramal_id = null;
    this.local_instalacao.tipo_objecto_id = null;
    this.local_instalacao.objecto_ligacao_id = null;
    this.local_instalacao.diamentro = null;
    this.local_instalacao.comprimento = null;
    this.local_instalacao.profundidade = null;
  }

  private async appendRowLocalInstalacao() {

    this.geolocation_updated = false;

    const loadedStateCallback = () => {
      console.log(this.local_instalacao);

      this.local_instalacaoRow.is_predio = this.predio_flag;
      this.local_instalacaoRow.predio_nome = (this.predio_flag) ? this.local_instalacao.predio_nome : null;
      this.local_instalacaoRow.predio_andar = this.local_instalacao.predio_andar;

      this.local_instalacaoRow.moradia_numero = this.local_instalacao.moradia_numero;
      this.local_instalacaoRow.cil = this.local_instalacao.cil;
      this.local_instalacaoRow.user_id = this.currentUser.user.id;

      this.local_instalacaoRow.ligacao_ramal_id = this.local_instalacao.ligacao_ramal_id;
      this.local_instalacaoRow.tipo_objecto_id = this.local_instalacao.tipo_objecto_id;
      this.local_instalacaoRow.objecto_ligacao_id = this.local_instalacao.objecto_ligacao_id;
      this.local_instalacaoRow.diamentro = this.local_instalacao.diamentro;
      this.local_instalacaoRow.comprimento = this.local_instalacao.comprimento;
      this.local_instalacaoRow.profundidade = this.local_instalacao.profundidade;

      this.local_instalacaoRow.instalacao_sanitaria_qtd = this.local_instalacao.instalacao_sanitaria_qtd;
      this.local_instalacaoRow.reservatorio_flag = this.local_instalacao.reservatorio_flag;
      this.local_instalacaoRow.reservatorio_capacidade = this.local_instalacao.reservatorio_capacidade;
      this.local_instalacaoRow.piscina_flag = this.local_instalacao.piscina_flag;
      this.local_instalacaoRow.piscina_capacidade = this.local_instalacao.piscina_capacidade;
      this.local_instalacaoRow.jardim_flag = this.local_instalacao.jardim_flag;
      this.local_instalacaoRow.campo_jardim_id = this.local_instalacao.campo_jardim_id;
      this.local_instalacaoRow.poco_alternativo_flag = this.local_instalacao.poco_alternativo_flag;
      this.local_instalacaoRow.fossa_flag = this.local_instalacao.fossa_flag;
      this.local_instalacaoRow.fossa_capacidade = this.local_instalacao.fossa_capacidade;
      this.local_instalacaoRow.acesso_camiao_flag = this.local_instalacao.acesso_camiao_flag;
      this.local_instalacaoRow.anexo_flag = this.local_instalacao.anexo_flag;
      this.local_instalacaoRow.anexo_quantidade = this.local_instalacao.anexo_quantidade;
      this.local_instalacaoRow.caixa_contador_flag = this.local_instalacao.caixa_contador_flag;
      this.local_instalacaoRow.estado_caixa_contador_id = this.local_instalacao.estado_caixa_contador_id;
      this.local_instalacaoRow.abastecimento_cil_id = this.local_instalacao.abastecimento_cil_id;

      //console.log(this.local_instalacaoRow);
      this.local_instalacaoRowValid = this.localInstalacaoRowValidation(this.local_instalacaoRow);
      if (this.local_instalacaoRowValid)
        this.local_instalacaosArray.unshift({ ...this.local_instalacaoRow });

    }

    await this.getRuaById(loadedStateCallback);

    //this.cleanLigacaoRamal();

  }

  private appendGeolocationValues() {
    /* 
        if(this.geolocation_flag) {        
          this.local_instalacaoRow.latitude = this.local_instalacao.latitude;
          this.local_instalacaoRow.longitude = this.local_instalacao.longitude;
        } */

    //console.log(this.local_instalacaoRow)


    this.local_instalacaoRowValid = this.localInstalacaoRowValidation(this.local_instalacaoRow);
    if (this.local_instalacaoRowValid)
      this.local_instalacaosArray.unshift({ ...this.local_instalacaoRow });

    if (this.geolocation_flag) {
      for (let i = 0; i < this.local_instalacaosArray.length; ++i) {
        if (this.local_instalacaosArray[i].rua_id == this.local_instalacaoRow.rua_id && this.local_instalacaosArray[i].moradia_numero == this.local_instalacaoRow.moradia_numero && this.local_instalacaosArray[i].is_predio == this.local_instalacaoRow.is_predio) {

          this.local_instalacaosArray[i].latitude = this.local_instalacaoRow.latitude;
          this.local_instalacaosArray[i].longitude = this.local_instalacaoRow.longitude;
        }
      }

      this.local_instalacaoRowValid = true;
    }

  }

  private getLocation() {


    /* if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.showPosition);
    } else {

      this.simpleFormcontagem.patchValue({
        latitude: null,
        longitude: null
      });
    } */
  }


  private deleteRowLocalInstalacaoAppended(row): void {

    for (let i = 0; i < this.local_instalacaosArray.length; ++i) {

      if ((this.local_instalacaosArray[i].rua_id == row.rua_id) && (this.local_instalacaosArray[i].moradia_numero == row.moradia_numero && this.local_instalacaosArray[i].is_predio == row.is_predio)) {
        this.local_instalacaosArray.splice(i, 1);
        //console.log(this.local_instalacaosArray[i]);
      }

    }


  }


  private localInstalacaoRowValidation(row): boolean {

    if (this.local_instalacaosArray.length == 0)
      return true;

    for (let i = 0; i < this.local_instalacaosArray.length; ++i) {
      //console.log(this.local_instalacaosArray[i]);
      if (this.local_instalacaosArray[i].rua_id == row.rua_id && this.local_instalacaosArray[i].moradia_numero == row.moradia_numero && this.local_instalacaosArray[i].is_predio == row.is_predio) {
        return false;
      }
    }

    return true;
  }


  private reloadLocalInstalacaos() {

    /*     this.http.call_get('local-instalacao/listagem/', null).subscribe(
          response => {
    
            this.local_instalacaos = Object(response).data;
          }
        ); */
  }

  private selectBoxProvincias() {

    this.local_instalacao.municipio_id = null;
    this.local_instalacao.distrito_id = null;
    this.local_instalacao.bairro_id = null;
    this.local_instalacao.quarteirao_id = null;
    this.local_instalacao.rua_id = null;

    this.http.call_get('provincia/selectBox', null).subscribe(
      response => {
        //console.log(Object(response).data);

        this.provincias = Object(response).data;
      }
    );
  }

  private setPredioValue() {
    this.predio_flag = !this.predio_flag;

    this.moradia_title = (this.predio_flag) ? "Porta" : "Moradia";
    //console.log(this.predio_flag);
  }

  private setGeolocationFlag() {
    this.geolocation_flag = !this.geolocation_flag;
  }

  private setGeolocationUpdate(flag: boolean) {
    this.geolocation_updated = flag;
    //console.log(this.geolocation_flag);
  }



  private async setGeolocationValues() {

    console.log("geo values");

    const loadedStateCallback = () => {
      //console.log(this.local_instalacao);

      this.local_instalacaoRow.is_predio = this.predio_flag;
      this.local_instalacaoRow.predio_nome = (this.predio_flag) ? this.local_instalacao.predio_nome : null;
      this.local_instalacaoRow.predio_andar = this.local_instalacao.predio_andar;

      this.local_instalacaoRow.cil = this.local_instalacao.cil;

      this.local_instalacaoRow.moradia_numero = this.local_instalacao.moradia_numero;
      this.local_instalacaoRow.user_id = this.currentUser.user.id;

      this.local_instalacaoRow.ligacao_ramal_id = this.local_instalacao.ligacao_ramal_id;
      this.local_instalacaoRow.tipo_objecto_id = this.local_instalacao.tipo_objecto_id;
      this.local_instalacaoRow.objecto_ligacao_id = this.local_instalacao.objecto_ligacao_id;
      this.local_instalacaoRow.diamentro = this.local_instalacao.diamentro;
      this.local_instalacaoRow.comprimento = this.local_instalacao.comprimento;
      this.local_instalacaoRow.profundidade = this.local_instalacao.profundidade;

      this.local_instalacaoRow.instalacao_sanitaria_qtd = this.local_instalacao.instalacao_sanitaria_qtd;
      this.local_instalacaoRow.reservatorio_flag = this.local_instalacao.reservatorio_flag;
      this.local_instalacaoRow.reservatorio_capacidade = this.local_instalacao.reservatorio_capacidade;
      this.local_instalacaoRow.piscina_flag = this.local_instalacao.piscina_flag;
      this.local_instalacaoRow.piscina_capacidade = this.local_instalacao.piscina_capacidade;
      this.local_instalacaoRow.jardim_flag = this.local_instalacao.jardim_flag;
      this.local_instalacaoRow.campo_jardim_id = this.local_instalacao.campo_jardim_id;
      this.local_instalacaoRow.poco_alternativo_flag = this.local_instalacao.poco_alternativo_flag;
      this.local_instalacaoRow.fossa_flag = this.local_instalacao.fossa_flag;
      this.local_instalacaoRow.fossa_capacidade = this.local_instalacao.fossa_capacidade;
      this.local_instalacaoRow.acesso_camiao_flag = this.local_instalacao.acesso_camiao_flag;
      this.local_instalacaoRow.anexo_flag = this.local_instalacao.anexo_flag;
      this.local_instalacaoRow.anexo_quantidade = this.local_instalacao.anexo_quantidade;
      this.local_instalacaoRow.caixa_contador_flag = this.local_instalacao.caixa_contador_flag;
      this.local_instalacaoRow.estado_caixa_contador_id = this.local_instalacao.estado_caixa_contador_id;
      this.local_instalacaoRow.abastecimento_cil_id = this.local_instalacao.abastecimento_cil_id;

      //console.log(this.local_instalacaoRow);

    }

    await this.getRuaById(loadedStateCallback);

    //console.log("modal data");
    console.log(this.local_instalacaoRow);

    /* 
        this.http.__call('local-instalacao/getCurrentGeolocation/' + this.local_instalacaoRow.rua_id, null).subscribe(
          response => {
            console.log(Object(response));
    
             this.lat = Object(response).results[0].geometry.location.lat;
            this.lng = Object(response).results[0].geometry.location.lng;
            //console.log(this.local_instalacaoRow);
          }
        ); */


    /*     const axios = require('axios');
        const places_apiKey = "AIzaSyDJ8uA-rm_2wI2r-oj_UEZIboOuIM0yzIY";
    
        const query = await this.local_instalacaoRow.provincia
          + "+" + this.local_instalacaoRow.municipio
          + ((this.local_instalacaoRow.distrito_id > 0) ? "+" + this.local_instalacaoRow.distrito : "");
    
        const uri = "https://maps.googleapis.com/maps/api/place/textsearch/json?query=" + query + "&key=" + places_apiKey; 
        console.log(uri) 
    
    
        axios.post(uri, {
          json: { todo: '' }
        }, (error, res, body) => {
          if (error) {
            console.error(error);
            //return null;
          }
          //console.log(`statusCode: ${res.statusCode}`)
          console.log(body);
    
          return body; 
        })
     */
    console.log("finished");

  }

  private async getAxios() {

    const query = "Luanda+Belas+Kilamba Kiaxe+golfo 2";

    this.http.__call('local-instalacao/getCurrentGeolocation/' + this.local_instalacao.rua_id, { query: query }).subscribe(
      response => {

        console.log(Object(response).results[0].geometry.location);

        this.lat = Object(response).results[0].geometry.location.lat;
        this.lng = Object(response).results[0].geometry.location.lng;
      }
    );

    //console.log("finished");

  }

  private getRuaById(callback: Function) {


    this.http.call_get('rua/getRuaById/' + this.local_instalacao.rua_id, null).subscribe(
      response => {

        console.log(Object(response));

        this.local_instalacaoRow.rua_id = Object(response).data.id;
        this.local_instalacaoRow.rua = Object(response).data.nome;

        this.local_instalacaoRow.bairro_id = Object(response).data.bairro_id;
        this.local_instalacaoRow.bairro = Object(response).data.bairro;
        this.local_instalacaoRow.quarteirao_id = Object(response).data.quarteirao_id;
        this.local_instalacaoRow.quarteirao = Object(response).data.quarteirao;
        this.local_instalacaoRow.has_quarteirao = Object(response).data.has_quarteirao;
        this.local_instalacaoRow.distrito_id = Object(response).data.distrito_id;
        this.local_instalacaoRow.distrito = Object(response).data.distrito;
        this.local_instalacaoRow.has_distrito = Object(response).data.has_distrito;
        this.local_instalacaoRow.municipio_id = Object(response).data.municipio_id;
        this.local_instalacaoRow.municipio = Object(response).data.municipio;
        this.local_instalacaoRow.provincia_id = Object(response).data.provincia_id;
        this.local_instalacaoRow.provincia = Object(response).data.provincia;

        if (!this.local_instalacaoRow.has_distrito) this.local_instalacaoRow.distrito_id = null;
        if (!this.local_instalacaoRow.has_quarteirao) this.local_instalacaoRow.quarteirao_id = null;

        callback();

        //console.log(this.local_instalacaoRow);
      }
    );

  }


  private selectBoxMunicipiosByProvincia() {

    //console.log(this.local_instalacao);

    this.local_instalacao.distrito_id = null;
    this.local_instalacao.bairro_id = null;
    this.local_instalacao.quarteirao_id = null;
    this.local_instalacao.rua_id = null;

    this.http.call_get('municipio/getMunicipiosByProvincia/' + this.local_instalacao.provincia_id, null).subscribe(
      response => {
        //console.log(Object(response));

        this.municipios = Object(response).data;

        //console.log(this.municipios);

      }
    );
  }

  private selectBoxDistritosByMunicipio() {

    this.local_instalacao.bairro_id = null;
    this.local_instalacao.quarteirao_id = null;
    this.local_instalacao.rua_id = null;

    for (let i = 0; i < this.municipios.length; ++i) {
      if (this.municipios[i].id == this.local_instalacao.municipio_id)
        this.local_instalacao.has_distrito = this.municipios[i].has_distrito;
    }

    console.log(this.local_instalacao);
    if (this.local_instalacao.has_distrito) {
      this.http.call_get('distrito/getDistritosByMunicipio/' + this.local_instalacao.municipio_id, null).subscribe(
        response => {
          console.log(Object(response).data);

          this.distritos = Object(response).data;
        }
      );
    }

    if (!this.local_instalacao.has_distrito) {
      this.selectBoxBairrosByMunicipio();
    }
  }


  private selectBoxQuarteiraosByBairro() {

    this.local_instalacao.quarteirao_id = null;
    this.local_instalacao.rua_id = null;

    for (let i = 0; i < this.bairros.length; ++i) {
      if (this.bairros[i].id == this.local_instalacao.bairro_id)
        this.local_instalacao.has_quarteirao = this.bairros[i].has_quarteirao;
    }

    //console.log(this.local_instalacao);
    if (this.local_instalacao.has_quarteirao) {
      this.http.call_get('quarteirao/getQuarteiraosByBairro/' + this.local_instalacao.bairro_id, null).subscribe(
        response => {
          //console.log(Object(response).data);

          this.quarteiraos = Object(response).data;
        }
      );
    }

    if (!this.local_instalacao.has_quarteirao) {
      this.selectBoxRuasByBairro();
    }

  }

  private selectBoxRuasByBairro() {

    //console.log(this.local_instalacao);

    this.http.call_get('rua/selectBoxByBairro/' + this.local_instalacao.bairro_id, null).subscribe(
      response => {
        //console.log(Object(response));

        this.ruas = Object(response);
      }
    );
  }

  private selectBoxRuasByQuarteirao() {

    //console.log(this.local_instalacao);

    this.http.call_get('rua/getRuasByQuarteirao/' + this.local_instalacao.quarteirao_id, null).subscribe(
      response => {
        //console.log(Object(response));

        this.ruas = Object(response).data;
      }
    );
  }

  private selectBoxTipoObjectos() {

    //console.log(this.local_instalacao);

    this.local_instalacao.ligacao_ramal_id = null;
    this.local_instalacao.objecto_ligacao_id = null;

    this.http.call_get('tipo-objecto-tecnico/selectBox', null).subscribe(
      response => {
        console.log(Object(response));

        this.tipo_objectos = Object(response).data;
      }
    );
  }

  private selectBoxObjectoLigacao() {

    //console.log(this.local_instalacao);

    this.local_instalacao.ligacao_ramal_id = null;

    this.http.call_get('objecto-ligacao-ramal/selectBoxByTipoLigacao/' + this.local_instalacao.tipo_objecto_id, null).subscribe(
      response => {
        console.log(Object(response));

        this.objectos_ramais = Object(response);
      }
    );

    this.getLigacaoRamals();
  }


  private selectBoxByObjectoLigacao() {

    //console.log(this.local_instalacao);

    this.http.call_get('objecto-ligacao-ramal/selectBoxByObjectoLigacao/' + this.local_instalacao.tipo_objecto_id, null).subscribe(
      response => {
        //console.log(Object(response));

        this.objectos_ramais = Object(response);
      }
    );
  }

  private selectBoxBairrosByMunicipio() {
    //console.log(this.local_instalacao);

    this.http.call_get('bairro/selectBoxByMunicipio/' + this.local_instalacao.municipio_id, null).subscribe(
      response => {
        console.log(Object(response));

        this.bairros = Object(response);
      }
    );
  }


  private getEstadoCaixaContadors() {
    this.http.call_get('estado-caixa-contador/selectBox', null).subscribe(
      response => {
        this.estado_caixa_contadors = Object(response);
      }
    );
    console.log(this.estado_caixa_contadors);

  }

  private getObjectosContratos() {

    this.http.call_get('objecto-contrato/selectBox', null).subscribe(
      response => {
        this.objecto_contratos = Object(response);
      }
    );



    console.log(this.objecto_contratos);
  }

  private getCampoJardims() {
    this.http.call_get('campo-jardim/selectBox', null).subscribe(
      response => {
        this.campo_jardims = Object(response);
      }
    );
    console.log(this.campo_jardims);

  }

  private getAbastecimentoCILs() {

    this.http.call_get('abastecimento-cil/selectBox', null).subscribe(
      response => {
        this.abastecimento_cils = Object(response);
      }
    );

  }

  private loadCIL() {

    let cil = null;

    for (let index = 0; index < this.quarteiraos.length; index++) {
      
      if(this.quarteiraos[index].id == this.local_instalacao.quarteirao_id) {
        this.local_instalacao.cil = this.quarteiraos[index].nome + '-000-C' + this.local_instalacao.moradia_numero
      }
      
    }
  }

  private openEndLocalInstalacaoModal(flag: boolean): void {
    this.local_instalacaoModal = flag;
  }

  public loadListEstabelecimentos(local_instalacaos) {
    this.local_instalacaos = local_instalacaos;
  }


  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }


  mapClicked($event: AgmCoreModule) {

    console.log($event["coords"].lat);

    this.local_instalacaoRow.latitude = $event["coords"].lat;
    this.local_instalacaoRow.longitude = $event["coords"].lng;

    this.markers.length = 0;
    this.markers.push({
      lat: $event["coords"].lat,
      lng: $event["coords"].lng,
      draggable: true
    });
  }

  markerDragEnd(m: marker, $event: AgmCoreModule) {
    console.log('dragEnd', m, $event);
  }

  markers: marker[] = [
    {
      lat: null,
      lng: null,
      draggable: false
    }
  ]


}


interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}