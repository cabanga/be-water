import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalInstalacaoComponent } from './local-instalacao.component';

describe('LocalInstalacaoComponent', () => {
  let component: LocalInstalacaoComponent;
  let fixture: ComponentFixture<LocalInstalacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalInstalacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalInstalacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
