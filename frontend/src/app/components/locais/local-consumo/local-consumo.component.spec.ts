import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalConsumoComponent } from './local-consumo.component';

describe('LocalConsumoComponent', () => {
  let component: LocalConsumoComponent;
  let fixture: ComponentFixture<LocalConsumoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalConsumoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalConsumoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
