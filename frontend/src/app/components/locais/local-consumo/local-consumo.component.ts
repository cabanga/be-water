import { Component, OnInit, Input, createPlatformFactory } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { ExcelService } from 'src/app/services/excel.service';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-local-consumo',
  templateUrl: './local-consumo.component.html',
  styleUrls: ['./local-consumo.component.css']
})
export class LocalConsumoComponent implements OnInit {

  private local_consumo = {
    id: null,
    predio_nome: null,
    is_active: null,

    has_distrito: null,
    rua_id: null,
    bairro_id: null,
    distrito_id: null,
    municipio_id: null,
    provincia_id: 29,
    user_id: null,
    //local_consumoModal: false
  };

  @Input() ruas: any [];
  @Input() bairros: any [];
  @Input() distritos: any [];
  @Input() municipios: any = [];

  private addRows: boolean = true;
  private title: string = null;

  private items: any = [];
  private local_consumos: any = [];

  constructor(private http: HttpService, private configService: ConfigService, private excelService: ExcelService) {
  }



  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "lista_local_consumos -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }

  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

  ngOnInit() {
    this.getPageFilterData(1);
  }

  private getDadosModal() {

  }

  private getLocalConsumos() {

    this.configService.loaddinStarter('start');

    this.http.__call('local-consumo/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  private initLocalConsumo() {

    this.local_consumo.id = null;
    this.local_consumo.predio_nome = null;
    this.local_consumo.rua_id = null;

    this.local_consumo.bairro_id = null;
    this.local_consumo.has_distrito = null;
    this.local_consumo.distrito_id = null;
    this.local_consumo.municipio_id = null;
    this.local_consumo.provincia_id = null;
    this.local_consumo.is_active = null;
    this.local_consumo.user_id = null;

    this.distritos = null;
    this.municipios = null;
    this.bairros = null;
    this.ruas = null;

    this.addRows = true;
  }

  private setDataLocalConsumo(item) {

    if (item !== undefined) {
      this.title = "Editar LocalConsumo";

      this.local_consumo.id = item.id;
      this.local_consumo.predio_nome = item.predio_nome;
      this.local_consumo.rua_id = item.rua_id;

      this.local_consumo.bairro_id = item.bairro_id;      
      this.local_consumo.has_distrito = (item.distrito_id > 0) ? true : false;
      this.local_consumo.distrito_id = item.distrito_id;
      this.local_consumo.municipio_id = item.municipio_id;
      this.local_consumo.provincia_id = item.provincia_id;
      this.local_consumo.is_active = item.is_active;
      this.local_consumo.user_id = item.user_id;

      if (this.local_consumo.has_distrito) this.selectBoxDistritosByMunicipio(item.municipio_id);
      
      this.selectBoxMunicipiosByProvincia(item.provincia_id);
      this.selectBoxBairrosByMunicipio(item.municipio_id);
      this.selectBoxRuasByBairro(item.bairro_id);

      this.addRows = false;

      //console.log(this.local_consumo);
    }


  }

  private updateStateLocalConsumo(item) {

    this.local_consumo.id = item.id;
    this.local_consumo.predio_nome = item.predio_nome;

    this.local_consumo.rua_id = item.rua_id;
    this.local_consumo.bairro_id = item.bairro_id;
    this.local_consumo.has_distrito = item.has_distrito;
    this.local_consumo.distrito_id = item.distrito_id;
    this.local_consumo.municipio_id = item.municipio_id;
    this.local_consumo.provincia_id = item.provincia_id;
    this.local_consumo.is_active = !item.is_active;
    this.local_consumo.user_id = item.user_id;

      //console.log(item);
    this.http.__call('local-consumo/update/' + this.local_consumo.id, this.local_consumo).subscribe(
      response => {

        if (Object(response).code == 200) {
          var update = (this.local_consumo.is_active == true) ? "Activado" : "Desactivado";

          this.configService.showAlert("Local de Consumo " + this.local_consumo.predio_nome + " foi " + update, "alert-success", true);
        }

      }
    );

    for (let i = 0; i < this.items.length; ++i) {
      if (this.items[i].id == this.local_consumo.id) {
        this.items[i].is_active = this.local_consumo.is_active;
      }
    }
  }


  private selectBoxRuasByBairro(id) {

    //console.log(this.local_consumo);

    this.http.call_get('rua/getRuasByBairro/' + id, null).subscribe(
      response => {
        //console.log(Object(response).data);

        this.ruas = Object(response).data;
      }
    );
  }
  
  private selectBoxDistritosByMunicipio(id) {

    //console.log(this.local_consumo);

    this.http.call_get('distrito/getDistritosByMunicipio/' + id, null).subscribe(
      response => {
        console.log(Object(response).data);

        this.distritos = Object(response).data;
      }
    );
  }

  private selectBoxBairrosByMunicipio(id) {

    //console.log(this.local_consumo);

    this.http.call_get('bairro/getBairrosByMunicipio/' + id, null).subscribe(
      response => {
        //console.log(Object(response).data);

        this.bairros = Object(response).data;
      }
    );
  }

  private selectBoxMunicipiosByProvincia(id) {

    //console.log(this.local_consumo);

    this.http.call_get('municipio/getMunicipiosByProvincia/' + id, null).subscribe(
      response => {
        //console.log(Object(response).data);

        this.municipios = Object(response).data;
      }
    );
  }


  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.getLocalConsumos();
  }

  private getLocalConsumosByLocalConsumo(id) {

    this.configService.loaddinStarter('start');

    this.http.call_get('local-consumo/getResidenciasByResidencia/' + id, this.http.filters).subscribe(

      response => {

        this.local_consumos = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

}
