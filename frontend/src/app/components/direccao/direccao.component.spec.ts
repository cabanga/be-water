import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DireccaoComponent } from './direccao.component';

describe('DireccaoComponent', () => {
  let component: DireccaoComponent;
  let fixture: ComponentFixture<DireccaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DireccaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DireccaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
