import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditContaBancariaComponent } from './create-or-edit-conta-bancaria.component';

describe('CreateOrEditContaBancariaComponent', () => {
  let component: CreateOrEditContaBancariaComponent;
  let fixture: ComponentFixture<CreateOrEditContaBancariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditContaBancariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditContaBancariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
