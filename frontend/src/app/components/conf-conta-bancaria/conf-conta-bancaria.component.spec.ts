import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfContaBancariaComponent } from './conf-conta-bancaria.component';

describe('ConfContaBancariaComponent', () => {
  let component: ConfContaBancariaComponent;
  let fixture: ComponentFixture<ConfContaBancariaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfContaBancariaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfContaBancariaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
