import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoLigacaoComponent } from './conf-tipo-ligacao.component';

describe('ConfTipoLigacaoComponent', () => {
  let component: ConfTipoLigacaoComponent;
  let fixture: ComponentFixture<ConfTipoLigacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoLigacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoLigacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
