import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetPdfEmailComponent } from './get-pdf-email.component';

describe('GetPdfEmailComponent', () => {
  let component: GetPdfEmailComponent;
  let fixture: ComponentFixture<GetPdfEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetPdfEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetPdfEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
