import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfObjectoContratoComponent } from './conf-objecto-contrato.component';

describe('ConfObjectoContratoComponent', () => {
  let component: ConfObjectoContratoComponent;
  let fixture: ComponentFixture<ConfObjectoContratoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfObjectoContratoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfObjectoContratoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
