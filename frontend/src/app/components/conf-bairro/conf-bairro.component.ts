import { Component, OnInit, Input, createPlatformFactory } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { ExcelService } from 'src/app/services/excel.service';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { ConfigModuloService } from 'src/app/services/config-modulo.service';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-conf-bairro',
  templateUrl: './conf-bairro.component.html',
  styleUrls: ['./conf-bairro.component.css']
})
export class ConfBairroComponent implements OnInit {

  @Input() simpleFormBairro: FormGroup;
  private distrito_view: boolean = false;

  public currentUser: any;

  private bairro = {
    id: null,
    nome: null,
    is_active: null,
    has_distrito: null,
    distrito_id: null,
    municipio_id: null,
    provincia_id: null,
    user_id: null,
    //bairroModal: false
  };

  @Input() distritos: any[];
  @Input() municipios: any = [];
  @Input() provincias: any = [];

  private addRows: boolean = true;
  private title: string = null;

  private items: any = [];
  private bairros: any = [];
  /*  private estados: any = []; */

  constructor(
    private auth: AuthService,
    private http: HttpService,
    private configService: ConfigService,
    private excelService: ExcelService,
    private config: ConfigModuloService
  ) {
    
    this.currentUser = this.auth.currentUserValue;

  }

  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "lista_bairros -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

  ngOnInit() {
    this.getPageFilterData(1);

    this.getConfiguracaos();
  }

  private getDadosModal() {

  }

  private getBairros() {

    this.configService.loaddinStarter('start');

    this.http.__call('bairro/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  private initBairro() {

    //console.log("init");

    this.bairro.id = null;
    this.bairro.nome = null;
    this.bairro.has_distrito = null;
    this.bairro.distrito_id = null;
    this.bairro.municipio_id = null;
    this.bairro.provincia_id = null;
    this.bairro.is_active = null;
    this.bairro.user_id = null;

    this.distritos = null;
    this.municipios = null;
    this.provincias = null;

    this.addRows = true;

    console.log(this.bairro);

  }


  private setDataBairro(item) {

    if (item !== undefined) {
      this.title = "Editar Bairro";

      this.bairro.id = item.id;
      this.bairro.nome = item.nome;
      this.bairro.has_distrito = (item.distrito_id > 0) ? true : false;
      this.bairro.distrito_id = item.distrito_id;
      this.bairro.municipio_id = item.municipio_id;
      this.bairro.provincia_id = item.provincia_id;
      this.bairro.is_active = item.is_active;
      this.bairro.user_id = item.user_id;

      if (this.bairro.has_distrito) this.selectBoxDistritosByMunicipio(item.municipio_id);

      this.selectBoxMunicipiosByProvincia(item.provincia_id);

      this.addRows = false;

      //console.log(this.bairro);
    }


  }

  private updateStateBairro(item) {

    this.bairro.id = item.id;
    this.bairro.nome = item.nome;
    this.bairro.has_distrito = item.has_distrito;
    this.bairro.distrito_id = item.distrito_id;
    this.bairro.municipio_id = item.municipio_id;
    this.bairro.provincia_id = item.provincia_id;
    this.bairro.is_active = !item.is_active;
    this.bairro.user_id = item.user_id;

    //console.log(item);
    this.http.__call('bairro/update/' + this.bairro.id, this.bairro).subscribe(
      response => {

        if (Object(response).code == 200) {
          var update = (this.bairro.is_active == true) ? "Activado" : "Desactivado";

          this.configService.showAlert("Bairro " + this.bairro.nome + " foi " + update, "alert-success", true);
        }

      }
    );

    for (let i = 0; i < this.items.length; ++i) {
      if (this.items[i].id == this.bairro.id) {
        this.items[i].is_active = this.bairro.is_active;
      }
    }
  }


  private selectBoxDistritosByMunicipio(id) {

    //console.log(this.bairro);

    this.http.call_get('distrito/getDistritosByMunicipio/' + id, null).subscribe(
      response => {
        console.log(Object(response).data);

        this.distritos = Object(response).data;
      }
    );
  }

  /* private selectBoxProvincias() {
    this.http.call_get('provincia/selectBox', null).subscribe(
      response => {

        this.provincias = Object(response).data;
      }
    );
  } */


  private selectBoxMunicipiosByProvincia(id) {

    //console.log(this.bairro);

    this.http.call_get('municipio/getMunicipiosByProvincia/' + id, null).subscribe(
      response => {
        //console.log(Object(response).data);

        this.municipios = Object(response).data;
      }
    );
  }


  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.getBairros();
  }

  private getRuasByBairro(id) {

    this.configService.loaddinStarter('start');

    this.http.call_get('bairro/getRuasByBairro/' + id, this.http.filters).subscribe(

      response => {

        this.bairros = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  public getConfiguracaos() {

    let result = null;

    const slugs = [
      this.config.provincia_default,
      this.config.distrito_view
    ];
    //console.log(slugs);

    for (let index = 0; index < slugs.length; index++) {

      //console.log(slugs[index]);
      this.http.__call('configuracao/getConfiguracaobySlug/' + slugs[index], null).subscribe(
        response => {

          //console.log(Object(response));

          if (Object(response).code != 200) {
            //this.config.saveConfig(slugs[index], this.config.modulo.CONFIGURACOES, null);
            result = null;
          }
          else {

            result = Object(response).data;

            if (slugs[index] == this.config.provincia_default) {
              this.bairro.provincia_id = result.valor;
              this.selectBoxMunicipiosByProvincia(result.valor);

              //console.log(this.bairro.provincia_id);
            }

            if (slugs[index] == this.config.distrito_view) this.distrito_view = Boolean(result.valor);
          }
        });
    }
  }



}
