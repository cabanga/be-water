import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditBairroComponent } from './create-or-edit-bairro.component';

describe('CreateOrEditBairroComponent', () => {
  let component: CreateOrEditBairroComponent;
  let fixture: ComponentFixture<CreateOrEditBairroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditBairroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditBairroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
