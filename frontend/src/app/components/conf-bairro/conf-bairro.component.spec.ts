import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfBairroComponent } from './conf-bairro.component';

describe('ConfBairroComponent', () => {
  let component: ConfBairroComponent;
  let fixture: ComponentFixture<ConfBairroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfBairroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfBairroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
