import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTipoMensagemComponent } from './conf-tipo-mensagem.component';

describe('ConfTipoMensagemComponent', () => {
  let component: ConfTipoMensagemComponent;
  let fixture: ComponentFixture<ConfTipoMensagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTipoMensagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTipoMensagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
