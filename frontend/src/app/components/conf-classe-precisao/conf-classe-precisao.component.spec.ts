import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfClassePrecisaoComponent } from './conf-classe-precisao.component';

describe('ConfClassePrecisaoComponent', () => {
  let component: ConfClassePrecisaoComponent;
  let fixture: ComponentFixture<ConfClassePrecisaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfClassePrecisaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfClassePrecisaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
