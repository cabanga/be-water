import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LigacaoRamalComponent } from './ligacao-ramal.component';

describe('LigacaoRamalComponent', () => {
  let component: LigacaoRamalComponent;
  let fixture: ComponentFixture<LigacaoRamalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LigacaoRamalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LigacaoRamalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
