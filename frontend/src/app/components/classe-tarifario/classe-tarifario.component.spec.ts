import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasseTarifarioComponent } from './classe-tarifario.component';

describe('ClasseTarifarioComponent', () => {
  let component: ClasseTarifarioComponent;
  let fixture: ComponentFixture<ClasseTarifarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClasseTarifarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClasseTarifarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
