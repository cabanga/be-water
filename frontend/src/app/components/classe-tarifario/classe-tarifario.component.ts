import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

@Component({
  selector: 'app-classe-tarifario',
  templateUrl: './classe-tarifario.component.html',
  styleUrls: ['./classe-tarifario.component.css']
})
export class ClasseTarifarioComponent implements OnInit {

  private classeTarifario = {
    id: null,
    //valor: null,
    tarifa_variavel: null,
    tarifa_fixa_mensal: null,
    consumo_minimo: null,
    consumo_maximo: null,
    tarifario: null,
    descricao: null,
    //produto_id: null,
    nome: null,
    tarifario_id: null
  }


  private items: any = [];
  private tarifarios: any = [];
  private produtos: any = [];

  constructor(private http: HttpService, private configService: ConfigService) { }

  ngOnInit() {
    this.getPageFilterData(1);
    // this.getTarifarios()

  }


  private listaclasseTarifario() {

    this.configService.loaddinStarter('start');

    this.http.__call('classe-tarifario/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.listaclasseTarifario();
  }


  private register(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (this.classeTarifario.tarifario_id == null) {
      this.configService.showAlert("É obrigatório selecionar Tarifário", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else if (this.classeTarifario.tarifa_variavel == null) {
      this.configService.showAlert("O valor dat tarifa variável não pode ser nulo", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else if (this.classeTarifario.tarifa_fixa_mensal == null) {
      this.configService.showAlert("O valor dat tarifa fixa mensal não pode ser nulo", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else if (this.classeTarifario.consumo_maximo <= this.classeTarifario.consumo_minimo) {
      this.configService.showAlert("Consumo Mínimo não pode ser maior ou igual que o Consumo Máximo", 'alert-danger', true);
      this.configService.loaddinStarter('stop');
    } else {
      this.http.__call('classe-tarifario/create', this.classeTarifario).subscribe(
        res => {
          if (Object(res).code == 201) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
            this.configService.loaddinStarter('stop');
          } else {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.clearFormInputs(e);
            this.listaclasseTarifario()
            this.configService.loaddinStarter('stop');
          }
        }
      )
    }

  }

  private clearFormInputs(e) {
    e.target.elements[0].value = null;
    e.target.elements[1].value = null;
    e.target.elements[2].value = null;
    e.target.elements[3].value = null;
    e.target.elements[4].value = null;
    e.target.elements[5].value = null;
  }

  private refresh(id, descricao, valor, consumo_minimo, consumo_maximo, tarifario_id) {
    this.classeTarifario.id = id;
    this.classeTarifario.descricao = descricao;
    //this.classeTarifario.valor = valor;
    this.classeTarifario.consumo_minimo = consumo_minimo;
    this.classeTarifario.consumo_maximo = consumo_maximo;
    this.classeTarifario.tarifario_id = tarifario_id
  }

  private getTarifarios() {
    this.http.call_get('classe-tarifario/selectBox', null).subscribe(
      response => {
        this.tarifarios = Object(response).data
        /* console.log(this.tarifarios) */
      }

    );
  }



  private editar(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    this.http.__call('classe-tarifario/update/' + this.classeTarifario.id, this.classeTarifario).subscribe(
      res => {
        if (Object(res).code == 500) {
          this.configService.showAlert(Object(res).message, 'alert-danger', true);
        } else {
          //this.configService.clearFormInputs(e);
          this.configService.showAlert(Object(res).message, 'alert-success', true);
          this.listaclasseTarifario();

        }
      }
    );

    this.configService.loaddinStarter('stop');
  }

  private loadingData = {
    produto: "Carregando..."
  }

  private getProdutos() {
    this.loadingData.produto = "Carregando..."
    this.http.call_get('artigo/selectProdutos', null).subscribe(
      response => {
        this.produtos = Object(response).data
        this.loadingData.produto = "Selecione o Produto"
      }
    );
  }

  private ini() {
    this.getTarifarios();
    this.getProdutos();
      this.classeTarifario.id = null;
      //this.classeTarifario.valor = null;
      this.classeTarifario.consumo_minimo = null;
      this.classeTarifario.consumo_maximo = null;
      this.classeTarifario.descricao = null;
      this.classeTarifario.tarifario_id = null;
      //this.classeTarifario.produto_id = null;
      this.classeTarifario.tarifario = null;
      this.classeTarifario.nome = null;
  }





}
