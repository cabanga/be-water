import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfModeloComponent } from './conf-modelo.component';

describe('ConfModeloComponent', () => {
  let component: ConfModeloComponent;
  let fixture: ComponentFixture<ConfModeloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfModeloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfModeloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
