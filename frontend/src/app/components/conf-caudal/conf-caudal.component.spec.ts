import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfCaudalComponent } from './conf-caudal.component';

describe('ConfCaudalComponent', () => {
  let component: ConfCaudalComponent;
  let fixture: ComponentFixture<ConfCaudalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfCaudalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfCaudalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
