import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditTarifarioComponent } from './create-or-edit-tarifario.component';

describe('CreateOrEditTarifarioComponent', () => {
  let component: CreateOrEditTarifarioComponent;
  let fixture: ComponentFixture<CreateOrEditTarifarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditTarifarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditTarifarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
