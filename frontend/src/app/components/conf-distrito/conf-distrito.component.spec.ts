import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfDistritoComponent } from './conf-distrito.component';

describe('ConfDistritoComponent', () => {
  let component: ConfDistritoComponent;
  let fixture: ComponentFixture<ConfDistritoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfDistritoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfDistritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
