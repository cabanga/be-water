import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditDistritoComponent } from './create-or-edit-distrito.component';

describe('CreateOrEditDistritoComponent', () => {
  let component: CreateOrEditDistritoComponent;
  let fixture: ComponentFixture<CreateOrEditDistritoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditDistritoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditDistritoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
