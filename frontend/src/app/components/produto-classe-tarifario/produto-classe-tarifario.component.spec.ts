import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdutoClasseTarifarioComponent } from './produto-classe-tarifario.component';

describe('ProdutoClasseTarifarioComponent', () => {
  let component: ProdutoClasseTarifarioComponent;
  let fixture: ComponentFixture<ProdutoClasseTarifarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdutoClasseTarifarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdutoClasseTarifarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
