import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfFabricanteComponent } from './conf-fabricante.component';

describe('ConfFabricanteComponent', () => {
  let component: ConfFabricanteComponent;
  let fixture: ComponentFixture<ConfFabricanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfFabricanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfFabricanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
