import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfMarcaComponent } from './conf-marca.component';

describe('ConfMarcaComponent', () => {
  let component: ConfMarcaComponent;
  let fixture: ComponentFixture<ConfMarcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfMarcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfMarcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
