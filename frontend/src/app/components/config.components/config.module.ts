import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser"
import { NgModule } from "@angular/core"
import { HttpClientModule } from "@angular/common/http"
import { ReactiveFormsModule, FormsModule } from "@angular/forms"
import { ConfigRoutingModule } from './config.routing.module'

@NgModule({
    declarations: [
    ],
    imports: [
        BrowserModule,
        ConfigRoutingModule,
        HttpClientModule,
        FormsModule,
        CommonModule,
        ReactiveFormsModule
    ],
    exports: [
    ],
    providers: [
    ]
})
export class AppModule {}
