import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

const routes: Routes = []

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        CommonModule,
        ReactiveFormsModule
    ],
    exports: [RouterModule]
})

export class ConfigRoutingModule {
}