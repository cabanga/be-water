import { Component, OnInit, Input, createPlatformFactory } from '@angular/core';
import { GeneroService } from './genero.service';

@Component({
    selector: 'app-genero',
    templateUrl: './genero.component.html'
})

export class GeneroComponent implements OnInit {

    genero: any = {
        abreviatura: null,
        descricao: null
    }

    generos: any = []

    constructor(
        private _generoService: GeneroService
    ){
        this._loadingDependences()
    }

    ngOnInit() {

    }

    _save(){
        this._generoService.CreateGenre( this.genero )
        .subscribe( response => {
            this._loadingDependences()
        }),
        (error) => {
            if (!error.ok) {
            }
        }
    }

    _loadingDependences(){
        this._generoService.getGenres()
        .subscribe((response) => {
            this.generos = response.data
        })
    }
}
