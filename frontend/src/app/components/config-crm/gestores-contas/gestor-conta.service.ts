import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { environment } from 'src/environments/environment'

@Injectable({
    providedIn: 'root'
})

export class GestorContaService {

    private token = localStorage.getItem('sessionToken')

    private headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Authorization', `Bearer ${this.token}`)

    constructor(
        private _http_client: HttpClient
    ){}

    getGestoresContas() {
        return this._http_client.get<any>(`${environment.app_url}api/${environment.apiVersion}/gestores_contas`, { 'headers': this.headers })
    }

    CreateGestorConta(gestor: any) {
        return this._http_client.post<any>(`${environment.app_url}api/${environment.apiVersion}/gestores_contas`, gestor, { 'headers': this.headers })
    }

    UpdateGestorConta(gestor: any) {
        return this._http_client.patch<any>(`${environment.app_url}api/${environment.apiVersion}/gestores_contas/${gestor.id}`, gestor, { 'headers': this.headers })
    }
}
