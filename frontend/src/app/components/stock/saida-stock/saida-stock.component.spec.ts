import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaidaStockComponent } from './saida-stock.component';

describe('SaidaStockComponent', () => {
  let component: SaidaStockComponent;
  let fixture: ComponentFixture<SaidaStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaidaStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaidaStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
