import { Component, OnInit, Input } from "@angular/core";
import { HttpService } from "src/app/providers/http/http.service";
import { ConfigService } from "src/app/providers/config/config.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "src/app/providers/auth/auth.service";

@Component({
  selector: 'app-entrada-stock',
  templateUrl: './entrada-stock.component.html',
  styleUrls: ['./entrada-stock.component.css']
})
export class EntradaStockComponent implements OnInit {

  submitted = false;
  simpleForm: FormGroup;
  loading = false;
  validar = false;

  artigos: any = [];

  constructor(
    public http: HttpService,
    public configService: ConfigService,
    public formBuilder: FormBuilder,
    public auth: AuthService
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.getProdutos();
    this.getArmazens();
  }

  createForm() {
    this.simpleForm = this.formBuilder.group({
      artigo_id: [null],
      armazem_id: [null, Validators.required],
      qtd_recebida: [null],
      artigos: [null, Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.simpleForm.controls;
  }

  onReset() {
    this.submitted = false;
    this.loading = false;
    this.simpleForm.reset();
    this.artigos = [];
    this.artigosSelecionados = [];
    this.simpleForm.patchValue({
      artigo_id: null,
      qtd_recebida: null,
    });
  }

  onSubmit() {
    this.validar = false;
    this.submitted = true;
    this.simpleForm.patchValue({
      artigos: this.artigosSelecionados,
    });

    // parar aquei se o simpleFormulário for inválido
    if (this.simpleForm.invalid) {
      return;
    }

    const uri = "stockMovimento/nova_entrada";
    this.createOrEdit(uri, this.simpleForm, true);
  }

  createOrEdit(uri: any, simpleFormulario: FormGroup, isCreate: boolean) {
    this.loading = true;
    // TODO: usado para fazer a requisição com a api de criação de organismo
    this.http.__call(uri, simpleFormulario.value).subscribe((res) => {
      if (Object(res).code == 200) {
        this.submitted = false;
        this.getProdutos();
        this.getArmazens();
        this.onReset();
      } else {
        this.configService.showAlert(Object(res).message, "alert-danger", true);
      }
      this.loading = false;
    });
  }

  public armazens = [];
  public getArmazens() {
    this.loading = true;
    this.http.__call("armazem/selectBox", null).subscribe((response) => {
      this.armazens = Object(response).data;
      this.loading = false;
    });
  }

  public artigosSelecionados = [];

  public adicionarArtigos() {
    var validar = 0;
    var nome_artigo = "";

    this.artigos.forEach((element) => {
      if (element.id == this.simpleForm.getRawValue().artigo_id) {
        nome_artigo = element.descricao;
      }
    });
    var artigo = {
      nome_artigo: nome_artigo,
      artigo_id: this.simpleForm.getRawValue().artigo_id,
      valor: 0,
      total: 0,
      quantidade: this.simpleForm.getRawValue().qtd_recebida,
    };

    if (this.artigosSelecionados.length >= 1) {
      for (let index = 0; index < this.artigosSelecionados.length; index++) {
        const l = this.artigosSelecionados[index];
        if (l.artigo_id == artigo.artigo_id) {
          this.artigosSelecionados.splice(index, 1);
          this.artigosSelecionados.splice(index, 0, artigo);
          validar = 1;
        }
      }
      if (validar == 0) {
        this.artigosSelecionados.push(artigo);
      }
    } else {
      this.artigosSelecionados.push(artigo);
    }

    this.simpleForm.patchValue({
      artigo_id: null,
      qtd_recebida: null,
    });
  }

  deleteRow(artigo: any) {
    for (let i = 0; i < this.artigosSelecionados.length; ++i) {
      if (this.artigosSelecionados[i].artigo_id === artigo.artigo_id) {
        this.artigosSelecionados.splice(i, 1);
      }
    }
  }

  getProdutos() {
    this.loading = true;
    this.http
      .__call("artigos/prod/selectBox", { pagination: { page: null } })
      .subscribe((res: any) => {
        this.artigos = Object(res).data;
        this.loading = false;
      });
  }


}
