import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntradaStockComponent } from './entrada-stock.component';

describe('EntradaStockComponent', () => {
  let component: EntradaStockComponent;
  let fixture: ComponentFixture<EntradaStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntradaStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntradaStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
