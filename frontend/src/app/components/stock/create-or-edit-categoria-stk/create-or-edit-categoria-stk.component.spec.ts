import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditCategoriaStkComponent } from './create-or-edit-categoria-stk.component';

describe('CreateOrEditCategoriaStkComponent', () => {
  let component: CreateOrEditCategoriaStkComponent;
  let fixture: ComponentFixture<CreateOrEditCategoriaStkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditCategoriaStkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditCategoriaStkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
