import { Component,ViewEncapsulation, OnInit, Input,Output, EventEmitter,OnChanges, SimpleChange } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms'; 
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-create-or-edit-categoria-stk',
  templateUrl: './create-or-edit-categoria-stk.component.html',
  styleUrls: ['./create-or-edit-categoria-stk.component.css']
})
export class CreateOrEditCategoriaStkComponent implements OnInit {




  @Input() modal: string = "modalcategoriaProdutoCreateOrEdit";
  @Input() title: string = "Registar categoria";
  @Input() categorias:any;

  submitted = false;
  formErrors: any;
  private loading: boolean = false;
  
  @Input() simpleFormcategorias: FormGroup; 

  @Output() private loadListcategorias = new EventEmitter<any>();
  

  constructor(private http: HttpService, private configService: ConfigService, private formBuilder: FormBuilder) { 
    this.createForm();
  }

  ngOnInit() {
    
  }

  createForm() {
    this.simpleFormcategorias = this.formBuilder.group({
      index: [{ value: null, disabled: true }],
      descricao: [null, Validators.required],
    })
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.simpleFormcategorias.controls;
  }

  onReset() {
    this.submitted = false;
    this.simpleFormcategorias.reset();
  }


  onSubmit() {

    this.submitted = true;

    // parar aquei se o formulário for inválido
    if (this.simpleFormcategorias.invalid) {
      return;
    }
    this.loading = true;
    const index = this.simpleFormcategorias.getRawValue().index;
    // TODO: usado para fazer a requisição com a api de criação de objsct or update
    const uri = (index === null ? 'categoria-produto/register' : 'categoria-produto/update/' + index);
    this.createOrEdit(uri, this.simpleFormcategorias, (index === null ? true : false));

  }

  createOrEdit(uri: any, formulario: FormGroup, isCreate: boolean) {

    // TODO: usado para fazer a requisição com a api de criação de object
    this.http.__call(uri, formulario.value).pipe(first()).subscribe(
      response => {
        this.submitted = false;
        this.loading = false;
        if (isCreate && Object(response).code ==200) {
          formulario.reset(); 
        }  
        
        if (Object(response).code ==200) {
          this.loadList_categorias(Object(response).data);
        }
      },
      error => {
        this.submitted = false;
        this.loading = false;
      });
  }


  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
     
    if (this.categorias !== undefined ) {
      
      this.title = "Editar categoria";
      //this.onReset()
      
      this.simpleFormcategorias.patchValue({
        index: this.categorias.id,
        descricao: this.categorias.descricao
      });
      
    } else {
      this.onReset()
      this.title = "Registar categoria";
      
    }
  }

  public loadList_categorias(categorias) { 
    this.loadListcategorias.emit(categorias);
  }



}
