import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditArmazemComponent } from './create-or-edit-armazem.component';

describe('CreateOrEditArmazemComponent', () => {
  let component: CreateOrEditArmazemComponent;
  let fixture: ComponentFixture<CreateOrEditArmazemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditArmazemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditArmazemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
