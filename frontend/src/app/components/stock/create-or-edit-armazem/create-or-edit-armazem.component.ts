import { Component,ViewEncapsulation, OnInit, Input,Output, EventEmitter,OnChanges, SimpleChange } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms'; 
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-create-or-edit-armazem',
  templateUrl: './create-or-edit-armazem.component.html',
  styleUrls: ['./create-or-edit-armazem.component.css']
})
export class CreateOrEditArmazemComponent implements OnInit {

  @Input() modal: string = "modalarmazemCreateOrEdit";
  @Input() title: string = "Registar armazem";
  @Input() armazem:any;

  submitted = false;
  formErrors: any;
  private loading: boolean = false;
  private ver: boolean = true;
  @Input() simpleFormArmazem: FormGroup; 

  @Output() private loadListarmazem = new EventEmitter<any>();
  
  private empresas: any = [];
  private lojas: any = [];
  private roles: any = [];

  constructor(private http: HttpService, private configService: ConfigService, private formBuilder: FormBuilder) { 
    this.createForm();
  }

  ngOnInit() {
    
  }

  createForm() {
    this.simpleFormArmazem = this.formBuilder.group({
      index: [{ value: null, disabled: true }],
      nome: [null, Validators.required],
      localizacao: [null, Validators.required],
      status: [null, Validators.required]
    })
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.simpleFormArmazem.controls;
  }

  onReset() {
    this.submitted = false;
    this.simpleFormArmazem.reset();
  }


  onSubmit() {

    this.submitted = true;

    // parar aquei se o formulário for inválido
    if (this.simpleFormArmazem.invalid) {
      return;
    }
    this.loading = true;
    const index = this.simpleFormArmazem.getRawValue().index;
    // TODO: usado para fazer a requisição com a api de criação de objsct or update
    const uri = (index === null ? 'armazem/register' : 'armazem/update/' + index);
    this.createOrEdit(uri, this.simpleFormArmazem, (index === null ? true : false));

  }

  createOrEdit(uri: any, formulario: FormGroup, isCreate: boolean) {

    // TODO: usado para fazer a requisição com a api de criação de object
    this.http.__call(uri, formulario.value).pipe(first()).subscribe(
      response => {
        this.submitted = false;
        this.loading = false;
        if (isCreate && Object(response).code ==200) {
          formulario.reset(); 
        }  
        
        if (Object(response).code ==200) {
          this.loadList_Armazem(Object(response).data);
        }
      },
      error => {
        this.submitted = false;
        this.loading = false;
      });
  }


  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
     
    if (this.armazem !== undefined ) {
      
      this.title = "Editar armazem";
      //this.onReset()
      
      this.simpleFormArmazem.patchValue({
        index: this.armazem.id,
        nome: this.armazem.nome,
        localizacao: this.armazem.localizacao,
        status: this.armazem.status
      });
      
    } else {
      this.onReset()
      this.title = "Registar armazem";
      
    }
  }

  public loadList_Armazem(armazem) { 
    this.loadListarmazem.emit(armazem);
  }

}
