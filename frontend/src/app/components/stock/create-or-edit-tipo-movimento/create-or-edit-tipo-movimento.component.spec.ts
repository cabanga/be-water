import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditTipoMovimentoComponent } from './create-or-edit-tipo-movimento.component';

describe('CreateOrEditTipoMovimentoComponent', () => {
  let component: CreateOrEditTipoMovimentoComponent;
  let fixture: ComponentFixture<CreateOrEditTipoMovimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditTipoMovimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditTipoMovimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
