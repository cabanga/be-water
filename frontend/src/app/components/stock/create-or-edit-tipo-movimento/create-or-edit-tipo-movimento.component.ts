import { Component,ViewEncapsulation, OnInit, Input,Output, EventEmitter,OnChanges, SimpleChange } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms'; 
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-create-or-edit-tipo-movimento',
  templateUrl: './create-or-edit-tipo-movimento.component.html',
  styleUrls: ['./create-or-edit-tipo-movimento.component.css']
})
export class CreateOrEditTipoMovimentoComponent implements OnInit {





  @Input() modal: string = "modalTipoMovimentoCreateOrEdit";
  @Input() title: string = "Registar Tipo Movimento";
  @Input() tipoMovimentos:any;

  submitted = false;
  formErrors: any;
  private loading: boolean = false;
  
  @Input() simpleFormtipoMovimentos: FormGroup; 

  @Output() private loadListtipoMovimentos = new EventEmitter<any>();
  

  constructor(private http: HttpService, private configService: ConfigService, private formBuilder: FormBuilder) { 
    this.createForm();
  }

  ngOnInit() {
    
  }

  createForm() {
    this.simpleFormtipoMovimentos = this.formBuilder.group({
      index: [{ value: null, disabled: true }],
      descricao: [null, Validators.required],
      slug: [null, Validators.required]
    })
  }
  // convenience getter for easy access to form fields
  get f() {
    return this.simpleFormtipoMovimentos.controls;
  }

  onReset() {
    this.submitted = false;
    this.simpleFormtipoMovimentos.reset();
  }


  onSubmit() {

    this.submitted = true;

    // parar aquei se o formulário for inválido
    if (this.simpleFormtipoMovimentos.invalid) {
      return;
    }
    this.loading = true;
    const index = this.simpleFormtipoMovimentos.getRawValue().index;
    // TODO: usado para fazer a requisição com a api de criação de objsct or update
    const uri = (index === null ? 'tipo-movimento/register' : 'tipo-movimento/update/' + index);
    this.createOrEdit(uri, this.simpleFormtipoMovimentos, (index === null ? true : false));

  }

  createOrEdit(uri: any, formulario: FormGroup, isCreate: boolean) {

    // TODO: usado para fazer a requisição com a api de criação de object
    this.http.__call(uri, formulario.value).pipe(first()).subscribe(
      response => {
        this.submitted = false;
        this.loading = false;
        if (isCreate && Object(response).code ==200) {
          formulario.reset(); 
        }  
        
        if (Object(response).code ==200) {
          this.loadList_tipoMovimentos(Object(response).data);
        }
      },
      error => {
        this.submitted = false;
        this.loading = false;
      });
  }


  ngOnChanges(changes: { [propKey: string]: SimpleChange }) {
     
    if (this.tipoMovimentos !== undefined ) {
      
      this.title = "Editar Tipo Movimento";
      //this.onReset()
      
      this.simpleFormtipoMovimentos.patchValue({
        index: this.tipoMovimentos.id,
        descricao: this.tipoMovimentos.descricao,
        slug: this.tipoMovimentos.slug
      });
      
    } else {
      this.onReset()
      this.title = "Registar Tipo Movimento";
      
    }
  }

  public loadList_tipoMovimentos(tipoMovimentos) { 
    this.loadListtipoMovimentos.emit(tipoMovimentos);
  }





}
