import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTipoMovimentosComponent } from './stock-tipo-movimentos.component';

describe('StockTipoMovimentosComponent', () => {
  let component: StockTipoMovimentosComponent;
  let fixture: ComponentFixture<StockTipoMovimentosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockTipoMovimentosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTipoMovimentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
