import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpService } from "src/app/providers/http/http.service";
import { ConfigService } from "src/app/providers/config/config.service";
import { ExcelService } from "src/app/services/excel.service";
import * as moment from "moment";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-stock-existente',
  templateUrl: './stock-existente.component.html',
  styleUrls: ['./stock-existente.component.css']
})
export class StockExistenteComponent implements OnInit {


  public movimentos: any = [];
  public serie: any;
  loading: boolean = false;
  artigo_id: any = null;
  data: any = null;

  public filters = {
    search: null, // ordem de pesquisa de elemento
    orderBy: null, // Ordenação
    pagination: {
      perPage: 5,// Entrada - define o limite de resultados a serem gerados
      page: 1, //  define a pagina inicial ou proxima, 
      lastPage: null,
      total: 0 // defini o total de registo da BD
    },
    filter: null // subelemente do filter
  }

  public armazens = [];
  public artigos = [];
  public armazem_id: any = null;
  public artigoId: any = null;
  loadings = {
    artigo: false,
    armazem: true,
  };

  constructor(
    public http: HttpService,
    public configService: ConfigService,
    public excelService: ExcelService,
    public route: ActivatedRoute,
    public _route: Router
  ) {
    this.route.paramMap.subscribe((params) => {
      this.artigo_id = +params.get("artigo_id");
      
    });
  }

  ngOnInit() {
    this.getPageFilterData(1);
    this.getArmazens();
    this.getProdutos();
  }

  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile =
      "movimento_stock-" +
      moment(CurrentDate).format("DD") +
      "-" +
      moment(CurrentDate).format("MM") +
      "-" +
      moment(CurrentDate).format("YYYY") +
      " " +
      moment(CurrentDate).format("H") +
      ":" +
      moment(CurrentDate).format("m");
    this.excelService.exportAsExcelFile(
      document.getElementsByClassName("exportAsXLSX")[0],
      nameFile
    );
  }
  exportAsPDF(): void {
    //this.reportCliente.relatorioClientes(this.items, 'save');
  }

  imprimirPDF(): void {
    //this.reportCliente.relatorioClientes(this.items);
  }

  public getExistenciaStock() {
    this.loading = true;
    this.http
      .__call("stock_movimento/armazem/existencia", { filter: this.filters, armazem_id: this.armazem_id, artigo_id: this.artigoId })
      .subscribe((response) => {
        this.filters.pagination.lastPage = Object(response).data.lastPage;
        this.filters.pagination.page = Object(response).data.page;
        this.filters.pagination.total = Object(response).data.total;
        this.filters.pagination.perPage = Object(response).data.perPage;
       this.movimentos = Object(response).data.data;
        this.loading = false;
      });
  }

  getPageFilterData(page: number) {
    if (this.filters.pagination.perPage == null) {
      return;
    }
    this.filters.pagination.page = page;
    this.getExistenciaStock();
  }

  public setDataSerie(serie: any) {
    this.serie = serie;
  }

  goToPageLink(artigo: any) {
    //this._route.navigate(["/stock/movimentacao/view/artigo", artigo.artigo_id]);
  }

  public getArmazens() {
    this.loadings.armazem = true;
    this.http.__call("armazem/selectBox", null).subscribe((response) => {
      this.armazens = Object(response).data;
      this.loadings.armazem = false;
    });
  }

  getProdutos() {
    this.loadings.artigo = true;
    this.http.__call("artigos/prod/selectBox", { pagination: { page: null } })
      .subscribe((res: any) => {
        this.artigos = Object(res).data;
        this.loadings.artigo = false;
      });
  }





}
