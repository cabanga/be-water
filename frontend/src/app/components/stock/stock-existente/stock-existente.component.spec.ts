import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockExistenteComponent } from './stock-existente.component';

describe('StockExistenteComponent', () => {
  let component: StockExistenteComponent;
  let fixture: ComponentFixture<StockExistenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockExistenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockExistenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
