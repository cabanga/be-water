import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockProdutosComponent } from './stock-produtos.component';

describe('StockProdutosComponent', () => {
  let component: StockProdutosComponent;
  let fixture: ComponentFixture<StockProdutosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockProdutosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockProdutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
