import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimentacaoStockComponent } from './movimentacao-stock.component';

describe('MovimentacaoStockComponent', () => {
  let component: MovimentacaoStockComponent;
  let fixture: ComponentFixture<MovimentacaoStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimentacaoStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimentacaoStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
