import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditProdutosComponent } from './create-or-edit-produtos.component';

describe('CreateOrEditProdutosComponent', () => {
  let component: CreateOrEditProdutosComponent;
  let fixture: ComponentFixture<CreateOrEditProdutosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditProdutosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditProdutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
