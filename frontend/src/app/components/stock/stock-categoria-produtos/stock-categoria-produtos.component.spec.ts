import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockCategoriaProdutosComponent } from './stock-categoria-produtos.component';

describe('StockCategoriaProdutosComponent', () => {
  let component: StockCategoriaProdutosComponent;
  let fixture: ComponentFixture<StockCategoriaProdutosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockCategoriaProdutosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockCategoriaProdutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
