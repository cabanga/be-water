import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaOrcamentoComponent } from './lista-orcamento.component';

describe('ListaOrcamentoComponent', () => {
  let component: ListaOrcamentoComponent;
  let fixture: ComponentFixture<ListaOrcamentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaOrcamentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaOrcamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
