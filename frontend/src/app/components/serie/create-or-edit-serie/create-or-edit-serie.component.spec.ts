import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditSerieComponent } from './create-or-edit-serie.component';

describe('CreateOrEditSerieComponent', () => {
  let component: CreateOrEditSerieComponent;
  let fixture: ComponentFixture<CreateOrEditSerieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditSerieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditSerieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
