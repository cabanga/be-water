import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditProdutoComponent } from './create-or-edit-produto.component';

describe('CreateOrEditProdutoComponent', () => {
  let component: CreateOrEditProdutoComponent;
  let fixture: ComponentFixture<CreateOrEditProdutoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditProdutoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditProdutoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
