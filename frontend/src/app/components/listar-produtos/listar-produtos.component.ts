import { Component, OnInit,ViewChild } from '@angular/core'; 
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

import { ExcelService } from 'src/app/services/excel.service';
import * as moment from 'moment';

@Component({
  selector: 'app-listar-produtos',
  templateUrl: './listar-produtos.component.html',
  styleUrls: ['./listar-produtos.component.css']
})

export class ListarProdutosComponent implements OnInit {

  constructor(private http: HttpService,private configService: ConfigService, private excelService: ExcelService) { }
  
  public produto: any;
  private loading: boolean = false;
   

 
  private items:any = [];
  private TipoProduto: any = [];
  ngOnInit() { 
    this.getPageFilterData(1);
    this.listarTipoproduto();
  }
  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "Lista_Produtos-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

/**
 * 
 * @param start 
 * @param end 
 * @param search 
 */
  private listarProdutos() {  
    this.loading = true
    this.http.__call('artigo/listar', this.http.filters).subscribe(
      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.loading = false
      }
    );
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      this.http.filters.pagination.page = 1;
      return;
    }
    this.http.filters.pagination.page = page;
    this.listarProdutos();
  } 

  private listarTipoproduto() {
    this.http.call_get('tipo-produto/selectBox', null).subscribe(
      response => {
        this.TipoProduto = Object(response);
      }
    );
  }


  
  private setDataProduto(produto) {
    this.produto = produto
  }

 
 
}