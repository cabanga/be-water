import { Component, OnInit, Input, createPlatformFactory } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { ExcelService } from 'src/app/services/excel.service';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-caucao',
  templateUrl: './caucao.component.html',
  styleUrls: ['./caucao.component.css']
})
export class CaucaoComponent implements OnInit {

  @Input() simpleFormCaucao: FormGroup;

  private caucao = {
    id: null,
    nome: null,
    slug: null,
    is_active: null,
    user_id: null
  };

  public currentUser: any;

  private title: string = "Registar Caução";

  private input_default: boolean = false;
  private input_required: boolean = false;

  private items: any = [];
  private modulos: any = [];
  /*  private estados: any = []; */

  constructor(private http: HttpService, private configService: ConfigService, private excelService: ExcelService, private auth: AuthService) {
    this.currentUser = this.auth.currentUserValue;
  }



  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "lista_configuracoes -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

  ngOnInit() {
    this.getPageFilterData(1);
  }

  private getDadosModal() {

  }

  private getCaucaos() {

    this.configService.loaddinStarter('start');

    this.http.__call('caucao/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }


  view_caucao = false;

  private saveCaucao() {
    console.log(this.caucao);

    if (this.caucao.id == null) {

      this.http.__call('caucao/create', {
        nome: this.caucao.nome,
        slug: this.caucao.slug,
        user_id: this.currentUser.user.id
      }).subscribe(
        res => {
          if (Object(res).code == 200) {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            /*    this.clearFormInputs();
               this.listarRotaRunByRotaHeader(); */
               
              this.getCaucaos();            

            this.configService.loaddinStarter('stop');
          } else {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
            this.configService.loaddinStarter('stop');
          }
        }
      )
    }
    else {

      this.http.__call('caucao/update/' + this.caucao.id, {
        nome: this.caucao.nome,
        slug: this.caucao.slug,
        user_id: this.currentUser.user.id
      }).subscribe(
        res => {
          if (Object(res).code == 200) {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            /*    this.clearFormInputs();
               this.listarRotaRunByRotaHeader(); */

               this.getCaucaos();

              this.configService.loaddinStarter('stop');
          } else {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
            this.configService.loaddinStarter('stop');
          }
        }
      )
    }

  }

  private onReset() {

    this.caucao.nome = null;
    this.caucao.slug = null;
  }

  
  private actualizarEstadoCaucao(item) {

    this.caucao.id = item.id;
    this.caucao.nome = item.nome;
    this.caucao.slug = item.slug;
    this.caucao.is_active = !item.is_active;
    this.caucao.user_id = item.user_id;

    //console.log(item);
    this.http.__call('caucao/update/' + this.caucao.id, this.caucao).subscribe(
      response => {

        if (Object(response).code == 200) {
          var update = (this.caucao.is_active == true) ? "Activado" : "Desactivado";

          this.configService.showAlert("Caução " + this.caucao.nome + " foi " + update, "alert-success", true);
        }

      }
    );

    for (let i = 0; i < this.items.length; ++i) {
      if (this.items[i].id == this.caucao.id) {
        this.items[i].is_active = this.caucao.is_active;
      }
    }
  }


  private setDataCaucao(item) {

    if (item !== undefined) {
      this.title = "Editar Caução";

      this.caucao.id = item.id;
      this.caucao.nome = item.nome;
      this.caucao.slug = item.slug;
      this.caucao.is_active = item.is_active;
      this.caucao.user_id = item.user_id;
    }
  }


  private getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.getCaucaos();
  }

  private setDefault() {
    this.input_default = !this.input_default;
  }

  private setRequired() {
    this.input_required = !this.input_required;
  }

}
