import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrioridadeComponent } from './prioridade.component';

describe('PrioridadeComponent', () => {
  let component: PrioridadeComponent;
  let fixture: ComponentFixture<PrioridadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrioridadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrioridadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
