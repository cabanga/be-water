import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

import { ExcelService } from 'src/app/services/excel.service';
import * as moment from 'moment';


@Component({
  selector: 'app-estado-reclamacoes',
  templateUrl: './estado-reclamacoes.component.html',
  styleUrls: ['./estado-reclamacoes.component.css']
})
export class EstadoReclamacoesComponent implements OnInit {
  
  private loading: boolean = false;

  private estado = {
    id: null,
    designacao: null,
    sigla: null
  }


  private items: any = [];

  constructor(private http: HttpService, private configService: ConfigService,private excelService: ExcelService) { }

  ngOnInit() {
    this.getPageFilterData(1);
  }

  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "Lista_Estados_Reclamações-" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

  private estadosReclamacao() {
    this.loading = true
    this.configService.loaddinStarter('start');

    this.http.__call('estado/reclamacao/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.loading = false
        this.configService.loaddinStarter('stop');

      }
    );
  }

  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.estadosReclamacao();
  }


  private register(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (this.estado.designacao == "" || this.estado.designacao == null) {
      this.configService.showAlert("O campo designacao é obrigatorio", 'alert-danger', true);
    } else {
    this.http.__call('estado/reclamacao/create', this.estado).subscribe(
      res => {
        if (Object(res).code == 500) {
          this.configService.showAlert(Object(res).message, 'alert-danger', true);
          this.configService.loaddinStarter('stop');
        } else {
          this.configService.showAlert(Object(res).message, 'alert-success', true);
          this.clearFormInputs(e);
          this.estadosReclamacao()
          this.configService.loaddinStarter('stop');
        }
      }
    )
  }
      ;
  }


  private clearFormInputs(e) {
    e.target.elements[0].value = null;
    e.target.elements[1].value = null;
    e.target.elements[2].value = null;
  }

  private refresh(id, designacao, sigla) {

    this.estado.id = id;
    this.estado.designacao = designacao;
    this.estado.sigla = sigla;
    

  }

  private editar(e) {
    this.configService.loaddinStarter('start');
    e.preventDefault();
    if (this.estado.designacao == "" || this.estado.designacao == null) {
      this.configService.showAlert("O campo designação é obrigatorio", 'alert-danger', true);
    } else {
      this.http.__call('estado/reclamacao/update/' + this.estado.id, this.estado).subscribe(
        res => {
          if (Object(res).code == 500) {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
          } else {
            //this.configService.clearFormInputs(e);
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            this.estadosReclamacao();

          }
        }
      );
    }
    this.configService.loaddinStarter('stop');
  }


  private ini() {
    this.estado = {
      id: null,
      designacao: null,
      sigla: null
    }
  }






}
