import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadoReclamacoesComponent } from './estado-reclamacoes.component';

describe('EstadoReclamacoesComponent', () => {
  let component: EstadoReclamacoesComponent;
  let fixture: ComponentFixture<EstadoReclamacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadoReclamacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadoReclamacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
