import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoReclamacoesComponent } from './tipo-reclamacoes.component';

describe('TipoReclamacoesComponent', () => {
  let component: TipoReclamacoesComponent;
  let fixture: ComponentFixture<TipoReclamacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoReclamacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoReclamacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
