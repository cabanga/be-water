import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockMovimentoComponent } from './stock-movimento.component';

describe('StockMovimentoComponent', () => {
  let component: StockMovimentoComponent;
  let fixture: ComponentFixture<StockMovimentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockMovimentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockMovimentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
