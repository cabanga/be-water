import { TestBed } from '@angular/core/testing';

import { ResumoRecebimentoPorCaixaService } from './resumo-recebimento-por-caixa.service';

describe('ResumoRecebimentoPorCaixaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ResumoRecebimentoPorCaixaService = TestBed.get(ResumoRecebimentoPorCaixaService);
    expect(service).toBeTruthy();
  });
});
