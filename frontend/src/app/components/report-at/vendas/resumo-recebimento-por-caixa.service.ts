import { Injectable, EventEmitter } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';

import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
// ES6 Modules or TypeScript
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class ResumoRecebimentoPorCaixaService {

  constructor(private http: HttpService, private configService: ConfigService) { }

  public imprimirResumoRecebimentoPorCaixa(data, byUser = 0, user_id = null) {
    this.configService.loaddinStarter('start');
    this.http.__call('vendas/resumoRecebimentoPorCaixa', { data: data, byUser, user_id }).subscribe(
      response => {
        const dados = Object(response).data;
        this.PDFResumoRecebimentoPorCaixa(dados);
        this.configService.loaddinStarter('stop');
      }
    );
  }




  public PDFResumoRecebimentoPorCaixa(dados: any) {

    var doc = new jsPDF('landscape')
    var posX;
    var posY;
    var imgData = dados.empresa.logotipo;
    doc.addImage(imgData, -5, -1, 35, 35);

    doc.setFontSize(12);
    //doc.text(5, 31, ' SRV-Sistema de Recebimento de Clientes');

    var users = dados.recebimentos;

    doc.setFontType("bold");
    doc.text(195, 14, 'Local Recebimento:');
    doc.setFontType("normal");
    doc.text(238, 14, '' + dados.loja.lojaNome);

    doc.setFontType("bold");
    doc.text(224.2, 21.4, 'Data:');
    doc.setFontType("normal");
    doc.text(238, 21.4, dados.data + ' a ' + dados.data);

    doc.setFontType("bold");
    doc.text(214.9, 29, 'Utilizador:');
    doc.setFontType("normal");
    doc.text(238, 29, '' + (users.length == 1 ? users[0].user.nome : 'Todos'));

    doc.setFontSize(15);
    doc.text(105, 34, 'Resumo de Recebimentos por Caixa');


    posY = 36;
    var page = 1;
    var totalpage = Math.round(users.length / 2);
    this.footer(doc, page, totalpage);
    var outraPage = 0
    var resumos = []
    for (var i = 0; i < users.length; i++) {

      if (outraPage == 2) {
        page++;
        posY = 20;
        doc.addPage('a4', 'l');
        this.footer(doc, page, totalpage);
        outraPage = 0;
      }
      outraPage++;

      doc.setFillColor(220, 220, 220);
      doc.rect(5, posY, 287, 10, 'F')

      doc.setFontType("bold");
      doc.setFontSize(10);
      doc.text(10, posY + 6, 'Utilizador: ' + users[i].user.nome);

      doc.setFontSize(10);
      doc.text(5, posY + 18, 'FACTURAS');
      doc.text(5, posY + 24, 'Meio Pagamento');

      //  doc.text(160, posY + 15, 'Valor AOA');
      // doc.text(204, posY + 15, 'Valor USD');
      // doc.text(257, posY + 15, 'Valor AOA(equiv)');
      doc.text(273, posY + 15, 'Valor AOA');

      doc.setFontType("normal");
      posY += 25;
      var pagamento = users[i].vendas[0]
      var total = 0
      for (var j = 0; j < pagamento.length; j++) {
        doc.setFontSize(10);
        doc.text(82, posY, '' + pagamento[j].designacao);
        //doc.text(162, posY, '' + this.configService.numberFormat(pagamento[j].total));
        //doc.text(216, posY, "0,00");
        doc.text("" + this.configService.numberFormat(pagamento[j].total), 290, posY, null, 'right');
        total += pagamento[j].total
        posY += 6;


        if (pagamento[j].designacao != 'Troco') {

          if (resumos.length == 0) {
            resumos.push(pagamento[j]);
          } else {
            var validar = 0;
            for (let index = 0; index < resumos.length; index++) {
              const element = resumos[index];
              if (element.designacao == pagamento[j].designacao) {
                resumos[index].total += pagamento[j].total
                validar = 1
              }
            };

            if (validar == 0) {
              resumos.push(pagamento[j]);
            }

          }
        }


      }

      posY += 10
      doc.setFontSize(10);
      doc.setFontType("bold");
      doc.text(5, posY - 10, 'RECIBOS');
      doc.text(5, posY - 5, 'Meio Pagamento');

      doc.setFontType("normal");

      var pagamento = users[i].vendas[1]


      for (var j = 0; j < pagamento.length; j++) {
        doc.setFontSize(10);
        doc.text(82, posY, '' + pagamento[j].designacao);
        // doc.text(162, posY, '' + this.configService.numberFormat(pagamento[j].total));
        // doc.text(216, posY, "0,00");
        doc.text("" + this.configService.numberFormat(pagamento[j].total), 290, posY, null, 'right');
        // doc.text(272, posY, "" + this.configService.numberFormat(pagamento[j].total));
        total += pagamento[j].total
        posY += 6;


        if (pagamento[j].designacao != 'Troco') {

          if (resumos.length == 0) {
            resumos.push(pagamento[j]);
          } else {
            var validar = 0;
            for (let index = 0; index < resumos.length; index++) {
              const element = resumos[index];
              if (element.designacao == pagamento[j].designacao) {
                resumos[index].total += pagamento[j].total
                validar = 1
              }
            };

            if (validar == 0) {
              resumos.push(pagamento[j]);
            }

          }
        }


      }

      posY -= 3;
      doc.text(82, posY, '___________________________________________________________________________________________________________');
      posY += 5;
      doc.setFontStyle('bold');
      doc.text(122, posY, 'Total');
      // doc.text(162, posY, '' + this.configService.numberFormat(total));
      // doc.text(216, posY, '0,00');
      doc.text("" + this.configService.numberFormat(total), 290, posY, null, 'right');
      // doc.text(272, posY, '' + this.configService.numberFormat(total));
      posY += 5;
    }

    if (outraPage == 3) {
      page++;
      posY = 20;
      doc.addPage('a4', 'l');
      this.footer(doc, page, totalpage);
      outraPage = 0;
    }

    doc.setFontStyle('normal');
    doc.setFontSize(12);
    doc.text(190, posY + 15, 'Resumo Total de Recebimentos');
    doc.setFontSize(10); doc.setFontStyle('bold');
    doc.text(190, posY + 22, 'Meio de Pagamento');
    doc.text(250, posY + 22, 'Valor Total AOA');
    doc.setFontStyle('normal');
    for (var j = 0; j < resumos.length; j++) {
      doc.setFontSize(10);

      doc.text(190, posY + 27, '' + resumos[j].designacao);
      doc.text(250, posY + 27, '' + this.configService.numberFormat(resumos[j].total));
      posY += 6;


    }

    doc.autoPrint();
    window.open(doc.output("bloburl")); //opens the data uri in new window 

  }


  footer(doc, page, totalpage) {
    var today = moment().format("DD-MM-YYYY H:mm:ss");
    var CurrentDate = new Date();
    doc.setFontType("normal");
    doc.setFontSize(10);
    doc.text(5, 203, '' + today);
    //doc.text( "" + this.configService.numberFormat(pagamento[j].total),290, posY,null,'right');
    doc.text( ''+'Página ' + page + ' / ' + totalpage,290, 203,null, 'right');
  }




}
