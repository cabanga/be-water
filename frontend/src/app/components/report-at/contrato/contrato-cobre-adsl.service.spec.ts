import { TestBed } from '@angular/core/testing';

import { ContratoCobreAdslService } from './contrato-cobre-adsl.service';

describe('ContratoCobreAdslService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContratoCobreAdslService = TestBed.get(ContratoCobreAdslService);
    expect(service).toBeTruthy();
  });
});
