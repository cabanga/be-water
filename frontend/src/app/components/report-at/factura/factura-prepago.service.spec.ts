import { TestBed } from '@angular/core/testing';

import { FacturaPrepagoService } from './factura-prepago.service';

describe('FacturaPrepagoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacturaPrepagoService = TestBed.get(FacturaPrepagoService);
    expect(service).toBeTruthy();
  });
});
