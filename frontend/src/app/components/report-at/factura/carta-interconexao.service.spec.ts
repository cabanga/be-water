import { TestBed } from '@angular/core/testing';

import { CartaInterconexaoService } from './carta-interconexao.service';

describe('CartaInterconexaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CartaInterconexaoService = TestBed.get(CartaInterconexaoService);
    expect(service).toBeTruthy();
  });
});
