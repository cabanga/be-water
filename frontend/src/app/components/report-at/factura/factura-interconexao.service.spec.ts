import { TestBed } from '@angular/core/testing';

import { FacturaInterconexaoService } from './factura-interconexao.service';

describe('FacturaInterconexaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FacturaInterconexaoService = TestBed.get(FacturaInterconexaoService);
    expect(service).toBeTruthy();
  });
});
