import { TestBed } from '@angular/core/testing';

import { CartaIplcService } from './carta-iplc.service';

describe('CartaIplcService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CartaIplcService = TestBed.get(CartaIplcService);
    expect(service).toBeTruthy();
  });
});
