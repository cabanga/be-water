import { Injectable, Input, EventEmitter } from '@angular/core';
import { ApiService } from 'src/app/providers/http/api.service';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { environment } from 'src/environments/environment';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';

@Injectable({
  providedIn: 'root'
})
export class ReciboService {

  @Input() app_environment: null;

  constructor(private http: ApiService, private auth: AuthService) {
    this.app_environment = this.auth.getAppEnvironment();

    if (this.app_environment == null) {

      var url = require('url');
      var app_url = url.parse(environment.app_url, true);

      this.app_environment = app_url.host;

    }
   }

  /*   public imprimirRecibo(id, via) {
      this.configService.loaddinStarter('start');
      this.http.__call('recibo/printerRecibo/' + id, null).subscribe(
        response => {
          const dados = Object(response).data;
          this.PDFRecibo(dados.recibo, dados.factura, dados.linha_pagamentos, dados.cliente, via, dados.user, dados.conta);
          this.configService.loaddinStarter('stop');
        }
      );
    } */


  public imprimirRecibo(id, report = 'imprimir', via = '2º Via') {
    this.http.get('facturas/preview/' + id).subscribe(
      response => {
        const dados = Object(response).data;

       //console.log(dados)
        this.PDFRecibo(dados.factura, dados.produtos, dados.cliente, dados.user, dados.pagamentos, dados.contrato, dados.leituras, report, via)
      }, error => {

      }
    );
  }

  public PDFRecibo(factura: any, produtos: any[], cliente: any, user: any, pagamento: any, contrato: any, leituras: [], report: string = 'imprimir', original: any) {





    /*   public PDFRecibo(recibo: any, facturas: any[], pagamento: any, cliente: any, original: any, user: any, conta: any) { */

    /*  var img_logotipo = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGEAAACGCAYAAADNR+SZAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAACCeSURBVHhe7Z0HeFVVtscdnarf9NHpvahv9M2o8/TZRp1RVMDewTL6BBUU6UhTAUVBeu8gvYTeaxokECD0mkCogVRaArm5SdZbv33PiScn52Ia5ATz/7715d5z9mnrv/Zaa6+978llUotqRy0JPkAtCT5ALQk+QLWTUFQkUlBoffmKwhc94Vx+keTlW1++gvAFCdoZ5ORZkbxg6PtXDb6JCfkFImmntEd8BYnwVWA+ebZIjpwoVELoG18d+IqEQtV96slCJaLoKxWsfUUCOKsBen96oRxTMiDlqwDfkQAyThfJnuOFkn76q9EdfEkCrmif9obdxwokO/fS7w6+JAGcVOXvSi2QXUrEKQ3YlzJ8SwLx4EBmoWw7Uih7jhXK2cClS0S1kxBU18MYwQu5qnhI2Hq4UJLTCi/ZjKnaSTiq44Ij2eG1y/7Nh0KSklFoRteXGqqVhDPnRLZbln5KP3uBETRtEg+oHCyU4zqqvtRQbSTgWpLUxdhWvldT0mAYt0Q5Y4OSsFFJ2KRyOgxhNRXVRgIWvVEVu0kJQLB0RstegLAdqYWSkBIS4kTgEqoxVQsJZDqb1QVtUKvGum2hRxCMvZCdUyTrlIC1KnH7NVBnXDpu6aKTwCROsg7E1qki16tCncI23BJtvLBb961OLpQ1+wolVv9eKvHhopOQlSPGmsNJvBKRccZbuTkBkTglACJik5Q0bX8mr+YTcVFJwLfjhtaoElEmglU7ZbUKrincmCA5vUgi9xZKjJIQpX+3anwI13NqCi4aCejpUFaRsWAsGUGR248Wyi4dEfPZWLi1PSXTW7NMhUJe5J4QCfyl9F2TcdFIOJevrgQFW1aMROtnpjVPnQuRw3cE5UJIOFfDoG35rgJZtbtAVqrEJBXomKPmEnFxSFD97FRrX7VbFYwFq/B5i7ome85gi7qVlY79fCYt9QKLAuKSC2TZzgJZoWQs1b+bDhXU2NH0RSEhU9PLyD0hq7Wtl7/OAEzJmu1uCRekKe4t2R4iAhKW7CiQozXULV1wErB0Aq1ttbblJuqYwKkygiu9Yakqc7nuR/i8XgdxFPnc4Lyr1Q0t2hZUMoKySCVaia6J1dYLTgLWaVusbbUol97hRpZuc7ZBFumxB7O83dKR7CKZvyUoC5UIyODzrjAuzM+4oCRQWiBoLlQrXbwjJAtUWfQMr/ljNtFDFm7VthyjgoJxZV5LYag1rdbYMHezEqDHIJBxoobNxl1QEpJ0ZIxiUCQCASg27XR4JREDaENb+zjOwZyzFw5rb4AEW2YnBnXkrUG6BvFwwUg4p6NbXAsuYoEqEUFJcfvClEod2HCgQOY4rHuengPXlKvndANlR+0JykxV/mxIUOEz8xA1BReMBAZhszep4lWBTksNl+04kaXuhOMgAuG4mXouRtteYLAWsTFflZ8vs1T4HLk7WGNm4i4ICad0AIZvnqWKs60zQq0zfl/QMxa4QRNcysyNeiznUMG66VVe/p7YELkrKNMS8mXGhpBMV9mvg7qagAtCQqIOnFAClolEqMzelK+xoOxKST9TqL0gZNX2eThngroqLx4PZxfK1PX5Ms2SKevyZbkmAjVh3qHKScBSURwKs60S5cTuLb821iQHZSrWrcdPt4TzZXq4NHoYAX2SKn+KHoNMiM+XlBow71ClJBAk49WNTFZF2BZpy/HzZEThQPyABKdMWguh3sGdbGyC7ocIBBIWqAvze2yoUhIy1YXQAyarsoqtUZUSo72gIoowo2IdB3AOzolMUjGknip9QoqE8zT+fL5GrxuXL+MtYTWfn1GlJJB+jlfrQ1HIRFyDKizc3HFZcOxUkSFzvBIx0ZJxqlhKFl5BfovGo7GrA/J5XEjG6Oel6qbCLSLwA6qMBOaAcQFYHm4AGacWuVLHCpVFjI6Yx+q5bMtGuWNVMrTnuUGFFaLGrM5XMkJ/kWMePccvqBISiAWxSUEZ7bDAcSrj4wNVMg+crvGEc3FOW7jWKh0LeI2M16UUyLCofBkdG5Lh0aFxg19RJSTYvcBYn1osMlIffoXm7lURFHE7UdobRsR8Ydmj9Pz0Cq/YQFlkXFxQhmt7jhmmJNA7cnw6H10lJKzdrw+sD4rVoZxR+uC4Avx5VYFzEQtQKgQjQ/Wa0Rr0va6yWAeLgyNDBCCDVuWb9U1+RKVJwLrw/5CAgpCh6gpWaS+oaqzQ+DJEFTuCa6lwzTHa67xW5DFahoQhei/IQCVhlo66A0H/9YZKk0Cxbag+LApB8MWkiMwNVDU4p02yLVj4Wh2buIGySVMHrAy1QfhcltrVxUalSDibX6Q5e9A8oK0ULI45hAsBYgPBuL8qEytHBuj1Jq8Leq7cY+DYe1m+aY/0Wa4DvQt0b5VBpUhgDVC/FSUtbaRaas4FnGLEkvHxKBXCEZTrtSgg9UShcV/s5z4hZIoS5rcRdKVIiNgYNA+H8hEeNt5jviAYLJT0zBw5V8ZfiheoltIyzkhWdq61pSSYU0ahXBvhulMTglLoUi7p6yRVes8lIQJ6LQ31nqMn/eWSKkxCmqaGPFBfhyLw11k5JTWRkZUjM+dvkydeHievNJkqUWv2WXu8sXVHqjTvMEfue2yYdPlsmSxYtlNOn8mz9obACLyvXq+PKpbr8pfvRz1+bMJ0aQ8loZe2+UxJ+GRRvlk84CdUmARWNvRYHFIAVoa1Ld9Rshdgb2dyA5J6/LTsP5AlY6esl/oNxsiE6RtDDVxYGZMsDz49UvoMjZE9yemSqQTuO5BpelGOY1oNdzJnU1A+0eujXORT/czsmxv86PAzvTeIQD5WEijqeQ3yqgsVIiFHDXN0bKib2xYGEfyY48uweVuq1H9htHT4aLEsWbVHDh05IdFx+6RH/0h54MnhMn/pDqtlSZw7FyxBBEU53Av30BMLVxJG6j2xms8JlsvgNj9aGCKKdv1X6rlKdq5qRYVIQAF0a6MAFT5P31C2WTOQevyUfNBjqbzQeJK83W62vPjmZHnnvdmybecxq4U3gtoFihwmPG5NULrrtVEugqJZ1+oGC5C7zA/1AqSbtqMU4hdUiATmfD+2LAvpuiDfzCm7sSo2SXoNjpZeg6Lls0FRMm/JDg2eXzw8nwMBspWSx65JOGCOGTVxnfTW42fM3aIEl1Ya10S53fUekG56H0yDulseyCxSYwnofQa0TUCPCcjWw/5JVctNwln1CEMig8aasEIefGi0ugHHD77PaF9v2Xme1FO307jFDA2wy6VJ61nySMOx0ki/p2fkWC1LIvdsvnzYc5l1XIT0HRYjLTrONUH9dT0uOSXTahnCSXU9pKBdVLEfLQwp+LOlgVJrlJjipN0H80JEvD83YCaI/IJyk7BT8/GPF+kD89AqH+qD4XNtEEj/03SqupgpkrQvw9oawpHUk8VK3bE7zdoawv6DWdJUXdOz/zdBdu4puS/7xFlp88ECqfv8aFm9NsXaGgKT/ygV5dqWzq9B3ZisKWznOaH77TibKmwNJoG1pJ0cD03X5leYwWCBURSpZefuiyXrhHeOz1ih58AouV+D8CtNpki/YbEaD+bIA0+NkHc7zNWMyPs4xg6fDY4y7V5oNEkWrdil7qzQGEVXvYcPEUvBuEs3GCmzD8I66F8GfH4ZtJWLBF4GxRxvJ8ui6N64gdOaaWRm58hETT2dlkoQPZZ2WpL2Z8ph7QUFjniQkHhIPp+6QT7pt0pGTUqQ2Pj9RtE2jutxKYey5dDRE5LvmBZjHDFxRqIM/zxe3VdAArqrx+KAsXIUjKIppee5CnUHs4rkvZmh/e1nBUyMKM/qjwuJcpFAAQ3r76wPi3TUB6dMwWsR3IFzw6ZD8lqzafJvtfgnX/ncWPDzjSbqYC3ZauGNzdtTpUmbWXLf48PkzVYzzbjh6VfHy9LIPSUyI8BXtjBo7KCKxThQMj3DvUye1/fQAxBIoF243z9cbJSLhPQzReYBeFiknVoWE+tuzFqwTe6qO9gE2bj1B2R3Urqx/E8HRModDw8ygzYvrIjeK/c8MlRadZova9alSLL2oPVKZl8dvN1Vb7AMGL7aM0ticRj3YiuZz/yaxwlqTuxrq/vY32pGwPyO2g8oFwksN3Q+bGt9EEbO4OixUxKvCu+qmdAdDw2U6XO2mO1uLFZffnf9IdKu6yJV9AEThBMSD0sPJeifun3MpPWlLB7ExO3XwdwIeUt7Ca5r++7jhtwCdVWUJlpOV+WqgSAt9b6IFU6wVqnTnDy95zxpE5EnzaflyfoUf6Sp5SIhQW+6jT4gvhWBBF53ALDW21X5/3l7msRpnu+EexyAX2/SeqYhg7T1X08MlwaNJ8uq1SVdlXNMAXYnpUkrTX3ve2yoPPTcKHn1nWmSkXFakjUJa6FKbavKRfjsLllDQofZmjpPz9NekCfNpuaZpZZ+QLlI4McePASW1Fql3cw884oDQE9AbHeBAinWMVh7V9PST/uvkuVReyXgeK/OcVUgPeFw6gkNyl8chwvrPzxWPu6zUj7uvdJkQqd4G4mF9Mwz5jiCNr2GZfPNUa7eE/KuksBvHJwgntETmqvyIanp5Dwz3+AHlIsEShM8IJYEGZDg7vYgQ9PM97oulP99aJA0eGOSyYBe1nT0fx8caILuwSMnrJYlwSDvvW6L5O56QzSYj5OBo9doz5pq4kvDNyeXGqzZ2H2sUN6dEnIxyNuqYOKEE5BATzDtlIgmE5WEMizTvxgoFwnMojXTh2ipD9pCH6SNkrHDVa7Yob66wRuT5cmXP5eNW45YW0PYsee4KWc/roO1dRsPWltDSNqfIa+9M13uVVfjdmcM5CCxzjMjTdxxAxK4L5SLvD2pNAn8PKv9rEuABMrHWBkPwcNAhj06HacZDwU5BmuNms8wrskLjH5bdJpnsqBXdGT9RsuZJpWt8/QIefa1CaVG2TZOnT6n8WC+3PPoUHnxrSnyul6DLCpVXRkxwU0Cv2twguyoHa7KatdU23jNTVcHykUCyxzfsUlQ4cHXHwg9yLwlO01KGjF/a4lBWcrBbFkVm1zKlSxZuVs+6as+X/3+R31WyOSIRDl77gvFHU8/bdJaeogzPBNncG9kYUPGxMkZjRV70oqMUm0S+GxnbTaompIZcd+04d43Wvde3SgXCVQjbRIQujQ/ifICruilN6eYQRoups5TI42lR2kG5JWC2ti07ai8pYO0OjpIo8T90LOh4+Yt3uE5RgC4lSYOEvjsrh+xEtDuBRBAkD50nte+XUyUiwQmbZwk0O3Hx5WeR5i9cLvc+sAAU0HFwmPi9plydPOO8+Tm+/rJqAkJZrTrBlnQLf/qJ//R1HOSHrd2w0GZs2i7dOq+RP56Vy+TBnthTGzA3Evxfek98jofJ3hXhn3v9JTeywKS7w8OykcCXZr0zn5YLKuzWhTLTchsErceVYUtNgRQhnZP7DPhP27KBrmtzkBp9f582bD5sOzamyZbtqea+Yab7usr3futNDUhNyDxtjoDpFn7OSaF3b7ruORrusvMWZd5geKYwD210czN/dtnJnbseAYJrNbwC8pFAuv/B+rN2w9jP3RSepH6/iyj/LrPjTaT8+dDZGySqSf999195K939pL/uqOX1Ht+tEyakWi18EakurKGmnlxDOXw3Nw8HSMUSSs1DNvX0yNYeMA7t51g/sC+b9qwVskvKBcJgAzpLY0FNgk8EOtPweGjJ+Wk9VpHegHlhT5Do0021F2DMAH6DG+OUrCCYt+BLNN79mpGdILXvSiomMYlHDQ9iYDdrddyE8TJjkBubsBUV5kiBZPWBksEZeIUa4vc6L9CXZZFAoZDWusXlJsEFtWSmjqDHPn3IUf3P55+xqSTWGz9F8ZIY01Dsdwb9HvjlhFm5YUXIOa9rovk7/f0NYG5/UeLTQX1pnv7mh4AWU7wW+X2OmB0uiL+sjTTidPnikxllZhAj2mh7srrtQ7VhXKTQNWB+r0zQGOJE+NDvQGLZbxACXvJyj2aQuZJIFAgOdoDYrRnPNJgjJF1GnSJETZ2JaWb8cXtOsqOmLvVEGKOU8snBjzScIzJtBjI2TWl6Tp4dPaCd5QElJ3v6gi84NC4LN3PfQ+O1Db+yE4Nyk0CYDl6U4dLQrAw6vO5uefko94rJNE1Wrax/0CmISmUPUXIhz2WadY011RQqZIyNvACk0IUB8mQSHFxJ6ZHOu4BV7TSYzU45XbbhUIaK/j8hAqRQPfuviiUCtoKwMI+Xhgo8ap9Ju7JYtZtPCRbdx4rzpbIfsZP2yivaSpKiQPlDhgRK2mZoQUAFPMo0G3ZkWrEXoFHz2AalWt0X1SyN4aun1fqx+a5edpW7wuXhbyn7sudvlY3KkQCYIDEg9t+GMHKhmhXD3mZIhkxfq3cVW+IKdzdWXewqf8sWLrLHG/jrKZcTrVt3HxE3mw907R/5rXxZhIIkih/A4qtJAJON0Rv4F74bZsbvOjcJoskou/ygG/mlm1UmAQUxw81cAG2MhCUg5IYO2SqZUfM2yrRa/aZNURM0BNk+2nm454rAKS2t/y7nzz24jjpMyRa1iSkyLBx8WYuOqhOnKUrn8dpqukgwL7mF+SXBKvF7fa0W5Pso2BgocIkAIpidtbhVApuakhUwLxk0AkGdP3V7dx4d2+zsmL9psOSrY0YsHX+ZIkS1E/eaj3LrD11AzcDuW7SuRb34LWibq/2grZM4Fiu6P25ecaV+g2VIgEQIFtrymenibZgdawBYjbOjamzNptM5+/39JH/ub+/6R13PDxYPu0fWWqUDTYfLjDncroghGu20mu7y+kAl8MqcbsX8Lcqfs57IVBpEkDC/oJipTiVZPeQETEB88oDp36PHjtpVmSQDZlqqWsMwFIWCobDowMmvXT3Nq5FCSUmzCsWeDukfT8ca5bm+PRt81VCAsDisUq3sgjcWCEK6aejVlJI1pDme3gFSg07UgvMjw4HrwplNBzrDP6IfY1wP31iPpl1UcVlCv3rl7kDL1QZCWD70QLjn5u43IYtKA+fTl3/E00xWVLP8nYk9AOOgJmtI6d3k2kLLqnLvDxJPOitVII3vafYDel5iCV+Gpy5UaUkAMrd/NgbJaIA52DKFnvkyn6nsM1t9aa9ir1/pGZk/EooHOYy+2cRQHvmDc73zj0/oMpJAKSKG9Q9Ud4w7kiV4VZsWYVjOQe9hCUq4SyajHfJtqAhjPa4Mnqc1+8V/IYLQoINXvbBoA7/TokBC3VaPYpySoneYVnzwJUBk9s7fqRTCpA+f3No/tv0MhWCNv+ToSbggpJgAz/N2tBVu3g/RUA+VatmIqbDrNCyGYTPLDDG4lkxvXxn0Kz488hYS4DxA+4PEu2RMz2AjK2m4KKQ4AYuhQW6KZmFZkCF8Bnf/WVKd4LXOlOvIljTA/gLkX5Z6FtWVAsJlcWh7CLzhhdSYuPalAB6Ai848XsQ9kKNIQHVHtTewqvXmMghjSXdxf1QJcX/e9WOagJ8TQIrMpiwZ/U005OMId6ckCdvqLDwl6Adr0Gbue+aDF+RgNJ5dQ+K5TWe1H6o/+NqWHaJ4qncsrCLEnWYZUg1Dr4i4VywyARp/ksIs3fRe4KyQUfGuCHeLsYPwD0q4DUeNTIwX2q4qCTwa8uCgoJiyc/PDyvBYLBE2/MtnazpqDISUNKZnBzJysqSjIwMSU9PLyVpaWlyPO14SI4fl2PHjp1XituqcKw5T8YX5+M6yImTJyQQOM+Q2ueoUhJyz56VU6dOycmTJ+XEiROSrZKVnSWZWZkhhVmKs8ngbzixlW8rm7+ZmZmG5OzsbHN+hGudPn1aAvm1JHwpIAl3ZFxS4Rdu5svEPgYpKrEk4NJBbWD2AWpJ8AFqSfABKkwCKWRqaqocPny4hBw6dMgESkAcIFCTCbHP3u6FsxrUDxw4YALvl4HzIaSyThCojxzxXn5J24MHWf8avky7fft2iYqK8hTuzQskCdHR0cXt+BwfHy+bN2+W3Fzvl6W4UWESeKBbb71Vfvvb38pvfvObYvnVr34lAwcONG0iZs6Ue+65R66//nr5/e9/L3fddZe0aNFCkpKSzH4n5s+fL7/85S+lSZMm1pbS4CFfeOEFc77rrrtO6tevL5GRkdZekW7dusmf//xnmTNnjrXlC+zZs0d+97vfyf79+60tpVGvXj257LLLPKVjx45Wq5JYsmSJZ3ukQ4cO5yXdRoVJSE5Olquuukruvfdec7F27doZadOmjSxbtsy0YTs3M2LECJk7d640bdpUfvCDHxglHtKeYYMe89BDD8mVV14pP//5z41FurF48WL58Y9/bMjs3LmzkZtuuknmzZtntRB55513zPX+8Y9/GAt1YuvWrWbf7t27rS2lUadOHXn44YcNUW5xn8/G0qVL5Wtf+5oMHjy4RPuGDRrKN77xDdm2bZvVMjwqTMK+ffvkO9/5jgwZMsTaUhoo6ic/+Yn1LQSUyU136tTJ2sLvEs6Yc7Vu3Vouv/xymTp1qrUnBAZiTzzxhPz0pz81YwYnnC6pWbNmcs01Vxtl9+3b19oaAspgOz0iHB544AF59tlnrW9lAyRw3tmzZ1tbQmCwyfbz6cdGpUkYMGCAtaU0IAHrdYIYgQUjNkaPHi3f/e53TQ+67vqQm3GCOHG9bnce44W3335b/va3v8nrr78uP/zhDyX1WGgRMSgrCU8//bT1rWywSYiIiLC2hIC7Zvsbb7xhbQmPSpMwdNhQa0tpeJHACPeGG26Qm2++2doi8vzzz5vvZ7RH4NJwS86gRk/AX0PU+vXer+kBkPCXv/zFtMHtEX9slJWEJ598UvLy8koJg0Uv2CQQ05zAJbF90aJF1pbwqBQJKOXV1141/h5LmBExwwRF20V4uaNZs2aZm+vdu7f5TjZFwGzUqJH5Trdm/8SJE813GwTAK6+6Ur73ve9Jn759zEO6QUyAQNCocSP55je/WeyTy0LC448/btwhxzmFa7qVbAMSOKZt27bm3m255ZZb5Be/+IXR05ehUiRcffXVxr9//etflyuuuEKuuPwK4wZIFcEHH3xgHmLChAnmIbBU9t9+++3FgW7hwoXmHLZSc3Jy5NrrrpUXX3zRfHdixYoVcueddxplkpXh9wnqNmwSCgsKJSUlxSjvkUceMfsI9hy3d+9eY9mky7bYRkNQvu2222Ty5MklZNq0aWFTX1woAZhz28Lz/OxnP5O1a9darc6PSpHAA/fo0cO4EbuYhtiK+fDDD81NEVDtrIjgSdCyQUpKmxEjRxrr56FvvPFG41YYh7iBwpYvX27SY44bOCiUDgObBLui+v7775s2kEc2x+ejR49K+/btQ0ZjyaRJk0x73BGusTyw3dHw4cPN2AWyMDDGPWVFpWOCPSbwAkr4/ve/bwYuDGCcygcoC1eE4jjXt7/9bSO4OZSzTJUdDgTrP/7xj3LttdcWxw+bBCwdUHnl/Fj4xo0bjbLocZAIEWRo/OX+QGUCM262oqg0CV+WHbljghMxMTGm6w4dOtQQYottta1atbJaeoMgTjtK3MBNAiDzog2uC2LPN06AhGeeecb6VjaEy47Kg0qTgALDwSs7cuLdd9+VH/3oR7JrV8nfsaHEu+++W37961+bcjbuzalYwDbiw5/+9CfjDoEXCefOnZN//vOf5lz47i8brJGF4VLcQq+yM6SdO3cWE19WEnCDDBhtV+lEpWPCK6+8EsqMZswwMmXKFFm3bp1pw1CfWOAFYgdBkFKGM7jaINbQSzgXPpbBWpcuXcwImetwXR7eORhiRP6tb32rFGELFiwoDp7nI6Fu3bqmjZeQ7XDPgEpBnz59zGcGn+wneJ8PZH+0Q29uVJgEsg/qNFg6gfeaa64xwne7/tO9e3djqV7AR9O2Z8+e1paSiI2NNedD8QTj5557zrSnNkVpgzoT8QhLt4F7YrubBHoTx5OZ4erCYeXKlcZ9jR07toSMGTPGEGlbMYF8x47QvxjAwseOHRO2wGeDmMi57V7rRIVJ4EFJ+xITE41CbdmwYYMhCBCI7TzdDW6G9uEqqxS+OD/jCBsE4y1bthh34FS+DSq17PcaWNGbuJ6bID+gwiTUoupQS4IPUK0kkHVUFvbovCajQiTge5lBIrVk4BMTG2NqQWQyBDKCEHMI+OG4uDijKDIo/DGjSrIb/Do1FrIbgjCpLvsoVTNRwwCKGDBu3DhzPO1JBznWjjnUgXr16mWCJEU7rs/nVI1F1LAgmbJCWWbrqhMVIoGaSN9+fSV+bby89NJL8thjj8kf/vAHGTZsmNR58EGT8dStV9csyhpsKZnJHupEEEd5d+bMmSZdfOqpp8yECOMCAjGzbqSKpIOQwjiC7UuXLZWEhARDWlR0lLkPBnsPam6/es0aQ0L//v1NMZH7o041ZMhgcy/b9Hg/o0IkkGVg+YmbEqVBgwbycfePTcVw1apV0kbTxJ6f9ZSXX37ZpG0btC3zA6R5KIkCGoM0LB3SmH0bNGiQKS2gPLKbli1bms+RUZEmrYuOiTZVVD6TstILya7oFZ9oGjxh4kRzDka7bGNyiBoUKSzZGsf5GRUigbybFJE0EmWQx9tVUVwO+/lOqsh33AltybOd6SPWzn6E89CGY2lHPehsbqgIxneECSGENsAuknEvVF8RjmMFn3092nils35ChUjgofDR5Pj4W3wvCkUBuBAePC09zVg9isnKzDL7GDfQjiJairWygv2cC5eDr8fP5wfzTVkA8lBousYJtjFmYPWevbIDodKKC/O7os+HCpHAAK1rt65GMYMHD5KN+r1Dx46m+3fq3NlUJgm0uAKm+eo/Ut/4atwRo1rcDecYNnyYCay4qubNm5s2DP/x/QT49h3am2PYt2TpEunatatxN8wDcw7mohs3bmxclz0nUBNRIRJQUtt2bY0b2bRpk/HHlCreevNNaaYKa9K0qSnekSWt0aB56223mjYjR440cwwNGzY029clrJNHH33UBGZiyOrVq812egVZF8qmLYGcEghLWgjk99//b7ONySKWwBAjuJeaigqRgHVj6bghLPrTTz81Vk9QxapR5Pjx401xi4Ie7ofCG1aO1fbr18+koSxz5y91GVJYAvuoUaPM+XFLnI95YjsFJbuivsMECvUbZutoz/HuVRg1CRUioRZVi1oSfIBaEnyAWhJ8gFoSfIBaEnyAWhKqHSL/Dx+VRyUIUR+qAAAAAElFTkSuQmCC'
  */
 
    var img_logotipo = user.empresa.logotipo;
    var img_codigobarras = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAX4AAAAtCAYAAACptwARAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAABORSURBVHhe7doFtFZF18Bxu1BssRu7G1uxFQu7GxW7u7sbFbEDuxvsBFQs7G7sbublt2Xw8X5yl159Wet72Xuts7hnzpzZs+u/5zw6XElJSUlJGaYkwZ+SkpIyjEmCPyUlJWUYkwR/SkpKyjAmCf6UlJSUYUyGCP4BAwYM+uuvy6+//jror/+e1H013Z/7luz5r0hL7Grpflrqw39T15+t9U9921K7hkZO/RMZmnYNTV0tjfe/peuf5tvfkeZ01Wd/dT9N57XEjpbGuak0p7vZE/8333xT+vfvX7766qvB188//1x++eWX8t1335WPP/64fPDBB+WTTz6JMWLTn376afnwww/j3Y8++ijeq8aY9/3338f7X375Zfniiy9ig+7N//rrr8tPP/0U8733ww8/xHNzv/3221ijijnet88q1v7ss89iDeP0u+zHfr1jTt0XfZ9//nns2R4a1zCfvcQezHFZ137M8a412WWt6q8q1vwzXdWWqst79kyqLn5tqoueRl2eVam6+KSu5e8aCzpd1mm0q9pKV41T9X+jb8XCXPr9TRf99kJP9U2dSw+9P/74Y4wR7ze1y2U/dBr3jvWqVF3sMLfmDn3u2VT9YU71iX+r7dY1zzvuredfYzUG9sBvVX+jXf4mbKl22VcV86oP+a3aZT0+Nu65d6tYq+ryvOqq+eO5MXu1xyrGvGdPfGbf1q3x9YzdxugmdY53qq/40fvVJu801WVu9Wf1U42f962DAcarrupDY3Utz+ixnr3IITobfVh9UNch1Xd8Utdo9Il1+NXe7avaZA/GG/fAN3X9mkNVl3U8q3Vq/+Y3in3XGNp3rQPvNLWlUep6/nXZk3uxrjrsh357J9UuczyzTxcdxhu5yz76+anGx3hz0iz4TznllDLrrLOWAw88MK6DDz64vPLKK+Wtt94qV1xxRVlsscXKRBNNVNq3b1/eeeedeEcAVlxxxTLllFOWWWaZpUw99dTl0EMPDaPIG2+8UW644YZy1VVXlb322qtsu+224YyLLroo5h9++OHlmWeeiXXou/XWW8OwPfbYI+Y0CmO32WabctZZZw0aKeWaa64p66+/fnn88cfL6aefXqabbroy00wzlammmqosuuiikTBXX311mXnmmctBBx1Ujj766LLllluWVVZZpVx88cWxxnXXXVfWXHPNMv/885cXXnghxiTJyiuvHLaddtppZffddy9rrbVW2XTTTcv7778f9h955JFl9tlnL/vtt1+8Qy699NLQtf/++5djjjmmbLXVVrFOt27d4vlNN91U1llnnTLPPPOUp59+OsYEcPXVVy/LLrts6GL72muvXTbaaKPy9ttvhz5rzTnnnPGsCp+ydYcddii9evWK5OefySefvMw111zlwgsvLF27di2bb775YLsk0rrrrlsWWmihcuKJJ8baRHLxz5lnnhn3pGfPnmXDDTeM+PubXfQff/zxoYcP7ZfceeedZeONNw5/PPzwwzGmcLy/xBJLlFNPPbXsvffeodv10ksvRcKedNJJZd555y2dOnWKd8jdd98duiaddNKYy85dd9019PHdeeedV4499tjInzZt2gz2yS677BL3s802W9jBfvHji+mnnz7GjckHOWMP/Cbn+ZA/5Cc9/E3uv//+iCFdbKxiv+3atYuaEf8NNtigdOzYsfTt2zdyRy7ysRjWonzwwQfDP9ttt13Y7d8OHTqUww47LADw0EMPhX62y5MqO+20U9gg38Tr1VdfDV+L7fnnnx/P1OUmm2wyGEb9+vWLe3Z4j43qCSjuvffeeCZPZphhhnLjjTfGO+SEE06IONnzzTffHPFbaqmlyhlnnFEuueSSyFu6+K9CjM1zzz13xOr666+PuPPBfffdF/Um99SLPLztttviHSIn2NYIXH+L3SGHHBL5/Oijj4af5Ll9sn/hhReOOC+zzDKRG3L3ySefjPpdb731giGEf3beeedoHvYuh2qTtZ/tt9++HHDAAcEg/pFXjYJ9YshXt99+e9SBtffdd9+yxRZblDvuuGPQzD/KUUcdFXFm+xFHHBE+lHtrrLFGxIBvL7/88tAP9OTNN98siyyySORD9+7dg4H2Ledwd4IJJijLLbdcef3114OncpL/5Jz6qnU8JGkW/Bwz3HDDhfNcNgxOCkSijjLKKPG8devW4RQCTBNOOGGM10tSKmoiAc8999wABLgxQlPgHHM322yzAAWn0qcwgZADzGkUXVPQ99lnn0EjJZJEoXAQADTuY+SRR44AK0L3knXrrbcuCy64YJl44okHF3eXLl3KNNNME3N69+4dYwJiDttAH7w1FQmumbFf8L0D0lWOO+64wWPgCA7W0QyJ5Grbtm3MAQKiSDTOccYZJ3QBD1DNMccc4XvBtpZ3+KUKnxpbeumlS48ePSJRJI8xl4IH8/nmm6/06dMn3pFQM844Yzzfcccdwxby3nvvBbwUfRVFJWFffPHF+Ns79qYQ6WGX/ZLLLrssGpM5tfAUrvdHG220sttuu0X86XY99dRTkawK3zuadBWJb8xlLjsBfJJJJglQOSAAZJ1jT2SFFVYYPMYO9jfNTWPyAcjBQpMx7jAD/vKTHv4mQKb4zWFjlepnzcaBADimnXba8sADD0Tu1FwEACAkYG5s8cUXD7v9q7k5TAD/LbfcEvrNqQcFAnDG5Jt4OSjxtdhqGp65NIB62tXQ3GtQDjqeqyc15LDkWX0PIKt07tw54gRO9iB+rVq1ikObeqnv8J8DHGGzMbE655xzIu4ajAOVepN76kUeNh7m8MZhp8KY+FvsNEz5DLjAVvfJ/hFGGCHucQj85JEmYwwLLrjggliLf6zPJw4K8sO+iP1YV7PCIO9qNo2Cfcb5SuNQB/gEtgsssEAc8v5Mqr/ZLmZ86N5hVAz41iFjySWXjAMvsfaII44Y+XDyyScHA+1bzlXujjvuuOX5558PH8tJNSXnRh999GBEc9Is+J1cKLBZm7fx5557LpwEsDq951NMMUV57bXX4p133303IGW8XoqydrLqLJsFdgUMdBqJubqe0zpAKzadUNfXeDinUTQTXa9ClAApkN9zzz3xldK4D0UPaBqPezYpOIUkSTmYODUppDHHHDOARCSIOWxzKnDSBH3FqkmxHygES+JUqTA2BgoSzzoSj/jKcMIFQ6cZItklh68lutjuZAUuTnf0WUsj86wKn9K12mqrReIrlOWXXz7GJAv/STQntmqXZHIy0mQUs7WJ4nGKANUqThYarfgDoHUVCtjTwy77JU7R4M0f9WQMAN7XIHwBKWa6XfUrT5GPMcYYkRdVrr322tDlMpedGil94AFA9ZDiqj4BYPfAoIidXsWvFs7www8fPpEPCkdh85txPuQP+3C6dFIkYCyG3tf8qoCIQnT6A27rAcQjjzwSuSMXxx577IhhBT+I2dtKK60UdvsXqDRS4Oc3+ukCmipAzAb5Jl6Kn6/F1kGj2qdpVPA/8cQTcc9PGrznas7pWAOqDcbVCGMNS52Ckz2I32STTRYQ9XUjb73D3gp+NhsXG9AVd1/nvhj4Ue6pF3nol4MqeOMUXNch/hY7LJDPGrNDV90n+8cff/y4d1g6++yz42cQX0vqFwtqg+Yf6/sKsnf5YV8EKB0YsA6DrFcPglWwTwzV/JVXXhl1gE++ABy2GvOhUfhbHbBdzPhQjjkEiAHf4in9fpEgL7/8cpzq5YNfNDDQvtVV5S4++HrCUzmppuScg0qt4yFJgj/Bn+BP8Cf4E/y/S4I/wZ/gT/An+H+TBH+CP8Gf4E/wJ/gT/An+BH+CP8FPEvx/lAT/QEnwJ/gT/P8b4Pd/0CT4E/zxToI/wU8S/An+BP9vkuAfKAn+BH+CP8Gf4E/wJ/gT/An+BH+CP8Gf4E/wJ/gT/An+BH+CP8E/8ErwJ/gT/An+BH+CP8Gf4I/7BH+CP8Gf4E/wJ/gT/EOQBH+CP8Gf4E/wJ/h/lwR/gj/Bn+BP8P8mCf4Ef4I/wZ/gT/An+BP8Cf4Ef4KfJPj/KAn+gZLgT/An+BP8Cf4Ef4I/wZ/gT/An+BP8Cf4Ef4I/wZ/gT/An+BP8A68Ef4I/wZ/gT/An+BP8Cf64T/An+BP8Cf4Ef4I/wT8ESfAn+BP8Cf4Ef4L/d0nwJ/gT/An+BP9vkuBP8Cf4E/wJ/gR/gj/Bn+BP8Cf4SYL/j5LgHygJ/gR/gv9/A/zgmeBP8Mc7Cf4EP0nwJ/gT/L/J/zvw77PPPqFAIgMXpzz77LOxqdNPP72MN9548ZwiQCKAbUPG66VhNIIfWLt06RIFCtzAryjN1SQee+yxWAeYL7300jBaoR9//PGxRhVJ2759+0jCKooYSHv06BGOaNyHwgT+CsjNN988AiEBp5tuusGNRfOYddZZI3mffPLJGFO85rBNgnbs2DECp5A5mf01YTbeeON4h0iwOiYBJIh1jj766HgueQVrpJFGioIhwA82k08+eYCE7RqRJJZsb775ZjQsyeNZFT6lS2Hce++9USiamjFz+e+II46IIq92AYMG1qpVqyjyRvCLN2BVue6668oCCywQ8fe3dRUPmNDDLvsliqBdu3Yx54477ogxAPC+hDZv/fXXj4ZGP+gC/5577hkNTSJXueaaa2Idl7mKWwHTx3dHHnlkFFWdY08EIOuYHAFF8QPbOs4n1gOpvn37xoHBOB9qaPJTsxA7Al5iaI7ir8J+oAHw2tA074cffjhyRy7ysRhW8N92222xjqbJ9/5t27Zt6dSpU4BfY6DfnEYYd+jQIWyQb+IFSHwttmAl1t4BmEbwu5c3NU/VnBrS0GusXEBSZbfddos6BSeHFPFr06ZNNHjQkrfe4b8KbDYDnVipdXHnAw1Gvck94JeHwFkFb1ZdddX/A3456BAin++6665osnRqKmqhdevWcT/ppJNGDQD/gw8+GPWLBRhC+Mf6AGrvcqiC336si3UYZD2NolGwTwz5UcNSB9bWGJdYYonSvXv3QTP/KJ07d4712K6p86F7UBcDvsVTTb4R/JqMfDjjjDMGg1/O4Zj38QH4+VhOqik554DLL81Js+CXjBQwzKWzOZm99NJLATRO8FwDACQC2BqB8XppGk7nxEaBlTHgCbpffPFFJKy5YA+Ab7/9duiT8N98800UcdMOLGl1X0VVReABWYIowsZ9OEn6ItFB3StQQQZvXy3AQDQGDldAffr0iTGNi6Mll5OgBHLiVOCczH4NzrqNp/Da0IwJupMZXZKQCNoss8wScxQM0QinnXbaCCBdbJckICkh3njjjVjLO55V4VNjYNGzZ88oFAlpzKXZ+DpyOgMCAgyanAL2FVETRoPUrCrICQADrfg7eVjTyVhC08Mu+yUKQ9GZA3AEALw/1lhjBeABWqLS7xSoYO3BO0suuWS8QyR7tcHcejqij+8OO+ywwcXlsidST4YuzZr9TXPTmPXmmGOO2AO/GedDzUR+OknyN7nxxhsjhuY0Qov9GpZmIdYa9QwzzBAAAj25yMdgWMHv68E6ipbd/qVLwwX+W2+9NfSbI0+qVPDJN/ECJL4WW03QMxcYV/D37t077kHf6dlzX539+/ePJu5Zfa9bt27xDtEo1Ck4Aa34+ToEaU2zvsN/FdhO28bEqmvXroPBr8GoN7nnoCQPL2n4ksEb8HPwqWJNsQNk+ewQURuvfaqFUUcdNe6d/AHUoeX++++PMSyoTZN/rA+g9i6HKvjtxyFygw02CAZ5V141CvaJIV/5ilAHmqGvf42zMR8apfqb7WLGh+4dXDCNb/3a4PDg8EOsrXHJB/8tBfjFUl1V7uKDrz0+lpNqSs5phPzSnDQL/gceeCAcxNkun55gy1mKRMH4KcCJCbyJQNkIsAGpRAThmoAA6oTihCfxwURAJab5PuUETvDp69evXySOk5Y5jfLdd98FgCowieDowk5CElDXluD2A4z2RzddbFLoTqeSqEJeITn1aG61A9u/hsU2xez0IqEEAqj5xFcG0FfQEUlOlzGnyarLz1lE4CSPwLOb8Ici04DoYrs5Eose+qzlC8WzKuxiq59hNE4JxT8S2Fz+8zXlZFLtAiHF5ycpxWJtotnyf6NvNTcJr4j9zS76fV3Qwy77JRLXfvmjJiGYeV/jNQ/Y6HYBEJ32wBfyoopmRxc7zGUnANPHL3zJH+b4WaD6xBz3cpgd7Bc/pz3AN27MenLGHvjNOB/62cc+6LE+cTIUQ7p8vVZhPx+qGbG2nvzQQOWOXPSc/fxA/LzAP37KYrd/6VIDAwYMCL/RT5c8qWJvbJBv4uVQxddi26tXr4i1upSbtckAint2eI+N8l0NgZ5n/AtE6rMKH6hT+7EH+3e4Yo/3xYou9qpTwmb7Eyu1xF6w5Tv1JvfwQh46CFaRE5qD/K9iTbGTi/JZo+MneW6f7Ad7ccYhX23yyAFP/doXhhD+sb511KX8sC9iP9b1lcX//FNrtAr2iSFfib1atLZ4O5w05kOjsFmc/cv3fGh9NS7/+NZXkAYM8ESN4ZV8wFpxtG91hbu+YPGh8lRO8p+cU1+1jockQwS/zfxdqQn935S6r6b7c9+SPf8VaYldLd1PS3W15L0hvfNn+/6nvm1pbrT0vaElQ9OuoamrpfFuqa6m+v5pvv0daU5XffZX9/Nv2NHSODeV5nQ3e+JPSUlJSfnfkwR/SkpKyjAmCf6UlJSUYUpK+Q/c6LlAK7RpugAAAABJRU5ErkJggg=='
    var img_telefone = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAAAoCAYAAABuIqMUAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAKFSURBVFhH7Zg9a2JBFIYNWkUDWmiMYBOM6Sw06C8QsUpjEyVqEbCzEASRoK2tgfwFwcZKCztBtLCISIokWlv41YlfqGc57tzFjcPu3KvOZVkfeBvnnOEZcGbuvQr4hznJy8VJXi7+H/l0Og0qlQoUCgU8Pj7CZDIhI/LALJ/JZDbS2wkGgzCdTkkFf5jkG40GnJ+f78hjQqEQzGYzUskXJvlwOEwVF4LjciyASf7y8pIqvZ2npydYLBakgw9M8jRZWkqlEungw0Hlc7kc6eDDweQdDgf0+33SwQcmea1WSxUWYrPZoN1uk2p+MMl7PB6qNObm5gY6nQ6p5AuT/OvrK1UcY7FYYDAYkEq+MMl3u10wGo1UeUw8HieVfGGSR56fn6niGI1GA/V6nVTyg1m+1+tt/t80eczd3R2MRiNSzQdmeaRQKIBSqaTKYx4eHrg+JoiSR2KxGFVcSCQSgfl8TqqPi2j58XgMXq+XKi4kEAjAcDgkHb/z9fUFLy8vUCwWYb1ek1+lIVoewdPHbrdTxYU4nU6o1Wqk4yeVSgWurq4242dnZ5BIJPZagCR55OPjA6xW6470di4uLiCZTG7ugWq1CgaDYacGx6UuQLI80mw24fr6ekfoe/Aio4kLwWNYygL2kkfe3t7g9vaWKiUmqVQKVqsVmZWNveWRz89PcLlcVCkxwfdkMRxEHsHHYb/fT5ViDe4RMRxMHlkul5DNZkGv11Pl/haz2UxmYuOg8gKtVgt8Pt8fb+Pvwa8T+XyezMDGUeQRPD3K5TLc39+DWq2mCgvBs1/KK+TR5Ld5f3/fbEa32w06ne6XtMlkgmg0utnwUuAifyxO8nJxkpeLk7w8APwA6uMHnXdcNTgAAAAASUVORK5CYII='
    var doc = new jsPDF()
    doc.addImage(img_logotipo, 'JPEG', 10, 17, 24, 20)
   // doc.addImage(img_codigobarras, 'PNG', 128, 39, 69, 10)

    doc.setFontSize(9);
    doc.setFont("arial");
    doc.setTextColor(0);
    /*     doc.text('EMPRESA PÚBLICA DE ÁGUAS E SANEAMENTO DO NAMIBE', 37, 20);
        doc.setTextColor(0);
        doc.text('Av. Eduardo Mondlane, n.º 139, Moçâmedes', 37, 25);
        doc.text('NIF: 5161162392', 37, 30);
        doc.text('Email: geral@epasnamibe.co.ao', 37, 34);
        doc.text('Telefone: +244 933 284 256', 106, 27);
        doc.text('WebSite: www.epasnamibe.co.ao', 100, 31); */
    factura.created_at = moment(factura.created_at).format('DD/MM/YYYY');
    doc.setFontSize(9);
    doc.setFont("arial");
    doc.setTextColor(0);
    doc.text('' + user.empresa.companyName, 37, 20);
    doc.setTextColor(0);
    doc.text('' + user.empresa.addressDetail, 37, 25);
    doc.text('NIF: ' + user.empresa.taxRegistrationNumber, 37, 30);
    doc.text('Email: ' + user.empresa.email, 37, 34);
    doc.text('Telefone: ' + user.empresa.telefone, 106, 27);
    doc.text('WebSite: ', 100, 31);
    doc.setTextColor(0, 0, 255);
    doc.text('www.epasnamibe.co.ao', 114, 31);
    doc.text('___________________', 114, 31);


    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(149, 16, 45, 17, 'B'); // filled red square
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('RECIBO N.º', 150, 20);
    doc.text('Data Emissão:', 150, 24);
    doc.text('Data de Vencimento:', 150, 28);
    doc.text('' + original, 150, 32);

    doc.setFontSize(8);
    doc.setFontType("normal");
    /*     doc.text('xxxxxxxxxxxxxxxxx', 169, 20);
        doc.text('xx/xx/xxxx', 170, 24);
        doc.text('xx/xx/xxxx', 179, 28); */
    doc.text('' + factura.factura_sigla, 169, 20);
    doc.text('' + factura.created_at, 170, 24);
    doc.text('' + (factura.data_vencimento == null ? '' : moment(factura.data_vencimento).format('DD/MM/YYYY')), 179, 28);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(10, 41, 105, 26, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.addImage(img_telefone, 'PNG', 15, 42, 4, 12)
    /*     doc.text('APOIO AO CLIENTE', 20, 50);
        doc.setFontType("normal");
        doc.text('ROTURAS/ AVARIAS/ FUGAS DE ÁGUA/ FALTA DE ÁGUA: 942 887 934', 15, 55);
        doc.text('APOIO COMERCIAL: 934 391 301', 15, 58.5);
        doc.text('GERAL: 933 284 256', 83, 58.5);
        doc.text('EMAIL: clientes@epasnamibe.co.ao', 15, 61.5);
        doc.text('WEBSITE E PORTAL DO CLIENTE: www.epasnamibe.co.ao', 15, 64.5); */
   if (this.app_environment == 'epasn-qas.unig-erp.com:3355' || this.app_environment == 'epasn.unig-erp.com:3347' || this.app_environment == 'localhost:3333') {
    doc.text('APOIO AO CLIENTE', 20, 50);
    doc.setFontType("normal");
    doc.text('ROTURAS/ AVARIAS/ FUGAS DE ÁGUA/ FALTA DE ÁGUA: 942 887 934', 15, 55);
    doc.text('APOIO COMERCIAL:  934 391 301' , 15, 58.5);
    doc.text('GERAL: 944 090 258', 83, 58.5);
    doc.text('EMAIL: clientes@epasnamibe.co.ao', 15, 61.5);
    doc.text('WEBSITE E PORTAL DO CLIENTE: www.epasnamibe.co.ao', 15, 64.5);

  }
    doc.setFontSize(8);
    doc.setFont("arial");
    doc.setFontType("normal");
    /*     doc.text('MARIA DAS DORES', 130, 51);
        doc.text('Bairro Valódia, Quarteirão 01', 130, 55);
        doc.text('Número 016, Apartamento 10', 130, 59);
        doc.text('Moçâmedes', 130, 63); */

    doc.text('' + cliente.nome, 130, 51);
    doc.text('' + cliente.morada, 130, 55);
    doc.text('' + cliente.municipio.nome, 130, 59);

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setFont("arial");
    /*     doc.text('CONTRATO N.º:', 10, 75);
        doc.text(' XXXXXXXXXX', 31, 75);
        doc.text('CIL: TT05-025-C05', 10, 79);
        doc.text('Morada Local de Consumo:', 10, 83);
        doc.setFontType("normal");
        doc.text('Bairro Torre do Tombo, Quarteirão 05', 10, 87);
        doc.text('Número 025, Apartamento 05', 10, 90);
        doc.text('Moçamedes', 10, 93);
    
        doc.setFontType("bold");
        doc.text('CLIENTE N.º: XXXXXXXX', 85, 75);
        doc.text('NIF: ', 85, 83);
        doc.text('Tarifa:', 85, 87);
        doc.setFontType("normal");
        doc.text('MARIA DAS DORES', 85, 79);
        doc.text(' XXXXXXXXXX', 90, 83);
        doc.text('Doméstico', 95, 87); */

    doc.text('CONTRATO N.º: ', 10, 75);
    doc.text(' ' + (contrato == null || contrato == "null" ? "" : contrato.id), 32, 75);
    doc.text('CIL: ' + (contrato == null ? "" : contrato.localconsumo == null ? '' : contrato.localconsumo.cil), 10, 79);
    doc.text('Morada Local de Consumo:', 10, 83);
    doc.setFontType("normal");
    doc.text('Bairro ' + (contrato == null ? "" : contrato.localconsumo.bairro + ', Quarteirão ' + contrato.localconsumo.quarteirao), 10, 87);
    //doc.text('Número 025, Apartamento 05', 10, 90);
    doc.text('' + (contrato == null ? "" : contrato.localconsumo.municipio), 10, 93);

    doc.setFontType("bold");
    doc.text('CLIENTE N.º: ' + cliente.id, 85, 75);
    doc.text('NIF: ', 85, 83);
    doc.text('Tarifa: ', 85, 87);
    doc.setFontType("normal");
    doc.text('' + cliente.nome, 85, 79);
    doc.text(' ' + cliente.numero_identificacao, 95, 83);
    doc.text('' + (contrato == null ? "" : contrato.tarifa == null ? '' : contrato.tarifa.descricao), 95, 87);

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, 110, 182, 5, 'B');
    doc.setFontSize(8)
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('RECIBO N.º', 13, 113.5);
    doc.text(''+ factura.factura_sigla, 32, 113.5);
    doc.text('de '+ factura.created_at, 65, 113.5);
    doc.setFontType("normal");
    doc.text('_________________________________________________________________________________________________________________________________', 12, 114.5);

    doc.setFontType("bold");
    doc.text('Documentos Liquidados', 20, 118);
    doc.text('TOTAL (Kz)', 163, 118);


    var vectItens1 = [
      ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
      ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
      /*              ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],
            ['Factura xxxxxxxxxxxxxxxxxxxx', '19.000,00'],  */
    ]



    doc.setTextColor(0);
    doc.setFontType("normal");
    doc.text('_________________________________________________________________________________________________________________________________', 12, 119);

    doc.setTextColor(0);
    var posY = 123;
    var totalImposto = 0;
    for (var i = 0; i < produtos.length; i++) {

      const data = produtos[i];
      doc.text(''+ (data.produto == null ? '' : data.produto.nome), 20, posY);
      doc.text(''+ this.numberFormat(data.total), 163, posY);
      posY += 5;
      totalImposto += data.valor_imposto;

      /*       const hasReachedPageLimit = doc.internal.pageSize.height < (posY + 5)
      
            if (hasReachedPageLimit) {
              posY = 10;
              doc.addPage()
            } */
    }

    doc.text(''+(produtos[0].imposto.descricao == null ? 'IVA (14%)' : produtos[0].imposto.descricao), 20, posY + 10);
    doc.text(''+this.numberFormat(totalImposto), 163, posY + 10);

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, posY + 18, 182, 5, 'B');
    doc.text('TOTAL RECIBO', 13, posY + 21);
    doc.text('' + this.numberFormat(factura.total), 163, posY + 21);
    doc.setFontType("normal")
    doc.text('_________________________________________________________________________________________________________________________________', 12, posY + 17);
   /*  doc.setFontType("bold")
    doc.text('Mensagens Importantes', 10, posY + 57);
    doc.setFontType("normal");
    doc.text('_____________________', 10, posY + 57);

    doc.setFontSize(8)
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(10, posY + 58.7, 66, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Débito Directo:', 12, posY + 62);
    doc.setFontType("normal");
    doc.text('Efectue os seus pagamentos por Débito Directo.', 12, posY + 65);
    doc.text('Directo. É automático e sem preocupações. ', 12, posY + 68);
    doc.text('Adira já.', 12, posY + 71);

    doc.setFontSize(8);
    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(76, posY + 58.7, 61, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Factura Digital:', 77, posY + 62);
    doc.setFontType("normal");
    doc.text('Evite deslocações desnecessárias. Receba a', 77, posY + 65);
    doc.text('sua factura por email ou WhatsApp e efectue', 77, posY + 68);
    doc.text('os seus pagamentos no Multicaixa ou ', 77, posY + 71);
    doc.text('Multicaixa Express. Adira já.', 77, posY + 73.9);

    doc.setDrawColor(0);
    doc.setFillColor(255);
    doc.rect(135, posY + 58.7, 60, 20, 'B');
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Poupe Água:', 136, posY + 62);
    doc.setFontType("normal");
    doc.text('Feche as torneiras e não deixe mangueiras ', 136, posY + 65);
    doc.text('largadas com água aberta. Não desperdice ', 136, posY + 68);
    doc.text('água.', 136, posY + 71);
    doc.text('-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------', 10, posY + 83);
 */
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('Cliente n.º', 12, posY + 86.5);
    doc.text('' + cliente.id, 28, posY + 86.5);
    doc.text('Contrato n.º', 158, posY + 86.5);
    doc.text(''+ (contrato == null || contrato == "null" ? "" : contrato.id), 175, posY + 86.5);
    doc.setFontType("normal");
    doc.text('___________________________________________________________________________________________________________________________________', 10, posY + 87.3);

    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(10, posY + 88, 185, 5, 'B');
    doc.setFontSize(8)
    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.text('RECIBO N.º', 12, posY + 91.5);
    doc.text('' + factura.factura_sigla, 28, posY + 91.5);
    doc.text('de ' + factura.created_at, 65, posY + 91.5);
    doc.setFontType("normal");
    doc.text('___________________________________________________________________________________________________________________________________', 10, posY + 92.3);

    doc.setFontType("bold");
    doc.text('Documentos Liquidados', 20, posY + 95.5);
    doc.text('TOTAL (Kz)', 163, posY + 95.5);
    doc.setTextColor(0);
    doc.setFontType("normal");
    doc.text('___________________________________________________________________________________________________________________________________', 10, posY + 96.3);

    doc.setTextColor(0);
    /*     doc.text('Factura xxxxxxxxxxxxxxxxxxxx', 20, 233);
        doc.text('25/01/2020', 97.5, 233);
        doc.text('7.500,00', 182.5, 233);
    
        doc.text('Factura xxxxxxxxxxxxxxxxxxxx', 20, 236);
        doc.text('25/02/2020', 97.5, 236);
        doc.text('10.000,00', 181, 236); */
        var totalImposto = 0;
    for (var i = 0; i < produtos.length; i++) {

      const data = produtos[i];

      doc.text(''+ (data.produto == null ? '' : data.produto.nome), 20, posY + 100);
      doc.text(''+ this.numberFormat(data.total), 163, posY + 100);
      posY += 5;

      totalImposto += data.valor_imposto;

      const hasReachedPageLimit = doc.internal.pageSize.height < (posY + 20)

      if (hasReachedPageLimit) {
        posY = 1;
        doc.addPage();
      }
    }

    doc.text(''+(produtos[0].imposto.descricao == null ? 'IVA (14%)' : produtos[0].imposto.descricao), 20, posY + 119);
    doc.text(''+this.numberFormat(totalImposto), 163, posY + 119);

    doc.setTextColor(0);
    doc.setFontType("bold");
    doc.setDrawColor(250);
    doc.setFillColor(220, 220, 220);
    doc.rect(12, posY + 126.5, 182, 5, 'B');
    doc.text('TOTAL RECIBO', 13, posY + 130);
    doc.text('' + this.numberFormat(factura.total), 163, posY + 130);
    doc.setFontType("normal");
    doc.text('_________________________________________________________________________________________________________________________________', 12, posY + 126.5);

    doc.setFontSize(7);
    doc.text("NIF: " + user.empresa.taxRegistrationNumber + " - " + user.empresa.companyName + " / " + user.empresa.addressDetail + " / " + user.empresa.telefone + " / " + user.empresa.email, 105, doc.internal.pageSize.height - 6, null, null, 'center');

    doc.setFontSize(8);
    doc.text('-Processado por programa validado nº 4/AGT119', 105, doc.internal.pageSize.height - 3, null, null, 'center');
    doc.autoPrint();
    doc.output("dataurlnewwindow");

    
  }

  public numberFormat(number) {
    return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(number).replace('€', '').trim();
  }
}
