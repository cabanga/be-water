


import { Injectable, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from 'src/app/providers/config/config.service';

@Injectable({
  providedIn: 'root'
})
export class ReportClienteService {

  public pdfEvent = new EventEmitter<Object>();

  constructor(private configService: ConfigService) { }

  public relatorioExtraccaoClientes(p = 'print', file: any) {

    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
    doc.setProperties({
      title: 'Relatorio_Extraccao_Clientes',
      subject: 'Report',
      author: 'Unig',
      keywords: '',
      creator: 'VistaWater'
    });

    doc.addImage(imgData, 'JPEG', 140, 10, 28, 18)
    const totalPagesExp = "{total_pages_count_string}";

    doc.setFontType('bold')
    doc.setFontSize(15)
    doc.text(150, 50, 'Relatório de Extracção de Clientes', 'center')

    doc.autoTable({
      html: file,
      didParseCell: function (data) {
        var rows = data.table.body;
        if (data.row.index === 0) {
          data.cell.styles.fontStyle = 'bold';
        }
      },
      didDrawPage: data => {
        let footerStr = "Página " + doc.internal.getNumberOfPages();
        if (typeof doc.putTotalPages === 'function') {
          footerStr = footerStr + " de " + totalPagesExp;
        }
        doc.setFontSize(10);
        doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
      },
      styles: { textColor: [0, 0, 0], font: 'helvetica', fontStyle: 'normal' },
       
      columnStyles: {
        0: { cellWidth: 7, halign: 'center', fontSize: 10 },
        1: { cellWidth: 14, halign: 'center' },
        2: { cellWidth: 17, halign: 'center' },
        3: { cellWidth: 20, halign: 'center' },
        4: { cellWidth: 25, halign: 'center' },
        5: { cellWidth: 10, halign: 'center' },
        6: { cellWidth: 10, halign: 'center' },
        7: { cellWidth: 10, halign: 'center' },
        8: { cellWidth: 10, halign: 'center' },
        9: { cellWidth: 10, halign: 'center' },
        10: { cellWidth: 10, halign: 'center' },
        11: { cellWidth: 10, halign: 'center' },
        12: { cellWidth: 10, halign: 'center' },
        13: { cellWidth: 10, halign: 'center' },
        14: { cellWidth: 10, halign: 'center' },
        15: { cellWidth: 10, halign: 'center', overflow: 'hidden', fillColor: [255, 255, 255] }
      },
      rowsStyles: {},
      startY: 60,
      theme: 'grid',

    })

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp);
    }

    if (p === 'save') {
      doc.save();
    } else {
      doc.autoPrint();
      doc.output("dataurlnewwindow");
    }
  }

  public relatorioClientes(p = 'print', file: any) {

    var imgData = this.configService.logotipoBase64;
    var doc = new jsPDF('l', '', 'a4')
    doc.setProperties({
      title: 'Listagem_Clientes',
      subject: 'Report',
      author: 'Unig',
      keywords: '',
      creator: 'Angola Telecom'
    });

    doc.addImage(imgData, 'JPEG', 140, 10, 28, 18)
    const totalPagesExp = "{total_pages_count_string}";

    doc.setFontType('bold')
    doc.setFontSize(15)
    // doc.text(145, 60, 'Relatório', 'center')
    doc.setFontSize(15)
    doc.text(150, 50, 'Listagem de Clientes', 'center')

    doc.autoTable({
      html: file,
      didParseCell: function (data) {
        var rows = data.table.body;
        if (data.row.index === 0) {
          data.cell.styles.fontStyle = 'bold';
        }
      },
      didDrawPage: data => {
        let footerStr = "Página " + doc.internal.getNumberOfPages();
        if (typeof doc.putTotalPages === 'function') {
          footerStr = footerStr + " de " + totalPagesExp;
        }
        doc.setFontSize(10);
        doc.text(footerStr, data.settings.margin.left, doc.internal.pageSize.height - 10);
      },
      styles: { textColor: [0, 0, 0] },
      columnStyles: {
        0: { cellWidth: 20, halign: 'center' },
        1: { cellWidth: 40, halign: 'center' },
        2: { cellWidth: 23, halign: 'center' },
        3: { cellWidth: 25, halign: 'center' },
        4: { cellWidth: 25, halign: 'center' },
        5: { cellWidth: 30, halign: 'center' },
        6: { cellWidth: 30, halign: 'center' },
        7: { cellWidth: 25, halign: 'center' },
        8: { cellWidth: 25, halign: 'center' },
        9: { cellWidth: 25, halign: 'center' },
        10: { cellWidth: 25, halign: 'center', overflow: 'hidden', fillColor: [255, 255, 255] }
      },
      rowsStyles: {},
      startY: 60,
      theme: 'grid',

    })

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp);
    }

    if (p === 'save') {
      doc.save();
    } else {
      doc.autoPrint();
      doc.output("dataurlnewwindow");
    }


  }
}
