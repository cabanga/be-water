import { Injectable } from '@angular/core';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from 'src/app/providers/config/config.service';

@Injectable({
  providedIn: 'root'
})
export class ReportLojaService {

  constructor(private configService: ConfigService) { }

  public relatorioLoja(relatorio: any, filter: any, p = 'print',imgData) {


    var doc = new jsPDF('l', '', 'a4')
    doc.addImage(imgData, 'JPEG', 130, 17, 24, 20)
    var name_relatorio = filter.loja_nome

    doc.setFontType('bold')
    doc.setFontSize(15)
    doc.text(145, 60, 'Relatório Loja', 'center')
    doc.setFontSize(15)
    name_relatorio==null?'':doc.text(145, 70, 'Loja ' + name_relatorio, 'center')

    doc.setDrawColor(150);
    doc.setFillColor(255);
    doc.setFontSize(7);
    doc.setTextColor(0);

    var posY = 0;
    doc.rect(5, 80 + posY, 40, 6, 'B'); // filled red square
    doc.rect(45, 80 + posY, 20, 6, 'B'); // filled red square
    doc.rect(65, 80 + posY, 30, 6, 'B'); // filled red square
    doc.rect(95, 80 + posY, 80, 6, 'B'); // filled red square
    doc.rect(175, 80 + posY, 45, 6, 'B'); // filled red square
    doc.rect(220, 80 + posY, 45, 6, 'B'); // filled red square
    doc.rect(265, 80 + posY, 25, 6, 'B'); // filled red square

    doc.setFontType("bold");
    doc.text('Número Factura', 25, 84 + posY, 'center');
    doc.text('Data', 55, 84 + posY, 'center');
    doc.text('Número Cliente', 80, 84 + posY, 'center');
    doc.text('Nome Cliente', 135, 84 + posY, 'center');
    doc.text('Valor', 200, 84 + posY, 'center');
    doc.text('Valor Pago', 240, 84 + posY, 'center');
    doc.text('Estado', 275, 84 + posY, 'center');
    posY = 5;
    var total = 0
    var total_valor_pago = 0
    for (var i = 0; i < relatorio.length; i++) {
      doc.setDrawColor(150);
      doc.setFillColor(255);
      doc.rect(5, 80 + posY, 40, 6, 'B'); // filled red square
      doc.rect(45, 80 + posY, 20, 6, 'B'); // filled red square
      doc.rect(65, 80 + posY, 30, 6, 'B'); // filled red square
      doc.rect(95, 80 + posY, 80, 6, 'B'); // filled red square
      doc.rect(175, 80 + posY, 45, 6, 'B'); // filled red square
      doc.rect(220, 80 + posY, 45, 6, 'B'); // filled red square
      doc.rect(265, 80 + posY, 25, 6, 'B'); // filled red square

      doc.setFontType("normal");
      doc.text('' + relatorio[i].factura_sigla, 25, 84 + posY, 'center');
      doc.text('' + relatorio[i].data, 55, 84 + posY, 'center');
      doc.text('' + relatorio[i].cliente_id, 80, 84 + posY, 'center');
      doc.text('' + relatorio[i].nome, 100, 84 + posY);
      doc.text('' + this.configService.numberFormat(relatorio[i].total), 218, 84 + posY, 'right');
      doc.text('' + (relatorio[i].pago == 1 ? this.configService.numberFormat(relatorio[i].total) : this.configService.numberFormat(relatorio[i].total - relatorio[i].valor_aberto)), 262, 84 + posY, 'right');
      doc.text('' + (relatorio[i].pago == 1 ? 'Pago' : 'Não Pago'), 275, 84 + posY, 'center');
      total += relatorio[i].total;
      total_valor_pago += (relatorio[i].pago == 1 ? (relatorio[i].total) : (relatorio[i].total - relatorio[i].valor_aberto));
      posY += 6;


      if (doc.internal.pageSize.height < (posY + 20)) {
        posY = posY - 250;
        doc.addPage();
      }
    }
    doc.setFontType("bold");
    doc.setDrawColor(150);
    doc.setFillColor(255);
    doc.rect(5, 80 + posY, 170, 6, 'B'); // filled red square
    doc.rect(175, 80 + posY, 45, 6, 'B'); // filled red square
    doc.rect(220, 80 + posY, 45, 6, 'B'); // filled red square
    doc.rect(265, 80 + posY, 25, 6, 'B'); // filled red square
    doc.text('Total', 170, 84 + posY, 'right');
    doc.text('' + this.configService.numberFormat(total), 218, 84 + posY, 'right');
    doc.text('' + this.configService.numberFormat(total_valor_pago), 262, 84 + posY, 'right');




    if (p === 'save') {
      doc.save();
    } else {
      doc.autoPrint();
      doc.output("dataurlnewwindow");
    }


  }
}
