import { Injectable } from '@angular/core';
import 'jspdf-autotable';
import * as moment from 'moment';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { ConfigService } from 'src/app/providers/config/config.service';

@Injectable({
  providedIn: 'root'
})
export class ReportResumoContaCorrenteService {

  constructor(private configService: ConfigService) { }

  public relatorioContaCorrente(p = 'print', file,imgData) {
    var doc = new jsPDF('l', '', 'a4')
    doc.setProperties({
      title: 'Listagem_Resumo_Contas_Correntes',
      subject: 'Report',
      author: 'Unig',
      keywords: '',
      creator: 'unig bewater'
   });

    doc.addImage(imgData, 'JPEG',14, 10, 28, 18)
    const totalPagesExp = "{total_pages_count_string}";

        doc.setFontType('bold')
        doc.setFontSize(15)
        doc.setFontSize(15)
        doc.text(150, 25, 'Resumo de Contas Correntes', 'center')

     doc.autoTable({ html: file ,
    didParseCell: function (data) {
    var rows = data.table.body;
    if (data.row.index === 1) {
      data.cell.styles.fontStyle = 'bold';
      data.cell.styles.textColor = "white";
      data.cell.styles.fillColor = [32, 95, 190];
    }
  },
    addPageContent: data => {
      let footerStr = "Página " + doc.internal.getNumberOfPages();
      if (typeof doc.putTotalPages === 'function') {
        footerStr = footerStr + " de " + totalPagesExp;
      }
      doc.setFontSize(10);
      doc.text(footerStr,  267, 200, 'left');
    },
    styles: { textColor: [0, 0, 0] },
    columnStyles: {
    0: {cellWidth: 20,halign: 'center',},
    1: {cellWidth: 60,halign: 'center'},
    2: {cellWidth: 25,halign: 'center'},
    3: {cellWidth: 25,halign: 'center'},
    4: {cellWidth: 25,halign: 'center'},
    5: {cellWidth: 30,halign: 'center'},
    6: {cellWidth: 23,halign: 'center'},
    7: {cellWidth: 25,halign: 'center'},
    8: {cellWidth: 35,halign: 'center'}},
  startY: 60,
  theme: 'grid',

  })

  if (typeof doc.putTotalPages === 'function') {
    doc.putTotalPages(totalPagesExp);
  }

     if (p === 'save') {
       doc.save('Listagem_Resumo_Contas_Correntes'+'.pdf');
     } else {
       doc.autoPrint();
       doc.output("dataurlnewwindow");
     }


   }
}
