import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditProvinciaComponent } from './create-or-edit-provincia.component';

describe('CreateOrEditProvinciaComponent', () => {
  let component: CreateOrEditProvinciaComponent;
  let fixture: ComponentFixture<CreateOrEditProvinciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditProvinciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditProvinciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
