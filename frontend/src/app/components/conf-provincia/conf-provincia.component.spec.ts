import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfProvinciaComponent } from './conf-provincia.component';

describe('ConfProvinciaComponent', () => {
  let component: ConfProvinciaComponent;
  let fixture: ComponentFixture<ConfProvinciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfProvinciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfProvinciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
