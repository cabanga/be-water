import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfTiposTipologiaComponent } from './conf-tipos-tipologia.component';

describe('ConfTiposTipologiaComponent', () => {
  let component: ConfTiposTipologiaComponent;
  let fixture: ComponentFixture<ConfTiposTipologiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfTiposTipologiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfTiposTipologiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
