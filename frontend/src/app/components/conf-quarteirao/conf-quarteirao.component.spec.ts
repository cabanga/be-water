import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfQuarteiraoComponent } from './conf-quarteirao.component';

describe('ConfQuarteiraoComponent', () => {
  let component: ConfQuarteiraoComponent;
  let fixture: ComponentFixture<ConfQuarteiraoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfQuarteiraoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfQuarteiraoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
