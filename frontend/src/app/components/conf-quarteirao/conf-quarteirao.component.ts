import { Component, OnInit, Input, createPlatformFactory } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { ExcelService } from 'src/app/services/excel.service';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';
import { ConfigModuloService } from 'src/app/services/config-modulo.service';

@Component({
  selector: 'app-conf-quarteirao',
  templateUrl: './conf-quarteirao.component.html',
  styleUrls: ['./conf-quarteirao.component.css']
})
export class ConfQuarteiraoComponent implements OnInit {

  @Input() simpleFormQuarteirao: FormGroup;
  private distrito_view: boolean = false;

  private quarteirao = {
    id: null,
    zona: null,
    rua_id: null,
    bairro_id: null,
    has_distrito: false,
    municipio_id: null,
    distrito_id: null,
    provincia_id: null,
    is_active: null,
    user_id: null,
  };

  @Input() ruas: any[];
  @Input() bairros: any[];
  @Input() distritos: any[];
  @Input() municipios: any = [];

  private addRows: boolean = true;
  private title: string = null;

  private items: any = [];
  private quarteiraos: any = [];

  constructor(
    private http: HttpService,
    private configService: ConfigService,
    private excelService: ExcelService,
    private config: ConfigModuloService
  ) { }



  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "lista_quarteiraos -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }

  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

  ngOnInit() {
    this.getPageFilterData(1);
  }

  private getDadosModal() {

  }

  private getQuarteiroes() {

    this.configService.loaddinStarter('start');

    this.http.__call('quarteirao/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  private initQuarteirao() {

    this.quarteirao.id = null;
    this.quarteirao.zona = null;
    this.quarteirao.bairro_id = null;
    this.quarteirao.has_distrito = null;
    this.quarteirao.distrito_id = null;
    this.quarteirao.municipio_id = null;
    this.quarteirao.provincia_id = null;
    this.quarteirao.is_active = null;
    this.quarteirao.user_id = null;

    this.distritos = null;
    this.municipios = null;
    this.bairros = null;
    this.ruas = null;

    this.addRows = true;
  }

  private setDataQuarteirao(item) {

    if (item !== undefined) {
      this.title = "Editar Quarteirao";

      this.quarteirao.id = item.id;
      this.quarteirao.zona = item.nome;
      this.quarteirao.bairro_id = item.bairro_id;

      this.quarteirao.has_distrito = (item.distrito_id > 0) ? true : false;
      this.quarteirao.distrito_id = item.distrito_id;
      this.quarteirao.municipio_id = item.municipio_id;
      this.quarteirao.provincia_id = item.provincia_id;
      this.quarteirao.is_active = item.is_active;
      this.quarteirao.user_id = item.user_id;

      if (this.quarteirao.has_distrito) this.selectBoxDistritosByMunicipio(item.municipio_id);

      this.selectBoxMunicipiosByProvincia(item.provincia_id);
      this.selectBoxBairrosByMunicipio(item.municipio_id);

      this.addRows = false;

      console.log(this.quarteirao);
    }


  }

  private updateStateQuarteirao(item) {

    this.quarteirao.id = item.id;
    this.quarteirao.zona = item.nome;
    this.quarteirao.bairro_id = item.bairro_id;
    this.quarteirao.has_distrito = item.has_distrito;
    this.quarteirao.distrito_id = item.distrito_id;
    this.quarteirao.municipio_id = item.municipio_id;
    this.quarteirao.provincia_id = item.provincia_id;
    this.quarteirao.is_active = !item.is_active;
    this.quarteirao.user_id = item.user_id;

    //console.log(item);
    this.http.__call('quarteirao/update/' + this.quarteirao.id, this.quarteirao).subscribe(
      response => {

        if (Object(response).code == 200) {
          var update = (this.quarteirao.is_active == true) ? "Activado" : "Desactivado";

          this.configService.showAlert("Quarteirao " + this.quarteirao.zona + " foi " + update, "alert-success", true);
        }

      }
    );

    for (let i = 0; i < this.items.length; ++i) {
      if (this.items[i].id == this.quarteirao.id) {
        this.items[i].is_active = this.quarteirao.is_active;
      }
    }
  }


  private selectBoxDistritosByMunicipio(id) {

    //console.log(this.quarteirao);

    this.http.call_get('distrito/getDistritosByMunicipio/' + id, null).subscribe(
      response => {
        console.log(Object(response).data);

        this.distritos = Object(response).data;
      }
    );
  }

  private selectBoxRuasByBairro(id) {

    //console.log(this.quarteirao);

    this.http.call_get('rua/getRuasByBairro/' + id, null).subscribe(
      response => {
        //console.log(Object(response).data);

        this.ruas = Object(response).data;
      }
    );
  }

  private selectBoxBairrosByMunicipio(id) {

    //console.log(this.quarteirao);

    this.http.call_get('bairro/getMunicipiosByProvincia/' + id, null).subscribe(
      response => {
        //console.log(Object(response).data);

        this.bairros = Object(response).data;
      }
    );
  }

  private selectBoxMunicipiosByProvincia(id) {

    //console.log(this.quarteirao);

    this.http.call_get('municipio/getMunicipiosByProvincia/' + id, null).subscribe(
      response => {
        //console.log(Object(response).data);

        this.municipios = Object(response).data;
      }
    );
  }


  getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.getQuarteiroes();
  }

  private getResidenciasByQuarteirao(id) {

    this.configService.loaddinStarter('start');

    this.http.call_get('quarteirao/getResidenciasByQuarteirao/' + id, this.http.filters).subscribe(

      response => {

        this.quarteiraos = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }

  public getConfiguracaos() {

    let result = null;

    const slugs = [
      this.config.provincia_default,
      this.config.distrito_view
    ];
    //console.log(slugs);

    for (let index = 0; index < slugs.length; index++) {

      //console.log(slugs[index]);
      this.http.__call('configuracao/getConfiguracaobySlug/' + slugs[index], null).subscribe(
        response => {

          //console.log(Object(response));

          if (Object(response).code != 200) { result = null; }
          else {

            result = Object(response).data;

            if (slugs[index] == this.config.provincia_default) {
              this.quarteirao.provincia_id = result.valor;
              this.selectBoxMunicipiosByProvincia(result.valor);
            }

            if (slugs[index] == this.config.distrito_view) this.distrito_view = Boolean(result.valor);
          }
        });
    }
  }


}
