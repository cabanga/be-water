import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrEditQuarteiraoComponent } from './create-or-edit-quarteirao.component';

describe('CreateOrEditQuarteiraoComponent', () => {
  let component: CreateOrEditQuarteiraoComponent;
  let fixture: ComponentFixture<CreateOrEditQuarteiraoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOrEditQuarteiraoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrEditQuarteiraoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
