import { Component, OnInit, Input, createPlatformFactory } from '@angular/core';
import { HttpService } from 'src/app/providers/http/http.service';
import { ConfigService } from 'src/app/providers/config/config.service';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { ExcelService } from 'src/app/services/excel.service';
import * as moment from 'moment';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ocorrencia',
  templateUrl: './ocorrencia.component.html',
  styleUrls: ['./ocorrencia.component.css']
})
export class OcorrenciaComponent implements OnInit {

  @Input() simpleFormOcorrencia: FormGroup;

  private ocorrencia = {
    id: null,
    motivo: null,
    rota_header_id: null,
    rota_run_id: null,
    tipo_ocorrencia_id: null,
    latitude: null,
    longitude: null,
    is_delected: null,
    user_id: null
  };

  public currentUser: any;

  private title: string = "Registar Ocorrência";

  private input_default: boolean = false;
  private input_required: boolean = false;

  private has_rotas_header: boolean = false;

  private items: any = [];

  private rotas_header: any = [];
  private rota_runs: any = [];
  private tipos_ocorrencias: any = [];
  /*  private estados: any = []; */

  constructor(private http: HttpService, private configService: ConfigService, private excelService: ExcelService, private auth: AuthService) {
    this.currentUser = this.auth.currentUserValue;
  }



  exportAsXLSX(): void {
    var CurrentDate = new Date();
    var nameFile = "lista_configuracoes -" + moment(CurrentDate).format('DD') + "-" + moment(CurrentDate).format('MM') + "-" + moment(CurrentDate).format('YYYY') + " "
      + moment(CurrentDate).format('H') + ":" + moment(CurrentDate).format('m')
    this.excelService.exportAsExcelFile(document.getElementsByClassName("exportAsXLSXCliente")[0], nameFile);
  }
  exportAsPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value, 'save');
  }

  imprimirPDF(): void {
    //this.reportLoja.relatorioLoja(this.cobrancas, this.simpleForm.value);
  }

  ngOnInit() {
    this.getPageFilterData(1);

  }

  private getDadosModal() {

  }

  private getOcorrencias() {

    this.configService.loaddinStarter('start');

    this.http.__call('ocorrencia/listagem', this.http.filters).subscribe(

      response => {
        this.http.filters.pagination.lastPage = Object(response).data.lastPage;
        this.http.filters.pagination.page = Object(response).data.page;
        this.http.filters.pagination.total = Object(response).data.total;
        this.http.filters.pagination.perPage = Object(response).data.perPage;

        this.items = Object(response).data.data;
        this.configService.loaddinStarter('stop');

      }
    );
  }


  view_ocorrencia = false;

  private saveOcorrencia() {
    console.log(this.ocorrencia);

    if (this.ocorrencia.id == null) {

      this.http.__call('ocorrencia/create', {
        motivo: this.ocorrencia.motivo,
        rota_run_id: this.ocorrencia.rota_run_id,
        tipo_ocorrencia_id: this.ocorrencia.tipo_ocorrencia_id,
        latitude: this.ocorrencia.latitude,
        longitude: this.ocorrencia.longitude,
        user_id: this.currentUser.user.id
      }).subscribe(
        res => {
          if (Object(res).code == 200) {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            /*    this.clearFormInputs();
               this.listarRotaRunByRotaHeader(); */

            this.getOcorrencias();

            this.configService.loaddinStarter('stop');
          } else {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
            this.configService.loaddinStarter('stop');
          }
        }
      )
    }
    else {

      this.http.__call('ocorrencia/update/' + this.ocorrencia.id, {
        motivo: this.ocorrencia.motivo,

        rota_run_id: this.ocorrencia.rota_run_id,
        tipo_ocorrencia_id: this.ocorrencia.tipo_ocorrencia_id,
        latitude: this.ocorrencia.latitude,
        longitude: this.ocorrencia.longitude,
        user_id: this.currentUser.user.id
      }).subscribe(
        res => {
          if (Object(res).code == 200) {
            this.configService.showAlert(Object(res).message, 'alert-success', true);
            /*    this.clearFormInputs();
               this.listarRotaRunByRotaHeader(); */

            this.getOcorrencias();

            this.configService.loaddinStarter('stop');
          } else {
            this.configService.showAlert(Object(res).message, 'alert-danger', true);
            this.configService.loaddinStarter('stop');
          }
        }
      )
    }

  }

  private onReset() {

    this.ocorrencia.motivo = null;
    this.ocorrencia.rota_header_id = null;
    this.ocorrencia.rota_run_id = null,
    this.ocorrencia.tipo_ocorrencia_id = null,
    this.ocorrencia.latitude = null,
    this.ocorrencia.longitude = null,
    this.ocorrencia.is_delected = false

  }


  private actualizarEstadoOcorrencia(item) {

    this.ocorrencia.id = item.id;
    this.ocorrencia.motivo = item.motivo;
    this.ocorrencia.rota_run_id = item.rota_run_id;
    this.ocorrencia.tipo_ocorrencia_id = item.tipo_ocorrencia_id;
    this.ocorrencia.latitude = item.latitude;
    this.ocorrencia.longitude = item.longitude;
    this.ocorrencia.is_delected = !item.is_delected;
    this.ocorrencia.user_id = item.user_id;

    //console.log(item);
    this.http.__call('ocorrencia/update/' + this.ocorrencia.id, this.ocorrencia).subscribe(
      response => {

        if (Object(response).code == 200) {
          var update = (this.ocorrencia.is_delected == true) ? "Activado" : "Desactivado";

          this.configService.showAlert("Ocorrência " + this.ocorrencia.motivo + " foi " + update, "alert-success", true);
        }

      }
    );

    for (let i = 0; i < this.items.length; ++i) {
      if (this.items[i].id == this.ocorrencia.id) {
        this.items[i].is_delected = this.ocorrencia.is_delected;
      }
    }
  }


  private setDataOcorrencia(item) {

    if (item !== undefined) {

      this.selectBoxTiposOcorrencias();

      this.title = "Editar Ocorrência";

      this.ocorrencia.id = item.id;
      this.ocorrencia.motivo = item.motivo;
      this.ocorrencia.rota_header_id = item.rota_header_id;
      this.ocorrencia.rota_run_id = item.rota_run_id;
      this.ocorrencia.tipo_ocorrencia_id = item.tipo_ocorrencia_id;
      this.ocorrencia.latitude = item.latitude;
      this.ocorrencia.longitude = item.longitude;
      this.ocorrencia.is_delected = item.is_delected;
      this.ocorrencia.user_id = item.user_id;

      this.getRotasHeaderWithRuns();
      this.selectBoxRotasRunPendentesByRotaReader();

    }
  }


  private getRotasHeaderWithRuns() {

    //console.log(this.currentUser);

    this.http.call_get('rota-header/getRotasHeaderWithRuns', null).subscribe(
      response => {

        //console.log(Object(response));

        this.rotas_header = Object(response).data;

        if (Object(response).data.length > 0) {
          this.has_rotas_header = true;
        }
        else {
          this.has_rotas_header = false;
        }

      }
    );
  }

  private selectBoxRotasRunPendentesByRotaReader() {

    this.http.call_get('rota-run/selectBoxRotasRunPendentesByRotaReader/' + this.ocorrencia.rota_header_id, null).subscribe(
      response => {

        //console.log(this.ocorrencia);
        //console.log(Object(response));

        this.rota_runs = Object(response);
      }
    );
  }

  private selectBoxTiposOcorrencias() {

    this.http.call_get('tipo-ocorrencia/selectBox', null).subscribe(
      response => {

        this.tipos_ocorrencias = Object(response);
      }
    );
  }


  private getPageFilterData(page: number) {
    if (this.http.filters.pagination.perPage == null) {
      return;
    }
    this.http.filters.pagination.page = page;
    this.getOcorrencias();
  }

  private setDefault() {
    this.input_default = !this.input_default;
  }

  private setRequired() {
    this.input_required = !this.input_required;
  }

}
