import { Component, OnInit, Compiler } from '@angular/core';
import { AuthService } from 'src/app/providers/auth/auth.service';
import { ConfigModuloService } from 'src/app/services/config-modulo.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {


  private app_environment: null;
  private app_version = "1.1.12 (15/01/2021)";
  private currentYear = new Date().getFullYear();

  public screenWidth: any;

  title = 'frontend';
  public currentUser: any = null;

  constructor(
    private auth: AuthService,
    private compiler: Compiler,
    private config: ConfigModuloService
  ) {
    this.currentUser = this.auth.currentUserValue;
    this.compiler.clearCache();
  }
  ngOnInit() {

    this.app_environment = this.auth.getAppEnvironment();
    if (this.app_environment == null) {

      var url = require('url');
      var app_url = url.parse(environment.app_url, true);

      this.app_environment = app_url.host;

      this.config.saveConfig(app_url.host, this.config.modulo.CONFIGURACOES, app_url.host);
    }

    //console.log(this.app_environment);

    this.config.initConfiguracaos();

    //location.reload(true);
    /*this.screenWidth = window.screen.width;*/
  }


}

