export interface UtilizadorDado {
  nome: string;
  sobrenome: string;
  username: string;
  email: string;
  telemovel: string;
  telefone: string;
  funcao_id: string;
  funcao: string;
  organismo_id: string;
  organismo: string;
}

export class ConfigRole {
  value: string;
  label: string;
  checked: boolean;
  slug: string;
  descricao: string;

  constructor(value: any, label: any, slug: any, descricao: any, checked?: boolean) {
    this.value = value;
    this.label = label;
    this.checked = checked ? checked : false;
    this.slug = slug;
    this.descricao = descricao;
  }
}

export interface OrganismoDTO {
  id: number;
  designacao: string;
  dataCriacao: Date;
  dataFim: Date;
  focalpoint: string;
  funcao: string;
  organismo_pai_designacao: string;
}

export interface AvaliacaoFornecerDTO {
  id: number;
  conformidade_agt: boolean;
  conformidade_inss: boolean;
  avaliacao: string;
  data_avaliacao: Date;
  avaliador: string;
}

export interface Investidor {
  id: number;
  nome: string;
  email: string;
  tipo: string;
  descricao: string;
}

export interface ProjectDTO {
  id: number;
  descricao: string;
  tipoProjecto: string;
  directorProjecto: string;
  gestorProjecto: string;
  prazoGarantia: string;
  nivelCriticidade: string;
  nivelRisco: string;
}

export interface AnexosData {
  id: number;
  nomeAnexo: string;
  fileName: string;
  tipo: string;
}

export interface IOSelect {
  value: string;
  label: string;
}

export interface IOSelectTableCell {
  value: string;
  title: string;
}

export interface Permisssions {
  id: number;
  name: string;
  slug: string;
  description: string;
  isActivo: boolean;
}

export interface ProjectDetail {
  id: number;
  revisao: string;
  tipo: string;
  descricao: string;
  valor: number;
  quantidade: number;
  total: number;
  totalFacturado: number;
  isActivo: boolean;
}

export interface FasesProjecto {
  id: number;
  ordem: string;
  descricao: string;
  data_arranque: Date;
  data_fim: Date;
  orcamento: number;
}
