export class Cliente {
    private nome;
    private telefone;
    private morada;
    private contribuente;

    public setNome(nome: string){
        this.nome = nome;
    }

    public setTelefone(telefone: string){
        this.telefone = telefone;
    }

    public setMorada(morada: string){
        this.morada = morada;
    }

    public setContribuente(contribuente: string){
        this.contribuente = contribuente;
    }
}
