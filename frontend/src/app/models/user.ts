export class User {
    id: number;
    email: string;
    nome: string;
    sobrenome: string; 
    alterPassword: boolean;
    telefone: string; 
    created_at: string;
    updated_at: string;
}
