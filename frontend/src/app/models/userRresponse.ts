import {User} from './user';
import {Token} from './token';

export class UserData {
  token: Token;
  user: User;
  empresa: any;
  role: any;
  permissions: any;
  alterPassword:boolean;
}

export class UserResponse {
  code: number;
  message: string;
  data: UserData;
}
