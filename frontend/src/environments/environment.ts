// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// import * as dotenv from 'dotenv'
// //const dotenv = require('dotenv')
//
// dotenv.config()

export const environment = {
  production: false,
  //app_url: 'http://localhost:3333/',
  apiVersion: 'v1',

  //QAS
  //app_url: 'http://epasln-qas.unig-erp.com:3349/' // BeWater - EPAS Lunda Norte
  //app_url: 'http://epasls-qas.unig-erp.com:3352/' // VistaWater - EPAS Lunda Sul
  app_url: 'http://epasn-qas.unig-erp.com:3355/' // VistaWater - EPAS Namibe

  //PRD
  //app_url: 'http://epasln.unig-erp.com:3348/' // BeWater - EPAS Lunda Norte
  //app_url: 'http://epasls.unig-erp.com:3349/' // VistaWater - EPAS Lunda Sul
  //app_url: 'http://epasn.unig-erp.com:3347/' // VistaWater - EPAS Namibe//PRD

  //app_url: 'http://188.166.174.217:3201/' // VistaWater - EPAS Namibe

  //app_url: process.env.API_URL

  };

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
